﻿using EnCryptDecrypt;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Xml;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region BrokerLogin
        //JK:260518
        public CLoginEflexUserResponse BrokerLogin(LoginUserInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CLoginEflexUserResponse lobjResponse = new CLoginEflexUserResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                decimal CompanyId = -1;
                string UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                bool IsApproaved = true;
                try
                {
                    TblUser UserObj = (from p in datacontext.TblUsers where p.Active == true && p.UserName == iObj.UserName && p.Pwd == CryptorEngine.Encrypt(iObj.Password) select p).Take(1).SingleOrDefault();
                    if (UserObj != null)
                    {
                        if (UserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker))
                        {
                            UserObj.UserToken = UserToken;
                            datacontext.SubmitChanges();
                            string FirstName = "";
                            string LastName = "";
                            bool IsPersonal = false;

                            TblBroker BrokerObj = (from p in datacontext.TblBrokers where p.Active == true && p.UserId == UserObj.UserId select p).Take(1).SingleOrDefault();
                            if (BrokerObj != null)
                            {
                                IsApproaved = Convert.ToBoolean(BrokerObj.IsApproved);
                                IsPersonal = Convert.ToBoolean(BrokerObj.IsPersonal);
                                List<TblBrokerCompany> BrokerCompanyObj = (from p in datacontext.TblBrokerCompanies where p.Active == true && p.BrokerId == BrokerObj.BrokerId select p).ToList();
                                if (BrokerCompanyObj != null && BrokerCompanyObj.Count() > 0)
                                {
                                    foreach (TblBrokerCompany item in BrokerCompanyObj)
                                    {
                                        CompanyId = item.CompanyId;
                                        string DbCon = DBFlag + item.CompanyId;
                                        string Constring = GetConnection(DbCon);
                                        using (EflexDBDataContext clientcontext = new EflexDBDataContext(Constring))
                                        {
                                            TblCompanyUser UserObj1 = (from p in clientcontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                                            if (UserObj1 != null)
                                            {
                                                UserObj1.UserToken = UserObj.UserToken;
                                                clientcontext.SubmitChanges();
                                            }

                                            clientcontext.Connection.Close();
                                        }
                                    }

                                }
                                lobjResponse.BrokerId = BrokerObj.BrokerId;


                            }

                            FirstName = UserObj.FirstName;
                            LastName = UserObj.LastName;

                            lobjResponse.FirstName = FirstName;
                            lobjResponse.LastName = LastName;
                            lobjResponse.IsPersonal = IsPersonal;
                            lobjResponse.IsApproaved = IsApproaved;
                            lobjResponse.ID = UserObj.UserId;
                            lobjResponse.UserName = UserObj.FirstName + " " + UserObj.LastName;
                            lobjResponse.UserTypeId = Convert.ToDecimal(UserObj.UserTypeId);
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.CompanyId = CompanyId;
                            lobjResponse.UserToken = UserToken;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        }
                        else
                        {
                            lobjResponse.Message = "Please enter valid login credentials.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                        }
                    }
                    else
                    {

                        lobjResponse.Message = "Please input correct Username/Password.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

              
                CreateMasterResponseLog(datacontext, StartTime, -1, "BrokerLogin", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }



        #endregion LoginEflexUser

        #region AddBroker
        public CResponse AddBroker(AddBrokerInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.EmailAddress == iObj.Email && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).Take(1).SingleOrDefault();
                        if (UserObj == null)
                        {
                            TblUser UserObj1 = (from p in mastercontext.TblUsers where p.Active == true && p.UserName == iObj.UserName && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).Take(1).SingleOrDefault();
                            if (UserObj1 == null)
                            {


                                TblUser LoggedInUserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                                bool IsApproved = false;

                                if (LoggedInUserObj != null)
                                {

                                    if (LoggedInUserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin) || LoggedInUserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin))
                                    {
                                        IsApproved = true;
                                    }
                                    else
                                    {
                                        IsApproved = false;
                                    }
                                }

                                string Password = RandomString();
                                string EncryptPwd = CryptorEngine.Encrypt(Password);
                                
                                TblUser Obj = new TblUser()
                                {
                                    FirstName = iObj.FirstName,
                                    LastName = iObj.LastName,
                                    UserName = iObj.UserName,
                                    ContactNo = iObj.MobileNo,
                                    Pwd = EncryptPwd,
                                    EmailAddress = iObj.Email,
                                    UserTypeId = Convert.ToInt16(CObjects.enumMasterUserType.Broker),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                mastercontext.TblUsers.InsertOnSubmit(Obj);
                                mastercontext.SubmitChanges();

                                TblBroker BrokerObj = new TblBroker()
                                {
                                    FirstName = iObj.FirstName,
                                    LastName = iObj.LastName,
                                    Address1 = iObj.Address1,
                                    Address2 = iObj.Address2,
                                    City = iObj.City,
                                    Email = iObj.Email,
                                    ProvinceId = iObj.ProvinceId,
                                    PostalCode = iObj.PostalCode,
                                    Country = iObj.Country,
                                    MobileNo = iObj.MobileNo,
                                    LandlineNo = iObj.LandlineNo,
                                    Licence = iObj.Licence,
                                    UserId = Obj.UserId,
                                    DOB = Convert.ToDateTime(iObj.DOB),
                                    EOInsuranceNo = iObj.EOInsuranceNo,
                                    SocialInsuranceNo = iObj.SocialInsuranceNo,
                                    IsAgrement = iObj.IsAgrement,
                                    Active = true,
                                    IsApproved = IsApproved,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow,
                                    IsPersonal = iObj.IsPersonal,
                                    UploadLicFront = iObj.UploadLicenceFront,
                                    MasterUserId = iObj.MasterUserId,
                                    GSTNO = iObj.GSTNo
                                };
                                mastercontext.TblBrokers.InsertOnSubmit(BrokerObj);
                                mastercontext.SubmitChanges();

                                
                                UpdateEflexMsgUser(Obj, "ADD", -1);

                                #region SendMail

                               
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + iObj.FirstName + ",<br/><br/>");
                                builder.Append("Welcome to eFlex. Please use the following credentials for login to eFlex Portal:<br/><br/>");
                                builder.Append("Username: " + iObj.UserName + "<br/><br/>");
                                builder.Append("Password: " + Password + "<br/><br/>");
                                builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                                builder.Append("Regards,<br/><br/>");
                                builder.Append("eFlex Team<br/><br/>");

                                string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                //send mail

                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = iObj.Email;
                                Mail.Subject = "eFlex Broker Account Registered";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = -1;
                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                #endregion SendMail

                                string Text = "A new broker named as " + iObj.FirstName + " " + iObj.LastName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                CreateHistoryLog(mastercontext, -1, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);

                                string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) && p.UserId != iObj.LoggedInUserId select p.UserId).ToList());
                                EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Broker already exists with this username.";
                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Broker already exists with this email address.";

                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "AddBroker", InputJson,lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region EditBroker
        public CResponse EditBroker(EditBrokerInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        bool isUpdate = false;
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            
                           
                                if (UserObj.UserName == iObj.UserName)
                                {
                                    isUpdate = true;
                                }
                                else
                                {
                                    TblUser UserObj1 = (from p in mastercontext.TblUsers where p.Active == true && p.UserName == iObj.UserName select p).Take(1).SingleOrDefault();
                                    if (UserObj1 != null)
                                    {
                                        isUpdate = false;
                                    }
                                    else
                                    {
                                        isUpdate = true;
                                    }
                                }
                        

                            if (isUpdate)
                            {
                             
                                UserObj.FirstName = iObj.FirstName;
                                UserObj.LastName = iObj.LastName;
                                UserObj.UserName = iObj.UserName;
                                UserObj.EmailAddress = iObj.Email;
                                UserObj.DateModified = DateTime.UtcNow;

                                UpdateEflexMsgUser(UserObj, "UPDATE", -1);

                                TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();

                                if (BrokerObj != null)
                                {
                                 BrokerObj.FirstName = iObj.FirstName;
                                 BrokerObj.LastName = iObj.LastName;
                                 BrokerObj.Address1 = iObj.Address1;
                                 BrokerObj.Address2 = iObj.Address2;
                                 BrokerObj.City = iObj.City;
                                 BrokerObj.Email = iObj.Email;
                                 BrokerObj.ProvinceId = iObj.ProvinceId;
                                 BrokerObj.PostalCode = iObj.PostalCode;
                                 BrokerObj.Country = iObj.Country;
                                 BrokerObj.MobileNo = iObj.MobileNo;
                                 BrokerObj.LandlineNo = iObj.LandlineNo;
                                 BrokerObj.Licence = iObj.Licence;
                                 BrokerObj.IsAgrement = iObj.IsAgrement;
                                 BrokerObj.DOB	= Convert.ToDateTime(iObj.DOB);
                                 BrokerObj.SocialInsuranceNo = iObj.SocialInsuranceNo;
                                 BrokerObj.BusinessNo = iObj.BusinessNo;
                                 BrokerObj.EOInsuranceNo = iObj.EOInsuranceNo;
                                 BrokerObj.CompanyName = iObj.CompanyName;
                                 BrokerObj.CompanyAddress = iObj.CompanyAddress;
                                 BrokerObj.CompanyCity = iObj.CompanyCity;
                                 BrokerObj.CompanyProvinceId = Convert.ToDecimal(iObj.CompanyProvinceId);
                                 BrokerObj.CompanyPostalCode = iObj.CompanyPostalCode;
                                 BrokerObj.CompanyContactPerson = iObj.CompanyContactPerson;
                                 BrokerObj.CompanyContactNo = iObj.CompanyContactNo;
                                 BrokerObj.CompanyEmail = iObj.CompanyEmail;
                                 BrokerObj.DateModified = DateTime.UtcNow;
                                 BrokerObj.IsPersonal = iObj.IsPersonal;
                                 BrokerObj.UploadLicFront = iObj.UploadLicenceFront;
                                 BrokerObj.GSTNO = iObj.GSTNo;
                                 BrokerObj.DateModified = DateTime.UtcNow;
                                }
                                mastercontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Broker already exists with this email address.";
                            }
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "EditBroker", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region DeleteBroker
        public CResponse DeleteBroker(DeleteBrokerInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                          
                                UserObj.Active = false;
                                UserObj.DateModified = DateTime.UtcNow;

                                TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.UserId == iObj.Id select p).Take(1).SingleOrDefault();

                                if (BrokerObj != null)
                                {
                                    BrokerObj.Active = false;
                                    BrokerObj.DateModified = DateTime.UtcNow;
                                }
                                TblBrokerCompany BrokerCompObj = (from p in mastercontext.TblBrokerCompanies 
                                                                  join q in mastercontext.TblBrokers on p.BrokerId equals q.BrokerId
                                                                  where p.Active == true && q.UserId == iObj.Id
                                                                  select p).Take(1).SingleOrDefault();

                                if (BrokerCompObj != null)
                                {
                                    BrokerCompObj.Active = false;
                                   
                                }

                                mastercontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                           
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Broker does'nt exists.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "DeleteBroker", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region MarkBrokerAsApproved
        public CResponse MarkBrokerAsApproved(MarkBrokerAsApprovedInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        string LoggedInUserName =(from p in mastercontext.TblUsers where p.Active == true && p.UserId==iObj.LoggedInUserId select p.FirstName+ " "+p.LastName).Take(1).SingleOrDefault();
                        TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            if (BrokerObj != null)
                            {
                                
                                BrokerObj.IsApproved = iObj.IsApproved;
                                BrokerObj.DateModified = DateTime.UtcNow;
                                string lstrMsg = "";
                                string Status = "";
                                #region SendMail
                                if(iObj.IsApproved)
                                {
                                    lstrMsg = "Your account has been marked as approved by Admin.";
                                    Status = "Approved";
                                }
                                else
                                {
                                    lstrMsg = "Your account has been marked as unapproved by Admin.";
                                    Status = "Un-Approved";

                                }

                                TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();
                                if (UserObj != null)
                                {
                                    
                                    StringBuilder builder = new StringBuilder();
                                    builder.Append("Hello " + BrokerObj.FirstName + ",<br/><br/>");
                                    builder.Append(lstrMsg);
                                    if (iObj.IsApproved)
                                    {
                                        builder.Append("Following is your login credentials.<br/><br/>");
                                        builder.Append("Username: " + UserObj.UserName + "<br/><br/>");
                                        builder.Append("Password: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                                        builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                                    }
                                    builder.Append("Regards,<br/><br/>");
                                    builder.Append("eFlex Team<br/><br/>");

                                    string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                    lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                    //send mail

                                    string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                    string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                    string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                    Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                    CMail Mail = new CMail();
                                    Mail.EmailTo = BrokerObj.Email;
                                    Mail.Subject = "Account - " + Status;
                                    Mail.MessageBody = lstrMessage;
                                    Mail.GodaddyUserName = GodaddyUserName;
                                    Mail.GodaddyPassword = GodaddyPwd;
                                    Mail.Server = Server;
                                    Mail.Port = Port;
                                    Mail.CompanyId = -1;
                                    bool IsSent = Mail.SendEMail(mastercontext, true);
                                }

                                #endregion SendMail

                                string Text = BrokerObj.FirstName + "'s (broker) account has been marked as " + Status.ToLower() + " by " + LoggedInUserName;
                                string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) && p.UserId != iObj.LoggedInUserId select p.UserId).ToList());
                                EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                            }
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "MarkBrokerAsApproved", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfBrokers
        public CListOfBrokersResponse ListOfBrokers(LazyLoadingInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            bool IsLogOut = false;
            CListOfBrokersResponse lobjResponse = new CListOfBrokersResponse();
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        decimal? StatusId = null;
                        if(iObj.StatusId == -1)
                        {
                            StatusId = null;
                        }
                        else
                        {
                            StatusId = iObj.StatusId;
                        }
                        decimal TotalBroker = (from p in mastercontext.TblBrokers where p.Active == true select p).Count();
                        decimal ApprovedBroker = (from p in mastercontext.TblBrokers where p.Active == true && p.IsApproved == true select p).Count();
                        decimal UnApprovedTotalBroker = (from p in mastercontext.TblBrokers where p.Active == true && p.IsApproved == false select p).Count();

                        List<ListOfBrokers> Obj = (from p in mastercontext.sp_ListOfBrokers(iObj.ChunckStart, Convert.ToInt32(iObj.ChunckSize), iObj.SearchString, StatusId)
                                                   select new ListOfBrokers()
                                                   {
                                                       UserId = Convert.ToDecimal(p.UserId),
                                                       BrokerId = p.BrokerId,
                                                       FName = p.FirstName,
                                                       LName = p.LastName,
                                                       Email = p.Email,
                                                       Address = p.Address1,
                                                       City = p.City,
                                                       PostalCode = p.PostalCode,
                                                       Country = p.Country,
                                                       IsPersonal = Convert.ToBoolean(p.IsPersonal),
                                                       Province = (p.ProvinceId == -1 || p.ProvinceId == null) ? "" :(from a in mastercontext.TblProvinces where a.Active == true && a.ProvinceId==p.ProvinceId select a.Name).Take(1).SingleOrDefault(),
                                                       IsApproved = Convert.ToBoolean(p.IsApproved)
                                                   }).ToList();
                        lobjResponse.ListOfBrokers = Obj;
                        lobjResponse.TotalBroker = TotalBroker;
                        lobjResponse.ApprovedBroker = ApprovedBroker;
                        lobjResponse.UnApprovedBroker = UnApprovedTotalBroker;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfBrokers", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region RegisterBroker
        public CRegisterBrokerResponse RegisterBroker(RegisterBrokerInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CRegisterBrokerResponse lobjResponse = new CRegisterBrokerResponse();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.EmailAddress == iObj.UserName && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).Take(1).SingleOrDefault();
                    if (UserObj == null)
                    {
                        TblUser UserObj1 = (from p in mastercontext.TblUsers where p.Active == true && p.UserName == iObj.UserName && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).Take(1).SingleOrDefault();
                        if (UserObj1 == null)
                        {


                            string Password = RandomString();
                            string EncryptPassword = CryptorEngine.Encrypt(Password);

                            TblUser Obj = new TblUser()
                            {
                                FirstName = iObj.FirstName,
                                LastName = iObj.LastName,
                                UserName = iObj.UserName,
                                ContactNo = iObj.MobileNo,
                                Pwd = EncryptPassword,
                                EmailAddress = iObj.Email,
                                UserToken = UserToken,
                                UserTypeId = Convert.ToInt16(CObjects.enumMasterUserType.Broker),
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            mastercontext.TblUsers.InsertOnSubmit(Obj);
                            mastercontext.SubmitChanges();

                            TblBroker BrokerObj = new TblBroker()
                            {
                                FirstName = iObj.FirstName,
                                LastName = iObj.LastName,
                                Address1 = iObj.Address1,
                                Address2 = iObj.Address2,
                                City = iObj.City,
                                Email = iObj.Email,
                                ProvinceId = iObj.ProvinceId,
                                PostalCode = iObj.PostalCode,
                                Country = iObj.Country,
                                MobileNo = iObj.MobileNo,
                                LandlineNo = iObj.LandlineNo,
                                Licence = iObj.Licence,
                                UserId = Obj.UserId,
                                IsAgrement = iObj.IsAgrement,
                                Active = true,
                                IsApproved = false,
                                DOB = Convert.ToDateTime(iObj.DOB),
                                SocialInsuranceNo = iObj.SocialInsuranceNo,
                                BusinessNo = iObj.BusinessNo,
                                EOInsuranceNo = iObj.EOInsuranceNo,
                                CompanyName = iObj.CompanyName,
                                CompanyAddress = iObj.CompanyAddress,
                                CompanyCity = iObj.CompanyCity,
                                CompanyProvinceId = Convert.ToDecimal(iObj.CompanyProvinceId),
                                CompanyPostalCode = iObj.CompanyPostalCode,
                                CompanyContactPerson = iObj.CompanyContactPerson,
                                CompanyContactNo = iObj.CompanyContactNo,
                                CompanyEmail = iObj.CompanyEmail,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow,
                                IsPersonal = iObj.IsPersonal,
                                UploadLicFront = iObj.UploadLicenceFront,
                                GSTNO = iObj.GSTNo
                            };
                            mastercontext.TblBrokers.InsertOnSubmit(BrokerObj);
                            mastercontext.SubmitChanges();

                            UpdateEflexMsgUser(Obj, "ADD", -1);

                            #region SendMail

                           
                            StringBuilder builder = new StringBuilder();
                            builder.Append("Hello " + iObj.FirstName + ",<br/><br/>");
                            builder.Append("Thanks for register in eFlex portal. You will get an approval mail soon from admin.<br/><br/>");
                            
                            builder.Append("Regards,<br/><br/>");
                            builder.Append("eFlex Team<br/><br/>");

                            string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                            lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                            //send mail

                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                            CMail Mail = new CMail();
                            Mail.EmailTo = iObj.Email;
                            Mail.Subject = "eFlex Broker Account Registered";
                            Mail.MessageBody = lstrMessage;
                            Mail.GodaddyUserName = GodaddyUserName;
                            Mail.GodaddyPassword = GodaddyPwd;
                            Mail.Server = Server;
                            Mail.Port = Port;
                            Mail.CompanyId = -1;
                            bool IsSent = Mail.SendEMail(mastercontext, true);


                            #endregion SendMail

                            string Text = "A new broker named as " + iObj.FirstName + " " + iObj.LastName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                            CreateHistoryLog(mastercontext, -1, Obj.UserId, Obj.UserId, Text);

                            string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                            EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.ID = Obj.UserId;
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Broker already exists with this Username.";

                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "Broker already exists with this email address.";

                    }
                   
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.UserToken = UserToken;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "RegisterBroker", "", lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion
        
        #region AddEflexUserPAPDetails
        public CResponse AddEflexUserPAPDetails(AddEflexUserPAPDetailsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(datacontext))
                    {
                        TblUserPAPDetail Obj = new TblUserPAPDetail()
                        {
                            BankName = iObj.BankName,
                            TransitId = iObj.TransitId,
                            InstitutionId = iObj.InstitutionId,
                            AccountNumber = iObj.AccountNumber,
                            UserId = iObj.UserId,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow,
                            Active = true
                        };
                        datacontext.TblUserPAPDetails.InsertOnSubmit(Obj);
                        datacontext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(datacontext, StartTime, -1, "AddEflexUserPAPDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdatePAPDetails

        #region GetBrokerProfile
        public CGetBrokerProfileResponse GetBrokerProfile(IdInputv2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CGetBrokerProfileResponse lobjResponse = new CGetBrokerProfileResponse();
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                
                    try
                    {
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.Id && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).Take(1).SingleOrDefault();
                        if(UserObj != null)
                        {
                            GetBrokerProfile Obj = (from p in mastercontext.TblBrokers
                                                    join q in mastercontext.TblUsers on p.UserId equals q.UserId
                                                    where p.Active == true && p.UserId == iObj.Id && q.Active == true
                                                    select new GetBrokerProfile()
                                                       {
                                                           FirstName = p.FirstName,
                                                           LastName = p.LastName,
                                                           Address1 = p.Address1,
                                                           Address2 = p.Address2,
                                                           City = p.City,
                                                           Email = p.Email,
                                                           ProvinceId = p.ProvinceId == null ? -1 : Convert.ToDecimal(p.ProvinceId),
                                                           PostalCode = p.PostalCode,
                                                           Country = p.Country,
                                                           MobileNo = p.MobileNo,
                                                           LandlineNo = p.LandlineNo,
                                                           Licence = p.Licence,
                                                           IsAgrement = p.IsAgrement ==  null ? false : Convert.ToBoolean(p.IsAgrement),
                                                           UserName = q.UserName,
                                                           DOB = Convert.ToString(p.DOB),
                                                           SocialInsuranceNo = p.SocialInsuranceNo,
                                                           BusinessNo = p.BusinessNo,
                                                           EOInsuranceNo = p.EOInsuranceNo,
                                                           GSTNo = p.GSTNO,
                                                           UploadLicenceFront = p.UploadLicFront,
                                                           CompanyName = p.CompanyName,
                                                           CompanyAddress = p.CompanyAddress,
                                                           CompanyCity = p.CompanyCity,
                                                           CompanyProvinceId = p.CompanyProvinceId == null ? -1 : Convert.ToDecimal(p.CompanyProvinceId),
                                                           CompanyPostalCode = p.CompanyPostalCode,
                                                           CompanyContactPerson = p.CompanyContactPerson,
                                                           CompanyContactNo = p.CompanyContactNo,
                                                           CompanyEmail = p.CompanyEmail,
                                                           IsPersonal = Convert.ToString(p.IsPersonal),
                                                           Password = CryptorEngine.Decrypt(q.Pwd),
                                                           FinancialDetails = (from a in mastercontext.TblUserPAPDetails
                                                                               where a.Active == true && a.UserId == p.UserId
                                                                               select new FinancialDetails()
                                                                               {
                                                                                   UserPAPDetailsId = a.PAPDetailId,
                                                                                   UserId = Convert.ToDecimal(p.UserId),
                                                                                   BankName = a.BankName,
                                                                                   TransitId = a.TransitId,
                                                                                   InstitutionId = a.InstitutionId,
                                                                                   AccountNumber = a.AccountNumber
                                                                               }).Take(1).SingleOrDefault(),
                                                       }).Take(1).SingleOrDefault();

                            GetBrokerFigures FiguresObj = (from p in mastercontext.sp_GetBrokerFigures(iObj.Id)
                                                           select new GetBrokerFigures()
                                                               {
                                                                   TotalCompanies = p.TotalCompanies == null ? 0 : Convert.ToDecimal(p.TotalCompanies),
                                                                   TotalAmountReceived = p.TotalAmtReceived == null ? 0 : Convert.ToDecimal(p.TotalAmtReceived),
                                                                   ToTalDueAmt = p.TotalDueAmt == null ? 0 : Convert.ToDecimal(p.TotalDueAmt)
                                                               }).Take(1).SingleOrDefault();
                            lobjResponse.GetBrokerProfileData = Obj;
                            lobjResponse.GetBrokerFigures = FiguresObj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Message = "No record found.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                        
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                
                    CreateMasterResponseLog(mastercontext, StartTime, -1, "GetBrokerProfile", InputJson, lobjResponse.Message);
                    mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddBrokerCompany
        public CAddBrokerCompanyResponse AddBrokerCompany(AddBrokerCompanyInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CAddBrokerCompanyResponse lobjResponse = new CAddBrokerCompanyResponse();
            UserId = iObj.LoggedInUserId;
            string UserToken = "";
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {

                TblUser UserObj1 = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == UserId select p).Take(1).SingleOrDefault();
                if(UserObj1 != null)
                {
                    UserToken = UserObj1.UserToken;
                }
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                      if (iObj.CompanyId == -1)
                       {
                           TblCompany CheckCompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && (p.CompanyName == iObj.CompanyName && p.PostalCode == iObj.PostalCode) select p).Take(1).SingleOrDefault();
                           if (CheckCompanyObj == null)
                           {
                             
                               TblCompany CheckCompanyObj1 = (from p in mastercontext.TblCompanies where p.Active == true && (p.CompanyName == iObj.CompanyName && p.Address1 == iObj.Address1) select p).Take(1).SingleOrDefault();
                               if (CheckCompanyObj1 == null)
                               {
                                   string Password = RandomString();
                                   string EncryptPwd = CryptorEngine.Encrypt(Password);
                                   TblCompany CompObj = new TblCompany()
                                   {
                                       CompanyName = iObj.CompanyName,
                                       Address1 = iObj.Address1,
                                       Address2 = iObj.Address2,
                                       City = iObj.City,
                                       Province = iObj.ProvinceId,
                                       PostalCode = iObj.PostalCode,
                                       Country = iObj.Country,
                                       OwnerContactNo = iObj.OwnerContactNo,
                                       OwnerEmail = iObj.OwnerEmail,
                                       ServiceUrl = iObj.ServiceUrl,
                                       HostUrl = iObj.HostUrl,
                                       UniqueCode = iObj.Code,
                                       UserName = iObj.UserName,
                                       Pwd = EncryptPwd,
                                       FirstName = iObj.FirstName,
                                       LastName = iObj.LastName,
                                       
                                       Active = true,
                                       IsBlocked = false,
                                       IsApproved = true,
                                       DateCreated = DateTime.UtcNow,
                                       DateModified = DateTime.UtcNow
                                   };
                                   mastercontext.TblCompanies.InsertOnSubmit(CompObj);
                                   mastercontext.SubmitChanges();

                                   decimal CompanyId = CompObj.CompanyId;
                                   TblBrokerCompany BrokerCompObj = new TblBrokerCompany()
                                   {
                                       CompanyId = CompanyId,
                                       BrokerId = iObj.BrokerId,
                                       Active = true,
                                       DateCreated = DateTime.UtcNow
                                   };
                                   mastercontext.TblBrokerCompanies.InsertOnSubmit(BrokerCompObj);
                                   mastercontext.SubmitChanges();

                                   #region ComissionCharges
                                   TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                   if (AdminFeeObj != null)
                                   {

                                       TblBrokerCompanyFee BrokerFeeObj = new TblBrokerCompanyFee()
                                       {
                                           CompanyId = CompanyId,
                                           AdminFee = AdminFeeObj.AdminMax,
                                           BrokerFee = iObj.CommissionCharges,
                                           BrokerId = iObj.BrokerId,
                                           Active = true,
                                           DateCreated = DateTime.UtcNow
                                       };
                                       mastercontext.TblBrokerCompanyFees.InsertOnSubmit(BrokerFeeObj);
                                       mastercontext.SubmitChanges();
                                   }
                                   #endregion


                                   #region Create DB
                                   if (CompanyId > 0)
                                   {
                                       TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                       if (CompanyObj != null)
                                       {
                                           #region Create Database
                                           string DBName = CompanyNameFlag + CompanyId;
                                           string DbCon = DBFlag + CompanyId;
                                           mastercontext.ExecuteCommand("CREATE DATABASE [" + DBName + "]");
                                           string contents = "";
                                           string DbPatch = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                           contents = File.ReadAllText(DbPatch + "DbPatch.txt");
                                           string Patch = contents.Replace("lstrDbName", DBName);
                                           mastercontext.ExecuteCommand(Patch.ToString());
                                           #endregion  Create Database

                                           #region Adding Connectionstring and run procedure path
                                           string ConnStrPath = ConStrPath;
                                           XmlDocument xmlDoc = new XmlDocument();
                                           xmlDoc.Load(ConnStrPath);
                                           XmlNode xmlnode = xmlDoc.SelectSingleNode("//connectionStrings");
                                           xmlnode.InnerXml += "<add name=\"" + DbCon + "\" connectionString=\"Data Source=18.216.133.20;Initial Catalog=" + DBName + ";Integrated Security=false;Max Pool Size=50000;Pooling=True;User ID=maan;Password=Jetking34\"  providerName=\"System.Data.SqlClient\" />";
                                           xmlDoc.Save(ConnStrPath);

                                           XmlNodeList nodes = xmlDoc.SelectNodes("/connectionStrings/add[@name='" + DbCon + "']");
                                           string Constring = nodes[0].Attributes["connectionString"].Value.ToString();
                                           using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                                           {
                                               contents = "";
                                               string[] filePaths = Directory.GetFiles(DbPatch + "Procedure Patch\\");
                                               foreach (var file in filePaths)
                                               {
                                                   contents = "";
                                                   contents = File.ReadAllText(file);
                                                   string ProcedurePatch = contents.Replace("lstrDbName", DBName);
                                                   datacontextClient.ExecuteCommand(ProcedurePatch.ToString());
                                               }

                                               #region Create Folders
                                               if (!Directory.Exists(ConPath + CompanyId))
                                               {
                                                   Directory.CreateDirectory(ConPath + CompanyId);
                                                   DirectoryInfo dInfo = new DirectoryInfo(ConPath + CompanyId);
                                                   DirectorySecurity dSecurity = dInfo.GetAccessControl();
                                                   dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                                                   dInfo.SetAccessControl(dSecurity);


                                               }
                                               #endregion Create Folders



                                               TblCompanyUser UserObj = new TblCompanyUser()
                                               {
                                                   FirstName = iObj.FirstName,
                                                   LastName = iObj.LastName,
                                                   EmailAddress = iObj.OwnerEmail,
                                                   UserName =iObj.UserName,
                                                   UserTypeId = Convert.ToInt32(CObjects.enmUserType.Owner),
                                                   Pwd = EncryptPwd,
                                                   IsActive = true,
                                                   UserToken = UserToken,
                                                   ContactNo = iObj.OwnerContactNo,
                                                   DateModified = DateTime.UtcNow,
                                                   DateCreated = DateTime.UtcNow,
                                                   Active = true
                                               };
                                               datacontextClient.TblCompanyUsers.InsertOnSubmit(UserObj);
                                               datacontextClient.SubmitChanges();

                                               TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.BrokerId == iObj.BrokerId select p).Take(1).SingleOrDefault();

                                               if (BrokerObj != null)
                                               {
                                                   TblCompanyReferral Obj1 = new TblCompanyReferral()
                                                   {
                                                       CompanyId = CompanyId,
                                                       CompanyUserId = UserObj.CompanyUserId,
                                                       BrokerId = iObj.BrokerId,
                                                       ReferralTypeId = 1,
                                                       Name = BrokerObj.FirstName,
                                                       OtherDetails = "",
                                                       Active = true,
                                                       DateCreated = DateTime.UtcNow
                                                   };
                                                   datacontextClient.TblCompanyReferrals.InsertOnSubmit(Obj1);
                                                   datacontextClient.SubmitChanges();
                                               }
                                               CompanyObj.DBCon = DbCon;
                                               CompanyObj.StagesCovered = "1";
                                               mastercontext.SubmitChanges();

                                               UpdateMsgUser(UserObj, "ADD", CompanyId);

                                               #region SendMail

                                               
                                               StringBuilder builder = new StringBuilder();

                                               builder.Append("Hello  " + iObj.FirstName + ",<br/><br/>");
                                               builder.Append("Your company, " + iObj.CompanyName + ", has been successfully registered with eFlex!<br/>");
                                               builder.Append("To access your company profile, please login <a href=" + lstrUrl + " target='_blank'>" + lstrUrl + "</a>.<br/><br/>");
                                               builder.Append("Your credentials are:<br/>");
                                               builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/>");
                                               builder.Append("Username: " + iObj.UserName.Split('_')[1] + "<br/>");
                                               builder.Append("Password: " + Password + "<br/><br/><br/>");
                                               builder.Append("If you have any questions or concerns about your eFlex account, please reach our to our team anytime at 416-748-9879 or eFlex@eclipseEIO.com <br/><br/>");
                                               builder.Append("Thanks, and enjoy your eFlex account(s)!<br/>");
                                               builder.Append("The eclipse EIO Team");

                                               string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                                               lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                               //send mail

                                               string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                               string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                               string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                               Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                               CMail Mail = new CMail();
                                               Mail.EmailTo = CompanyObj.OwnerEmail.Trim();
                                               Mail.Subject = "eFlex Corporate Account Registered";
                                               Mail.MessageBody = lstrMessage;
                                               Mail.GodaddyUserName = GodaddyUserName;
                                               Mail.GodaddyPassword = GodaddyPwd;
                                               Mail.Server = Server;
                                               Mail.Port = Port;
                                               Mail.CompanyId = CompanyId;
                                               bool IsSent = Mail.SendEMail(mastercontext, true);


                                               #endregion SendMail

                                               CreateResponseLog(datacontextClient, StartTime, CompanyId, "AddCompany", InputJson, lobjResponse.Message);
                                               lobjResponse.CompanyUserId = UserObj.CompanyUserId;

                                               datacontextClient.Connection.Close();
                                           }

                                           #endregion Adding Connectionstring and run procedure path

                                           #region Provincial Tax Details
                                           TaxDetails TaxObj = (from p in mastercontext.TblProvinces
                                                                where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                                select new TaxDetails()
                                                                {
                                                                    AdminMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                                    AdminMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                                    BrokerMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                                    BrokerMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                                    HST = Convert.ToDecimal(p.HST),
                                                                    RSTax = Convert.ToDecimal(p.RST),
                                                                    GST = Convert.ToDecimal(p.GST),
                                                                    PPTax = Convert.ToDecimal(p.PPT),
                                                                }).Take(1).SingleOrDefault();

                                           #endregion Provincial Tax Details

                                           lobjResponse.TaxDetails = TaxObj;
                                           lobjResponse.DbCon = DbCon;

                                           string Text = "A new company named as  " + iObj.CompanyName + " is resgistered on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                           CreateHistoryLog(mastercontext, CompanyId, -1, lobjResponse.CompanyUserId, Text);

                                           string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                           EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                       }
                                       lobjResponse.CompanyId = CompanyId;

                                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                       lobjResponse.Message = "SUCCESS";
                                       lobjResponse.UserToken = UserToken;
                                       mastercontext.Connection.Close();
                                   }
                                   #endregion

                                   #region SendMail commented

                                   
                                   #endregion SendMail

                                   lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                               }
                               else
                               {
                                   lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                   lobjResponse.Message = "Company already exist with same name and address.";
                               }
                           }
                           else
                           {
                               lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                               lobjResponse.Message = "Company already exist with same name and address.";
                           }
                       }
                       else
                       {
                           TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                           if (CompanyObj != null)
                           {
                               TblCompany CompanyObj1 = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyName.ToUpper() == iObj.CompanyName.ToUpper() && p.Address1 == iObj.Address1 && p.PostalCode == iObj.PostalCode select p).Take(1).SingleOrDefault();
                               if (CompanyObj1 != null && CompanyObj1.CompanyId != iObj.CompanyId)
                               {
                                   lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                   lobjResponse.Message = "Company already exist with this name.";
                               }
                               else
                               {
                                   CompanyObj.CompanyName = iObj.CompanyName;
                                   CompanyObj.FirstName = iObj.FirstName;
                                   CompanyObj.LastName = iObj.LastName;
                                   CompanyObj.Address1 = iObj.Address1;
                                   CompanyObj.Address2 = iObj.Address2;
                                   CompanyObj.City = iObj.City;
                                   CompanyObj.Province = iObj.ProvinceId;
                                   CompanyObj.PostalCode = iObj.PostalCode;
                                   CompanyObj.Country = iObj.Country;
                                   CompanyObj.OwnerContactNo = iObj.OwnerContactNo;
                                   CompanyObj.OwnerEmail = iObj.OwnerEmail;
                                   CompanyObj.UserName = iObj.UserName;
                                  
                                   CompanyObj.DateModified = DateTime.UtcNow;
                                   mastercontext.SubmitChanges();
                                   lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                   lobjResponse.Message = "Company details updated successfully.";
                                   lobjResponse.DbCon = CompanyObj.DBCon;
                                   lobjResponse.CompanyId = CompanyObj.CompanyId;

                                   #region Provincial Tax Details
                                   TaxDetails TaxObj = (from p in mastercontext.TblProvinces
                                                        where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                        select new TaxDetails()
                                                        {
                                                            AdminMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                            AdminMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                            BrokerMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                            BrokerMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                            HST = Convert.ToDecimal(p.HST),
                                                            RSTax = Convert.ToDecimal(p.RST),
                                                            GST = Convert.ToDecimal(p.GST),
                                                            PPTax = Convert.ToDecimal(p.PPT),
                                                        }).Take(1).SingleOrDefault();

                                   #endregion Provincial Tax Details


                                   string DbCon = DBFlag + iObj.CompanyId;
                                   string Constring = GetConnection(DbCon);
                                   using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                                   {
                                       TblCompanyUser UserObj = (from p in datacontextClient.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                                       if (UserObj != null)
                                       {
                                           lobjResponse.CompanyUserId = UserObj.CompanyUserId;
                                           UserToken = UserObj.UserToken;
                                       }
                                       datacontextClient.Connection.Close();
                                   }

                                   lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                   lobjResponse.Message = "Company details updated successfully.";
                                   lobjResponse.TaxDetails = TaxObj;
                               }
                           }

                           lobjResponse.UserToken = UserToken;
                       }
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "AddBrokerCompany", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfBrokerCompanies
        public CListOfBrokerCompaniesResponse ListOfBrokerCompanies(LazyLoadingInputV2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CListOfBrokerCompaniesResponse lobjResponse = new CListOfBrokerCompaniesResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                List<ListOfBrokerCompanies> Result = new List<ListOfBrokerCompanies>();
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        List<ListOfBrokerCompanies> Obj = (from p in mastercontext.TblCompanies.AsEnumerable()
                                                           join q in mastercontext.TblBrokerCompanies on p.CompanyId equals q.CompanyId
                                                           join r in mastercontext.TblBrokers on q.BrokerId equals r.BrokerId
                                                           join x in mastercontext.TblPlanTypes on Convert.ToDecimal(p.PlanTypeId) equals x.PlanTypeId
                                                           join s in mastercontext.TblProvinces on p.Province equals s.ProvinceId
                                                           where p.Active == true && q.Active == true && r.Active == true 
                                                           && r.UserId == iObj.LoggedInUserId 
                                                           select new ListOfBrokerCompanies()
                                                           {
                                                               CompanyId = Convert.ToDecimal(p.CompanyId),
                                                               Name = p.CompanyName,
                                                               Code = p.UniqueCode,
                                                               Email = p.OwnerEmail,
                                                               Phone = p.OwnerContactNo,
                                                               OwnerName = p.FirstName + " " + p.LastName,
                                                               Province = s.Name,
                                                               PlanType = x.Title,
                                                               DbCon = p.DBCon,
                                                               OwnerId = 1,
                                                               CreationDt = Convert.ToString(p.DateCreated),
                                                               IsApproved = Convert.ToString(p.IsApproved),
                                                               CommissionCharges = GetBrokerComissionCharges(p.CompanyId),
                                                               BlockReason = (from a in mastercontext.TblCompBlockReasons
                                                                              where a.Active == true && a.CompanyId == p.CompanyId
                                                                              select new BlockReason()
                                                                              {
                                                                                  Reason = a.Reason,
                                                                                  Date = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                                                  IsBlock = Convert.ToBoolean(a.IsBlock)
                                                                              }).ToList(),
                                                               IsBlocked = Convert.ToBoolean(p.IsBlocked)
                                                           }).ToList();

                        if (Obj != null && Obj.Count > 0)
                        {
                            if (iObj.ChunckStart == -1)
                            {
                                Result = Obj.OrderByDescending(i => i.CompanyId).Take(iObj.ChunckSize).ToList();
                            }
                            else
                            {
                                Result = Obj.OrderByDescending(i => i.CompanyId).Where(i => i.CompanyId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                            }
                        }
                        

                        lobjResponse.ListOfBrokerCompanies = Result;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                    
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfBrokerCompanies", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ApproveCompany
        public CResponse ApproveCompany(IdInputv2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        TblCompany CompObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                        if(CompObj != null)
                        {
                            CompObj.IsApproved = true;
                            mastercontext.SubmitChanges();

                            #region Creata DB
                            #region Create Database
                            string DBName = CompanyNameFlag + CompObj.CompanyId;
                            string DbCon = DBFlag + CompObj.CompanyId;
                            mastercontext.ExecuteCommand("CREATE DATABASE [" + DBName + "]");
                            string contents = "";
                            string DbPatch = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                            contents = File.ReadAllText(DbPatch + "DbPatch.txt");
                            string Patch = contents.Replace("lstrDbName", DBName);
                            mastercontext.ExecuteCommand(Patch.ToString());
                            #endregion  Create Database

                            #region Adding Connectionstring and run procedure path
                            string ConnStrPath = ConStrPath;
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(ConnStrPath);
                            XmlNode xmlnode = xmlDoc.SelectSingleNode("//connectionStrings");
                            xmlnode.InnerXml += "<add name=\"" + DbCon + "\" connectionString=\"Data Source=18.216.133.20;Initial Catalog=" + DBName + ";Integrated Security=false;Max Pool Size=50000;Pooling=True;User ID=maan;Password=Jetking34\" providerName=\"System.Data.SqlClient\" />";
                            xmlDoc.Save(ConnStrPath);
                             
                            XmlNodeList nodes = xmlDoc.SelectNodes("/connectionStrings/add[@name='" + DbCon + "']");
                            string Constring = nodes[0].Attributes["connectionString"].Value.ToString();
                            using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                            {
                                contents = "";
                                string[] filePaths = Directory.GetFiles(DbPatch + "Procedure Patch\\");
                                foreach (var file in filePaths)
                                {
                                    contents = "";
                                    contents = File.ReadAllText(file);
                                    string ProcedurePatch = contents.Replace("lstrDbName", DBName);
                                    datacontextClient.ExecuteCommand(ProcedurePatch.ToString());
                                }

                                #region Create Folders
                                if (!Directory.Exists(ConPath + CompObj.CompanyId))
                                {
                                    Directory.CreateDirectory(ConPath + CompObj.CompanyId);
                                    DirectoryInfo dInfo = new DirectoryInfo(ConPath + CompObj.CompanyId);
                                    DirectorySecurity dSecurity = dInfo.GetAccessControl();
                                    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                                    dInfo.SetAccessControl(dSecurity);


                                }
                                #endregion Create Folders



                                TblCompanyUser UserObj = new TblCompanyUser()
                                {
                                    FirstName = CompObj.FirstName,
                                    LastName = CompObj.LastName,
                                    EmailAddress = CompObj.OwnerEmail,
                                    UserName = CompObj.UserName,
                                    UserTypeId = Convert.ToInt32(CObjects.enmUserType.Owner),
                                    Pwd = CompObj.Pwd,
                                    IsActive = true,
                                    UserToken = UserToken,
                                    ContactNo = CompObj.OwnerContactNo,
                                    DateModified = DateTime.UtcNow,
                                    DateCreated = DateTime.UtcNow,
                                    Active = true
                                };
                                datacontextClient.TblCompanyUsers.InsertOnSubmit(UserObj);
                                datacontextClient.SubmitChanges();

                                CompObj.DBCon = DbCon;
                                CompObj.StagesCovered = "4";
                                mastercontext.SubmitChanges();

                                UpdateMsgUser(UserObj, "ADD", CompObj.CompanyId);

                                #region SendMail

                                
                                StringBuilder builder = new StringBuilder();

                                builder.Append("Hello " + CompObj.CompanyName + ",<br/><br/>");
                                builder.Append("Welcome to eFlex, your very own health spending account set up by your employer," + CompObj.CompanyName + " <br/><br/>");
                                builder.Append("This account provides you with the flexibility to claim medical expenses so you’re not out of pocket. To view how much money you’re eligible to use, click <a href=" + lstrUrl + " >here</a> to complete your online account as well as view your balance.<br/><br/>");
                                builder.Append("For further convenience, we have SMS Messaging.  Here you will receive updates throughout your claims process so your always aware of what’s happening in your account<br/><br/>");
                                builder.Append("Company Code: " + CompObj.UniqueCode + "<br/><br/>");
                                builder.Append("Username: " + UserObj.UserName.Split('_')[1] + "<br/><br/>");
                                builder.Append("Click here for reset your password: <a href=" + lstrAuthorizationUrl + "/" + CompObj.CompanyId + "/" +UserObj.CompanyUserId + " >" + lstrAuthorizationUrl + "/" + CompObj.CompanyId + "/" + UserObj.CompanyUserId + "</a><br/><br/>");
                                           
                                builder.Append("Enjoy your eFlex account,<br/><br/>");
                                builder.Append("The eclipse EIO Team<br/><br/>");


                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                //send mail

                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = CompObj.OwnerEmail.Trim();
                                Mail.Subject = "eFlex Corporate Account Registered";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompObj.CompanyId;
                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                #endregion SendMail

                                CreateResponseLog(datacontextClient, StartTime, CompObj.CompanyId, "AddCompany", InputJson, lobjResponse.Message);
                                datacontextClient.Connection.Close();
                            }

                            #endregion Adding Connectionstring and run procedure path

                            #endregion Creata DB

                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ApproveCompany", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfCompanyBrokers
        public CListOfBrokersResponse ListOfCompanyBrokers(ListOfCompanyBrokersInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CListOfBrokersResponse lobjResponse = new CListOfBrokersResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                List<ListOfBrokers> Result = new List<ListOfBrokers>();
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        List<ListOfBrokers> Obj = (from p in mastercontext.TblBrokers
                                                   join q in mastercontext.TblBrokerCompanies on p.BrokerId equals q.BrokerId
                                                  where p.Active == true && q.Active == true && q.CompanyId == iObj.CompanyId
                                                   select new ListOfBrokers()
                                                   {
                                                       UserId = Convert.ToDecimal(p.UserId),
                                                       BrokerId = p.BrokerId,
                                                       FName = p.FirstName,
                                                       LName = p.LastName,
                                                       Email = p.Email,
                                                       Address = p.Address1,
                                                       City = p.City,
                                                       PostalCode = p.PostalCode,
                                                       Country = p.Country,
                                                       Province = (p.ProvinceId == -1 || p.ProvinceId == null) ? "" : (from a in mastercontext.TblProvinces where a.Active == true && a.ProvinceId == p.ProvinceId select a.Name).Take(1).SingleOrDefault(),
                                                       IsApproved = Convert.ToBoolean(p.IsApproved)
                                                   }).ToList();
                        lobjResponse.ListOfBrokers = Obj;

                        if (Obj != null && Obj.Count > 0)
                        {
                            if (iObj.ChunckStart == -1)
                            {
                                Result = Obj.OrderByDescending(i => i.BrokerId).Take(iObj.ChunckSize).ToList();
                            }
                            else
                            {
                                Result = Obj.OrderByDescending(i => i.BrokerId).Where(i => i.BrokerId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                            }
                        }


                        lobjResponse.ListOfBrokers = Result;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfCompanyBrokers", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfChildBrokers
        public CListOfBrokersResponse ListOfChildBrokers(LazyLoadingInputV2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CListOfBrokersResponse lobjResponse = new CListOfBrokersResponse();
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                List<ListOfBrokers> Result = new List<ListOfBrokers>();
                
                    try
                    {
                         List<ListOfBrokers> Obj = (from p in mastercontext.TblBrokers
                                                  where p.Active == true && p.MasterUserId == iObj.LoggedInUserId
                                                   select new ListOfBrokers()
                                                   {
                                                       UserId = Convert.ToDecimal(p.UserId),
                                                       BrokerId = p.BrokerId,
                                                       FName = p.FirstName,
                                                       LName = p.LastName,
                                                       Email = p.Email,
                                                       Address = p.Address1,
                                                       City = p.City,
                                                       PostalCode = p.PostalCode,
                                                       Country = p.Country,
                                                       ContactNo = p.MobileNo,
                                                       Province = (p.ProvinceId == -1 || p.ProvinceId == null) ? "" : (from a in mastercontext.TblProvinces where a.Active == true && a.ProvinceId == p.ProvinceId select a.Name).Take(1).SingleOrDefault(),
                                                       IsApproved = Convert.ToBoolean(p.IsApproved)
                                                   }).ToList();
                        lobjResponse.ListOfBrokers = Obj;

                        if (Obj != null && Obj.Count > 0)
                        {
                            if (iObj.ChunckStart == -1)
                            {
                                Result = Obj.OrderByDescending(i => i.BrokerId).Take(iObj.ChunckSize).ToList();
                            }
                            else
                            {
                                Result = Obj.OrderByDescending(i => i.BrokerId).Where(i => i.BrokerId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                            }
                        }


                        lobjResponse.ListOfBrokers = Result;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                
                    CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfChildBrokers", InputJson, lobjResponse.Message);
                    mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region EditEflexUserPAPDetails
        public CResponse EditEflexUserPAPDetails(EditEflexUserPAPDetailsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(datacontext))
                    {
                        if (iObj.UserPAPDetailId == -1)
                        {
                            TblUserPAPDetail PAPObj = new TblUserPAPDetail()
                            {
                                BankName = iObj.BankName,
                                TransitId = iObj.TransitId,
                                InstitutionId = iObj.InstitutionId,
                                AccountNumber = iObj.AccountNumber,
                                DateModified = DateTime.UtcNow,
                                UserId = iObj.UserId,
                                DateCreated = DateTime.UtcNow,
                                Active =true
                            };
                            datacontext.TblUserPAPDetails.InsertOnSubmit(PAPObj);
                            datacontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        }
                        else
                        {
                            TblUserPAPDetail Obj = (from p in datacontext.TblUserPAPDetails where p.Active == true && p.PAPDetailId == iObj.UserPAPDetailId select p).Take(1).SingleOrDefault();
                            if (Obj != null)
                            {
                                Obj.BankName = iObj.BankName;
                                Obj.TransitId = iObj.TransitId;
                                Obj.InstitutionId = iObj.InstitutionId;
                                Obj.AccountNumber = iObj.AccountNumber;
                                Obj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                        }
                       
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(datacontext, StartTime, UserId, "EditEflexUserPAPDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdatePAPDetails

        #region UpdateBrokerCommissionCharges
        public CResponse UpdateBrokerCommissionCharges(UpdateCommissionInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.UserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                   
                    TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();

                    if (BrokerObj != null)
                    {
                        BrokerObj.CommissionCharges = iObj.CommissionCharges;
                        BrokerObj.DateModified = DateTime.UtcNow;
                    }
                    mastercontext.SubmitChanges();
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                           
                       
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "UpdateBrokerCommissionCharges", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetCompanyDetails
        public CGetCompanyDetailsResponse GetBrokerCompanyDetails(GetCompanyDetailsInput iObj)
        {
            CGetCompanyDetailsResponse lobjResponse = new CGetCompanyDetailsResponse();
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            try
            {
                UserId = iObj.LoggedInUserId;
                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();

                        if (CompanyObj != null)
                        {
                            string Constring = GetConnection(CompanyObj.DBCon);
                            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                            {

                                #region BasicDetails

                                CompanyBasicaData CompanyDetailObj = (from p in mastercontext.TblCompanies
                                                                      where p.Active == true && p.CompanyId == iObj.Id
                                                                      select new CompanyBasicaData()
                                                                      {
                                                                          CompanyName = p.CompanyName,
                                                                          FirstName = p.FirstName,
                                                                          LastName = p.LastName,
                                                                          Address1 = p.Address1,
                                                                          Address2 = p.Address2,
                                                                          City = p.City,
                                                                          ProvinceId = Convert.ToDecimal(p.Province),
                                                                          PostalCode = p.PostalCode,
                                                                          Country = p.Country,
                                                                          OwnerContactNo = p.OwnerContactNo,
                                                                          OwnerEmail = p.OwnerEmail,
                                                                          ServiceUrl = p.ServiceUrl,
                                                                          HostUrl = p.HostUrl,
                                                                          Code = p.UniqueCode,
                                                                          UserName = p.UserName,
                                                                          Password = p.Pwd,
                                                                          IsCorporation = p.IsCorporation == null ? "" : Convert.ToString(p.IsCorporation),
                                                                          PlanTypeId = p.PlanTypeId == null ? -1 : Convert.ToDecimal(p.PlanTypeId),
                                                                          StagesCovered = p.StagesCovered,
                                                                          DbCon = p.DBCon
                                                                      }).Take(1).SingleOrDefault();
                                #endregion BasicDetails

                                #region Provincial Tax Details
                                TaxDetails TaxObj = (from p in mastercontext.TblProvinces
                                                     where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                     select new TaxDetails()
                                                     {
                                                         AdminMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                         AdminMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                         BrokerMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                         BrokerMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                         HST = Convert.ToDecimal(p.HST),
                                                         RSTax = Convert.ToDecimal(p.RST),
                                                         GST = Convert.ToDecimal(p.GST),
                                                         PPTax = Convert.ToDecimal(p.PPT),
                                                     }).Take(1).SingleOrDefault();

                                #endregion Provincial Tax Details

                                if (CompanyDetailObj != null)
                                {

                                    #region Company plan details

                                    List<GetPlanDetails> PlanList = (from p in datacontext.TblCompanyPlans
                                                                     where p.Active == true
                                                                     select new GetPlanDetails()
                                                                     {
                                                                         PlanId = p.CompanyPlanId,
                                                                         PlanStartDt = Convert.ToString(p.PlanStartDt),
                                                                         Name = p.Name,
                                                                         ContributionSchedule = Convert.ToInt32(p.ContributionSchedule),
                                                                         RecurringDeposit = Convert.ToDecimal(p.RecurringDeposit),
                                                                         InitialDeposit = Convert.ToDecimal(p.InitialDeposit),
                                                                         NetDeposit = Convert.ToDecimal(p.NetDeposit),
                                                                         AdministrationInitialFee = Convert.ToDecimal(p.AdministrationInitialFee),
                                                                         AdministrationMonthlyFee = Convert.ToDecimal(p.AdministrationMonthlyFee),
                                                                         HSTInitial = Convert.ToDecimal(p.HSTInitial),
                                                                         HSTMonthly = Convert.ToDecimal(p.HSTMonthly),
                                                                         PremiumTaxInitial = Convert.ToDecimal(p.PremiumTaxInitial),
                                                                         PremiumTaxMonthly = Convert.ToDecimal(p.PremiumTaxMonthly),
                                                                         RetailSalesTaxInitial = Convert.ToDecimal(p.RetailSalesTaxInitial),
                                                                         RetailSalesTaxMonthly = Convert.ToDecimal(p.RetailSalesTaxMonthly),
                                                                         GrossTotalInitial = Convert.ToDecimal(p.GrossTotalInitial),
                                                                         GrossTotalMonthly = Convert.ToDecimal(p.GrossTotalMonthly),
                                                                         PlanDesign = Convert.ToDecimal(p.PlanDesign),
                                                                         RollOver = Convert.ToDecimal(p.RollOver),
                                                                         CarryForwardPeriod = p.CarryForwardPeriod,
                                                                         Termination = Convert.ToDecimal(p.Termination),

                                                                     }).ToList();
                                    #endregion Company plan details

                                    #region Company Employees details
                                    List<EmployeesData> EmployeesList = (from p in datacontext.TblCompanyUsers
                                                                         join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                                         where p.Active == true && q.Active == true && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee)
                                                                         select new EmployeesData()
                                                                         {
                                                                             CompanyUserId = p.CompanyUserId,
                                                                             FName = p.FirstName,
                                                                             LName = p.LastName,
                                                                             Email = p.EmailAddress,
                                                                             CompanyPlanId = Convert.ToDecimal(q.CompanyPlanId),
                                                                             UserTypeId = Convert.ToDecimal(p.UserTypeId)
                                                                         }).ToList();

                                    #endregion Company Employees details

                                    #region Company Admin details
                                    List<AdminsData> AdminsList = (from p in datacontext.TblCompanyUsers
                                                                   where p.Active == true
                                                                   && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Admin)
                                                                   select new AdminsData()
                                                                   {
                                                                       CompanyUserId = p.CompanyUserId,
                                                                       FName = p.FirstName,
                                                                       LName = p.LastName,
                                                                       Email = p.EmailAddress,
                                                                       UserTypeId = Convert.ToDecimal(p.UserTypeId)
                                                                   }).ToList();

                                    #endregion Company Admin details

                                    #region Payment Overview
                                    PaymentOverview PaymentOverviewObj = new PaymentOverview();
                                    List<TblCompanyPlan> Planlist = (from p in datacontext.TblCompanyPlans where p.Active == true select p).ToList();
                                    if (Planlist != null && Planlist.Count() > 0)
                                    {
                                        decimal TotalInitialOfAllMembers = 0;
                                        decimal TotalRecurringOfAllMembers = 0;
                                        foreach (TblCompanyPlan item in Planlist)
                                        {
                                            decimal MemberCount = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p.EmployeeId).Count();

                                            #region Initial amount
                                            decimal TotalInitial = 0;
                                            decimal PlanGrossInitialAmt = (from p in datacontext.TblCompanyPlans
                                                                           where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                           select Convert.ToDecimal(p.GrossTotalInitial ?? 0)).Sum();
                                            decimal PlanInitialAmt = (from p in datacontext.TblCompanyPlans
                                                                      where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                      select Convert.ToDecimal(p.InitialDeposit ?? 0)).Sum();

                                            TotalInitial = PlanGrossInitialAmt + PlanInitialAmt;
                                            #endregion Initial amount

                                            #region Recurring amount
                                            decimal TotalRecurringAmt = 0;

                                            decimal PlanGrossRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                                             where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                             select Convert.ToDecimal(p.GrossTotalMonthly ?? 0)).Sum();
                                            decimal PlanRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                                        where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                        select Convert.ToDecimal(p.RecurringDeposit ?? 0)).Sum();

                                            TotalRecurringAmt = PlanGrossRecurringAmt + PlanRecurringAmt;


                                            #endregion Recurring amount



                                            if (MemberCount > 0)
                                            {
                                                TotalInitialOfAllMembers = TotalInitialOfAllMembers + (MemberCount * (TotalInitial));
                                                TotalRecurringOfAllMembers = TotalRecurringOfAllMembers + (MemberCount * (TotalRecurringAmt));
                                            }
                                            
                                        }

                                        PaymentOverviewObj = new PaymentOverview()
                                        {
                                            InitialPayment = TotalInitialOfAllMembers,
                                            RecurringPayment = TotalRecurringOfAllMembers,
                                        };
                                    }
                                    #endregion Payment Overview

                                    #region CompanyReferrals
                                    CompanyReferral ReferralObj = (from p in datacontext.TblCompanyReferrals
                                                                   join q in datacontext.TblReferralTypes on p.ReferralTypeId equals q.ReferralTypeId
                                                                   where p.Active == true && p.CompanyId == iObj.Id && q.Active == true
                                                                   select new CompanyReferral()
                                                                   {
                                                                       CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                                       ReferralTypeId = Convert.ToDecimal(p.ReferralTypeId),
                                                                       ReferralTypeName = q.Title,
                                                                       Name = p.Name,
                                                                       OtherDetails = p.OtherDetails,
                                                                       BrokerId = Convert.ToDecimal(p.BrokerId)
                                                                   }).Take(1).SingleOrDefault();
                                    #endregion CompanyReferrals

                                    #region MyRegion

                                    TblBrokerCompanyFee BrokerFeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                        where p.Active == true && p.CompanyId == iObj.Id
                                                                        select p).Take(1).SingleOrDefault();
                                    if (BrokerFeeObj != null)
                                    {
                                        CompanyBrokerFee CompBrokerObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                          where p.Active == true && p.CompanyId == iObj.Id
                                                                          select new CompanyBrokerFee()
                                                                          {
                                                                              CompAdminFee = Convert.ToDecimal(p.AdminFee),
                                                                              CompBrokerFee = Convert.ToDecimal(p.BrokerFee)
                                                                          }).Take(1).SingleOrDefault();
                                    }
                                    #endregion

                                    lobjResponse.PaymentOverview = PaymentOverviewObj;
                                    lobjResponse.PlanDetails = PlanList;
                                    lobjResponse.EmployeesData = EmployeesList;
                                    lobjResponse.AdminsData = AdminsList;
                                    lobjResponse.CompanyReferral = ReferralObj;
                                }
                                lobjResponse.TaxDetails = TaxObj;
                                lobjResponse.CompanyBasicaData = CompanyDetailObj;
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                                CreateResponseLog(datacontext, StartTime, UserId, "GetBrokerCompanyDetails", InputJson, lobjResponse.Message);
                                datacontext.Connection.Close();
                            }
                        }//end compobj
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }

                    mastercontext.Connection.Close();
                }
                           
            }
            catch (Exception ex)
            {

                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }
        #endregion

        #region GetBrokerDashboardData
        public CGetBrokerDashboardDataResponse GetBrokerDashboardData(GetBrokerDashboardDataInput iObj)
        {
            CGetBrokerDashboardDataResponse lobjResponse = new CGetBrokerDashboardDataResponse();
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            try
            {
                UserId = iObj.UserId;
                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                     
                        BrokerFiguresData FiguresObj = (from p in mastercontext.sp_GetBrokerFigures(iObj.UserId)
                                                        select new BrokerFiguresData()
                                                       {
                                                           TotalBroker = (from a in mastercontext.TblBrokers
                                                                          where a.Active == true && a.MasterUserId == iObj.UserId
                                                                          select a).Count(),
                                                           TotalCompany = p.TotalCompanies == null ? 0 : Convert.ToInt32(p.TotalCompanies),
                                                           TotalAmtReceived = p.TotalAmtReceived == null ? 0 : Convert.ToInt32(p.TotalAmtReceived),
                                                          }).Take(1).SingleOrDefault();


                        
                        List<RecentRegCompany> RecentComp = (from p in mastercontext.TblBrokerPayments
                                                                   where p.Active == true 
                                                                   && p.UserId == iObj.UserId
                                                                   orderby p.PaymentDate descending
                                                                   select new RecentRegCompany()
                                                                   {
                                                                     CompanyName =(from q in mastercontext.TblCompanies where q.Active == true && q.CompanyId == p.CompanyId select q.CompanyName).Take(1).SingleOrDefault(),
                                                                     Contribution = Convert.ToDecimal(p.Contribution),
                                                                     Month = Convert.ToString(p.PaymentDueDate),
                                                                     YourFee = Convert.ToDecimal(p.PercentFee),
                                                                     TotalComission = Convert.ToDecimal(p.Amount)
                                                             }).ToList();

                        Int32 Count = 0;
                        List<ChildBrokersDetails> BrokerDetails = (from p in mastercontext.TblBrokers
                                                                  where p.Active == true 
                                                                   && p.MasterUserId == iObj.UserId orderby p.BrokerId descending
                                                                   select new ChildBrokersDetails()
                                                                     {
                                                                       BrokerName = p.FirstName + " " + p.LastName,
                                                                       NoOfCompanies = (from a in mastercontext.TblBrokerCompanies
                                                                                             where a.Active == true && a.BrokerId == p.BrokerId 
                                                                                             select Convert.ToDecimal(a.BrokerCompanyId)).Count(),
                                                                       TotalComission = GetTotalCommission(p.UserId)
                                                                    }).Take(10).ToList();


                        
                            List<string> AllMonths = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames.ToList();
                            List<decimal> ComissionAmtReceived = new List<decimal>();
                            List<decimal> TotalCompanyCount = new List<decimal>();
                            List<string> Date = new List<string>();
                            List<string> Months = new List<string>();
                            Int32 TotalMonths = 12;
                            Int32 CurrentMonth = DateTime.UtcNow.Month;
                            GetBrokerGraphData BrokerGraphData = new GetBrokerGraphData();
                            List<EmpDashboardDataForAdmin> Result = new List<EmpDashboardDataForAdmin>();
                            for (int i = 0; i < CurrentMonth; i++)
                            {
                                int month = i + 1;
                                DateTime StartDate = new DateTime(DateTime.Now.Year, month, 1);
                                DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);

                                List<TblBrokerPayment> PaymentObj = (from p in mastercontext.TblBrokerPayments
                                                                     where p.Active == true && p.UserId == iObj.UserId &&
                                                                     p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                                     select p).ToList();

                                List<TblBrokerCompany> CompanyObj = (from p in mastercontext.TblBrokerCompanies
                                                                     join q in mastercontext.TblBrokers on p.BrokerId equals q.BrokerId
                                                                     where p.Active == true && q.UserId == iObj.UserId &&
                                                                     p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                                     select p).ToList();
                                decimal ComissionAmountReceived = 0;
                                if (PaymentObj != null && PaymentObj.Count() > 0)
                                {
                                    ComissionAmountReceived = PaymentObj.Select(x => Convert.ToDecimal(x.Amount)).Sum();
                                    ComissionAmtReceived.Add(ComissionAmountReceived);
                                }
                                else
                                {
                                    ComissionAmtReceived.Add(ComissionAmountReceived);
                                }
                                string MonthName = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(month);
                                Months.Add(MonthName);

                                decimal CompanyCount = 0;

                                if (CompanyObj != null && CompanyObj.Count() > 0)
                                {
                                    CompanyCount = CompanyObj.Count();
                                    TotalCompanyCount.Add(CompanyCount);
                                }
                                else
                                {
                                    TotalCompanyCount.Add(CompanyCount);
                                }

                               
                               
                            }

                            BrokerGraphData = new GetBrokerGraphData()
                            {
                                Months = Months,
                                NoOfCompanies = TotalCompanyCount,
                                ComissionAmt = ComissionAmtReceived,
                            };
                              
                            

                        lobjResponse.GetBrokerGraphDetails = BrokerGraphData;
                        lobjResponse.BrokerFiguresData = FiguresObj;
                        lobjResponse.ChildBrokersDetails = BrokerDetails;
                        lobjResponse.RecentRegCompany = RecentComp;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                    lobjResponse.IsLogOut = IsLogOut;
                    CreateMasterResponseLog(mastercontext, StartTime, UserId, "GetBrokerDashboardData", InputJson,lobjResponse.Message);
                    mastercontext.Connection.Close();       
                }

            }
            catch (Exception ex)
            {

                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }

            return lobjResponse;
        }

        
        #endregion

        #region GetAllBrokerTransactions
        public CGetAllBrokerTransactionsResponse GetAllBrokerTransactions(LazyLoadingInputV2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            CGetAllBrokerTransactionsResponse lobjResponse = new CGetAllBrokerTransactionsResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    List<GetAllBrokerTransactions> Result = new List<GetAllBrokerTransactions>();
                   
                    List<GetAllBrokerTransactions> lstNotes = (from p in datacontext.TblBrokerPayments
                                                               where p.Active == true && p.UserId == iObj.LoggedInUserId
                                                                 orderby p.DateCreated descending
                                                                 select new GetAllBrokerTransactions
                                                                 {
                                                                     RowId = p.BrokerPaymentId,
                                                                     Amount = Convert.ToDecimal(p.Amount),
                                                                     Description = p.PaymentDetails,
                                                                     TransactionDt = Convert.ToString(p.PaymentDate),
                                                                     PaymentStatus = Convert.ToDecimal(p.PaymentStatus),
                                                                     PaymentType = p.PaymentTypeId
                                                                  }).ToList();
                    if (lstNotes != null && lstNotes.Count > 0)
                    {
                        if (iObj.ChunckStart == -1)
                        {
                            Result = lstNotes.OrderByDescending(i => i.RowId).Take(iObj.ChunckSize).ToList();
                        }
                        else
                        {
                            Result = lstNotes.OrderByDescending(i => i.RowId).Where(i => i.RowId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                        }
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.GetAllBrokerTransactions = lstNotes;
                }
                catch (Exception ex)
                {
                    
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }

                CreateMasterResponseLog(datacontext, StartTime, UserId, "GetAllBrokerTransactions", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetAllBrokerTransactions

        #region AddBrokerAdjustments
        public CResponse AddBrokerAdjustments(AddBrokerAdjustmentsInput iObj)
        {

            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                        decimal Amount = 0;
                        TblBrokerAdjustment AdjObj = new TblBrokerAdjustment()
                        {
                            UserId = iObj.UserId,
                            Amount = iObj.Amount,
                            AdjustmentDT = Convert.ToDateTime(iObj.AdjustmentDT),
                            Remarks = iObj.Remarks,
                            TypeId = Convert.ToInt16(iObj.TypeId),
                            Active = true,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow
                        };
                        mastercontext.TblBrokerAdjustments.InsertOnSubmit(AdjObj);
                        if (iObj.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit))
                        {
                            Amount = (iObj.Amount);
                        }
                        else
                        {
                            Amount = -(iObj.Amount);
                        }

                        TblBrokerPayment TransactionObj = new TblBrokerPayment()
                        {
                            PaymentDate = DateTime.UtcNow,
                            PaymentDueDate = DateTime.UtcNow,
                            PaymentDetails = iObj.Remarks,
                            Amount = Amount,
                            UserId = iObj.UserId,
                            PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                            Active = true,
                            DateCreated = DateTime.UtcNow
                        };
                        mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj);

                      
                        #region Notify code
                        string NotifyText = "";
                        if (iObj.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit))
                        {
                           
                            NotifyText = "$" + Amount + " was deposited into your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ". For details please login to your account.";
                        }
                        else
                        {
                           NotifyText = "$" + Amount + " was withdrawn from your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ". For details, please login to your account.";
                        }

                        TblEflexNotifyLog NotifyObj = new TblEflexNotifyLog()
                        {
                            NotifyText = NotifyText,
                            NotifyDate = DateTime.UtcNow,
                            ForUserId = Convert.ToString(iObj.UserId),
                            IsRead = false,
                            DateCreated = DateTime.UtcNow,
                            Active = true
                        };
                        mastercontext.TblEflexNotifyLogs.InsertOnSubmit(NotifyObj);

                       
                        #endregion

                        mastercontext.SubmitChanges();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                   
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "AddBrokerAdjustments", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }



            return lobjResponse;
        }
        #endregion

        #region ListBrokerDuePayment
         public CListBrokerPaymentResponse ListBrokerDuePayment(BrokerPaymentInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            InputJson = new JavaScriptSerializer().Serialize(iObj);
            CListBrokerPaymentResponse lobjResponse = new CListBrokerPaymentResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    DateTime? StartDt = null;
                    DateTime? EndDt = null;
                 
                    if(iObj.StartDate != "")
                    {
                        StartDt = Convert.ToDateTime(iObj.StartDate);
                    } 
                    if (iObj.EndDate != "")
                    {
                        EndDt = Convert.ToDateTime(iObj.EndDate);
                    }
                    if (IsValidateEflexUser(datacontext))
                    {
                     
                        List<ListBrokerPayment> Obj = (from p in datacontext.sp_ListBrokerDuePayments(iObj.ChunckStart, iObj.ChunckSize, iObj.SearchString,StartDt,EndDt)
                                                       select new ListBrokerPayment()
                                                        {
                                                            RowId = Convert.ToDecimal(p.RowId),
                                                            BrokerPaymentId = p.BrokerPaymentId,
                                                            CompanyName = p.CompanyName,
                                                            PaymentDate = Convert.ToString(p.PaymentDate),
                                                            Amount = Convert.ToDecimal(p.Amount),
                                                            PaymentDetails = p.PaymentDetails,
                                                            BrokerName = p.BrokerName,
                                                            Contribution = Convert.ToDecimal(p.Contribution),
                                                            PercentFee = Convert.ToDecimal(p.PercentFee),
                                                            UserId = Convert.ToDecimal(p.UserId),
                                                            GetPAPDetails = GetUserBankDetails(datacontext, p.UserId),
                                                        }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.PaymentData = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                    lobjResponse.IsLogOut = IsLogOut;
                    CreateMasterResponseLog(datacontext, CurrentTime, -1, "ListBrokerDuePayment", InputJson, lobjResponse.Message);
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListBrokerReceivedPayments
         public CListBrokerPaymentResponse ListBrokerReceivedPayments(BrokerPaymentInput iObj)
         {
             string InputJson = "";
             DateTime CurrentTime = DateTime.UtcNow;
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListBrokerPaymentResponse lobjResponse = new CListBrokerPaymentResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {

                     if (IsValidateEflexUser(datacontext))
                     {
                         DateTime? StartDt = null;
                         DateTime? EndDt = null;

                         if (iObj.StartDate != "")
                         {
                             StartDt = Convert.ToDateTime(iObj.StartDate);
                         }
                         if (iObj.EndDate != "")
                         {
                             EndDt = Convert.ToDateTime(iObj.EndDate);
                         }
                         List<ListBrokerPayment> Obj = (from p in datacontext.sp_ListBrokerReceivedPayments(iObj.ChunckStart, iObj.ChunckSize, iObj.SearchString,StartDt,EndDt)
                                                           select new ListBrokerPayment()
                                                           {
                                                               RowId = Convert.ToDecimal(p.RowId),
                                                               BrokerPaymentId = p.BrokerPaymentId,
                                                               CompanyName = p.CompanyName,
                                                               PaymentDate = Convert.ToString(p.PaymentDate),
                                                               Amount = Convert.ToDecimal(p.Amount),
                                                               PaymentDetails = p.PaymentDetails,
                                                               BrokerName = p.BrokerName,
                                                               Contribution = Convert.ToDecimal(p.Contribution),
                                                               PercentFee = Convert.ToDecimal(p.PercentFee),
                                                               PaymentDueDate = Convert.ToString(p.PaymentDueDate),
                                                           }).ToList();

                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         lobjResponse.PaymentData = Obj;
                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                        
                     }
                     lobjResponse.IsLogOut = IsLogOut;
                     CreateMasterResponseLog(datacontext, CurrentTime, -1, "ListBrokerDuePayment", InputJson, lobjResponse.Message);
                 }
                 catch (Exception ex)
                 {

                     InputJson = new JavaScriptSerializer().Serialize(iObj);
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.ToString();
                 }
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region ForgotBrokerPassword
         //JK:270518
         public CResponse ForgotBrokerPassword(ForgetPasswordInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             decimal UserId = -1;
             CResponse lobjResponse = new CResponse();
           
             using (MasterDBDataContext dc = new MasterDBDataContext())
             {
                     try
                     {
                         TblUser UserObj = (from p in dc.TblUsers where p.Active == true && p.UserName == iObj.UserName && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker) select p).FirstOrDefault();
                         if (UserObj != null)
                         {
                             UserId = Convert.ToDecimal(UserObj.UserId);
                             StringBuilder builder = new StringBuilder();
                             string lstrMessage = "";

                             builder.Append("Dear " + UserObj.FirstName + ",<br/><br/>");
                             builder.Append("Following are your login credentials:<br/><br/>");
                             builder.Append("Username: " + UserObj.UserName + "<br/><br/>");
                             builder.Append("Password: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                             builder.Append("Team<br/><br/>");
                             builder.Append("eFlex<br/><br/>");


                             lstrMessage = builder.ToString();

                             string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                             string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                             string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                             Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                             //Send Mail
                             CMail Mail = new CMail();
                             Mail.EmailTo = UserObj.EmailAddress;
                             Mail.Subject = "eFlex : Retrieve Password";
                             Mail.MessageBody = lstrMessage;
                             Mail.GodaddyUserName = GodaddyUserName;
                             Mail.GodaddyPassword = GodaddyPwd;
                             Mail.Server = Server;
                             Mail.Port = Port;
                             bool IsSent = Mail.SendEMail(dc, true);

                             if (IsSent)
                             {
                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                             }
                             else
                             {
                                 lobjResponse.Message = "Currently server is facing some problem sending email. Please try after some time.";
                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             }

                         }
                         else
                         {
                             lobjResponse.Message = "This broker doesn't exist.";
                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                         }
                     }
                     catch (Exception ex)
                     {
                         InputJson = new JavaScriptSerializer().Serialize(iObj);
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message = ex.Message;
                     }

                     CreateMasterResponseLog(dc, StartTime, -1, "ForgotBrokerPassword", InputJson, lobjResponse.Message);
                     dc.Connection.Close();
                
             }
             return lobjResponse;
         }
         #endregion ForgotPassword

        #region RemitBrokerPayment
         public CResponse RemitBrokerPayment(IdInputv2 iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     TblBrokerPayment PaymentObj = (from p in mastercontext.TblBrokerPayments where p.Active == true && p.BrokerPaymentId == iObj.Id  select p).Take(1).SingleOrDefault();
                     if (PaymentObj != null)
                     {
                         PaymentObj.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                         PaymentObj.PaymentDate = DateTime.UtcNow;
                         mastercontext.SubmitChanges();
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                         #region Notify code
                         string NotifyText = "";

                        NotifyText = "$" + PaymentObj.Amount + " has been deposited into your eFlex Spending Account on " + Convert.ToDateTime(PaymentObj.PaymentDate).ToString("MM/dd/yyyy") + ". For details please login to your account.";
                        

                         TblEflexNotifyLog NotifyObj = new TblEflexNotifyLog()
                         {
                             NotifyText = NotifyText,
                             NotifyDate = DateTime.UtcNow,
                             ForUserId = Convert.ToString(PaymentObj.UserId),
                             IsRead = false,
                             DateCreated = DateTime.UtcNow,
                             Active = true
                         };
                         mastercontext.TblEflexNotifyLogs.InsertOnSubmit(NotifyObj);
                         mastercontext.SubmitChanges();

                         #endregion

                         #region SendMail
                         string EmailTo = "";
                         string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                         string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                         string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                         Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                         TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.UserId == PaymentObj.UserId select p).Take(1).SingleOrDefault();


                         if (!string.IsNullOrEmpty(BrokerObj.Email))
                         {
                             
                             StringBuilder builder = new StringBuilder();
                             builder.Append("Hi " + BrokerObj.FirstName + " " + BrokerObj.LastName + ",<br/><br/>");
                             builder.Append(NotifyText+"<br/><br/>");
                             builder.Append("If you have any questions, please email eflex@eclipseeio.com<br/><br/>");
                             builder.Append("Thank you,<br/><br/>");
                             builder.Append("The eFlex Team<br/><br/>");

                             
                             //send mail
                             string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                             lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());


                             CMail Mail = new CMail();
                             Mail.EmailTo = BrokerObj.Email;
                             Mail.Subject = "Amount Credited";
                             Mail.MessageBody = lstrMessage;
                             Mail.GodaddyUserName = GodaddyUserName;
                             Mail.GodaddyPassword = GodaddyPwd;
                             Mail.Server = Server;
                             Mail.Port = Port;
                             Mail.CompanyId = -1;
                             bool IsSent = Mail.SendEMail(mastercontext, true);

                         }

                         #endregion SendMail
                     }
                     else
                     {
                         lobjResponse.Message = "No record found.";

                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                   
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, UserId, "RemitBrokerPayment", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus 

        #region ResentBrokerCredentials
         public CResponse ResentBrokerCredentials(ResentBrokerCredentialsInput iObj)
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {

                     TblUser UserObj = (from p in datacontext.TblUsers where p.Active == true && p.UserId == iObj.Id select p).Take(1).SingleOrDefault();
                     if (UserObj != null)
                     {
                         UserObj.EmailAddress = iObj.EmailId;
                         datacontext.SubmitChanges();

                         TblBroker BrokerObj = (from p in datacontext.TblBrokers where p.Active == true && p.UserId == iObj.Id select p).Take(1).SingleOrDefault();

                         #region SendMail

                         string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                         string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                         string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                         Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                         CMail Mail = new CMail();
                     
                         StringBuilder builder = new StringBuilder();
                         builder.Append("Hello " + UserObj.FirstName + ",<br/><br/>");
                         builder.Append("Welcome to eFlex. Please use the following credentials for login to Eflex Portal:<br/><br/>");
                         
                         builder.Append("Username: " + UserObj.UserName + "<br/><br/>");
                         builder.Append("Password: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                         builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                      
                         string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                         lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                         Mail.EmailTo = iObj.EmailId.Trim();
                         Mail.Subject = "Welcome to eFlex! Your Account Is Ready";
                         Mail.MessageBody = lstrMessage;
                         Mail.GodaddyUserName = GodaddyUserName;
                         Mail.GodaddyPassword = GodaddyPwd;
                         Mail.Server = Server;
                         Mail.Port = Port;
                         Mail.CompanyId = iObj.Id;
                         bool IsSent = Mail.SendEMail(datacontext, true);


                         #endregion SendMail
                     }

                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 datacontext.Connection.Close();
             }
            
             return lobjResponse;
         }
         #endregion UpdatePAPDetails

        #region SaveBrokerReferral
         public CResponse SaveBrokerReferral(SaveBrokerReferralInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             bool IsLogOut = false;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 try
                 {
                  
                         TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == Convert.ToDecimal(iObj.UserId) select p).Take(1).SingleOrDefault();
                         if (UserObj != null)
                         {

                             TblBrokerReferral BrokerferralObj = new TblBrokerReferral()
                                 {
                                     UserId	= Convert.ToDecimal(iObj.UserId),			
                                     CompanyName = iObj.CompanyName,			
                                     CompanyWebsite	= iObj.CompanyWebsite,	
                                     Industry =iObj.Industry,			
                                     NoOfEmployees = iObj.NoOfEmployees,	    
                                     HaveGroupBenefits = Convert.ToBoolean(iObj.HaveGroupBenefits),	
                                     ContactName = iObj.ContactName,			
                                     ContactTitle = iObj.ContactTitle,		
                                     EmailAddress = iObj.EmailAddress,	
                                     Phone = iObj.Phone,			
                                     DocFile = iObj.ReferralFile,				
                                     IsFinal = false,				
                                     Active = true,		
		                             Status = Convert.ToInt32(CObjects.enumBrokerReferralStatus.Pending),
                                     DateCreated= DateTime.UtcNow,			
                                     DateModified = DateTime.UtcNow		
                                 };
                                 mastercontext.TblBrokerReferrals.InsertOnSubmit(BrokerferralObj);
                                 mastercontext.SubmitChanges();

                               

                                 #region SendMail

                                 
                                 StringBuilder builder = new StringBuilder();
                                 builder.Append("Hello " + iObj.ContactName + ",<br/><br/>");
                                 builder.Append("Thanks for reffer the company details. Please go through the link to update the status of the referral:<br/><br/>");
                                 builder.Append("Click here: <a href=" + lstrBrokerReferralUrl + BrokerferralObj.BrokerReferralId + " >" + lstrBrokerReferralUrl + BrokerferralObj.BrokerReferralId + "</a><br/><br/>");
                                 builder.Append("Regards,<br/><br/>");
                                 builder.Append("eFlex Team<br/><br/>");

                                 string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                                 lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                 //send mail

                                 string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                 string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                 string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                 Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                 CMail Mail = new CMail();
                                 Mail.EmailTo = iObj.EmailAddress;
                                 Mail.Subject = "Confirm your Availability";
                                 Mail.MessageBody = lstrMessage;
                                 Mail.GodaddyUserName = GodaddyUserName;
                                 Mail.GodaddyPassword = GodaddyPwd;
                                 Mail.Server = Server;
                                 Mail.Port = Port;
                                 Mail.CompanyId = -1;
                                 bool IsSent = Mail.SendEMail(mastercontext, true);


                                 #endregion SendMail

                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                 lobjResponse.ID = BrokerferralObj.BrokerReferralId;
                            
                         }
                         else
                         {
                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             lobjResponse.Message = "Broker doesn't exists.";

                         }
                    
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, iObj.UserId, "SaveBrokerReferral", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region SavePreferredSlots
         public CResponse SavePreferredSlots(SavePreferredSlotsInput iObj)
         {

             DateTime StartTime = DateTime.UtcNow;
             bool IsLogOut = false;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 try
                 {
                        if(iObj.PreferredSlots.Count() > 0 && iObj.PreferredSlots != null)
                        {
                            List<TblPreferredSlot> NewPreferedSlots = new List<TblPreferredSlot>();
                            foreach (PreferredSlots item in iObj.PreferredSlots)
                            {
                                TblPreferredSlot Obj = new TblPreferredSlot()
                                {
                                    BrokerReferralId = iObj.BrokerReferralId,
                                    PreferredDate = Convert.ToDateTime(item.PreferredDateTime),
                                    Location = item.Location,
                                    Attendents = item.Attendents,
                                    Notes = item.Notes,
                                    IsFinal = false,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow


                                };
                                NewPreferedSlots.Add(Obj);

                            }

                            mastercontext.TblPreferredSlots.InsertAllOnSubmit(NewPreferedSlots);
                            mastercontext.SubmitChanges();
                        }
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                  

                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, -1, "SavePreferredSlots", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
        #endregion

        #region UpdatePreferredSlots
         public CResponse UpdatePreferredSlots(UpdatePreferredSlotsInput iObj)
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext= new MasterDBDataContext())
             {
                 try
                 {
                     if (iObj.PreferredSlotId == -1)
                     {
                         TblPreferredSlot Obj = new TblPreferredSlot()
                         {
                             BrokerReferralId = iObj.BrokerReferralId,
                             PreferredDate = Convert.ToDateTime(iObj.PreferredDateTime),
                             Location = iObj.Location,
                             Attendents = iObj.Attendents,
                             Notes = iObj.Notes,
                             IsFinal = true,
                             Active = true,
                             DateCreated = DateTime.UtcNow,
                             DateModified = DateTime.UtcNow


                         };
                         mastercontext.TblPreferredSlots.InsertOnSubmit(Obj);
                         TblBrokerReferral ReferralObj = (from p in mastercontext.TblBrokerReferrals where p.Active == true && p.BrokerReferralId == iObj.BrokerReferralId select p).Take(1).SingleOrDefault();
                         if (ReferralObj != null)
                         {
                             ReferralObj.IsFinal = true;
                             ReferralObj.Status = Convert.ToInt32(CObjects.enumBrokerReferralStatus.Confirmed);
                             ReferralObj.DateModified = DateTime.UtcNow;
                            
                         }
                         mastercontext.SubmitChanges();

                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         TblPreferredSlot PreferredObj = (from p in mastercontext.TblPreferredSlots where p.Active == true && p.PreferredSlotId == iObj.PreferredSlotId select p).Take(1).SingleOrDefault();
                         if (PreferredObj != null)
                         {
                             PreferredObj.IsFinal = true;
                             PreferredObj.DateModified = DateTime.UtcNow;
                             TblBrokerReferral ReferralObj = (from p in mastercontext.TblBrokerReferrals where p.Active == true && p.BrokerReferralId == iObj.BrokerReferralId select p).Take(1).SingleOrDefault();
                             if (ReferralObj != null)
                             {
                                 ReferralObj.IsFinal = true;
                                 ReferralObj.Status = Convert.ToInt32(CObjects.enumBrokerReferralStatus.Confirmed);
                                 ReferralObj.DateModified = DateTime.UtcNow;

                             }
                             mastercontext.SubmitChanges();
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                         }
                     }
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                 }
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region ListOfBrokerReferrals
         public CListOfBrokerReferralsResponse ListOfBrokerReferrals(LazyLoadingInputV2 iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListOfBrokerReferralsResponse lobjResponse = new CListOfBrokerReferralsResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 List<BrokerReferralData> Result = new List<BrokerReferralData>();
                   try
                     {
                         List<BrokerReferralData> Obj = (from p in mastercontext.TblBrokerReferrals.AsEnumerable()
                                                            where p.Active == true 
                                                            && p.UserId == iObj.LoggedInUserId
                                                            select new BrokerReferralData()
                                                            {
                                                                BrokerReferralId = p.BrokerReferralId,
                                                                 CompanyName = p.CompanyName,
                                                                 CompanyWebsite = p.CompanyWebsite,
                                                                 Industry = p.Industry,
                                                                 NoOfEmployees = Convert.ToDecimal(p.NoOfEmployees),
                                                                 HaveGroupBenefits = Convert.ToString(p.HaveGroupBenefits),
                                                                 ContactName = p.ContactName,
                                                                 ContactTitle = p.ContactTitle,
                                                                 EmailAddress = p.EmailAddress,
                                                                 Phone =p.Phone ,
                                                                 ReferralFile = p.DocFile,
                                                                 Status = Convert.ToDecimal(p.Status)
                                                            }).ToList();

                         if (Obj != null && Obj.Count > 0)
                         {
                             if (iObj.ChunckStart == -1)
                             {
                                 Result = Obj.OrderByDescending(i => i.BrokerReferralId).Take(iObj.ChunckSize).ToList();
                             }
                             else
                             {
                                 Result = Obj.OrderByDescending(i => i.BrokerReferralId).Where(i => i.BrokerReferralId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                             }
                         }


                         lobjResponse.BrokerReferralData = Result;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     catch (Exception ex)
                     {

                         lobjResponse.Message = ex.Message;
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     }

                   CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfBrokerReferrals", InputJson, lobjResponse.Message);
                   mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region GetPreferredSlotsById
         public CGetPreferredSlotsByIdResponse GetPreferredSlotsById(IdInput iObj)
         {
             CGetPreferredSlotsByIdResponse lobjResponse = new CGetPreferredSlotsByIdResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
               
                 try
                 {
                     List<PreferredSlots> Obj = (from p in mastercontext.TblPreferredSlots.AsEnumerable()
                                                 join q in mastercontext.TblBrokerReferrals on p.BrokerReferralId equals q.BrokerReferralId
                                                 where p.Active == true && q.Active==true
                                                 && p.BrokerReferralId == iObj.Id
                                                 select new PreferredSlots()
                                                     {
                                                         PreferredSlotId = p.PreferredSlotId,
                                                         Location = p.Location,
                                                         Attendents = p.Attendents,
                                                         Notes = p.Notes,
                                                         PreferredDateTime = Convert.ToString(p.PreferredDate),
                                                         IsFinal = Convert.ToBoolean(p.IsFinal),
                                                         Status = Convert.ToInt32(q.Status),
                                                     }).ToList();


                     Int32 Status = (from p in mastercontext.TblBrokerReferrals 
                                     where p.Active == true && p.BrokerReferralId == iObj.Id select Convert.ToInt32(p.Status)).Take(1).SingleOrDefault();
                     lobjResponse.PreferredSlotsList = Obj;
                     lobjResponse.BrokerReferralStatus = Status;

                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                 }
                 catch (Exception ex)
                 {

                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 mastercontext.Connection.Close();
                 }
             return lobjResponse;
         }
        #endregion

        #region MarkDoneBrokerReferral
         public CResponse MarkDoneBrokerReferral(MarkDoneBrokerReferralInput iObj)
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 try
                 {

                     TblBrokerReferral ReferralObj = (from p in mastercontext.TblBrokerReferrals where p.Active == true && p.BrokerReferralId == iObj.BrokerReferralId select p).Take(1).SingleOrDefault();
                     if (ReferralObj != null)
                        {
                            ReferralObj.Status = Convert.ToInt32(iObj.Status);
                            ReferralObj.DateModified = DateTime.UtcNow;
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                        }
                    
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                 }
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

    }
}