﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CAdminObjects
    {
    }
    public class CLoginEflexUserResponse
    {
        public string Message { get; set; }
        public decimal ID { get; set; }
        public string UserToken { get; set; }
        public string Status { get; set; }
        public decimal UserTypeId { get; set; }
        public string UserName { get; set; }
        public bool IsApproaved { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsPersonal { get; set; }
        public decimal CompanyId { get; set; }
        public decimal BrokerId { get; set; }
        public int NotifyType { get; set; }
    }
    public class LazyLoadingInput
    {
        public decimal ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public decimal Id { get; set; }
        public decimal StatusId { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CGetDashboardDataResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal TotalCompany { get; set; }
        public decimal TotalEmployess { get; set; }
        public decimal TotalBrokers { get; set; }
        public decimal TotalClaims { get; set; }
        public decimal TotalPendingClaims { get; set; }
        public decimal TotalPayments { get; set; }
        public bool IsLogOut { get; set; }
        public List<ReceivedPaymentData> ReceivedPaymentData { get; set; }
        public List<DuePaymentData> DuePaymentData { get; set; }

    }
 
    public class PopulateCompany
    {
        public string CompanyName { get; set; }
        public decimal CompanyId { get; set; }
        public string DbCon { get; set; }
    }

    public class AdminClaimList
    {
        public Int32 RowId { get; set; }
        public decimal EmpClaimId { get; set; }
        public string Employee { get; set; }
        public string Dependent { get; set; }
        public string ClaimDt { get; set; }
        public decimal ClaimAmt { get; set; }
        public string Company { get; set; }
        public string Status { get; set; }
        public string ReceiptDT { get; set; }
        public string Receipt { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }
        public string FileName { get; set; }
        public string Note { get; set; }
        public bool IsManual { get; set; }
        public bool IsRefund { get; set; }
        public string CompanyCode { get; set; }
        public decimal CompanyId { get; set; }
        public ClaimServiceDetails ClaimServiceDetails { get; set; }
        public GetPAPDetails GetPAPDetails { get; set; }
    }

    public class UpdateClaimStatusInput
    {
        public decimal ClaimId { get; set; }
        public Int32 StatusId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public bool IsRefundClaimFee { get; set; }
    }

    public class CListOfCompaniesResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public bool IsLogOut { get; set; }
        public List<ListOfCompany> ListOfCompany { get; set; }
    }

    public class ListOfCompany
    {
        public decimal CompanyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OwnerName { get; set; }
        public string Province { get; set; }
        public string PlanType { get; set; }
        public string DbCon { get; set; }
        public bool IsBlocked { get; set; }
        public decimal OwnerId { get; set; }
        public string IsApproved { get; set; }
        public List<BlockReason> BlockReason { get; set; }
    }

    public class BlockReason
    {
        public string Reason { get; set; }
        public string Date { get; set; }
        public bool IsBlock { get; set; }
    }
    public class CListOfBrokersResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public decimal TotalBroker { get; set; }
        public decimal ApprovedBroker { get; set; }
        public decimal UnApprovedBroker { get; set; }
        public List<ListOfBrokers> ListOfBrokers { get; set; }
    }

    public class ListOfBrokers
    {
        public decimal BrokerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
        public decimal UserId { get; set; }
        public bool IsApproved { get; set; }
        public bool IsPersonal { get; set; }
        public string ContactNo { get; set; }
    }

    public class CGetCompanyDetailsForAdminResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public bool IsLogOut { get; set; }
        public CompanyFiguresData CompanyFiguresData { get; set; }
        public CompanyBasicaData CompanyBasicaData { get; set; }
        public CompanyPAPDetails CompanyPAPDetails { get; set; }
        public List<PlanListOfCompany> PlanListOfCompany { get; set; }
        public CompReferralData CompReferralData { get; set; }

    }

    public class CompanyFiguresData
    {
        public decimal TotalEmployees { get; set; }
        public decimal TotalClaims { get; set; }
        public decimal DuePayments { get; set; }
        public decimal AvailableCredits { get; set; }
    }

    public class CompanyPAPDetails
    {
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string TransitId { get; set; }
        public string InstitutionName { get; set; }
        public string Cheque { get; set; }
        public decimal CompFinancialdetailId { get; set; }
    }

    public class PlanListOfCompany
    {
        public decimal PlanId { get; set; }
        public string PlanName { get; set; }
        public decimal NoOfEmployees { get; set; }
        public string PlanStartDT { get; set; }
    }
    public class CompanyDetailsInput
    {
        public string DbCon { get; set; }
        public decimal CompanyId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class ListCompanyEmployeesInput
    {
        public int ChunkSize { get; set; }
        public int ChunkStart { get; set; }
        public string SearchString { get; set; }
        public decimal CompanyId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public int Filter { get; set; } // -1 ALL,0 InActive, 1 Active, 2 NotUpdated
    }

    public class CListCompEmployeesResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public bool IsLogOut { get; set; }
        public List<CompEmpDetails> CompEmpDetails { get; set; }
    }

    public class CompEmpDetails
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string PlanName { get; set; }
        public decimal AvailableAmount { get; set; }
        public decimal PlanId { get; set; }
        public string IsActive { get; set; }
        public bool IsDeleteable { get; set; }
    }

    public class CListOfAllReceivedPayment
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<CompPaymentDetails> ReceivedPaymentData { get; set; }
    }

    public class CompPaymentDetails
    {
        public decimal CompanyPaymentId { get; set; }
        public string CompanyName { get; set; }
        public string PaymentDueDate { get; set; }
        public string PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentDetails { get; set; }

        public GetPAPDetails GetPAPDetails { get; set; }
    }
    public class PaymentsInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string FromDt { get; set; }
        public string ToDt { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CListOfAllDuePayment
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<CompPaymentDetails> DuePaymentData { get; set; }
    }

    public class MarkPaymentReceivedInput
    {
        public decimal CompPaymentId { get; set; }
        public decimal StatusId { get; set; }
        public decimal LoggedInUserId { get; set; }

    }

    public class CEmployeeClaimListResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }  
        public bool IsLogOut { get; set; }
        public List<EmployeeClaimList> EmployeeClaimList { get; set; }
    }

    public class EmployeeClaimList
    {
        public decimal EmpClaimId { get; set; }
        public string Employee { get; set; }
        public string Dependent { get; set; }
        public string ClaimDt { get; set; }
        public decimal ClaimAmt { get; set; }
        public string Status { get; set; }
        public string ReceiptDT { get; set; }
        public string Receipt { get; set; }
        public string FileName { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }
        public bool IsManual { get; set; }
        public string Note { get; set; }
        public ClaimServiceDetails ClaimServiceDetails { get; set; }
    }

    public class EmployeeClaimListInput
    {
        public decimal ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public decimal CompanyId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }

        public decimal StatusId { get; set; }
    }

    public class AddUserInput
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string ContactNo { get; set; }
        public string Pwd { get; set; }
        public string EmailAddress { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class EditUserInput
    {
        public decimal UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string ContactNo { get; set; }
        public string Pwd { get; set; }
        public string EmailAddress { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CListAdminUserResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListAdminUser> ListAdminUser { get; set; }
    }

    public class ListAdminUser
    {
        public decimal UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string ContactNo { get; set; }
        public string Pwd { get; set; }
        public string EmailAddress { get; set; }
        public decimal UserTypeId { get; set; }
    }

    public class DeleteAdminUserInput
    {
        public decimal UserId { get; set; }
        public decimal LoggedInUserId { get; set; }

    }

    public class GetEmpDetailsForAdminInput
    {
        public decimal UserId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyId { get; set; }

    }

    public class CGetEmpDetailsForAdminResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public EmpFiguresData EmployeeFiguresData { get; set; }
        public EmpBasicData EmpBasicData { get; set; }
        public EmpPAPDetails EmpPAPDetails { get; set; }
        public List<PlanListOfEmp> PlanListOfEmp { get; set; }
        
    }

    public class EmpFiguresData
    {
        public decimal TotalClaimAmount { get; set; }
        public decimal TotalClaims { get; set; }
        public decimal TotalBalance { get; set; }
        public decimal TotalAdjustments { get; set; }
        public decimal CreditAdjustments { get; set; }
        public decimal DebitAdjustments { get; set; }
    }
    public class EmpBasicData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string MobileNo { get; set; }
        public string EveningPhone { get; set; }
        public string MorningPhone { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public bool IsActive { get; set; }
        public string InActiveDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public CellNotificationDetails CellNotificationDetails { get; set; }
    }

    public class EmpPAPDetails
    {
        public decimal PAPDetailId { get; set; }
        
        public string BankName { get; set; }
        public string TransitId { get; set; }
        public string InstitutionId { get; set; }
        public string AccountNumber { get; set; }
        public string ChequeFile { get; set; }
        public string IsCheque { get; set; }
        public string IsAgree { get; set; }

    }
    public class PlanListOfEmp
    {
        public decimal PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanStartDT { get; set; }
    }

    public class UpdatePlanDateInput
    {

        public decimal CompanyId { get; set; }
        public List<UpPlan> Plans { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class UpPlan
    {
        public string PlanStartDT { get; set; }
        public decimal PlanId { get; set; }
    }

    public class GetEmpDashboardInput
    {
        public decimal UserId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyId { get; set; }
        public string Type { get; set; }//MONTH,YEAR

    }

    public class UpdateEmpBasicDetailsByAdminInput
    {
        public string DbCon { get; set; }
        public decimal CompanyUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string MobileNo { get; set; }
        public string EveningPhone { get; set; }
        public string MorningPhone { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Carrier { get; set; }
        public string IsNotify { get; set; }
        public decimal LoggedInUserId { get; set; }
    }
    public class CGetEmpDashboardResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        
        public List<string> Data { get; set; }
        public List<decimal> ClaimAmtReceived { get; set; }
        public List<decimal> ClaimCount { get; set; }
    }

    public class AddEmployeeByadminInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public List<EmployeesList> EmployeesList { get; set; }

    }

    public class EmpClaimData
    {
        public decimal Amount { get; set; }
        public string Data { get; set; }

    }
    public class EmpWithdrawData
    {
        public decimal Amount { get; set; }
        public string Data { get; set; }

    }

    public class EmpDashboardDataForAdmin
    {
        public List<string> Data { get; set; }
        public List<decimal> ClaimAmount { get; set; }
        public List<string> ClaimPaidAmt { get; set; }
    }

    public class CListOfProviceTaxDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListOfProviceTaxDetails> ListOfProviceTaxDetails { get; set; }
    }

    public class ListOfProviceTaxDetails
    {
        public string Name { get; set; }
        public decimal AdminFeePercent { get; set; }
        public decimal RST { get; set; }
        public decimal HST { get; set; }
        public decimal GST { get; set; }
        public decimal PPT { get; set; }
        public decimal ProvinceId { get; set; }
    }

    public class UpdateProvinceTaxDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public string Name { get; set; }
        
        public decimal RST { get; set; }
        public decimal HST { get; set; }
        public decimal GST { get; set; }
        public decimal PPT { get; set; }
        public decimal ProvinceId { get; set; }
    }
    public class EflexNotificationLogInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string LoggedInUserId { get; set; }

    }

    public class AddProvinceTaxDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public string Name { get; set; }
        public decimal RST { get; set; }
        public decimal HST { get; set; }
        public decimal GST { get; set; }
        public decimal PPT { get; set; }

    }

    public class AddFeeDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public string Title { get; set; }
        public decimal FeeTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal Frequency { get; set; }
        public bool IsApplicable { get; set; }
        public decimal UserTypeId { get; set; }
    }

    public class UpdateFeeDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public string Title { get; set; }
        public decimal FeeTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal Frequency { get; set; }
        public bool IsApplicable { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal FeeDetailId { get; set; }
    }

    public class CListFeeDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ListFeeDetails> ListFeeDetails { get; set; }
    }

    public class ListFeeDetails
    {
        public string FeeType { get; set; }

        public decimal FeeTypeId { get; set; }

        public List<FeeDetailData> FeeDetailData { get; set; }

    }

    public class FeeDetailData
    {
        public decimal Amount { get; set; }
        public string Title { get; set; }
        public decimal Frequency { get; set; }
        public bool IsApplicable { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal FeeDetailId { get; set; }
    }
    public class CPopulateFeeTypeResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<PopulateFeeType> PopulateFeeType { get; set; }
    }

    public class PopulateFeeType
    {
        public string Title { get; set; }
        public decimal FeeTypeId { get; set; }
    }

    public class UpdateAdminFeeDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal AdminMax { get; set; }
        public decimal AdminMin { get; set; }
        public decimal BrokerMax { get; set; }
        public decimal BrokerMin { get; set; }
    }

    public class CGetAdminFeeResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public GetAdminFeeData GetAdminFeeData { get; set; }

    }
    public class GetAdminFeeData
    {
        public decimal AdminMax { get; set; }
        public decimal AdminMin { get; set; }
        public decimal BrokerMax { get; set; }
        public decimal BrokerMin { get; set; }
    }

    public class EflexNotiMsgCountInfo
    {
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
    }

    public class AddCompAdjustmentsInput
    {
        public decimal CompanyId { get; set; }
        public decimal Amount { get; set; }
        public string AdjustmentDT { get; set; }
        public string Remarks { get; set; }
        public decimal TypeId { get; set; }//,--1   Credit,2    Debit
        public decimal LoggedInUserId { get; set; }
    }

    public class AddLateFeeToCompanyInput
    {
        public decimal LoggedInUserId { get; set; }
        public bool IsLateFee { get; set; }
        public decimal LateFee { get; set; }
        public bool IsESFFee { get; set; }
        public decimal ESFFee { get; set; }
        public string Remarks { get; set; }
        public decimal CompanyPaymentId { get; set; }

    }

    public class UpdateCompanyCreditsInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal Credits { get; set; }
    }

    public class ResetPasswordInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyUserId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CListTaxPotResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ListTaxPot> ListTaxPotData { get; set; }


    }

    public class ListTaxPot
    {
        public decimal CompanyId { get; set; }
        public string CompanyName { get; set; }
        public decimal RST { get; set; }
        public decimal HST { get; set; }
        public decimal AdminFee { get; set; }
        public decimal PPT { get; set; }
        public decimal Total { get; set; }
        public string RemitDate { get; set; }
    }

    public class ListMasterTransactionsInput
    {
        public Int32 ChunckSize { get; set; }
        public Int32 ChunckStart { get; set; }
        public decimal Id { get; set; }
      
        public decimal CompanyId { get; set; }//-1  All,1  Monthly, 2  Quaterly, 3  Yearly
        public string FilterType { get; set; }//Adj,Credits,Fee,Claims
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public string FromDT { get; set; }
        public string EndDT { get; set; }
    }

    public class CListMasterTransactionsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<MasterCompDetails> MasterCompData { get; set; }
    }

    public class MasterCompDetails
    {
        public Int32 RowId { get; set; }
       
        public decimal Id { get; set; }
        public string CompanyName { get; set; }
        
        public string EmployeeName { get; set; }

        public decimal Amount { get; set; }
        public string AdjustmentDT { get; set; }
        public string Remarks { get; set; }
        public string Type { get; set; }//,--1   Credit,2    Debit
        public string FilterType { get; set; }


        public decimal PaymentStatus { get; set; }
    }

    public class CompTransactionData
    {
        public decimal Amount { get; set; }
        public string AdjustmentDT { get; set; }
        public string Remarks { get; set; }
        public string Type { get; set; }//,--1   Credit,2    Debit
        public string FilterType { get; set; }
        
    }

    public class EmpTransactionData
    {
        public string EmpName { get; set; }
        public decimal Amount { get; set; }
        public string AdjustmentDT { get; set; }
        public string Remarks { get; set; }
        public string Type { get; set; }//,--1   Credit,2    Debit
        public string FilterType { get; set; }

    }

    public class CListRollOverPotResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListRollOverPot> ListRollOverPot { get; set; }
    }

    public class ListRollOverPot
    {
        public string CompanyName { get; set; }
        public decimal TotalCredits { get; set; }
        public List<EmployeeCredits> EmployeeCredits { get; set; }
    }

    public class EmployeeCredits
    {
        public string EmployeeName { get; set; }
        public decimal TotalBalance { get; set; }
        public string InActiveDate { get; set; }
    }

    public class CGetBrokerFeeSetByEflexResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal BrokerFee { get; set; }
        public decimal BrokerCommissionFee { get; set; }
    }

    public class ChangePasswordEflexUserInput
    {
        public decimal UserId { get; set; }
        public string PrevPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ResetBrokerPasswordInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal UserId { get; set; }

    }

    public class ListOfBrokerCompaniesForAdminInput
    {
        public Int32 ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal UserId { get; set; }
    }
    public class GetPlanDesignDetailsInput
    {
        public decimal PlanId { get; set; }
        public decimal CompanyId { get; set; }

    }

    public class CGetPlanDesignDetailsResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public PlanDetails1 PlanDesign { get; set; }
    }

    public class AddCardFeeRangeInput
    {
        public decimal MaxValue { get; set; }
	    public decimal MinValue { get; set; }
	    public decimal DayMax { get; set; }
	    public decimal WeeklyMax { get; set; }
	    public decimal MonthlyMax { get; set; }
	    public decimal OverAllMax { get; set; }
        public decimal UserId { get; set; }
        public decimal ContributionType { get; set; }
        public bool IsApplicable { get; set; }
	
    }

    public class CListCardFeeRangeResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListCardFeeRange> ListCardFeeRange { get; set; }
    }

    public class ListCardFeeRange
    {
        public decimal CardFeeRangeId { get; set; }
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public decimal DayMax { get; set; }
        public decimal WeeklyMax { get; set; }
        public decimal MonthlyMax { get; set; }
        public decimal OverAllMax { get; set; }
        public decimal UserId { get; set; }
        public decimal ContributionType { get; set; }
        public string IsApplicable { get; set; }
    }

    public class EditCardFeeRangeInput
    {
        public decimal CardFeeRangeId { get; set; }
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public decimal DayMax { get; set; }
        public decimal WeeklyMax { get; set; }
        public decimal MonthlyMax { get; set; }
        public decimal OverAllMax { get; set; }
        public decimal UserId { get; set; }
        public decimal ContributionType { get; set; }
        public bool IsApplicable { get; set; }

    }

    public class AddServiceInput
    {
        public string Title { get; set; }
        public decimal ServiceTypeId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }
        public bool IsApproved { get; set; }
    }

    public class EditServiceInput
    {
        public decimal ServiceId { get; set; }
        public string Title { get; set; }
        public decimal ServiceTypeId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }
        public bool IsApproved { get; set; }
    }

    public class CListOfServicesResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ListOfServices> ListOfServices { get; set; }
    }

    public class ListOfServices
    {
        public decimal ServiceId { get; set; }
        public string Title { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }
        public string ServiceType { get; set; }
        public decimal ServiceTypeId { get; set; }
        public string DateCreated { get; set; }
    }
    public class CPopulateServiceTypesResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<PopulateServiceTypes> PopulateServiceTypes { get; set; }
    }

    public class PopulateServiceTypes
    {
        public decimal ServiceTypeId { get; set; }
        public string Title { get; set; }
    }
    public class ListOfServiceProviderIdInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public decimal Id { get; set; }
        public decimal ServiceId { get; set; }
    }

    public class ListOfServicesIdInput
    {
        public string SearchString { get; set; }
        public decimal Id { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal ServiceId { get; set; }
    }
    public class AddServiceProviderInput
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
      
        public decimal CompanyId { get; set; }
        public decimal ServiceId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CListOfServicesProviderResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ListOfServicesProvider> ListOfServicesProvider { get; set; }
    }

    public class ListOfServicesProvider
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public decimal CompanyId { get; set; }
        public string ServiceType { get; set; }
        public decimal ServiceId { get; set; }
        public decimal ServiceProviderId { get; set; }

    }

    public class ReceivedPaymentData
    {
        public string PaymentDetails { get; set; }
        public string CompanyName { get; set; }
        public string PaymentDate { get; set; }
        public string PaymentDueDate { get; set; }
        public decimal Amount { get; set; }
    }
    public class DuePaymentData
    {
        public string PaymentDetails { get; set; }
        public string CompanyName { get; set; }
        public string PaymentDueDate { get; set; }
        public string PaymentDate { get; set; }
        public decimal Amount { get; set; }
    }

    public class AdminClaimListInput
    {
        public Int32 ChunckSize { get; set; }
        public Int32 ChunckStart { get; set; }
        public decimal Id { get; set; }
        public decimal StatusId { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public Int32 Ismanual { get; set; }
    }

    public class CPopulateCarrierResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<PopulateCarrier> PopulateCarrierData { get; set; }


    }

    public class PopulateCarrier
    {
        public decimal CarrierId { get; set; }
        public string Title { get; set; }
        public string Carrier { get; set; }
    }

    public class AddEditCarrierInput
    {
        public decimal CarrierId { get; set; }
        public string Title { get; set; }
        public string Carrier { get; set; }
    }
}