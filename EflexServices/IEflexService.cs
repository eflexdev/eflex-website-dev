﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
   // [ServiceContract(SessionMode = SessionMode.Required)]
    public partial interface IEflexService
    {
        #region AddCompany
        [WebInvoke(UriTemplate = "AddCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAddCompanyResponse AddCompany(AddCompanyInput iObj);
        #endregion AddCompany

        #region ValidateUniqueCode
        [WebInvoke(UriTemplate = "ValidateUniqueCode", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CValidateUniqueCodeResponse ValidateUniqueCode(ValidateUniqueCodeInput iObj);
        #endregion ValidateUniqueCode

        #region AddCompanyPlan
        [WebInvoke(UriTemplate = "AddCompanyPlan", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddCompanyPlan(AddCompanyPlanInput iObj);
        #endregion AddCompanyPlan

        #region PopulateProvince
        [WebInvoke(UriTemplate = "PopulateProvince", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateProvinceResponse PopulateProvince();
        #endregion PopulateProvince

        #region UpdateCompanyPlanType
        [WebInvoke(UriTemplate = "UpdateCompanyPlanType", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateCompanyPlanType(CompanyPlanTypeInput iObj);
        #endregion UpdateCompanyPlanType

        #region GetCompanyDetails
        [WebInvoke(UriTemplate = "GetCompanyDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetCompanyDetailsResponse GetCompanyDetails(GetCompanyDetailsInput iObj);
        #endregion GetCompanyDetails

        #region LoginUser
        [WebInvoke(UriTemplate = "LoginUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoginUserResponse LoginUser(LoginUserInput iObj);
        #endregion LoginUser

        #region LoginUserForApp
        [WebInvoke(UriTemplate = "LoginUserForApp", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoginUserResponse LoginUserForApp(LoginUserForAppInput iObj);
        #endregion LoginUserForApp

        #region AddEditEmployess
        [WebInvoke(UriTemplate = "AddEditEmployess", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEditEmployess(AddEditEmployessInput iObj);
        #endregion AddEditEmployess

        #region PopulateClasses
        [WebInvoke(UriTemplate = "PopulateClasses", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateClassesResponse PopulateClasses(DbConInput iObj);
        #endregion PopulateClasses

        #region ChangePassword
        [WebInvoke(UriTemplate = "ChangePassword", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ChangePassword(ChangePasswordInput iObj);
        #endregion ChangePassword

        #region ForgotPassword
        [WebInvoke(UriTemplate = "ForgotPassword", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ForgotPassword(ForgetPasswordInput iObj);
        #endregion ForgotPassword

        #region ListEmployees
        [WebInvoke(UriTemplate = "ListEmployees", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListEmployeesResponse ListEmployees(ListEmployeesInput iObj);
        #endregion ListEmployees

        #region AddCompanyFinancialDetails
        [WebInvoke(UriTemplate = "AddCompanyFinancialDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddCompanyFinancialDetails(AddEditCompanyFinancialDetailsInput iObj);
        #endregion AddCompanyFinancialDetails

        #region GetRandomCode
        [WebInvoke(UriTemplate = "GetRandomCode", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetRandomCodeResponse GetRandomCode();
        #endregion GetRandomCode

        #region GetPaymentOverview
        [WebInvoke(UriTemplate = "GetPaymentOverview", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetPaymentOverviewResponse GetPaymentOverview(DbConInput iObj);
        #endregion GetPaymentOverview

        #region InActiveEmployee
        [WebInvoke(UriTemplate = "InActiveEmployee", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse InActiveEmployee(InActiveEmployeeInput iObj);
        #endregion InActiveEmployee

        #region UpdatePlanDetails
        [WebInvoke(UriTemplate = "UpdatePlanDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdatePlanDetails(UpdatePlanDetailsInput iObj);
        #endregion UpdatePlanDetails

        #region UpdatePaymentDate
        [WebInvoke(UriTemplate = "UpdatePaymentDate", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdatePaymentDate(UpdatePaymentDateInput iObj);
        #endregion UpdatePaymentDate

        #region PopulateReferrals
        [WebInvoke(UriTemplate = "PopulateReferrals", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateReferralsResponse PopulateReferrals(DbConInput iObj);
        #endregion PopulateBrokers

        #region SaveCompanyReferrals
        [WebInvoke(UriTemplate = "SaveCompanyReferrals", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SaveCompanyReferrals(SaveCompanyReferralsInput iObj);
        #endregion SaveCompanyReferrals

        #region PopulateBrokers
        [WebInvoke(UriTemplate = "PopulateBrokers", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateBrokersResponse PopulateBrokers();
        #endregion PopulateBrokers

        #region GetCompanyHistoryLog
        [WebInvoke(UriTemplate = "GetCompanyHistoryLog", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponseGetCompanyHistoryLog GetCompanyHistoryLog(GetCompanyHistoryLogInput iObj);
        #endregion GetCompanyDashboardData

        #region GetNearbyServiceProvider
        [WebInvoke(UriTemplate = "GetNearbyServiceProvider", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetNearbyServiceProviderResponse GetNearbyServiceProvider(GetNearbyServiceProviderInput iObj);
        #endregion GetNearbyServiceProvider

        #region GetBrokerFeeSetByEflex
        [WebInvoke(UriTemplate = "GetBrokerFeeSetByEflex", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetBrokerFeeSetByEflexResponse GetBrokerFeeSetByEflex(IdInput iObj);
        #endregion GetBrokerFeeSetByEflex

        #region GetCardData
        [WebInvoke(UriTemplate = "GetCardData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetCardDataResponse GetCardData(DbConLoggedInUIdInput iObj);
        #endregion GetCardData

        #region UserLogOut
        [WebInvoke(UriTemplate = "UserLogOut", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UserLogOut(LogOutInput iObj);
        #endregion UserLogOut

        #region ResentEmployeeCredentials
        [WebInvoke(UriTemplate = "ResentEmployeeCredentials", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResentEmployeeCredentials(ResentEmployeeCredentialsInput iObj);
        #endregion ResentEmployeeCredentials

        #region AddGetInTouch
        [WebInvoke(UriTemplate = "AddGetInTouch", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddGetInTouch(AddGetInTouchInput iObj);
        #endregion AddGetInTouch

        #region ListOfEnquiry
        [WebInvoke(UriTemplate = "ListOfEnquiry", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfEnquiryResponse ListOfEnquiry(LazyLoadingSearchInput iObj);
        #endregion ListOfEnquiry

        #region SendFinalMailToEmployee
        [WebInvoke(UriTemplate = "SendFinalMailToEmployee", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SendFinalMailToEmployee(IdInputv2 iObj);
        #endregion SendFinalMailToEmployee

        #region UpdateFCMToken
        [WebInvoke(UriTemplate = "UpdateFCMToken", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateFCMToken(UpdateFCMInput Obj);
        #endregion UpdateFCMToken
        

        #region AuthorizeUser
        [WebInvoke(UriTemplate = "AuthorizeUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoginUserResponse AuthorizeUser(AuthorizeUserInput iObj);
        #endregion AuthorizeUser

        #region ListEmployeesV2
        [WebInvoke(UriTemplate = "ListEmployeesV2", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListEmployeesResponse ListEmployeesV2(ListEmployeesInputV2 iObj);
        #endregion ListEmployeesV2
    }
}