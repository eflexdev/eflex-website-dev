﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    public partial interface IEflexService
    {
        #region ImportEmployeesExcel
        [WebInvoke(UriTemplate = "ImportEmployeesExcel", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ImportEmployeesExcel(ImportUsersExcelInput iObj);
        #endregion ImportEmployeesExcel

        #region DeleteUser
        [WebInvoke(UriTemplate = "DeleteUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteUser(DeleteUserInput iObj);
        #endregion DeleteUser

        #region AddAdmin
        [WebInvoke(UriTemplate = "AddAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddAdmin(AddAdminInput iObj);
        #endregion AddAdmin

        #region GetEmpDashboardTransactions
        [WebInvoke(UriTemplate = "GetEmpDashboardTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDashboardTransactionsResponse GetEmpDashboardTransactions(DbConIdInput iObj);
        #endregion GetEmpDashboardTransactions

        #region GetEmpTransactionData
        [WebInvoke(UriTemplate = "GetEmpTransactionData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpTransactionDataResponse GetEmpTransactionData(GetEmpTransactionInput iObj);
        #endregion GetEmpTransactionData

        #region GetEmpDashboardData
        [WebInvoke(UriTemplate = "GetEmpDashboardData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDashboardDataResponse GetEmpDashboardData(DbConIdInput iObj);
        #endregion GetEmpDashboardData

        #region GetEmpDetailsById
        [WebInvoke(UriTemplate = "GetEmpDetailsById", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDetailsByIdResponse GetEmpDetailsById(DbConIdInput iObj);
        #endregion GetEmpDetailsById

        #region UpdateEmpBasicDetails
        [WebInvoke(UriTemplate = "UpdateEmpBasicDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateEmpBasicDetails(UpdateEmpBasicDetailsInput iObj);
        #endregion UpdateEmpBasicDetails

        #region AddEmpDependents
        [WebInvoke(UriTemplate = "AddEmpDependents", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEmpDependents(AddEmpDependentsInput iObj);
        #endregion AddEmpDependents

        #region GetEmpDependentDetails
        [WebInvoke(UriTemplate = "GetEmpDependentDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDependentDetailsResponse GetEmpDependentDetails(DbConLoggedInUIdInput iObj);
        #endregion GetEmpDependentDetails

        #region EditEmpDependents
        [WebInvoke(UriTemplate = "EditEmpDependents", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse EditEmpDependents(EditEmpDependentsInput iObj);
        #endregion EditEmpDependents

        #region DeleteDependents
        [WebInvoke(UriTemplate = "DeleteDependents", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteDependents(DbConLoggedInUIdInput iObj);
        #endregion DeleteDependents

        #region ListOfDependents
        [WebInvoke(UriTemplate = "ListOfDependents", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfDependentsResponse ListOfDependents(LazyLoadingSearchInput iObj);
        #endregion ListOfDependents

        #region PopulateDependants
        [WebInvoke(UriTemplate = "PopulateDependants", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateDependantsResponse PopulateDependants(DbConIdInput iObj);
        #endregion PopulateDependants

        #region PopulateClaimType
        [WebInvoke(UriTemplate = "PopulateClaimType", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateClaimTypeResponse PopulateClaimType(DbConInput iObj);
        #endregion PopulateClaimType

        #region AddClaim
        [WebInvoke(UriTemplate = "AddClaim", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoadValueResponse AddClaim(AddClaimInput iObj);
        #endregion AddClaim

        #region ListOfClaims
        [WebInvoke(UriTemplate = "ListOfClaims", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfClaimsResponse ListOfClaims(ListOfClaimsInput iObj);
        #endregion ListOfClaims

        #region AddUserCarrier
        [WebInvoke(UriTemplate = "AddUserCarrier", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAddUserCarrierResponse AddUserCarrier(AddUserCarrierInput iObj);
        #endregion AddUserCarrier

        #region AddEmpAdjustments
        [WebInvoke(UriTemplate = "AddEmpAdjustments", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEmpAdjustments(AddEmpAdjustmentsInput iObj);
        #endregion AddEmpAdjustments

        #region ListEmpAllTransactions
        [WebInvoke(UriTemplate = "ListEmpAllTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListEmpAllTransactionsResponse ListEmpAllTransactions(ListEmpAllTransactionsInput iObj);
        #endregion ListEmpAllTransactions

        #region GetEmpCardList
        [WebInvoke(UriTemplate = "GetEmpCardList", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpCardListResponse GetEmpCardList(DbConLoggedInUIdInput iObj);
        #endregion GetEmpCardList

        #region GetEmpCardDetails
        [WebInvoke(UriTemplate = "GetEmpCardDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpCardDetailsResponse GetEmpCardDetails(DbConLoggedInUIdInput iObj);
        #endregion GetEmpCardDetails

        #region ValidatePasscode
        [WebInvoke(UriTemplate = "ValidatePasscode", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ValidatePasscode(ValidatePasscodeInput iObj);
        #endregion ValidatePasscode

        #region ResentOTP
        [WebInvoke(UriTemplate = "ResentOTP", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResentOTP(DbConIdInput iObj);
        #endregion ResentOTP

        #region DeleteEmployee
        [WebInvoke(UriTemplate = "DeleteEmployee", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteEmployee(DeleteEmployeeInput iObj);
        #endregion DeleteEmployee


        #region GetEmpDashboardDataForApp
        [WebInvoke(UriTemplate = "GetEmpDashboardDataForApp", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDashboardDataForAppResponse GetEmpDashboardDataForApp(DbConIdInput iObj);
        #endregion GetEmpDashboardDataForApp

        #region UpdateDependant
        [WebInvoke(UriTemplate = "UpdateDependant", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateDependant(UpdateDependantInput iObj);
        #endregion UpdateDependant


        #region SendMailToUnUpdatedUser
        [WebInvoke(UriTemplate = "SendMailToUnUpdatedUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SendMailToUnUpdatedUser(SendMailToUnUpdatedUserInput iObj);
        #endregion SendMailToUnUpdatedUser

    }
}