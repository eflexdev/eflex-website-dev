﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public static class CObjects
    {
        public static readonly string SessionExpired = "Session timed out, please login again.";

        #region HashPlanDesign
        public static Hashtable HashPlanDesign()
        {
            Hashtable HashPlanDesign = new Hashtable();
            HashPlanDesign.Add("A plan that covers medical health and dental expenses only", 1);
            HashPlanDesign.Add("A plan that covers medical health,dental and wellness (ex: gym membership, vitamins, etc)", 2);
            HashPlanDesign.Add("Revert to Plan Sponsor", 3);
            HashPlanDesign.Add("Carry Forward to benefit of the account holder", 4);
            HashPlanDesign.Add("Revert back to the employer", 5);
            HashPlanDesign.Add("Carry forward to the benefit of the account holder as per plan design", 6);
            return HashPlanDesign;

        }
        #endregion



        #region Enums
        public enum enmStatus
        {
            Success,
            Failure,
            Error
        }
        public enum enmUserType
        {
            Owner = 1,
            Admin = 2,
            Employee = 3,
            SuperAdmin = 4
        }
        public enum enumPlanType
        {
            Single = 1,
            Group = 2
        }
        public enum enumPaymentType
        {
            IN,
            DM,
            DA,
            ADJ,
            WDR,//claim
            CRD,//
            CTR,
            FEE
        }
        public enum enumPaymentStatus
        {
            Pending = 0,
            OverDue = 1,
            PartialPaid = 2,
            Deposited = 3
        }

        public enum enumDeviceType
        {
            Android = 1,
            iPhone = 2
        }
        public enum enumClaimStatus
        {
            InProgress = 1,
            Paid = 2,
            IneligibleClaim = 3,
            ClaimCancelled = 4
        }

        public enum enumMasterUserType
        {
            SuperAdmin = 1,
            Admin = 2,
            Broker = 3
        }
        public enum enumAdjustmentType
        {
            Credit = 1,
            Debit = 2

        }
        public enum enumCardStatus
        {
            Activate,
            Suspend,
            Unsuspend,
            Pending
        }
        public enum enumFeeType
        {
            Claim = 1,
            Card = 2,
            Account = 3

        }

        public enum enumCardStatusByBerkely
        {
            DORMANT = 0,
            READY = 1,
            ACTIVE = 2,
            CLOSED = 3,
            LOST = 4,
            REPLACED = 5,
            SUSPENDED = 6,
            EXPIRED = 7,
            SACTIVE = 8,
            REVOKED = 9,
            CCLOSED = 11,
            MBCLOSED = 12,
            FRAUD = 14,
            PFRAUD = 15,
            LASTSTMT = 16,
            CHARGEOFF = 17,
            DECEASED = 18,
            WARNING = 19,
            MUCLOSED = 20,
            VOID = 21,
            NONRENEWAL = 22,
            DESTROYED = 23 
        }

        public enum enumFeeDetails
        {
            ClaimFee = 1, 
            CardAnnualFee =2,
            CardReplacementFee =3,
            SupplementaryCardFee =4,
            Setupfee = 5,
            InactiveAccountFee = 6,
            Otherfee = 7,
            LateFee = 8,
            NSFFee = 9

        }
        public enum enumServiceType
        {
            MedicalHealth = 1,
            Dental = 2,
            Wellness = 3
        }
        public enum enmNotifyType
        {
            Claim,
            Message,
            Payment,
            Adjustment,
            Password,
            Card,
            Account,
            Plan,
            Employee,
            Dependant,
            Load
        }
        public enum enumBrokerReferralStatus
        {
            Pending = 0,
            Confirmed = 1,
            Completed = 2
        }
        #endregion Enums
    }

    public class ResponseResult
    {
        public string multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<Result> results { get; set; }
    }

    public class Result
    {
        public string message_id { get; set; }
        public string registration_id { get; set; }
        public string error { get; set; }
    }

    public class IdInput
    {
        public decimal Id { get; set; }
    }
    public class DbConIdInput
    {
        public string DbCon { get; set; }
        public decimal Id { get; set; }

    }
    public class DbConLoggedInUIdInput
    {
        public string DbCon { get; set; }
        public decimal Id { get; set; }
        public decimal LoggedInUserId { get; set; }

    }
    public class IdInputv2
    {
        public decimal Id { get; set; }
        public decimal LoggedInUserId { get; set; }
    }
    public class GetCompanyDetailsInput
    {
        public decimal Id { get; set; }
        public decimal LoggedInUserId { get; set; }
    }
    public class AddCompanyInput
    {
        public decimal CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string OwnerContactNo { get; set; }
        public string OwnerEmail { get; set; }
        public string ServiceUrl { get; set; }
        public string HostUrl { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        
    }

    public class ValidateUniqueCodeInput
    {
        public string UniqueCode { get; set; }
    }

    public class AddCompanyPlanInput
    {
        public string DbCon { get; set; }
        public decimal CompanyId { get; set; }
        public decimal PlanTypeId { get; set; }

        public decimal LoggedInUserId { get; set; }

        public List<PlanDetails> PlanDetails { get; set; }
    }
    public class PlanDetails
    {
        public string PlanStartDt { get; set; }
        public string Name { get; set; }
        public int ContributionSchedule { get; set; }
        public decimal RecurringDeposit { get; set; }
        public decimal InitialDeposit { get; set; }
        public decimal NetDeposit { get; set; }
        public decimal AdministrationInitialFee { get; set; }
        public decimal AdministrationMonthlyFee { get; set; }
        public decimal HSTInitial { get; set; }
        public decimal HSTMonthly { get; set; }
        public decimal PremiumTaxInitial { get; set; }
        public decimal PremiumTaxMonthly { get; set; }
        public decimal RetailSalesTaxInitial { get; set; }
        public decimal RetailSalesTaxMonthly { get; set; }
        public decimal GrossTotalInitial { get; set; }
        public decimal GrossTotalMonthly { get; set; }
        public decimal PlanDesign { get; set; }
        public decimal RollOver { get; set; }
        public string CarryForwardPeriod { get; set; }
        public decimal Termination { get; set; }
       
    }
    public class PlanDetails1
    {
        public string PlanStartDt { get; set; }
        public string Name { get; set; }
        public int ContributionSchedule { get; set; }
        public decimal RecurringDeposit { get; set; }
        public decimal InitialDeposit { get; set; }
        public decimal NetDeposit { get; set; }
        public decimal AdministrationInitialFee { get; set; }
        public decimal AdministrationMonthlyFee { get; set; }
        public decimal HSTInitial { get; set; }
        public decimal HSTMonthly { get; set; }
        public decimal PremiumTaxInitial { get; set; }
        public decimal PremiumTaxMonthly { get; set; }
        public decimal RetailSalesTaxInitial { get; set; }
        public decimal RetailSalesTaxMonthly { get; set; }
        public decimal GrossTotalInitial { get; set; }
        public decimal GrossTotalMonthly { get; set; }
        public string PlanDesign { get; set; }
        public string RollOver { get; set; }
        public string CarryForwardPeriod { get; set; }
        public string Termination { get; set; }
    }

    public class GetPlanDetails
    {
        public decimal PlanId { get; set; }
        public string PlanStartDt { get; set; }
        public string Name { get; set; }
        public int ContributionSchedule { get; set; }
        public decimal RecurringDeposit { get; set; }
        public decimal InitialDeposit { get; set; }
        public decimal NetDeposit { get; set; }
        public decimal AdministrationInitialFee { get; set; }
        public decimal AdministrationMonthlyFee { get; set; }
        public decimal HSTInitial { get; set; }
        public decimal HSTMonthly { get; set; }
        public decimal PremiumTaxInitial { get; set; }
        public decimal PremiumTaxMonthly { get; set; }
        public decimal RetailSalesTaxInitial { get; set; }
        public decimal RetailSalesTaxMonthly { get; set; }
        public decimal GrossTotalInitial { get; set; }
        public decimal GrossTotalMonthly { get; set; }
        public decimal PlanDesign { get; set; }
        public decimal RollOver { get; set; }
        public string CarryForwardPeriod { get; set; }
        public decimal Termination { get; set; }
        public decimal NoOfEmployees { get; set; }
        public string PlanTitle { get; set; }
    }

    public class ProvinceList
    {
        public string Title { get; set; }
        public decimal PPTax { get; set; }
        public decimal RSTax { get; set; }
        public decimal ProvinceId { get; set; }
    }

    public class CompanyPlanTypeInput
    {
        public decimal CompanyId { get; set; }
        public bool IsCorporation { get; set; }
        public decimal PlanTypeId { get; set; }
    }

    public class CompanyBasicaData
    {
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string OwnerContactNo { get; set; }
        public string OwnerEmail { get; set; }
        public string ServiceUrl { get; set; }
        public string HostUrl { get; set; }
        public string Code { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public decimal PlanTypeId { get; set; }
        public string IsCorporation { get; set; }
        public string StagesCovered { get; set; }
        public string DbCon { get; set; }
        public string Province { get; set; }
    }

    public class EmployeesData
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal CompanyPlanId { get; set; }
    }

    public class AdminsData
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public decimal UserTypeId { get; set; }
    }
    public class LoginUserInput
    {
        public string UserName { get; set; }
        public string Password { get; set; }
  
    }

    public class LoginUserForAppInput
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
        public string FCMToken { get; set; }
        public string Ver { get; set; }
        public string DeviceName { get; set; }
        public string OSVersion { get; set; }
        public decimal DeviceTypeId { get; set; }
    }
    public class AddEditEmployessInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public List<AdminList> AdminList { get; set; }
        public List<EmployeesList> EmployeesList { get; set; }
    }


    public class AdminList
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public decimal UserTypeId { get; set; }

    }

    public class EmployeesList
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal CompanyPlanId { get; set; }
    }

    public class DbConInput
    {
        public string DbCon { get; set; }
    }

    public class ClassList
    {
        public string Name { get; set; }
        public decimal ClassId { get; set; }
    }

    public class ChangePasswordInput
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal CompanyId { get; set; }
        public string PrevPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ForgetPasswordInput
    {
        public string UserName { get; set; }
    }
    public class LazyLoadingSearchInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }

    }

    public class ListEmployeesInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }
      
    }

    public class ListEmployeesInputV2
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }
        public decimal Filter { get; set; }

    }

    public class ListEmployees
    {
        public Int32 RowId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string InActive { get; set; }
        public string InActiveDate { get; set; }
        public string PlanName { get; set; }
        public decimal CompanyPlanId { get; set; }
        public bool IsDeleteable { get; set; }
    }

    public class PaymentOverview
    {
        public decimal InitialPayment { get; set; }
        public decimal RecurringPayment { get; set; }
    }
    public class AddEditCompanyFinancialDetailsInput
    {
        public decimal CompFinancialDetailId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string DbCon { get; set; }
        public string BankName { get; set; }
        public string InstitutionNumber { get; set; }
        public string TransitNumber { get; set; }
        public string AccountNumber { get; set; }
        public bool IsCheque { get; set; }
        public string FileName { get; set; }
        public decimal InitialPayment { get; set; }
        public decimal RecurringPayment { get; set; }
        public bool IsAgree { get; set; }
        public decimal CompanyId { get; set; }

    }
    public class AddEditAdminPAPDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal CompFinancialDetailId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string DbCon { get; set; }
        public string BankName { get; set; }
        public string InstitutionNumber { get; set; }
        public string TransitNumber { get; set; }
        public string AccountNumber { get; set; }
        public bool IsCheque { get; set; }
        public string FileName { get; set; }
        public decimal InitialPayment { get; set; }
        public decimal RecurringPayment { get; set; }
        public bool IsAgree { get; set; }
        public decimal CompanyId { get; set; }
    }
    public class MultipleAccounts
    {
        public string UserType { get; set; }
        public decimal CompanyUserId { get; set; }
    }

    public class TaxDetails
    {
        public decimal AdminMinFeePct { get; set; }
        public decimal AdminMaxFeePct { get; set; }
        public decimal BrokerMinFeePct { get; set; }
        public decimal BrokerMaxFeePct { get; set; }
        public decimal RSTax { get; set; }
        public decimal HST { get; set; }
        public decimal GST { get; set; }
        public decimal PPTax { get; set; }
        public decimal BrokerComissionCharges { get; set; }

    }

    public class ClasswisePayments
    {
        public decimal PlanId { get; set; }
        public string PlanName { get; set; }
        public decimal NoOfEmployees { get; set; }
        public decimal PlanGrossInitial { get; set; }
        public decimal PlanGrossRecurring { get; set; }
        public string Color { get; set; }

    }

    public class InActiveEmployeeInput
    {
        public bool IsActive { get; set; }
        public decimal CompanyUserId { get; set; }
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class UpdateCompanyDetailsInput
    {
        public decimal UserId { get; set; }
        public decimal CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string OwnerContactNo { get; set; }
        public string OwnerEmail { get; set; }
        public string ServiceUrl { get; set; }
        public string HostUrl { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
     
    }

    public class DeleteUserInput
    {
        public decimal UserId { get; set; }
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }

    }



    public class UpdatePlanDetailsInput
    {
        public decimal LoggedInUserId { get; set; }
        public string DbCon { get; set; }
        public decimal PlanId { get; set; }
        public string PlanStartDt { get; set; }
        public string Name { get; set; }
        public int ContributionSchedule { get; set; }
        public decimal RecurringDeposit { get; set; }
        public decimal InitialDeposit { get; set; }
        public decimal NetDeposit { get; set; }
        public decimal AdministrationInitialFee { get; set; }
        public decimal AdministrationMonthlyFee { get; set; }
        public decimal HSTInitial { get; set; }
        public decimal HSTMonthly { get; set; }
        public decimal PremiumTaxInitial { get; set; }
        public decimal PremiumTaxMonthly { get; set; }
        public decimal RetailSalesTaxInitial { get; set; }
        public decimal RetailSalesTaxMonthly { get; set; }
        public decimal GrossTotalInitial { get; set; }
        public decimal GrossTotalMonthly { get; set; }
        public decimal PlanDesign { get; set; }
        public decimal RollOver { get; set; }
        public string CarryForwardPeriod { get; set; }
        public decimal Termination { get; set; }
    }
    public class UpdatePaymentDateInput
    {
        public string DbCon { get; set; }
        public string PaymentDate { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CListOfClaimsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal ClaimedAmt { get; set; }
        public bool IsReceipt { get; set; }
        public List<ListOfClaimsData> ListOfClaimsData { get; set; }
    }

    public class ListOfClaimsData
    {
        public decimal EmpClaimId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string ReceiptDate { get; set; }
        public string ReceiptNo { get; set; }
        public string UploadFileName { get; set; }
        public decimal ClaimAmt { get; set; }
        public decimal ReimburseableAmt { get; set; }
        public string Dependants { get; set; }
        public string ClainStatus { get; set; }
        public string ClaimDate { get; set; }
        public decimal CRA { get; set; }
        public decimal NonCRA { get; set; }  
        public bool IsManual { get; set; }
        public string Note { get; set; }
        public bool IsExpired { get; set; }
        public ClaimServiceDetails ClaimServiceDetails { get; set; }

    }

    public class ClaimServiceDetails
    {
        public string ServiceName { get; set; }
        public string ServiceProviderId { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string ServiceProvider { get; set; }
    }

    public class SwitchToAnotherAccountsInput
    {
        public decimal CompanyUserId { get; set; }
        public string DbCon { get; set; }
    }

    public class SaveCompanyReferralsInput
    {
        public decimal CompanyId { get; set; }
        public decimal CompanyUserId { get; set; }
        public decimal ReferralTypeId { get; set; }
        public string Name { get; set; }
        public string DbCon { get; set; }
        public decimal BrokerId { get; set; }
        public string OtherDetails { get; set; }
    }

    public class PopulateReferralsData
    {
        public decimal ReferralTypeId { get; set; }
        public string ReferralType { get; set; }
    }

    public class CompanyReferral
    {
        public decimal CompanyUserId { get; set; }
        public decimal ReferralTypeId { get; set; }
        public string ReferralTypeName { get; set; }
        public string Name { get; set; }
        public decimal BrokerId { get; set; }
        public string OtherDetails { get; set; }
    }
    public class CompReferralData
    {
        public string ReferralType { get; set; }
        public string Name { get; set; }
        public string OtherDetails { get; set; }
        public string BrokerName { get; set; }
        public decimal ReferralTypeId { get; set; }
        public decimal BrokerId { get; set; }
    }

    public class CResponseGetCompanyHistoryLog
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<HistoryLog> HistoryData { get; set; }
    }
    public class HistoryLog
    {
        public decimal historyId { get; set; }
        public string Text { get; set; }
        public string DateCreated { get; set; }
    }

    public class GetCompanyHistoryLogInput
    {
        public decimal CompanyId { get; set; }
        public decimal ChunckStart { get; set; }
        public Int32 ChunckSize { get; set; }
        public Int32 Year { get; set; }
        public Int32 Month { get; set; }

    }

    public class GetCompDashboardFigures
    {
        public decimal AmountDue { get; set; }
        public decimal AvailableCredits { get; set; }
        public decimal TotalMembers { get; set; }
        public decimal TotalClaimed { get; set; }
        public decimal TotalAdmin { get; set; }

    }

    public class GetBrokerDashboardDataInput
    {
        public decimal UserId { get; set; }
    }

    public class GetNearbyServiceProviderInput
    {
        public string LatLog { get; set; }
        public string Type { get; set; }
        public decimal UserId { get; set; }
        public string Keyword { get; set; }
        public string DbCon { get; set; }
        public decimal ServiceId { get; set; }
    }

    public class ServiceClaimAmt
    {
        public decimal ServiceId { get; set; }
        public string ServiceName { get; set; }
        public decimal Amount { get; set; }
    }

    public class RecentClaimList
    {
        public string EmpName { get; set; }
        public decimal ClaimAmt { get; set; }
        public decimal PctPremium { get; set; }
    }

    public class GetCardData
    {
        public decimal AccountBalance { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsActive { get; set; }
    }

    public class LogOutInput
    {
        public string DbCon { get; set; }
        public string FCMToken { get; set; }
        public decimal CompanyUserId { get; set; }
        public decimal DeviceTypeId { get; set; }
        public string DeviceId { get; set; }
    }

    public class AddGetInTouchInput
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Messages { get; set; }
    }


    public class UpdateFCMInput
    {
        public decimal UserId { get; set; }
        public string DeviceId { get; set; }
        public decimal DeviceTypeId { get; set; }
        public string NewFCMToken { get; set; }
        public string DbCon { get; set; }
    }

    public class AuthorizeUserInput
    {
        public decimal CompanyId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string Password { get; set; }
    }
}