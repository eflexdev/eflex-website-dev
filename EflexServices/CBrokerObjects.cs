﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CBrokerObjects
    {
    }

    public class AddBrokerInput
    {
        public string FirstName { get; set; } 
	    public string LastName { get; set; } 
	    public string Address1 { get; set; } 
	    public string Address2 { get; set; } 
	    public string City { get; set; } 
	    public string Email	{ get; set; } 
        public decimal ProvinceId { get; set; } 
   	    public string PostalCode { get; set; } 
   	    public string Country { get; set; } 
	    public string MobileNo { get; set; } 
	    public string LandlineNo { get; set; } 
	    public string Licence { get; set; }
        public bool IsAgrement { get; set; }
        public string UserName { get; set; }
        public decimal LoggedInUserId { get; set; }
        public string UploadLicenceFront { get; set; }
        public string GSTNo { get; set; }
        public bool IsPersonal { get; set; }
      
        public decimal MasterUserId { get; set; }
        public string DOB { get; set; }
       public string SocialInsuranceNo { get; set; }
        public string BusinessNo { get; set; }
        public string EOInsuranceNo { get; set; }
	
    }

    public class EditBrokerInput
    {
        public decimal UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string MobileNo { get; set; }
        public string LandlineNo { get; set; }
        public string Licence { get; set; }
        public bool IsAgrement { get; set; }
        public string UserName { get; set; }
        public string DOB { get; set; }
        public string SocialInsuranceNo { get; set; }
        public string BusinessNo { get; set; }
        public string EOInsuranceNo { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public decimal CompanyProvinceId { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyContactPerson { get; set; }
        public string CompanyContactNo { get; set; }
        public string CompanyEmail { get; set; }
        public string UploadLicenceFront { get; set; }
        public string GSTNo { get; set; }
        public bool IsPersonal { get; set; }
       public decimal LoggedInUserId { get; set; }
    }

    public class DeleteBrokerInput
    {
        public decimal Id { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class MarkBrokerAsApprovedInput
    {
        public decimal UserId { get; set; }
        public bool IsApproved { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class RegisterBrokerInput
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string MobileNo { get; set; }
        public string LandlineNo { get; set; }
        public string Licence { get; set; }
        public bool IsAgrement { get; set; }
        public string UserName { get; set; }
        public string DOB { get; set; }
	    public string SocialInsuranceNo { get; set; }
	    public string BusinessNo { get; set; }
	    public string EOInsuranceNo { get; set; }
	    public string CompanyName { get; set; }
	    public string CompanyAddress { get; set; }
	    public string CompanyCity { get; set; }
	    public decimal CompanyProvinceId { get; set; }
	    public string CompanyPostalCode  { get; set; }
	    public string CompanyContactPerson { get; set; }
	    public string CompanyContactNo { get; set; }
        public string CompanyEmail { get; set; }
        public string UploadLicenceFront { get; set; }
        public string GSTNo { get; set; }
        public bool IsPersonal { get; set; }
    }

    public class AddEflexUserPAPDetailsInput
    {
        public decimal UserId { get; set; }
        public string BankName { get; set; }
        public string TransitId { get; set; }
        public string InstitutionId { get; set; }
        public string AccountNumber { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CGetBrokerProfileResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public GetBrokerProfile GetBrokerProfileData { get; set; }
        public GetBrokerFigures GetBrokerFigures { get; set; }
    }
    public class GetBrokerProfile
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string MobileNo { get; set; }
        public string LandlineNo { get; set; }
        public string Licence { get; set; }
        public bool IsAgrement { get; set; }
        public string UserName { get; set; }
        public string DOB { get; set; }
        public string SocialInsuranceNo { get; set; }
        public string BusinessNo { get; set; }
        public string EOInsuranceNo { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public decimal CompanyProvinceId { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyContactPerson { get; set; }
        public string CompanyContactNo { get; set; }
        public string CompanyEmail { get; set; }
        public string GSTNo { get; set; }
        public string UploadLicenceFront { get; set; }
        public string Password { get; set; }
       public FinancialDetails FinancialDetails { get; set; }
       public string IsPersonal { get; set; }
    }

    public class AddBrokerCompanyInput
    {
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public decimal ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string OwnerContactNo { get; set; }
        public string OwnerEmail { get; set; }
        public string ServiceUrl { get; set; }
        public string HostUrl { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal BrokerId { get; set; }
       // public decimal PlanTypeId { get; set; }
        public decimal CommissionCharges { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CListOfBrokerCompaniesResponse
    {
        public bool IsLogOut { get; set; }
       public string Status { get; set; }
        public string Message { get; set; }
        public List<ListOfBrokerCompanies> ListOfBrokerCompanies { get; set; }
       // public decimal CommissionCharges { get; set; }
    }

    public class ListOfBrokerCompanies
    {
        public decimal CompanyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OwnerName { get; set; }
        public string Province { get; set; }
        public string PlanType { get; set; }
        public string DbCon { get; set; }
        public bool IsBlocked { get; set; }
        public decimal OwnerId { get; set; }
        public string IsApproved { get; set; }
        public string CreationDt { get; set; }
        public decimal BrokerFee { get; set; }
        public decimal CommissionCharges { get; set; }
        public List<BlockReason> BlockReason { get; set; }
    }

    public class LazyLoadingInputV2
    {
        public Int32 ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
    }
    public class ListOfCompanyBrokersInput
    {
        public Int32 ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class FinancialDetails
    {
        public decimal UserId { get; set; }
	    public string BankName { get; set; }
	    public string TransitId	 { get; set; }
	    public string InstitutionId	 { get; set; }
        public string AccountNumber { get; set; }
        public decimal UserPAPDetailsId { get; set; }
	
    }

    public class EditEflexUserPAPDetailsInput
    {
        public decimal UserId { get; set; }
        public string BankName { get; set; }
        public string TransitId { get; set; }
        public string InstitutionId { get; set; }
        public string AccountNumber { get; set; }
        public decimal UserPAPDetailId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class UpdateCommissionInput
    {
        public decimal UserId { get; set; }
        public decimal CommissionCharges { get; set; }
    }

    public class CRegisterBrokerResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public dynamic ID { get; set; }
        public string UserToken { get; set; }
    }
    
    public class CAddBrokerCompanyResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string UserToken { get; set; }
        public decimal CompanyId { get; set; }
        public string DbCon { get; set; }
        public TaxDetails TaxDetails { get; set; }
        public decimal CompanyUserId { get; set; }
    }

    public class CGetBrokerDashboardDataResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }

        public BrokerFiguresData BrokerFiguresData { get; set; }
        public List<RecentRegCompany> RecentRegCompany { get; set; }
        public List<ChildBrokersDetails> ChildBrokersDetails { get; set; }
        public GetBrokerGraphData GetBrokerGraphDetails { get; set; }
    }

    public class BrokerFiguresData
    {
        public Int32 TotalCompany { get; set; }
        public Int32 TotalBroker { get; set; }
        public Int32 TotalAmtReceived { get; set; }

    }
    public class ChildBrokersDetails
    {
        public string BrokerName { get; set; }
        public decimal NoOfCompanies { get; set; }
        public decimal TotalComission { get; set; }

       
    }
    public class RecentRegCompany
    {
        public string CompanyName { get; set; }
        public decimal Contribution { get; set; }
        public string Month { get; set; }
        public decimal YourFee { get; set; }
        public decimal TotalComission { get; set; }

    }

    public class GetBrokerProfileInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal Id { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class GetBrokerGraphData
    {
        public List<string> Months { get; set; }
        public List<decimal> NoOfCompanies { get; set; }
        public List<decimal> ComissionAmt { get; set; }
    }
    public class CGetAllBrokerTransactionsResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<GetAllBrokerTransactions> GetAllBrokerTransactions { get; set; }
    }

    public class GetAllBrokerTransactions
    {
        public string TransactionDt { get; set; }
        public decimal Amount { get; set; }
        public decimal RowId { get; set; }
        public string Description { get; set; }
        public decimal PaymentStatus { get; set; }
        public string PaymentType { get; set; }
    }

    public class AddBrokerAdjustmentsInput
    {
        public decimal UserId { get; set; }
	    public decimal Amount { get; set; }
	    public string AdjustmentDT { get; set; }
	    public string Remarks { get; set; }
	    public decimal TypeId { get; set; }//,--1   Credit,2    Debit
       public decimal LoggedInUserId { get; set; }
    }

    public class GetBrokerFigures
    {
        public decimal TotalCompanies { get; set; }
        public decimal TotalAmountReceived { get; set; }
        public decimal ToTalDueAmt { get; set; }
    }

    public class LazyLoadingInput1
    {
        public Int32 ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CListBrokerPaymentResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListBrokerPayment> PaymentData { get; set; }
    }
    public class ListBrokerPayment
    {
        public decimal UserId { get; set; }
        public decimal RowId { get; set; }
        public decimal BrokerPaymentId { get; set; }
        public string CompanyName { get; set; }
        public string PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentDetails { get; set; }
        public string BrokerName { get; set; }
        public decimal Contribution { get; set; }
        public decimal PercentFee { get; set; }
        public string PaymentDueDate { get; set; }
        public GetPAPDetails GetPAPDetails { get; set; }
    }

    public class BrokerPaymentInput
    {
        public Int32 ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class SaveBrokerReferralInput
    {
        public decimal BrokerReferralId { get; set; }	
        public decimal UserId { get; set; }				
        public string CompanyName { get; set; }			
        public string CompanyWebsite { get; set; }		
        public string Industry { get; set; }			
        public decimal NoOfEmployees { get; set; }	    
        public string HaveGroupBenefits { get; set; }	
        public string ContactName { get; set; }			
        public string ContactTitle { get; set; }		
        public string EmailAddress { get; set; }		
        public string Phone { get; set; }
        public string ReferralFile { get; set; }	
			
     		
    }

    public class SavePreferredSlotsInput
    {
        public decimal BrokerReferralId { get; set; }
        public List<PreferredSlots> PreferredSlots { get; set; }
    }

    public class PreferredSlots
    {
        public decimal PreferredSlotId { get; set; }

        public string PreferredDateTime { get; set; }
        public string Location  { get; set; }
        public string Attendents { get; set; }
        public string Notes { get; set; }
        public bool IsFinal { get; set; }
        public Int32 Status { get; set; }
    }

    public class UpdatePreferredSlotsInput
    {
        public decimal PreferredSlotId { get; set; }
        public string PreferredDateTime { get; set; }
        public string Location { get; set; }
        public string Attendents { get; set; }
        public string Notes { get; set; }
        public decimal BrokerReferralId { get; set; }
    }

    public class CListOfBrokerReferralsResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<BrokerReferralData> BrokerReferralData { get; set; }
    }
    public class BrokerReferralData
    {
        public decimal BrokerReferralId { get; set; }
        public decimal UserId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyWebsite { get; set; }
        public string Industry { get; set; }
        public decimal NoOfEmployees { get; set; }
        public string HaveGroupBenefits { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string ReferralFile { get; set; }

        public decimal Status { get; set; }
    }

    public class CGetPreferredSlotsByIdResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public int BrokerReferralStatus { get; set; }
        public List<PreferredSlots> PreferredSlotsList { get; set; }
    }

    public class MarkDoneBrokerReferralInput
    {
        public decimal BrokerReferralId { get; set; }
        public Int32 Status { get; set; }
    }
}