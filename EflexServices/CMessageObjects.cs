﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CMessageObjects
    {
    }
    public class CSendMessageResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public dynamic ID { get; set; }
        public List<ConvMsgs> ConversationMessages { get; set; }
    }
    public class ConvMsgs
    {
        public decimal MsgId { get; set; }
        public string Msg { get; set; }
        public string MsgFile { get; set; }
        public string FileType { get; set; }
        public string MsgDate { get; set; }
        public string SenderName { get; set; }
        public string SenderPic { get; set; }
        public int SenderUserTypeId { get; set; }
        public bool AmISender { get; set; }
    }

    public class MessageInputInfo
    {
        public string DbCon { get; set; }
        public string Msg { get; set; }
        public decimal CId { get; set; }
        public decimal SenderId { get; set; }
        public decimal RecipientId { get; set; }
        public int SenderUserTypeId { get; set; }
        public int RecipientUserTypeId { get; set; }
        public decimal SenderCompanyId { get; set; }
        public decimal RecipientCompanyId { get; set; }
        public string MsgFile { get; set; }
        public string MsgFileType { get; set; }
        public decimal LatestMsgId { get; set; }
    }
    public class OtherUser1
    {
        public decimal Id { get; set; }
        public decimal UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Pic { get; set; }
        public int UserTypeId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal Cid { get; set; }
        public LatestMsgDetails LatestMsgObj { get; set; }
        public string ReceipientName { get; set; }
        public string CompanyName { get; set; }
        public decimal UnreadCount { get; set; }
        public string LatestMessageDate { get; set; }
        
    }

    public class LatestMsgDetails
    {
        public string LatestMsg { get; set; }
        public string LatestMsgDate { get; set; }

    }
    public class RecentConvInputInfo
    {
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal CompanyId { get; set; }
        public string DbCon { get; set; }
    }

    public class CRecentConversationResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<Conversation> RecentConversations { get; set; }
    }

    public class Conversation
    {
        public decimal CId { get; set; }
        public string SenderName { get; set; }
        public string SenderPic { get; set; }
        public int SenderUserTypeId { get; set; }
        public string LatestMsg { get; set; }
        public string LatestMsgDate { get; set; }
        public string ReceipientName { get; set; }
        public OtherUser OtherUserDetails { get; set; }
        public List<ConvMsgs> Messages { get; set; }
    }

    public class OtherUser
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Pic { get; set; }
        public int UserTypeId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CAllConversationResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<Conversation> Conversations { get; set; }
    }
    public class ConvInputInfo
    {
        public string DbCon { get; set; }
        public decimal CId { get; set; }
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal CompanyId { get; set; }
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
    }

    public class CConvMessagesResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ConvMsgs> ConversationMessages { get; set; }
    }

    public class ConvInputAjaxInfo
    {
        public string DbCon { get; set; }
        public decimal CId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal LatestMsgId { get; set; }
    }
    public class CFreshConversationResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public decimal CId { get; set; }
        public List<Conversation> Conversations { get; set; }
    }

    public class UserConvInputInfo
    {
        public decimal LoggedInUserId { get; set; }
        public decimal LoggedInUserTypeId { get; set; }
        public decimal SelectedUserId { get; set; }
        public decimal SelectedUserTypeId { get; set; }
        public decimal SelectedUserCompanyId { get; set; }
        public decimal CompanyId { get; set; }
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string DbCon { get; set; }
    }

    public class DeleteMessageInput
    {
        public decimal CompanyId { get; set; }
        public decimal LoggedinUserId { get; set; }
        public decimal MessageId { get; set; }
        public string DbCon { get; set; }
    }

    public class SendBroadcastMessageInput
    {
        public string DbCon { get; set; }
        public string Msg { get; set; }
        public decimal SenderId { get; set; }
        public int SenderUserTypeId { get; set; }
        public decimal SenderCompanyId { get; set; }
        public List<RecipientData> RecipientData { get; set; }
    }

    public class RecipientData
    {
        public decimal RecipientId { get; set; }
        public int RecipientUserTypeId { get; set; }
        public decimal RecipientCompanyId { get; set; }
        public decimal CId { get; set; }
    }

    public class ListAllCompanyUsersInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal LoggedInUserTypeId { get; set; }
    }
    public class CListAllCompanyUsersResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<OtherUser1> OtherUser1 { get; set; }
    }

    public class ListAllEflexCompanyUsersInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal LoggedInUserTypeId { get; set; }
      
    }

    public class EflexMessageInputInfo
    {
        public string Msg { get; set; }
        public decimal CId { get; set; }
        public decimal SenderId { get; set; }
        public decimal RecipientId { get; set; }
        public int SenderUserTypeId { get; set; }
        public int RecipientUserTypeId { get; set; }
        public decimal SenderCompanyId { get; set; }
        public decimal RecipientCompanyId { get; set; }
        public string MsgFile { get; set; }
        public string MsgFileType { get; set; }
        public decimal LatestMsgId { get; set; }
    }

    public class ConvEflexInputInfo
    {
        public decimal CId { get; set; }
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
    }
    public class RecentConvEflexInputInfo
    {
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
       
    }
    public class ConvEflexInputAjaxInfo
    {
        public decimal CId { get; set; }
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal LatestMsgId { get; set; }
    }
    public class EflexUserConvInputInfo
    {
        public decimal LoggedInUserId { get; set; }
        public decimal LoggedInUserTypeId { get; set; }
        public decimal SelectedUserId { get; set; }
        public decimal SelectedUserTypeId { get; set; }
        public decimal SelectedUserCompanyId { get; set; }
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
      
    }
}