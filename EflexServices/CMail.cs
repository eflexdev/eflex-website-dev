﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Configuration;

namespace EflexServices
{
    public class CMail
    {
        #region Properties
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string Subject { get; set; }
        public string MessageBody { get; set; }
        public string GodaddyUserName { get; set; }
        public string GodaddyPassword { get; set; }
        public string Server { get; set; }
        public decimal CompanyId { get; set; }
        public string ErrorMessage { get; set; }
        public Int32 Port { get; set; }
        public List<string> AttachmentFiles { get; set; }
   
        #endregion
        public static string WWDateFormat = "MM/dd/yyyy HH:mm:ss";

        #region SendMail
        public bool SendEMail(MasterDBDataContext datacontext, bool IsAutoEmail)
        {
           bool Status = false;
            bool IsAttachments = false;
            string ErrorMessage = "";
            using (System.Net.Mail.MailMessage emailMessage = new System.Net.Mail.MailMessage())
            {
                SmtpClient smtp = null;
                try
                {

                    emailMessage.From = new MailAddress(GodaddyUserName,"Eflex");
                    emailMessage.To.Add(EmailTo);

                    if (!String.IsNullOrEmpty(EmailCC) )
                        emailMessage.CC.Add(EmailCC);

                    if (!String.IsNullOrEmpty(EmailBCC))
                        emailMessage.Bcc.Add(EmailBCC);

                    emailMessage.Subject = Subject;
                    emailMessage.Body = MessageBody;
                    emailMessage.IsBodyHtml = true;

                    if (AttachmentFiles != null && AttachmentFiles.Count>0)
                    {
                        Attachment attachment = null;
                        foreach(string file in AttachmentFiles)
                        {
                            attachment = new Attachment(file, MediaTypeNames.Application.Octet);
                            emailMessage.Attachments.Add(attachment);
                        }
                        IsAttachments = true;
                    }
                    smtp = new SmtpClient(Server, Port);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new System.Net.NetworkCredential(GodaddyUserName, GodaddyPassword);
                    smtp.EnableSsl = true ;
                    smtp.Send(emailMessage);
                    Status = true;
                }
                catch (Exception ex)
                {
                    Status = false;
                    ErrorMessage = ex.Message;
                    if(ex.InnerException != null)
                        ErrorMessage += " "+ex.InnerException.Message;
                    //throw new Exception("Send Email Failed." + ex.Message);
                }
                finally
                {
                    if (smtp != null)
                        smtp.Dispose();
                }
                if (IsAutoEmail)
                {
                    SaveSentEmailDetails(datacontext, EmailTo, EmailCC, EmailBCC, Subject, MessageBody, IsAttachments, Status, AttachmentFiles, ErrorMessage,CompanyId);
                }
                    return Status;
            }
        }

        public bool SendEMail(EflexDBDataContext datacontext, bool IsAutoEmail)
        {
            bool Status = false;
            bool IsAttachments = false;
            string ErrorMessage = "";
            using (System.Net.Mail.MailMessage emailMessage = new System.Net.Mail.MailMessage())
            {
                SmtpClient smtp = null;
                try
                {

                    emailMessage.From = new MailAddress(GodaddyUserName, "Eflex");
                    emailMessage.To.Add(EmailTo);

                    if (!String.IsNullOrEmpty(EmailCC))
                        emailMessage.CC.Add(EmailCC);

                    if (!String.IsNullOrEmpty(EmailBCC))
                        emailMessage.Bcc.Add(EmailBCC);

                    emailMessage.Subject = Subject;
                    emailMessage.Body = MessageBody;
                    emailMessage.IsBodyHtml = true;

                    if (AttachmentFiles != null && AttachmentFiles.Count > 0)
                    {
                        Attachment attachment = null;
                        foreach (string file in AttachmentFiles)
                        {
                            attachment = new Attachment(file, MediaTypeNames.Application.Octet);
                            emailMessage.Attachments.Add(attachment);
                        }
                        IsAttachments = true;
                    }
                    smtp = new SmtpClient(Server, Port);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new System.Net.NetworkCredential(GodaddyUserName, GodaddyPassword);
                    smtp.EnableSsl = true;
                    smtp.Send(emailMessage);
                    Status = true;
                }
                catch (Exception ex)
                {
                    Status = false;
                    ErrorMessage = ex.Message;
                    if (ex.InnerException != null)
                        ErrorMessage += " " + ex.InnerException.Message;
                    //throw new Exception("Send Email Failed." + ex.Message);
                }
                finally
                {
                    if (smtp != null)
                        smtp.Dispose();
                }
                if (IsAutoEmail)
                {
                    SaveSentEmailDetails(datacontext, EmailTo, EmailCC, EmailBCC, Subject, MessageBody, IsAttachments, Status, AttachmentFiles, ErrorMessage, CompanyId);
                }
                return Status;
            }
        }              
        #endregion

        #region SaveSentEmailDetails
        //JK:051117
        private void SaveSentEmailDetails(MasterDBDataContext datacontext, string EmailTo, string EmailCC, string EmailBCC, string Subject, string MessageBody, bool IsAttachments, bool Status, List<string> AttachmentFiles, string ErrorMessage,decimal CompanyId)
        {
            try
            {
                    TblAutoEmailLog Obj = new TblAutoEmailLog()
                    {
                        EmailTo = EmailTo,
                        EmailCC = EmailCC,
                        EmailBCC = EmailBCC,
                        EmailSub = Subject,
                        CompanyId = CompanyId,
                        EmailMsg = MessageBody,
                        IsAttachments = IsAttachments,
                        IsSentSuccess = Status,
                        ErrorMessage = ErrorMessage,
                        DateCreated = DateTime.UtcNow,
                        Active = true
                    };
                    datacontext.TblAutoEmailLogs.InsertOnSubmit(Obj);
                    datacontext.SubmitChanges();
                    if (AttachmentFiles != null && AttachmentFiles.Count > 0)
                    {
                        IsAttachments = true;
                        foreach (string file in AttachmentFiles)
                        {
                            TblAutoEmailAttachment attachments = new TblAutoEmailAttachment()
                            {
                                AutoEmailLogId = Obj.AutoEmailLogId,
                                Name = file,
                                FileType = file.Substring(file.LastIndexOf(".")+1),
                                DateCreated = Convert.ToDateTime(DateTime.UtcNow.ToString(WWDateFormat)),
                                Active = true
                            };
                            datacontext.TblAutoEmailAttachments.InsertOnSubmit(attachments);
                            datacontext.SubmitChanges();
                        }
                    }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion SaveSentEmailDetails

        #region SaveSentEmailDetails
        //JK:051117
        private void SaveSentEmailDetails(EflexDBDataContext datacontext, string EmailTo, string EmailCC, string EmailBCC, string Subject, string MessageBody, bool IsAttachments, bool Status, List<string> AttachmentFiles, string ErrorMessage, decimal CompanyUserId)
        {
            try
            {
                TblCompanyAutoEmailLog Obj = new TblCompanyAutoEmailLog()
                {
                    EmailTo = EmailTo,
                    EmailCC = EmailCC,
                    EmailBCC = EmailBCC,
                    EmailSub = Subject,
                    CompanyUerId = CompanyUserId,
                    EmailMsg = MessageBody,
                    IsAttachments = IsAttachments,
                    IsSentSuccess = Status,
                    ErrorMessage = ErrorMessage,
                    DateCreated = DateTime.UtcNow,
                    Active = true
                };
                datacontext.TblCompanyAutoEmailLogs.InsertOnSubmit(Obj);
                datacontext.SubmitChanges();
               

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion SaveSentEmailDetails

    }
}