﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CEmployeeObjects
    {
    }

    public class ImportUsersExcelInput
    {
        public string FileName { get; set; }
        public string DbCon { get; set; }
        public decimal CompanyId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class AddAdminInput
    {
        public string DbCon { get; set; }  
        public string FName { get; set; }
        public string LName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CGetEmpDashboardTransactionsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal AmountDue { get; set; }
        public List<EmpPendingTransaction> PendingTransaction { get; set; }
        public List<EmpCurrentTransaction> CurrentTransaction { get; set; }

    }
    public class EmpPendingTransaction
    {
        public string Date { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public string TypeId { get; set; }
        public decimal ClaimId { get; set; }
        public string ClaimReceipt { get; set; }

    }

    public class EmpCurrentTransaction
    {
        public string Date { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public string TypeId { get; set; }
        public decimal ClaimId { get; set; }
        public string ClaimReceipt { get; set; }
    
    }
    public class CGetEmpTransactionDataResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public AmountDetails AmountDetails { get; set; }
        public List<EmpTransactionList> EmpTransactionList { get; set; }

    }
    public class AmountDetails
    {
        public decimal CurrentBalance { get; set; }
        public decimal Contributions { get; set; }
        public decimal Withdrawal { get; set; }
    }
    public class EmpTransactionList
    {
        public string TransactionDate { get; set; }
        public string ContributionType { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string TypeId { get; set; }
        public decimal PaymentStatus { get; set; }
    }

    public class GetEmpTransactionInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }
        public string Period { get; set; }
        public decimal UserId { get; set; }

    }

    public class CGetEmpDashboardDataResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string PlanName { get; set; }
        public decimal PlanDesign { get; set; }
        public List<GetEmpDashboardData> GetEmpDashboardData { get; set; }
    }
    public class GetEmpDashboardData
    {
        public string Type { get; set; }
        public decimal Amount { get; set; }
     
    }

    public class PaymentType
    {
        public string Title { get; set; }
        public string Abbrievation { get; set; }
    }
    public class CGetEmpDetailsByIdResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public GetEmpBasicDetails GetEmpBasicDetails { get; set; }
    }

    public class GetEmpBasicDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
	    public string EmailAddress { get; set; }
	    public string DOB { get; set; }
	    public string Gender { get; set; }
	    public string MobileNo { get; set; }
	    public string EveningPhone { get; set; }
	    public string MorningPhone { get; set; }
	    public string Address { get; set; }
	    public string Address2 { get; set; }
	    public string City { get; set; }
        public string Province { get; set; }
   	    public string PostalCode { get; set; }
	    public bool IsActive { get; set; }
        public string InActiveDate { get; set; }
        public string UserName { get; set; }
        public CellNotificationDetails CellNotificationDetails { get; set; }
    }

    public class CellNotificationDetails
    {
        public string ContactNo { get; set; }
        public string CarrierName { get; set; }
        public string IsNotify { get; set; }
    }

    public class UpdateEmpBasicDetailsInput
    {
        public string DbCon { get; set; }
        public decimal CompanyUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string MobileNo { get; set; }
        public string EveningPhone { get; set; }
        public string MorningPhone { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Carrier { get; set; }
        public string IsNotify { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public decimal Sin { get; set; }
    }

    public class AddEmpDependentsInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public List<EmpDependentList> EmpDependentList { get; set; }
    }
    public class EmpDependentList
    {
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public decimal RelationshipId { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string OtherRelation { get; set; }

    }

    public class CGetEmpDependentDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public EmpDependentsListDetails EmpDependentsListDetails { get; set; }
    }

    public class EmpDependentsListDetails
    {
        public decimal DependentId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public decimal RelationshipId { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string OtherRelation { get; set; }

    }

    public class EditEmpDependentsInput
    {
        public decimal DependentId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public decimal RelationshipId { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string DbCon { get; set; }
        public string OtherRelation { get; set; }

    }

    public class CListOfDependentsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListOfDependents> ListOfDependents { get; set; }

    }

    public class ListOfDependents
    {
        public decimal DependentId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Relationship { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string OtherRelation { get; set; }
        public decimal RelationShipId { get; set; }

    }

    public class CPopulateDependantsResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<PopulateDependants> PopulateDependants { get; set; }

    }

   
    public class PopulateDependants
    {
        public decimal DependantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    public class CPopulateClaimTypeResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<PopulateClaimType> PopulateClaimType { get; set; }
    }

    public class PopulateClaimType
    {
        public decimal ClaimTypeId { get; set; }
        public string ClaimType { get; set; }
    }

    public class AddClaimInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string ServiceProvider { get; set; }
        public string ReceiptDate { get; set; }
        public string ReceiptNo { get; set; }
        public string UploadFileName { get; set; }
        public decimal Amount { get; set; }
        public decimal ClaimAmount { get; set; }
        public decimal Dependant { get; set; }
        public string Note { get; set; }
        public decimal ServiceProviderId { get; set; }
        public string ProviderAddress { get; set; }
        public bool IsManual { get; set; }
        public decimal ServiceId { get; set; }
        public string ProviderEmail { get; set; }
        public string ProviderContactNo { get; set; }
        public string GoogleUniqueId { get; set; }
    }

    public class AddClaimList
    {
        public decimal CompanyUserId { get; set; }
        public decimal ClaimTypeId { get; set; }
        public string OtherClaimType { get; set; }
        public string ServiceProvider { get; set; }
        public string ReceiptDate { get; set; }
        public string ReceiptNo { get; set; }
        public string UploadFileName { get; set; }
        public decimal Amount { get; set; }
        public decimal ClaimAmt { get; set; }
        public decimal DependantId { get; set; }
        public string Note { get; set; }
    }
    public class ListOfClaimsInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class AddUserCarrierInput
    {
        public string DbCon { get; set; }
        public string Carrier { get; set; }
        public decimal CarrierId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public bool IsSMS { get; set; }
        public string ContactNo { get; set; }
        public int NotifyType { get; set; }// 1 yes, 2 No, 3 AskmeLater
    }

    public class CAddUserCarrierResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal Id { get; set; }
    }

    public class CPopulateBrokersResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<PopulateBrokers> PopulateBrokers { get; set; }

    }

    public class PopulateBrokers
    {
        public string Name { get; set; }
        public decimal BrokerId { get; set; }
    }

    public class AddEmpAdjustmentsInput
    {
        public decimal CompanyUserId { get; set; }
	    public decimal Amount { get; set; }
	    public string AdjustmentDT { get; set; }
	    public string Remarks { get; set; }
	    public decimal TypeId { get; set; }//,--1   Credit,2    Debit
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class ListEmpAllTransactionsInput
    {
        public int ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public string FromDt { get; set; }
        public string ToDt { get; set; }
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompUserId { get; set; }
    }


    public class CListEmpAllTransactionsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<EmpAllTransactionsData> EmpAllTransactionsData { get; set; }
    }

    public class EmpAllTransactionsData
    {
        public decimal RowId { get; set; }
        public decimal TransactionId { get; set; }
        public string EmpName { get; set; }
        public string TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public string TransactionDetails { get; set; }
        public decimal PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string Type { get; set; }
    }

    public class GetEmpCardList
    {
        public decimal MasterCardId { get; set; }
        public string SessionId { get; set; }
	    public string UniqueId { get; set; }
	    public string OrderItemId { get; set; }
	    public string OrderId { get; set; }
	    public string AccountId { get; set; }
	    public string CardId { get; set; }
	    public string CardHolderId { get; set; }
	    public string CountryCode { get; set; }
	    public string CountrySubDivisionCode { get; set; }
	    public string FourthLine { get; set; }
	    public decimal UserId { get; set; }
	    public decimal CompanyId { get; set; }
	    public decimal CourierShipping { get; set; }
	    public decimal Quantity { get; set; }
        public string IsActive { get; set; }
        public string BlockDate { get; set; }
        public string DateCreated { get; set; }
        public string CardStatus { get; set; }
        public decimal CVV { get; set; }
        public decimal ExpMonth { get; set; }
        public decimal ExpYear { get; set; }
        public string CardNumber { get; set; }
        public string BlockReason { get; set; }
        public string BlockDT { get; set; }
        public string EmpName { get; set; }
        public decimal LastFourDigit { get; set; }
        public string ExpiryDt { get; set; }
        public AccountSummary AccountSummaryData { get; set; }
    }


    public class GetEmpCardDetails
    {
        public decimal MasterCardId { get; set; }
        public string FourthLine { get; set; }
        public string CardStatus { get; set; }
        public decimal LastFourDigit { get; set; }
        public AccountSummary AccountSummaryData { get; set; }

       
    }

    public class ValidatePasscodeInput
    {
        public string OTP { get; set; }
        public decimal UserId { get; set; }
        public string DbCon { get; set; }
    }

    public class DeleteEmployeeInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyUserId { get; set; }
        public string DbCon { get; set; }
    }

    public class CGetEmpDashboardDataForAppResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string PlanName { get; set; }
        public decimal PlanDesign { get; set; }
        public List<GetEmpDashboardData> GetEmpDashboardData { get; set; }
      
        public bool AssignNewCard { get; set; }
        public List<GetEmpCardDetails> CardData { get; set; }
    }


    public class UpdateDependantInput
    {
        public string DbCon { get; set; }
        public decimal DependantId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public decimal RelationshipId { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string OtherRelation { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class SendMailToUnUpdatedUserInput
    {
        public int CompanyId { get; set; }
        public List<int> CompanyUserIds { get; set; }
    }
}