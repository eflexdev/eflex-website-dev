﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    public partial interface IEflexService
    {
        #region SendMessage
        [WebInvoke(UriTemplate = "SendMessage", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CSendMessageResponse SendMessage(MessageInputInfo iObj);
        #endregion SendMessage

        #region ListRecentConversations
        [WebInvoke(UriTemplate = "ListRecentConversations", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CRecentConversationResponse ListRecentConversations(RecentConvInputInfo iObj);
        #endregion ListRecentConversations

        #region ListAllConversations
        [WebInvoke(UriTemplate = "ListAllConversations", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAllConversationResponse ListAllConversations(ConvInputInfo iObj);
        #endregion ListAllConversations

        #region ListAllConversationMessages
        [WebInvoke(UriTemplate = "ListAllConversationMessages", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CConvMessagesResponse ListAllConversationMessages(ConvInputInfo iObj);
        #endregion ListAllConversationMessages

        #region ListAllConversationMessagesAjax
        [WebInvoke(UriTemplate = "ListAllConversationMessagesAjax", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CConvMessagesResponse ListAllConversationMessagesAjax(ConvInputAjaxInfo iObj);
        #endregion ListAllConversationMessagesAjax

        #region ListConversationDetailsWithUser
        [WebInvoke(UriTemplate = "ListConversationDetailsWithUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CFreshConversationResponse ListConversationDetailsWithUser(UserConvInputInfo iObj);
        #endregion ListConversationDetailsWithUser
    
        #region SendBroadcastMessage
        [WebInvoke(UriTemplate = "SendBroadcastMessage", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SendBroadcastMessage(SendBroadcastMessageInput iObj);
        #endregion SendBroadcastMessage

        #region DeleteMessage
        [WebInvoke(UriTemplate = "DeleteMessage", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteMessage(DeleteMessageInput iObj);
        #endregion DeleteMessage

        #region ListAllCompanyUsers
        [WebInvoke(UriTemplate = "ListAllCompanyUsers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListAllCompanyUsersResponse ListAllCompanyUsers(ListAllCompanyUsersInput iObj);
        #endregion ListAllCompanyUsers

        #region ListAllEflexCompanyUsers
        [WebInvoke(UriTemplate = "ListAllEflexCompanyUsers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListAllCompanyUsersResponse ListAllEflexCompanyUsers(ListAllEflexCompanyUsersInput Obj);
        #endregion ListAllEflexCompanyUsers

        #region SendMessageByEflex
        [WebInvoke(UriTemplate = "SendMessageByEflex", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CSendMessageResponse SendMessageByEflex(EflexMessageInputInfo iObj);
        #endregion SendMessageByEflex

        #region ListAllConversationMessagesEflex
        [WebInvoke(UriTemplate = "ListAllConversationMessagesEflex", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CConvMessagesResponse ListAllConversationMessagesEflex(ConvEflexInputInfo Obj);
        #endregion ListAllConversationMessagesEflex

        #region ListEflexRecentConversations
        [WebInvoke(UriTemplate = "ListEflexRecentConversations", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CRecentConversationResponse ListEflexRecentConversations(RecentConvEflexInputInfo iObj);
        #endregion ListEflexRecentConversations

        #region ListAllEflexConversations
        [WebInvoke(UriTemplate = "ListAllEflexConversations", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAllConversationResponse ListAllEflexConversations(ConvEflexInputInfo iObj);
        #endregion ListAllEflexConversations

        #region ListAllEflexConversationMessagesAjax
        [WebInvoke(UriTemplate = "ListAllEflexConversationMessagesAjax", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CConvMessagesResponse ListAllEflexConversationMessagesAjax(ConvEflexInputAjaxInfo Obj);
        #endregion ListAllEflexConversationMessagesAjax

        #region ListConversationDetailsWithEflexUser
        [WebInvoke(UriTemplate = "ListConversationDetailsWithEflexUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CFreshConversationResponse ListConversationDetailsWithEflexUser(EflexUserConvInputInfo Obj);
        #endregion ListConversationDetailsWithEflexUser

        #region ListAllAdminUserForMsg
        [WebInvoke(UriTemplate = "ListAllAdminUserForMsg", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListAdminUserResponse ListAllAdminUserForMsg();
        #endregion ListAllAdminUserForMsg

        #region GetConvBetweenTwoUsers
        [WebInvoke(UriTemplate = "GetConvBetweenTwoUsers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CFreshConversationResponse GetConvBetweenTwoUsers(UserConvInputInfo iObj);
        #endregion GetConvBetweenTwoUsers
    }
}