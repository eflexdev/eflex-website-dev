
   <div id="main">     
     
      <div align="center">
         <table style="border:1px solid #000" width="650" cellspacing="0" cellpadding="0" border="0" bgcolor="" align="center">
            <tbody>
               <tr>
                  <td width="650" valign="center" bgcolor="#FFFFFF" align="center">
                     <p style="margin:0px; background-image:url(https://eflex.app/assets/email-img/headers.png); height: 370px; color: #fff;font-size: 16px;font-weight: bold;padding-top: 50px;">					 
						<img src="https://eflex.app/assets/email-img/logo.png" border="0"><br>
						Redefining Health Benefits in Canada
					 </p>
					 <table width="610" height="" cellspacing="0" cellpadding="0" align="center">
                        <tbody>
							<tr>								
								<td colspan="2" align="center">
									<p style="margin:0px;color: #000;font-size:28px;font-weight: bold;padding-top: 5px;">Welcome </p>
									<img src="https://eflex.app/assets/email-img/line.png" width="150">
								</td>
							</tr>
						</tbody>
					 </table>
					 <br>
                     <table width="610" height="" cellspacing="0" cellpadding="0" align="center">
                        <tbody>							
                           <tr>                            
                              <td style="font-family:Arial,Helvetica,sans-serif;font-size:16px;text-align:left;line-height:1.3">
                                 lstrEmailContent
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <table width="610" height="" cellspacing="0" cellpadding="0" align="center">
                        <tbody>  
							<tr> 
								<td colspan="2" align="center">
									<img src="https://eflex.app/assets/email-img/download-eflex.png" width="150">
								</td>								
							</tr>
							<tr></tr>
							<tr> 
								<td align="center" colspan="2"> 
									<img src="https://eflex.app/assets/email-img/app-icons.png" width="100"/>
								</td>
							<tr> 
							<br>
							<tr> 
								<td align="center" colspan="2"> 
									<p style="margin:0px;color: #000;font-size:24px;font-weight: bold;">
										Coming Soon </p> 
								</td>
							<tr> 
							<br>
                           <tr>                           
							   <td align="right">
                                 <a href="https://play.google.com/store/apps/details?id=com.eflex&hl=en" target="_blank" > <img src="https://eflex.app/assets/email-img/play-store.png" width="150"> </a>
                              </td>
							   <td align="left">
                                  <a href="https://play.google.com/store/apps/details?id=com.eflex&hl=en" target="_blank"> <img src="https://eflex.app/assets/email-img/app-store.png" width="150"> </a>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
              
				
               <tr>                
				   <td>
					
					<table style="margin:0px; background-image:url(https://eflex.app/assets/email-img/footers.png); height: 135px; color: #fff" width="650" height="" cellspacing="0" cellpadding="0" align="center">
						<tbody>
						
						
						
						<tr>
							<td colspan="3" align="center">
								<p> &nbsp;</p>
							</td> 
							
						</tr>
						<tr>
							<td colspan="3" align="center">
								<p> &nbsp;</p>
							</td> 
							
						</tr>
						<tr>
							<td valign="center" align="center">
								<img src="https://eflex.app/assets/email-img/logo.png" width="150">
							</td>
							<td width="200" valign="center" align="center">
								&nbsp;
							</td>
							<td valign="center" align="center">
								<table>
									<tbody><tr>
										
										    <td>
                                                        <a href="https://twitter.com/eflexbyeclipse"><img src="https://eflex.app/assets/email-img/twitter.png" width="20" /></a>
                                                    </td>
                                                    <td><a href="https://www.instagram.com/eflexbyeclipse/"><img src="https://eflex.app/assets/email-img/instagram-logo.png" width="20" /></a></td>
                                                    <td><a href="https://www.linkedin.com/company/10798134/"><img src="https://eflex.app/assets/email-img/linkedin.png" width="25" /></a></td>
																		
									</tr>
								</tbody></table>
							</td>
						</tr>
						
						<tr>
							<td colspan="3" border="2" align="center">
								<p style="border-top:1px solid #fff;"> &nbsp;</p>
							</td> 
							
						</tr>
						<tr>
							<td colspan="3" border="1" align="center">
								<p style="color: #fff; font-size: 12px; font-weight: bold;">All Rights Reserved by eFlex</p>
							</td>
							
						</tr>
						
					 </tbody>
					</table>
                  </td>				  
               </tr>
              
            </tbody>
         </table>
      </div>      
   </div>



