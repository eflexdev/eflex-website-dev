﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    public partial interface IEflexService
    {

        #region GetCompanyDashboardData
        [WebInvoke(UriTemplate = "GetCompanyDashboardData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetCompanyDashboardDataResponse GetCompanyDashboardData(DbConIdInput iObj);
        #endregion GetCompanyDashboardData

        #region GetDashboardTransactions
        [WebInvoke(UriTemplate = "GetDashboardTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetDashboardTransactionsResponse GetDashboardTransactions(DbConIdInput iObj);
        #endregion GetDashboardTransactions

        #region GetStatementPeriod
        [WebInvoke(UriTemplate = "GetStatementPeriod", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetStatementPeriodResponse GetStatementPeriod(DbConInput iObj);
        #endregion GetStatementPeriod

        #region GetActivityData
        [WebInvoke(UriTemplate = "GetActivityData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetActivityDataResponse GetActivityData(GetActivityInput iObj);
        #endregion GetActivityData

        #region GetPAPDetails
        [WebInvoke(UriTemplate = "GetPAPDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetPAPDetailsResponse GetPAPDetails(DbConIdInput iObj);
        #endregion GetPAPDetails

        #region UpdatePAPDetails
        [WebInvoke(UriTemplate = "UpdatePAPDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdatePAPDetails(UpdatePAPDetailsInput iObj);
        #endregion UpdatePAPDetails

        #region UpdateCompanyDetails
        [WebInvoke(UriTemplate = "UpdateCompanyDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateCompanyDetails(UpdateCompanyDetailsInput iObj);
        #endregion UpdateCompanyDetails

        #region AddEmployee
        [WebInvoke(UriTemplate = "AddEmployee", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEmployee(AddEmployeeInput iObj);
        #endregion AddEmployee

        #region GetEStatement
        [WebInvoke(UriTemplate = "GetEStatement", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEStatementResponse GetEStatement(GetEStatementInput iObj);
        #endregion GetEStatement

        #region ListOfNotificationLog
        [WebInvoke(UriTemplate = "ListOfNotificationLog", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CNotificationLogResponse ListOfNotificationLog(LazyLoadingSearchInput iObj);
        #endregion ListOfNotificationLog

        #region GetNotiMsgCount
        [WebInvoke(UriTemplate = "GetNotiMsgCount", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CNotiMsgCountResponse GetNotiMsgCount(NotiMsgCountInfo iObj);
        #endregion GetNotiMsgCount

        #region AddPlan
        [WebInvoke(UriTemplate = "AddPlan", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddPlan(AddPlanInput iObj);
        #endregion AddPlan

        #region UpdateCompanyStatus
        [WebInvoke(UriTemplate = "UpdateCompanyStatus", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateCompanyStatus(UpdateStatusIdInput iObj);
        #endregion UpdateCompanyStatus

        #region ListCompAllTransactions
        [WebInvoke(UriTemplate = "ListCompAllTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListCompAllTransactionsResponse ListCompAllTransactions(ListCompAllTransactionsInput iObj);
        #endregion ListCompAllTransactions

        #region DeleteCompany
        [WebInvoke(UriTemplate = "DeleteCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteCompany(IdInputv2 iObj);
        #endregion DeleteCompany

        #region GetRecentClaimDashboard
        [WebInvoke(UriTemplate = "GetRecentClaimDashboard", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetRecentClaimDashboardResponse GetRecentClaimDashboard(GetDashboardTransactionsInput iObj);
        #endregion GetRecentClaimDashboard

        #region GetEmpPlanDashboardDetails
        [WebInvoke(UriTemplate = "GetEmpPlanDashboardDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpPlanDashboardDetailsResponse GetEmpPlanDashboardDetails(GetEmpPlanDashboardDetailsInput iObj);
        #endregion GetEmpPlanDashboardDetails

        #region EmployeeClaimListForCompany
        [WebInvoke(UriTemplate = "EmployeeClaimListForCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CEmployeeClaimListResponse EmployeeClaimListForCompany(EmployeeClaimListInput iObj);
        #endregion EmployeeClaimListForCompany

        #region ResentOwnerCredentials
        [WebInvoke(UriTemplate = "ResentOwnerCredentials", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResentOwnerCredentials(ResentOwnerCredentialsInput iObj);
        #endregion ResentOwnerCredentials

        #region ImportEmployeesExcelOnRegister
        [WebInvoke(UriTemplate = "ImportEmployeesExcelOnRegister", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ImportEmployeesExcelOnRegister(ImportUsersExcelInput iObj);
        #endregion ImportEmployeesExcelOnRegister
    }
}