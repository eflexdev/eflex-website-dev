﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region SendMessage
        public CSendMessageResponse SendMessage(MessageInputInfo Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            
            decimal UserId = Obj.SenderId;
            string ActionTaken = "";
            string ServiceName = "SendMessage";
            CSendMessageResponse lobjResponse = new CSendMessageResponse();
            string Constring = GetConnection(Obj.DbCon);

            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
            CMail Mail = new CMail();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    //Enteries added to appropriate tables
                    decimal? CId = datacontext.sp_SendMsgNew(Obj.CId, Obj.SenderId, Obj.SenderUserTypeId, Obj.RecipientId, Obj.RecipientUserTypeId, Obj.Msg, Obj.MsgFile, Obj.MsgFileType, Obj.SenderCompanyId, Obj.RecipientCompanyId);
                    if (CId > 0)
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SenderUserTypeId && x.Active == true && x.OrigUserId == Obj.SenderId && x.CompanyId == Obj.SenderCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();
                       
                        //Retreive all messages for selected conversation
                        List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessagesAjax(CId, MsgUserId, Obj.LatestMsgId)
                                                 select new ConvMsgs
                                                 {
                                                     MsgId = a.MsgId,
                                                     Msg = a.Msg,
                                                     MsgFile = a.MsgFile,
                                                     MsgDate = a.MsgDate.ToString(),
                                                     FileType = a.FileType,
                                                     SenderName = a.Name,
                                                     SenderPic = a.Pic,
                                                     SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                     AmISender = Convert.ToBoolean(a.AmISender)
                                                 }).ToList();

                        //Send Notification
                        OtherUser1 User = (from p in datacontext.sp_GetOtherUserMoreDetails(CId, MsgUserId)
                                           select new OtherUser1
                                           {
                                               UserId = Convert.ToDecimal(p.UserId),
                                               Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                               Username = p.Username,
                                               Email = p.EmailAddress,
                                               Phone = p.ContactNo,
                                               Pic = p.Pic,
                                               UserTypeId = Convert.ToInt32(p.UserTypeId),
                                               CompanyId = Convert.ToDecimal(p.CompanyId)
                                           }).Take(1).FirstOrDefault();

                        string SenderName = (from p in datacontext.TblMsgUsers where p.OrigUserId == Obj.SenderId && p.UserTypeId == Obj.SenderUserTypeId && p.CompanyId == Obj.SenderCompanyId && p.Active == true select (p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName))).Take(1).SingleOrDefault();
                        string lstrMessage = SenderName + " has sent a message (" + DecodeBase64(Obj.Msg) + ").";

                        if (Obj.RecipientCompanyId > -1)
                        {
                            using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                            {
                                NotifyUser1(dc, lstrMessage, User.UserId,CObjects.enmNotifyType.Message.ToString());
                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                NotificationList = InsertionInNotificationTable(User.UserId, lstrMessage, dc, User.UserId, CObjects.enmNotifyType.Message.ToString());


                                datacontext.SubmitChanges();

                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(dc, NotificationList, User.UserId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications

                                #region SendMail

                                StringBuilder builder = new StringBuilder();
                             
                                builder.Append("Hello " + User.Name + ",<br/><br/>");
                                builder.Append(lstrMessage + " <br/><br/>");
                              
                                string lstrMessage1 = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage1.Replace("lstrEmailContent", builder.ToString());

                                Mail.EmailTo = User.Email.Trim();
                                Mail.Subject = "New Message";
                                Mail.MessageBody = lstrMessage1;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = User.CompanyId;
                                bool IsSent = Mail.SendEMail(dc, true);
                              

                                #endregion SendMail
                                dc.Connection.Close();

                            }
                        }
                        else // if Recipient is a master user
                        {
                            using (MasterDBDataContext context = new MasterDBDataContext())
                            {
                               EflexNotifyLog(context, DateTime.UtcNow, lstrMessage, Convert.ToString(User.UserId));
                               #region SendMail

                               StringBuilder builder = new StringBuilder();
                               builder.Append("Hello " + User.Name + ",<br/><br/>");
                               builder.Append(lstrMessage + " <br/><br/>");

                               string lstrMessage1 = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                               lstrMessage = lstrMessage1.Replace("lstrEmailContent", builder.ToString());

                               Mail.EmailTo = User.Email.Trim();
                               Mail.Subject = "New Message";
                               Mail.MessageBody = lstrMessage1;
                               Mail.GodaddyUserName = GodaddyUserName;
                               Mail.GodaddyPassword = GodaddyPwd;
                               Mail.Server = Server;
                               Mail.Port = Port;
                               Mail.CompanyId = User.CompanyId;
                               bool IsSent = Mail.SendEMail(context, true);


                               #endregion SendMail

                               context.Connection.Close();
                            }
                        }
                        lobjResponse.ConversationMessages = lstMsg;
                        lobjResponse.ID = CId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "Message could not be sent";
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                
            }
            return lobjResponse;
        }



        #endregion SendMessage1

        #region ListRecentConversations
        public CRecentConversationResponse ListRecentConversations(RecentConvInputInfo Obj)
        {
            string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
           
            string ActionTaken = "";
            string ServiceName = "ListRecentConversations";
            bool IsLogOut = false;
            UserId = Obj.UserId;
            CRecentConversationResponse lobjResponse = new CRecentConversationResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();
                    List<Conversation> lstCon = (from p in datacontext.TblConversations
                                                 where (p.User1 == MsgUserId || p.User2 == MsgUserId) && p.Active==true
                                                 orderby p.LastMsgDate descending
                                                 select new Conversation
                                                 {
                                                     CId = p.CId,
                                                     LatestMsg = (String.IsNullOrEmpty(p.LastMessage) ? "U2VudCBhIGZpbGU=" : p.LastMessage),
                                                     LatestMsgDate = p.LastMsgDate.ToString(),
                                                     SenderName = p.LastMsgSender,
                                                     SenderPic = p.LastSenderPic,
                                                     OtherUserDetails = GetAllOtherUserDetails(datacontext, Obj.CompanyId),
                                                     SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId)
                                                 }).Take(3).ToList();

                    lobjResponse.RecentConversations = lstCon;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                      
                     }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }

       
        #endregion ListRecentConversations

        #region ListAllConversations
        public CAllConversationResponse ListAllConversations(ConvInputInfo Obj)
        {
            string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
           
            string ActionTaken = "";
            string ServiceName = "ListAllConversations";
            CAllConversationResponse lobjResponse = new CAllConversationResponse();
            decimal CId = Obj.CId;

            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        List<Conversation> lstCon;
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                        //get the latest Cid 
                        if (CId == -1)
                            CId = datacontext.TblConversations.Where(p => (p.User1 == MsgUserId || p.User2 == MsgUserId) && p.Active == true).OrderByDescending(y => y.LastMsgDate).Select(x => x.CId).Take(1).FirstOrDefault();


                        //Retreive all conversations but messages for only selected conversation
                        lstCon = (from p in datacontext.TblConversations
                                  where (p.User1 == MsgUserId || p.User2 == MsgUserId) && p.Active == true
                                  orderby p.LastMsgDate descending
                                  select new Conversation
                                  {
                                      CId = p.CId,
                                      LatestMsg = p.LastMessage,
                                      LatestMsgDate = p.LastMsgDate.ToString(),
                                      SenderName = p.LastMsgSender,
                                      SenderPic = p.LastSenderPic,
                                      SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId),
                                      OtherUserDetails = GetAllOtherUserDetails(datacontext, Obj.CompanyId),
                                      Messages = GetMessages(datacontext, p.CId, MsgUserId, Obj.ChunkStart, Obj.ChunkSize, CId)
                                  }).ToList();

                        lobjResponse.Conversations = lstCon;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversations

        #region ListAllConversationMessages
        public CConvMessagesResponse ListAllConversationMessages(ConvInputInfo Obj)
        {
          string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
          
            string ActionTaken = "";
            string ServiceName = "ListAllConversationMessages";
            CConvMessagesResponse lobjResponse = new CConvMessagesResponse();
            decimal CId = Obj.CId;
            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                    decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                    //Retreive all messages for selected conversation
                    List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessages(CId, MsgUserId, Obj.ChunkStart, Obj.ChunkSize)
                                             select new ConvMsgs
                                             {
                                                 MsgId = a.MsgId,
                                                 Msg = a.Msg,
                                                 MsgFile = a.MsgFile,
                                                 MsgDate = a.MsgDate.ToString(),
                                                 FileType = a.FileType,
                                                 SenderName = a.Name,
                                                 SenderPic = a.Pic,
                                                 SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                 AmISender = Convert.ToBoolean(a.AmISender)
                                             }).ToList();

                    lobjResponse.ConversationMessages = lstMsg;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversationMessages

        #region ListAllConversationMessagesAjax
        public CConvMessagesResponse ListAllConversationMessagesAjax(ConvInputAjaxInfo Obj)
        {
            string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
           
            string ActionTaken = "";
            string ServiceName = "ListAllConversationMessagesAjax";
            CConvMessagesResponse lobjResponse = new CConvMessagesResponse();
            decimal CId = Obj.CId;
            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                        //Retreive all messages for selected conversation
                        List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessagesAjax(CId, MsgUserId, Obj.LatestMsgId)
                                                 select new ConvMsgs
                                                 {
                                                     MsgId = a.MsgId,
                                                     Msg = a.Msg,
                                                     MsgFile = a.MsgFile,
                                                     MsgDate = a.MsgDate.ToString(),
                                                     FileType = a.FileType,
                                                     SenderName = a.Name,
                                                     SenderPic = a.Pic,
                                                     SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                     AmISender = Convert.ToBoolean(a.AmISender)
                                                 }).ToList();

                        lobjResponse.ConversationMessages = lstMsg;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversationMessagesAjax

        #region ListConversationDetailsWithUser
        public CFreshConversationResponse ListConversationDetailsWithUser(UserConvInputInfo Obj)
        {
           string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
         
            string ActionTaken = "";
            string ServiceName = "ListConversationDetailsWithUser";
            UserId = Obj.LoggedInUserId;
            CFreshConversationResponse lobjResponse = new CFreshConversationResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        List<Conversation> lstCon;
                        decimal MsgUserId1 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.LoggedInUserTypeId && x.Active == true && x.OrigUserId == Obj.LoggedInUserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();
                        decimal MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.Active == true && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == Obj.SelectedUserCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                        if (Obj.SelectedUserTypeId == Convert.ToDecimal(CObjects.enmUserType.SuperAdmin))
                            MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.Active == true && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        decimal? tempCId = (from p in datacontext.TblConversations
                                            where ((p.User1 == MsgUserId1 && p.User2 == MsgUserId2) || (p.User1 == MsgUserId2 && p.User2 == MsgUserId1)) && p.Active == true
                                            select p.CId).Take(1).FirstOrDefault();

                        if (tempCId == 0) tempCId = -2;
                        decimal CId = (tempCId ?? -2);

                        //Retreive all conversations but messages for only selected conversation
                        lstCon = (from p in datacontext.TblConversations
                                  where (p.User1 == MsgUserId1 || p.User2 == MsgUserId1) && p.Active == true
                                  orderby p.LastMsgDate descending
                                  select new Conversation
                                  {
                                      CId = p.CId,
                                      LatestMsg = ((p.LastMessage == null || p.LastMessage == "") ? "U2VudCBhIGZpbGU=" : p.LastMessage),
                                      LatestMsgDate = p.LastMsgDate.ToString(),
                                      SenderName = p.LastMsgSender,
                                      SenderPic = p.LastSenderPic,
                                      SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId),
                                      OtherUserDetails = GetAllOtherUserDetails(datacontext, Obj.CompanyId),
                                      Messages = GetMessages(datacontext, p.CId, MsgUserId1, Obj.ChunkStart, Obj.ChunkSize, CId)
                                  }).ToList();

                        lobjResponse.Conversations = lstCon;
                        lobjResponse.CId = CId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListConversationDetailsWithUser

       
        #region ListAllCompanyUsers
        public CListAllCompanyUsersResponse ListAllCompanyUsers(ListAllCompanyUsersInput Obj)
        {
            string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
            
            string ActionTaken = "";
            string ServiceName = "ListAllCompanyUsers";
            UserId = Obj.LoggedInUserId;
            decimal CompanyId = Convert.ToDecimal(Obj.DbCon.Split('_')[1]);
            CListAllCompanyUsersResponse lobjResponse = new CListAllCompanyUsersResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.LoggedInUserTypeId && x.Active == true && x.OrigUserId == Obj.LoggedInUserId && x.CompanyId == CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();
                        List<OtherUser1> OtherUserObj = new List<OtherUser1>();
                        if (Obj.LoggedInUserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin) || Obj.LoggedInUserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                        {
                            OtherUserObj = (from p in datacontext.TblMsgUsers.AsEnumerable()
                                            where p.CompanyId == CompanyId && p.Active == true
                                            && p.UserId != MsgUserId  
                                            select new OtherUser1
                                            {
                                                Id = p.UserId,
                                                UserId = Convert.ToDecimal(p.OrigUserId),
                                                Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                Username = p.UserName,
                                                Email = p.EmailAddress,
                                                Phone = p.ContactNo,
                                                Pic = p.Pic,
                                                Cid = GetCid(datacontext, MsgUserId, p.UserId),
                                                LatestMsgObj = GetLatestMsgData(datacontext, MsgUserId, p.UserId),
                                                UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                CompanyId = Convert.ToDecimal(p.CompanyId),
                                                UnreadCount = GetUnreadCount(datacontext, MsgUserId, p.UserId),
                                                LatestMessageDate = GetLatestMsgDate(datacontext, MsgUserId, p.UserId)
                                                  }).Distinct().ToList();


                        }
                        else
                        {
                            OtherUserObj = (from p in datacontext.TblMsgUsers.AsEnumerable()
                                            where p.CompanyId == CompanyId && p.Active == true
                                            && p.UserId != MsgUserId && p.IsActive == true &&
                                            (p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin))
                                            select new OtherUser1
                                            {
                                                Id = p.UserId,
                                                UserId = Convert.ToDecimal(p.OrigUserId),
                                                Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                Username = p.UserName,
                                                Email = p.EmailAddress,
                                                Phone = p.ContactNo,
                                                Pic = p.Pic,
                                                Cid = GetCid(datacontext, MsgUserId, p.UserId),
                                                LatestMsgObj = GetLatestMsgData(datacontext, MsgUserId, p.UserId),
                                                UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                CompanyId = Convert.ToDecimal(p.CompanyId),
                                                UnreadCount = GetUnreadCount(datacontext, MsgUserId, p.UserId),
                                                LatestMessageDate = GetLatestMsgDate(datacontext, MsgUserId, p.UserId)
                                                
                                            }).Distinct().ToList();


                        }

                        List<OtherUser1> EflexOtherUserObj = (from p in datacontext.TblMsgUsers.AsEnumerable()
                                                              where p.Active == true && p.CompanyId == -1
                                                              && p.UserId != MsgUserId 
                                                              && (p.UserTypeId != Convert.ToDecimal(CObjects.enumMasterUserType.Broker) && p.UserTypeId != Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin))
                                                              select new OtherUser1
                                                              {
                                                                  Id = p.UserId,
                                                                  UserId = Convert.ToDecimal(p.OrigUserId),
                                                                  Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                                  Username = p.UserName,
                                                                  Email = p.EmailAddress,
                                                                  Phone = p.ContactNo,
                                                                  Pic = p.Pic,
                                                                  Cid = GetCid(datacontext, MsgUserId, p.UserId),
                                                                  LatestMsgObj = GetLatestMsgData(datacontext, MsgUserId, p.UserId),
                                                                  UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                                  CompanyId = Convert.ToDecimal(p.CompanyId),
                                                                  CompanyName = GetCompanyName(p.CompanyId),
                                                                  UnreadCount = GetUnreadCount(datacontext, MsgUserId, p.UserId),
                                                                  LatestMessageDate = GetLatestMsgDate(datacontext, MsgUserId, p.UserId)
                                                                  }).Distinct().ToList();

                        
                        EflexOtherUserObj.AddRange(OtherUserObj);

                        lobjResponse.OtherUser1 = EflexOtherUserObj.OrderByDescending(u => Convert.ToDateTime(u.LatestMessageDate)).Distinct().ToList();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }


        private LatestMsgDetails GetLatestMsgData(EflexMsgDBDataContext datacontext, decimal LoggedInUserId, decimal UserId)
        {
            decimal Cid = -1;
            LatestMsgDetails Obj = null;
            decimal UserCid = (from a in datacontext.TblConversations where a.Active == true && ((a.User1 == LoggedInUserId && a.User2 == UserId) || (a.User1 == UserId && a.User2 == LoggedInUserId)) select a.CId).Take(1).SingleOrDefault();
            if (UserCid > 0)
            {
                Cid = UserCid;
                Obj = (from p in datacontext.TblConversations
                       where p.Active == true && p.CId == Cid
                       select new LatestMsgDetails()
                           {
                               LatestMsg = p.LastMessage,
                               LatestMsgDate = Convert.ToString(p.LastMsgDate)
                           }).Take(1).SingleOrDefault();
            }
            return Obj;
        }

        private string GetLatestMsgDate(EflexMsgDBDataContext datacontext, decimal LoggedInUserId, decimal UserId)
        {
            decimal Cid = -1;
            string LatestMsgDate = null;
            TblConversation ConvObj = (from a in datacontext.TblConversations where a.Active == true && ((a.User1 == LoggedInUserId && a.User2 == UserId) || (a.User1 == UserId && a.User2 == LoggedInUserId)) select a).Take(1).SingleOrDefault();
            if (ConvObj != null)
            {
                LatestMsgDate = Convert.ToString(ConvObj.LastMsgDate);
                      
            }
            return LatestMsgDate;
        }


        private decimal GetCid(EflexMsgDBDataContext datacontext, decimal LoggedInUserId, decimal UserId)
        {
            decimal Cid = -1;
            decimal UserCid= (from a in datacontext.TblConversations where a.Active == true && ((a.User1 == LoggedInUserId && a.User2 == UserId) || (a.User1 == UserId && a.User2 == LoggedInUserId)) select a.CId).Take(1).SingleOrDefault();
               if(UserCid > 0)
               {
                   Cid = UserCid;
               }
               return Cid;
        }
        #endregion ListAllCompanyUsers


        private void UpdateMsgUser(TblCompanyUser UserObj, string Mode, decimal CompanyId)
        {
            try
            {
                using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
                {
                    if (Mode.ToUpper() == "UPDATE")
                    {
                        TblMsgUser Obj = (from p in datacontext.TblMsgUsers where p.Active == true && p.OrigUserId == UserObj.CompanyUserId && p.UserTypeId == UserObj.UserTypeId && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            Obj.FirstName = UserObj.FirstName;
                            Obj.LastName = UserObj.LastName;
                            Obj.EmailAddress = UserObj.EmailAddress;
                            Obj.UserName = UserObj.UserName;
                            Obj.UserTypeId = UserObj.UserTypeId;
                            Obj.ContactNo = UserObj.ContactNo;
                            Obj.DateModified = DateTime.UtcNow;

                            datacontext.SubmitChanges();
                        }
                    }
                    else if (Mode.ToUpper() == "ADD")
                    {
                        TblMsgUser Obj = new TblMsgUser()
                        {
                            OrigUserId = UserObj.CompanyUserId,
                            FirstName = UserObj.FirstName,
                            LastName = UserObj.LastName,
                            EmailAddress = UserObj.EmailAddress,
                            UserName = UserObj.UserName,
                            UserTypeId = UserObj.UserTypeId,
                            ContactNo = UserObj.ContactNo,
                            IsActive = true,
                            DateModified = DateTime.UtcNow,
                            DateCreated = DateTime.UtcNow,
                            Active = true,
                            CompanyId = CompanyId                            
                        };
                        datacontext.TblMsgUsers.InsertOnSubmit(Obj);
                        datacontext.SubmitChanges();
                    }
                    else
                    {
                        if (UserObj != null)
                        {
                            TblMsgUser Obj = (from p in datacontext.TblMsgUsers where p.Active == true && p.OrigUserId == UserObj.CompanyUserId && p.CompanyId == CompanyId && p.UserTypeId == UserObj.UserTypeId select p).Take(1).SingleOrDefault();
                            if (Obj != null)
                                Obj.Active = false;

                            List<TblConversation> ConversationObj = (from p in datacontext.TblConversations
                                                                     where p.Active == true
                                                                         && (p.User1 == Obj.UserId || p.User2 == Obj.UserId)
                                                                     select p).ToList();
                            if (ConversationObj != null && ConversationObj.Count() > 0)
                                ConversationObj.ForEach(x => x.Active = false);

                            datacontext.SubmitChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message + "Update");
            }
        }


        #region SendBroadcastMessage
        public CResponse SendBroadcastMessage(SendBroadcastMessageInput Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
           
            string ActionTaken = "";
            string ServiceName = "SendBroadcastMessage";
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(Obj.DbCon);
            UserId = Obj.SenderId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                      EflexDBDataContext dc1 = new EflexDBDataContext(Constring);
                      if (IsValidateUser(dc1))
                      {
                          string MsgNotReceivedUsers = "";
                          List<decimal> MsgReceivedUsersCount = new List<decimal>();
                          if (Obj.RecipientData != null && Obj.RecipientData.Count() > 0)
                          {
                              foreach (RecipientData item in Obj.RecipientData)
                              {
                                  //Enteries added to appropriate tables
                                  decimal? CId = datacontext.sp_SendBroadcastMsg(item.CId, Obj.SenderId, Obj.SenderUserTypeId, item.RecipientId, item.RecipientUserTypeId, Obj.Msg, Obj.SenderCompanyId, Convert.ToDecimal(item.RecipientCompanyId));
                                  if (CId > 0)
                                  {
                                      try
                                      {
                                          decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SenderUserTypeId && x.Active == true && x.OrigUserId == Obj.SenderId && x.CompanyId == Obj.SenderCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                                          string SenderName = (from p in datacontext.TblMsgUsers where p.OrigUserId == Obj.SenderId && p.UserTypeId == Obj.SenderUserTypeId && p.CompanyId == Obj.SenderCompanyId && p.Active == true select (p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName))).Take(1).SingleOrDefault();
                                          string lstrMessage = SenderName + " has sent a message (" + DecodeBase64(Obj.Msg) + ").";//Todo

                                          //Send Notification
                                          OtherUser1 User = (from p in datacontext.sp_GetOtherUserMoreDetails(CId, MsgUserId)
                                                             select new OtherUser1
                                                             {
                                                                 UserId = Convert.ToDecimal(p.UserId),
                                                                 Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                                 Username = p.Username,
                                                                 Email = p.EmailAddress,
                                                                 Phone = p.ContactNo,
                                                                 Pic = p.Pic,
                                                                 UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                                 CompanyId = Convert.ToDecimal(p.CompanyId)
                                                             }).Take(1).FirstOrDefault();



                                          if (item.RecipientCompanyId > -1)
                                          {
                                              using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                              {
                                                  NotifyUser1(dc, lstrMessage, User.UserId,CObjects.enmNotifyType.Message.ToString());
                                                  dc.Connection.Close();

                                              }
                                          }
                                          else // if Recipient is a presidential user
                                          {
                                              using (MasterDBDataContext context = new MasterDBDataContext())
                                              {
                                                  EflexNotifyLog(context, DateTime.UtcNow, lstrMessage, Convert.ToString(User.UserId));
                                                  context.Connection.Close();
                                              }
                                          }
                                          lobjResponse.ID = CId;

                                      }
                                      catch (Exception ex)
                                      {
                                          
                                          MsgReceivedUsersCount.Add(item.RecipientId);
                                          string UserName = (from p in datacontext.TblMsgUsers where p.UserId == item.RecipientId && p.UserTypeId == item.RecipientUserTypeId && p.CompanyId == item.RecipientCompanyId select (p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName))).Take(1).SingleOrDefault();
                                          if (UserName != null && UserName == "")
                                          {
                                              if (MsgNotReceivedUsers != null && MsgNotReceivedUsers == "")
                                              {
                                                  MsgNotReceivedUsers = MsgNotReceivedUsers + "," + UserName;
                                              }
                                              else
                                              {
                                                  MsgNotReceivedUsers = MsgNotReceivedUsers + UserName;
                                              }
                                          }
                                      }


                                  }
                                  else
                                  {
                                      MsgReceivedUsersCount.Add(item.RecipientId);
                                      string UserName = (from p in datacontext.TblMsgUsers where p.UserId == item.RecipientId && p.UserTypeId == item.RecipientUserTypeId && p.CompanyId == item.RecipientCompanyId select (p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName))).Take(1).SingleOrDefault();
                                      if (UserName != null && UserName == "")
                                      {
                                          if (MsgNotReceivedUsers != null && MsgNotReceivedUsers == "")
                                          {
                                              MsgNotReceivedUsers = MsgNotReceivedUsers + "," + UserName;
                                          }
                                          else
                                          {
                                              MsgNotReceivedUsers = MsgNotReceivedUsers + UserName;
                                          }
                                      }
                                      lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                      lobjResponse.Message = "Message could not be sent";
                                  }
                              }

                          }
                          if (MsgReceivedUsersCount.Count() == Obj.RecipientData.Count())
                          {
                              lobjResponse.Message = "Message could not be broadcasted successfully.";
                              lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                          }
                          if (MsgNotReceivedUsers != null && MsgNotReceivedUsers != "")
                          {
                              lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                              lobjResponse.Message = "Message could not be sent to users: " + MsgNotReceivedUsers + ".";
                          }
                          else
                          {
                              lobjResponse.Message = "Message broadcasted successfully.";
                              lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                          }
                      }
                      else
                      {
                          lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                          lobjResponse.Message =  CObjects.SessionExpired.ToString();
                          IsLogOut = true;
                         
                      }

                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }

        #endregion SendMessage1

        #region DeleteMessage
        public CResponse DeleteMessage(DeleteMessageInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
           
            string ActionTaken = "";
            bool IsLogOut = false;
            string ServiceName = "DeleteMessage";
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedinUserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        TblMsgUser UserObj = (from p in datacontext.TblMsgUsers where p.Active == true && p.OrigUserId == iObj.LoggedinUserId && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            TblMsg MsgObj = (from p in datacontext.TblMsgs where p.Active == true && p.MsgId == Convert.ToDecimal(iObj.MessageId) select p).Take(1).SingleOrDefault();
                            if (MsgObj != null)
                            {

                                if (MsgObj.SenderId == UserObj.UserId)
                                {
                                    MsgObj.IsDeletedBySender = true;
                                    datacontext.SubmitChanges();
                                }
                                if (MsgObj.RecipientId == UserObj.UserId)
                                {
                                    MsgObj.IsDeletedByReceiver = true;
                                    datacontext.SubmitChanges();
                                }
                            }
                        }


                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }


                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }

        #endregion SendMessage1

        #region Eflex Message Flow
        #region ListAllEflexCompanyUsers
        public CListAllCompanyUsersResponse ListAllEflexCompanyUsers(ListAllEflexCompanyUsersInput Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
           
            string ActionTaken = "";
            string ServiceName = "ListAllEflexCompanyUsers";
            UserId = Obj.LoggedInUserId;
            CListAllCompanyUsersResponse lobjResponse = new CListAllCompanyUsersResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.LoggedInUserTypeId && x.Active == true && x.OrigUserId == Obj.LoggedInUserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                    if (Obj.LoggedInUserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin) || Obj.LoggedInUserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                    {
                        List<OtherUser1> OtherUserObj = (from p in datacontext.TblMsgUsers.AsEnumerable()
                                                         where p.Active == true && p.CompanyId != -1
                                                         && p.UserId != MsgUserId
                                                         
                                                         select new OtherUser1
                                                         {
                                                             UserId = Convert.ToDecimal(p.OrigUserId),
                                                             Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                             Username = p.UserName,
                                                             Email = p.EmailAddress,
                                                             Phone = p.ContactNo,
                                                             Pic = p.Pic,
                                                             Cid = GetCid(datacontext, MsgUserId, p.UserId),
                                                             LatestMsgObj = GetLatestMsgData(datacontext, MsgUserId, p.UserId),
                                                             UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                             CompanyId = Convert.ToDecimal(p.CompanyId),
                                                             CompanyName = GetCompanyName(p.CompanyId),
                                                             UnreadCount = GetUnreadCount(datacontext, MsgUserId, p.UserId)
                                                            }).Distinct().OrderByDescending(x=>x.UnreadCount).ToList();

                        lobjResponse.OtherUser1 = OtherUserObj;
                    }
                    else
                    {
                        List<OtherUser1> OtherUserObj = (from p in datacontext.TblMsgUsers.AsEnumerable()
                                                         where p.CompanyId != -1 && p.Active == true
                                                         && p.UserId != MsgUserId && p.IsActive == true && 
                                                         (p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin) || p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                                                         select new OtherUser1
                                                         {
                                                             UserId = Convert.ToDecimal(p.OrigUserId),
                                                             Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                             Username = p.UserName,
                                                             Email = p.EmailAddress,
                                                             Phone = p.ContactNo,
                                                             Pic = p.Pic,
                                                             Cid = GetCid(datacontext, MsgUserId, p.UserId),
                                                             LatestMsgObj = GetLatestMsgData(datacontext, MsgUserId, p.UserId),
                                                             UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                             CompanyId = Convert.ToDecimal(p.CompanyId),
                                                             CompanyName = GetCompanyName(p.CompanyId),
                                                             UnreadCount = GetUnreadCount(datacontext, MsgUserId, p.UserId)
                                                           }).Distinct().OrderByDescending(x => x.UnreadCount).ToList();

                        lobjResponse.OtherUser1 = OtherUserObj;
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }

       
      

     
        #endregion ListAllCompanyUsers

        #region SendMessageByEflex
        public CSendMessageResponse SendMessageByEflex(EflexMessageInputInfo Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            UserId = Obj.SenderId;
            string ActionTaken = "";
            bool IsLogOut = false;
            string ServiceName = "SendMessageByEflex";
            CSendMessageResponse lobjResponse = new CSendMessageResponse();
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                        //Enteries added to appropriate tables
                        decimal? CId = datacontext.sp_SendMsgNew(Obj.CId, Obj.SenderId, Obj.SenderUserTypeId, Obj.RecipientId, Obj.RecipientUserTypeId, Obj.Msg, Obj.MsgFile, Obj.MsgFileType, Obj.SenderCompanyId, Obj.RecipientCompanyId);
                        if (CId > 0)
                        {
                            decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SenderUserTypeId && x.Active == true && x.OrigUserId == Obj.SenderId && x.CompanyId == Obj.SenderCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                            //Retreive all messages for selected conversation
                            List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessagesAjax(CId, MsgUserId, Obj.LatestMsgId)
                                                     select new ConvMsgs
                                                     {
                                                         MsgId = a.MsgId,
                                                         Msg = a.Msg,
                                                         MsgFile = a.MsgFile,
                                                         MsgDate = a.MsgDate.ToString(),
                                                         FileType = a.FileType,
                                                         SenderName = a.Name,
                                                         SenderPic = a.Pic,
                                                         SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                         AmISender = Convert.ToBoolean(a.AmISender)
                                                     }).ToList();

                            //Send Notification
                            OtherUser1 User = (from p in datacontext.sp_GetOtherUserMoreDetails(CId, MsgUserId)
                                               select new OtherUser1
                                               {
                                                   UserId = Convert.ToDecimal(p.UserId),
                                                   Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                   Username = p.Username,
                                                   Email = p.EmailAddress,
                                                   Phone = p.ContactNo,
                                                   Pic = p.Pic,
                                                   UserTypeId = Convert.ToInt32(p.UserTypeId),
                                                   CompanyId = Convert.ToDecimal(p.CompanyId)
                                               }).Take(1).FirstOrDefault();

                            string SenderName = (from p in datacontext.TblMsgUsers where p.OrigUserId == Obj.SenderId && p.UserTypeId == Obj.SenderUserTypeId && p.CompanyId == Obj.SenderCompanyId && p.Active == true select (p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName))).Take(1).SingleOrDefault();
                            string lstrMessage = SenderName + " has sent a message (" + DecodeBase64(Obj.Msg) + ")."; //Todo

                            if (Obj.RecipientCompanyId > -1)
                            {
                                string DbCon = DBFlag + Obj.RecipientCompanyId;
                                string Constring = GetConnection(DbCon);

                                using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                {
                                    NotifyUser1(dc, lstrMessage, User.UserId,CObjects.enmNotifyType.Message.ToString());
                                    #region Push Notifications
                                    List<TblNotificationDetail> NotificationList = null;
                                    NotificationList = InsertionInNotificationTable(User.UserId, lstrMessage, dc, User.UserId, CObjects.enmNotifyType.Message.ToString());


                                    datacontext.SubmitChanges();

                                    if (NotificationList != null && NotificationList.Count() > 0)
                                    {
                                        SendNotification(dc, NotificationList, User.UserId);
                                        NotificationList.Clear();

                                    }
                                    #endregion Push Notifications

                                    dc.Connection.Close();

                                }
                            }
                            else // if Recipient is a master user
                            {
                                using (MasterDBDataContext context = new MasterDBDataContext())
                                {
                                    EflexNotifyLog(context, DateTime.UtcNow, lstrMessage, Convert.ToString(User.UserId));
                                    context.Connection.Close();
                                }
                            }
                            lobjResponse.ConversationMessages = lstMsg;
                            lobjResponse.ID = CId;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Message could not be sent";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                
            }
            return lobjResponse;
        }



        #endregion SendMessage1

        #region ListAllConversationMessagesEflex
        public CConvMessagesResponse ListAllConversationMessagesEflex(ConvEflexInputInfo Obj)
        {
           DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";

            
            string ActionTaken = "";
            string ServiceName = "ListAllConversationMessagesEflex";
            bool IsLogOut = false;
            CConvMessagesResponse lobjResponse = new CConvMessagesResponse();
            decimal CId = Obj.CId;
            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {

                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        //Retreive all messages for selected conversation
                        List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessages(CId, MsgUserId, Obj.ChunkStart, Obj.ChunkSize)
                                                 select new ConvMsgs
                                                 {
                                                     MsgId = a.MsgId,
                                                     Msg = a.Msg,
                                                     MsgFile = a.MsgFile,
                                                     MsgDate = a.MsgDate.ToString(),
                                                     FileType = a.FileType,
                                                     SenderName = a.Name,
                                                     SenderPic = a.Pic,
                                                     SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                     AmISender = Convert.ToBoolean(a.AmISender)
                                                 }).ToList();

                        lobjResponse.ConversationMessages = lstMsg;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversationMessages

        #region ListEflexRecentConversations
        public CRecentConversationResponse ListEflexRecentConversations(RecentConvEflexInputInfo Obj)
        {
             DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            
            string ActionTaken = "";
            string ServiceName = "ListEflexRecentConversations";
            bool IsLogOut = false;
            UserId = Obj.UserId;
            CRecentConversationResponse lobjResponse = new CRecentConversationResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();
                        List<Conversation> lstCon = (from p in datacontext.TblConversations
                                                     where p.User1 == MsgUserId || p.User2 == MsgUserId
                                                     orderby p.LastMsgDate descending
                                                     select new Conversation
                                                     {
                                                         CId = p.CId,
                                                         LatestMsg = (String.IsNullOrEmpty(p.LastMessage) ? "U2VudCBhIGZpbGU=" : p.LastMessage),
                                                         LatestMsgDate = p.LastMsgDate.ToString(),
                                                         SenderName = p.LastMsgSender,
                                                         SenderPic = p.LastSenderPic,
                                                         OtherUserDetails = GetAllOtherUserDetails(datacontext, -1),
                                                         SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId)
                                                     }).Take(3).ToList();

                        lobjResponse.RecentConversations = lstCon;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                     
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }


        #endregion ListRecentConversations

        #region ListAllEflexConversations
        public CAllConversationResponse ListAllEflexConversations(ConvEflexInputInfo Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";

            
            string ActionTaken = "";
            string ServiceName = "ListAllEflexConversations";
            CAllConversationResponse lobjResponse = new CAllConversationResponse();
            decimal CId = Obj.CId;
            bool IsLogOut = false;
            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        List<Conversation> lstCon;
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        //get the latest Cid 
                        if (CId == -1)
                            CId = datacontext.TblConversations.Where(p => (p.User1 == MsgUserId || p.User2 == MsgUserId) && p.Active == true).OrderByDescending(y => y.LastMsgDate).Select(x => x.CId).Take(1).FirstOrDefault();


                        //Retreive all conversations but messages for only selected conversation
                        lstCon = (from p in datacontext.TblConversations
                                  where (p.User1 == MsgUserId || p.User2 == MsgUserId) && p.Active == true
                                  orderby p.LastMsgDate descending
                                  select new Conversation
                                  {
                                      CId = p.CId,
                                      LatestMsg = p.LastMessage,
                                      LatestMsgDate = p.LastMsgDate.ToString(),
                                      SenderName = p.LastMsgSender,
                                      SenderPic = p.LastSenderPic,
                                      SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId),
                                      OtherUserDetails = GetAllOtherUserDetails(datacontext, -1),
                                      
                                      Messages = GetMessages(datacontext, p.CId, MsgUserId, Obj.ChunkStart, Obj.ChunkSize, CId)
                                  }).ToList();

                        lobjResponse.Conversations = lstCon;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                     
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversations

        #region ListAllEflexConversationMessagesAjax
        public CConvMessagesResponse ListAllEflexConversationMessagesAjax(ConvEflexInputAjaxInfo Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
            
            string ActionTaken = "";
            string ServiceName = "ListAllEflexConversationMessagesAjax";
            CConvMessagesResponse lobjResponse = new CConvMessagesResponse();
            decimal CId = Obj.CId;
            UserId = Obj.UserId;
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        decimal MsgUserId = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.UserTypeId && x.Active == true && x.OrigUserId == Obj.UserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        //Retreive all messages for selected conversation
                        List<ConvMsgs> lstMsg = (from a in datacontext.sp_ListMessagesAjax(CId, MsgUserId, Obj.LatestMsgId)
                                                 select new ConvMsgs
                                                 {
                                                     MsgId = a.MsgId,
                                                     Msg = a.Msg,
                                                     MsgFile = a.MsgFile,
                                                     MsgDate = a.MsgDate.ToString(),
                                                     FileType = a.FileType,
                                                     SenderName = a.Name,
                                                     SenderPic = a.Pic,
                                                     SenderUserTypeId = Convert.ToInt32(a.UserTypeId),
                                                     AmISender = Convert.ToBoolean(a.AmISender)
                                                 }).ToList();

                        lobjResponse.ConversationMessages = lstMsg;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListAllConversationMessagesAjax

        #region ListConversationDetailsWithEflexUser
        public CFreshConversationResponse ListConversationDetailsWithEflexUser(EflexUserConvInputInfo Obj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
            //   decimal UserId = Obj.LoggedInUserId;
            string ActionTaken = "";
            string ServiceName = "ListConversationDetailsWithEflexUser";
            UserId = Obj.LoggedInUserId;
            CFreshConversationResponse lobjResponse = new CFreshConversationResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    MasterDBDataContext dc = new MasterDBDataContext();
                    if (IsValidateEflexUser(dc))
                    {
                        List<Conversation> lstCon;
                        decimal MsgUserId1 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.LoggedInUserTypeId && x.Active == true && x.OrigUserId == Obj.LoggedInUserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();
                        decimal MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.Active == true && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == Obj.SelectedUserCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                        if (Obj.SelectedUserTypeId == Convert.ToDecimal(CObjects.enmUserType.SuperAdmin))
                            MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        decimal? tempCId = (from p in datacontext.TblConversations
                                            where ((p.User1 == MsgUserId1 && p.User2 == MsgUserId2) || (p.User1 == MsgUserId2 && p.User2 == MsgUserId1)) && p.Active == true
                                            select p.CId).Take(1).FirstOrDefault();

                        if (tempCId == 0) tempCId = -2;
                        decimal CId = (tempCId ?? -2);

                        //Retreive all conversations but messages for only selected conversation
                        lstCon = (from p in datacontext.TblConversations
                                  where (p.User1 == MsgUserId1 || p.User2 == MsgUserId1) && p.Active == true
                                  orderby p.LastMsgDate descending
                                  select new Conversation
                                  {
                                      CId = p.CId,
                                      LatestMsg = ((p.LastMessage == null || p.LastMessage == "") ? "U2VudCBhIGZpbGU=" : p.LastMessage),
                                      LatestMsgDate = p.LastMsgDate.ToString(),
                                      SenderName = p.LastMsgSender,
                                      SenderPic = p.LastSenderPic,
                                      SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId),
                                      OtherUserDetails = GetAllOtherUserDetails(datacontext, -1),
                                      
                                      Messages = GetMessages(datacontext, p.CId, MsgUserId1, Obj.ChunkStart, Obj.ChunkSize, CId)
                                  }).ToList();

                        lobjResponse.Conversations = lstCon;
                        lobjResponse.CId = CId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListConversationDetailsWithUser

        #endregion

        #region ListAllAdminUserForMsg
        public CListAdminUserResponse ListAllAdminUserForMsg()
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            CListAdminUserResponse lobjResponse = new CListAdminUserResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                        List<ListAdminUser> Obj = (from p in datacontext.TblMsgUsers
                                                   where p.Active == true
                                                       && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin)
                                                       || p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) && p.CompanyId ==-1
                                                   select new ListAdminUser()
                                                   {
                                                       UserId = Convert.ToDecimal(p.OrigUserId),
                                                       FirstName = p.FirstName,
                                                       LastName = p.LastName,
                                                       UserName = p.UserName,
                                                       ContactNo = p.ContactNo,
                                                       Pwd = p.Pwd,
                                                       UserTypeId = Convert.ToDecimal(p.UserTypeId),
                                                       EmailAddress = p.EmailAddress
                                                   }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.ListAdminUser = Obj;
                  
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
            }
            return lobjResponse;
        }
        #endregion

        #region GetConvBetweenTwoUsers
        public CFreshConversationResponse GetConvBetweenTwoUsers(UserConvInputInfo Obj)
        {
            string Constring = GetConnection(Obj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            bool IsLogOut = false;
            
            string ActionTaken = "";
            string ServiceName = "GetConvBetweenTwoUsers";
            UserId = Obj.LoggedInUserId;
            CFreshConversationResponse lobjResponse = new CFreshConversationResponse();
            using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
            {
                try
                {
                    EflexDBDataContext dc = new EflexDBDataContext(Constring);
                    if (IsValidateUser(dc))
                    {
                        List<Conversation> lstCon;
                        decimal MsgUserId1 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.LoggedInUserTypeId && x.Active == true && x.OrigUserId == Obj.LoggedInUserId && x.CompanyId == Obj.CompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();
                        decimal MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.Active == true && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == Obj.SelectedUserCompanyId).Select(y => y.UserId).Take(1).FirstOrDefault();

                        if (Obj.SelectedUserTypeId == Convert.ToDecimal(CObjects.enmUserType.SuperAdmin))
                            MsgUserId2 = datacontext.TblMsgUsers.Where(x => x.UserTypeId == Obj.SelectedUserTypeId && x.OrigUserId == Obj.SelectedUserId && x.CompanyId == -1).Select(y => y.UserId).Take(1).FirstOrDefault();

                        decimal? tempCId = (from p in datacontext.TblConversations
                                            where (p.User1 == MsgUserId1 && p.User2 == MsgUserId2) && p.Active == true
                                            select p.CId).Take(1).FirstOrDefault();

                        if (tempCId == 0) tempCId = -2;
                        decimal CId = (tempCId ?? -2);

                        //Retreive all conversations but messages for only selected conversation
                        lstCon = (from p in datacontext.TblConversations
                                  where (p.User1 == MsgUserId1 && p.User2 == MsgUserId2) && p.Active == true
                                  orderby p.LastMsgDate descending
                                  select new Conversation
                                  {
                                      CId = p.CId,
                                      LatestMsg = ((p.LastMessage == null || p.LastMessage == "") ? "U2VudCBhIGZpbGU=" : p.LastMessage),
                                      LatestMsgDate = p.LastMsgDate.ToString(),
                                      SenderName = p.LastMsgSender,
                                      SenderPic = p.LastSenderPic,
                                      SenderUserTypeId = Convert.ToInt32(p.LastSenderUserTypeId),
                                      OtherUserDetails = GetAllOtherUserDetails(datacontext, Obj.CompanyId),
                                      
                                      Messages = GetMessages(datacontext, p.CId, MsgUserId1, Obj.ChunkStart, Obj.ChunkSize, CId)
                                  }).ToList();

                        lobjResponse.Conversations = lstCon;
                        lobjResponse.CId = CId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(Obj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
            }
            return lobjResponse;
        }
        #endregion ListConversationDetailsWithUser

        private OtherUser GetAllOtherUserDetails(EflexMsgDBDataContext datacontext, decimal CompanyId)
        {
            OtherUser User = null;
            User = (from p in datacontext.sp_GetAllOtherUserDetails(CompanyId)
                    select new OtherUser
                    {
                        Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                        Username = p.UserName,
                        Email = p.EmailAddress,
                        Phone = p.ContactNo,
                        Pic = p.Pic,
                        UserTypeId = Convert.ToInt32(p.UserTypeId),
                        CompanyId = Convert.ToDecimal(p.CompanyId)
                    }).Take(1).FirstOrDefault();

            return User;
        }

        private decimal GetUnreadCount(EflexMsgDBDataContext datacontext, decimal LoggedInUserId, decimal UserId)
        {
            decimal UnReadCount = 0;
            decimal UserUnReadCount1 = 0;
            decimal UserUnReadCount2 = 0;
            UserUnReadCount1 = (from a in datacontext.TblConversations where a.Active == true && 
                                      (a.User1 == LoggedInUserId && a.User2 == UserId)
                                       select Convert.ToDecimal(a.UnreadUser1)).Take(1).SingleOrDefault();
            UserUnReadCount2 = (from a in datacontext.TblConversations
                                        where a.Active == true &&
                                            (a.User1 == UserId && a.User2 == LoggedInUserId)
                                        select Convert.ToDecimal(a.UnreadUser2)).Take(1).SingleOrDefault();
            
            UnReadCount = UserUnReadCount1 + UserUnReadCount2;
           
            return UnReadCount;
        }

        private decimal GetUnreadCount1(EflexMsgDBDataContext datacontext, decimal LoggedInUserId, decimal UserId)
        {
            decimal UnReadCount = 0;
            decimal UserUnReadCount1 = 0;
            decimal UserUnReadCount2 = 0;
            UserUnReadCount1 = (from a in datacontext.TblConversations
                                where a.Active == true &&
                                    (a.User1 == LoggedInUserId && a.User2 == UserId)
                                select Convert.ToDecimal(a.UnreadUser2)).Take(1).SingleOrDefault();
            UserUnReadCount2 = (from a in datacontext.TblConversations
                                where a.Active == true &&
                                    (a.User1 == UserId && a.User2 == LoggedInUserId)
                                select Convert.ToDecimal(a.UnreadUser1)).Take(1).SingleOrDefault();
            
            UnReadCount = UserUnReadCount1 + UserUnReadCount2;

            return UnReadCount;
        }

        private decimal GetUnreadCount1(EflexMsgDBDataContext datacontext, decimal? OrigUserId, Int16? UserTypeId, decimal? CompanyId)
        {
            decimal UnReadCount = 0;
            decimal UserUnReadCount = datacontext.sp_GetUnreadCount(OrigUserId, UserTypeId, CompanyId).Select(x => Convert.ToDecimal(x.UnreadCount)).Take(1).SingleOrDefault();
            if (UserUnReadCount > 0)
            {
                UnReadCount = UserUnReadCount;
            }
            return UnReadCount;
        }
    }
}