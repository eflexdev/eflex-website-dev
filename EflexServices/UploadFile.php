<?php
   $CompanyId = $_POST['CompanyId'];
   $uploadFolder = $CompanyId."/".$FolderName;
   /*$message = 'Error uploading file';
        switch( $_FILES['file']['error'] ) {
            case UPLOAD_ERR_OK:
                $message = false;
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $message .= ' - file too large (limit of '.get_max_upload().' bytes).';
                break;
            case UPLOAD_ERR_PARTIAL:
                $message .= ' - file upload was not completed.';
                break;
            case UPLOAD_ERR_NO_FILE:
                $message .= ' - zero-length file uploaded.';
                break;
            default:
                $message .= ' - internal error #'.$_FILES['file']['error'];
                break;
        }*/
 if(isset($_FILES['file'])){  

    $errors= array();      

    $file_name = $_FILES['file']['name'];
    $file_size =$_FILES['file']['size'];
    $file_tmp =$_FILES['file']['tmp_name'];
    $file_type=$_FILES['file']['type'];  

	if(!is_dir($uploadFolder)) 
			mkdir($uploadFolder);
	
	$file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
    $extensions = array("jpeg","jpg","png","pdf","doc","docx");        
    if(in_array($file_ext,$extensions )=== false){
         $errors[]="image extension not allowed, please choose a JPEG,PNG,PDF,DOC,DOCX file.";
    }
    if($file_size > 26214400){
        $errors[]='File size cannot exceed 25 MB';
    }   

	 if(empty($errors)==true){

      $temp = explode(".",$_FILES["file"]["name"]);
      $extension = end($temp);
      $filoriginalname=$temp[0];
      $file_name = $filoriginalname.'_'.microtime(true).'.'.$extension;
		
	   if (move_uploaded_file($file_tmp,$uploadFolder."/".$file_name ))
	   {
			echo  "Success###".$file_name;
	   }
	   else{
		 echo  "Failure";	
	   }

    }else{
        echo  "Failure".$errors;	
        print_r($errors);
	}
}
else{

    echo  $errors;	
    $errors= array();
    $errors[]="No File found";
    print_r($errors);
}
?>