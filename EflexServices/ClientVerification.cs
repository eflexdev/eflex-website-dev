﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace EflexServices
{
    public class ClientVerification : ServiceAuthorizationManager
    {
        public const string MYHOST = "eflex.app";
        public const string APIKEY = "Eflex";

        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            
            return IsValidClient(operationContext);
        }

        public bool IsValidClient(OperationContext operationContext)
        {
            // if verification is disabled, return true
            if (Global.ClientVerification == false)
                return true;

            string key = GetAPIKey(operationContext);//Key is included in the calls that generate from mobile devices

            if (APIKEY == key)
            {
                return true;
            }
            else
            {

                string hostname = GetHostName(operationContext);
                if (hostname.ToUpper().IndexOf(MYHOST.ToUpper()) > -1)
                {
                    return true;
                }
                else
                {
                    // Send back an HTML reply
                    CreateErrorReply(operationContext, hostname);
                    return false;
                }
            }

        }
        public string GetAPIKey(OperationContext operationContext)
        {
            String apiKey = "";

            NameValueCollection headersCollection = HttpContext.Current.Request.Headers;
            // HttpContext.Current.Request.Url.Host;

            if (headersCollection.Count > 0)
            {
                var items = headersCollection.AllKeys.SelectMany(headersCollection.GetValues, (k, v) => new { key = k, value = v });
                foreach (var item in items)
                {
                    if (item.key == "APIKEY")
                        apiKey = item.value;
                }
            }
            return apiKey;
        }
        public string GetHostName(OperationContext operationContext)
        {
            String hostname = "";
            hostname = HttpContext.Current.Request.UrlReferrer.DnsSafeHost;
            return hostname;
        }

        private static void CreateErrorReply(OperationContext operationContext, string key)
        {
            // The error message is padded so that IE shows the response by default
            using (var sr = new StringReader("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + APIErrorHTML))
            {
                XElement response = XElement.Load(sr);
                using (Message reply = Message.CreateMessage(MessageVersion.None, null, response))
                {
                    HttpResponseMessageProperty responseProp = new HttpResponseMessageProperty() { StatusCode = HttpStatusCode.Unauthorized, StatusDescription = "Invalid client" };
                    responseProp.Headers[HttpResponseHeader.ContentType] = "text/html";
                    reply.Properties[HttpResponseMessageProperty.Name] = responseProp;
                    operationContext.RequestContext.Reply(reply);
                    // set the request context to null to terminate processing of this request
                    operationContext.RequestContext = null;
                }
            }
        }

        public const string APIErrorHTML = @"
        <html>
        <head>
            <title>Request Error - Invalid Caller Client</title>
            <style type=""text/css"">
                body
                {
                    font-family: Verdana;
                    font-size: x-large;
                }
            </style>
        </head>
        <body>
            <h1>
                Request Error
            </h1>
            <p>
                Invalid Caller Client
            </p>
        </body>
        </html>
        ";
    }
}