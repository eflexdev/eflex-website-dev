﻿using EnCryptDecrypt;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Xml;

namespace EflexServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, UseSynchronizationContext = false, AddressFilterMode = AddressFilterMode.Any)]
    
    public partial class EflexService : IEflexService
    {
        public static string ConStrPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "ConStr.xml";
        public static string ConPath = ConfigurationManager.AppSettings["ConPath"].ToString();
        public static string EmailFormats = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Email Templates\\";

        public static string lstrBrokerReferralUrl = "https://eflex.app/meeting-time-confirmation/";
        public static string lstrloginUrl = "https://eflex.app/login";
        public static string lstrAuthorizationUrl = "https://eflex.app/authenticate";
        public static string lstrAdminUrl = "https://eflex.app/eflex-admin";
        public static string lstrUrl = "https://eflex.app";//live
        public static string DBFlag = "DbCon_";
        public static string CompanyNameFlag = "Company_";
        string BaseUrl = @";
        public static string UserName = "";
        public static string Password = "";
        public static string ProgramId = "";
        public static decimal PackageID = ;
        public static decimal PurseId = -1;
        public static decimal BusinessEntityID = -1;
        public static decimal ShipID = -1;
		
        XmlDocument ConnectionxmlDoc = new XmlDocument();
        private static Random random = new Random();
        string UserToken = string.Empty;
        public decimal UserId { get; set; }

        //Method to validate user with token
        public bool IsValidateUser(EflexDBDataContext datacontext)
        {
            bool IsRequestValid = false;
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            string HeaderToken = woc.Headers["TokenHeader"];
            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == UserId select p).Take(1).SingleOrDefault();
            if (UserObj != null)
            {
                if (UserObj.UserToken == HeaderToken)
                {
                    IsRequestValid = true;
                }
                else
                {
                    IsRequestValid = false;
                }
            }

            return IsRequestValid;
        }

        #region AddCompany
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Function used to register new company
        /// </summary>
        /// <param name="iObj"></param>
        /// <returns></returns>
        public CAddCompanyResponse AddCompany(AddCompanyInput iObj)
        {
            string InputJson = "";
            DateTime StartTime = DateTime.UtcNow;
            CAddCompanyResponse lobjResponse = new CAddCompanyResponse();

            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    string Password = RandomString();
                    string EncryptPwd = CryptorEngine.Encrypt(Password);
                  
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    if (iObj.CompanyId == -1)
                    {
                        UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                        TblCompany CheckCompanyObj = (from p in datacontext.TblCompanies where p.Active == true && (p.CompanyName == iObj.CompanyName && p.PostalCode == iObj.PostalCode) select p).Take(1).SingleOrDefault();
                        if (CheckCompanyObj == null)
                        {
                            TblCompany CheckCompanyObj1 = (from p in datacontext.TblCompanies where p.Active == true && (p.CompanyName == iObj.CompanyName && p.Address1 == iObj.Address1) select p).Take(1).SingleOrDefault();
                            if (CheckCompanyObj1 == null)
                            {
                                TblCompany CompObj = new TblCompany()
                                {
                                    CompanyName = iObj.CompanyName,
                                    Address1 = iObj.Address1,
                                    Address2 = iObj.Address2,
                                    City = iObj.City,
                                    Province = iObj.ProvinceId,
                                    PostalCode = iObj.PostalCode,
                                    Country = iObj.Country,
                                    OwnerContactNo = iObj.OwnerContactNo,
                                    OwnerEmail = iObj.OwnerEmail,
                                    ServiceUrl = iObj.ServiceUrl,
                                    HostUrl = iObj.HostUrl,
                                    UniqueCode = iObj.Code,
                                    UserName = iObj.UserName,
                                    Pwd = EncryptPwd,
                                    FirstName = iObj.FirstName,
                                    LastName = iObj.LastName,
                                    Active = true,
                                    IsBlocked = false,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow,
                                    IsApproved = true,
                                };
                                datacontext.TblCompanies.InsertOnSubmit(CompObj);
                                datacontext.SubmitChanges();

                                decimal CompanyId = CompObj.CompanyId;
                                if (CompanyId > 0)
                                {
                                    TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                    if (CompanyObj != null)
                                    {
                                        #region Create Database
                                        string DBName = CompanyNameFlag + CompanyId;
                                        string DbCon = DBFlag + CompanyId;
                                        datacontext.ExecuteCommand("CREATE DATABASE [" + DBName + "]");
                                        string contents = "";
                                        string DbPatch = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                        contents = File.ReadAllText(DbPatch + "DbPatch.txt");
                                        string Patch = contents.Replace("lstrDbName", DBName);
                                        datacontext.ExecuteCommand(Patch.ToString());
                                        #endregion  Create Database

                                        #region Adding Connectionstring and run procedure path
                                        string ConnStrPath = ConStrPath;
                                        XmlDocument xmlDoc = new XmlDocument();
                                        xmlDoc.Load(ConnStrPath);
                                        XmlNode xmlnode = xmlDoc.SelectSingleNode("//connectionStrings");
                                        xmlnode.InnerXml += "<add name=\"" + DbCon + "\" connectionString=\"Data Source=18.216.133.20;Initial Catalog=" + DBName + ";Integrated Security=false;Max Pool Size=50000;Pooling=True;User ID=maan;Password=Jetking34\" providerName=\"System.Data.SqlClient\" />";
                                        xmlDoc.Save(ConnStrPath);

                                        XmlNodeList nodes = xmlDoc.SelectNodes("/connectionStrings/add[@name='" + DbCon + "']");
                                        string Constring = nodes[0].Attributes["connectionString"].Value.ToString();
                                        using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                                        {
                                            contents = "";
                                            string[] filePaths = Directory.GetFiles(DbPatch + "Procedure Patch\\");
                                            foreach (var file in filePaths)
                                            {
                                                contents = "";
                                                contents = File.ReadAllText(file);
                                                string ProcedurePatch = contents.Replace("lstrDbName", DBName);
                                                datacontextClient.ExecuteCommand(ProcedurePatch.ToString());
                                            }

                                            #region Create Folders
                                            if (!Directory.Exists(ConPath + CompanyId))
                                            {
                                                Directory.CreateDirectory(ConPath + CompanyId);
                                                DirectoryInfo dInfo = new DirectoryInfo(ConPath + CompanyId);
                                                DirectorySecurity dSecurity = dInfo.GetAccessControl();
                                                dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                                                dInfo.SetAccessControl(dSecurity);


                                            }
                                            #endregion Create Folders



                                            TblCompanyUser UserObj = new TblCompanyUser()
                                            {
                                                FirstName = iObj.FirstName,
                                                LastName = iObj.LastName,
                                                EmailAddress = iObj.OwnerEmail,
                                                UserName = iObj.UserName,
                                                UserTypeId = Convert.ToInt32(CObjects.enmUserType.Owner),
                                                Pwd = EncryptPwd,
                                                IsActive = true,
                                                UserToken = UserToken,
                                                ContactNo = iObj.OwnerContactNo,
                                                DateModified = DateTime.UtcNow,
                                                DateCreated = DateTime.UtcNow,
                                                Active = true
                                            };
                                            datacontextClient.TblCompanyUsers.InsertOnSubmit(UserObj);
                                            datacontextClient.SubmitChanges();

                                            CompanyObj.DBCon = DbCon;
                                            CompanyObj.StagesCovered = "1";
                                            datacontext.SubmitChanges();

                                            UpdateMsgUser(UserObj, "ADD", CompanyId);

                                            #region SendMail

                                           
                                            StringBuilder builder = new StringBuilder();


                                            builder.Append("Hello  " + iObj.FirstName + ",<br/><br/>");
                                            builder.Append("Your company, " + iObj.CompanyName + ", has been successfully registered with eFlex!<br/><br/>");
                                            builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                            builder.Append("Username: " + iObj.UserName.Split('_')[1] + "<br/><br/>");
                                            builder.Append("Click <a href=" + lstrAuthorizationUrl + "/" + CompanyId + "/" + UserObj.CompanyUserId + " >here</a> to set your password<br/><br/>");
                                            builder.Append("If you have any questions or concerns about your eFlex account, please reach out to our team at 416-748-9879 or eflex@eclipseeio.com <br/><br/>");
                                            builder.Append("Thanks, and enjoy your eFlex benefits !</h4><br/><br/>");
                                            builder.Append("The eclipse EIO Team");

                                            //send mail

                                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                            CMail Mail = new CMail();
                                            string lstrMessage = File.ReadAllText(EmailFormats + "CompanyRegister.txt");
                                            lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                            
                                            Mail.EmailTo = CompanyObj.OwnerEmail.Trim();
                                            Mail.Subject = "eFlex Corporate Account Registered";
                                            Mail.MessageBody = lstrMessage;
                                            Mail.GodaddyUserName = GodaddyUserName;
                                            Mail.GodaddyPassword = GodaddyPwd;
                                            Mail.Server = Server;
                                            Mail.Port = Port;
                                            Mail.CompanyId = CompanyId;
                                            bool IsSent = Mail.SendEMail(datacontext, true);


                                            #endregion SendMail

                                            CreateResponseLog(datacontextClient, StartTime, CompanyId, "AddCompany", InputJson, lobjResponse.Message);
                                            lobjResponse.CompanyUserId = UserObj.CompanyUserId;

                                            datacontextClient.Connection.Close();
                                        }

                                        #endregion Adding Connectionstring and run procedure path

                                        #region Provincial Tax Details
                                        TaxDetails TaxObj = (from p in datacontext.TblProvinces
                                                             where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                             select new TaxDetails()
                                                             {
                                                                 AdminMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                                 AdminMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                                 BrokerMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                                 BrokerMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                                 HST = Convert.ToDecimal(p.HST),
                                                                 RSTax = Convert.ToDecimal(p.RST),
                                                                 GST = Convert.ToDecimal(p.GST),
                                                                 PPTax = Convert.ToDecimal(p.PPT),
                                                             }).Take(1).SingleOrDefault();

                                        #endregion Provincial Tax Details

                                        lobjResponse.TaxDetails = TaxObj;
                                        lobjResponse.DbCon = DbCon;

                                        string Text = "A new company named as  " + iObj.CompanyName + " is resgistered on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                        CreateHistoryLog(datacontext, CompanyId, -1, lobjResponse.CompanyUserId, Text);

                                        string ForUserIds = String.Join(",", (from p in datacontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                        EflexNotifyLog(datacontext, DateTime.UtcNow, Text, ForUserIds);

                                    }
                                    lobjResponse.CompanyId = CompanyId;

                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                    lobjResponse.Message = "SUCCESS";
                                    lobjResponse.UserToken = UserToken;
                                }
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Company already exist with same name and address.";
                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Company already exist with this name.";
                        }
                    }
                    else
                    {
                        TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        if (CompanyObj != null)
                        {
                            TblCompany CompanyObj1 = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyName.ToUpper() == iObj.CompanyName.ToUpper() && p.Address1 == iObj.Address1 && p.PostalCode == iObj.PostalCode select p).Take(1).SingleOrDefault();
                            if (CompanyObj1 != null && CompanyObj1.CompanyId != iObj.CompanyId)
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Company already exist with this name.";
                            }
                            else
                            {
                                CompanyObj.CompanyName = iObj.CompanyName;
                                CompanyObj.FirstName = iObj.FirstName;
                                CompanyObj.LastName = iObj.LastName;
                                CompanyObj.Address1 = iObj.Address1;
                                CompanyObj.Address2 = iObj.Address2;
                                CompanyObj.City = iObj.City;
                                CompanyObj.Province = iObj.ProvinceId;
                                CompanyObj.PostalCode = iObj.PostalCode;
                                CompanyObj.Country = iObj.Country;
                                CompanyObj.OwnerContactNo = iObj.OwnerContactNo;
                                CompanyObj.OwnerEmail = iObj.OwnerEmail;
                                CompanyObj.UserName = iObj.UserName;
                                
                                CompanyObj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "Company details updated successfully.";
                                lobjResponse.DbCon = CompanyObj.DBCon;
                                lobjResponse.CompanyId = CompanyObj.CompanyId;

                                #region Provincial Tax Details
                                TaxDetails TaxObj = (from p in datacontext.TblProvinces
                                                     where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                     select new TaxDetails()
                                                     {
                                                         AdminMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                         AdminMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                         BrokerMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                         BrokerMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                         HST = Convert.ToDecimal(p.HST),
                                                         RSTax = Convert.ToDecimal(p.RST),
                                                         GST = Convert.ToDecimal(p.GST),
                                                         PPTax = Convert.ToDecimal(p.PPT),
                                                     }).Take(1).SingleOrDefault();

                                #endregion Provincial Tax Details


                                string DbCon = DBFlag + iObj.CompanyId;
                                string Constring = GetConnection(DbCon);
                                using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                                {
                                    TblCompanyUser UserObj = (from p in datacontextClient.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                                    if (UserObj != null)
                                    {
                                        lobjResponse.CompanyUserId = UserObj.CompanyUserId;
                                        UserToken = UserObj.UserToken;
                                    }

                                    datacontextClient.Connection.Close();
                                }


                                lobjResponse.TaxDetails = TaxObj;
                            }
                        }

                        lobjResponse.UserToken = UserToken;
                    }


                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                CreateMasterResponseLog(datacontext, StartTime, UserId, "AddCompany", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                
            }

            return lobjResponse;
        }


        #endregion AddCompany

        #region UpdateCompanyPlanType
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Date:260518
        /// Function to Update Company PlanType.
        /// </summary>
        /// <returns></returns>
        public CResponse UpdateCompanyPlanType(CompanyPlanTypeInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CompanyObj != null)
                    {
                        CompanyObj.PlanTypeId = Convert.ToInt16(iObj.PlanTypeId);
                        CompanyObj.IsCorporation = iObj.IsCorporation;
                        CompanyObj.StagesCovered = "2";
                        datacontext.SubmitChanges();
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.Message = "SUCCESS";
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateMasterResponseLog(datacontext, StartTime, iObj.CompanyId, "UpdateCompanyPlanType", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ValidateUniqueCode
        //JK:240518
        public CValidateUniqueCodeResponse ValidateUniqueCode(ValidateUniqueCodeInput iObj)
        {
            CValidateUniqueCodeResponse lobjResponse = new CValidateUniqueCodeResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    TblCompany Obj = (from p in datacontext.TblCompanies where p.Active == true && p.UniqueCode == iObj.UniqueCode select p).Take(1).SingleOrDefault();
                    if (Obj != null)
                    {
                        lobjResponse.IsAlreadyExists = true;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.IsAlreadyExists = false;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    CreateMasterResponseLog(datacontext, StartTime, -1, "ValidateUniqueCode", InputJson, lobjResponse.Message);
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion ValidateUniqueCode

     

        #region AddCompanyPlan
        public CResponse AddCompanyPlan(AddCompanyPlanInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        bool IsValid = true;
                        foreach (PlanDetails item in iObj.PlanDetails)
                        {
                            if (item.InitialDeposit < 0 || item.AdministrationInitialFee < 0 || item.AdministrationMonthlyFee < 0 || item.GrossTotalInitial < 0 || item.GrossTotalMonthly < 0 || item.NetDeposit <= 0  || item.PremiumTaxInitial < 0 || item.RetailSalesTaxInitial < 0 || item.RetailSalesTaxMonthly < 0)
                            {
                                IsValid = false;
                                break;
                            }
                        }



                        if (IsValid)
                        {

                            if (iObj.PlanDetails != null && iObj.PlanDetails.Count() > 0)
                            {
                                List<TblCompanyPlan> ClearPlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true select p).ToList();
                                if (ClearPlanObj != null && ClearPlanObj.Count() > 0)
                                {
                                    ClearPlanObj.ForEach(x => { x.Active = false; x.DateModified = DateTime.UtcNow; });
                                    datacontext.SubmitChanges();
                                }

                                List<TblPayment> ClearPaymentObj = (from p in datacontext.TblPayments where p.Active == true select p).ToList();
                                if (ClearPaymentObj != null && ClearPaymentObj.Count() > 0)
                                {
                                    ClearPaymentObj.ForEach(x => { x.Active = false; });
                                    datacontext.SubmitChanges();
                                }
                                List<TblCompanyPlan> PlanObj = new List<TblCompanyPlan>();
                                List<TblPayment> PaymentObj = new List<TblPayment>();


                                foreach (PlanDetails item in iObj.PlanDetails)
                                {
                                    
                                    TblCompanyPlan Obj = new TblCompanyPlan()
                                    {
                                        PlanTypeId = iObj.PlanTypeId,
                                        PlanStartDt = DateTime.ParseExact(item.PlanStartDt, "MM/dd/yyyy", null),
                                        Name = item.Name,
                                        ContributionSchedule = item.ContributionSchedule,
                                        RecurringDeposit = item.RecurringDeposit,
                                        InitialDeposit = item.InitialDeposit,
                                        NetDeposit = item.NetDeposit,
                                        AdministrationInitialFee = item.AdministrationInitialFee,
                                        AdministrationMonthlyFee = item.AdministrationMonthlyFee,
                                        HSTInitial = item.HSTInitial,
                                        HSTMonthly = item.HSTMonthly,
                                        PremiumTaxInitial = item.PremiumTaxInitial,
                                        PremiumTaxMonthly = item.PremiumTaxMonthly,
                                        RetailSalesTaxInitial = item.RetailSalesTaxInitial,
                                        RetailSalesTaxMonthly = item.RetailSalesTaxMonthly,
                                        GrossTotalInitial = item.GrossTotalInitial,
                                        GrossTotalMonthly = item.GrossTotalMonthly,
                                        PlanDesign = item.PlanDesign,
                                        RollOver = item.RollOver,
                                        CarryForwardPeriod = item.CarryForwardPeriod,
                                        Termination = item.Termination,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow,
                                    };
                                    datacontext.TblCompanyPlans.InsertOnSubmit(Obj);
                                    datacontext.SubmitChanges();
                                    string Colorname = GetRandomColor(iObj.CompanyId, Obj.CompanyPlanId);

                                    Obj.Color = Colorname;
                                    datacontext.SubmitChanges();


                                }
                                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                                {
                                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                                    if (CompanyObj != null)
                                    {
                                        CompanyObj.StagesCovered = "3";
                                        mastercontext.SubmitChanges();
                                        lobjResponse.ID = CompanyObj.CompanyId;

                                        string PlanName = String.Join(",", iObj.PlanDetails.Select(x => x.Name));
                                        string Text = "Plan named as  " + PlanName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);
                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                    }
                                    mastercontext.Connection.Close();
                                }
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Please enter some amount for the plan.";
                          

                        }
                        CreateResponseLog(datacontext, StartTime, lobjResponse.ID, "AddCompanyPlan", lobjResponse.Message, lobjResponse.Message);
                        lobjResponse.IsLogOut = IsLogOut;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

                datacontext.Connection.Close();
            }

            return lobjResponse;
        }

        #endregion

        #region PopulateProvince
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Date:260518
        /// Function to get Province list.
        /// </summary>
        /// <returns></returns>
        public CPopulateProvinceResponse PopulateProvince()
        {
            CPopulateProvinceResponse lobjResponse = new CPopulateProvinceResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    List<ProvinceList> Obj = (from p in datacontext.TblProvinces
                                              where p.Active == true
                                              select new ProvinceList()
                                              {
                                                  Title = p.Name,
                                                  ProvinceId = p.ProvinceId,
                                                  PPTax = Convert.ToDecimal(p.PPT),
                                                  RSTax = Convert.ToDecimal(p.RST)
                                              }).ToList();

                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.Message = "SUCCESS";
                    lobjResponse.ProvinceList = Obj;
                    CreateMasterResponseLog(datacontext, StartTime, -1, "PopulateProvince", InputJson, lobjResponse.Message);
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetCompanyDetails
        public CGetCompanyDetailsResponse GetCompanyDetails(GetCompanyDetailsInput iObj)
        {
            CGetCompanyDetailsResponse lobjResponse = new CGetCompanyDetailsResponse();
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            try
            {
                UserId = iObj.LoggedInUserId;
                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();

                    if (CompanyObj != null)
                    {
                        string Constring = GetConnection(CompanyObj.DBCon);
                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            if (IsValidateUser(datacontext))
                            {
                                #region BasicDetails

                                CompanyBasicaData CompanyDetailObj = (from p in mastercontext.TblCompanies
                                                                      where p.Active == true && p.CompanyId == iObj.Id
                                                                      select new CompanyBasicaData()
                                                                          {
                                                                              CompanyName = p.CompanyName,
                                                                              FirstName = p.FirstName,
                                                                              LastName = p.LastName,
                                                                              Address1 = p.Address1,
                                                                              Address2 = p.Address2,
                                                                              City = p.City,
                                                                              ProvinceId = Convert.ToDecimal(p.Province),
                                                                              PostalCode = p.PostalCode,
                                                                              Country = p.Country,
                                                                              OwnerContactNo = p.OwnerContactNo,
                                                                              OwnerEmail = p.OwnerEmail,
                                                                              ServiceUrl = p.ServiceUrl,
                                                                              HostUrl = p.HostUrl,
                                                                              Code = p.UniqueCode,
                                                                              UserName = p.UserName,
                                                                              Password = CryptorEngine.Decrypt(p.Pwd),
                                                                              IsCorporation = p.IsCorporation == null ? "" : Convert.ToString(p.IsCorporation),
                                                                              PlanTypeId = p.PlanTypeId == null ? -1 : Convert.ToDecimal(p.PlanTypeId),
                                                                              StagesCovered = p.StagesCovered,
                                                                              DbCon = p.DBCon
                                                                          }).Take(1).SingleOrDefault();
                                #endregion BasicDetails

                                #region Provincial Tax Details
                                TaxDetails TaxObj = (from p in mastercontext.TblProvinces
                                                     where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                     select new TaxDetails()
                                                     {
                                                         AdminMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                         AdminMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                         BrokerMinFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                         BrokerMaxFeePct = (from a in mastercontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                         BrokerComissionCharges = GetBrokerComissionCharges(iObj.Id),
                                                         HST = Convert.ToDecimal(p.HST),
                                                         RSTax = Convert.ToDecimal(p.RST),
                                                         GST = Convert.ToDecimal(p.GST),
                                                         PPTax = Convert.ToDecimal(p.PPT),
                                                     }).Take(1).SingleOrDefault();

                                #endregion Provincial Tax Details

                                if (CompanyDetailObj != null)
                                {

                                    #region Company plan details

                                    List<GetPlanDetails> PlanList = (from p in datacontext.TblCompanyPlans
                                                                     where p.Active == true
                                                                     select new GetPlanDetails()
                                                                         {
                                                                             PlanId = p.CompanyPlanId,
                                                                             PlanStartDt = Convert.ToString(p.PlanStartDt),
                                                                             Name = p.Name,
                                                                             ContributionSchedule = Convert.ToInt32(p.ContributionSchedule),
                                                                             RecurringDeposit = Convert.ToDecimal(p.RecurringDeposit),
                                                                             InitialDeposit = Convert.ToDecimal(p.InitialDeposit),
                                                                             NetDeposit = Convert.ToDecimal(p.NetDeposit),
                                                                             AdministrationInitialFee = Convert.ToDecimal(p.AdministrationInitialFee),
                                                                             AdministrationMonthlyFee = Convert.ToDecimal(p.AdministrationMonthlyFee),
                                                                             HSTInitial = Convert.ToDecimal(p.HSTInitial),
                                                                             HSTMonthly = Convert.ToDecimal(p.HSTMonthly),
                                                                             PremiumTaxInitial = Convert.ToDecimal(p.PremiumTaxInitial),
                                                                             PremiumTaxMonthly = Convert.ToDecimal(p.PremiumTaxMonthly),
                                                                             RetailSalesTaxInitial = Convert.ToDecimal(p.RetailSalesTaxInitial),
                                                                             RetailSalesTaxMonthly = Convert.ToDecimal(p.RetailSalesTaxMonthly),
                                                                             GrossTotalInitial = Convert.ToDecimal(p.GrossTotalInitial),
                                                                             GrossTotalMonthly = Convert.ToDecimal(p.GrossTotalMonthly),
                                                                             PlanDesign = Convert.ToDecimal(p.PlanDesign),
                                                                             RollOver = Convert.ToDecimal(p.RollOver),
                                                                             CarryForwardPeriod = p.CarryForwardPeriod,
                                                                             Termination = Convert.ToDecimal(p.Termination),
                                                                           }).ToList();
                                    #endregion Company plan details

                                    #region Company Employees details
                                    List<EmployeesData> EmployeesList = (from p in datacontext.TblCompanyUsers
                                                                         join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                                         where p.Active == true && q.Active == true && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee)
                                                                         select new EmployeesData()
                                                                          {
                                                                              CompanyUserId = p.CompanyUserId,
                                                                              FName = p.FirstName,
                                                                              LName = p.LastName,
                                                                              Email = p.EmailAddress,
                                                                              CompanyPlanId = Convert.ToDecimal(q.CompanyPlanId),
                                                                              UserTypeId = Convert.ToDecimal(p.UserTypeId)
                                                                          }).ToList();

                                    #endregion Company Employees details

                                    #region Company Admin details
                                    List<AdminsData> AdminsList = (from p in datacontext.TblCompanyUsers
                                                                   where p.Active == true
                                                                   && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Admin)
                                                                   select new AdminsData()
                                                                         {
                                                                             CompanyUserId = p.CompanyUserId,
                                                                             FName = p.FirstName,
                                                                             LName = p.LastName,
                                                                             Email = p.EmailAddress,
                                                                             UserTypeId = Convert.ToDecimal(p.UserTypeId)
                                                                         }).ToList();

                                    #endregion Company Admin details

                                    #region Payment Overview
                                    PaymentOverview PaymentOverviewObj = new PaymentOverview();
                                    List<TblCompanyPlan> Planlist = (from p in datacontext.TblCompanyPlans where p.Active == true select p).ToList();
                                    if (Planlist != null && Planlist.Count() > 0)
                                    {
                                        decimal TotalInitialOfAllMembers = 0;
                                        decimal TotalRecurringOfAllMembers = 0;
                                        foreach (TblCompanyPlan item in Planlist)
                                        {
                                            decimal MemberCount = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p.EmployeeId).Count();

                                            #region Initial amount
                                            decimal TotalInitial = 0;
                                            decimal PlanGrossInitialAmt = (from p in datacontext.TblCompanyPlans
                                                                           where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                           select Convert.ToDecimal(p.GrossTotalInitial ?? 0)).Sum();
                                            decimal PlanInitialAmt = (from p in datacontext.TblCompanyPlans
                                                                      where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                      select Convert.ToDecimal(p.InitialDeposit ?? 0)).Sum();

                                            TotalInitial = PlanGrossInitialAmt + PlanInitialAmt;
                                            #endregion Initial amount

                                            #region Recurring amount
                                            decimal TotalRecurringAmt = 0;

                                            decimal PlanGrossRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                                             where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                             select Convert.ToDecimal(p.GrossTotalMonthly ?? 0)).Sum();
                                            decimal PlanRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                                        where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                                        select Convert.ToDecimal(p.RecurringDeposit ?? 0)).Sum();

                                            TotalRecurringAmt = PlanGrossRecurringAmt + PlanRecurringAmt;


                                            #endregion Recurring amount



                                            if (MemberCount > 0)
                                            {
                                                TotalInitialOfAllMembers = TotalInitialOfAllMembers + (MemberCount * (TotalInitial));
                                                TotalRecurringOfAllMembers = TotalRecurringOfAllMembers + (MemberCount * (TotalRecurringAmt));
                                            }
                                            


                                        }

                                        PaymentOverviewObj = new PaymentOverview()
                                        {
                                            InitialPayment = TotalInitialOfAllMembers,
                                            RecurringPayment = TotalRecurringOfAllMembers,
                                        };
                                    }
                                    #endregion Payment Overview

                                    #region CompanyReferrals
                                    CompanyReferral ReferralObj = (from p in datacontext.TblCompanyReferrals
                                                                   join q in datacontext.TblReferralTypes on p.ReferralTypeId equals q.ReferralTypeId
                                                                   where p.Active == true && p.CompanyId == iObj.Id && q.Active == true
                                                                   select new CompanyReferral()
                                                                       {
                                                                           CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                                           ReferralTypeId = Convert.ToDecimal(p.ReferralTypeId),
                                                                           ReferralTypeName = q.Title,
                                                                           Name = p.Name,
                                                                           OtherDetails = p.OtherDetails,
                                                                           BrokerId = Convert.ToDecimal(p.BrokerId)
                                                                       }).Take(1).SingleOrDefault();
                                    #endregion CompanyReferrals

                                    #region MyRegion

                                    TblBrokerCompanyFee BrokerFeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                        where p.Active == true && p.CompanyId == iObj.Id
                                                                        select p).Take(1).SingleOrDefault();
                                    if (BrokerFeeObj != null)
                                    {
                                        CompanyBrokerFee CompBrokerObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                          where p.Active == true && p.CompanyId == iObj.Id
                                                                          select new CompanyBrokerFee()
                                                                               {
                                                                                   CompAdminFee = Convert.ToDecimal(p.AdminFee),
                                                                                   CompBrokerFee = Convert.ToDecimal(p.BrokerFee)
                                                                               }).Take(1).SingleOrDefault();
                                    }
                                    #endregion

                                    lobjResponse.PaymentOverview = PaymentOverviewObj;
                                    lobjResponse.PlanDetails = PlanList;
                                    lobjResponse.EmployeesData = EmployeesList;
                                    lobjResponse.AdminsData = AdminsList;
                                    lobjResponse.CompanyReferral = ReferralObj;
                                }
                                lobjResponse.TaxDetails = TaxObj;
                                lobjResponse.CompanyBasicaData = CompanyDetailObj;
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                lobjResponse.Message =  CObjects.SessionExpired.ToString();
                                IsLogOut = true;

                            }
                            lobjResponse.IsLogOut = IsLogOut;
                            CreateResponseLog(datacontext, StartTime, UserId, "GetCompanyDetails", InputJson, lobjResponse.Message);
                            datacontext.Connection.Close();
                        
                        }
                    }//end compobj
                    mastercontext.Connection.Close();
                }

            }
            catch (Exception ex)
            {

                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }

            return lobjResponse;
        }

      
        #endregion

        #region LoginUser
        //JK:260518
        public CLoginUserResponse LoginUser(LoginUserInput iObj)
        {

            DateTime StartTime = DateTime.UtcNow;
            CLoginUserResponse lobjResponse = new CLoginUserResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {

                try
                {
                    string UniqueCode = "";
                    if (iObj.UserName.Contains("_"))
                    {
                        UniqueCode = iObj.UserName.Split('_')[0].Trim();
                        TblCompany Obj = (from p in datacontext.TblCompanies where p.UniqueCode == UniqueCode && p.Active == true select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            if (IsBlock(Obj.DBCon))
                            {
                                string Constring = GetConnection(Obj.DBCon);
                                bool InActive = true;
                                using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                {
                                    try
                                    {
                                        TblCompanyUser UserObj = (from p in dc.TblCompanyUsers where p.Active == true && p.UserName == iObj.UserName && p.Pwd == CryptorEngine.Encrypt(iObj.Password) select p).Take(1).SingleOrDefault();
                                        if (UserObj != null)
                                        {
                                            if (UserObj.IsActive == false && UserObj.InActiveDate.Value.AddDays(30) <= DateTime.UtcNow)
                                            {
                                                lobjResponse.Message = "Sorry! You could not login, because you are terminated by company.";
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                lobjResponse.InActive = Convert.ToBoolean(UserObj.IsActive);
                                            }
                                            else
                                            {

                                                TblCompanyLoginLog logObj;

                                                logObj = new TblCompanyLoginLog()
                                                {
                                                    CompanyUserId = UserObj.CompanyUserId,
                                                    Action = "Login from web",
                                                    DateCreated = DateTime.UtcNow,
                                                    Active = true
                                                };
                                                dc.TblCompanyLoginLogs.InsertOnSubmit(logObj);
                                                string UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

                                                UserObj.UserToken = UserToken;
                                                dc.SubmitChanges();
                                                if (UserObj.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee))
                                                {
                                                    TblEmployee EmpObj = (from p in dc.TblEmployees where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                    if (EmpObj != null)
                                                    {

                                                        if (EmpObj.IsActive == false && EmpObj.InActiveDate.Value.AddDays(30) >= DateTime.UtcNow)
                                                        {
                                                            InActive = true;
                                                        }
                                                        else
                                                        {
                                                            InActive = Convert.ToBoolean(EmpObj.IsActive);
                                                        }
                                                        #region Payment Overview
                                                        decimal PlanAmt = 0;
                                                        
                                                        TblEmpBalance EmpPaymentObj = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                        if (EmpPaymentObj != null)
                                                        {
                                                            PlanAmt = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                                            lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);
                                                        }
                                                        lobjResponse.AmountDue = PlanAmt;

                                                        #endregion Payment Overview

                                                        #region Carrier
                                                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                                        TblUserCarrier CarrierObj = (from p in dc.TblUserCarriers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                                        if (CarrierObj != null)
                                                        {
                                                            if (CarrierObj.IsVerified == true)
                                                            {
                                                                string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                                                string lstrMessage = "";
                                                                StringBuilder builder = new StringBuilder();
                                                                builder.Append("Your are loggedin into eFlex.<br/><br/>");
                                                                                                                                lstrMessage = builder.ToString();
                                                                //send mail

                                                                CMail Mail = new CMail();
                                                                Mail.EmailTo = CarrierEmails;
                                                                Mail.Subject = "LoggedIn";
                                                                Mail.MessageBody = lstrMessage;
                                                                Mail.GodaddyUserName = GodaddyUserName;
                                                                Mail.GodaddyPassword = GodaddyPwd;
                                                                Mail.Server = Server;
                                                                Mail.Port = Port;
                                                                Mail.CompanyId = Obj.CompanyId;
                                                                bool IsSent = Mail.SendEMail(dc, true);

                                                            }
                                                            lobjResponse.IsMobileVerified = Convert.ToBoolean(CarrierObj.IsVerified);
                                                            lobjResponse.IsCarrier = true;
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.IsMobileVerified = false;
                                                            lobjResponse.IsCarrier = false;
                                                        }
                                                        #endregion

                                                        TblMasterCard MasterCard = (from p in datacontext.TblMasterCards where p.Active == true && p.CompanyId == Obj.CompanyId && p.UserId == EmpObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                        if (MasterCard != null)
                                                        {
                                                            lobjResponse.IsCardExists = true;
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.IsCardExists = false;

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    #region Payment Overview
                                                    decimal PlanAmt = 0;
                                                    TblPayment PaymentObj = (from p in dc.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                                    if (PaymentObj != null)
                                                    {
                                                        PlanAmt = (from p in dc.TblPayments
                                                                   where p.Active == true &&
                                                                   p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                   select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                                        lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);
                                                    }

                                                    lobjResponse.AmountDue = PlanAmt;

                                                    #endregion Payment Overview
                                                }

                                                #region PAPDetails
                                                bool IsPAPDetails = false;
                                                TblPAPDetail PAPOBj = (from p in dc.TblPAPDetails where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                if (PAPOBj != null)
                                                {
                                                    IsPAPDetails = true;
                                                }
                                                else
                                                {
                                                    IsPAPDetails = false;
                                                }
                                                #endregion

                                                #region Check Multiple accounts of logged in user

                                                List<MultipleAccounts> AccountList = (from p in dc.TblCompanyUsers.AsEnumerable()
                                                                                      join q in dc.TblUserTypes on p.UserTypeId equals Convert.ToInt32(q.UserTypeId)
                                                                                      where p.Active == true && p.EmailAddress == UserObj.EmailAddress
                                                                                      select new MultipleAccounts()
                                                                                      {
                                                                                          UserType = q.Title,
                                                                                          CompanyUserId = p.CompanyUserId
                                                                                      }).ToList();

                                                #endregion

                                                lobjResponse.ContactNo = UserObj.ContactNo;
                                                lobjResponse.NotifyType = Convert.ToInt16(UserObj.NotifyType);

                                                lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);

                                                lobjResponse.MultipleAccounts = AccountList;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                lobjResponse.CompanyId = Obj.CompanyId;
                                                lobjResponse.LoggedInUserId = UserObj.CompanyUserId;
                                                lobjResponse.IsPAPDetails = IsPAPDetails;
                                                lobjResponse.DbCon = Obj.DBCon;
                                                lobjResponse.StagesCovered = Obj.StagesCovered;
                                                lobjResponse.CompanyName = Obj.CompanyName;
                                                lobjResponse.UserTypeId = Convert.ToDecimal(UserObj.UserTypeId);
                                                lobjResponse.Name = UserObj.FirstName;
                                                lobjResponse.UniqueCode = Obj.UniqueCode;
                                                lobjResponse.InActive = InActive;
                                                lobjResponse.IsCorporation = Convert.ToBoolean(Obj.IsCorporation);
                                                lobjResponse.UserToken = UserToken;

                                         }

                                        }
                                        else
                                        {

                                            lobjResponse.Message = "Please input correct Username/Password.";
                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            lobjResponse.InActive = false;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        lobjResponse.Message =ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                    CreateResponseLog(dc, StartTime, lobjResponse.CompanyId, "LoginUser", InputJson, lobjResponse.Message);
                                    dc.Connection.Close();
                                }

                            }
                            else
                            {
                                lobjResponse.Message = "Company has been blocked by admin due to certain reasons.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.InActive = true;
                            }

                            lobjResponse.CompanyRegDate = Convert.ToString(Obj.DateCreated);
                        }
                        else
                        {
                            lobjResponse.Message = "Please enter correct company code.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.InActive = true;
                        }

                    }
                    else
                    {
                        lobjResponse.Message = "Please input correct Username.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.InActive = true;
                    }
                   
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
               
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }



        #endregion LoginUser

        #region AddEditEmployess
        public CResponse AddEditEmployess(AddEditEmployessInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            string ErrorString = "";
            string ErrorString1 = "";
            decimal ToTalInitialAmt = 0;
            decimal TotalRecurringAmt = 0;
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            EflexMsgDBDataContext MsgContext = new EflexMsgDBDataContext();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.DBCon == iObj.DbCon select p).Take(1).SingleOrDefault();
                if (CompanyObj != null)
                {
                    CompanyObj.NoOfAdmins = 0;
                    CompanyObj.NoOfEmployee = 0;
                    UniqueCode = CompanyObj.UniqueCode;
                    CompanyObj.StagesCovered = "4";
                    TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                    if(CompPaymentObj != null)
                    {
                        CompPaymentObj.Active = false;
                    }
                    TblCompAdjustment CompanyAdjObj = (from p in mastercontext.TblCompAdjustments where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                    if (CompanyAdjObj != null)
                    {
                        CompanyAdjObj.Active = false;
                    }
                    List<TblBrokerPayment> BrokerPaymentObj = (from p in mastercontext.TblBrokerPayments where p.Active == true && p.CompanyId == CompanyId select p).ToList();
                    if(BrokerPaymentObj != null && BrokerPaymentObj.Count() > 0)
                    {
                        BrokerPaymentObj.ForEach(x => x.Active = false);
                    }
                    mastercontext.SubmitChanges();
                }

                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        if (IsValidateUser(datacontext))
                        {
                            #region Add Admin
                            if (iObj.AdminList != null && iObj.AdminList.Count() > 0)
                            {
                                List<TblCompanyUser> AdminUserList = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId != Convert.ToDecimal(CObjects.enmUserType.Owner) select p).ToList();
                                if (AdminUserList != null && AdminUserList.Count() > 0)
                                {
                                    AdminUserList.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();


                                    List<TblMsgUser> MsgUserList = (from p in MsgContext.TblMsgUsers where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.UserTypeId != Convert.ToDecimal(CObjects.enmUserType.Owner)  select p).ToList();
                                    if (MsgUserList != null && MsgUserList.Count() > 0)
                                    {
                                        MsgUserList.ForEach(x => x.Active = false);
                                        MsgContext.SubmitChanges();
                                    }

                                }
                                
                                foreach (AdminList item in iObj.AdminList)
                                {
                                    if (item.CompanyUserId == -1)
                                    {
                                        string Password = RandomString();
                                        string EncryptPwd = CryptorEngine.Encrypt(Password);
                                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.EmailAddress == item.Email && p.UserTypeId == item.UserTypeId select p).Take(1).SingleOrDefault();
                                        if (UserObj == null)
                                        {
                                            decimal GetLatestId = 0;
                                            GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                            if (GetLatestId == 0)
                                            {
                                                GetLatestId = 1;
                                            }
                                            else
                                            {
                                                GetLatestId = GetLatestId + 1;
                                            }

                                            TblCompanyUser Obj = new TblCompanyUser()
                                            {
                                                FirstName = item.FName,
                                                LastName = item.LName,
                                                UserName = UniqueCode + "_" + item.FName + GetLatestId.ToString(),
                                                ContactNo = item.ContactNo,
                                                Pwd = EncryptPwd,
                                                UserTypeId = Convert.ToInt16(item.UserTypeId),
                                                EmailAddress = item.Email,
                                                IsActive = true,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                DateModified = DateTime.UtcNow
                                            };
                                            datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                            datacontext.SubmitChanges();
                                            //insertion in Msg DB
                                            UpdateMsgUser(Obj, "ADD", CompanyObj.CompanyId);

                                            CompanyObj.NoOfAdmins = CompanyObj.NoOfAdmins + 1;
                                            mastercontext.SubmitChanges();

                                                                                    }
                                        else
                                        {
                                            ErrorString1 = item.Email;
                                        }
                                    }

                                }



                            }
                            #endregion Add Admin

                            if (ErrorString1 == "")
                            {
                                List<TblCompanyUser> EmpUserList = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee) select p).ToList();
                                if (EmpUserList != null && EmpUserList.Count() > 0)
                                {
                                    EmpUserList.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();
                                }
                                List<TblEmployee> EmpList = (from p in datacontext.TblEmployees where p.Active == true select p).ToList();
                                if (EmpList != null && EmpList.Count() > 0)
                                {
                                    EmpList.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();
                                }
                                List<TblEmpBalance> EmpBalanceList1 = (from p in datacontext.TblEmpBalances where p.Active == true select p).ToList();
                                if (EmpBalanceList1 != null && EmpBalanceList1.Count() > 0)
                                {
                                    EmpBalanceList1.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();
                                }

                                List<TblEmpTransaction> EmpTransactionList1 = (from p in datacontext.TblEmpTransactions where p.Active == true select p).ToList();
                                if (EmpTransactionList1 != null && EmpTransactionList1.Count() > 0)
                                {
                                    EmpTransactionList1.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();
                                }
                                List<TblPayment> PaymentList = (from p in datacontext.TblPayments where p.Active == true select p).ToList();
                                if (PaymentList != null && PaymentList.Count() > 0)
                                {
                                    PaymentList.ForEach(x => x.Active = false);
                                    datacontext.SubmitChanges();
                                }


                                List<TblMsgUser> MsgUserList = (from p in MsgContext.TblMsgUsers where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee) select p).ToList();
                                if (MsgUserList != null && MsgUserList.Count() > 0)
                                {
                                    MsgUserList.ForEach(x => x.Active = false);
                                    MsgContext.SubmitChanges();
                                }

                                #region Add Employees
                                if (iObj.EmployeesList != null && iObj.EmployeesList.Count() > 0)
                                {
                                    List<ClasswisePayments> ClasswisePaymentsObj = new List<ClasswisePayments>();
                                    List<TblEmpBalance> EmpPaymentList = new List<TblEmpBalance>();
                                    List<TblEmpTransaction> EmpTransactionList = new List<TblEmpTransaction>();
                                    
                                    //add employee
                                    DateTime? PaymentDate = null;
                                    #region Set Payment date


                                    TblCompPayment CompanyPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                    if (CompanyPaymentObj != null)
                                    {
                                        if (CompanyPaymentObj.PaymentDate >= DateTime.UtcNow)
                                        {
                                            PaymentDate = CompanyPaymentObj.PaymentDate;
                                        }
                                        else
                                        {
                                            DateTime now = DateTime.UtcNow;
                                            var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                            var SevenDT = new DateTime(now.Year, now.Month, 7);
                                            DateTime date = DateTime.Now;

                                            DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                            string dayToday = DayOnTwelve.ToString();

                                            // compare enums
                                            if (DayOnTwelve == DayOfWeek.Saturday)
                                            {
                                                PaymentDate = TweleveDT.Date.AddDays(2);
                                            }
                                            else if (DayOnTwelve == DayOfWeek.Sunday)
                                            {
                                                PaymentDate = TweleveDT.Date.AddDays(1);
                                            }
                                            else
                                            {
                                                PaymentDate = TweleveDT.Date;
                                            }


                                            if (now.Date > PaymentDate)
                                            {
                                                PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        DateTime now = DateTime.UtcNow;
                                        var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                        var SevenDT = new DateTime(now.Year, now.Month, 7);
                                        DateTime date = DateTime.Now;

                                        DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                        string dayToday = DayOnTwelve.ToString();

                                        // compare enums
                                        if (DayOnTwelve == DayOfWeek.Saturday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(2);
                                        }
                                        else if (DayOnTwelve == DayOfWeek.Sunday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(1);
                                        }
                                        else
                                        {
                                            PaymentDate = TweleveDT.Date;
                                        }


                                        if (now.Date > PaymentDate)
                                        {
                                            PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                        }

                                    }

                                    #endregion


                                    foreach (EmployeesList item in iObj.EmployeesList)
                                    {
                                        if (item.CompanyUserId == -1)
                                        {
                                            string Password = RandomString();
                                            string EncryptPwd = CryptorEngine.Encrypt(Password);
                                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.EmailAddress == item.Email && p.UserTypeId == item.UserTypeId select p).Take(1).SingleOrDefault();
                                            if (UserObj == null)
                                            {
                                                decimal GetLatestId = 0;
                                                GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                                if (GetLatestId == 0)
                                                {
                                                    GetLatestId = 1;
                                                }
                                                else
                                                {
                                                    GetLatestId = GetLatestId + 1;
                                                }
                                                TblCompanyUser Obj = new TblCompanyUser()
                                                {
                                                    FirstName = item.FName,
                                                    LastName = item.LName,
                                                    UserName = (UniqueCode + "_" + item.FName) + GetLatestId.ToString(),
                                                    ContactNo = item.ContactNo,
                                                    Pwd = EncryptPwd,
                                                    UserTypeId = Convert.ToInt16(item.UserTypeId),
                                                    EmailAddress = item.Email,
                                                    IsActive = true,
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow
                                                };
                                                datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                                datacontext.SubmitChanges();
                                                //insertion in Msg DB
                                                UpdateMsgUser(Obj, "ADD", CompanyObj.CompanyId);


                                                TblEmployee EmpObj = new TblEmployee()
                                                {
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    CompanyPlanId = item.CompanyPlanId,
                                                    FirstName = item.FName,
                                                    LastName = item.LName,
                                                    MobileNo = item.ContactNo,
                                                    EmailAddress = item.Email,
                                                    IsActive = true,
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow
                                                };
                                                datacontext.TblEmployees.InsertOnSubmit(EmpObj);
                                                datacontext.SubmitChanges();
                                                CompanyObj.NoOfEmployee = CompanyObj.NoOfEmployee + 1;
                                                CompanyObj.StagesCovered = "4";
                                                mastercontext.SubmitChanges();

                                                #region Payments
                                                
                                                TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();
                                                if (PlanObj != null)
                                                {
                                                    //add employee transaction without tax
                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                    {
                                                        TransactionDate = PaymentDate,
                                                        TransactionDetails = "Initial Payment",
                                                        Amount = PlanObj.NetDeposit,
                                                        CompanyUserId = Obj.CompanyUserId,
                                                        CompanyPlanId = item.CompanyPlanId,
                                                        ClaimId = -1,
                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                                        PaymentTypeId = CObjects.enumPaymentType.IN.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow
                                                    };


                                                    EmpTransactionList.Add(EmpTransactionObj);
                                                    decimal BalanceAmount = 0;
                                                    
                                                    #region Apply account setUp fee
                                                    TblFeeDetail FeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.Title.ToUpper() == "SETUP FEE/INITIAL FEE" select p).Take(1).SingleOrDefault();
                                                    if (FeeObj != null)
                                                    {
                                                        TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                                        {
                                                            TransactionDate = DateTime.UtcNow,
                                                            TransactionDetails = "Account Set-up Fee",
                                                            Amount = FeeObj.Amount,
                                                            CompanyUserId = Obj.CompanyUserId,
                                                            CompanyPlanId = item.CompanyPlanId,
                                                            ClaimId = -1,
                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                            PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                            Active = true,
                                                            DateCreated = DateTime.UtcNow
                                                        };
                                                        EmpTransactionList.Add(EmpTransactionObj1);

                                                        TblEmpAdjustment EmpAdjObj = new TblEmpAdjustment()
                                                        {
                                                            CompanyUserId = Obj.CompanyUserId,
                                                            Amount = FeeObj.Amount,
                                                            AdjustmentDT = DateTime.UtcNow,// PaymentDate,
                                                            Remarks = "Account Set-up Fee",
                                                            TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                            Active = true,
                                                            DateCreated = DateTime.UtcNow,
                                                            DateModified = DateTime.UtcNow
                                                        };
                                                        datacontext.TblEmpAdjustments.InsertOnSubmit(EmpAdjObj);
                                                        BalanceAmount = Convert.ToDecimal(-FeeObj.Amount);
                                                    }
                                                    #endregion


                                                    #region Payments
                                                    //add employee payment with tax
                                                    TblPayment PaymentObj1 = new TblPayment()
                                                    {
                                                        PaymentDate = DateTime.UtcNow,
                                                        PaymentDetails = "Initial Payment",
                                                        RecurringDeposit = PlanObj.RecurringDeposit,
                                                        InitialDeposit = PlanObj.InitialDeposit,
                                                        NetDeposit = PlanObj.NetDeposit,
                                                        AdministrationInitialFee = PlanObj.AdministrationInitialFee,
                                                        AdministrationMonthlyFee = PlanObj.AdministrationMonthlyFee,
                                                        HSTInitial = PlanObj.HSTInitial,
                                                        HSTMonthly = PlanObj.HSTMonthly,
                                                        PremiumTaxInitial = PlanObj.PremiumTaxInitial,
                                                        PremiumTaxMonthly = PlanObj.PremiumTaxMonthly,
                                                        RetailSalesTaxInitial = PlanObj.RetailSalesTaxInitial,
                                                        RetailSalesTaxMonthly = PlanObj.RetailSalesTaxMonthly,
                                                        GrossTotalInitial = PlanObj.GrossTotalInitial,
                                                        GrossTotalMonthly = PlanObj.GrossTotalMonthly,
                                                        Amount = PlanObj.GrossTotalInitial + PlanObj.GrossTotalMonthly + PlanObj.NetDeposit,
                                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                                        CompanyUserId = Obj.CompanyUserId,
                                                        PaymentDueDate = DateTime.UtcNow,
                                                        PaymentTypeId = CObjects.enumPaymentType.IN.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow,
                                                        IsRemit = false
                                                    };


                                                    datacontext.TblPayments.InsertOnSubmit(PaymentObj1);

                                                    TblEmpBalance EmpPaymentObj = new TblEmpBalance()
                                                    {
                                                        Amount = BalanceAmount,
                                                        CompanyUserId = Obj.CompanyUserId,
                                                        Active = true,
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        DateModified = DateTime.UtcNow,
                                                        DateCreated = DateTime.UtcNow
                                                    };

                                                    datacontext.TblEmpBalances.InsertOnSubmit(EmpPaymentObj);
                                                    datacontext.SubmitChanges();


                                                    #endregion
                                                }
                                                #endregion Payments

                                              
                                            }
                                            else
                                            {
                                                if (ErrorString == "")
                                                {
                                                    ErrorString = item.Email + ",";
                                                }
                                                else
                                                {
                                                    ErrorString = ErrorString + "," + item.Email + ",";
                                                }
                                            }
                                        }


                                    }

                                    if (EmpTransactionList != null && EmpTransactionList.Count() > 0)
                                    {
                                        datacontext.TblEmpTransactions.InsertAllOnSubmit(EmpTransactionList);
                                        datacontext.SubmitChanges();
                                    }

                                    decimal OwnerId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();
                                    decimal AmountToBepaid = (from p in datacontext.TblPayments where p.Active == true select Convert.ToDecimal(p.Amount)).Sum();

                                    

                                    TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                    if (CompPaymentObj != null)
                                    {
                                        CompPaymentObj.Amount = CompPaymentObj.Amount + AmountToBepaid;
                                    }
                                    else
                                    {
                                        TblCompPayment CompPaymentObj2 = new TblCompPayment()
                                        {
                                            PaymentDueDate = PaymentDate,
                                            PaymentDate = PaymentDate,
                                            PaymentDetails = "Initial Payment",
                                            Amount = AmountToBepaid,
                                            CompanyId = CompanyObj.CompanyId,
                                            CompanyUserId = OwnerId,
                                            PaymentTypeId = CObjects.enumPaymentType.IN.ToString(),
                                            TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                            Active = true,
                                            DateCreated = DateTime.UtcNow
                                        };
                                        mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj2);
                                        mastercontext.SubmitChanges();
                                        TblCompAdjustment AdjObj = new TblCompAdjustment()
                                        {
                                            CompanyId = CompanyObj.CompanyId,
                                            Amount = AmountToBepaid,
                                            CompanyPaymentId = CompPaymentObj2.CompPaymentId,
                                            PaymentTypeId = CObjects.enumPaymentType.IN.ToString(),
                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                            AdjustmentDT = DateTime.UtcNow,
                                            Remarks = "Initial Payment",
                                            TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                            Active = true,
                                            DateCreated = DateTime.UtcNow,
                                            DateModified = DateTime.UtcNow
                                        };
                                        mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                                    }
                                    mastercontext.SubmitChanges();

                                    #region Broker Commission
                                    TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                       where p.Active == true && p.CompanyId == CompanyObj.CompanyId
                                                                       select p).Take(1).SingleOrDefault();
                                    if (CompReferral != null)
                                    {
                                        
                                        decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                        where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                        select Convert.ToDecimal(p.NetDeposit)).Sum();

                                        if (CompReferral.BrokerId != -1)
                                        {
                                            TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                                   where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                                   select p).Take(1).SingleOrDefault();
                                            TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                          where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyObj.CompanyId
                                                                          select p).Take(1).SingleOrDefault();
                                            decimal PercentageBrokerCharge = 0;
                                            if (FeeObj != null)
                                            {
                                                if (FeeObj.BrokerFee > 0)
                                                {
                                                    PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                                }
                                            }
                                            else
                                            {
                                                if(BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                                {
                                                    PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                                }
                                                else
                                                {
                                                    PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                                }
                                            }


                                            if(PercentageBrokerCharge > 0)
                                            {
                                                decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                                TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                                   where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                                   p.UserId == BrokerObj.UserId
                                                                                   select p).Take(1).SingleOrDefault();
                                                if (TransactionObj != null)
                                                {
                                                    TransactionObj.Amount = Amount;
                                                    TransactionObj.Contribution = BrokerAmountToBepaid;
                                                    TransactionObj.PercentFee = PercentageBrokerCharge;
                                                }
                                                else
                                                {
                                                    TblBrokerPayment TransactionObj1 = new TblBrokerPayment()
                                                    {
                                                        PaymentDate = DateTime.UtcNow,
                                                        PaymentDueDate = DateTime.UtcNow,
                                                        PaymentDetails = "Payments",
                                                        Amount = Amount,
                                                        CompanyId = CompanyObj.CompanyId,
                                                        Contribution = BrokerAmountToBepaid,
                                                        PercentFee = PercentageBrokerCharge,
                                                        UserId = BrokerObj.UserId,
                                                        PaymentTypeId = CObjects.enumPaymentType.DM.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow
                                                    };
                                                    mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj1);
                                                }
                                                mastercontext.SubmitChanges();
                                            }
                                        }
                                    }
                                    #endregion Broker Commission
                                }
                                #endregion

                                if (ErrorString != "")
                                {
                                    ErrorString = ErrorString.TrimEnd(',');
                                    lobjResponse.Message = "We couldn't add all user because some email ids are already used by other users (" + ErrorString + ").";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                                else
                                {
                                    lobjResponse.Message = "SUCCESS";
                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                }
                            }
                            else
                            {
                                lobjResponse.Message = "This email id already used by owner .";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }

                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                            lobjResponse.Message =  CObjects.SessionExpired.ToString();
                            IsLogOut = true;

                        }

                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    lobjResponse.IsLogOut = IsLogOut;
                    CreateResponseLog(datacontext, StartTime, UserId, "AddEditEmployess", "", lobjResponse.Message);
                    datacontext.Connection.Close();
                }
                mastercontext.Connection.Close();

            }

            return lobjResponse;
        }
        #endregion AddEditEmployess

        #region PopulateClasses
        public CPopulateClassesResponse PopulateClasses(DbConInput iObj)
        {
            CPopulateClassesResponse lobjResponse = new CPopulateClassesResponse();
            string Constring = GetConnection(iObj.DbCon);
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    List<ClassList> Obj = (from p in datacontext.TblCompanyPlans
                                           where p.Active == true
                                           select new ClassList()
                                           {
                                               ClassId = p.CompanyPlanId,
                                               Name = p.Name
                                           }).Distinct().ToList();
                    lobjResponse.ClassList = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "PopulateClasses", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion PopulateClasses

        #region ChangePassword
        //JK:270518 
        public CResponse ChangePassword(ChangePasswordInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            decimal UserId = iObj.UserId;
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId && p.Pwd == CryptorEngine.Encrypt(iObj.PrevPassword) select p).FirstOrDefault();
                    if (UserObj != null)
                    {
                        string EncryptPwd = CryptorEngine.Encrypt(iObj.NewPassword);
                        UserObj.Pwd = EncryptPwd;
                        UserObj.DateCreated = DateTime.UtcNow;
                        datacontext.SubmitChanges();

                        if (UserObj.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                        {
                            using (MasterDBDataContext dc = new MasterDBDataContext())
                            {
                                TblCompany Obj = (from p in dc.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                                if (Obj != null)
                                {
                                    Obj.Pwd = EncryptPwd;
                                    dc.SubmitChanges();
                                }
                                dc.Connection.Close();
                            }
                        }


                        #region SendMail

                        
                        StringBuilder builder = new StringBuilder();
                        builder.Append("Hello " + UserObj.FirstName + ",<br/><br/>");
                        builder.Append("Your password has been updated successfully.<br/><br/>");
                        builder.Append("Thanks, and enjoy your eFlex account(s)!<br/>");
                        builder.Append("The eclipse EIO Team</h4>");

                        string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                        lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                        CMail Mail = new CMail();
                        Mail.EmailTo = UserObj.EmailAddress;
                        Mail.Subject = "Password Changed";
                        Mail.MessageBody = lstrMessage;
                        Mail.GodaddyUserName = GodaddyUserName;
                        Mail.GodaddyPassword = GodaddyPwd;
                        Mail.Server = Server;
                        Mail.Port = Port;
                        Mail.CompanyId = iObj.CompanyId;
                        bool IsSent = Mail.SendEMail(datacontext, true);


                        #endregion SendMail

                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Message = "Invalid Old Password.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateResponseLog(datacontext, StartTime, -1, "ChangePassword", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }

        #endregion ChangePassword

        #region ForgotPassword
        //JK:270518
        public CResponse ForgotPassword(ForgetPasswordInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            decimal UserId = -1;
            CResponse lobjResponse = new CResponse();
            string UniqueCode = iObj.UserName.Split('_')[0];
            string DbCon = "";
            using (MasterDBDataContext dc = new MasterDBDataContext())
            {
                DbCon = (from p in dc.TblCompanies where p.Active == true && p.UniqueCode == UniqueCode select p.DBCon).Take(1).SingleOrDefault();
                if (!string.IsNullOrEmpty(DbCon))
                {
                    string Constring = GetConnection(DbCon);
                    using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                    {
                        try
                        {
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserName == iObj.UserName select p).FirstOrDefault();
                            if (UserObj != null)
                            {
                                UserId = Convert.ToDecimal(UserObj.CompanyUserId);
                                StringBuilder builder = new StringBuilder();
                                
                                builder.Append("Dear " + UserObj.FirstName + ",<br/><br/>");
                                builder.Append("Following are your login credentials:<br/><br/>");
                                builder.Append("Company Code: " + UniqueCode + "<br/><br/>");
                                builder.Append("Username: " + UserObj.UserName.Split('_')[1] + "<br/><br/>");
                                builder.Append("Password: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                                
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                //Send Mail
                                CMail Mail = new CMail();
                                Mail.EmailTo = UserObj.EmailAddress;
                                Mail.Subject = "eFlex : Retrieve Password";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                bool IsSent = Mail.SendEMail(dc, true);

                                if (IsSent)
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                }
                                else
                                {
                                    lobjResponse.Message = "Currently server is facing some problem sending email. Please try after some time.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }

                            }
                            else
                            {
                                lobjResponse.Message = "This username doesn't exist.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            InputJson = new JavaScriptSerializer().Serialize(iObj);
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                            lobjResponse.Message = ex.Message;
                        }

                        CreateResponseLog(datacontext, StartTime, UserId, "ForgotPassword", InputJson, lobjResponse.Message);
                        datacontext.Connection.Close();
                    }
                }
                else
                {
                    lobjResponse.Message = "Please enter valid company code.";
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                }
                dc.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion ForgotPassword

        #region ListEmployees
        public CListEmployeesResponse ListEmployees(ListEmployeesInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CListEmployeesResponse lobjResponse = new CListEmployeesResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    List<ListEmployees> Obj = new List<ListEmployees>();
                    List<ListEmployees> Result = new List<ListEmployees>();
                   if(iObj.Id == 1 || iObj.Id ==2)
                 {
                     Obj = (from p in datacontext.TblCompanyUsers where p.Active == true && 
                                (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                                                select new ListEmployees()
                                                {
                                                    CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                    FName = p.FirstName,
                                                    LName = p.LastName,
                                                    UserName = p.UserName,
                                                    ContactNo = p.ContactNo,
                                                    Email = p.EmailAddress,
                                                    InActive = Convert.ToString(p.IsActive),
                                                   CompanyPlanId =-1,
                                                    IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                                                }).ToList();

                 }
                 else
                 {
                     Obj = (from p in datacontext.TblEmployees
                            join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                            join r in datacontext.TblCompanyPlans on p.CompanyPlanId equals r.CompanyPlanId
                            where p.Active == true && q.Active == true &&
                             (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                            select new ListEmployees()
                            {
                                CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                FName = p.FirstName,
                                LName = p.LastName,
                                UserName = q.UserName,
                                ContactNo = q.ContactNo,
                                Email = p.EmailAddress,
                                InActive = Convert.ToString(p.IsActive),
                                InActiveDate = Convert.ToString(p.InActiveDate),
                                PlanName = r.Name,
                                CompanyPlanId = Convert.ToDecimal(p.CompanyPlanId),
                                IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                            }).ToList();
                 }
                    
                 if (Obj != null && Obj.Count > 0)
                 {
                     if (iObj.ChunkStart == -1)
                     {
                         Result = Obj.OrderByDescending(i => i.CompanyUserId).Take(iObj.ChunkSize).ToList();
                     }
                     else
                     {
                         Result = Obj.OrderByDescending(i => i.CompanyUserId).Where(i => i.CompanyUserId < iObj.ChunkSize).Take(iObj.ChunkSize).ToList();
                     }
                 }
                    lobjResponse.ListEmployees = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateResponseLog(datacontext, StartTime, iObj.Id, "ListEmployees", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddEditCompanyFinancialDetails
        public CResponse AddCompanyFinancialDetails(AddEditCompanyFinancialDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.CompanyUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        decimal CompanyUserId = -1;
                        if (iObj.CompanyUserId == -1)
                        {
                            CompanyUserId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();

                        }
                        else
                        {
                            CompanyUserId = iObj.CompanyUserId;
                        }

                        if (iObj.CompFinancialDetailId == -1)
                        {
                            TblPAPDetail Obj1 = (from p in datacontext.TblPAPDetails where p.Active == true && p.CompanyUserId == CompanyUserId select p).Take(1).SingleOrDefault();
                            if (Obj1 == null)
                            {
                                TblPAPDetail Obj = new TblPAPDetail()
                                {
                                    InitialPayment = iObj.InitialPayment,
                                    RecurringPayment = iObj.RecurringPayment,
                                    CompanyUserId = CompanyUserId,
                                    BankName = iObj.BankName,
                                    InstitutionId = iObj.InstitutionNumber,
                                    TransitId = iObj.TransitNumber,
                                    AccountNumber = iObj.AccountNumber,
                                    IsCheque = iObj.IsCheque,
                                    ChequeFile = iObj.FileName,
                                    IsAgree = iObj.IsAgree,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblPAPDetails.InsertOnSubmit(Obj);
                                datacontext.SubmitChanges();
                            }
                            else
                            {
                                    Obj1.BankName = iObj.BankName;
                                    Obj1.InstitutionId = iObj.InstitutionNumber;
                                    Obj1.TransitId = iObj.TransitNumber;
                                    Obj1.AccountNumber = iObj.AccountNumber;
                                    datacontext.SubmitChanges();

                            }

                            if (iObj.CompanyId != -1)
                            {
                                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                                {
                                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                                    if (CompanyObj != null)
                                    {
                                        CompanyObj.StagesCovered = "5";
                                        mastercontext.SubmitChanges();
                                    }
                                    mastercontext.Connection.Close();
                                }
                            }

                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "Financial details added successfully";
                        }
                        else
                        {
                            TblPAPDetail FinancialObj = (from p in datacontext.TblPAPDetails where p.Active == true && p.PAPDetailId == iObj.CompFinancialDetailId select p).Take(1).SingleOrDefault();
                            if (FinancialObj != null)
                            {
                                FinancialObj.InstitutionId = iObj.InstitutionNumber;
                                FinancialObj.TransitId = iObj.TransitNumber;
                                FinancialObj.AccountNumber = iObj.AccountNumber;
                                FinancialObj.IsCheque = iObj.IsCheque;
                                FinancialObj.ChequeFile = iObj.FileName;
                                FinancialObj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "Financial details updated successfully";
                            }

                        }



                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "AddEditCompanyFinancialDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetRandomCode
        public CGetRandomCodeResponse GetRandomCode()
        {
            DateTime StartTime = DateTime.UtcNow;

            string InputJson = "";
            CGetRandomCodeResponse lobjResponse = new CGetRandomCodeResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                Random r = new Random();
                Int32 RandomCode = r.Next(100000, 999999);
                TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.UniqueCode == "EF" + Convert.ToString(RandomCode) select p).Take(1).SingleOrDefault();
                if (CompanyObj == null)
                {
                    string UniqueCode = "EF" + Convert.ToString(RandomCode);
                    lobjResponse.Message = "SUCCESS";
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.RandomCode = UniqueCode;
                }
                else
                {
                    GetRandomCode();
                }
                CreateMasterResponseLog(datacontext, StartTime, -1, "GetRandomCode", InputJson, lobjResponse.Message);
                
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetRandomCode

        

        #region GetPaymentOverview
        public CGetPaymentOverviewResponse GetPaymentOverview(DbConInput iObj)
        {
            CGetPaymentOverviewResponse lobjResponse = new CGetPaymentOverviewResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                List<ClasswisePayments> ClasswisePaymentsObj = new List<ClasswisePayments>();
                List<TblCompanyPlan> Planlist = (from p in datacontext.TblCompanyPlans where p.Active == true select p).ToList();
                if (Planlist != null && Planlist.Count() > 0)
                {
                    #region Payment Overview
                    foreach (TblCompanyPlan item in Planlist)
                    {
                        decimal MemberCount = (from p in datacontext.TblEmployees where p.Active == true && p.IsActive == true && p.CompanyPlanId == item.CompanyPlanId select p.EmployeeId).Count();

                        #region Initial amount
                        decimal TotalInitial = 0;
                        decimal PlanGrossInitialAmt = (from p in datacontext.TblCompanyPlans
                                                       where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                       select Convert.ToDecimal(p.GrossTotalInitial ?? 0)).Sum();
                        decimal PlanInitialAmt = (from p in datacontext.TblCompanyPlans
                                                  where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                  select Convert.ToDecimal(p.InitialDeposit ?? 0)).Sum();

                        TotalInitial = PlanGrossInitialAmt + PlanInitialAmt;
                        #endregion Initial amount

                        #region Recurring amount
                        decimal TotalRecurringAmt = 0;

                        decimal PlanGrossRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                         where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                         select Convert.ToDecimal(p.GrossTotalMonthly ?? 0)).Sum();
                        decimal PlanRecurringAmt = (from p in datacontext.TblCompanyPlans
                                                    where p.Active == true && p.CompanyPlanId == item.CompanyPlanId
                                                    select Convert.ToDecimal(p.RecurringDeposit ?? 0)).Sum();

                        TotalRecurringAmt = PlanGrossRecurringAmt + PlanRecurringAmt;


                        #endregion Recurring amount

                        decimal TotalInitialOfAllMembers = 0;
                        decimal TotalRecurringOfAllMembers = 0;

                        if (MemberCount > 0)
                        {
                            TotalInitialOfAllMembers = MemberCount * (TotalInitial);
                            TotalRecurringOfAllMembers = MemberCount * (TotalRecurringAmt);
                        }

                        
                        ClasswisePayments PaymentOverviewObj = new ClasswisePayments()
                        {
                            PlanId = item.CompanyPlanId,
                            PlanName = item.Name,
                            NoOfEmployees = MemberCount,
                            PlanGrossInitial = TotalInitialOfAllMembers,
                            PlanGrossRecurring = TotalRecurringOfAllMembers,
                            Color = (item.Color == null || item.Color == "") ? "#ffbb33" : item.Color,
                        };

                        ClasswisePaymentsObj.Add(PaymentOverviewObj);

                    }

                    #endregion Payment Overview

                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.Message = "SUCCESS";
                    lobjResponse.ClasswisePayments = ClasswisePaymentsObj;
                    lobjResponse.NetGrossInitial = ClasswisePaymentsObj.Select(x => x.PlanGrossInitial).Sum();
                    lobjResponse.NetGrossRecurring = ClasswisePaymentsObj.Select(x => x.PlanGrossRecurring).Sum();

                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    lobjResponse.Message = "No record found.";
                }

                CreateResponseLog(datacontext, StartTime, -1, "GetPaymentOverview", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }

            return lobjResponse;
        }
        #endregion GetPaymentOverview

        #region InActiveEmployee
        public CResponse InActiveEmployeeOld(InActiveEmployeeInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {

                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            EmpObj.InActiveDate = DateTime.UtcNow;
                            EmpObj.IsActive = iObj.IsActive;
                            TblCompanyUser CompUserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (CompUserObj != null)
                            {
                                CompUserObj.IsActive = false;
                                CompUserObj.InActiveDate = DateTime.UtcNow;
                                using (EflexMsgDBDataContext msgcontext = new EflexMsgDBDataContext())
                                {
                                    TblMsgUser MsgUserObj = (from p in msgcontext.TblMsgUsers where p.Active == true && p.OrigUserId == CompUserObj.CompanyUserId && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                    if (MsgUserObj != null)
                                    {
                                        MsgUserObj.IsActive = iObj.IsActive;
                                        msgcontext.SubmitChanges();
                                    }

                                }



                            }

                            TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == EmpObj.CompanyPlanId select p).Take(1).SingleOrDefault();
                            if (PlanObj != null)
                            {
                                TblPayment PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.CompanyPlanId == EmpObj.CompanyPlanId && p.CompanyUserId == iObj.CompanyUserId && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (PaymentObj != null)
                                {
                                    PaymentObj.Active = false;
                                }
                                Hashtable HashPlanDesign = CObjects.HashPlanDesign();
                                if (PlanObj.Termination == (int)HashPlanDesign["Revert back to the employer"])
                                {
                                    TblEmpTransaction EmpPaymentObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.CompanyPlanId == EmpObj.CompanyPlanId select p).Take(1).SingleOrDefault();
                                    if (EmpPaymentObj != null)
                                    {

                                     
                                        string Text = "Your account has been deactivated by your company. For further queries , Please contact with your company.";
                                        NotifyUser1(datacontext, Text, iObj.CompanyUserId, CObjects.enmNotifyType.Account.ToString());
                      
                                    
                                        datacontext.SubmitChanges();
                                    }




                                }

                            }
                            datacontext.SubmitChanges();

                            #region SendMail

                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                            string EmailTo = "";
                            TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                            if (CarrierObj != null)
                            {
                                string NotiText = "Your account has been deactivated by your company. You can access your account only for 30 days from now. For further queries , Please contact with your company.";
                                string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                               
                                CMail Mail1 = new CMail();
                                Mail1.EmailTo = CarrierEmails;
                                Mail1.Subject = "Account Inactive";
                                Mail1.MessageBody = NotiText;
                                Mail1.GodaddyUserName = GodaddyUserName;
                                Mail1.GodaddyPassword = GodaddyPwd;
                                Mail1.Server = Server;
                                Mail1.Port = Port;
                                Mail1.CompanyId = CompanyId;
                                bool IsSent1 = Mail1.SendEMail(datacontext, true);
                            }
                            if (CompUserObj.EmailAddress != null && CompUserObj.EmailAddress != "")
                            {
                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + CompUserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = CompUserObj.EmailAddress;
                                }
                            }
                            if (!string.IsNullOrEmpty(EmailTo))
                            {
                                
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + EmpObj.FirstName + " " + EmpObj.LastName + ",<br/><br/>");
                                builder.Append("Your account has been deactivated by your company. You can access your account only for 30 days from now. For further queries , Please contact with your company.<br/><br/>");
                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                            
                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;
                                Mail.Subject = "Account Inactive";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);

                            }

                            #endregion SendMail

                            using (MasterDBDataContext Master = new MasterDBDataContext())
                            {
                                TblCompanyUser CompUserObj1 = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                                string Text = CompUserObj.FirstName + " " + CompUserObj.LastName + " employee is deactivated by " + CompUserObj1.FirstName + " " + CompUserObj1.LastName + "";
                                CreateHistoryLog(Master, CompanyId, iObj.LoggedInUserId, CompUserObj.CompanyUserId, Text);

                                string ForUserIds = String.Join(",", (from p in Master.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                EflexNotifyLog(Master, DateTime.UtcNow, Text, ForUserIds);
                                Master.Connection.Close();
                            }


                            lobjResponse.Message = "SUCCESS";
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            CreateResponseLog(datacontext, StartTime, iObj.CompanyUserId, "InActiveEmployee", InputJson, lobjResponse.Message);
                        }
                        else
                        {
                            lobjResponse.Message = "Fail";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "InActiveEmployeeOld", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion

        #region UpdatePlanDetails
        public CResponse UpdatePlanDetails(UpdatePlanDetailsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateUser(datacontext))
                    {
                        TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == iObj.PlanId select p).Take(1).SingleOrDefault();
                        if (PlanObj != null)
                        {
                            DateTime PlanStDt = Convert.ToDateTime(PlanObj.PlanStartDt);
                            
                            decimal PreviousRecurring = Convert.ToDecimal(PlanObj.RecurringDeposit);
                            decimal PreviousRecurringInculdingTax = Convert.ToDecimal(PlanObj.RecurringDeposit) + Convert.ToDecimal(PlanObj.GrossTotalMonthly);
                            if(PlanObj.PlanStartDt.Value.Date != Convert.ToDateTime(iObj.PlanStartDt).Date)
                            {
                                string Text = PlanObj.Name + " plan start date has been changed from " + PlanStDt.ToString("MM/dd/yyyy")  +" to " + Convert.ToDateTime(iObj.PlanStartDt).ToString("MM/dd/yyyy") + " .";
                                CreateHistoryLog(mastercontext, CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);
                               
                            }


                            PlanObj.Name = iObj.Name;
                            PlanObj.ContributionSchedule = iObj.ContributionSchedule;
                            PlanObj.RecurringDeposit = iObj.RecurringDeposit;
                            PlanObj.InitialDeposit = iObj.InitialDeposit;
                            PlanObj.NetDeposit = iObj.NetDeposit;
                            PlanObj.AdministrationInitialFee = iObj.AdministrationInitialFee;
                            PlanObj.AdministrationMonthlyFee = iObj.AdministrationMonthlyFee;
                            PlanObj.HSTInitial = iObj.HSTInitial;
                            PlanObj.HSTMonthly = iObj.HSTMonthly;
                            PlanObj.PremiumTaxInitial = iObj.PremiumTaxInitial;
                            PlanObj.PremiumTaxMonthly = iObj.PremiumTaxMonthly;
                            PlanObj.RetailSalesTaxInitial = iObj.RetailSalesTaxInitial;
                            PlanObj.RetailSalesTaxMonthly = iObj.RetailSalesTaxMonthly;
                            PlanObj.GrossTotalInitial = iObj.GrossTotalInitial;
                            PlanObj.GrossTotalMonthly = iObj.GrossTotalMonthly;
                            PlanObj.PlanDesign = iObj.PlanDesign;
                            PlanObj.RollOver = iObj.RollOver;
                            PlanObj.CarryForwardPeriod = iObj.CarryForwardPeriod;
                            PlanObj.Termination = iObj.Termination;
                            PlanObj.DateModified = DateTime.UtcNow;
                            decimal PlanTotalAmt = (iObj.GrossTotalInitial + iObj.GrossTotalMonthly) + iObj.NetDeposit;
                            datacontext.SubmitChanges();
                            List<TblEmployee> EmpList = (from p in datacontext.TblEmployees where p.Active == true && p.IsActive == true && p.CompanyPlanId == iObj.PlanId select p).ToList();
                            if (EmpList.Count() > 0 && EmpList != null)
                            {
                                foreach (TblEmployee item in EmpList)
                                {
                                    TblPayment PaymentObj1 = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == item.CompanyUserId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) && p.CompanyPlanId == PlanObj.CompanyPlanId select p).Take(1).SingleOrDefault();
                                    if (PaymentObj1 != null)
                                    {
                                        if (PaymentObj1.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())
                                        {
                                            PaymentObj1.RecurringDeposit = iObj.RecurringDeposit;
                                            PaymentObj1.InitialDeposit = iObj.InitialDeposit;
                                            PaymentObj1.NetDeposit = iObj.NetDeposit;
                                            PaymentObj1.AdministrationInitialFee = iObj.AdministrationInitialFee;
                                            PaymentObj1.AdministrationMonthlyFee = iObj.AdministrationMonthlyFee;
                                            PaymentObj1.HSTInitial = iObj.HSTInitial;
                                            PaymentObj1.HSTMonthly = iObj.HSTMonthly;
                                            PaymentObj1.PremiumTaxInitial = iObj.PremiumTaxInitial;
                                            PaymentObj1.PremiumTaxMonthly = iObj.PremiumTaxMonthly;
                                            PaymentObj1.RetailSalesTaxInitial = iObj.RetailSalesTaxInitial;
                                            PaymentObj1.RetailSalesTaxMonthly = iObj.RetailSalesTaxMonthly;
                                            PaymentObj1.GrossTotalInitial = iObj.GrossTotalInitial;
                                            PaymentObj1.GrossTotalMonthly = iObj.GrossTotalMonthly;
                                            PaymentObj1.Amount = PlanTotalAmt;
                                            TblEmpTransaction EmpPayment = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == item.CompanyUserId && p.CompanyPlanId == iObj.PlanId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                            if (EmpPayment != null)
                                            {
                                                
                                                if (EmpPayment.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())
                                                {
                                                    EmpPayment.Amount = iObj.NetDeposit;
                                                }
                                                else
                                                {
                                                    EmpPayment.Amount = iObj.RecurringDeposit;

                                                }

                                                datacontext.SubmitChanges();
                                            }

                                            TblEmpTransaction EmpTransactionObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyPlanId == iObj.PlanId && p.CompanyUserId == item.CompanyUserId select p).Take(1).SingleOrDefault();
                                            if (EmpTransactionObj != null)
                                            {
                                                EmpTransactionObj.Amount = iObj.NetDeposit;

                                            }

                                            datacontext.SubmitChanges();
                                        }
                                        else
                                        {
                                            DateTime CurrentDate = DateTime.UtcNow;
                                            DateTime FifthDayOfMonth = new DateTime(CurrentDate.Year, CurrentDate.Month, 5);
                                            DateTime TwelveDayOfMonth = new DateTime(CurrentDate.Year, CurrentDate.Month, 12);
                                            
                                            TblPayment CompPaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == item.CompanyUserId && (Convert.ToDateTime(p.PaymentDate).Date >= FifthDayOfMonth.Date && Convert.ToDateTime(p.PaymentDate).Date <= TwelveDayOfMonth.Date) && p.CompanyPlanId == iObj.PlanId select p).SingleOrDefault();
                                            if (CompPaymentObj != null)
                                            {
                                                PaymentObj1.RecurringDeposit = iObj.RecurringDeposit;
                                                PaymentObj1.InitialDeposit = iObj.InitialDeposit;
                                                PaymentObj1.NetDeposit = iObj.NetDeposit;
                                                PaymentObj1.AdministrationInitialFee = iObj.AdministrationInitialFee;
                                                PaymentObj1.AdministrationMonthlyFee = iObj.AdministrationMonthlyFee;
                                                PaymentObj1.HSTInitial = iObj.HSTInitial;
                                                PaymentObj1.HSTMonthly = iObj.HSTMonthly;
                                                PaymentObj1.PremiumTaxInitial = iObj.PremiumTaxInitial;
                                                PaymentObj1.PremiumTaxMonthly = iObj.PremiumTaxMonthly;
                                                PaymentObj1.RetailSalesTaxInitial = iObj.RetailSalesTaxInitial;
                                                PaymentObj1.RetailSalesTaxMonthly = iObj.RetailSalesTaxMonthly;
                                                PaymentObj1.GrossTotalInitial = iObj.GrossTotalInitial;
                                                PaymentObj1.GrossTotalMonthly = iObj.GrossTotalMonthly;
                                                PaymentObj1.Amount = PlanTotalAmt;

                                                
                                                TblEmpTransaction EmpPayment = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == item.CompanyUserId && p.CompanyPlanId == iObj.PlanId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                                if (EmpPayment != null)
                                                {
                                                    
                                                    if (EmpPayment.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())
                                                    {
                                                        EmpPayment.Amount = iObj.NetDeposit;
                                                    }
                                                    else
                                                    {
                                                        EmpPayment.Amount = iObj.RecurringDeposit;

                                                    }

                                                    datacontext.SubmitChanges();
                                                }

                                                TblEmpTransaction EmpTransactionObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyPlanId == iObj.PlanId && p.CompanyUserId == item.CompanyUserId select p).Take(1).SingleOrDefault();
                                                if (EmpTransactionObj != null)
                                                {
                                                    EmpTransactionObj.Amount = iObj.NetDeposit;

                                                }

                                                datacontext.SubmitChanges();

                                                string NotifyText = "Hi, your plan amount has been changed $" + PaymentObj1.Amount + ".";
                                                NotifyUser1(datacontext, NotifyText, item.CompanyUserId, CObjects.enmNotifyType.Plan.ToString());
                                                List<TblNotificationDetail> NotificationUserList1 = null;
                                                NotificationUserList1 = InsertionInNotificationTable(item.CompanyUserId, NotifyText, datacontext, item.CompanyUserId, CObjects.enmNotifyType.Plan.ToString());
                                                if (NotificationUserList1 != null)
                                                {
                                                    NotificationList.AddRange(NotificationUserList1);
                                                    NotificationUserList1.Clear();
                                                }
                                                
                                                datacontext.SubmitChanges();


                                            }
                                        }
                                    }

                                }


                                #region Push Notifications


                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, PlanObj.CompanyPlanId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications
                            }

                            lobjResponse.Message = "SUCCESS";
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "UpdatePlanDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdatePlanDetails

        #region UpdatePaymentDate
        public CResponse UpdatePaymentDate(UpdatePaymentDateInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() select p).ToList();
                    if (PaymentObj != null && PaymentObj.Count() > 0)
                    {

                        MasterDBDataContext mastercontext = new MasterDBDataContext();
                        TblCompPayment CompPaymentobj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() select p).Take(1).SingleOrDefault();
                        if (CompPaymentobj != null)
                        {
                            CompPaymentobj.PaymentDate = Convert.ToDateTime(iObj.PaymentDate);
                            CompPaymentobj.PaymentDueDate = Convert.ToDateTime(iObj.PaymentDate);
                            mastercontext.SubmitChanges();
                        }

                        TblBrokerPayment BrokerPaymentobj = (from p in mastercontext.TblBrokerPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString() select p).Take(1).SingleOrDefault();
                        if (BrokerPaymentobj != null)
                        {
                            BrokerPaymentobj.PaymentDate = Convert.ToDateTime(iObj.PaymentDate);
                            BrokerPaymentobj.PaymentDueDate = Convert.ToDateTime(iObj.PaymentDate);
                            mastercontext.SubmitChanges();
                        }
                        
                        List<TblEmpTransaction> EmpTransactionList = (from p in datacontext.TblEmpTransactions
                                                                      where p.Active == true
                                                                      select p).ToList();
                        if (EmpTransactionList != null && EmpTransactionList.Count() > 0)
                        {
                            EmpTransactionList.ForEach(x => x.TransactionDate = Convert.ToDateTime(iObj.PaymentDate));

                        }

                        PaymentObj.ForEach(x => { x.PaymentDate = Convert.ToDateTime(iObj.PaymentDate); x.PaymentDueDate = Convert.ToDateTime(iObj.PaymentDate); });
                        datacontext.SubmitChanges();
                        
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "UpdatePaymentDate", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }

            return lobjResponse;
        }
        #endregion UpdatePaymentDate

        #region SwitchToAnotherAccounts
        public CLoginUserResponse SwitchToAnotherAccounts(SwitchToAnotherAccountsInput iObj)
        {

            DateTime StartTime = DateTime.UtcNow;
            CLoginUserResponse lobjResponse = new CLoginUserResponse();
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {

                try
                {
                    UserId = iObj.CompanyUserId;
                    string Constring = GetConnection(iObj.DbCon);
                    bool InActive = true;
                    using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                    {
                        if (IsValidateUser(dc))
                        {
                            TblCompanyUser UserObj = (from p in dc.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (UserObj != null)
                            {
                                if (UserObj.IsActive == true)
                                {
                                    TblCompanyLoginLog logObj;

                                    logObj = new TblCompanyLoginLog()
                                    {
                                        CompanyUserId = UserObj.CompanyUserId,
                                        Action = "Login from web",
                                        DateCreated = DateTime.UtcNow,
                                        Active = true
                                    };
                                    dc.TblCompanyLoginLogs.InsertOnSubmit(logObj);
                                    dc.SubmitChanges();
                                    if (UserObj.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee))
                                    {
                                        TblEmployee EmpObj = (from p in dc.TblEmployees where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                        if (EmpObj != null)
                                        {
                                            InActive = Convert.ToBoolean(EmpObj.IsActive);
                                            #region Payment Overview
                                            decimal PlanAmt = 0;
                                            
                                            TblEmpBalance EmpPaymentObj = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                            if (EmpPaymentObj != null)
                                            {
                                                PlanAmt = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                                           
                                              
                                            }
                                            lobjResponse.AmountDue = PlanAmt;
                                            #endregion Payment Overview

                                        }
                                    }
                                    else
                                    {
                                        #region Payment Overview
                                        decimal PlanAmt = 0;
                                        TblPayment PaymentObj = (from p in dc.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                        if (PaymentObj != null)
                                        {
                                            PlanAmt = (from p in dc.TblPayments
                                                       where p.Active == true &&
                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                       select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                            
                                        }

                                        lobjResponse.AmountDue = PlanAmt;

                                        #endregion Payment Overview
                                    }

                                    #region PAPDetails
                                    bool IsPAPDetails = false;
                                    TblPAPDetail PAPOBj = (from p in dc.TblPAPDetails where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                    if (PAPOBj != null)
                                    {
                                        IsPAPDetails = true;
                                    }
                                    else
                                    {
                                        IsPAPDetails = false;
                                    }
                                    #endregion

                                    #region Check Multiple accounts of logged in user

                                    List<MultipleAccounts> AccountList = (from p in dc.TblCompanyUsers.AsEnumerable()
                                                                          join q in dc.TblUserTypes on p.UserTypeId equals Convert.ToInt32(q.UserTypeId)
                                                                          where p.Active == true && p.EmailAddress == UserObj.EmailAddress
                                                                          select new MultipleAccounts()
                                                                              {
                                                                                  UserType = q.Title,
                                                                                  CompanyUserId = p.CompanyUserId
                                                                              }).ToList();

                                    #endregion
                                    lobjResponse.MultipleAccounts = AccountList;
                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                    
                                }
                                else
                                {
                                    lobjResponse.Message = "Sorry! You could not login, because you are terminated by company.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                                }
                            }
                            else
                            {
                                lobjResponse.Message = "Please input correct Username/Password.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.InActive = true;

                            }
                            CreateResponseLog(dc, StartTime, lobjResponse.CompanyId, "SwitchToAnotherAccounts", InputJson, lobjResponse.Message);
                            dc.Connection.Close();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                            lobjResponse.Message =  CObjects.SessionExpired.ToString();
                            IsLogOut = true;

                        }
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(datacontext, StartTime, -1, "SwitchToAnotherAccounts", InputJson, lobjResponse.Message);

                datacontext.Connection.Close();
            }
            return lobjResponse;
        }

        #endregion

        #region SaveCompanyReferrals
        //discuss
        public CResponse SaveCompanyReferrals(SaveCompanyReferralsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.CompanyUserId;
            string Constring = GetConnection(iObj.DbCon);

            try
            {
                MasterDBDataContext mastercontext = new MasterDBDataContext();
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        List<TblCompanyReferral> ListObj = (from p in datacontext.TblCompanyReferrals where p.Active == true && p.CompanyId == iObj.CompanyId select p).ToList();
                        if (ListObj == null && ListObj.Count() > 0)
                        {
                            ListObj.ForEach(x => x.Active = false);
                            List<TblBrokerCompanyFee> BrokerCompList = (from p in mastercontext.TblBrokerCompanyFees where p.Active == true && p.CompanyId == iObj.CompanyId select p).ToList();

                            if (BrokerCompList == null && BrokerCompList.Count() > 0)
                            {
                                BrokerCompList.ForEach(x => x.Active = false);
                                mastercontext.SubmitChanges();
                            }

                            datacontext.SubmitChanges();
                        }

                        TblCompanyReferral Obj = (from p in datacontext.TblCompanyReferrals where p.Active == true && p.CompanyId == iObj.CompanyId && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (Obj == null)
                        {
                            TblCompanyReferral Obj1 = new TblCompanyReferral()
                            {
                                CompanyId = iObj.CompanyId,
                                CompanyUserId = iObj.CompanyUserId,
                                BrokerId = iObj.BrokerId,
                                ReferralTypeId = iObj.ReferralTypeId,
                                Name = iObj.Name,
                                OtherDetails = iObj.OtherDetails,
                                Active = true,
                                DateCreated = DateTime.UtcNow
                            };
                            datacontext.TblCompanyReferrals.InsertOnSubmit(Obj1);

                            if (iObj.ReferralTypeId == 1)//broker
                            {
                                if (iObj.BrokerId == -1)
                                {

                                    TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                    if (AdminFeeObj != null)
                                    {
                                        string Password = RandomString();
                                        string EncryptPwd = CryptorEngine.Encrypt(Password);
                                        TblUser UserObj1 = new TblUser()
                                        {
                                            FirstName = iObj.Name,
                                            UserName = iObj.Name,
                                            Pwd = EncryptPwd,
                                            UserTypeId = Convert.ToInt16(CObjects.enumMasterUserType.Broker),
                                            Active = true,
                                            DateCreated = DateTime.UtcNow,
                                            DateModified = DateTime.UtcNow
                                        };
                                        mastercontext.TblUsers.InsertOnSubmit(UserObj1);
                                        mastercontext.SubmitChanges();

                                        TblBroker BrokerObj = new TblBroker()
                                        {
                                            FirstName = iObj.Name,
                                            UserId = UserObj1.UserId,
                                            Active = true,
                                            IsApproved = false,
                                            IsPersonal = true,
                                            DateCreated = DateTime.UtcNow,
                                            DateModified = DateTime.UtcNow
                                        };
                                        mastercontext.TblBrokers.InsertOnSubmit(BrokerObj);
                                        mastercontext.SubmitChanges();

                                        TblBrokerCompanyFee BrokerCompFeeObj = new TblBrokerCompanyFee()
                                        {
                                            CompanyId = iObj.CompanyId,
                                            AdminFee = AdminFeeObj.AdminMin,
                                            BrokerFee = AdminFeeObj.BrokerMin,
                                            BrokerId = BrokerObj.BrokerId,
                                            Active = true,
                                            DateCreated = DateTime.UtcNow
                                        };
                                        mastercontext.TblBrokerCompanyFees.InsertOnSubmit(BrokerCompFeeObj);

                                        TblBrokerCompany BrokerCompObj = new TblBrokerCompany()
                                        {
                                            CompanyId = iObj.CompanyId,
                                            BrokerId = BrokerObj.BrokerId,
                                            Active = true,
                                            DateCreated = DateTime.UtcNow
                                        };
                                        mastercontext.TblBrokerCompanies.InsertOnSubmit(BrokerCompObj);
                                        mastercontext.SubmitChanges();
                                        Obj1.BrokerId = BrokerCompFeeObj.BrokerId;
                                        datacontext.SubmitChanges();
                                    }
                                }
                                if (iObj.BrokerId > 0)
                                {
                                    TblBroker BrokerObj = (from p in mastercontext.TblBrokers where p.Active == true && p.BrokerId == iObj.BrokerId select p).Take(1).SingleOrDefault();
                                    if (BrokerObj != null)
                                    {
                                        TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                        if (AdminFeeObj != null)
                                        {

                                            TblBrokerCompanyFee BrokerCompFeeObj = new TblBrokerCompanyFee()
                                            {
                                                CompanyId = iObj.CompanyId,
                                                AdminFee = AdminFeeObj.AdminMin,
                                                BrokerFee = BrokerObj.CommissionCharges != null ? BrokerObj.CommissionCharges : AdminFeeObj.BrokerMin,
                                                BrokerId = iObj.BrokerId,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow
                                            };
                                            mastercontext.TblBrokerCompanyFees.InsertOnSubmit(BrokerCompFeeObj);

                                            TblBrokerCompany BrokerCompObj = new TblBrokerCompany()
                                            {
                                                CompanyId = iObj.CompanyId,
                                                BrokerId = iObj.BrokerId,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow
                                            };
                                            mastercontext.TblBrokerCompanies.InsertOnSubmit(BrokerCompObj);
                                            mastercontext.SubmitChanges();
                                        }

                                        
                                        StringBuilder builder = new StringBuilder();
                                        builder.Append("Hello,<br/><br/>");
                                        builder.Append("A new company registered with the reference of external broker named as: " + BrokerObj.FirstName + ".<br/><br/>");
                                        builder.Append("Regards,<br/><br/>");
                                        builder.Append("eFlex Team<br/><br/>");

                                        
                                        //send mail
                                        string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                        lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                        CMail Mail = new CMail();
                                        Mail.EmailTo = "test@eclipseeio.com";
                                        Mail.Subject = "New Company Registered";
                                        Mail.MessageBody = lstrMessage;
                                        Mail.GodaddyUserName = GodaddyUserName;
                                        Mail.GodaddyPassword = GodaddyPwd;
                                        Mail.Server = Server;
                                        Mail.Port = Port;
                                        
                                        bool IsSent = Mail.SendEMail(datacontext, true);
                                    }

                                 
                                }
                            }
                            else
                            {
                                TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                if (AdminFeeObj != null)
                                {
                                    TblBrokerCompanyFee BrokerCompObj = new TblBrokerCompanyFee()
                                    {
                                        CompanyId = iObj.CompanyId,
                                        AdminFee = AdminFeeObj.AdminMax,
                                        BrokerFee = 0,
                                        BrokerId = iObj.BrokerId,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    mastercontext.TblBrokerCompanyFees.InsertOnSubmit(BrokerCompObj);
                                    mastercontext.SubmitChanges();
                                }
                            }


                        }
                        else
                        {
                            Obj.ReferralTypeId = iObj.ReferralTypeId;
                            Obj.Name = iObj.Name;
                        }







                        datacontext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                    }
                    CreateResponseLog(datacontext, StartTime, UserId, "SaveCompanyReferrals", InputJson, lobjResponse.Message);
                    datacontext.Connection.Close();
                }
                mastercontext.Connection.Close();
            }
            catch (Exception ex)
            {
                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }

            return lobjResponse;
        }

        #endregion

        #region PopulateReferrals
        public CPopulateReferralsResponse PopulateReferrals(DbConInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CPopulateReferralsResponse lobjResponse = new CPopulateReferralsResponse();
            if (iObj.DbCon != "")
            {
                string Constring = GetConnection(iObj.DbCon);
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        List<PopulateReferralsData> Obj = (from p in datacontext.TblReferralTypes
                                                           where p.Active == true
                                                           select new PopulateReferralsData()
                                                           {
                                                               ReferralTypeId = p.ReferralTypeId,
                                                               ReferralType = p.Title
                                                           }).Distinct().ToList();
                        lobjResponse.PopulateReferralsData = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    datacontext.Connection.Close();
                }
            }
            else
            {
                try
                {
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    List<PopulateReferralsData> Obj = (from p in mastercontext.TblMasterReferralTypes
                                                       where p.Active == true
                                                       select new PopulateReferralsData()
                                                       {
                                                           ReferralTypeId = p.MasterReferralTypeId,
                                                           ReferralType = p.Title
                                                       }).Distinct().ToList();
                    lobjResponse.PopulateReferralsData = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
               
            }
            return lobjResponse;
        }
        #endregion PopulateReferrals

        #region PopulateBrokers
        public CPopulateBrokersResponse PopulateBrokers()
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            CPopulateBrokersResponse lobjResponse = new CPopulateBrokersResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    List<PopulateBrokers> Obj = (from p in datacontext.TblBrokers
                                                 where p.Active == true && p.IsApproved == true
                                                 select new PopulateBrokers()
                                                       {
                                                           BrokerId = Convert.ToDecimal(p.BrokerId),
                                                           Name = p.FirstName + ((p.LastName == "" || p.LastName == null) ? "" : " " + p.LastName)
                                                       }).Distinct().ToList();
                    lobjResponse.PopulateBrokers = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(datacontext, StartTime, -1, "PopulateBrokers", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion PopulateReferrals

        #region GetCompanyHistoryLog
        public CResponseGetCompanyHistoryLog GetCompanyHistoryLog(GetCompanyHistoryLogInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponseGetCompanyHistoryLog lobjResponse = new CResponseGetCompanyHistoryLog();

            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    if (iObj.Year != -1 && iObj.Month != -1)
                    {
                        string FirstDate = "" + iObj.Year + "/" + iObj.Month + "/01";

                        DateTime Date = Convert.ToDateTime(FirstDate);
                        var LastDayOfMonth = Date.AddMonths(1).AddDays(-1);
                        if (iObj.ChunckStart == -1)
                        {
                            lobjResponse.HistoryData = (from a in mastercontext.Tblhistories
                                                        where a.Active == true && a.CompanyId == iObj.CompanyId && a.DateCreated.Value.Date >= Date.Date
                                                        && a.DateCreated.Value.Date <= LastDayOfMonth.Date
                                                        orderby a.HistoryId descending
                                                        select
                                                            new HistoryLog
                                                            {
                                                                historyId = a.HistoryId,
                                                                DateCreated = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                                Text = a.Historytext
                                                            }).Take(iObj.ChunckSize).ToList();
                        }
                        else
                        {
                            lobjResponse.HistoryData = (from a in mastercontext.Tblhistories
                                                        where a.Active == true && a.CompanyId == iObj.CompanyId && a.DateCreated.Value.Date >= Date.Date && a.HistoryId < iObj.ChunckStart
                                                        && a.DateCreated.Value.Date <= LastDayOfMonth.Date
                                                        orderby a.HistoryId descending
                                                        select
                                                            new HistoryLog
                                                            {
                                                                historyId = a.HistoryId,
                                                                DateCreated = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                                Text = a.Historytext
                                                            }).Take(iObj.ChunckSize).ToList();
                        }

                    }
                    else
                    {
                        lobjResponse.HistoryData = (from a in mastercontext.sp_ListCompanyhistorylog(iObj.ChunckStart, iObj.ChunckSize, iObj.CompanyId)
                                                    select new HistoryLog
                                                    {
                                                        historyId = a.HistoryId,
                                                        DateCreated = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                        Text = a.Historytext

                                                    }).ToList();
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateMasterResponseLog(mastercontext, StartTime, iObj.CompanyId, "GetCompanyHistoryLog", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region InActiveEmployee
        public CResponse InActiveEmployee(InActiveEmployeeInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {

                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            EmpObj.InActiveDate = DateTime.UtcNow.AddMonths(1);
                            EmpObj.IsActive = iObj.IsActive;
                            TblCompanyUser CompUserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (CompUserObj != null)
                            {
                                CompUserObj.IsActive = false;
                                CompUserObj.InActiveDate = DateTime.UtcNow.AddMonths(1);
                                using (EflexMsgDBDataContext msgcontext = new EflexMsgDBDataContext())
                                {
                                    TblMsgUser MsgUserObj = (from p in msgcontext.TblMsgUsers where p.Active == true && p.OrigUserId == CompUserObj.CompanyUserId && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                    if (MsgUserObj != null)
                                    {
                                        MsgUserObj.IsActive = iObj.IsActive;
                                        msgcontext.SubmitChanges();
                                    }

                                }

                                TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                if (CompPaymentObj != null)
                                {

                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == EmpObj.CompanyPlanId select p).Take(1).SingleOrDefault();

                                    if (PlanObj != null)
                                    {
                                        TblPayment PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                       if (PaymentObj != null)
                                        {
                                            CompPaymentObj.Amount = CompPaymentObj.Amount - PaymentObj.Amount;
                                            PaymentObj.Active = false;

                                           
                                            TblEmpTransaction TranObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                            if (TranObj != null)
                                            {
                                                TranObj.Active = false;
                                            
                                            }

                                            if (CompPaymentObj.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())
                                            {
                                                TblEmpBalance Balance = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                if (Balance != null)
                                                {
                                                    Balance.Amount = 0;

                                                }
                                            }
                                            mastercontext.SubmitChanges();
                                            datacontext.SubmitChanges();
                                        }
            
                                       

                                    }


                                }
                                
                                #region SendMail
                                string EmailTo = "";
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    string NotiText = "Effective today, your eFlex account will no longer be sponsored.  You will have 30 days to submit any pending claims up to your account balance. On day 30, your account will no longer be active and your remaining balance will be unavailable for use. If you have any questions, please email eflex@eclipseeio.com.";
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;
                                    Mail1.Subject = "Account Deactivated";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);

                                }
                                if (CompUserObj.EmailAddress != null && CompUserObj.EmailAddress != "")
                                {
                                    if (!string.IsNullOrEmpty(EmailTo))
                                    {
                                        EmailTo = EmailTo + "," + CompUserObj.EmailAddress;
                                    }
                                    else
                                    {
                                        EmailTo = CompUserObj.EmailAddress;
                                    }
                                }
                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    StringBuilder builder = new StringBuilder();
                                    builder.Append("Hi " + EmpObj.FirstName + " " + EmpObj.LastName + ",<br/><br/>");
                                    builder.Append("Effective today, your eFlex account will no longer be sponsored.  You will have 30 days to submit any pending claims up to your account balance. On day 30, your account will no longer be active and your remaining balance will be unavailable for use.<br/><br/>");
                                    builder.Append("If you have any questions, please email eflex@eclipseeio.com<br/><br/>");
                                    builder.Append("Thank you,<br/><br/>");
                                    builder.Append("The eFlex Team<br/><br/>");

                                    //send mail
                                    string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                    lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                 
                                    CMail Mail = new CMail();
                                    Mail.EmailTo = EmailTo;
                                    Mail.Subject = "Account Deactivated";
                                    Mail.MessageBody = lstrMessage;
                                    Mail.GodaddyUserName = GodaddyUserName;
                                    Mail.GodaddyPassword = GodaddyPwd;
                                    Mail.Server = Server;
                                    Mail.Port = Port;
                                    Mail.CompanyId = CompanyId;
                                    bool IsSent = Mail.SendEMail(datacontext, true);

                                }

                                #endregion SendMail

                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            }
                        }
                        else
                        {
                            lobjResponse.Message = "Fail";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                        
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "InActiveEmployee", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetBrokerFeeSetByEflex
        /// <summary>
        /// For Broker Only
        /// </summary>
        /// <param name="iObj"></param>
        /// <returns></returns>
        public CGetBrokerFeeSetByEflexResponse GetBrokerFeeSetByEflex(IdInput iObj)
        {
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CGetBrokerFeeSetByEflexResponse lobjResponse = new CGetBrokerFeeSetByEflexResponse();
            DateTime StartTime = DateTime.UtcNow;
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    TblAdminFeeDetail AdminFeeObj = (from p in datacontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                    if (AdminFeeObj != null)
                    {
                        lobjResponse.BrokerFee = Convert.ToDecimal(AdminFeeObj.BrokerMax);
                        TblBroker FeeObj = (from p in datacontext.TblBrokers where p.Active == true && p.UserId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (FeeObj != null)
                        {
                            lobjResponse.BrokerCommissionFee = FeeObj.CommissionCharges == null ? 0 : Convert.ToDecimal(FeeObj.CommissionCharges);
                        }
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Message = "Fail";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(datacontext, StartTime, UserId, "GetBrokerFeeSetByEflex", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetNearbyServiceProvider
        public CGetNearbyServiceProviderResponse GetNearbyServiceProvider(GetNearbyServiceProviderInput iObj)
        {
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            DateTime CurrentTime = DateTime.UtcNow;
            CGetNearbyServiceProviderResponse lobjResponse = new CGetNearbyServiceProviderResponse();
            string struri = "";
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            bool IsLogOut = false;
            UserId = iObj.UserId;
            string GoogleKey = WebConfigurationManager.AppSettings["GoogleApiKey"].ToString();

            struri = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + iObj.LatLog + "&radius=15000&type=" + iObj.Type + "&keyword=" + iObj.Keyword + "&key=" + GoogleKey;
            HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(struri)) as HttpWebRequest;
            webRequest.Method = System.Net.WebRequestMethods.Http.Get;

            try
            {

                string Constring = GetConnection(iObj.DbCon);
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {

                    if (IsValidateUser(datacontext))
                    {
                        HttpWebResponse webresponse;
                        webresponse = (HttpWebResponse)webRequest.GetResponse();
                        StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                        string text = sr.ReadToEnd().ToString();
                        dynamic results = JsonConvert.DeserializeObject<dynamic>(text);
                        JObject arr = (JObject)results;
                        List<ServiceProviderList> Obj = new List<ServiceProviderList>();
                        if (arr["results"].Count() > 0)
                        {
                            foreach (var item in arr["results"])
                            {
                                string vicinity = "";
                                if (item["vicinity"] != null)
                                {
                                    vicinity = item["vicinity"].ToString();
                                }

                                Obj.Add(new ServiceProviderList()
                                    {
                                        Name = item["name"].ToString(),
                                        Address = vicinity,
                                        UniqueId = item["place_id"].ToString(),
                                        Type = "NearBy",
                                        Email = "",
                                        ContactNo = "",
                                        ServiceProviderId = -1
                                    });
                            }
                        }

                        List<ServiceProviderList> Obj1 = new List<ServiceProviderList>();
                        List<ServiceProviderList> RecentProvider = new List<ServiceProviderList>();

                        Obj1 = (from p in datacontext.TblEmpClaims.AsEnumerable()
                                join q in mastercontext.TblServiceProviders on p.ServiceProviderId equals q.ServiceProviderId
                                join r in mastercontext.TblServices on q.ServiceId equals r.ServiceId
                                where p.Active == true && p.CompanyUserId == iObj.UserId && (q.Name.ToUpper().Contains(iObj.Keyword.ToUpper()) || r.Title.ToUpper().Contains(iObj.Keyword.ToUpper()))
                                && q.ServiceId == iObj.ServiceId
                                select new ServiceProviderList()
                                    {
                                        Name = q.Name,
                                        Address = q.Address,
                                        UniqueId = q.GoogleId,
                                        Type = "Recent",
                                        Email = q.Email,
                                        ContactNo = q.ContactNo,
                                        ServiceProviderId = q.ServiceProviderId
                                    }).Distinct().GroupBy(x => x.Name).Select(y => y.First()).ToList();



                        List<ServiceProviderList> RecentlyAddedProvider = new List<ServiceProviderList>();
                        RecentlyAddedProvider = (from p in mastercontext.TblServiceProviders.AsEnumerable()
                                                 join r in mastercontext.TblServices on p.ServiceId equals r.ServiceId
                                                 where p.Active == true 
                                                 && p.CompanyUserId == iObj.UserId && p.DateCreated.Value.Date == DateTime.UtcNow.Date
                                                 && p.ServiceId == iObj.ServiceId && (p.Name.ToUpper().Contains(iObj.Keyword.ToUpper()) || r.Title.ToUpper().Contains(iObj.Keyword.ToUpper()))
                                                 orderby p.ServiceProviderId descending
                                                 select new ServiceProviderList()
                                                 {
                                                     Name = p.Name,
                                                     Address = p.Address,
                                                     UniqueId = p.GoogleId,
                                                     Type = "Recently Added",
                                                     Email = p.Email,
                                                     ContactNo = p.ContactNo,
                                                     ServiceProviderId = p.ServiceProviderId
                                                 }).Distinct().GroupBy(x => x.Name).Select(y => y.First()).ToList();


                        List<ServiceProviderList> Obj2 = new List<ServiceProviderList>();
                        List<ServiceProviderList> OtherProvider = new List<ServiceProviderList>();
                        List<ServiceProviderList> NearByProvider = new List<ServiceProviderList>();
                        Obj2 = (from p in mastercontext.TblServiceProviders.AsEnumerable()
                                join r in mastercontext.TblServices on p.ServiceId equals r.ServiceId
                                where p.Active == true && p.ServiceId == iObj.ServiceId && (p.Name.ToUpper().Contains(iObj.Keyword.ToUpper()) || r.Title.ToUpper().Contains(iObj.Keyword.ToUpper()))
                                 && p.ServiceId == iObj.ServiceId
                                orderby p.ServiceProviderId descending
                                select new ServiceProviderList()
                                {
                                    Name = p.Name,
                                    Address = p.Address,
                                    UniqueId = p.GoogleId,
                                    Type = "Other",
                                    Email = p.Email,
                                    ContactNo = p.ContactNo,
                                    ServiceProviderId = p.ServiceProviderId
                                }).Distinct().GroupBy(x => x.Name).Select(y => y.First()).ToList();
                        OtherProvider = Obj2.Where(p => !Obj1.Any(q => q.ServiceProviderId == p.ServiceProviderId)).ToList();
                        NearByProvider = Obj.Where(p => !Obj2.Any(q => q.UniqueId == p.UniqueId)).ToList();
                        OtherProvider = OtherProvider.Where(p => !RecentlyAddedProvider.Any(q => q.ServiceProviderId == p.ServiceProviderId)).Distinct().ToList();
                        RecentProvider = Obj1.Where(p => !OtherProvider.Any(q => q.ServiceProviderId == p.ServiceProviderId)).Distinct().ToList();


                        lobjResponse.NearByServiceProvider = NearByProvider.Distinct().ToList();
                        lobjResponse.RecentlyAddedServiceProvider = RecentlyAddedProvider.Distinct().ToList();
                        lobjResponse.RecentServiceProvider = RecentProvider.Distinct().ToList();
                        lobjResponse.OtherServiceProvider = OtherProvider.Distinct().ToList();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message = "You are logged in from another device.";
                        IsLogOut = true;
                    }
                    datacontext.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }
            CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "GetNearbyServiceProvider", InputJson, lobjResponse.Message);
            mastercontext.Connection.Close();
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }
        #endregion

        #region LoginUserForApp
        //JK:19112018
        public CLoginUserResponse LoginUserForApp(LoginUserForAppInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CLoginUserResponse lobjResponse = new CLoginUserResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {

                try
                {
                    string UniqueCode = "";
                    if (iObj.UserName.Contains("_"))
                    {
                        string Mode = "";
                        UniqueCode = iObj.UserName.Split('_')[0];
                        TblCompany Obj = (from p in datacontext.TblCompanies where p.UniqueCode == UniqueCode && p.Active == true select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            if (IsBlock(Obj.DBCon))
                            {
                                string Constring = GetConnection(Obj.DBCon);
                                bool InActive = true;
                                using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                {
                                    try
                                    {


                                        TblCompanyUser UserObj = (from p in dc.TblCompanyUsers where p.Active == true && p.UserName.Trim() == iObj.UserName.Trim() && p.Pwd == CryptorEngine.Encrypt(iObj.Password) select p).Take(1).SingleOrDefault();
                                        if (UserObj != null)
                                        {
                                            if (UserObj.IsActive == false && UserObj.InActiveDate.Value.AddDays(30) <= DateTime.UtcNow)
                                            {
                                                lobjResponse.Message = "Sorry! You could not login, because you are terminated by company.";
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                lobjResponse.InActive = Convert.ToBoolean(UserObj.IsActive);
                                            }
                                            else
                                            {
                                                TblCompanyLoginLog logObj;

                                                if (iObj.DeviceId == "1")
                                                {
                                                    Mode = "Android";
                                                }
                                                if (iObj.DeviceId == "2")
                                                {
                                                    Mode = "iPhone";
                                                }

                                                ////////////Adding User Device /////////////////////////////////////////////////////////////////////
                                                AddCompanyUserDevice(dc, iObj.DeviceTypeId, UserObj.CompanyUserId, iObj.FCMToken, iObj.Ver, iObj.DeviceId, iObj.DeviceName, iObj.OSVersion);

                                                logObj = new TblCompanyLoginLog()
                                                {
                                                    CompanyUserId = UserObj.CompanyUserId,
                                                    Action = "Login from app",
                                                    DateCreated = DateTime.UtcNow,
                                                    Active = true
                                                };
                                                dc.TblCompanyLoginLogs.InsertOnSubmit(logObj);
                                                dc.SubmitChanges();
                                              
                                                string UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

                                                UserObj.UserToken = UserToken;
                                                dc.SubmitChanges();
                                                if (UserObj.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee))
                                                {
                                                    TblEmployee EmpObj = (from p in dc.TblEmployees where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                    if (EmpObj != null)
                                                    {

                                                        if (EmpObj.IsActive == false && EmpObj.InActiveDate.Value.AddDays(30) >= DateTime.UtcNow)
                                                        {
                                                            InActive = true;
                                                        }
                                                        else
                                                        {
                                                            InActive = Convert.ToBoolean(EmpObj.IsActive);
                                                        }
                                                        #region Payment Overview
                                                        decimal PlanAmt = 0;
                                                        TblEmpBalance EmpPaymentObj = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                        if (EmpPaymentObj != null)
                                                        {
                                                           
                                                            PlanAmt = (from p in dc.TblEmpBalances where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select Convert.ToDecimal(p.Amount ?? 0)).Sum();


                                                            lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);
                                                        }
                                                        lobjResponse.AmountDue = PlanAmt;

                                                        #endregion Payment Overview

                                                        #region Carrier


                                                        TblUserCarrier CarrierObj = (from p in dc.TblUserCarriers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                                        if (CarrierObj != null)
                                                        {
                                                            if (CarrierObj.IsVerified == true)
                                                            {
                                                                string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                                                string lstrMessage = "";
                                                                StringBuilder builder = new StringBuilder();
                                                                builder.Append("You are loggedin into eFlex.<br/><br/>");
                                                                
                                                                lstrMessage = builder.ToString();
                                                                //send mail

                                                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                                                CMail Mail = new CMail();
                                                                Mail.EmailTo = CarrierEmails;
                                                                Mail.Subject = "LoggedIn";
                                                                Mail.MessageBody = lstrMessage;
                                                                Mail.GodaddyUserName = GodaddyUserName;
                                                                Mail.GodaddyPassword = GodaddyPwd;
                                                                Mail.Server = Server;
                                                                Mail.Port = Port;
                                                                Mail.CompanyId = Obj.CompanyId;
                                                                bool IsSent = Mail.SendEMail(dc, true);

                                                            }
                                                            lobjResponse.IsMobileVerified = Convert.ToBoolean(CarrierObj.IsVerified);
                                                            lobjResponse.IsCarrier = true;
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.IsMobileVerified = false;
                                                            lobjResponse.IsCarrier = false;
                                                        }
                                                        #endregion

                                                        TblMasterCard MasterCard = (from p in datacontext.TblMasterCards where p.Active == true && p.CompanyId == Obj.CompanyId && p.UserId == EmpObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                        if (MasterCard != null)
                                                        {
                                                            lobjResponse.IsCardExists = true;
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.IsCardExists = false;

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    #region Payment Overview
                                                    decimal PlanAmt = 0;
                                                    TblPayment PaymentObj = (from p in dc.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                                    if (PaymentObj != null)
                                                    {
                                                        PlanAmt = (from p in dc.TblPayments
                                                                   where p.Active == true &&
                                                                   p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                   select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                                        lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);
                                                    }

                                                    lobjResponse.AmountDue = PlanAmt;

                                                    #endregion Payment Overview
                                                }

                                                #region PAPDetails
                                                bool IsPAPDetails = false;
                                                TblPAPDetail PAPOBj = (from p in dc.TblPAPDetails where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                                if (PAPOBj != null)
                                                {
                                                    IsPAPDetails = true;
                                                }
                                                else
                                                {
                                                    IsPAPDetails = false;
                                                }
                                                #endregion

                                                #region Check Multiple accounts of logged in user

                                                List<MultipleAccounts> AccountList = (from p in dc.TblCompanyUsers.AsEnumerable()
                                                                                      join q in dc.TblUserTypes on p.UserTypeId equals Convert.ToInt32(q.UserTypeId)
                                                                                      where p.Active == true && p.EmailAddress == UserObj.EmailAddress
                                                                                      select new MultipleAccounts()
                                                                                      {
                                                                                          UserType = q.Title,
                                                                                          CompanyUserId = p.CompanyUserId
                                                                                      }).ToList();

                                                #endregion

                                                lobjResponse.NotifyType = Convert.ToInt16(UserObj.NotifyType);
                                                lobjResponse.PlanTypeId = Convert.ToDecimal(Obj.PlanTypeId);
                                                lobjResponse.ContactNo = UserObj.ContactNo;
                                                lobjResponse.Email = UserObj.EmailAddress;
                                                lobjResponse.MultipleAccounts = AccountList;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                lobjResponse.CompanyId = Obj.CompanyId;
                                                lobjResponse.LoggedInUserId = UserObj.CompanyUserId;
                                                lobjResponse.IsPAPDetails = IsPAPDetails;
                                                lobjResponse.DbCon = Obj.DBCon;
                                                lobjResponse.StagesCovered = Obj.StagesCovered;
                                                lobjResponse.CompanyName = Obj.CompanyName;
                                                lobjResponse.UserTypeId = Convert.ToDecimal(UserObj.UserTypeId);
                                                lobjResponse.Name = UserObj.FirstName;
                                                lobjResponse.UniqueCode = Obj.UniqueCode;
                                                lobjResponse.InActive = InActive;
                                                lobjResponse.IsCorporation = Convert.ToBoolean(Obj.IsCorporation);
                                                lobjResponse.UserToken = UserToken;
                                            }

                                        }
                                        else
                                        {

                                            lobjResponse.Message = "Please input correct Username/Password.";
                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            lobjResponse.InActive = false;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                    CreateResponseLog(dc, StartTime, lobjResponse.CompanyId, "LoginUserForApp", InputJson, lobjResponse.Message);
                                    dc.Connection.Close();
                                }

                            }
                            else
                            {
                                lobjResponse.Message = "Company has been blocked by admin due to certain reasons.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.InActive = true;
                            }


                        }
                        else
                        {
                            lobjResponse.Message = "Please enter correct company code.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.InActive = true;
                        }

                    }
                    else
                    {
                        lobjResponse.Message = "Please input correct Username.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.InActive = true;
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }



        #endregion LoginUser

        #region GetCardData
        public CGetCardDataResponse GetCardData(DbConLoggedInUIdInput iObj)
        {
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            CGetCardDataResponse lobjResponse = new CGetCardDataResponse();
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        decimal AccountBalance = 0;
                        bool IsActive = false;
                        bool IsSuspended = false;
                        bool IsExpired = false;
                        decimal DaysRemaining = 0;
                        decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
                        decimal EmpCount = (from p in datacontext.TblEmpBalances
                                            where p.Active == true && p.CompanyUserId == iObj.Id
                                            select p).Count();
                        if (EmpCount > 0)
                        {
                            AccountBalance = (from p in datacontext.TblEmpBalances
                                              where p.Active == true && p.CompanyUserId == iObj.Id
                                              select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                        }
                        List<TblMasterCard> MasterCardList = (from p in mastercontext.TblMasterCards where p.Active == true && p.CompanyId == CompanyId && p.UserId == iObj.Id select p).ToList();
                        if (MasterCardList != null && MasterCardList.Count() > 0)
                        {
                            TblMasterCard CardObj1 = MasterCardList.FirstOrDefault(x => x.IsActive == true);

                            if (CardObj1 != null)
                            {
                                IsActive = true;
                                IsSuspended = false;
                            }
                            else
                            {
                                IsActive = false;
                                IsSuspended = true;
                            }

                        }
                        GetCardData Obj = new GetCardData()
                        {
                            IsActive = Convert.ToBoolean(IsActive),
                            IsSuspended = Convert.ToBoolean(IsSuspended),
                            AccountBalance = AccountBalance < 0 ? 0 : AccountBalance

                        };

                        DaysRemaining = datacontext.sp_GetRemainingDays(iObj.LoggedInUserId).Select(x => Convert.ToDecimal(x.RemainingDays)).FirstOrDefault();
                        IsExpired = datacontext.sp_GetRemainingDays(iObj.LoggedInUserId).Select(x => Convert.ToBoolean(x.IsExpired)).FirstOrDefault();
                        

                        TblEmpClaim EmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.Id && p.DateCreated.Value.Date == DateTime.UtcNow.Date select p).Take(1).SingleOrDefault();
                        if (EmpClaimObj == null)
                        {
                            TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                          where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                              p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                          select p).Take(1).SingleOrDefault();
                            if (FeeDetailsObj != null)
                            {
                                lobjResponse.ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);

                            }

                            lobjResponse.ApplyClaimfee = true;
                        }
                        else
                        {
                            lobjResponse.ClaimFee = 0;
                        }


                        TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                  join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                  where p.Active == true && q.Active == true && q.CompanyUserId == iObj.Id
                                                  select p).Take(1).SingleOrDefault();
                        if (PlanObj != null)
                        {
                            lobjResponse.PlanStartDate = Convert.ToString(PlanObj.PlanStartDt);
                            Hashtable HashPlanDesign = CObjects.HashPlanDesign();
                            if (PlanObj.PlanDesign == (int)HashPlanDesign["A plan that covers medical health,dental and wellness (ex: gym membership, vitamins, etc)"])
                            {
                                lobjResponse.IsCoverAll = true;
                            }
                            else
                            {
                                lobjResponse.IsCoverAll = false;

                            }
                        }
                        lobjResponse.IsExpired = IsExpired;
                        lobjResponse.DaysRemaining = DaysRemaining;
                        lobjResponse.Data = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message = "You are loggedin from another device.";
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateResponseLog(datacontext, CurrentTime, UserId, "GetCardData", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }
        #endregion

        #region UserLogOut
        //JK:271218
        public CResponse UserLogOut(LogOutInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            decimal EventId = -1;
            decimal UserId = iObj.CompanyUserId;
            string ActionTaken = "";
            string ServiceName = "UserLogOut";
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompanyLoginLog LoginLogObj;
                    string Mode = "";
                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                    if (UserObj != null)
                    {
                        UserId = Convert.ToDecimal(UserObj.CompanyUserId);
                        if (iObj.DeviceTypeId == -1)
                        {
                            Mode = "Web";
                            LoginLogObj = new TblCompanyLoginLog()
                            {
                                CompanyUserId = iObj.CompanyUserId,
                                Action = "Logout from web",
                                DateCreated = DateTime.UtcNow,
                                Active = true
                            };
                            datacontext.TblCompanyLoginLogs.InsertOnSubmit(LoginLogObj);
                            datacontext.SubmitChanges();

                        }
                        else
                        {
                            List<TblCompanyUserDevice> DeviceObj = (from p in datacontext.TblCompanyUserDevices where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.DeviceTypeId == iObj.DeviceTypeId && p.FCMToken == iObj.FCMToken && p.DeviceId == iObj.DeviceId select p).ToList();
                            if (DeviceObj != null && DeviceObj.Count > 0)
                            {
                                DeviceObj.ForEach(x => x.Active = false);
                                datacontext.SubmitChanges();
                            }

                            if (iObj.DeviceId == "1")
                            {
                                Mode = "Android";
                            }
                            if (iObj.DeviceId == "2")
                            {
                                Mode = "iPhone";
                            }

                            LoginLogObj = new TblCompanyLoginLog()
                            {
                                CompanyUserId = iObj.CompanyUserId,
                                Action = "Logout from App",
                                DateCreated = DateTime.UtcNow,
                                Active = true
                            };
                            datacontext.TblCompanyLoginLogs.InsertOnSubmit(LoginLogObj);
                            datacontext.SubmitChanges();
                        }


                    }

                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateResponseLog(datacontext, StartTime, UserId, "UserLogOut", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }

        #endregion

        #region AddGetInTouch
        public CResponse AddGetInTouch(AddGetInTouchInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    TblGetInTouch Obj = new TblGetInTouch()
                    {
                        Name = iObj.Name,
                        EmailId = iObj.Email,
                        Message = iObj.Messages,
                        Active = true,
                        DateCreated = DateTime.UtcNow,
                        DateModified = DateTime.UtcNow
                    };
                    datacontext.TblGetInTouches.InsertOnSubmit(Obj);
                    datacontext.SubmitChanges();
                    
                    lobjResponse.Message = "SUCCESS";
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                datacontext.Connection.Close();
            }

            return lobjResponse;
        }

        #endregion

        #region ListOfEnquiry
        public CListOfEnquiryResponse ListOfEnquiry(LazyLoadingSearchInput iObj)
        {
            CListOfEnquiryResponse lobjResponse = new CListOfEnquiryResponse();

            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    List<ListOfEnquiry> Obj = new List<ListOfEnquiry>();
                    if (iObj.ChunkStart == -1)
                    {
                        Obj = (from p in datacontext.TblGetInTouches
                               where p.Active == true
                               orderby p.GetInTouchId descending
                               select new ListOfEnquiry()
                               {
                                   Id = Convert.ToDecimal(p.GetInTouchId),
                                   Name = p.Name,
                                   Email = p.EmailId,
                                   Message = p.Message,
                                   DateCreated = Convert.ToString(p.DateCreated)
                               }).Take(iObj.ChunkSize).ToList();
                    }
                    else
                    {
                        Obj = (from p in datacontext.TblGetInTouches
                               where p.Active == true && p.GetInTouchId < iObj.ChunkStart
                               orderby p.GetInTouchId descending
                               select new ListOfEnquiry()
                               {
                                   Id = Convert.ToDecimal(p.GetInTouchId),
                                   Name = p.Name,
                                   Email = p.EmailId,
                                   Message = p.Message,
                                   DateCreated = Convert.ToString(p.DateCreated)
                               }).Take(iObj.ChunkSize).ToList();
                    }
                    lobjResponse.ListOfEnquiryData = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region SendFinalMailToEmployee
        public CResponse SendFinalMailToEmployee(IdInputv2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string DbCon = DBFlag + iObj.Id;
            string Constring = GetConnection(DbCon);
            string NotiText = "";
           using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {

                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                           #region Add Admin
                          
                                List<TblCompanyUser> AdminUserList = (from p in datacontext.TblCompanyUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.SuperAdmin) || p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin))  select p).ToList();
                                if (AdminUserList != null && AdminUserList.Count() > 0)
                                {

                                    
                                    foreach (TblCompanyUser item in AdminUserList)
                                    {
                                                 #region SendMail

                                                
                                                StringBuilder builder = new StringBuilder();
                                               
                                                builder.Append("Hello " + item.FirstName + ",<br/><br/>");
                                                builder.Append("Welcome to eFlex, You has been added as an Admin for the company named as ," + CompanyObj.CompanyName + " <br/><br/>");
                                                builder.Append("Please use the following credentials for login to eFlex Portal:<br/><br/>");
                                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                                builder.Append("Username: " + item.UserName.Split('_')[1] + "<br/><br/>");
                                                builder.Append("Password: " + CryptorEngine.Decrypt(item.Pwd) + "<br/><br/>");
                                                
                                                //send mail
                                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                                CMail Mail = new CMail();
                                                Mail.EmailTo = item.EmailAddress;
                                                Mail.Subject = "eFlex Corporate Account Administrator - Welcome";
                                                Mail.MessageBody = lstrMessage;
                                                Mail.GodaddyUserName = GodaddyUserName;
                                                Mail.GodaddyPassword = GodaddyPwd;
                                                Mail.Server = Server;
                                                Mail.Port = Port;
                                                Mail.CompanyId = CompanyObj.CompanyId;
                                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                                #endregion SendMail

                                                string Text = "An admin user named as " + item.FirstName + " " + item.LastName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                                CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);
                                                string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                      
                                    }
                                }



                           
                            #endregion Add Admin

                           #region Add Employees
                                List<TblCompanyUser> EmployeesList = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee) select p).ToList();


                                if (EmployeesList != null && EmployeesList.Count() > 0)
                                {

                                    foreach (TblCompanyUser empitem in EmployeesList)
                                    {
                                           #region SendMail

                                            
                                            StringBuilder builder = new StringBuilder();


                                            builder.Append("Hello " + empitem.FirstName + ",<br/><br/>");
                                            builder.Append("Congratulations! Your employer " + CompanyObj.CompanyName + " has set-up an eFlex Health Spending account for you.<br/><br/>");
                                            builder.Append("Please activate your account now:<br/><br/>");
                                            builder.Append("1. Login to: <a href=" + lstrloginUrl + " ></a><br/><br/>");
                                            builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                            builder.Append("Username: " + empitem.UserName.Split('_')[1] + "<br/><br/>");
                                            builder.Append("Password: " + CryptorEngine.Decrypt(empitem.Pwd) + "<br/><br/>");
                                            builder.Append("2. Follow the onscreen instructions to activate your account, check your account balance and start submitting claims online.<br/><br/>");
                                            builder.Append("3. We'll send you a personalized eFlex card so you can use the money in your account to pay for eligible health and wellness expenses anywhere Mastercard is accepted.<br/><br/>");
                                            builder.Append("If you need help activating your account, or have a question about your health spending account and how to use it simply reply to this email or give us a call at 1-855-440-3993. We're here to help!<br/><br/>");
                                            
                                         
                                            //send mail
                                            string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                            lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                         
                                            CMail Mail = new CMail();
                                            Mail.EmailTo = empitem.EmailAddress;
                                            Mail.Subject = "eFlex Account - Welcome";
                                            Mail.MessageBody = lstrMessage;
                                            Mail.GodaddyUserName = GodaddyUserName;
                                            Mail.GodaddyPassword = GodaddyPwd;
                                            Mail.Server = Server;
                                            Mail.Port = Port;
                                            Mail.CompanyId = CompanyObj.CompanyId;
                                            bool IsSent = Mail.SendEMail(mastercontext, true);


                                            #endregion SendMail

                                            string Text = "An employee named as " + empitem.FirstName + " " + empitem.LastName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                            CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);
                                            string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                            EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                       
                                    }

                                }
                                #endregion

                           #region SendInvoiceMail
                                List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() select p).ToList();
                                if (PaymentObj != null && PaymentObj.Count() > 0)
                                {
                                    decimal TotalDueAmount = PaymentObj.Select(x => Convert.ToDecimal(x.Amount)).Sum();
                                    DateTime PaymentDate = PaymentObj.Select(x => Convert.ToDateTime(x.PaymentDate)).Take(1).SingleOrDefault();
                                    string EmailTo = "";
                                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                                    if (UserObj != null)
                                    {
                                        NotiText = "Due date of Plan(s) Initial amount " + TotalDueAmount.ToString() + " is " + Convert.ToDateTime(PaymentDate).ToString("MM/dd/yyyy") + ".";
                                           
                                        TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                        if (CarrierObj != null)
                                        {
                                            string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                             CMail Mail1 = new CMail();
                                            Mail1.EmailTo = EmailTo;
                                            Mail1.Subject = "Initial Payment.";
                                            Mail1.MessageBody = NotiText;
                                            Mail1.GodaddyUserName = GodaddyUserName;
                                            Mail1.GodaddyPassword = GodaddyPwd;
                                            Mail1.Server = Server;
                                            Mail1.Port = Port;
                                            Mail1.CompanyId = iObj.Id;
                                            bool IsSent1 = Mail1.SendEMail(datacontext, true);


                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(EmailTo))
                                            {
                                                EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                            }
                                            else
                                            {
                                                EmailTo = UserObj.EmailAddress;

                                            }
                                        }


                                        NotifyUser1(datacontext, NotiText, UserObj.CompanyUserId,CObjects.enmNotifyType.Payment.ToString());
                                        #region Push Notifications
                                        List<TblNotificationDetail> NotificationList = null;
                                        NotificationList = InsertionInNotificationTable(UserObj.CompanyUserId, NotiText, datacontext, UserObj.CompanyUserId, CObjects.enmNotifyType.Payment.ToString());
                                        if (NotificationList != null && NotificationList.Count() > 0)
                                        {
                                            SendNotification(datacontext, NotificationList, UserObj.CompanyUserId);
                                            NotificationList.Clear();

                                        }
                                        #endregion Push Notifications
                                        
                                        StringBuilder builder1 = new StringBuilder();

                                        builder1.Append("Hello  " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                        builder1.Append("Payment due date: " + Convert.ToDateTime(PaymentDate).ToString("MM/dd/yyyy") + ".<br/><br/>");
                                        builder1.Append("You and your employees will receive information on activating your individual accounts momentarily,");
                                        builder1.Append("and your customized eFlex Access Cards will be mailed to you within the next 10 business days.<br/><br/><br/><br/>");
                                        builder1.Append("If you have any questions or concerns about your eFlex account, please reach our to our team anytime at 416-748-9879 or eFlex@eclipseEIO.com <br/><br/>");
                                        builder1.Append("Thanks, and enjoy your eFlex account(s)!<br/>");
                                        builder1.Append("The eclipse EIO Team");


                                        string lstratchMessage = File.ReadAllText(EmailFormats + "InitialPaymentEmail.txt");
                                        lstratchMessage = lstratchMessage.Replace("lstrEmailContent", builder1.ToString());

                                        #region AddAttachment
                                        string contents = "";
                                        StringBuilder attchbuilder = new StringBuilder();
                                        string SamplePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                        contents = File.ReadAllText(SamplePath + "eStatementSample.html");
                                        string lstrDetailHtml = "";
                                        System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                                        decimal TotalContribution = 0;

                                        decimal TotalAdminFee = 0;
                                        decimal TotalHST = 0;
                                        decimal TotalPPT = 0;
                                        decimal TotalRST = 0;
                                        decimal TotaltaxFees = 0;
                                        if (PaymentObj != null && PaymentObj.Count() > 0)
                                        {
                                            attchbuilder.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                                            attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                                            attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                                            attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                            attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td></tr>");
                                            foreach (TblPayment item in PaymentObj)
                                            {
                                                string EmployeeName = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == item.CompanyUserId select p.FirstName + " " + p.LastName).Take(1).SingleOrDefault();
                                                TblCompanyPlan Plan = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();

                                                attchbuilder.Append("<tr style=\"background-color:#eee;\">");
                                                attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.PaymentDate).ToString("MMM dd, yyyy") + "</b></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>" + EmployeeName + "- " + Plan.Name + "</b></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>$" + item.NetDeposit + "</b></td>");
                                                attchbuilder.Append("</tr>");
                                                if (item.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())//if initial amount is pending
                                                {
                                                    attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                    attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                    attchbuilder.Append("<td width=\"30%\" align=\"right\">Initial</td>");
                                                    attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                                    attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.InitialDeposit + "</td>");
                                                    attchbuilder.Append("</tr>");
                                                    if (Plan.ContributionSchedule == 1)
                                                    {
                                                        attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                        attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                                        attchbuilder.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                        attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                                        attchbuilder.Append("</tr>");
                                                    }
                                                }

                                                TotalContribution = Convert.ToDecimal(TotalContribution + item.NetDeposit);

                                                TotalAdminFee = Convert.ToDecimal(TotalAdminFee + item.AdministrationInitialFee + item.AdministrationMonthlyFee);
                                                TotalRST = Convert.ToDecimal(TotalRST + item.RetailSalesTaxInitial + item.RetailSalesTaxMonthly);
                                                TotalPPT = Convert.ToDecimal(TotalPPT + item.PremiumTaxInitial + item.PremiumTaxMonthly);
                                                TotalHST = Convert.ToDecimal(TotalHST + item.HSTInitial + item.HSTMonthly);


                                            }

                                            TotaltaxFees = TotaltaxFees + (TotalAdminFee + TotalRST + TotalPPT + TotalHST);
                                        }
                                        lstrDetailHtml = attchbuilder.ToString();


                                        contents = contents.Replace("lstrBodyContent", lstrDetailHtml);
                                        contents = contents.Replace("lstrGrandTotal", String.Format("{0:0.00}", TotalContribution));

                                        decimal pctAdminFee = 0;
                                        decimal pctHST = 0;
                                        decimal pctPPT = 0;
                                        decimal pctRST = 0;


                                        if (CompanyObj != null)
                                        {
                                            TblProvince ProvinceObj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == CompanyObj.Province select p).Take(1).SingleOrDefault();
                                            if (ProvinceObj != null)
                                            {
                                                pctAdminFee = Convert.ToDecimal(ProvinceObj.AdminFeePercent);
                                                pctRST = Convert.ToDecimal(ProvinceObj.RST);
                                                pctPPT = Convert.ToDecimal(ProvinceObj.PPT);
                                                pctHST = Convert.ToDecimal(ProvinceObj.HST) == 0 ? Convert.ToDecimal(ProvinceObj.GST) : Convert.ToDecimal(ProvinceObj.HST);
                                            }

                                            TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                            if (AdminFeeObj != null)
                                            {
                                                decimal BrokerCommission = GetBrokerComissionCharges(iObj.Id);
                                                if (BrokerCommission > 0)
                                                {
                                                    pctAdminFee = Convert.ToDecimal(BrokerCommission + AdminFeeObj.AdminMin);
                                                }
                                                else
                                                {
                                                    pctAdminFee = Convert.ToDecimal(AdminFeeObj.AdminMax);
                                                }
                                            }


                                            contents = contents.Replace("lstrInvoiceNo", CompanyObj.UniqueCode + "_" + DateTime.UtcNow.Month + DateTime.UtcNow.Year);
                                            contents = contents.Replace("lstrCompanyName", CompanyObj.CompanyName);
                                            contents = contents.Replace("lstrAddress", CompanyObj.Address1 + ((CompanyObj.Address2 == null || CompanyObj.Address2 == "") ? "" : " ," + CompanyObj.Address2) + ((CompanyObj.City == null || CompanyObj.City == "") ? "" : " ," + CompanyObj.City));
                                            contents = contents.Replace("lstrEmail", CompanyObj.OwnerEmail);

                                            string strMonthName = mfi.GetMonthName(PaymentDate.Month).ToString();
                                            contents = contents.Replace("lstrYear", PaymentDate.Year.ToString());
                                            contents = contents.Replace("LstrMonth", strMonthName);

                                        }
                                        contents = contents.Replace("lstrAdminFee", (String.Format("{0:0.00}", pctAdminFee)));
                                        contents = contents.Replace("lstrHST", (String.Format("{0:0.00}", pctHST)));
                                        contents = contents.Replace("lstrPPT", (String.Format("{0:0.00}", pctPPT)));
                                        contents = contents.Replace("lstrRST", (String.Format("{0:0.00}", pctRST)));
                                        contents = contents.Replace("TotalAdminFee", "$" + (String.Format("{0:0.00}", TotalAdminFee)));
                                        contents = contents.Replace("TotalHST", "$" + (String.Format("{0:0.00}", TotalHST)));
                                        contents = contents.Replace("TotalPPT", "$" + (String.Format("{0:0.00}", TotalPPT)));
                                        contents = contents.Replace("TotalRST", "$" + (String.Format("{0:0.00}", TotalRST)));
                                        contents = contents.Replace("TotalFeeTax", "$" + (String.Format("{0:0.00}", TotaltaxFees)));
                                        contents = contents.Replace("lstrNetTotal", "$" + (String.Format("{0:0.00}", TotalContribution + TotaltaxFees)));
                                        
                                        TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select p).Take(1).SingleOrDefault();
                                        if (CreditObj != null)
                                        {
                                            decimal AvaiableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select Convert.ToDecimal(p.Credits)).Sum();

                                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", AvaiableCredits));

                                        }
                                        else
                                        {
                                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", 0));

                                        }

                                        string FileName = "Estatement_" + DateTime.UtcNow.Month + DateTime.UtcNow.Year;
                                        if (File.Exists(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf"))
                                        {
                                            System.IO.File.Delete(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf");
                                        }
                                        createPDF(FileName, contents, CompanyObj.CompanyId);


                                        #endregion



                                        CMail atchMail = new CMail();
                                        atchMail.EmailTo = EmailTo;// CarrierEmails;
                                        atchMail.Subject = "Initial Payment";
                                        atchMail.MessageBody = lstratchMessage;
                                        if (File.Exists(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf"))
                                        {
                                            atchMail.AttachmentFiles = new List<string> { ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf" };

                                        }

                                        atchMail.GodaddyUserName = GodaddyUserName;
                                        atchMail.GodaddyPassword = GodaddyPwd;
                                        atchMail.Server = Server;
                                        atchMail.Port = Port;
                                        atchMail.CompanyId = iObj.Id;
                                        bool IsSent2 = atchMail.SendEMail(datacontext, true);

                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, 1, NotiText);
                                    }


                                   
                                }
                                #endregion
                         
                           lobjResponse.Message = "SUCCESS";
                           lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                           
                        

                     
                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    CreateResponseLog(datacontext, StartTime, UserId, "SendFinalMailToEmployee", "", lobjResponse.Message);
                    datacontext.Connection.Close();
                }
                mastercontext.Connection.Close();

            }

            return lobjResponse;
        }
        #endregion SendFinalMailToEmployee

        #region CreateCompanyDB
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Function used to register new company
        /// </summary>
        /// <param name="iObj"></param>
        /// <returns></returns>
        public CAddCompanyResponse CreateCompanyDB(IdInput iObj)
        {
            string InputJson = "";
            DateTime StartTime = DateTime.UtcNow;
            CAddCompanyResponse lobjResponse = new CAddCompanyResponse();

            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                
                                    TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                                    if (CompanyObj != null)
                                    {
                                        #region Create Database
                                        string DBName = CompanyNameFlag + iObj.Id;
                                        string DbCon = DBFlag + iObj.Id;
                                        datacontext.ExecuteCommand("CREATE DATABASE [" + DBName + "]");
                                        string contents = "";
                                        string DbPatch = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                        contents = File.ReadAllText(DbPatch + "DbPatch.txt");
                                        string Patch = contents.Replace("lstrDbName", DBName);
                                        datacontext.ExecuteCommand(Patch.ToString());
                                        #endregion  Create Database

                                        #region Adding Connectionstring and run procedure path
                                        string ConnStrPath = ConStrPath;
                                        XmlDocument xmlDoc = new XmlDocument();
                                        xmlDoc.Load(ConnStrPath);
                                        XmlNode xmlnode = xmlDoc.SelectSingleNode("//connectionStrings");
                                        xmlnode.InnerXml += "<add name=\"" + DbCon + "\" connectionString=\"Data Source=18.216.133.20;Initial Catalog=" + DBName + ";Integrated Security=false;Max Pool Size=50000;Pooling=True;User ID=maan;Password=Jetking34\"  providerName=\"System.Data.SqlClient\" />";
                                          
                                        xmlDoc.Save(ConnStrPath);

                                        XmlNodeList nodes = xmlDoc.SelectNodes("/connectionStrings/add[@name='" + DbCon + "']");
                                        string Constring = nodes[0].Attributes["connectionString"].Value.ToString();
                                        using (EflexDBDataContext datacontextClient = new EflexDBDataContext(Constring))
                                        {
                                            contents = "";
                                            string[] filePaths = Directory.GetFiles(DbPatch + "Procedure Patch\\");
                                            foreach (var file in filePaths)
                                            {
                                                contents = "";
                                                contents = File.ReadAllText(file);
                                                string ProcedurePatch = contents.Replace("lstrDbName", DBName);
                                                datacontextClient.ExecuteCommand(ProcedurePatch.ToString());
                                            }

                                            #region Create Folders
                                            if (!Directory.Exists(ConPath + iObj.Id))
                                            {
                                                Directory.CreateDirectory(ConPath + iObj.Id);
                                                DirectoryInfo dInfo = new DirectoryInfo(ConPath + iObj.Id);
                                                DirectorySecurity dSecurity = dInfo.GetAccessControl();
                                                dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                                                dInfo.SetAccessControl(dSecurity);


                                            }
                                            #endregion Create Folders



                                            TblCompanyUser UserObj = new TblCompanyUser()
                                            {
                                                FirstName = CompanyObj.FirstName,
                                                LastName = CompanyObj.LastName,
                                                EmailAddress = CompanyObj.OwnerEmail,
                                                UserName = CompanyObj.UserName,
                                                UserTypeId = Convert.ToInt32(CObjects.enmUserType.Owner),
                                                Pwd = CompanyObj.Pwd,
                                                IsActive = true,
                                                UserToken = UserToken,
                                                ContactNo = CompanyObj.OwnerContactNo,
                                                DateModified = DateTime.UtcNow,
                                                DateCreated = DateTime.UtcNow,
                                                Active = true
                                            };
                                            datacontextClient.TblCompanyUsers.InsertOnSubmit(UserObj);
                                            datacontextClient.SubmitChanges();

                                            CompanyObj.DBCon = DbCon;
                                            CompanyObj.StagesCovered = "1";
                                            datacontext.SubmitChanges();

                                            UpdateMsgUser(UserObj, "ADD", iObj.Id);
                                            datacontextClient.Connection.Close();
                                     
                                        }

                                        #endregion Adding Connectionstring and run procedure path

                                        #region Provincial Tax Details
                                        TaxDetails TaxObj = (from p in datacontext.TblProvinces
                                                             where p.Active == true && p.ProvinceId == CompanyObj.Province
                                                             select new TaxDetails()
                                                             {
                                                                 AdminMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMin)).Take(1).SingleOrDefault(),
                                                                 AdminMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.AdminMax)).Take(1).SingleOrDefault(),
                                                                 BrokerMinFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMin)).Take(1).SingleOrDefault(),
                                                                 BrokerMaxFeePct = (from a in datacontext.TblAdminFeeDetails where a.Active == true select Convert.ToDecimal(a.BrokerMax)).Take(1).SingleOrDefault(),
                                                                 HST = Convert.ToDecimal(p.HST),
                                                                 RSTax = Convert.ToDecimal(p.RST),
                                                                 GST = Convert.ToDecimal(p.GST),
                                                                 PPTax = Convert.ToDecimal(p.PPT),
                                                             }).Take(1).SingleOrDefault();

                                        #endregion Provincial Tax Details

                                        lobjResponse.TaxDetails = TaxObj;
                                        lobjResponse.DbCon = DbCon;

                                        string Text = "A new company named as  " + CompanyObj.CompanyName + " is resgistered on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                        CreateHistoryLog(datacontext, iObj.Id, -1, lobjResponse.CompanyUserId, Text);

                                        string ForUserIds = String.Join(",", (from p in datacontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                        EflexNotifyLog(datacontext, DateTime.UtcNow, Text, ForUserIds);

                                    }
                                    lobjResponse.CompanyId = iObj.Id;

                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                    lobjResponse.Message = "SUCCESS";
                                    lobjResponse.UserToken = UserToken;
                               
                       
                 
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                datacontext.Connection.Close();
            }

            return lobjResponse;
        }


        #endregion AddCompany

        #region UpdateFCMToken
        public CResponse UpdateFCMToken(UpdateFCMInput Obj)
        {
             CResponse lobjResponse = new CResponse();
             try
             {
                 string Constring = GetConnection(Obj.DbCon);
                 using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                    {
                         TblCompanyUserDevice lobjDevice = datacontext.TblCompanyUserDevices.Where(p => p.Active == true && p.DeviceTypeId == Obj.DeviceTypeId && p.CompanyUserId == Obj.UserId).Take(1).FirstOrDefault();
                         if (lobjDevice != null)
                         {
                             lobjDevice.FCMToken = Obj.NewFCMToken;
                             lobjDevice.DateModified = DateTime.UtcNow;
                             datacontext.SubmitChanges();
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                             lobjResponse.Message = "SUCCESS";
                         }
                         else
                         {
                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             lobjResponse.Message = "No device found.";
                         }

                         datacontext.Connection.Close();
                    }
               
             }
             catch (Exception ex)
             {
                  lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                  lobjResponse.Message = ex.ToString();
             }
             return lobjResponse;
        }
        #endregion UpdateFCMToken

        #region AuthorizeUser
        public CLoginUserResponse AuthorizeUser(AuthorizeUserInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CLoginUserResponse lobjResponse = new CLoginUserResponse();
            UserId = iObj.CompanyUserId;
            bool IsLogOut = false;
            decimal PlanAmt = 0;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                
                    string DbCon = DBFlag + iObj.CompanyId;
                    string Constring = GetConnection(DbCon);
                    using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                    {
                        try
                        {
                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                            TblCompanyUser CompUserObj = (from p in datacontext.TblCompanyUsers
                                                          where p.Active == true &&
                                                              p.CompanyUserId == iObj.CompanyUserId
                                                          select p).Take(1).SingleOrDefault();
                            if (CompUserObj != null)
                            {
                               
                                if (CompUserObj.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                                {
                                    string UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());


                                    CompUserObj.Pwd = CryptorEngine.Encrypt(iObj.Password);
                                    CompUserObj.UserToken = UserToken;
                                    datacontext.SubmitChanges();


                                    datacontext.SubmitChanges();
                                    if (CompanyObj != null)
                                    {
                                        CompanyObj.Pwd = CompUserObj.Pwd;
                                    }
                                    mastercontext.SubmitChanges();
                                    #region Response
                                    #region PAPDetails
                                    bool IsPAPDetails = false;
                                    TblPAPDetail PAPOBj = (from p in datacontext.TblPAPDetails where p.Active == true && p.CompanyUserId == CompUserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                    if (PAPOBj != null)
                                    {
                                        IsPAPDetails = true;
                                    }
                                    else
                                    {
                                        IsPAPDetails = false;
                                    }
                                    #endregion

                                    #region Check Multiple accounts of logged in user

                                    List<MultipleAccounts> AccountList = (from p in datacontext.TblCompanyUsers.AsEnumerable()
                                                                          join q in datacontext.TblUserTypes on p.UserTypeId equals Convert.ToInt32(q.UserTypeId)
                                                                          where p.Active == true && p.EmailAddress == CompUserObj.EmailAddress
                                                                          select new MultipleAccounts()
                                                                          {
                                                                              UserType = q.Title,
                                                                              CompanyUserId = p.CompanyUserId
                                                                          }).ToList();

                                    #endregion

                                    TblEmpBalance EmpPaymentObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == CompUserObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                    if (EmpPaymentObj != null)
                                    {
                                        PlanAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == CompUserObj.CompanyUserId select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                                        lobjResponse.PlanTypeId = Convert.ToDecimal(CompanyObj.PlanTypeId);
                                    }
                                    lobjResponse.AmountDue = PlanAmt;

                                    lobjResponse.ContactNo = CompUserObj.ContactNo;
                                    lobjResponse.PlanTypeId = Convert.ToDecimal(CompanyObj.PlanTypeId);

                                    lobjResponse.MultipleAccounts = AccountList;
                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                    lobjResponse.CompanyId = iObj.CompanyId;
                                    lobjResponse.LoggedInUserId = CompUserObj.CompanyUserId;
                                    lobjResponse.IsPAPDetails = IsPAPDetails;
                                    lobjResponse.DbCon = CompanyObj.DBCon;
                                    lobjResponse.StagesCovered = CompanyObj.StagesCovered;
                                    lobjResponse.CompanyName = CompanyObj.CompanyName;
                                    lobjResponse.UserTypeId = Convert.ToDecimal(CompUserObj.UserTypeId);
                                    lobjResponse.Name = CompUserObj.FirstName;
                                    lobjResponse.UniqueCode = CompanyObj.UniqueCode;
                                    lobjResponse.InActive = true;
                                    lobjResponse.IsCorporation = Convert.ToBoolean(CompanyObj.IsCorporation);
                                    lobjResponse.UserToken = UserToken;
                                    lobjResponse.CompanyRegDate = Convert.ToString(CompanyObj.DateCreated);
                                    lobjResponse.Email = CompUserObj.EmailAddress;
                                    #endregion
                                }

                                #region SendMail

                               
                                StringBuilder builder = new StringBuilder();
                                string lstrFName = CompUserObj.FirstName + ((CompUserObj.LastName == null || CompUserObj.LastName == "") ? "" : " " + CompUserObj.LastName);
                               

                                builder.Append("Hello  " + lstrFName + ",<br/><br/>");
                                builder.Append("Your company, " + CompanyObj.CompanyName + ", has been successfully registered with eFlex!<br/>");
                                builder.Append("To complete the set up, please use the following credentials to login in to your corporate eFlex account.<br/>");
                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/>");
                                builder.Append("Username: " + CompanyObj.UserName.Split('_')[1] + "<br/>");
                                builder.Append("Password: " + CryptorEngine.Decrypt(CompUserObj.Pwd) + "<br/><br/><br/>");
                                builder.Append("Click here to login - put the login link i.e. " + lstrUrl + "<br/>");
                                builder.Append("If you have any questions or concerns about your eFlex account, please reach out to our team at 416-748-9879 or eflex@eclipseeio.com <br/>");
                                builder.Append("Thanks, and enjoy your eFlex benefits !</h4>");
                                builder.Append("The eclipse EIO Team");

                               

                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                 Mail.EmailTo = CompanyObj.OwnerEmail.Trim();
                                Mail.Subject = "eFlex Corporate Account Registered";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = iObj.CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);


                                #endregion SendMail
             
                                datacontext.SubmitChanges();

                                string Text = CompUserObj.FirstName + " " + CompUserObj.LastName + "'s password has been reset by the admin on " + DateTime.UtcNow.ToString("MM/dd/yyyy");
                                CreateHistoryLog(mastercontext, iObj.CompanyId, UserId, CompUserObj.CompanyUserId, Text);
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            }
                            else
                            {
                                lobjResponse.Message = "No record found.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }

                        }
                        catch (Exception ex)
                        {

                            lobjResponse.Message = ex.Message;
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        }
                        datacontext.Connection.Close();
                    }
               
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion AuthorizeUser

        #region ListEmployeesV2
        public CListEmployeesResponse ListEmployeesV2(ListEmployeesInputV2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CListEmployeesResponse lobjResponse = new CListEmployeesResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                

                try
                {
                    List<ListEmployees> Obj = new List<ListEmployees>();
                    List<ListEmployees> Result = new List<ListEmployees>();
                    if (iObj.Id == 1 || iObj.Id == 2)
                    {
                        Obj = (from p in datacontext.TblCompanyUsers
                               where p.Active == true && (p.UserTypeId == 1 || p.UserTypeId==2) &&
                                (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                               select new ListEmployees()
                               {
                                   CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                   FName = p.FirstName,
                                   LName = p.LastName,
                                   UserName = p.UserName,
                                   ContactNo = p.ContactNo,
                                   Email = p.EmailAddress,
                                   InActive = Convert.ToString(p.IsActive),
                                   InActiveDate = Convert.ToString(p.InActiveDate),
                                  
                                   CompanyPlanId = -1,
                                   IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                               }).ToList();

                    }
                    else
                    {
                        if (iObj.Filter == -1)
                        {
                            Obj = (from p in datacontext.TblEmployees
                                   join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                   join r in datacontext.TblCompanyPlans on p.CompanyPlanId equals r.CompanyPlanId
                                   where p.Active == true && q.Active == true &&
                                    (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                                   select new ListEmployees()
                                   {
                                       CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                       FName = p.FirstName,
                                       LName = p.LastName,
                                       UserName = q.UserName,
                                       ContactNo = q.ContactNo,
                                       Email = p.EmailAddress,
                                       InActive = Convert.ToString(p.IsActive),
                                       InActiveDate = Convert.ToString(p.InActiveDate),
                                       PlanName = r.Name,
                                       CompanyPlanId = Convert.ToDecimal(p.CompanyPlanId),
                                       IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                                   }).ToList();
                        }
                        else if (iObj.Filter == 1)
                        {
                             Obj = (from p in datacontext.TblEmployees
                                   join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                   join r in datacontext.TblCompanyPlans on p.CompanyPlanId equals r.CompanyPlanId
                                   where p.Active == true && q.Active == true && p.IsActive == true &&
                                    (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                                   select new ListEmployees()
                                   {
                                       CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                       FName = p.FirstName,
                                       LName = p.LastName,
                                       UserName = q.UserName,
                                       ContactNo = q.ContactNo,
                                       Email = p.EmailAddress,
                                       InActive = Convert.ToString(p.IsActive),
                                       InActiveDate = Convert.ToString(p.InActiveDate),
                                       PlanName = r.Name,
                                       CompanyPlanId = Convert.ToDecimal(p.CompanyPlanId),
                                       IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                                   }).ToList();
                        }
                        else if (iObj.Filter == 0)
                        {
                            Obj = (from p in datacontext.TblEmployees
                                   join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                   join r in datacontext.TblCompanyPlans on p.CompanyPlanId equals r.CompanyPlanId
                                   where p.Active == true && q.Active == true && p.IsActive == false &&
                                    (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                                   select new ListEmployees()
                                   {
                                       CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                       FName = p.FirstName,
                                       LName = p.LastName,
                                       UserName = q.UserName,
                                       ContactNo = q.ContactNo,
                                       Email = p.EmailAddress,
                                       InActive = Convert.ToString(p.IsActive),
                                       InActiveDate = Convert.ToString(p.InActiveDate),
                                       PlanName = r.Name,
                                       CompanyPlanId = Convert.ToDecimal(p.CompanyPlanId),
                                       IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                                   }).ToList();
                        }
                        else
                        {
                            Obj = (from p in datacontext.TblEmployees
                                   join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                   join r in datacontext.TblCompanyPlans on p.CompanyPlanId equals r.CompanyPlanId
                                   where p.Active == true && q.Active == true && p.SIN == null && p.Province == null &&
                                    (p.FirstName.Contains(iObj.SearchString) || p.LastName.Contains(iObj.SearchString) || p.EmailAddress.Contains(iObj.SearchString))
                                   select new ListEmployees()
                                   {
                                       CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                       FName = p.FirstName,
                                       LName = p.LastName,
                                       UserName = q.UserName,
                                       ContactNo = q.ContactNo,
                                       Email = p.EmailAddress,
                                       InActive = Convert.ToString(p.IsActive),
                                       InActiveDate = Convert.ToString(p.InActiveDate),
                                       PlanName = r.Name,
                                       CompanyPlanId = Convert.ToDecimal(p.CompanyPlanId),
                                       IsDeleteable = GetIsDeleteable(datacontext, CompanyId)
                                   }).ToList();
                        }
                }
                   
                    if (Obj != null && Obj.Count > 0)
                    {
                        if (iObj.ChunkStart == -1)
                        {
                            Result = Obj.OrderByDescending(i => i.CompanyUserId).Take(iObj.ChunkSize).ToList();
                        }
                        else
                        {
                            Result = Obj.OrderByDescending(i => i.CompanyUserId).Where(i => i.CompanyUserId < iObj.ChunkSize).Take(iObj.ChunkSize).ToList();
                        }
                    }
                    lobjResponse.ListEmployees = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                CreateResponseLog(datacontext, StartTime, iObj.Id, "ListEmployees", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion

    
    }
}