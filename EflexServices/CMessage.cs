﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CMessage
    {
    }
    public class CResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public dynamic ID { get; set; }
        public bool IsLogOut { get; set; }
    }

    public class CValidateUniqueCodeResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public bool IsAlreadyExists { get; set; }
    }

    public class CPopulateProvinceResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ProvinceList> ProvinceList { get; set; }

    }

    public class CGetCompanyDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public TaxDetails TaxDetails { get; set; }
        public PaymentOverview PaymentOverview { get; set; }
        public CompanyBasicaData CompanyBasicaData { get; set; }
        public List<GetPlanDetails> PlanDetails { get; set; }
        public List<EmployeesData> EmployeesData { get; set; }
        public List<AdminsData> AdminsData { get; set; }
        public CompanyReferral CompanyReferral { get; set; }
        public CompanyBrokerFee CompanyBrokerFee { get; set; }
    

    }

    public class CompanyBrokerFee
    {
        public decimal CompAdminFee { get; set; }
        public decimal CompBrokerFee { get; set; }
    }
    public class CLoginUserResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal CompanyId { get; set; }
        public string DbCon { get; set; }
        public string StagesCovered { get; set; }
        public decimal UserTypeId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string UniqueCode { get; set; }
        public decimal AmountDue { get; set; }
        public decimal LoggedInUserId { get; set; }
        public bool InActive { get; set; }
        public decimal PlanTypeId { get; set; }
        public bool IsCorporation { get; set; }
        public bool IsPAPDetails { get; set; }
        public string UserToken { get; set; }
        public bool IsCardExists { get; set; }
        public bool IsCarrier { get; set; }
        public bool IsMobileVerified { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string CompanyRegDate { get; set; }
        public List<MultipleAccounts> MultipleAccounts { get; set; }
        public int NotifyType { get; set; }
    }

    public class CAddCompanyResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public string UserToken { get; set; }
        public decimal CompanyId { get; set; }
        public string DbCon { get; set; }
        public TaxDetails TaxDetails { get; set; }
        public decimal CompanyUserId { get; set; }

    }
     
    public class CPopulateClassesResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ClassList> ClassList { get; set; }
    }
     
    public class CListEmployeesResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListEmployees> ListEmployees { get; set; }
    }

    public class CGetRandomCodeResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public string RandomCode { get; set; }
    }

    public class CGetPaymentOverviewResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ClasswisePayments> ClasswisePayments { get; set; }
        public decimal NetGrossInitial { get; set; }
        public decimal NetGrossRecurring { get; set; }
      

    }

   public class CGetStatementPeriodResponse
   {
       public string Message { get; set; }
       public string Status { get; set; }
       public List<StatementPeriod> StatementPeriod { get; set; }
   }

    public class StatementPeriod
    {
        public string Period { get; set; }
    }

    public class CGetDashboardTransactionsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public decimal AmountDue { get; set; }
        public decimal AvailableCredits { get; set; }

        public GetCompDashboardFigures GetCompDashboardFigures { get; set; }
        public List<PendingTransaction> PendingTransaction { get; set; }
        public List<CurrentTransaction> CurrentTransaction { get; set; }
        public List<ServiceClaimAmt> ServiceClaimAmt { get; set; }
      
    }

    public class PendingTransaction
    {
        public string Date { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public string TypeId { get; set; }
    }
    public class CurrentTransaction
    {
        public string Date { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; } 
        public string TypeId { get; set; }
    }

    public class CGetEStatementResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string FileName { get; set; }
    }

   public class CPopulateReferralsResponse
   {
       public string Message { get; set; }
       public string Status { get; set; }
       public List<PopulateReferralsData> PopulateReferralsData { get; set; }
   }
   public class CPopulateCompanyResponse
   {
       public string Message { get; set; }
       public string Status { get; set; }
       public List<PopulateCompany> PopulateCompany { get; set; }
   }

    public class CAdminClaimListResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public bool IsLogOut { get; set; }
        public List<AdminClaimList> AdminClaimList { get; set; }
    }

    public class CGetEmpCardListResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public bool AssignNewCard { get; set; }
        public List<GetEmpCardList> GetEmpCardList { get; set; }
    }

    public class CGetNearbyServiceProviderResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public bool IsLogOut { get; set; }
        public List<ServiceProviderList> NearByServiceProvider { get; set; }
        public List<ServiceProviderList> RecentServiceProvider { get; set; }
        public List<ServiceProviderList> OtherServiceProvider { get; set; }
        public List<ServiceProviderList> RecentlyAddedServiceProvider { get; set; }
    }

    public class ServiceProviderList
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string UniqueId { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public decimal ServiceProviderId { get; set; }
    }

    public class CGetRecentClaimDashboardResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<RecentClaimList> RecentClaimList { get; set; }

    }

    public class CGetEmpPlanDashboardDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<EmployeesDetails> EmployeesDetails { get; set; }
        public List<PlanDashboardDetails> PlanDashboardDetails { get; set; }
        public ClaimOverview ClaimOverview { get; set; }

    }

    public class EmployeesDetails
    {
        public string EmpName { get; set; }
        public string PlanName { get; set; }
        public decimal TotalClaimed { get; set; }
    }

    public class PlanDashboardDetails
    {
        public string PlanName { get; set; }
        public decimal NoOfEmployees { get; set; }
    }

    public class ClaimOverview
    {
        public List<string> Months { get; set; }
        public List<decimal> NoOfClaims { get; set; }
        public List<decimal> TotalClaimAmt { get; set; }
    }

    public class CGetEmpCardDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public bool AssignNewCard { get; set; }
        public List<GetEmpCardDetails> CardData { get; set; }
    }

   public class CGetCardDataResponse
   {
       public string Message { get; set; }
       public string Status { get; set; }
       public GetCardData Data { get; set; }
       public bool IsExpired { get; set; }
       public decimal DaysRemaining { get; set; }
       public bool ApplyClaimfee { get; set; }
       public decimal ClaimFee { get; set; }
       public string PlanStartDate { get; set; }

       public bool IsCoverAll { get; set; }
       public bool IsLogOut { get; set; }
   }

   public class CLoadValueResponse
   {
       public string Status { get; set; }
       public string Message { get; set; }
       public dynamic ID { get; set; }
       public bool IsLogOut { get; set; }

       public decimal BalanceAmount { get; set; }
       public decimal ClaimProcessingFee { get; set; }
   }

    public class CListOfEnquiryResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ListOfEnquiry> ListOfEnquiryData { get; set; }
    }

    public class ListOfEnquiry
    {
        public string Message { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public decimal Id { get; set; }

        public string DateCreated { get; set; }
    }
}