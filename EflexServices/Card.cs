﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region CreateCard
        public CResponse CreateCard(CreateCardInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                    if (EmpObj != null)
                    {
                        TblMasterCard CardExistsObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.UserId && p.CompanyId == iObj.CompanyId && p.CardStatus.ToUpper() == "PENDING FOR ACTIVATION" select p).Take(1).SingleOrDefault();
                        if (CardExistsObj == null)
                        {
                            string DOB = Convert.ToDateTime(EmpObj.DOB).ToString("yyyy-MM-dd");
                            string DateFormat = @"\";
                            string DateFormat1 = @")\/";

                            DateFormat = DateFormat + "/Date(";
                            DateFormat = DateFormat + DOB;
                            DateFormat = (DateFormat + DateFormat1);
                            decimal CardCount = 000;
                            
                            decimal Count = (from p in mastercontext.TblMasterCards select Convert.ToDecimal(p.MasterCardId)).Count();
                            if (Count > 0)
                            {
                                CardCount = Count + 1;
                            }
                            else
                            {
                                CardCount = 001;
                            }

                                string SessionId = GetUserSessionId();
                                if (SessionId != null && SessionId != "")
                                {
                                    string UniqueId = "";
                                    string AccountID = "";
                                    string CardID = "";

                                    //call create card api
                                    string CreateCardUri = BaseUrl + "Card/CreateCard/?SessionID=" + SessionId + "&ProgramID=" + ProgramId;
                                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                    webRequest2.ContentType = "application/json; charset=utf-8";

                                    CardID = DateTime.UtcNow.ToString("yyyyMMdd") + CardCount;
                                    UniqueId = "berkely_" + DateTime.UtcNow.ToString("yyyyMMdd") + "_" + CardCount;
                                    AccountID = "berkely_" + DateTime.UtcNow.ToString("yyyyMMdd") + "_acct_" + CardCount;
                                    string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":\"1\",\"AccountDetails\":{\"AccountID\":\"" + AccountID + "\",\"Description\":\"\"},\"CardDetails\":{\"CardID\":\"" + CardID + "\",\"Description\":\"\",\"FourthLine\":\"\",\"PackageID\":\"" + PackageID + "\",\"ProcessorCardID\":\"\",\"ShipID\":\""+ShipID+"\",\"ThirdLine\":\"\"},\"CardHolderDetails\":{\"AddressLine1\":\"" + EmpObj.Address1 + "\",\"AddressLine2\":\"" + EmpObj.Address1 + "\",\"BirthDate\":\"" + DateFormat + "\",\"BusinessPhone\":\"" + EmpObj.BusinessPhone + "\",\"CardHolderID\":\"\",\"City\":\"" + EmpObj.City + "\",\"Company\":\"" + CompanyObj.CompanyName + "\",\"CountryCode\":\"CA\",\"CountrySubdivisionCode\":\"ON\",\"Email\":\"" + EmpObj.EmailAddress + "\",\"FirstName\":\"" + EmpObj.FirstName + "\",\"HomePhone\":\"" + EmpObj.HomePhone + "\",\"LastName\":\"" + EmpObj.LastName + "\",\"MiddleName\":\"\",\"PostalCode\":\"" + EmpObj.PostalCode + "\",\"SIN\":\"" + EmpObj.SIN + "\"},\"CourierShipping\":0,\"Description\":\"\",\"Quantity\":1}";
                                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                    // Set the content length in the request headers  
                                    webRequest2.ContentLength = byteData.Length;


                                    try
                                    {

                                        //// Write data  
                                        using (Stream postStream = webRequest2.GetRequestStream())
                                        {
                                            postStream.Write(byteData, 0, byteData.Length);
                                        }

                                        // Get response  
                                        using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                        {
                                            StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                            string text3 = sr2.ReadToEnd().ToString();
                                            dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                            if (CreateCardRes.Result["Success"] == true)
                                            {
                                                TblMasterCard CardObj = new TblMasterCard()
                                                  {
                                                      SessionId = SessionId,
                                                      ProgramId = ProgramId,
                                                      UniqueId = UniqueId,
                                                      OrderItemId = "1",
                                                      OrderId = CreateCardRes.Result["ID"].ToString(),
                                                      AccountId = AccountID,
                                                      CardId = CardID,
                                                      PackageId = Convert.ToString(PackageID),
                                                      CardHolderId = "",
                                                      CountryCode = "CA",
                                                      CountrySubDivisionCode = "ON",
                                                      UserId = iObj.UserId,
                                                      CompanyId = iObj.CompanyId,
                                                      CourierShipping = 1,
                                                      Quantity = 1,
                                                      IsActive = false,
                                                      SIN = EmpObj.SIN,
                                                      FourthLine = EmpObj.FirstName + ((EmpObj.LastName != null && EmpObj.LastName != "") ? " " + EmpObj.LastName : ""),
                                                      Active = true,
                                                      CardStatus = "Pending for activation",
                                                      DateCreated = DateTime.UtcNow,
                                                      DateModified = DateTime.UtcNow,
                                                  };

                                                mastercontext.TblMasterCards.InsertOnSubmit(CardObj);
                                                mastercontext.SubmitChanges();
                                                CreateCardApiResponseLog(iObj.UserId, "CreateCard", "SUCCESS", CreateCardRes.Result["ID"].ToString(), iObj.CompanyId, CardObj.MasterCardId, CreateCardRes.Result["ErrorMessage"].ToString(), data);

                                              

                                                
                                                lobjResponse.ID = CardObj.MasterCardId;
                                                lobjResponse.Message = CardObj.OrderId;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                            }
                                            else
                                            {
                                                
                                                CreateCardApiResponseLog(iObj.UserId, "CreateCard", "FAILURE", CreateCardRes.Result["ID"].ToString(), iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"].ToString(), data);
                                               
                                                lobjResponse.Message = CreateCardRes.Result["ErrorMessage"].ToString();
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }

                                    }
                                    catch (WebException ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                }
                                else
                                {
                                    CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, -1, SessionId ,InputJson);
                                               
                                    lobjResponse.Message = "The service is not available.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }

                          
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Card already assigned to user, just pending for activation!!.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "User not found.";
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //throw;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "CreateCard", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }

       
        #endregion CreateCard

        #region ChangeCardStatus
        public CResponse ChangeCardStatus(ChangeCardStatusInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                    if (EmpObj != null)
                    {
                        string SessionId = GetUserSessionId();
                        if (SessionId != null && SessionId != "")
                            {
                                TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.MasterCardId == iObj.MasterCardId select p).Take(1).SingleOrDefault();
                                if (CardObj != null)
                                {
                                    
                                    
                                    //call create card api
                                    string ChangeCardStatusUri = BaseUrl + "Card/ChangeCardStatus/?SessionID=" + SessionId + "&NewStatus=" + iObj.Status;
                                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(ChangeCardStatusUri)) as HttpWebRequest;
                                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                    webRequest2.ContentType = "application/json; charset=utf-8";
                                    string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                    // Set the content length in the request headers  
                                    webRequest2.ContentLength = byteData.Length;


                                    try
                                    {

                                        //// Write data  
                                        using (Stream postStream = webRequest2.GetRequestStream())
                                        {
                                            postStream.Write(byteData, 0, byteData.Length);
                                        }

                                        // Get response  
                                        using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                        {
                                            StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                            string text3 = sr2.ReadToEnd().ToString();
                                            dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                            string MailText = "";
                                            if (CreateCardRes.Result["Success"] == true)
                                            {
                                                CardObj.CardStatus = iObj.Status;
                                                if(iObj.Status == CObjects.enumCardStatus.Activate.ToString())
                                                {
                                                    CardObj.IsActive = true;
                                                    MailText = "You card status has been activated on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                                }
                                                else if (iObj.Status == CObjects.enumCardStatus.Suspend.ToString())
                                                {
                                                    MailText = "You card status has been suspended on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);

                                                }
                                                else
                                                {
                                                    MailText = "You card status has been un-suspended on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);

                                                }
                                               
                                                CreateCardApiResponseLog(iObj.UserId, "ChangeCardStatus", "SUCCESS", CreateCardRes.Result["ID"].ToString(), iObj.CompanyId, CardObj.MasterCardId, CreateCardRes.Result["ErrorMessage"].ToString(), InputJson);
                                               

                                                #region Send Mail
                                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);


                                                string EmailTo = "";
                                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == EmpObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                                if (CarrierObj != null)
                                                {
                                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                                   
                                                    CMail Mail1 = new CMail();
                                                    Mail1.EmailTo = CarrierEmails;// CarrierEmails;
                                                    Mail1.Subject = "New Claim Request";
                                                    Mail1.MessageBody = MailText;
                                                    Mail1.GodaddyUserName = GodaddyUserName;
                                                    Mail1.GodaddyPassword = GodaddyPwd;
                                                    Mail1.Server = Server;
                                                    Mail1.Port = Port;
                                                    Mail1.CompanyId = CompanyObj.CompanyId;
                                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                                }

                                                if (!string.IsNullOrEmpty(EmailTo))
                                                {
                                                    EmailTo = EmailTo + "," + EmpObj.EmailAddress;
                                                }
                                                else
                                                {
                                                    EmailTo = EmpObj.EmailAddress;

                                                }


                                             
                                                StringBuilder builder = new StringBuilder();
                                                builder.Append("Hello " + EmpObj.FirstName + " " + EmpObj.LastName + ",<br/><br/>");
                                                builder.Append(MailText+"<br/><br/>");
                                                builder.Append("Regards,<br/><br/>");
                                                builder.Append("eFlex Team<br/><br/>");

                                                
                                                //send mail
                                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                               
                                                CMail Mail = new CMail();
                                                Mail.EmailTo = EmailTo;
                                                Mail.Subject = "New Claim Request";
                                                Mail.MessageBody = lstrMessage;
                                                Mail.GodaddyUserName = GodaddyUserName;
                                                Mail.GodaddyPassword = GodaddyPwd;
                                                Mail.Server = Server;
                                                Mail.Port = Port;
                                                Mail.CompanyId = CompanyObj.CompanyId;
                                                bool IsSent = Mail.SendEMail(datacontext, true);

                                    
                                                #endregion

                                                
                                                lobjResponse.Message = CardObj.OrderId;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                            }
                                            else
                                            {

                                                CreateCardApiResponseLog(iObj.UserId, "ChangeCardStatus", "Failure", text3, iObj.CompanyId, CardObj.MasterCardId, CreateCardRes.Result["ErrorMessage"].ToString(), data);
                                               
                                               lobjResponse.Message = CreateCardRes.Result["ErrorMessage"].ToString();
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }

                                    }
                                    catch (WebException ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                }
                                else
                                {
                                    lobjResponse.Message = "Card not found.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                            }
                            else
                            {
                                lobjResponse.Message = "The service is not available.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }

                      

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "User not found.";
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //throw;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ChangeCardStatus", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
          
        }
        #endregion

        #region LoadValueold
        public CResponse LoadValueold(LoadValueInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                   
                    TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.UserId && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CardObj != null)
                    {
                        decimal ServiceProviderId = -1;
                        #region Add ServiceProvider
                        if(iObj.ServiceProviderId == -1)
                        {
                          TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Email.ToUpper() == iObj.ProviderEmail.ToUpper() select p).Take(1).SingleOrDefault();
                            if (ObjExists == null)
                            {
                                TblServiceProvider ObjExists1 = (from p in mastercontext.TblServiceProviders where p.GoogleId == iObj.GoogleUniqueId && p.Active == true select p).Take(1).SingleOrDefault();
                                if (ObjExists1 == null)
                                {
                                    TblServiceProvider Obj = new TblServiceProvider()
                                    {
                                        Name = iObj.ServiceProvider,
                                        Address = iObj.ProviderAddress,
                                        ContactNo = iObj.ProviderContactNo,
                                        Email = iObj.ProviderEmail,
                                        ServiceId = iObj.ServiceId,
                                        CompanyId = iObj.CompanyId,
                                        GoogleId = iObj.GoogleUniqueId,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                    mastercontext.SubmitChanges();
                                    ServiceProviderId = Obj.ServiceProviderId;
                                }
                                else
                                {
                                    ServiceProviderId = ObjExists1.ServiceProviderId;

                                }
                               

                            }
                            else
                            {
                                ServiceProviderId = ObjExists.ServiceProviderId;
                               
                            }


                     


                        }
                        #endregion

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            string Text = "$" + iObj.ClaimAmount + " has been loaded on the card of an employee " + EmpObj.FirstName + " " + EmpObj.LastName + " on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);

                            decimal ClaimAmt = 0;
                            decimal ClaimFee = 0;
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            if (EmpPayment != null)
                            {
                                if (EmpPayment.Amount >= iObj.ClaimAmount)
                                {
                                     #region AddClaim processing fee
                                    TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                    if (ClaimObj1 != null)
                                    {//
                                        ClaimAmt = iObj.ClaimAmount;
                                        EmpPayment.Amount = EmpPayment.Amount - (ClaimAmt);
                                    }
                                    else
                                    {
                                        TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                      where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                          p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                      select p).Take(1).SingleOrDefault();
                                        if (FeeDetailsObj != null)
                                        {
                                            ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                            ClaimAmt = iObj.ClaimAmount;
                                            EmpPayment.Amount = EmpPayment.Amount - (ClaimAmt + ClaimFee);
                                            TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                            {
                                                TransactionDate = DateTime.UtcNow,
                                                TransactionDetails = "Claim Processing Fee",
                                                Amount = ClaimFee,
                                                CompanyUserId = iObj.UserId,
                                                CompanyPlanId = -1,
                                                ClaimId = -1,
                                                TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                Active = true,
                                                DateCreated = DateTime.UtcNow
                                            };
                                            datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                            TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                            {
                                                CompanyUserId = iObj.UserId,
                                                Amount = ClaimFee,
                                                AdjustmentDT = DateTime.UtcNow,
                                                Remarks = "Claim Processing Fee",
                                                TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                DateModified = DateTime.UtcNow
                                            };
                                            datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                            

                                        }
                                    }
                                    #endregion AddClaim processing fee
                                    
                                    
                             
                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                              join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                              where p.Active == true && q.Active == true && q.CompanyUserId == iObj.UserId
                                                              select p).Take(1).SingleOrDefault();


                                    if (PlanObj != null)
                                    {
                                        var ticks = new DateTime(2016, 1, 1).Ticks;
                                        var ans = DateTime.Now.Ticks - ticks;
                                        var UniqueId = ans.ToString("x");
                                        #region Monthly
                                        if (PlanObj.ContributionSchedule == 1)//Monthly
                                        {

                                            TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                            if (CardFeeRangeObj != null)
                                            {
                                                List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                {
                                                    //check yearly limit
                                                    int year = DateTime.Now.Year;
                                                    DateTime firstDay = new DateTime(year, 1, 1);
                                                    DateTime lastDay = new DateTime(year, 12, 31);
                                                    decimal YearClaimAmt = 0;
                                                    List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                        if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                        {
                                                            YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                        }
                                                        if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                        {
                                                            //check monthly limit
                                                            var now = DateTime.Now;
                                                            var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                            var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                            List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                            decimal MonthlyClaimAmt = 0;
                                                            if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                            {
                                                                MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                            }
                                                            if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                            {
                                                                //check day limit
                                                                decimal DailyClaimAmt = 0;
                                                                List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                {
                                                                    DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                }
                                                                if (CardFeeRangeObj.DayMax > DailyClaimAmt)
                                                                {

                                                                    #region Add Claim


                                                                    string Keyvalue = "";
                                                                    string Pingstruri = "";
                                                                    Pingstruri = BaseUrl + "Ping/";
                                                                    HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                                    webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                                    webRequest.ContentLength = 0;
                                                                    webRequest.ContentType = "application/json; charset=utf-8";
                                                                    webRequest.KeepAlive = false;
                                                                    HttpWebResponse webresponse;
                                                                    webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                                    StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                                    string text = sr.ReadToEnd().ToString();
                                                                    dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                                    if (Pingresult.Result == true)
                                                                    {
                                                                        
                                                                        string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                                        HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                                        webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                                        webRequest1.ContentLength = 0;
                                                                        webRequest1.ContentType = "application/json; charset=utf-8";
                                                                        webRequest1.KeepAlive = false;
                                                                        HttpWebResponse webresponse1;
                                                                        webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                                        StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                                        string text1 = sr1.ReadToEnd().ToString();
                                                                        dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                                        if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                                        {
                                                                            string SessionId = Loginresult.Result;
                                                                            #region LoadValue
                                                                            
                                                                            
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            DependantId = iObj.Dependant,
                                                                                            Note = iObj.Note,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            IsManual = iObj.IsManual,
                                                                                            ServiceId = iObj.ServiceId,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();

                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            TransactionDetails = "Claim Paid - Card",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                           
                                                                                        
                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                    
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                        IsSuccess = true;

                                                                                    }

                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }
                                                                            #endregion
                                                                        }
                                                                    }
                                                                    #endregion Add Claim
                                                                }
                                                                else
                                                                {
                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                    lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                    IsSuccess = false;
                                                                }


                                                            }
                                                            else
                                                            {
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                IsSuccess = false;
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                            lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                            IsSuccess = false;
                                                        }
                                                 
                                                }
                                                else
                                                {
                                                    #region Add Claim


                                                    string Keyvalue = "";
                                                    string Pingstruri = "";
                                                    Pingstruri = BaseUrl + "Ping/";
                                                    HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                    webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                    webRequest.ContentLength = 0;
                                                    webRequest.ContentType = "application/json; charset=utf-8";
                                                    webRequest.KeepAlive = false;
                                                    HttpWebResponse webresponse;
                                                    webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                    StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                    string text = sr.ReadToEnd().ToString();
                                                    dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                    if (Pingresult.Result == true)
                                                    {
                                                        
                                                        string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                        HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                        webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                        webRequest1.ContentLength = 0;
                                                        webRequest1.ContentType = "application/json; charset=utf-8";
                                                        webRequest1.KeepAlive = false;
                                                        HttpWebResponse webresponse1;
                                                        webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                        StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                        string text1 = sr1.ReadToEnd().ToString();
                                                        dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                        if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                        {
                                                            string SessionId = Loginresult.Result;

                                                            
                                                            //call load card api
                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                            // Set the content length in the request headers  
                                                            webRequest2.ContentLength = byteData.Length;
                                                            try
                                                            {

                                                                //// Write data  
                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                {
                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                }

                                                                // Get response  
                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                {
                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                    {
                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                        {
                                                                            CompanyUserId = iObj.UserId,
                                                                            ClaimTypeId = -1,
                                                                            OtherClaimType = "",
                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                            ServiceProviderId = ServiceProviderId,
                                                                            ReceiptDate = DateTime.UtcNow,
                                                                            ReceiptNo = "",
                                                                            UploadFileName = "",
                                                                            Amount = iObj.ClaimAmount,
                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                            ClaimFee = ClaimFee,
                                                                            DependantId = iObj.Dependant,
                                                                            Note = iObj.Note,
                                                                            ReimburseableAmt = ClaimAmt,
                                                                            ServiceId = iObj.ServiceId,
                                                                            IsManual = iObj.IsManual,
                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                            Active = true,
                                                                            DateCreated = DateTime.UtcNow,
                                                                            DateModified = DateTime.UtcNow
                                                                        };
                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                        datacontext.SubmitChanges();

                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                        {
                                                                            TransactionDate = DateTime.UtcNow,
                                                                            TransactionDetails = "Card has been loaded.",
                                                                            Amount = ClaimAmt,
                                                                            CompanyUserId = iObj.UserId,
                                                                            CompanyPlanId = -1,
                                                                            ClaimId = Obj.EmpClaimId,
                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                            Active = true,
                                                                            DateCreated = DateTime.UtcNow
                                                                        };
                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                        
                                                                        datacontext.SubmitChanges();
                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                           
                                                                        
                                                                        datacontext.SubmitChanges();
                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                       
                                                                        IsSuccess = true;
                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                        IsSuccess = false;
                                                                    }

                                                                }

                                                            }
                                                            catch (WebException ex)
                                                            {
                                                                lobjResponse.Message = ex.Message;
                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                IsSuccess = false;
                                                            }

                                                        }
                                                    }
                                                    #endregion Add Claim
                                                }

                                            }
                                            else
                                            {
                                                #region Add Claim


                                                string Keyvalue = "";
                                                string Pingstruri = "";
                                                Pingstruri = BaseUrl + "Ping/";
                                                HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                webRequest.ContentLength = 0;
                                                webRequest.ContentType = "application/json; charset=utf-8";
                                                webRequest.KeepAlive = false;
                                                HttpWebResponse webresponse;
                                                webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                string text = sr.ReadToEnd().ToString();
                                                dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                if (Pingresult.Result == true)
                                                {
                                                    //call login api
                                                   
                                                    string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                    HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                    webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                    webRequest1.ContentLength = 0;
                                                    webRequest1.ContentType = "application/json; charset=utf-8";
                                                    webRequest1.KeepAlive = false;
                                                    HttpWebResponse webresponse1;
                                                    webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                    StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                    string text1 = sr1.ReadToEnd().ToString();
                                                    dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                    if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                    {
                                                        string SessionId = Loginresult.Result;
                                                        
                                                        //call load card api
                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                        // Set the content length in the request headers  
                                                        webRequest2.ContentLength = byteData.Length;
                                                        try
                                                        {

                                                            //// Write data  
                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                            {
                                                                postStream.Write(byteData, 0, byteData.Length);
                                                            }

                                                            // Get response  
                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                            {
                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                if (CreateCardRes.Result["Success"] == true)
                                                                {
                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                    {
                                                                        CompanyUserId = iObj.UserId,
                                                                        ClaimTypeId = -1,
                                                                        OtherClaimType = "",
                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                        ServiceProviderId = ServiceProviderId,
                                                                        ReceiptDate = DateTime.UtcNow,
                                                                        ReceiptNo = "",
                                                                        UploadFileName = "",
                                                                        Amount = iObj.ClaimAmount,
                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                        ClaimFee = ClaimFee,
                                                                        DependantId = iObj.Dependant,
                                                                        Note = iObj.Note,
                                                                        ReimburseableAmt = ClaimAmt,
                                                                        IsManual = iObj.IsManual,
                                                                        ServiceId = iObj.ServiceId,
                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                    datacontext.SubmitChanges();

                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                    {
                                                                        TransactionDate = DateTime.UtcNow,
                                                                       TransactionDetails = "Card has been loaded.",
                                                                        Amount = iObj.ClaimAmount,
                                                                        CompanyUserId = iObj.UserId,
                                                                        CompanyPlanId = -1,
                                                                        ClaimId = Obj.EmpClaimId,
                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                    datacontext.SubmitChanges();
                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                    
                                                                    
                                                                    datacontext.SubmitChanges();
                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                
                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                    IsSuccess = true;

                                                                }

                                                            }

                                                        }
                                                        catch (WebException ex)
                                                        {
                                                            lobjResponse.Message = ex.Message;
                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                            IsSuccess = false;
                                                        }

                                                    }
                                                }
                                                #endregion Add Claim
                                            }
                                        }
                                        #endregion Monthly

                                        #region Annual
                                        if (PlanObj.ContributionSchedule == 2)//Annual
                                        {

                                            TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                            if (CardFeeRangeObj != null)
                                            {
                                                List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                {
                                                    //check yearly limit
                                                    int year = DateTime.Now.Year;
                                                    DateTime firstDay = new DateTime(year, 1, 1);
                                                    DateTime lastDay = new DateTime(year, 12, 31);
                                                    decimal YearClaimAmt = 0;
                                                    List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                    if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                    {
                                                        YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                    }

                                                    if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                    {
                                                        //check monthly limit
                                                        var now = DateTime.Now;
                                                        var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                        var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                        decimal MonthlyClaimAmt = 0;
                                                        List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                        if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                        {
                                                            MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                        }
                                                        if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                        {
                                                            //check day limit
                                                            decimal DailyClaimAmt = 0;
                                                            List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                            if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                            {
                                                                DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                            }
                                                            if (CardFeeRangeObj.DayMax > MonthlyClaimAmt)
                                                            {

                                                                #region Add Claim


                                                                string Keyvalue = "";
                                                                string Pingstruri = "";
                                                                Pingstruri = BaseUrl + "Ping/";
                                                                HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                                webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                                webRequest.ContentLength = 0;
                                                                webRequest.ContentType = "application/json; charset=utf-8";
                                                                webRequest.KeepAlive = false;
                                                                HttpWebResponse webresponse;
                                                                webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                                StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                                string text = sr.ReadToEnd().ToString();
                                                                dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                                if (Pingresult.Result == true)
                                                                {
                                                                    
                                                                    string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                                    HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                                    webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                                    webRequest1.ContentLength = 0;
                                                                    webRequest1.ContentType = "application/json; charset=utf-8";
                                                                    webRequest1.KeepAlive = false;
                                                                    HttpWebResponse webresponse1;
                                                                    webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                                    StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                                    string text1 = sr1.ReadToEnd().ToString();
                                                                    dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                                    if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                                    {
                                                                        string SessionId = Loginresult.Result;
                                                                        
                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                         byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        DependantId = iObj.Dependant,
                                                                                        Note = iObj.Note,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        ServiceId = iObj.ServiceId,
                                                                                        IsManual = iObj.IsManual,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();

                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        TransactionDetails = "Card has been loaded.",
                                                                                        Amount = ClaimAmt,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                    
                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    IsSuccess = true;

                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }

                                                                    }
                                                                }
                                                                #endregion Add Claim
                                                            }
                                                            else
                                                            {
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                IsSuccess = false;
                                                            }


                                                        }
                                                    }
                                                    else
                                                    {
                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                        lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                        IsSuccess = false;
                                                    }

                                                }
                                                else
                                                {
                                                    #region Add Claim


                                                    string Keyvalue = "";
                                                    string Pingstruri = "";
                                                    Pingstruri = BaseUrl + "Ping/";
                                                    HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                    webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                    webRequest.ContentLength = 0;
                                                    webRequest.ContentType = "application/json; charset=utf-8";
                                                    webRequest.KeepAlive = false;
                                                    HttpWebResponse webresponse;
                                                    webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                    StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                    string text = sr.ReadToEnd().ToString();
                                                    dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                    if (Pingresult.Result == true)
                                                    {
                                                        //call login api
                                                       
                                                        string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                        HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                        webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                        webRequest1.ContentLength = 0;
                                                        webRequest1.ContentType = "application/json; charset=utf-8";
                                                        webRequest1.KeepAlive = false;
                                                        HttpWebResponse webresponse1;
                                                        webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                        StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                        string text1 = sr1.ReadToEnd().ToString();
                                                        dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                        if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                        {
                                                            string SessionId = Loginresult.Result;
                                                            
                                                            //call load card api
                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                            // Set the content length in the request headers  
                                                            webRequest2.ContentLength = byteData.Length;
                                                            try
                                                            {

                                                                //// Write data  
                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                {
                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                }

                                                                // Get response  
                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                {
                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                    {
                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                        {
                                                                            CompanyUserId = iObj.UserId,
                                                                            ClaimTypeId = -1,
                                                                            OtherClaimType = "",
                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                            ServiceProviderId = ServiceProviderId,
                                                                            ReceiptDate = DateTime.UtcNow,
                                                                            ReceiptNo = "",
                                                                            UploadFileName = "",
                                                                            Amount = iObj.ClaimAmount,
                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                            ClaimFee = ClaimFee,
                                                                            DependantId = iObj.Dependant,
                                                                            Note = iObj.Note,
                                                                            ReimburseableAmt = ClaimAmt,
                                                                            ServiceId = iObj.ServiceId,
                                                                            IsManual = iObj.IsManual,
                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                            Active = true,
                                                                            DateCreated = DateTime.UtcNow,
                                                                            DateModified = DateTime.UtcNow
                                                                        };
                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                        datacontext.SubmitChanges();

                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                        {
                                                                            TransactionDate = DateTime.UtcNow,
                                                                            TransactionDetails = "Card has been loaded.",
                                                                            Amount = ClaimAmt,
                                                                            CompanyUserId = iObj.UserId,
                                                                            CompanyPlanId = -1,
                                                                            ClaimId = Obj.EmpClaimId,
                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                            Active = true,
                                                                            DateCreated = DateTime.UtcNow
                                                                        };
                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                        datacontext.SubmitChanges();
                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                        datacontext.SubmitChanges();
                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                      
                                                                        IsSuccess = true;
                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                        IsSuccess = false;
                                                                    }
                                                                }

                                                            }
                                                            catch (WebException ex)
                                                            {
                                                                lobjResponse.Message = ex.Message;
                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                IsSuccess = false;
                                                            }

                                                        }
                                                    }
                                                    #endregion Add Claim
                                                }

                                            }
                                            else
                                            {
                                                #region Add Claim


                                                string Keyvalue = "";
                                                string Pingstruri = "";
                                                Pingstruri = BaseUrl + "Ping/";
                                                HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                                                webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                                                webRequest.ContentLength = 0;
                                                webRequest.ContentType = "application/json; charset=utf-8";
                                                webRequest.KeepAlive = false;
                                                HttpWebResponse webresponse;
                                                webresponse = (HttpWebResponse)webRequest.GetResponse();
                                                StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                                                string text = sr.ReadToEnd().ToString();
                                                dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                                                if (Pingresult.Result == true)
                                                {
                                                    //call login api
                                                    
                                                    string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                                                    HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                                                    webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                                                    webRequest1.ContentLength = 0;
                                                    webRequest1.ContentType = "application/json; charset=utf-8";
                                                    webRequest1.KeepAlive = false;
                                                    HttpWebResponse webresponse1;
                                                    webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                                                    StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                                                    string text1 = sr1.ReadToEnd().ToString();
                                                    dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                                                    if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                                                    {
                                                        string SessionId = Loginresult.Result;
                                                        
                                                        //call load card api
                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                         byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                        // Set the content length in the request headers  
                                                        webRequest2.ContentLength = byteData.Length;
                                                        try
                                                        {

                                                            //// Write data  
                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                            {
                                                                postStream.Write(byteData, 0, byteData.Length);
                                                            }

                                                            // Get response  
                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                            {
                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                if (CreateCardRes.Result["Success"] == true)
                                                                {
                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                    {
                                                                        CompanyUserId = iObj.UserId,
                                                                        ClaimTypeId = -1,
                                                                        OtherClaimType = "",
                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                        ServiceProviderId = ServiceProviderId,
                                                                        ReceiptDate = DateTime.UtcNow,
                                                                        ReceiptNo = "",
                                                                        UploadFileName = "",
                                                                        Amount = iObj.ClaimAmount,
                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                        ClaimFee = ClaimFee,
                                                                        DependantId = iObj.Dependant,
                                                                        Note = iObj.Note,
                                                                        ReimburseableAmt = ClaimAmt,
                                                                        ServiceId = iObj.ServiceId,
                                                                        IsManual = iObj.IsManual,
                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                    datacontext.SubmitChanges();

                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                    {
                                                                        TransactionDate = DateTime.UtcNow,
                                                                        TransactionDetails = "Card has been loaded.",
                                                                        Amount = ClaimAmt,
                                                                        CompanyUserId = iObj.UserId,
                                                                        CompanyPlanId = -1,
                                                                        ClaimId = Obj.EmpClaimId,
                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                    datacontext.SubmitChanges();
                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                    
                                                                    
                                                                    datacontext.SubmitChanges();
                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                    IsSuccess = true;
                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                }
                                                                else
                                                                {
                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                    IsSuccess = false;
                                                                }
                                                            }

                                                        }
                                                        catch (WebException ex)
                                                        {
                                                            lobjResponse.Message = ex.Message;
                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                            IsSuccess = false;
                                                        }

                                                    }
                                                }
                                                #endregion Add Claim
                                            }
                                        }
                                        #endregion Monthly



                                    }

                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You have not sufficient credits in your account.So, you are not eligible applicant for claim.";
                                    IsSuccess = false;
                                }
                            }


                            if (IsSuccess)
                            {
                                #region Send Mail
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                              
                                string EmailTo = "";
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    string NotiText = "$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.";
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;
                                    Mail1.Subject = "Card Value Loaded";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = iObj.CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                }
                               
                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = UserObj.EmailAddress;

                                }
                             
                                
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                builder.Append("$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.<br/><br/>");
                                
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                         
                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;// CarrierEmails;
                                Mail.Subject = "Card Value Loaded";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = iObj.CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);
                                #endregion

                            }

                        }
                        else
                        {
                            lobjResponse.Message = "User doesn't exist in our database.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }

                }
               catch (System.Net.Sockets.SocketException ex1)
                { 
                  
                        lobjResponse.Message = ex1.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 
                }
                CreateResponseLog(datacontext, StartTime, iObj.UserId, "LoadValue", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                
            }
            return lobjResponse;

        }
        #endregion CreateCard

        #region LoadValuev1
        public CLoadValueResponse LoadValuev1(LoadValueInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CLoadValueResponse lobjResponse = new CLoadValueResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            decimal BalanceAmount = 0;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.UserId && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CardObj != null)
                    {
                        decimal ServiceProviderId = -1;
                        #region Add ServiceProvider
                        if (iObj.ServiceProviderId == -1)
                        {
                            TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Email.ToUpper() == iObj.ProviderEmail.ToUpper() select p).Take(1).SingleOrDefault();
                            if (ObjExists == null)
                            {
                                TblServiceProvider ObjExists1 = (from p in mastercontext.TblServiceProviders where p.GoogleId == iObj.GoogleUniqueId && p.Active == true select p).Take(1).SingleOrDefault();
                                if (ObjExists1 == null)
                                {
                                    TblServiceProvider Obj = new TblServiceProvider()
                                    {
                                        Name = iObj.ServiceProvider,
                                        Address = iObj.ProviderAddress,
                                        ContactNo = iObj.ProviderContactNo,
                                        Email = iObj.ProviderEmail,
                                        ServiceId = iObj.ServiceId,
                                        CompanyId = iObj.CompanyId,
                                        GoogleId = iObj.GoogleUniqueId,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                    mastercontext.SubmitChanges();
                                    ServiceProviderId = Obj.ServiceProviderId;
                                }
                                else
                                {
                                    ServiceProviderId = ObjExists1.ServiceProviderId;

                                }


                            }
                            else
                            {
                                ServiceProviderId = ObjExists.ServiceProviderId;

                            }
                        }
                        #endregion

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            string Text = "$" + iObj.ClaimAmount + " has been loaded on the card of an employee " + EmpObj.FirstName + " " + EmpObj.LastName + " on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);

                            decimal ClaimAmt = 0;
                            decimal ClaimFee = 0;
                            decimal CheckAmount = 0;
                            bool IsClaimFeeCharge = false;
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            if (EmpPayment != null)
                            {
                                if (EmpPayment.Amount >= iObj.ClaimAmount)
                                {
                                    
                                    #region AddClaim processing fee
                                    TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.UserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                    if (ClaimObj1 != null)
                                    {
                                        ClaimAmt = iObj.ClaimAmount;
                                        BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt));
                                        CheckAmount = iObj.ClaimAmount;
                                      
                                    }
                                    else
                                    {
                                        TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                      where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                          p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                      select p).Take(1).SingleOrDefault();
                                        if (FeeDetailsObj != null)
                                        {
                                            IsClaimFeeCharge = true;
                                            ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                            ClaimAmt = iObj.ClaimAmount;
                                            CheckAmount = iObj.ClaimAmount + ClaimFee;
                                           
                                           
                                        }
                                    }
                                    #endregion AddClaim processing fee

                                    //check balance if balance is greater than load amount

                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                              join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                              where p.Active == true && q.Active == true && q.CompanyUserId == iObj.UserId
                                                              select p).Take(1).SingleOrDefault();

                                    if (EmpPayment.Amount >= CheckAmount)
                                    {
                                        if (PlanObj != null)
                                        {
                                            string SessionId = GetUserSessionId();
                                            if (SessionId != null && SessionId != "")
                                            {


                                                string LoadLimitUri = BaseUrl + "Card/LoadLimits/?SessionID=" + SessionId;
                                                HttpWebRequest webRequest3 = HttpWebRequest.Create(new Uri(LoadLimitUri)) as HttpWebRequest;
                                                webRequest3.Method = System.Net.WebRequestMethods.Http.Post;
                                                webRequest3.ContentType = "application/json; charset=utf-8";
                                                string data1 = "{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                                byte[] byteData1 = UTF8Encoding.UTF8.GetBytes(data1);
                                                webRequest3.ContentLength = byteData1.Length;
                                                try
                                                {
                                                    //// Write data  
                                                    using (Stream postStream3 = webRequest3.GetRequestStream())
                                                    {
                                                        postStream3.Write(byteData1, 0, byteData1.Length);
                                                    }


                                                    // Get response  
                                                    using (HttpWebResponse response3 = webRequest3.GetResponse() as HttpWebResponse)
                                                    {
                                                        StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                                                        string text4 = sr3.ReadToEnd().ToString();
                                                        dynamic LoadLimitRes = JsonConvert.DeserializeObject<dynamic>(text4);

                                                         if (LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value > 0)
                                                        {

                                                            decimal FrequencyAmt = Convert.ToDecimal(LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value);
                                                            if (FrequencyAmt >= CheckAmount)
                                                            {
                                                                var ticks = new DateTime(2016, 1, 1).Ticks;
                                                                var ans = DateTime.Now.Ticks - ticks;
                                                                var UniqueId = ans.ToString("x");


                                                                #region Charge claim fee
                                                                if(IsClaimFeeCharge)
                                                                {
                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                    {
                                                                        TransactionDate = DateTime.UtcNow,
                                                                        TransactionDetails = "Claim Processing Fee",
                                                                        Amount = ClaimFee,
                                                                        CompanyUserId = iObj.UserId,
                                                                        CompanyPlanId = -1,
                                                                        ClaimId = -1,
                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                                    {
                                                                        CompanyUserId = iObj.UserId,
                                                                        Amount = ClaimFee,
                                                                        AdjustmentDT = DateTime.UtcNow,
                                                                        Remarks = "Claim Processing Fee",
                                                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                                                    
                                                                    BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt + ClaimFee));
                                                                }
                                                                #endregion


                                                                #region Monthly
                                                                if (PlanObj.ContributionSchedule == 1)//Monthly
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit
                                                                            int year = DateTime.Now.Year;
                                                                            DateTime firstDay = new DateTime(year, 1, 1);
                                                                            DateTime lastDay = new DateTime(year, 12, 31);
                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }
                                                                            if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }
                                                                                if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    if (CardFeeRangeObj.DayMax > DailyClaimAmt)
                                                                                    {

                                                                                        #region Add Claim

                                                                                        #region LoadValue

                                                                                        
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        DependantId = iObj.Dependant,
                                                                                                        Note = iObj.Note,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        IsManual = iObj.IsManual,
                                                                                                        ServiceId = iObj.ServiceId,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();

                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Card has been loaded.",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }

                                                                                                else
                                                                                                {
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", text3, CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                                    lobjResponse.Message = "The load value service is not available.";
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {
                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                        
                                                                                        #endregion

                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                    IsSuccess = false;
                                                                                }
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim
                                                                            
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            DependantId = iObj.Dependant,
                                                                                            Note = iObj.Note,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            ServiceId = iObj.ServiceId,
                                                                                            IsManual = iObj.IsManual,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();

                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            TransactionDetails = "Card has been loaded.",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                        
                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                        lobjResponse.ID = Obj.EmpClaimId;

                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], InputJson);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }

                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }

                                                                            
                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim


                                                                        
                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        DependantId = iObj.Dependant,
                                                                                        Note = iObj.Note,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        IsManual = iObj.IsManual,
                                                                                        ServiceId = iObj.ServiceId,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();

                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                         TransactionDetails = "Card has been loaded.",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                    
                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;

                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    IsSuccess = true;

                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], InputJson);

                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }
                                                                        
                                                                        #endregion Add Claim
                                                                    }

                                                                }

                                                                #endregion Monthly

                                                                #region Annual
                                                                if (PlanObj.ContributionSchedule == 2)//Annual
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit
                                                                            int year = DateTime.Now.Year;
                                                                            DateTime firstDay = new DateTime(year, 1, 1);
                                                                            DateTime lastDay = new DateTime(year, 12, 31);
                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }

                                                                            if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }
                                                                                if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    if (CardFeeRangeObj.DayMax > DailyClaimAmt)
                                                                                    {

                                                                                        #region Add Claim
                                                                                        
                                                                                        //call load card api
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        DependantId = iObj.Dependant,
                                                                                                        Note = iObj.Note,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        ServiceId = iObj.ServiceId,
                                                                                                        IsManual = iObj.IsManual,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();

                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Card has been loaded.",
                                                                                                        Amount = ClaimAmt,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                                    
                                                                                                    
                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], InputJson);
                                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                    IsSuccess = false;
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {
                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                        

                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                    
                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim




                                                                            //call load card api
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            DependantId = iObj.Dependant,
                                                                                            Note = iObj.Note,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            ServiceId = iObj.ServiceId,
                                                                                            IsManual = iObj.IsManual,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();

                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            
                                                                                            TransactionDetails = "Card has been loaded.",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                        
                                                                                        
                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                        lobjResponse.ID = Obj.EmpClaimId;
                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], InputJson);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }
                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }


                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim



                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        DependantId = iObj.Dependant,
                                                                                        Note = iObj.Note,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        ServiceId = iObj.ServiceId,
                                                                                        IsManual = iObj.IsManual,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();

                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        
                                                                                        TransactionDetails = "Card has been loaded.",
                                                                                        Amount = ClaimAmt,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                    
                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                    IsSuccess = true;
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], InputJson);
                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }


                                                                        #endregion Add Claim



                                                                    }
                                                                }


                                                                #endregion Monthly
                                                            }
                                                            else
                                                            {
                                                                CreateCardApiResponseLog(iObj.UserId, "LoadLimits", "FAILURE", text4, iObj.CompanyId, CardObj.MasterCardId, FrequencyAmt.ToString(), data1);

                                                                lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                                                            }
                                                        }
                                                        else
                                                        {
                                                            lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                            CreateCardApiResponseLog(iObj.UserId, "LoadLimits", text4, "", iObj.CompanyId, CardObj.MasterCardId, lobjResponse.Message, data1);
                                                            
                                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                        }
                                                    }


                                                }
                                                catch (Exception)
                                                {

                                                    throw;
                                                }
                                            }
                                            else
                                            {
                                                CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, CardObj.MasterCardId, SessionId, InputJson);

                                                lobjResponse.Message = "The service is not available.";
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }
                                    }
                                    else
                                    {
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                        lobjResponse.Message = "You have not sufficient credits in your account. Because your account balanace is " + EmpPayment.Amount + " while yours claim amount including claim processingfee is $ " + CheckAmount + " .So, you are not eligible applicant for claim.";
                                        IsSuccess = false;
                                    }
                                    lobjResponse.ClaimProcessingFee = ClaimFee;
                                   
                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You have not sufficient credits in your account.So, you are not eligible applicant for claim.";
                                    IsSuccess = false;
                                }
                            }


                            if (IsSuccess)
                            {
                                #region Send Mail
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                string EmailTo = "";
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    
                                    string NotiText = "$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.";
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;
                                    Mail1.Subject = "Card Value Loaded";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = iObj.CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                }

                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = UserObj.EmailAddress;

                                }

                               
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                builder.Append("$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.<br/><br/>");
                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                               
                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;// CarrierEmails;
                                Mail.Subject = "Card Value Loaded";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = iObj.CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);
                                #endregion

                            }
                            decimal BalanceAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select Convert.ToDecimal(p.Amount)).Take(1).SingleOrDefault();
                            lobjResponse.BalanceAmount = BalanceAmt;
                        }
                        else
                        {
                            lobjResponse.Message = "User doesn't exist in our database.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                }
                CreateResponseLog(datacontext, StartTime, iObj.UserId, "LoadValue", InputJson, lobjResponse.Message);
                datacontext.Connection.Close(); 
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion CreateCard

        #region LoadValue
        public CLoadValueResponse LoadValue(LoadValueInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CLoadValueResponse lobjResponse = new CLoadValueResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            decimal BalanceAmount = 0;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.UserId && p.IsActive==true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CardObj != null)
                    {
                        decimal ServiceProviderId = -1;
                        #region Add ServiceProvider
                        if (iObj.ServiceProviderId == -1)
                        {
                            TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Email.ToUpper() == iObj.ProviderEmail.ToUpper() select p).Take(1).SingleOrDefault();
                            if (ObjExists == null)
                            {
                                TblServiceProvider ObjExists1 = (from p in mastercontext.TblServiceProviders where p.GoogleId == iObj.GoogleUniqueId && p.Active == true select p).Take(1).SingleOrDefault();
                                if (ObjExists1 == null)
                                {
                                    TblServiceProvider Obj = new TblServiceProvider()
                                    {
                                        Name = iObj.ServiceProvider,
                                        Address = iObj.ProviderAddress,
                                        ContactNo = iObj.ProviderContactNo,
                                        Email = iObj.ProviderEmail,
                                        ServiceId = iObj.ServiceId,
                                        CompanyId = iObj.CompanyId,
                                        GoogleId = iObj.GoogleUniqueId,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                    mastercontext.SubmitChanges();
                                    ServiceProviderId = Obj.ServiceProviderId;
                                }
                                else
                                {
                                    ServiceProviderId = ObjExists1.ServiceProviderId;

                                }


                            }
                            else
                            {
                                ServiceProviderId = ObjExists.ServiceProviderId;

                            }
                        }
                        #endregion

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            string Text = "$" + iObj.ClaimAmount + " has been loaded on the card of an employee " + EmpObj.FirstName + " " + EmpObj.LastName + " on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                            string NotiText = "$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.";
                                   
                            decimal ClaimAmt = 0;
                            decimal ClaimFee = 0;
                            decimal CheckAmount = 0;
                            bool IsClaimFeeCharge = false;
                            decimal ClaimId = -1;
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            if (EmpPayment != null)
                            {
                                if (EmpPayment.Amount >= iObj.ClaimAmount)
                                {
                                   
                                   
                                    #region AddClaim processing fee
                                    TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.UserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                    if (ClaimObj1 != null)
                                    {
                                        ClaimAmt = iObj.ClaimAmount;
                                        BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt));
                                        CheckAmount = iObj.ClaimAmount;
                                        
                                    }
                                    else
                                    {
                                        TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                      where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                          p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                      select p).Take(1).SingleOrDefault();
                                        if (FeeDetailsObj != null)
                                        {
                                            IsClaimFeeCharge = true;
                                            ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                            ClaimAmt = iObj.ClaimAmount;
                                            CheckAmount = iObj.ClaimAmount + ClaimFee;
                                            BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (CheckAmount));
                                            
                                        }
                                    }
                                    #endregion AddClaim processing fee

                                    //check balance if balance is greater than load amount

                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                              join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                              where p.Active == true && q.Active == true && q.CompanyUserId == iObj.UserId
                                                              select p).Take(1).SingleOrDefault();

                                    if (EmpPayment.Amount >= CheckAmount)
                                    {
                                        if (PlanObj != null)
                                        {
                                            string SessionId = GetUserSessionId();
                                            if (SessionId != null && SessionId != "")
                                            {


                                                string LoadLimitUri = BaseUrl + "Card/LoadLimits/?SessionID=" + SessionId;
                                                HttpWebRequest webRequest3 = HttpWebRequest.Create(new Uri(LoadLimitUri)) as HttpWebRequest;
                                                webRequest3.Method = System.Net.WebRequestMethods.Http.Post;
                                                webRequest3.ContentType = "application/json; charset=utf-8";
                                                string data1 = "{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                                byte[] byteData1 = UTF8Encoding.UTF8.GetBytes(data1);
                                                webRequest3.ContentLength = byteData1.Length;
                                                try
                                                {
                                                    //// Write data  
                                                    using (Stream postStream3 = webRequest3.GetRequestStream())
                                                    {
                                                        postStream3.Write(byteData1, 0, byteData1.Length);
                                                    }


                                                    // Get response  
                                                    using (HttpWebResponse response3 = webRequest3.GetResponse() as HttpWebResponse)
                                                    {
                                                        StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                                                        string text4 = sr3.ReadToEnd().ToString();
                                                        dynamic LoadLimitRes = JsonConvert.DeserializeObject<dynamic>(text4);

                                                        if (LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value > 0)
                                                        {

                                                            decimal FrequencyAmt = Convert.ToDecimal(LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value);
                                                            if (FrequencyAmt >= CheckAmount)
                                                            {
                                                                var ticks = new DateTime(2016, 1, 1).Ticks;
                                                                var ans = DateTime.Now.Ticks - ticks;
                                                                var UniqueId = ans.ToString("x");
                                                              
                                                                #region Monthly
                                                                if (PlanObj.ContributionSchedule == 1)//Monthly
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                            where p.CompanyUserId == iObj.UserId && 
                                                                                                            (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                             && p.Active == true
                                                                                                            select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit
                                                                         
                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }
                                                                            decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                                            
                                                                            if (CheckAmount <= CheckYearAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId
                                                                                                                         && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                         && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }
                                                                                decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                                if (CheckAmount <= CheckMonthlyAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId &&
                                                                                                                          (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                          && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                                    if (CheckAmount <= CheckDailyAmt)
                                                                                    {

                                                                                        #region Add Claim

                                                                                        #region LoadValue

                                                                                        
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        DependantId = iObj.Dependant,
                                                                                                        Note = iObj.Note,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        IsManual = iObj.IsManual,
                                                                                                        ServiceId = iObj.ServiceId,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();
                                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                                    
                                                                                                    

                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }

                                                                                                else
                                                                                                {
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", text3, CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                                    lobjResponse.Message = "The load value service is not available.";
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {

                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                        
                                                                                        #endregion

                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                    IsSuccess = false;
                                                                                }
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim
                                                                            
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                             byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            DependantId = iObj.Dependant,
                                                                                            Note = iObj.Note,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            ServiceId = iObj.ServiceId,
                                                                                            IsManual = iObj.IsManual,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();
                                                                                        ClaimId = Obj.EmpClaimId;
                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            TransactionDetails = "Claim Paid - Card",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                        
                                                                                        
                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                        lobjResponse.ID = Obj.EmpClaimId;

                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }

                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }

                                                                            
                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim


                                                                        
                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        DependantId = iObj.Dependant,
                                                                                        Note = iObj.Note,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        IsManual = iObj.IsManual,
                                                                                        ServiceId = iObj.ServiceId,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();
                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                        
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                    
                                                                                    
                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;

                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    IsSuccess = true;

                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }
                                                                        
                                                                        #endregion Add Claim
                                                                    }

                                                                }

                                                                #endregion Monthly

                                                                #region Annual
                                                                if (PlanObj.ContributionSchedule == 2)//Annual
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                            where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                            && p.Active == true
                                                                                                            select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit
                                                                          
                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                   where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                   && p.Active == true
                                                                                                                   select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }

                                                                            decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                                            
                                                                            if (CheckAmount >= CheckYearAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                        where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                        && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date)
                                                                                                                        select p).ToList();
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }

                                                                                decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                                
                                                                                if (CheckAmount >= CheckMonthlyAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                          where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                          && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date
                                                                                                                          select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                                    
                                                                                    if (CheckAmount >= CheckDailyAmt)
                                                                                    {

                                                                                        #region Add Claim
                                                                                        
                                                                                        //call load card api
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                         byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        DependantId = iObj.Dependant,
                                                                                                        Note = iObj.Note,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        ServiceId = iObj.ServiceId,
                                                                                                        IsManual = iObj.IsManual,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();
                                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                                        Amount = ClaimAmt,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                                    
                                                                                                    
                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);
                                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                    IsSuccess = false;
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {
                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                        
                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim




                                                                            //call load card api
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProvider = iObj.ServiceProvider,
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            DependantId = iObj.Dependant,
                                                                                            Note = iObj.Note,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            ServiceId = iObj.ServiceId,
                                                                                            IsManual = iObj.IsManual,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();
                                                                                        ClaimId = Obj.EmpClaimId;
                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            TransactionDetails = "Claim Paid - Card",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                        lobjResponse.ID = Obj.EmpClaimId;
                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }
                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }


                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim



                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        DependantId = iObj.Dependant,
                                                                                        Note = iObj.Note,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        ServiceId = iObj.ServiceId,
                                                                                        IsManual = iObj.IsManual,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();
                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                        Amount = ClaimAmt,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());
                      
                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                    IsSuccess = true;
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);
                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }


                                                                        #endregion Add Claim



                                                                    }
                                                                }


                                                                #endregion Monthly

                                                                #region Charge claim fee
                                                                if (IsClaimFeeCharge)
                                                                {
                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                    {
                                                                        TransactionDate = DateTime.UtcNow,
                                                                        TransactionDetails = "Claim Processing Fee",
                                                                        Amount = ClaimFee,
                                                                        CompanyUserId = iObj.UserId,
                                                                        CompanyPlanId = -1,
                                                                        ClaimId = ClaimId,
                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                                    {
                                                                        CompanyUserId = iObj.UserId,
                                                                        Amount = ClaimFee,
                                                                        AdjustmentDT = DateTime.UtcNow,
                                                                        Remarks = "Claim Processing Fee",
                                                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                                                    
                                                                    BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt + ClaimFee));
                                                                }
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                CreateCardApiResponseLog(iObj.UserId, "LoadLimits", "FAILURE", text4, iObj.CompanyId, CardObj.MasterCardId, FrequencyAmt.ToString(), data1);
                                                                IsSuccess = false;
                                                                lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                                                            }
                                                        }
                                                        else
                                                        {
                                                            IsSuccess = false;
                                                            lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                            CreateCardApiResponseLog(iObj.UserId, "LoadLimits", text4, "", iObj.CompanyId, CardObj.MasterCardId, lobjResponse.Message, data1);
                                                            
                                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                        }
                                                    }


                                                }
                                                catch (Exception ex)
                                                {
                                                    IsSuccess = false;
                                                    lobjResponse.Message = ex.Message;
                                                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                }
                                            }
                                            else
                                            {
                                                CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, CardObj.MasterCardId, SessionId, InputJson);
                                                IsSuccess = false;
                                                lobjResponse.Message = "The service is not available.";
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }
                                    }
                                    else
                                    {
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                        lobjResponse.Message = "You have not sufficient credits in your account. Because your account balanace is " + EmpPayment.Amount + " while yours claim amount including claim processingfee is $ " + CheckAmount + " .So, you are not eligible applicant for claim.";
                                        IsSuccess = false;
                                    }
                                    lobjResponse.ClaimProcessingFee = ClaimFee;

                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You have not sufficient credits in your account.So, you are not eligible applicant for claim.";
                                    IsSuccess = false;
                                }
                            }


                            if (IsSuccess)
                            {
                                #region Send Mail
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                string EmailTo = "";
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;
                                    Mail1.Subject = "Card Value Loaded";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = iObj.CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                }

                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = UserObj.EmailAddress;

                                }

                               
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                builder.Append("$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.<br/><br/>");
                                builder.Append("Regards,<br/><br/>");
                                builder.Append("Eflex Team<br/><br/>");

                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                              
                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;
                                Mail.Subject = "Card Value Loaded";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = iObj.CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);
                                #endregion


                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                NotifyUser1(datacontext, NotiText, iObj.UserId, CObjects.enmNotifyType.Load.ToString());
                                NotificationList = InsertionInNotificationTable(iObj.UserId, NotiText, datacontext, ClaimId, CObjects.enmNotifyType.Load.ToString());
                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, ClaimId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications
                            }
                            decimal BalanceAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select Convert.ToDecimal(p.Amount)).Take(1).SingleOrDefault();
                            lobjResponse.BalanceAmount = BalanceAmt;
                        }
                        else
                        {
                            IsSuccess = false;
                            lobjResponse.Message = "User doesn't exist in our database.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }
                    else
                    {
                        IsSuccess = false;
                        lobjResponse.Message = "Sorry! We are unable to found your active card.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    
                    IsSuccess = false;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                }
                if (IsSuccess)
                {
                    CreateResponseLog(datacontext, StartTime, iObj.UserId, "LoadValue", InputJson, lobjResponse.Message);
                }

                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion CreateCard

        #region ActivateCardStatus
        public CResponse ActivateCardStatus(ActivateCardStatusInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                    if (EmpObj != null)
                    {

                        TblMasterCard MasterCardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.CompanyId == iObj.CompanyId && p.UserId == iObj.UserId && p.IsActive == true select p).Take(1).SingleOrDefault();
                        if (MasterCardObj == null)
                        {
                            string SessionId = GetUserSessionId();
                            if (SessionId != null && SessionId != "")
                                {
                                    TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.MasterCardId == iObj.MasterCardId && p.CardStatus.ToUpper() == "PENDING FOR ACTIVATION" select p).Take(1).SingleOrDefault();
                                    if (CardObj != null)
                                    {
                                      
                                        #region Authenticate Card
                                        string AuthenticateCardUri = BaseUrl + "Card/Authenticate/?sessionID=" + SessionId;
                                        HttpWebRequest AuthwebRequest = HttpWebRequest.Create(new Uri(AuthenticateCardUri)) as HttpWebRequest;
                                        AuthwebRequest.Method = System.Net.WebRequestMethods.Http.Post;
                                        AuthwebRequest.ContentType = "application/json; charset=utf-8";
                                        string Authdata = "{\"CardNumber\":\"" + iObj.CardNumber + "\",\"AccessCode\":\"" + iObj.AccessCode + "\",\"CVV\":\"" + iObj.CVV + "\"}";
                                        byte[] AuthbyteData = UTF8Encoding.UTF8.GetBytes(Authdata);

                                        // Set the content length in the request headers  
                                        AuthwebRequest.ContentLength = AuthbyteData.Length;

                                        using (Stream postStream = AuthwebRequest.GetRequestStream())
                                        {
                                            postStream.Write(AuthbyteData, 0, AuthbyteData.Length);
                                        }
                                        try
                                        {
                                            // Get response  
                                            using (HttpWebResponse Authresponse2 = AuthwebRequest.GetResponse() as HttpWebResponse)
                                            {
                                                StreamReader sr2 = new StreamReader(Authresponse2.GetResponseStream());
                                                string text3 = sr2.ReadToEnd().ToString();
                                                dynamic AuthCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                
                                                if (AuthCardRes.Result["UserID"] != null && AuthCardRes.Result["UserID"] != "")
                                                {
                                                    string ProcessorCardID = AuthCardRes.Result["CardInfo"].ProcessorCardID;
                                                    TblMasterCard AlreadyActiveCard = (from p in mastercontext.TblMasterCards where p.Active == true && p.ProcessorCardID == ProcessorCardID select p).Take(1).SingleOrDefault();
                                                    if (AlreadyActiveCard == null)
                                                    {
                                                        #region Change Card status

                                                        //change card status
                                                        string ChangeCardStatusUri = BaseUrl + "Card/ChangeCardStatus/?SessionID=" + SessionId + "&NewStatus=" + iObj.Status;
                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(ChangeCardStatusUri)) as HttpWebRequest;
                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                        string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + ProcessorCardID + "\",\"ReferenceType\":3}";
                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                        // Set the content length in the request headers  
                                                        webRequest2.ContentLength = byteData.Length;


                                                        try
                                                        {

                                                            //// Write data  
                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                            {
                                                                postStream.Write(byteData, 0, byteData.Length);
                                                            }

                                                            // Get response  
                                                            using (HttpWebResponse response3 = webRequest2.GetResponse() as HttpWebResponse)
                                                            {
                                                                StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                                                                string text4 = sr3.ReadToEnd().ToString();
                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text4);
                                                                if (CreateCardRes.Result["Success"] == true)
                                                                {
                                                                    CardObj.CardStatus = iObj.Status;
                                                                    if (iObj.Status == CObjects.enumCardStatus.Activate.ToString())
                                                                    {
                                                                        string Expirydt = AuthCardRes.Result["AccountInfo"].ExpiryDate;
                                                                        
                                                                        CardObj.BerkeleyUserId = AuthCardRes.Result["UserID"];
                                                                        CardObj.IsActive = true;
                                                                        CardObj.SIN = AuthCardRes.Result["CardHolderInfo"].SIN;
                                                                        CardObj.ProcessorCardID = AuthCardRes.Result["CardInfo"].ProcessorCardID;
                                                                        CardObj.LastFourDigitOfCard = AuthCardRes.Result["AccountInfo"].LastFourDigitsOfCardNumber;
                                                                        CardObj.ExpiryDt = Convert.ToDateTime(Expirydt);

                                                                     
                                                                    }

                                                                    TblCardApisResponse ApiObj = new TblCardApisResponse()
                                                                    {
                                                                        UserID = iObj.UserId,
                                                                        ServiceName = "ActivateCardStatus",
                                                                        Response = "SUCCESS",
                                                                        UniqueId = CreateCardRes.Result["ID"].ToString(),
                                                                        MasterCardId = CardObj.MasterCardId,
                                                                        Error = "",
                                                                        Input = data,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow,
                                                                        Active = true
                                                                    };


                                                                    mastercontext.TblCardApisResponses.InsertOnSubmit(ApiObj);
                                                                    mastercontext.SubmitChanges();
                                                                    string Text = "Your card has been activated.";
                                                                    string AdminNotiText = EmpObj.FirstName + " " + EmpObj.LastName + " card has been activated.";
                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                    NotifyUser1(datacontext, Text, iObj.UserId, CObjects.enmNotifyType.Card.ToString());

                                                                     datacontext.SubmitChanges();


                                                                    #region Push Notifications
                                                                    List<TblNotificationDetail> NotificationList = null;

                                                                    NotificationList = InsertionInNotificationTable(iObj.UserId, Text, datacontext, CardObj.MasterCardId, CObjects.enmNotifyType.Card.ToString());
                                                                    if (NotificationList != null && NotificationList.Count() > 0)
                                                                    {
                                                                        SendNotification(datacontext, NotificationList, CardObj.MasterCardId);
                                                                        NotificationList.Clear();

                                                                    }
                                                                    #endregion Push Notifications
                                                                    
                                                                    lobjResponse.Message = CardObj.OrderId;
                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                }
                                                                else
                                                                {
                                                                    CreateCardApiResponseLog(iObj.UserId, "ChangeCardStatus(Activate)", text4, CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"].ToString(), data);

                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"].ToString();
                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                }

                                                            }

                                                        }
                                                        catch (WebException ex)
                                                        {
                                                            lobjResponse.Message = "Please enter valid card details";
                                                           
                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                        lobjResponse.Message = "Please enter valid card details";
                                                    }
                                                }
                                                else
                                                {
                                                    CreateCardApiResponseLog(iObj.UserId, "Authenticate", text3, AuthCardRes.Result["UserID"].ToString(), iObj.CompanyId, -1, AuthCardRes.Result["UserID"].ToString(), Authdata);

                                                    lobjResponse.Message = "Sorry! Your card is not authorized.";
                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            lobjResponse.Message = "Please enter correct Card details.";
                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                        }




                                        #endregion Authenticate Card

                                    }
                                    else
                                    {
                                        lobjResponse.Message = "Card not found.";
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    }
                                }
                                else
                                {
                                    CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, -1, SessionId, InputJson);
                                    
                                    lobjResponse.Message = "The service is not available.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }

                            
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "We couldn't activate this card, because its card already activated.";
                        }

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "User not found.";
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //throw;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ChangeCardStatus", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion

        #region BlockCard
        public CResponse BlockCard(BlockCardStatusInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                    if (EmpObj != null)
                    {
                        string SessionId = GetUserSessionId();
                        if (SessionId != null && SessionId != "")
                            {
                                TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.MasterCardId == iObj.MasterCardId select p).Take(1).SingleOrDefault();
                                if (CardObj != null)
                                {
                                  
                                    
                                    //call create card api
                                    string ChangeCardStatusUri = BaseUrl + "Card/ChangeCardStatus/?SessionID=" + SessionId + "&NewStatus=" + iObj.Status;
                                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(ChangeCardStatusUri)) as HttpWebRequest;
                                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                    webRequest2.ContentType = "application/json; charset=utf-8";
                                    string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                    // Set the content length in the request headers  
                                    webRequest2.ContentLength = byteData.Length;


                                    try
                                    {

                                        //// Write data  
                                        using (Stream postStream = webRequest2.GetRequestStream())
                                        {
                                            postStream.Write(byteData, 0, byteData.Length);
                                        }

                                        // Get response  
                                        using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                        {
                                            StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                            string text3 = sr2.ReadToEnd().ToString();
                                            dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                            if (CreateCardRes.Result["Success"] == true)
                                            {
                                                CardObj.CardStatus = iObj.Status;
                                                CardObj.IsActive = false;
                                                CardObj.BlockDate = DateTime.UtcNow;
                                                CardObj.BlockReason = iObj.BlockReason;
                                               

                                                TblCardApisResponse ApiObj = new TblCardApisResponse()
                                                {
                                                    UserID = iObj.UserId,
                                                    ServiceName = "BlockCard",
                                                    Response = "SUCCESS",
                                                    UniqueId = CreateCardRes.Result["ID"].ToString(),
                                                    MasterCardId = CardObj.MasterCardId,
                                                    Error = "",
                                                    Input = InputJson,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow,
                                                    Active = true
                                                };

                                                mastercontext.TblCardApisResponses.InsertOnSubmit(ApiObj);
                                                mastercontext.SubmitChanges();
                                                
                                                lobjResponse.Message = CardObj.OrderId;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                #region Push Notifications
                                                List<TblNotificationDetail> NotificationList = null;
                                                string Text = "Your card has been blocked.";
                                                NotifyUser1(datacontext, Text, iObj.UserId, CObjects.enmNotifyType.Card.ToString());
                                                NotificationList = InsertionInNotificationTable(iObj.UserId, Text, datacontext, CardObj.MasterCardId, CObjects.enmNotifyType.Card.ToString());
                                                if (NotificationList != null && NotificationList.Count() > 0)
                                                {
                                                    SendNotification(datacontext, NotificationList, CardObj.MasterCardId);
                                                    NotificationList.Clear();

                                                }
                                                #endregion Push Notifications
                                            }
                                            else
                                            {
                                                CreateCardApiResponseLog(iObj.UserId, "ChangeCardStatus(Block)", "FAILURE", CreateCardRes.Result["ID"].ToString(), iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"].ToString(), data);
                                                lobjResponse.Message = CreateCardRes.Result["ErrorMessage"].ToString();
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }

                                    }
                                    catch (WebException ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                }
                                else
                                {
                                    lobjResponse.Message = "Card not found.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                            }
                            else
                            {
                                CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", "FAILURE", iObj.CompanyId, -1, "FAILURE", InputJson);
                                    
                                lobjResponse.Message = "The service is not available.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }

                        

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "User not found.";
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //throw;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ChangeCardStatus", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion

        #region GetMasterCardList
        public CGetMasterCardListResponse GetMasterCardList(GetMasterCardListInput Obj)
        {
            CGetMasterCardListResponse lobjResponse = new CGetMasterCardListResponse();
            using(MasterDBDataContext mastercontext = new MasterDBDataContext())
	        {
		        try 
	            {
                    int Skip = Obj.ChunckSize * (Obj.ChunckStart - 1);
                    List<GetMasterCardList> lstCards = new List<GetMasterCardList>(); 
                    List<GetMasterCardList> Result = new List<GetMasterCardList>();
                    if (Obj.Status.ToUpper() == "ALL")
                    {
                        lstCards = (from p in mastercontext.TblMasterCards
                                    where p.Active == true && 
                                    (p.CardNumber.Contains(Obj.SearchString) || p.CardId.Contains(Obj.SearchString) || p.FourthLine.Contains(Obj.SearchString))
                                        orderby p.DateModified descending
                                        select new GetMasterCardList
                                            {
                                                MasterCardId = p.MasterCardId,
                                                SessionId = p.SessionId,
                                                UniqueId = p.UniqueId,
                                                OrderItemId = p.OrderItemId,
                                                OrderId = p.OrderId,
                                                AccountId = p.AccountId,
                                                CardId = p.CardId,
                                                CardHolderId = p.CardHolderId,
                                                CountryCode = p.CountryCode,
                                                CountrySubDivisionCode = p.CountrySubDivisionCode,
                                                FourthLine = p.FourthLine,
                                                UserId = Convert.ToDecimal(p.UserId),
                                                CompanyId = Convert.ToDecimal(p.CompanyId),
                                                CourierShipping = Convert.ToDecimal(p.CourierShipping),
                                                Quantity = Convert.ToDecimal(p.Quantity),
                                                IsActive = Convert.ToString(p.IsActive),
                                                DateCreated = Convert.ToString(p.DateCreated),
                                                BlockDate = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                                CardStatus = p.CardStatus,
                                                CardNumber = p.CardNumber,
                                                CVV = p.CVV == null ?-1 : Convert.ToDecimal(p.CVV),
                                                ExpMonth = p.ExpMonth == null ? -1 : Convert.ToDecimal(p.ExpMonth),
                                                ExpYear = p.ExpYear == null ? -1 : Convert.ToDecimal(p.ExpYear),
                                                BlockReason = p.BlockReason,
                                                BlockDT = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                                EmpName = GetEmpName(p.CompanyId,p.UserId),
                                                ExpiryDt = p.ExpiryDt == null ? "" : Convert.ToString(p.ExpiryDt),
                                                LastFourDigit = p.LastFourDigitOfCard == null ? "" : Convert.ToString(p.LastFourDigitOfCard),
                                              
                                            }).ToList();
                        Result = lstCards.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(Obj.ChunckSize).ToList();
                    }
                    if (Obj.Status == CObjects.enumCardStatus.Suspend.ToString())
                    {
                       lstCards = (from p in mastercontext.TblMasterCards
                                    where p.Active == true && p.CardStatus == CObjects.enumCardStatus.Suspend.ToString()
                                    && (p.CardNumber.Contains(Obj.SearchString) || p.CardId.Contains(Obj.SearchString) || p.FourthLine.Contains(Obj.SearchString))
                                    orderby p.DateModified descending
                                       select new GetMasterCardList
                                       {
                                           MasterCardId = p.MasterCardId,
                                           SessionId = p.SessionId,
                                           UniqueId = p.UniqueId,
                                           OrderItemId = p.OrderItemId,
                                           OrderId = p.OrderId,
                                           AccountId = p.AccountId,
                                           CardId = p.CardId,
                                           CardHolderId = p.CardHolderId,
                                           CountryCode = p.CountryCode,
                                           CountrySubDivisionCode = p.CountrySubDivisionCode,
                                           FourthLine = p.FourthLine,
                                           UserId = Convert.ToDecimal(p.UserId),
                                           CompanyId = Convert.ToDecimal(p.CompanyId),
                                           CourierShipping = Convert.ToDecimal(p.CourierShipping),
                                           Quantity = Convert.ToDecimal(p.Quantity),
                                           IsActive = Convert.ToString(p.IsActive),
                                           DateCreated = Convert.ToString(p.DateCreated),
                                           BlockDate = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                           CardStatus = p.CardStatus,
                                           CardNumber = p.CardNumber,
                                           CVV = p.CVV == null ? -1 : Convert.ToDecimal(p.CVV),
                                           ExpMonth = p.ExpMonth == null ? -1 : Convert.ToDecimal(p.ExpMonth),
                                           ExpYear = p.ExpYear == null ? -1 : Convert.ToDecimal(p.ExpYear),
                                           BlockReason = p.BlockReason,
                                           BlockDT = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                           EmpName = GetEmpName(p.CompanyId, p.UserId),
                                           ExpiryDt = p.ExpiryDt == null ? "" : Convert.ToString(p.ExpiryDt),
                                           LastFourDigit = p.LastFourDigitOfCard == null ? "" : Convert.ToString(p.LastFourDigitOfCard),
                                           

                                       }).ToList();
                       Result = lstCards.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(Obj.ChunckSize).ToList();
                    }
                    if (Obj.Status == CObjects.enumCardStatus.Activate.ToString())
                    {
                        lstCards = (from p in mastercontext.TblMasterCards
                                    where p.Active == true && p.IsActive == true
                                    && (p.CardNumber.Contains(Obj.SearchString) || p.CardId.Contains(Obj.SearchString) || p.FourthLine.Contains(Obj.SearchString))
                                    orderby p.DateModified descending
                                    select new GetMasterCardList
                                    {
                                        MasterCardId = p.MasterCardId,
                                        SessionId = p.SessionId,
                                        UniqueId = p.UniqueId,
                                        OrderItemId = p.OrderItemId,
                                        OrderId = p.OrderId,
                                        AccountId = p.AccountId,
                                        CardId = p.CardId,
                                        CardHolderId = p.CardHolderId,
                                        CountryCode = p.CountryCode,
                                        CountrySubDivisionCode = p.CountrySubDivisionCode,
                                        FourthLine = p.FourthLine,
                                        UserId = Convert.ToDecimal(p.UserId),
                                        CompanyId = Convert.ToDecimal(p.CompanyId),
                                        CourierShipping = Convert.ToDecimal(p.CourierShipping),
                                        Quantity = Convert.ToDecimal(p.Quantity),
                                        IsActive = Convert.ToString(p.IsActive),
                                        DateCreated = Convert.ToString(p.DateCreated),
                                        BlockDate = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                        CardStatus = p.CardStatus,
                                        CardNumber = p.CardNumber,
                                        CVV = p.CVV == null ? -1 : Convert.ToDecimal(p.CVV),
                                        ExpMonth = p.ExpMonth == null ? -1 : Convert.ToDecimal(p.ExpMonth),
                                        ExpYear = p.ExpYear == null ? -1 : Convert.ToDecimal(p.ExpYear),
                                        BlockReason = p.BlockReason,
                                        BlockDT = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                        EmpName = GetEmpName(p.CompanyId, p.UserId),
                                        ExpiryDt = p.ExpiryDt == null ? "" : Convert.ToString(p.ExpiryDt),
                                        LastFourDigit = p.LastFourDigitOfCard == null ? "" : Convert.ToString(p.LastFourDigitOfCard),
                                        

                                    }).ToList();
                        Result = lstCards.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(Obj.ChunckSize).ToList();
                    }
                    if (Obj.Status == CObjects.enumCardStatus.Pending.ToString())
                    {
                        lstCards = (from p in mastercontext.TblMasterCards
                                    where p.Active == true && p.IsActive == false
                                    && (p.CardNumber.Contains(Obj.SearchString) || p.CardId.Contains(Obj.SearchString) || p.FourthLine.Contains(Obj.SearchString))
                                    orderby p.DateModified descending
                                    select new GetMasterCardList
                                    {
                                        MasterCardId = p.MasterCardId,
                                        SessionId = p.SessionId,
                                        UniqueId = p.UniqueId,
                                        OrderItemId = p.OrderItemId,
                                        OrderId = p.OrderId,
                                        AccountId = p.AccountId,
                                        CardId = p.CardId,
                                        CardHolderId = p.CardHolderId,
                                        CountryCode = p.CountryCode,
                                        CountrySubDivisionCode = p.CountrySubDivisionCode,
                                        FourthLine = p.FourthLine,
                                        UserId = Convert.ToDecimal(p.UserId),
                                        CompanyId = Convert.ToDecimal(p.CompanyId),
                                        CourierShipping = Convert.ToDecimal(p.CourierShipping),
                                        Quantity = Convert.ToDecimal(p.Quantity),
                                        IsActive = Convert.ToString(p.IsActive),
                                        DateCreated = Convert.ToString(p.DateCreated),
                                        BlockDate = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                        CardStatus = p.CardStatus,
                                        CardNumber = p.CardNumber,
                                        CVV = p.CVV == null ? -1 : Convert.ToDecimal(p.CVV),
                                        ExpMonth = p.ExpMonth == null ? -1 : Convert.ToDecimal(p.ExpMonth),
                                        ExpYear = p.ExpYear == null ? -1 : Convert.ToDecimal(p.ExpYear),
                                        BlockReason = p.BlockReason,
                                        BlockDT = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                        EmpName = GetEmpName(p.CompanyId, p.UserId),
                                        ExpiryDt = p.ExpiryDt == null ? "" : Convert.ToString(p.ExpiryDt),
                                        LastFourDigit = p.LastFourDigitOfCard == null ? "" : Convert.ToString(p.LastFourDigitOfCard),
                                        

                                    }).ToList();
                        Result = lstCards.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(Obj.ChunckSize).ToList();
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.GetMasterCardList = Result;
                }
                catch (Exception ex)
                {
                    //Sending exception message back
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
	        }

            return lobjResponse;
        }

        private AccountSummary GetAccountData(MasterDBDataContext mastercontext, decimal? UserId, string AccountId, decimal CardId,decimal? CompanyId)
        {
            AccountSummary ResponseObj = new AccountSummary();
            string ErrorMessage = "";
           string SessionId = GetUserSessionId();
           if (SessionId != null && SessionId != "")
           {
             
               #region Change Card status

               //change card status
               string AccountSummaruUri = BaseUrl + "Account/Summary/?SessionID=" + SessionId;
               HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(AccountSummaruUri)) as HttpWebRequest;
               webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
               webRequest2.ContentType = "application/json; charset=utf-8";
               string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + AccountId + "\",\"ReferenceType\":0}";
               byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

               // Set the content length in the request headers  
               webRequest2.ContentLength = byteData.Length;


               try
               {

                   //// Write data  
                   using (Stream postStream = webRequest2.GetRequestStream())
                   {
                       postStream.Write(byteData, 0, byteData.Length);
                   }

                   // Get response  
                   using (HttpWebResponse response3 = webRequest2.GetResponse() as HttpWebResponse)
                   {
                       StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                       string text4 = sr3.ReadToEnd().ToString();
                       dynamic AccountSummaryRes = JsonConvert.DeserializeObject<dynamic>(text4);
                       if (AccountSummaryRes.Result["Success"] == true)
                       {

                           ResponseObj.ExpiryDate = AccountSummaryRes.Result["AccountInfoResult"].ExpiryDate;
                           ResponseObj.LastFourDigitOfCardNo = AccountSummaryRes.Result["AccountInfoResult"].LastFourDigitsOfCardNumber;
                           ResponseObj.Balance = AccountSummaryRes.Result["AccountInfoResult"].Balance;
                           ResponseObj.Status = AccountSummaryRes.Result["AccountInfoResult"].Status;
                           ErrorMessage = null;


                       }
                       else
                       {
                           ErrorMessage = AccountSummaryRes.Result["ErrorMessage"].Value;
                       }

                   }

               }
               catch (WebException ex)
               {
                   ErrorMessage = ex.Message;
               }
               #endregion

               
           }
            
            return ResponseObj;
        }
       
       
        #endregion

        #region Import Service Excel

        #region ImportServicesExcel
        public CResponse ImportServicesExcel(ImportServiceInput iObj)
        {
            CResponse lobjResponse = new CResponse();
          
            try
            {
                if (iObj.FileName != null)
                {
                    string FileName = Path.GetFileName(iObj.FileName);
                    string FileName1 = Path.GetFileNameWithoutExtension(iObj.FileName);
                    string Extension = Path.GetExtension(iObj.FileName);
                    string FolderPath = ConfigurationManager.AppSettings["ConPath"].ToString();
                    FolderPath = FolderPath + "UploadServices\\";

                    DataTable datatab = new DataTable();
                    string FilePath = FolderPath + FileName;
                    if (File.Exists(FilePath))
                    {
                        datatab = ImportServiceData(FilePath, Extension);
                        string Message = AddXLSServiceDetails(datatab);
                        if (Message == "")
                        {
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "SUCCESS";
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = Message;
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "File not found.";
                    }




                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    lobjResponse.Message = "FAIL";
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return lobjResponse;
        }


        #endregion ImportServicesExcel

        #region ImportServiceData
        private DataTable ImportServiceData(string FilePath, string Extension)
        {
            try
            {
                string conStr = "";
                string isHDR = "Yes";
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conStr = String.Format(conStr, FilePath, isHDR);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();

                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw new Exception("An error ocurred while executing ImportVehicles(). " + ex.Message);
            }
        }


        #endregion ImportEmpData

        #region AddXLSServiceDetails
        /// <summary>
        /// Author:Jasmeet kaur
        /// Date:180618
        /// Function used to insert the data from excel in database.
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="DbCon"></param>
        private string AddXLSServiceDetails(DataTable datatab)
        {
            string lstrServiceType = "";
            string lstrService = "";
            decimal? ldblCRA = -1;
            decimal? ldblNonCRA = -1;
            string ErrorString = "";
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {

                  for (int i = 0; i < datatab.Rows.Count; i++)
                    {
                        if (datatab.Rows[i]["Service Type"].ToString() != "")
                        {
                            lstrServiceType = Convert.ToString(datatab.Rows[i]["Service Type"]).ToString();

                        }
                        if (datatab.Rows[i]["Service"].ToString() != "")
                        {
                            lstrService = Convert.ToString(datatab.Rows[i]["Service"]).ToString();

                        }
                        if (datatab.Rows[i]["CRA"].ToString() != "")
                        {
                            ldblCRA = Convert.ToDecimal(datatab.Rows[i]["CRA"]);

                        }
                        if (datatab.Rows[i]["Non-CRA"].ToString() != "")
                        {
                            ldblNonCRA = Convert.ToDecimal(datatab.Rows[i]["Non-CRA"]);

                        }

                     

                        TblServiceType ServiceTypeObj = (from p in datacontext.TblServiceTypes where p.Active == true && p.Title.ToUpper() == lstrServiceType.ToUpper() select p).Take(1).SingleOrDefault();
                        if (ServiceTypeObj != null)
                        {
                            #region Add Service
                            TblService ServiceObj = (from p in datacontext.TblServices where p.Active == true && p.Title.ToUpper() == lstrService.ToUpper() select p).Take(1).SingleOrDefault();
                            if (ServiceObj == null)
                            {
                                if ((ldblCRA == 0 && ldblNonCRA == 0) || (ldblCRA == null && ldblNonCRA == null))
                                {
                                    ldblCRA = 100;
                                    ldblNonCRA = 0;
                                }

                                TblService Obj = new TblService()
                                {
                                    Title = lstrService,
                                    ServiceTypeId = ServiceTypeObj.ServiceTypeId,
                                    CRA = ldblCRA,
                                    NonCRA = ldblNonCRA,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblServices.InsertOnSubmit(Obj);
                                datacontext.SubmitChanges();
              
                            }

                            #endregion

                           

                        }
                        
                    }

                }
                catch (Exception ex)
                {
                    if (ErrorString == "")
                    {
                        ErrorString = ex.Message;
                    }
                    else
                    {
                        ErrorString = ErrorString + "," + ex.Message + ",";
                    }
                }

                datacontext.Connection.Close();
            }

            return ErrorString;
        }
        #endregion AddXLSSyllabusDetails
        #endregion

        #region UploadReceipt
        public CResponse UploadReceipt(UploadReceiptInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            string UniqueCode = "";
            UserId = iObj.LoggedInUserId;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                    if(ClaimObj != null)
                    {
                        ClaimObj.UploadFileName = iObj.FileName;
                        ClaimObj.DateModified = DateTime.UtcNow;
                        datacontext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, iObj.LoggedInUserId, "UploadReceipt", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            
            }
            return lobjResponse;
        }
        #endregion

        #region UnSuspendCard
        public CResponse UnSuspendCard(UnSuspendStatusInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                    if (EmpObj != null)
                    {
                           string SessionId = GetUserSessionId();
                           if (SessionId != null && SessionId != "")
                            {
                                TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.MasterCardId == iObj.MasterCardId select p).Take(1).SingleOrDefault();
                                if (CardObj != null)
                                {
                                   
                                    
                                    //call create card api
                                    string ChangeCardStatusUri = BaseUrl + "Card/ChangeCardStatus/?SessionID=" + SessionId + "&NewStatus=Unsuspend";
                                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(ChangeCardStatusUri)) as HttpWebRequest;
                                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                    webRequest2.ContentType = "application/json; charset=utf-8";
                                    string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                    // Set the content length in the request headers  
                                    webRequest2.ContentLength = byteData.Length;


                                    try
                                    {

                                        //// Write data  
                                        using (Stream postStream = webRequest2.GetRequestStream())
                                        {
                                            postStream.Write(byteData, 0, byteData.Length);
                                        }

                                        // Get response  
                                        using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                        {
                                            StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                            string text3 = sr2.ReadToEnd().ToString();
                                            dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                            if (CreateCardRes.Result["Success"] == true)
                                            {
                                                CardObj.CardStatus = "Unsuspend";
                                                CardObj.IsActive = true;
                                                CardObj.DateModified = DateTime.UtcNow;
                                           

                                                TblCardApisResponse ApiObj = new TblCardApisResponse()
                                                {
                                                    UserID = iObj.UserId,
                                                    ServiceName = "Unsuspend ",
                                                    Response = "SUCCESS",
                                                    UniqueId = CreateCardRes.Result["ID"].ToString(),
                                                    MasterCardId = CardObj.MasterCardId,
                                                    Error = "",
                                                    Input = InputJson,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow,
                                                    Active = true
                                                };

                                                mastercontext.TblCardApisResponses.InsertOnSubmit(ApiObj);
                                                mastercontext.SubmitChanges();
                                                
                                                lobjResponse.Message = CardObj.OrderId;
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                #region Push Notifications
                                                List<TblNotificationDetail> NotificationList = null;
                                                string Text = "Your card has been unsuspended.";
                                                NotifyUser1(datacontext, Text, iObj.UserId, CObjects.enmNotifyType.Card.ToString());
                                                NotificationList = InsertionInNotificationTable(iObj.UserId, Text, datacontext, CardObj.MasterCardId, CObjects.enmNotifyType.Card.ToString());
                                                if (NotificationList != null && NotificationList.Count() > 0)
                                                {
                                                    SendNotification(datacontext, NotificationList, CardObj.MasterCardId);
                                                    NotificationList.Clear();

                                                }
                                                #endregion Push Notifications
                                            }
                                            else
                                            {
                                                CreateCardApiResponseLog(iObj.UserId, "ChangeCardStatus(Unsuspend)", "FAILURE", CreateCardRes.Result["Success"].ToString(), iObj.CompanyId, CardObj.MasterCardId, CreateCardRes.Result["ErrorMessage"].ToString(), InputJson);
                                               
                                                lobjResponse.Message = CreateCardRes.Result["ErrorMessage"].ToString();
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }

                                    }
                                    catch (WebException ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                }
                                else
                                {
                                    lobjResponse.Message = "Card not found.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                            }
                            else
                            {
                                CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, -1, SessionId, InputJson);
                                   
                                lobjResponse.Message = "The service is not available.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }

                     

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "User not found.";
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //throw;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, -1, "ChangeCardStatus", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion

        #region AddService
        public CResponse AddService(AddServiceInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    TblService ObjExists = (from p in masterContext.TblServices where p.Active == true && p.Title == iObj.Title select p).Take(1).SingleOrDefault();
                    if (ObjExists == null)
                    {
                        TblService Obj = new TblService()
                        {
                            Title = iObj.Title,
                            ServiceTypeId = iObj.ServiceTypeId,
                            CRA = iObj.CRA,
                            NonCRA = iObj.NonCRA,
                            CompanyUserId = iObj.LoggedInUserId,
                            Active = true,
                            IsApproved = iObj.IsApproved,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow
                        };
                        masterContext.TblServices.InsertOnSubmit(Obj);
                        masterContext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.ID = Obj.ServiceId;
                        lobjResponse.Message = "Service added successfully";

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "Service already exists with this name.";

                    }

                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region EditService
        public CResponse EditService(EditServiceInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
           
            bool IsLogOut = false;
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(masterContext))
                    {
                        TblService ObjExists = (from p in masterContext.TblServices where p.Active == true && p.ServiceId == iObj.ServiceId select p).Take(1).SingleOrDefault();
                        if (ObjExists != null)
                        {
                            TblService ObjExists1 = (from p in masterContext.TblServices where p.Active == true && p.ServiceId != iObj.ServiceId && p.Title == iObj.Title select p).Take(1).SingleOrDefault();
                            if (ObjExists1 == null)
                            {
                                ObjExists.Title = iObj.Title;
                                ObjExists.CRA = iObj.CRA;
                                ObjExists.NonCRA = iObj.NonCRA;
                                ObjExists.IsApproved = iObj.IsApproved;
                                ObjExists.ServiceTypeId = iObj.ServiceTypeId;
                                ObjExists.DateModified = DateTime.UtcNow;
                                masterContext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "Service updated successfully";
                            }
                            else
                            {

                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Service associated with this service already exists with this name.";
                                  
                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "No Record Found.";

                        }

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region DeleteService
        public CResponse DeleteService(IdInputv2 iObj)
        {
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(masterContext))
                    {
                        TblService ObjExists = (from p in masterContext.TblServices where p.Active == true && p.ServiceId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (ObjExists != null)
                        {
                            ObjExists.Active = false;
                            ObjExists.DateModified = DateTime.UtcNow;
                            masterContext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "No Record Found.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                        
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfServices
        public CListOfServicesResponse ListOfServices(ListOfServicesIdInput iObj)
        {
            CListOfServicesResponse lobjResponse = new CListOfServicesResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    
                        if (iObj.Id == -1)
                        {
                            List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                        join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                        where p.Active == true && q.Active == true && (p.IsApproved == true || p.CompanyUserId == iObj.LoggedInUserId)
                                                        && p.Title.Contains(iObj.SearchString)
                                                        orderby p.ServiceId descending
                                                        select new ListOfServices()
                                                        {
                                                            ServiceId = p.ServiceId,
                                                            Title = p.Title,
                                                            ServiceType = q.Title,
                                                            CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                            NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                            DateCreated = Convert.ToString(p.DateCreated),
                                                            ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                        }).ToList();

                            lobjResponse.ListOfServices = Obj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        }
                        else
                        {
                            List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                        join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                        where p.Active == true && q.Active == true && p.ServiceTypeId == iObj.Id 
                                                        && p.Title.Contains(iObj.SearchString) && (p.IsApproved == true || p.CompanyUserId == iObj.LoggedInUserId)
                                                        orderby p.ServiceId descending
                                                        select new ListOfServices()
                                                        {
                                                            ServiceId = p.ServiceId,
                                                            Title = p.Title,
                                                            ServiceType = q.Title,
                                                            CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                            NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                            DateCreated = Convert.ToString(p.DateCreated),
                                                            ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                        }).ToList();

                            lobjResponse.ListOfServices = Obj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region PopulateServiceTypes
        public CPopulateServiceTypesResponse PopulateServiceTypes()
        {
            CPopulateServiceTypesResponse lobjResponse = new CPopulateServiceTypesResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {

                    List<PopulateServiceTypes> Obj = (from p in masterContext.TblServiceTypes
                                                      where p.Active == true
                                                      select new PopulateServiceTypes()
                                                      {
                                                          ServiceTypeId = p.ServiceTypeId,
                                                          Title = p.Title,
                                                      }).ToList();

                    lobjResponse.PopulateServiceTypes = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfUnApprovedServices
        public CListOfServicesResponse ListOfUnApprovedServices(ListOfServicesIdInput iObj)
        {
            CListOfServicesResponse lobjResponse = new CListOfServicesResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    if (iObj.Id == -1)
                    {
                        List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                    join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                    where p.Active == true && q.Active == true && p.IsApproved == false
                                                    && p.Title.Contains(iObj.SearchString)
                                                    orderby p.ServiceId descending
                                                    select new ListOfServices()
                                                    {
                                                        ServiceId = p.ServiceId,
                                                        Title = p.Title,
                                                        ServiceType = q.Title,
                                                        CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                        NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                        DateCreated = Convert.ToString(p.DateCreated),
                                                        ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                    }).ToList();

                        lobjResponse.ListOfServices = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                    }
                    else
                    {
                        List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                    join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                    where p.Active == true && q.Active == true && p.ServiceTypeId == iObj.Id && p.IsApproved == true
                                                    && p.Title.Contains(iObj.SearchString) && p.IsApproved == false
                                                    select new ListOfServices()
                                                    {
                                                        ServiceId = p.ServiceId,
                                                        Title = p.Title,
                                                        ServiceType = q.Title,
                                                        CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                        NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                        DateCreated = Convert.ToString(p.DateCreated),
                                                        ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                    }).ToList();

                        lobjResponse.ListOfServices = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }


                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddServiceProvider
        public CResponse AddServiceProvider(AddServiceProviderInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    
                    TblServiceProvider ObjExists = (from p in masterContext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.Name.ToUpper() && p.Address.ToUpper() == iObj.Address.ToUpper() select p).Take(1).SingleOrDefault();
                    if (ObjExists == null)
                    {
                        TblServiceProvider Obj = new TblServiceProvider()
                        {
                            Name = iObj.Name,
                            Address = iObj.Address,
                            ContactNo = iObj.ContactNo,
                            Email = iObj.Email,
                            ServiceId = iObj.ServiceId,
                            CompanyId = iObj.CompanyId,
                            CompanyUserId = iObj.LoggedInUserId,
                           
                            Active = true,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow
                        };
                        masterContext.TblServiceProviders.InsertOnSubmit(Obj);
                        masterContext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.ID = Obj.ServiceProviderId;
                        lobjResponse.Message = "Service Provider added successfully.";

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "Service Provider already exists with this name.";

                    }

                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfServicesProvider
        public CListOfServicesProviderResponse ListOfServicesProvider(ListOfServiceProviderIdInput iObj)
        {
            CListOfServicesProviderResponse lobjResponse = new CListOfServicesProviderResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    
                    decimal? Id= null;
                    decimal? ServiceId = null;
                    if(iObj.Id != -1)
                    {
                      Id= iObj.Id;
                    }
                      if(iObj.ServiceId != -1)
                    {
                        ServiceId = iObj.ServiceId;
                    }
                    List<ListOfServicesProvider> Obj = (from p in masterContext.sp_ListOfServicesProvider(iObj.ChunkStart,iObj.SearchString,iObj.ChunkSize,Id,ServiceId)
                                                       select new ListOfServicesProvider()
                                                        {
                                                            ServiceProviderId = Convert.ToDecimal(p.ServiceProviderId),
                                                            Email = p.Email,
                                                            ServiceId = Convert.ToDecimal(p.ServiceId),
                                                            Name = p.Name,
                                                            Address = p.Address,
                                                            ServiceType = p.ServiceType,
                                                            ContactNo = p.ContactNo,
                                                        }).Distinct().ToList();

                    lobjResponse.ListOfServicesProvider = Obj.GroupBy(x => x.Name).Select(y => y.First()).ToList();
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region PopulateServices
        public CPopulateServicesResponse PopulateServices(IdInput iObj)
        {
            CPopulateServicesResponse lobjResponse = new CPopulateServicesResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {

                    List<PopulateServices> Obj = (from p in masterContext.TblServices
                                                  join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                  where p.Active == true && q.Active == true && p.ServiceTypeId == iObj.Id 
                                                  orderby p.ServiceId descending
                                                  select new PopulateServices()
                                                    {
                                                        ServiceId = p.ServiceId,
                                                        ServiceName = p.Title
                                                        }).ToList();

                        lobjResponse.PopulateServices = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
           
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfServicesForAdmin
        public CListOfServicesResponse ListOfServicesForAdmin(ListOfServicesIdInput iObj)
        {
            CListOfServicesResponse lobjResponse = new CListOfServicesResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    if (iObj.Id == -1)
                    {
                        List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                    join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                    where p.Active == true && q.Active == true && p.IsApproved == true
                                                    && p.Title.Contains(iObj.SearchString)
                                                    orderby p.ServiceId descending
                                                    select new ListOfServices()
                                                    {
                                                        ServiceId = p.ServiceId,
                                                        Title = p.Title,
                                                        ServiceType = q.Title,
                                                        CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                        NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                        DateCreated = Convert.ToString(p.DateCreated),
                                                        ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                    }).ToList();

                        lobjResponse.ListOfServices = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                    }
                    else
                    {
                        List<ListOfServices> Obj = (from p in masterContext.TblServices
                                                    join q in masterContext.TblServiceTypes on p.ServiceTypeId equals q.ServiceTypeId
                                                    where p.Active == true && q.Active == true && p.ServiceTypeId == iObj.Id 
                                                    && p.Title.Contains(iObj.SearchString) && p.IsApproved == true 
                                                    orderby p.ServiceId descending
                                                    select new ListOfServices()
                                                    {
                                                        ServiceId = p.ServiceId,
                                                        Title = p.Title,
                                                        ServiceType = q.Title,
                                                        CRA = p.CRA == null ? 0 : Convert.ToDecimal(p.CRA),
                                                        NonCRA = p.NonCRA == null ? 0 : Convert.ToDecimal(p.NonCRA),
                                                        DateCreated = Convert.ToString(p.DateCreated),
                                                        ServiceTypeId = Convert.ToDecimal(p.ServiceTypeId)
                                                    }).ToList();

                        lobjResponse.ListOfServices = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }


                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListCardTransactions
        public CListCardTransactionsResponse ListCardTransactions(ListCardTransactionsInput iObj)
        {
            CListCardTransactionsResponse lobjResponse = new CListCardTransactionsResponse();
            using (MasterDBDataContext masterContext = new MasterDBDataContext())
            {
                try
                {
                    TblMasterCard MasterCardObj = (from p in masterContext.TblMasterCards where p.Active == true && p.MasterCardId == iObj.CardId select p).Take(1).SingleOrDefault();
                    if (MasterCardObj != null)
                    {
                        string SessionId = GetUserSessionId();
                        if (SessionId != null && SessionId != "")
                            {
                                
                                string UniqueId = "";
                                string AccountID = "";
                                string CardID = "";

                                //call create card api
                                string TransactionUri = BaseUrl + "Card/Transactions/?sessionID=" + SessionId;
                                HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(TransactionUri)) as HttpWebRequest;
                                webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                webRequest2.ContentType = "application/json; charset=utf-8";

                                #region Convert string date to json date format
                              
                                string jsonDt = null;

                                // Creates date in .NET date format "\/Date(14123123132)\/"
                                JavaScriptSerializer ser = new JavaScriptSerializer();
                                DateTime dt = DateTime.ParseExact(iObj.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                jsonDt = ser.Serialize(dt.Date);// pass your date
                               
                                #endregion

                                AccountID = MasterCardObj.AccountId;
                                string data = "{\"StartDate\":" + jsonDt + ",\"Days\":" + iObj.Days + ",\"TransType\":\""+iObj.TransType+"\",\"CardReferenceInfo\":{\"BusinessEntityID\":" + BusinessEntityID + ",\"ReferenceID\":\"" + AccountID + "\",\"ReferenceType\":\"0\"}}";
                                byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                // Set the content length in the request headers  
                                webRequest2.ContentLength = byteData.Length;

                                try
                                {
                                    using (Stream postStream = webRequest2.GetRequestStream())
                                    {
                                        postStream.Write(byteData, 0, byteData.Length);
                                    }

                                    // Get response  
                                    using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                    {
                                        StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                        string text3 = sr2.ReadToEnd().ToString();
                                        dynamic TransactionRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                        JObject arr = (JObject)TransactionRes;
                                        List<ListCardTransactions> Obj = new List<ListCardTransactions>();
                                        if (arr["Result"]["Transactions"].Count() > 0)
                                        {
                                            foreach (var item in arr["Result"]["Transactions"])
                                            {

                                                Obj.Add(new ListCardTransactions()
                                                {
                                                   CardCurrencyCode = item["CardCurrencyCode"].ToString(),
                                                   LocalCurrencyAmount = item["LocalCurrencyAmount"].ToString(),
                                                   LocalCurrencyCode = item["LocalCurrencyCode"].ToString(),
                                                   MerchantName = item["MerchantName"].ToString(),
                                                   Reference = item["Reference"].ToString(),
                                                   SettlementDate = item["SettlementDate"].ToString(),
                                                   TransactionAmount =item["TransactionAmount"].ToString(),
                                                   TransactionCode =item["TransactionCode"].ToString(),
                                                   TransactionDate = item["TransactionDate"].ToString(),
                                                   TransactionDescription = item["TransactionDescription"].ToString(),
                                                });


                                            }
                                        }
                                        else
                                        {
                                            CreateCardApiResponseLog(iObj.CardId, "Transactions", "No record found", "", arr["Result"]["Transactions"].Count(), -1, "", "");
                                            lobjResponse.Message = "No record found.";
                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                        }


                                        if (Obj.ToList() != null && Obj.Count() > 0)
                                        {
                                            lobjResponse.ListCardTransactions = Obj;
                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                          
                                        }
                                        else
                                        {
                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            lobjResponse.Message = "No record found.";
                                        }
                                    }
                                }
                                catch (Exception ex1)
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    lobjResponse.Message =" Exception1: " + ex1.Message;
                                }
                            }
                            else
                            {
                                CreateCardApiResponseLog(iObj.CardId, "Login", "FAILURE", SessionId, -1, -1, SessionId, "");
                                lobjResponse.Message = "The service is not available.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                       
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "Card doesn't exists.";
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                masterContext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetAccountData
         public CAccountSummaryResponse GetAccountSummaryData(GetAccountDataInput iObj)
        {
            //id will be accountId
            CAccountSummaryResponse ResponseObj = new CAccountSummaryResponse();
            string ErrorMessage = "";
            UserId = iObj.LoggedInUserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            if (IsValidateEflexUser(mastercontext))
            {
                string SessionId = GetUserSessionId();
                AccountSummary AccountSummaryObj = new AccountSummary();
                if (SessionId != null && SessionId != "")
                {
                    #region Change Card status

                    //change card status
                    string AccountSummaruUri = BaseUrl + "Account/Summary/?SessionID=" + SessionId;
                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(AccountSummaruUri)) as HttpWebRequest;
                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                    webRequest2.ContentType = "application/json; charset=utf-8";
                    string data = "{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + iObj.AccountId + "\",\"ReferenceType\":0}";
                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                    // Set the content length in the request headers  
                    webRequest2.ContentLength = byteData.Length;


                    try
                    {

                        //// Write data  
                        using (Stream postStream = webRequest2.GetRequestStream())
                        {
                            postStream.Write(byteData, 0, byteData.Length);
                        }

                        // Get response  
                        using (HttpWebResponse response3 = webRequest2.GetResponse() as HttpWebResponse)
                        {
                            StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                            string text4 = sr3.ReadToEnd().ToString();
                            dynamic AccountSummaryRes = JsonConvert.DeserializeObject<dynamic>(text4);
                            if (AccountSummaryRes.Result["Success"] == true)
                            {

                                AccountSummaryObj.ExpiryDate = AccountSummaryRes.Result["AccountInfoResult"].ExpiryDate;
                                AccountSummaryObj.LastFourDigitOfCardNo = AccountSummaryRes.Result["AccountInfoResult"].LastFourDigitsOfCardNumber;
                                AccountSummaryObj.Balance = AccountSummaryRes.Result["AccountInfoResult"].Balance;
                                AccountSummaryObj.Status = AccountSummaryRes.Result["AccountInfoResult"].Status;
                                ErrorMessage = null;

                                ResponseObj.Status = CObjects.enmStatus.Success.ToString();
                                ResponseObj.AccountSummaryData = AccountSummaryObj;

                            }
                            else
                            {
                                ErrorMessage = AccountSummaryRes.Result["ErrorMessage"].Value;
                                ResponseObj.Status = CObjects.enmStatus.Failure.ToString();

                            }

                        }

                    }
                    catch (WebException ex)
                    {
                        ErrorMessage = ex.Message;
                        ResponseObj.Status = CObjects.enmStatus.Error.ToString();

                    }
                    #endregion
                    TblCardApisResponse ApiObj = new TblCardApisResponse()
                    {
                        UserID = UserId,
                        ServiceName = "AccountSummary",
                        Response = ResponseObj.Status,
                        UniqueId = "",
                        MasterCardId = iObj.MasterCardId,
                        Error = ErrorMessage,
                        Input = data,
                        DateCreated = DateTime.UtcNow,
                        DateModified = DateTime.UtcNow,
                        Active = true
                    };


                    mastercontext.TblCardApisResponses.InsertOnSubmit(ApiObj);
                    mastercontext.SubmitChanges();
                }
            }
            else
            {
                ResponseObj.Status = CObjects.enmStatus.Failure.ToString();
                ResponseObj.Message =  CObjects.SessionExpired.ToString();

            }
            return ResponseObj;
        }
        #endregion

      
 
        #region UploadReceiptV2
        public CResponse UploadReceiptV2(UploadReceiptInputV2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            string UniqueCode = "";
            UserId = iObj.LoggedInUserId;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                    if (ClaimObj != null)
                    {
                        TblSPReceipt ReceiptObj = (from p in mastercontext.TblSPReceipts where p.Active == true && p.ReceiptNo.ToUpper() == iObj.ReceiptNo.ToUpper() && p.ServiceProviderId == ClaimObj.ServiceProviderId select p).Take(1).SingleOrDefault();
                       if (ReceiptObj == null || string.IsNullOrEmpty(iObj.ReceiptNo))
                        {
                            ClaimObj.UploadFileName = iObj.FileName;
                            ClaimObj.DateModified = DateTime.UtcNow;
                            ClaimObj.ReceiptDate = Convert.ToDateTime(iObj.ReceiptDt);
                            ClaimObj.ReceiptNo = iObj.ReceiptNo;
                            datacontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "SUCCESS";
                        }
                        else
                        {
                            TblSPReceipt RcptObj = new TblSPReceipt()
                            {
                                ServiceProviderId = ClaimObj.ServiceProviderId,
                                ReceiptNo = iObj.ReceiptNo,
                                CompanyId = iObj.CompanyId,
                                UserId = ClaimObj.CompanyUserId,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow,

                            };
                            mastercontext.TblSPReceipts.InsertOnSubmit(RcptObj);
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "SUCCESS";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, iObj.LoggedInUserId, "UploadReceiptV2", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        
        #region LoadValue_new
        public CLoadValueResponse LoadValue_New(LoadValueInfoNew iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CLoadValueResponse lobjResponse = new CLoadValueResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            decimal BalanceAmount = 0;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.UserId && p.IsActive == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CardObj != null)
                    {
                        decimal ServiceProviderId = -1;
                       
                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            string Text = "$" + iObj.ClaimAmount + " has been loaded on the card of an employee " + EmpObj.FirstName + " " + EmpObj.LastName + " on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                            string NotiText = "$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.";

                            decimal ClaimAmt = 0;
                            decimal ClaimFee = 0;
                            decimal CheckAmount = 0;
                            bool IsClaimFeeCharge = false;
                            decimal ClaimId = -1;
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                            if (EmpPayment != null)
                            {
                                if (EmpPayment.Amount >= iObj.ClaimAmount)
                                {

                                   
                                    #region AddClaim processing fee
                                    TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.UserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                    if (ClaimObj1 != null)
                                    {//
                                        ClaimAmt = iObj.ClaimAmount;
                                        BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt));
                                        CheckAmount = iObj.ClaimAmount;
                                        
                                    }
                                    else
                                    {
                                        TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                      where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                          p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                      select p).Take(1).SingleOrDefault();
                                        if (FeeDetailsObj != null)
                                        {
                                            IsClaimFeeCharge = true;
                                            ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                            ClaimAmt = iObj.ClaimAmount;
                                            CheckAmount = iObj.ClaimAmount + ClaimFee;
                                            BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (CheckAmount));
                                            
                                        }
                                    }
                                    #endregion AddClaim processing fee

                                    //check balance if balance is greater than load amount

                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                              join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                              where p.Active == true && q.Active == true && q.CompanyUserId == iObj.UserId
                                                              select p).Take(1).SingleOrDefault();

                                    if (EmpPayment.Amount >= CheckAmount)
                                    {
                                        if (PlanObj != null)
                                        {
                                            string SessionId = GetUserSessionId();
                                            if (SessionId != null && SessionId != "")
                                            {


                                                string LoadLimitUri = BaseUrl + "Card/LoadLimits/?SessionID=" + SessionId;
                                                HttpWebRequest webRequest3 = HttpWebRequest.Create(new Uri(LoadLimitUri)) as HttpWebRequest;
                                                webRequest3.Method = System.Net.WebRequestMethods.Http.Post;
                                                webRequest3.ContentType = "application/json; charset=utf-8";
                                                string data1 = "{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}";
                                                byte[] byteData1 = UTF8Encoding.UTF8.GetBytes(data1);
                                                webRequest3.ContentLength = byteData1.Length;
                                                try
                                                {
                                                    //// Write data  
                                                    using (Stream postStream3 = webRequest3.GetRequestStream())
                                                    {
                                                        postStream3.Write(byteData1, 0, byteData1.Length);
                                                    }


                                                    // Get response  
                                                    using (HttpWebResponse response3 = webRequest3.GetResponse() as HttpWebResponse)
                                                    {
                                                        StreamReader sr3 = new StreamReader(response3.GetResponseStream());
                                                        string text4 = sr3.ReadToEnd().ToString();
                                                        dynamic LoadLimitRes = JsonConvert.DeserializeObject<dynamic>(text4);

                                                         if (LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value > 0)
                                                        {

                                                            decimal FrequencyAmt = Convert.ToDecimal(LoadLimitRes.Result.First["FrequencyAmountAvailable"].Value);
                                                            if (FrequencyAmt >= CheckAmount)
                                                            {
                                                                var ticks = new DateTime(2016, 1, 1).Ticks;
                                                                var ans = DateTime.Now.Ticks - ticks;
                                                                var UniqueId = ans.ToString("x");
                                                                
                                                                #region Monthly
                                                                if (PlanObj.ContributionSchedule == 1)//Monthly
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                            where p.CompanyUserId == iObj.UserId &&
                                                                                                            (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                             && p.Active == true
                                                                                                            select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit

                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.UserId && p.Active == true select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }
                                                                            decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                                            if (CheckAmount <= CheckYearAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                        where p.CompanyUserId == iObj.UserId
                                                                                                                            && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                            && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date)
                                                                                                                        select p).ToList();
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }
                                                                                decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                                if (CheckAmount <= CheckMonthlyAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                          where p.CompanyUserId == iObj.UserId &&
                                                                                                                              (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                              && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date
                                                                                                                          select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                                    if (CheckAmount <= CheckDailyAmt)
                                                                                    {

                                                                                        #region Add Claim

                                                                                        #region LoadValue

                                                                                        
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        IsManual = false,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();
                                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());


                                                                                                    
                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }

                                                                                                else
                                                                                                {
                                                                                                    IsSuccess = false;
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", text3, CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                                    lobjResponse.Message = "The load value service is not available.";
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {
                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                       
                                                                                        #endregion

                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                }
                                                                                else
                                                                                {
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                    IsSuccess = false;
                                                                                }
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim
                                                                            
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            IsManual = false,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();
                                                                                        ClaimId = Obj.EmpClaimId;
                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            TransactionDetails = "Claim Paid - Card",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());


                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                        lobjResponse.ID = Obj.EmpClaimId;

                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }

                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }

                                                                           
                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim


                                                                        
                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        IsManual = false,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();
                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());

                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;

                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    IsSuccess = true;

                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }
                                                                        
                                                                        #endregion Add Claim
                                                                    }

                                                                }

                                                                #endregion Monthly

                                                                #region Annual
                                                                if (PlanObj.ContributionSchedule == 2)//Annual
                                                                {

                                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                                    if (CardFeeRangeObj != null)
                                                                    {
                                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                            where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                            && p.Active == true
                                                                                                            select p).ToList();
                                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                                        {
                                                                            //check yearly limit

                                                                            decimal YearClaimAmt = 0;
                                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                   where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                   && p.Active == true
                                                                                                                   select p).ToList();
                                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                            {
                                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                            }

                                                                            decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                                            if (CheckAmount >= CheckYearAmt)
                                                                            {
                                                                                //check monthly limit
                                                                                var now = DateTime.Now;
                                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                                decimal MonthlyClaimAmt = 0;
                                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                        where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                        && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date)
                                                                                                                        select p).ToList();
                                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                                {
                                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                }

                                                                                decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                                if (CheckAmount >= CheckMonthlyAmt)
                                                                                {
                                                                                    //check day limit
                                                                                    decimal DailyClaimAmt = 0;
                                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                                          where p.CompanyUserId == iObj.UserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                          && p.Active == true && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date
                                                                                                                          select p).ToList();
                                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                                    {
                                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                                    }
                                                                                    decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                                    if (CheckAmount >= CheckDailyAmt)
                                                                                    {

                                                                                        #region Add Claim
                                                                                        
                                                                                        //call load card api
                                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                                        // Set the content length in the request headers  
                                                                                        webRequest2.ContentLength = byteData.Length;
                                                                                        try
                                                                                        {

                                                                                            //// Write data  
                                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                                            {
                                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                                            }

                                                                                            // Get response  
                                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                            {
                                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                                {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                    {
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        ClaimTypeId = -1,
                                                                                                        OtherClaimType = "",
                                                                                                       ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                                        ReceiptNo = "",
                                                                                                        UploadFileName = "",
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                      ReimburseableAmt = ClaimAmt,
                                                                                                       IsManual = false,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();
                                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                                        //"Card has been loaded.",
                                                                                                        //TransactionDetails = "Amount ($" + ClaimAmt + ") has been loaded.",
                                                                                                        Amount = ClaimAmt,
                                                                                                        CompanyUserId = iObj.UserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());

                                                                                                    datacontext.SubmitChanges();
                                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                    IsSuccess = true;

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);
                                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                                    IsSuccess = false;
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        catch (WebException ex)
                                                                                        {
                                                                                            lobjResponse.Message = ex.Message;
                                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                            IsSuccess = false;
                                                                                        }
                                                                                        

                                                                                        #endregion Add Claim
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                        IsSuccess = false;
                                                                                    }


                                                                                    
                                                                                }
                                                                                else
                                                                                {
                                                                                    IsSuccess = false;
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                                IsSuccess = false;
                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            #region Add Claim




                                                                            //call load card api
                                                                            string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                            HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                            webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                            webRequest2.ContentType = "application/json; charset=utf-8";

                                                                            string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                            // Set the content length in the request headers  
                                                                            webRequest2.ContentLength = byteData.Length;
                                                                            try
                                                                            {

                                                                                //// Write data  
                                                                                using (Stream postStream = webRequest2.GetRequestStream())
                                                                                {
                                                                                    postStream.Write(byteData, 0, byteData.Length);
                                                                                }

                                                                                // Get response  
                                                                                using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                                {
                                                                                    StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                    string text3 = sr2.ReadToEnd().ToString();
                                                                                    dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                    if (CreateCardRes.Result["Success"] == true)
                                                                                    {
                                                                                        TblEmpClaim Obj = new TblEmpClaim()
                                                                                        {
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            ClaimTypeId = -1,
                                                                                            OtherClaimType = "",
                                                                                            ServiceProviderId = ServiceProviderId,
                                                                                            ReceiptDate = DateTime.UtcNow,
                                                                                            ReceiptNo = "",
                                                                                            UploadFileName = "",
                                                                                            Amount = iObj.ClaimAmount,
                                                                                            ClaimAmt = iObj.ClaimAmount,
                                                                                            ClaimFee = ClaimFee,
                                                                                            ReimburseableAmt = ClaimAmt,
                                                                                            IsManual = false,
                                                                                            ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow,
                                                                                            DateModified = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                        datacontext.SubmitChanges();
                                                                                        ClaimId = Obj.EmpClaimId;
                                                                                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                        {
                                                                                            TransactionDate = DateTime.UtcNow,
                                                                                            //TransactionDetails = "Amount ($" + ClaimAmt + ") has been loaded.",
                                                                                            TransactionDetails = "Claim Paid - Card",
                                                                                            //"Card has been loaded.",
                                                                                            Amount = ClaimAmt,
                                                                                            CompanyUserId = iObj.UserId,
                                                                                            CompanyPlanId = -1,
                                                                                            ClaimId = Obj.EmpClaimId,
                                                                                            TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                            PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                            Active = true,
                                                                                            DateCreated = DateTime.UtcNow
                                                                                        };
                                                                                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                        EmpPayment.Amount = BalanceAmount;
                                                                                        datacontext.SubmitChanges();
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                        string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                        EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);
                                                                                        NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());

                                                                                        datacontext.SubmitChanges();
                                                                                        CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);

                                                                                        lobjResponse.ID = Obj.EmpClaimId;
                                                                                        IsSuccess = true;
                                                                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);

                                                                                        lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                        IsSuccess = false;
                                                                                    }
                                                                                }

                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }


                                                                            #endregion Add Claim
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        #region Add Claim



                                                                        //call load card api
                                                                        string CreateCardUri = BaseUrl + "Card/ILoadValue/?SessionID=" + SessionId;
                                                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(CreateCardUri)) as HttpWebRequest;
                                                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                                                        webRequest2.ContentType = "application/json; charset=utf-8";

                                                                        string data = "{\"ID\":\"" + UniqueId + "\",\"OrderItemID\":1,\"Escrow\":{\"ClientEscrowTag\":\"\"},\"FXQuoteEscrowInfo\":{\"EscrowSourceProgramID\":\"\",\"RateQuoteID\":\"\"},\"Payment\":{\"ClientReference\":\"\",\"NotificationInfo\":{},\"PurseID\":\"" + PurseId + "\",\"RequestedAmount\":\"" + iObj.ClaimAmount + "\"},\"Reference\":{\"BusinessEntityID\":" + ProgramId + ",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                                                        // Set the content length in the request headers  
                                                                        webRequest2.ContentLength = byteData.Length;
                                                                        try
                                                                        {

                                                                            //// Write data  
                                                                            using (Stream postStream = webRequest2.GetRequestStream())
                                                                            {
                                                                                postStream.Write(byteData, 0, byteData.Length);
                                                                            }

                                                                            // Get response  
                                                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                                                            {
                                                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                                                string text3 = sr2.ReadToEnd().ToString();
                                                                                dynamic CreateCardRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                                                if (CreateCardRes.Result["Success"] == true)
                                                                                {
                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                    {
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        ClaimTypeId = -1,
                                                                                        OtherClaimType = "",
                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                        ReceiptDate = DateTime.UtcNow,
                                                                                        ReceiptNo = "",
                                                                                        UploadFileName = "",
                                                                                        Amount = iObj.ClaimAmount,
                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                        ClaimFee = ClaimFee,
                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                        IsManual = false,
                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.Paid),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow,
                                                                                        DateModified = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                    datacontext.SubmitChanges();
                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                    {
                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                        TransactionDetails = "Claim Paid - Card",
                                                                                        Amount = ClaimAmt,
                                                                                        CompanyUserId = iObj.UserId,
                                                                                        CompanyPlanId = -1,
                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                                        Active = true,
                                                                                        DateCreated = DateTime.UtcNow
                                                                                    };
                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                    datacontext.SubmitChanges();
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                                                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                                                                    NotifyUser1(datacontext, Text, 1, CObjects.enmNotifyType.Claim.ToString());

                                                                                    datacontext.SubmitChanges();
                                                                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, UserId, UserId, Text);
                                                                                    lobjResponse.ID = Obj.EmpClaimId;
                                                                                    IsSuccess = true;
                                                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                }
                                                                                else
                                                                                {
                                                                                    CreateCardApiResponseLog(iObj.UserId, "LoadValue", "FAILURE", CreateCardRes.Result["Success"], iObj.CompanyId, -1, CreateCardRes.Result["ErrorMessage"], data);
                                                                                    lobjResponse.Message = CreateCardRes.Result["ErrorMessage"];
                                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                    IsSuccess = false;
                                                                                }
                                                                            }

                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }


                                                                        #endregion Add Claim



                                                                    }
                                                                }


                                                                #endregion Monthly

                                                                #region Charge claim fee
                                                                if (IsClaimFeeCharge)
                                                                {
                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                    {
                                                                        TransactionDate = DateTime.UtcNow,
                                                                        TransactionDetails = "Claim Processing Fee",
                                                                        Amount = ClaimFee,
                                                                        CompanyUserId = iObj.UserId,
                                                                        CompanyPlanId = -1,
                                                                        ClaimId = ClaimId,
                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                                    {
                                                                        CompanyUserId = iObj.UserId,
                                                                        Amount = ClaimFee,
                                                                        AdjustmentDT = DateTime.UtcNow,
                                                                        Remarks = "Claim Processing Fee",
                                                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                                        Active = true,
                                                                        DateCreated = DateTime.UtcNow,
                                                                        DateModified = DateTime.UtcNow
                                                                    };
                                                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                                                    
                                                                    BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt + ClaimFee));
                                                                }
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                CreateCardApiResponseLog(iObj.UserId, "LoadLimits", "FAILURE", text4, iObj.CompanyId, CardObj.MasterCardId, FrequencyAmt.ToString(), data1);
                                                                IsSuccess = false;
                                                                lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                                                            }
                                                        }
                                                        else
                                                        {
                                                            IsSuccess = false;
                                                            lobjResponse.Message = "Claim amount exceeds card limit so your card cannot be loaded at this time.<br/> You are receiving this error because you have reached a maximum spending limit or maximum transaction limit.";
                                                            CreateCardApiResponseLog(iObj.UserId, "LoadLimits", text4, "", iObj.CompanyId, CardObj.MasterCardId, lobjResponse.Message, data1);
                                                            
                                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                        }
                                                    }


                                                }
                                                catch (Exception ex)
                                                {
                                                    IsSuccess = false;
                                                    lobjResponse.Message = ex.Message;
                                                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                }
                                            }
                                            else
                                            {
                                                IsSuccess = false;
                                                CreateCardApiResponseLog(iObj.UserId, "Login", "FAILURE", SessionId, iObj.CompanyId, CardObj.MasterCardId, SessionId, InputJson);

                                                lobjResponse.Message = "The service is not available.";
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }

                                        }
                                    }
                                    else
                                    {
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                        lobjResponse.Message = "You have not sufficient credits in your account. Because your account balanace is " + EmpPayment.Amount + " while yours claim amount including claim processingfee is $ " + CheckAmount + " .So, you are not eligible applicant for claim.";
                                        IsSuccess = false;
                                    }
                                    lobjResponse.ClaimProcessingFee = ClaimFee;

                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You have not sufficient credits in your account.So, you are not eligible applicant for claim.";
                                    IsSuccess = false;
                                }
                            }


                            if (IsSuccess)
                            {
                                #region Send Mail
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                string EmailTo = "";
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;// CarrierEmails;
                                    Mail1.Subject = "Card Value Loaded";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = iObj.CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                }

                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = UserObj.EmailAddress;

                                }

                                
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                builder.Append("$ " + iObj.ClaimAmount + " has been loaded on your activated card. For further queries you can contact with ADMIN.<br/><br/>");
                                builder.Append("Regards,<br/><br/>");
                                builder.Append("Eflex Team<br/><br/>");

                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());


                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;
                                Mail.Subject = "Card Value Loaded";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = iObj.CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);
                                #endregion


                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                NotifyUser1(datacontext, NotiText, iObj.UserId, CObjects.enmNotifyType.Load.ToString());
                                NotificationList = InsertionInNotificationTable(iObj.UserId, NotiText, datacontext, ClaimId, CObjects.enmNotifyType.Load.ToString());
                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, ClaimId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications
                            }
                            decimal BalanceAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.UserId select Convert.ToDecimal(p.Amount)).Take(1).SingleOrDefault();
                            lobjResponse.BalanceAmount = BalanceAmt;
                        }
                        else
                        {
                            IsSuccess = false;
                            lobjResponse.Message = "User doesn't exist in our database.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }
                    else
                    {
                        IsSuccess = false;
                        lobjResponse.Message = "Sorry! We are unable to found your active card.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    }

                }
                catch (System.Net.Sockets.SocketException ex1)
                {
                    //  catch (Exception ex1)
                    //{
                    IsSuccess = false;
                    lobjResponse.Message = ex1.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();

                }

                CreateResponseLog(datacontext, StartTime, iObj.UserId, "LoadValue_New", InputJson, lobjResponse.Message);
               
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion CreateCard

        #region UpdateLoadValueDetails
        public CResponse UpdateLoadValueDetails(UpdateLoadValueDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            decimal BalanceAmount = 0;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                
                        decimal ServiceProviderId = -1;
                        #region Add ServiceProvider
                        if (iObj.ServiceProviderId == -1)
                        {
                            TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Email.ToUpper() == iObj.ProviderEmail.ToUpper() select p).Take(1).SingleOrDefault();
                            if (ObjExists == null)
                            {
                                TblServiceProvider ObjExists1 = (from p in mastercontext.TblServiceProviders where p.GoogleId == iObj.GoogleUniqueId && p.Active == true select p).Take(1).SingleOrDefault();
                                if (ObjExists1 == null)
                                {
                                    TblServiceProvider Obj = new TblServiceProvider()
                                    {
                                        Name = iObj.ServiceProvider,
                                        Address = iObj.ProviderAddress,
                                        ContactNo = iObj.ProviderContactNo,
                                        Email = iObj.ProviderEmail,
                                        ServiceId = iObj.ServiceId,
                                        CompanyId = iObj.CompanyId,
                                        GoogleId = iObj.GoogleUniqueId,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                    mastercontext.SubmitChanges();
                                    ServiceProviderId = Obj.ServiceProviderId;
                                }
                                else
                                {
                                    ServiceProviderId = ObjExists1.ServiceProviderId;

                                }


                            }
                            else
                            {
                                ServiceProviderId = ObjExists.ServiceProviderId;

                            }
                        }
                        #endregion

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                           
                                                         
                                try
                                {
                                          #region Update claim details
                                            TblEmpClaim Obj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                                            if(Obj != null)
                                            {
                                                Obj.ServiceProvider = iObj.ServiceProvider;
                                                Obj.ServiceProviderId = ServiceProviderId;
                                                Obj.DependantId = iObj.Dependant;
                                                Obj.Note = iObj.Note;
                                                Obj.ServiceId = iObj.ServiceId;
                                                Obj.DateModified = DateTime.UtcNow;
                                            }
                                        
                                            datacontext.SubmitChanges();
                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                            

                                            lobjResponse.ID = Obj.EmpClaimId;
                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                            IsSuccess = true;
                                            #endregion
                                                                  
                                }
                                catch (WebException ex)
                                {

                                    lobjResponse.Message = ex.Message;
                                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    IsSuccess = false;
                                }
                         
                        }
                        else
                        {
                            IsSuccess = false;
                            lobjResponse.Message = "User doesn't exist in our database.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                   

               
                
                    CreateResponseLog(datacontext, StartTime, iObj.UserId, "UpdateLoadValueDetails", InputJson, lobjResponse.Message);
                    datacontext.Connection.Close();
                    mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion CreateCard

        #region UpdateLoadValueDetailsForApp
        public CResponse UpdateLoadValueDetailsForApp(UpdateLoadValueDetailsForAppInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            UserId = iObj.UserId;
            bool IsSuccess = false;
            decimal BalanceAmount = 0;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {

                decimal ServiceProviderId = -1;
                #region Add ServiceProvider
                if (iObj.ServiceProviderId == -1)
                {
                    TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Email.ToUpper() == iObj.ProviderEmail.ToUpper() select p).Take(1).SingleOrDefault();
                    if (ObjExists == null)
                    {
                        TblServiceProvider ObjExists1 = (from p in mastercontext.TblServiceProviders where p.GoogleId == iObj.GoogleUniqueId && p.Active == true select p).Take(1).SingleOrDefault();
                        if (ObjExists1 == null)
                        {
                            TblServiceProvider Obj = new TblServiceProvider()
                            {
                                Name = iObj.ServiceProvider,
                                Address = iObj.ProviderAddress,
                                ContactNo = iObj.ProviderContactNo,
                                Email = iObj.ProviderEmail,
                                ServiceId = iObj.ServiceId,
                                CompanyId = iObj.CompanyId,
                                GoogleId = iObj.GoogleUniqueId,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                            mastercontext.SubmitChanges();
                            ServiceProviderId = Obj.ServiceProviderId;
                        }
                        else
                        {
                            ServiceProviderId = ObjExists1.ServiceProviderId;

                        }


                    }
                    else
                    {
                        ServiceProviderId = ObjExists.ServiceProviderId;

                    }
                }
                #endregion

                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                if (EmpObj != null)
                {
                    
                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();


                    try
                    {
                        #region Update claim details
                        TblEmpClaim Obj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            TblSPReceipt ReceiptObj = (from p in mastercontext.TblSPReceipts where p.Active == true && p.ReceiptNo.ToUpper() == iObj.ReceiptNo.ToUpper() && p.ServiceProviderId == Obj.ServiceProviderId select p).Take(1).SingleOrDefault();
                            if (ReceiptObj == null || string.IsNullOrEmpty(iObj.ReceiptNo))
                            {
                                Obj.ServiceProvider = iObj.ServiceProvider;
                                Obj.ServiceProviderId = ServiceProviderId;
                                Obj.DependantId = iObj.Dependant;
                                Obj.Note = iObj.Note;
                                Obj.ServiceId = iObj.ServiceId;
                                Obj.DateModified = DateTime.UtcNow;
                                Obj.UploadFileName = iObj.FileName;
                                Obj.ReceiptDate = Convert.ToDateTime(iObj.ReceiptDt);
                                Obj.ReceiptNo = iObj.ReceiptNo;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                lobjResponse.ID = Obj.EmpClaimId;
                                IsSuccess = true;
                            }
                            else
                            {
                                TblSPReceipt RcptObj = new TblSPReceipt()
                                {
                                    ServiceProviderId = ServiceProviderId,
                                    ReceiptNo = iObj.ReceiptNo,
                                    CompanyId = iObj.CompanyId,
                                    UserId = iObj.UserId,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow,

                                };
                                mastercontext.TblSPReceipts.InsertOnSubmit(RcptObj);
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                lobjResponse.ID = Obj.EmpClaimId;
                                IsSuccess = true;
                            }
                        }
                        else
                        {
                           lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                           lobjResponse.Message = "Claim doesn't exist.";
                        }
                        #endregion

                    }
                    catch (WebException ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        IsSuccess = false;
                    }

                }
                else
                {
                    IsSuccess = false;
                    lobjResponse.Message = "User doesn't exist in our database.";
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                }




                CreateResponseLog(datacontext, StartTime, iObj.UserId, "UpdateLoadValueDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion UpdateLoadValueDetailsForApp

    }
}