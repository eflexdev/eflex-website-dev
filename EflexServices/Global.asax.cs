﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace EflexServices
{
    public class Global : System.Web.HttpApplication
    {
        public static bool ClientVerification = false;
        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
          
            RouteTable.Routes.Add(new ServiceRoute("EflexService", new WebServiceHostFactory(), typeof(EflexService)));
        }
      
    }
}