﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    public partial interface IEflexService
    {
        #region BrokerLogin
        [WebInvoke(UriTemplate = "BrokerLogin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoginEflexUserResponse BrokerLogin(LoginUserInput iObj);
        #endregion BrokerLogin

        #region AddBroker
        [WebInvoke(UriTemplate = "AddBroker", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddBroker(AddBrokerInput iObj);
        #endregion AddBroker

        #region EditBroker
        [WebInvoke(UriTemplate = "EditBroker", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse EditBroker(EditBrokerInput iObj);
        #endregion EditBroker

        #region DeleteBroker
        [WebInvoke(UriTemplate = "DeleteBroker", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteBroker(DeleteBrokerInput iObj);
        #endregion DeleteBroker

        #region MarkBrokerAsApproved
        [WebInvoke(UriTemplate = "MarkBrokerAsApproved", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse MarkBrokerAsApproved(MarkBrokerAsApprovedInput iObj);
        #endregion MarkBrokerAsApproved

        #region ListOfBrokers
        [WebInvoke(UriTemplate = "ListOfBrokers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokersResponse ListOfBrokers(LazyLoadingInput iObj);
        #endregion ListOfBrokers

        #region RegisterBroker
        [WebInvoke(UriTemplate = "RegisterBroker", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CRegisterBrokerResponse RegisterBroker(RegisterBrokerInput iObj);
        #endregion RegisterBroker

        #region AddEflexUserPAPDetails
        [WebInvoke(UriTemplate = "AddEflexUserPAPDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEflexUserPAPDetails(AddEflexUserPAPDetailsInput iObj);
        #endregion AddEflexUserPAPDetails

        #region GetBrokerProfile
        [WebInvoke(UriTemplate = "GetBrokerProfile", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetBrokerProfileResponse GetBrokerProfile(IdInputv2 iObj);
        #endregion GetBrokerProfile

        #region AddBrokerCompany
        [WebInvoke(UriTemplate = "AddBrokerCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAddBrokerCompanyResponse AddBrokerCompany(AddBrokerCompanyInput iObj);
        #endregion AddBrokerCompany

        #region ListOfBrokerCompanies
        [WebInvoke(UriTemplate = "ListOfBrokerCompanies", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokerCompaniesResponse ListOfBrokerCompanies(LazyLoadingInputV2 iObj);
        #endregion ListOfBrokerCompanies

        #region ApproveCompany
        [WebInvoke(UriTemplate = "ApproveCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ApproveCompany(IdInputv2 iObj);
        #endregion ApproveCompany

        #region ListOfCompanyBrokers
        [WebInvoke(UriTemplate = "ListOfCompanyBrokers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokersResponse ListOfCompanyBrokers(ListOfCompanyBrokersInput iObj);
        #endregion ListOfCompanyBrokers

        #region ListOfChildBrokers
        [WebInvoke(UriTemplate = "ListOfChildBrokers", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokersResponse ListOfChildBrokers(LazyLoadingInputV2 iObj);
        #endregion ListOfChildBrokers

        #region EditEflexUserPAPDetails
        [WebInvoke(UriTemplate = "EditEflexUserPAPDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse EditEflexUserPAPDetails(EditEflexUserPAPDetailsInput iObj);
        #endregion EditEflexUserPAPDetails

        #region UpdateBrokerCommissionCharges
        [WebInvoke(UriTemplate = "UpdateBrokerCommissionCharges", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateBrokerCommissionCharges(UpdateCommissionInput iObj);
        #endregion UpdateBrokerCommissionCharges

        #region GetBrokerCompanyDetails
        [WebInvoke(UriTemplate = "GetBrokerCompanyDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetCompanyDetailsResponse GetBrokerCompanyDetails(GetCompanyDetailsInput iObj);
        #endregion GetBrokerCompanyDetails

        #region GetBrokerDashboardData
        [WebInvoke(UriTemplate = "GetBrokerDashboardData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetBrokerDashboardDataResponse GetBrokerDashboardData(GetBrokerDashboardDataInput iObj);
        #endregion GetBrokerDashboardData

        #region GetAllBrokerTransactions
        [WebInvoke(UriTemplate = "GetAllBrokerTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetAllBrokerTransactionsResponse GetAllBrokerTransactions(LazyLoadingInputV2 Obj);
        #endregion GetAllBrokerTransactions

        #region AddBrokerAdjustments
        [WebInvoke(UriTemplate = "AddBrokerAdjustments", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddBrokerAdjustments(AddBrokerAdjustmentsInput iObj);
        #endregion AddBrokerAdjustments

        #region ListBrokerDuePayment
        [WebInvoke(UriTemplate = "ListBrokerDuePayment", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListBrokerPaymentResponse ListBrokerDuePayment(BrokerPaymentInput iObj);
        #endregion ListBrokerDuePayment

        #region ListBrokerReceivedPayments
        [WebInvoke(UriTemplate = "ListBrokerReceivedPayments", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListBrokerPaymentResponse ListBrokerReceivedPayments(BrokerPaymentInput iObj);
        #endregion ListBrokerReceivedPayments

        #region ForgotBrokerPassword
        [WebInvoke(UriTemplate = "ForgotBrokerPassword", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ForgotBrokerPassword(ForgetPasswordInput iObj);
        #endregion ForgotBrokerPassword

        #region RemitBrokerPayment
        [WebInvoke(UriTemplate = "RemitBrokerPayment", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse RemitBrokerPayment(IdInputv2 iObj);
        #endregion RemitBrokerPayment

        #region ResentBrokerCredentials
        [WebInvoke(UriTemplate = "ResentBrokerCredentials", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResentBrokerCredentials(ResentBrokerCredentialsInput iObj);
        #endregion ResentBrokerCredentials

        #region SaveBrokerReferral
        [WebInvoke(UriTemplate = "SaveBrokerReferral", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SaveBrokerReferral(SaveBrokerReferralInput iObj);
        #endregion SaveBrokerReferral

        #region SavePreferredSlots
        [WebInvoke(UriTemplate = "SavePreferredSlots", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse SavePreferredSlots(SavePreferredSlotsInput iObj);
        #endregion SavePreferredSlots

        #region UpdatePreferredSlots
        [WebInvoke(UriTemplate = "UpdatePreferredSlots", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdatePreferredSlots(UpdatePreferredSlotsInput iObj);
        #endregion UpdatePreferredSlots

        #region ListOfBrokerReferrals
        [WebInvoke(UriTemplate = "ListOfBrokerReferrals", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokerReferralsResponse ListOfBrokerReferrals(LazyLoadingInputV2 iObj);
        #endregion ListOfBrokerReferrals

        #region GetPreferredSlotsById
        [WebInvoke(UriTemplate = "GetPreferredSlotsById", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetPreferredSlotsByIdResponse GetPreferredSlotsById(IdInput iObj);
        #endregion GetPreferredSlotsById

        #region MarkDoneBrokerReferral
        [WebInvoke(UriTemplate = "MarkDoneBrokerReferral", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse MarkDoneBrokerReferral(MarkDoneBrokerReferralInput iObj);
        #endregion MarkDoneBrokerReferral
    }
}