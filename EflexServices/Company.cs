﻿using EnCryptDecrypt;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Script.Serialization;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region GetCompanyDashboardData
        public CGetCompanyDashboardDataResponse GetCompanyDashboardData(DbConIdInput iObj)
        {
            CGetCompanyDashboardDataResponse lobjResponse = new CGetCompanyDashboardDataResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            UserId = iObj.Id;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        List<GetDashboardData> Obj = new List<GetDashboardData>();

                        List<TblCompanyPlan> Planobj = (from p in datacontext.TblCompanyPlans where p.Active == true select p).Distinct().ToList();
                        if (Planobj != null && Planobj.Count() > 0)
                        {
                            foreach (TblCompanyPlan item in Planobj)
                            {
                                decimal TotalPayableAmount = 0;
                                decimal PlanMemberCount = 0;
                                PlanMemberCount = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyPlanId == item.CompanyPlanId && p.IsActive == true select p).Count();
                                if (PlanMemberCount > 0)
                                {
                                    List<TblPayment> PaymentObj = (from p in datacontext.TblPayments 
                                                                   join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                                   where p.Active == true && q.IsActive == true && q.Active ==true 
                                                                   && p.PaymentTypeId != "ADJ" && p.CompanyPlanId == item.CompanyPlanId && 
                                                                   p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) select p).ToList();
                                    if (PaymentObj != null && PaymentObj.Count() > 0)
                                    {

                                        foreach (TblPayment Paymentitem in PaymentObj)
                                        {
                                          TotalPayableAmount = TotalPayableAmount + (Convert.ToDecimal(Paymentitem.Amount));

                                         
                                        }
                                    }
                                }
                                else
                                {
                                    TotalPayableAmount = 0;
                                }
                                GetDashboardData Obj1 = new GetDashboardData()
                                {
                                    Color = (item.Color == null || item.Color == "") ? "#ffbb33" : item.Color,
                                    Plan = item.Name,
                                    GrossAmount = TotalPayableAmount,
                                    CompanyEmpData = GetCompanyEmpData(datacontext, item.CompanyPlanId)
                                };

                                Obj.Add(Obj1);


                            }

                            lobjResponse.GetDashboardData = Obj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "SUCCESS";


                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "FAILURE";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }


                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "GetCompanyDashboardData", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
               
            }
            return lobjResponse;
        }

         #endregion

        #region GetDashboardTransactionsOld
        public CGetDashboardTransactionsResponse GetDashboardTransactionsOld(DbConIdInput iObj)
        {
            CGetDashboardTransactionsResponse lobjResponse = new CGetDashboardTransactionsResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    if (IsValidateUser(datacontext))
                    {
                        
                        List<PendingTransaction> PendingObj = (from p in mastercontext.TblCompPayments
                                                               where p.Active == true && p.CompanyId == CompanyId 
                                                               && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                               select new PendingTransaction()
                                                               {
                                                                   Amount = Convert.ToDecimal(p.Amount),
                                                                   Date = Convert.ToString(p.PaymentDueDate),
                                                                   Details = p.PaymentDetails,
                                                                   PaymentType = p.PaymentTypeId,
                                                                   TypeId = p.TypeId == 1 ? "Credit" : "Debit"
                                                               }).OrderByDescending(x => x.Date).Take(10).ToList();
                        List<CurrentTransaction> CurrentObj = (from p in mastercontext.TblCompPayments
                                                               where p.Active == true && p.CompanyId == CompanyId
                                                               && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                               select new CurrentTransaction()
                                                               {
                                                                   Amount = Convert.ToDecimal(p.Amount),
                                                                   Date = Convert.ToString(p.PaymentDueDate),
                                                                   Details = p.PaymentDetails,
                                                                   PaymentType = p.PaymentTypeId,
                                                                   TypeId = p.TypeId == 1 ? "Credit" : "Debit"
                                                               }).OrderByDescending(x => x.Date).Take(10).ToList();


                        List<TblCompPayment> DueAmt = (from p in mastercontext.TblCompPayments where p.Active == true && 
                                                           p.CompanyId == CompanyId && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() 
                                                           && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                        decimal TotalDueAmt = 0;
                        if(DueAmt != null && DueAmt.Count() > 0)
                        {
                             TotalDueAmt = (from p in mastercontext.TblCompPayments where p.Active == true && 
                                                       p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) 
                                                      select Convert.ToDecimal(p.Amount)).Sum();
                            lobjResponse.AmountDue = TotalDueAmt;
                        
                        }
                        decimal AvailableCredits = 0;

                        TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                       
                       if (CreditObj != null)
                        {
                            AvailableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select Convert.ToDecimal(p.Credits)).Sum();
                        }

                        GetCompDashboardFigures FiguresObj = new GetCompDashboardFigures()
                        {
                            TotalAdmin = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin) select p).Count(),
                            TotalMembers = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee) select p).Count(),
                            TotalClaimed = 1,
                            AmountDue = TotalDueAmt,
                            AvailableCredits = AvailableCredits,
                        };

                        lobjResponse.GetCompDashboardFigures = FiguresObj;
                        lobjResponse.AvailableCredits = AvailableCredits;
                        lobjResponse.PendingTransaction = PendingObj;
                        lobjResponse.CurrentTransaction = CurrentObj;
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, UserId, "GetDashboardTransactionsOld", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion

        #region GetStatementPeriod
        public CGetStatementPeriodResponse GetStatementPeriod(DbConInput iObj)
        {
            CGetStatementPeriodResponse lobjResponse = new CGetStatementPeriodResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            List<StatementPeriod> Obj = new List<StatementPeriod>();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    DateTime? CompanyCreationDT = null;
                    using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                    {
                         decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
                         CompanyCreationDT = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p.DateCreated).Take(1).SingleOrDefault();
                         Int32 CreationDtMonths = CompanyCreationDT.Value.Month;
                         Int32 CurrentMonth = DateTime.UtcNow.Month;
                         Int32 MonthDiff = CurrentMonth-CreationDtMonths;

                        DateTime TwelveDayOfMonth = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 12);
                        if(MonthDiff >0)
                        {
                            for (int i = 1; i <= MonthDiff; i++)
			                {
                                StatementPeriod StatementPeriodObj = new StatementPeriod()
                                {
                                    Period = String.Format("{0:MMM dd,yyyy}", (TwelveDayOfMonth.AddMonths(-i))),
                                };
                                Obj.Add(StatementPeriodObj);
                                if(i==4)
                                {
                                    break;
                                }
			                }

                        }
                        else 
                        {
                            if (MonthDiff < 0)
                            {
                                for (int i = 1; i <= 4; i++)
                                {
                                    StatementPeriod StatementPeriodObj = new StatementPeriod()
                                    {
                                        Period = String.Format("{0:MMM dd,yyyy}", (TwelveDayOfMonth.AddMonths(-i))),
                                    };
                                    Obj.Add(StatementPeriodObj);
                                }
                            }
                        }

                        mastercontext.Connection.Close();
                    }
                    lobjResponse.StatementPeriod = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    lobjResponse.Message = "SUCCESS";
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, UserId, "GetStatementPeriod", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetStatementPeriod

        #region GetActivityData
        public CGetActivityDataResponse GetActivityData(GetActivityInput iObj)
        {
            CGetActivityDataResponse lobjResponse = new CGetActivityDataResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    if (IsValidateUser(datacontext))
                    {
                        decimal Payments = 0;
                        decimal Adjustments = 0;
                        decimal PaymentCount = 0;
                        decimal AdjustmentsCount = 0;
                        PaymentCount = (from p in mastercontext.TblCompPayments where p.CompanyId == CompanyId && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() && p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) select p.CompPaymentId).Count();
                        if (PaymentCount > 0)
                        {
                            Payments = (from p in mastercontext.TblCompPayments where p.CompanyId == CompanyId && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() && p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) select p.Amount ?? 0).Sum();
                        }



                        AdjustmentsCount = (from p in mastercontext.TblCompAdjustments where p.CompanyId == CompanyId 
                                             && p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString() 
                                            && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) 
                                            select p.CompAdjustmentId).Count();
                        if (AdjustmentsCount > 0)
                        {
                            Adjustments = (from p in mastercontext.TblCompAdjustments where p.CompanyId == CompanyId && 
                                           p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                           && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) 
                                           select Convert.ToDecimal(p.Amount)).Sum();
                       
                        }
                        decimal AvailableCredits = 0;
                        
                        TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();

                        if (CreditObj != null)
                        {
                            AvailableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select Convert.ToDecimal(p.Credits)).Sum();
                        }
                        List<PendingTransaction> PendingObj = (from p in mastercontext.TblCompPayments
                                                               where p.Active == true && p.CompanyId == CompanyId 
                                                               && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                               select new PendingTransaction()
                                                               {
                                                                   Amount = Convert.ToDecimal(p.Amount),
                                                                   Date = Convert.ToString(p.PaymentDueDate),
                                                                   Details = p.PaymentDetails,
                                                                   PaymentType = p.PaymentTypeId,
                                                                   TypeId = p.TypeId == 1? "Credit":"Debit"
                                                               }).OrderByDescending(x => x.Date).Take(10).ToList();
                       List<ActivityList> ActivityObj = (from p in mastercontext.sp_GetCompActivityList(iObj.ChunkStart, iObj.SearchString, iObj.ChunkSize, Convert.ToDateTime(iObj.Period),CompanyId, iObj.Id)
                                                              select new ActivityList()
                                                              {
                                                                  TransactionDate = Convert.ToString(p.AdjustmentDT),
                                                                  PostedDate = Convert.ToString(p.AdjustmentDT),
                                                                  Description = p.Remarks,
                                                                  Amount = Convert.ToDecimal(p.Amount),
                                                                  TypeId = p.TypeId == 1 ? "Credit" : "Debit"
                                                              }).ToList();
                            lobjResponse.ActivityList = ActivityObj;
                        

                       
                        lobjResponse.PendingTransaction = PendingObj;
                        lobjResponse.TotalBalance = Convert.ToDecimal(Payments);
                        lobjResponse.Adjustments = Convert.ToDecimal(Adjustments);
                        lobjResponse.AvailableCredits = Convert.ToDecimal(AvailableCredits);
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }

                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                    mastercontext.Connection.Close();
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, UserId, "GetActivityData", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetActivityData

        #region GetPAPDetails
        public CGetPAPDetailsResponse GetPAPDetails(DbConIdInput iObj)
        {
            CGetPAPDetailsResponse lobjResponse = new CGetPAPDetailsResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        GetPAPDetails Obj = (from p in datacontext.TblPAPDetails
                                             where p.Active == true && p.CompanyUserId == iObj.Id
                                             select new GetPAPDetails()
                                                 {
                                                     PAPDetailId = p.PAPDetailId,
                                                     CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                     BankName = p.BankName,
                                                     TransitId = p.TransitId,
                                                     InstitutionId = p.InstitutionId,
                                                     AccountNumber = p.AccountNumber,
                                                     ChequeFile = p.ChequeFile,
                                                     IsCheque = Convert.ToString(p.IsCheque),
                                                     IsAgree = Convert.ToString(p.IsAgree)
                                                 }).Take(1).SingleOrDefault();
                        lobjResponse.GetPAPDetails = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "GetPAPDetails", InputJson, lobjResponse.Message);

                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetPAPDetails

        #region UpdatePAPDetails
        public CResponse UpdatePAPDetails(UpdatePAPDetailsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        TblPAPDetail Obj = (from p in datacontext.TblPAPDetails where p.Active == true && p.PAPDetailId == iObj.PAPDetailId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            Obj.BankName = iObj.BankName;
                            Obj.TransitId = iObj.TransitId;
                            Obj.InstitutionId = iObj.InstitutionId;
                            Obj.AccountNumber = iObj.AccountNumber;
                            Obj.ChequeFile = iObj.ChequeFile;
                            Obj.IsCheque = Convert.ToBoolean(iObj.IsCheque);
                            Obj.IsAgree = Convert.ToBoolean(iObj.IsAgree);
                            Obj.DateModified = DateTime.UtcNow;
                            datacontext.SubmitChanges();
                        }

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "UpdatePAPDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdatePAPDetails

        #region UpdateCompanyDetails
        public CResponse UpdateCompanyDetails(UpdateCompanyDetailsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {

                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CompanyObj != null)
                    {
                        TblCompany CompanyObj1 = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyName.ToUpper() == iObj.CompanyName.ToUpper() && p.Address1 == iObj.Address1 && p.PostalCode == iObj.PostalCode select p).Take(1).SingleOrDefault();
                        if (CompanyObj1 != null && CompanyObj1.CompanyId != iObj.CompanyId)
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Company already exist with this name.";
                        }
                        else
                        {
                            
                            CompanyObj.CompanyName = iObj.CompanyName;
                            CompanyObj.FirstName = iObj.FirstName;
                            CompanyObj.LastName = iObj.LastName;
                            CompanyObj.Address1 = iObj.Address1;
                            CompanyObj.Address2 = iObj.Address2;
                            CompanyObj.City = iObj.City;
                            CompanyObj.Province = iObj.ProvinceId;
                            CompanyObj.PostalCode = iObj.PostalCode;
                            CompanyObj.Country = iObj.Country;
                            CompanyObj.OwnerContactNo = iObj.OwnerContactNo;
                            CompanyObj.OwnerEmail = iObj.OwnerEmail;
                          
                            CompanyObj.DateModified = DateTime.UtcNow;


                            TblCompany CompanyObj2 = (from p in mastercontext.TblCompanies where p.Active == true && p.UserName.ToUpper() == iObj.UserName.ToUpper() select p).Take(1).SingleOrDefault();
                            if (CompanyObj2 != null)
                            {
                                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                                {
                                    TblCompanyUser CompUserObj1 = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                                    if (CompUserObj1 != null)
                                    {
                                        TblCompanyUser CompUserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserName.ToUpper() == iObj.UserName.ToUpper() select p).Take(1).SingleOrDefault();
                                        if (CompUserObj == null)
                                        {
                                            CompUserObj1.UserName = iObj.UserName;
                                            
                                            datacontext.SubmitChanges();
                                        }
                                        CompUserObj1.FirstName = iObj.FirstName;
                                        CompUserObj1.LastName = iObj.LastName;
                                        CompUserObj1.EmailAddress = iObj.OwnerEmail;

                                    }
                                    datacontext.SubmitChanges();
                                    datacontext.Connection.Close();
                                }


                            }

                            if (CompanyObj.UserName != iObj.UserName)
                            {
                                CompanyObj.UserName = iObj.UserName;
                            }
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "Company details updated successfully.";

                            string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                            EflexNotifyLog(mastercontext, DateTime.UtcNow, lobjResponse.Message, ForUserIds);
                            mastercontext.Connection.Close();
                        }



                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "UpdateCompanyDetails", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }


            return lobjResponse;
        }
        #endregion

        #region AddEmployee
        public CResponse AddEmployee(AddEmployeeInput iObj) 
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            string UniqueCode = "";
            string ErrorString = "";
            UserId = iObj.LoggedInUserId;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            string PaymentType = "";
            decimal AmountToBepaid = 0;
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
                                               
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                        {
                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.DBCon == iObj.DbCon select p).Take(1).SingleOrDefault();
                            if (CompanyObj != null)
                            {
                                UniqueCode = CompanyObj.UniqueCode;
                                mastercontext.SubmitChanges();
                            }

                            #region Add Employees
                            DateTime? PaymentDate = null;
                            string PaymentDetails = "";
                            if (iObj.EmployeesList != null && iObj.EmployeesList.Count() > 0)
                            {
                                List<ClasswisePayments> ClasswisePaymentsObj = new List<ClasswisePayments>();
                                
                                #region Set Payment date


                                TblCompPayment CompanyPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (CompanyPaymentObj != null)
                                {
                                    if (CompanyPaymentObj.PaymentDate >= DateTime.UtcNow)
                                    {
                                        PaymentDate = CompanyPaymentObj.PaymentDate;
                                    }
                                    else
                                    {
                                        DateTime now = DateTime.UtcNow;
                                        var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                        var SevenDT = new DateTime(now.Year, now.Month, 7);
                                        DateTime date = DateTime.Now;

                                        DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                        string dayToday = DayOnTwelve.ToString();

                                        // compare enums
                                        if (DayOnTwelve == DayOfWeek.Saturday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(2);
                                        }
                                        else if (DayOnTwelve == DayOfWeek.Sunday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(1);
                                        }
                                        else
                                        {
                                            PaymentDate = TweleveDT.Date;
                                        }


                                        if (now.Date > PaymentDate)
                                        {
                                            PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                        }

                                    }
                                }
                                else
                                {
                                    DateTime now = DateTime.UtcNow;
                                    var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                    var SevenDT = new DateTime(now.Year, now.Month, 7);
                                    DateTime date = DateTime.Now;

                                    DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                    string dayToday = DayOnTwelve.ToString();

                                    // compare enums
                                    if (DayOnTwelve == DayOfWeek.Saturday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(2);
                                    }
                                    else if (DayOnTwelve == DayOfWeek.Sunday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(1);
                                    }
                                    else
                                    {
                                        PaymentDate = TweleveDT.Date;
                                    }


                                    if (now.Date > PaymentDate)
                                    {
                                        PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                    }

                                }

                                #endregion
                                
                                foreach (EmployeesList item in iObj.EmployeesList)
                                {
                                    if (item.CompanyUserId == -1)
                                    {
                                        TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();
                                        if (PlanObj != null)
                                        {

                                            TblCompPayment CompPaymentobj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                            if(CompPaymentobj != null)
                                             {
                                                 PaymentDate = CompPaymentobj.PaymentDueDate;
                                             }
                                             else
                                             {
                                                 PaymentDate = PlanObj.PlanStartDt;
                                             }


                                            if (PlanObj.ContributionSchedule == 1)
                                            {
                                                PaymentType = CObjects.enumPaymentType.DM.ToString();
                                            }
                                            else
                                            {
                                                PaymentType = CObjects.enumPaymentType.DA.ToString();

                                            }
                                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.EmailAddress == item.Email select p).Take(1).SingleOrDefault();
                                            if (UserObj == null)
                                            {
                                                string Password = RandomString();
                                                string EncryptPwd = CryptorEngine.Encrypt(Password);
                                                decimal GetLatestId = 0;
                                                GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                                if (GetLatestId == 0)
                                                {
                                                    GetLatestId = 1;
                                                }
                                                else
                                                {
                                                    GetLatestId = GetLatestId + 1;
                                                }
                                                TblCompanyUser Obj = new TblCompanyUser()
                                                {
                                                    FirstName = item.FName,
                                                    LastName = item.LName,
                                                    UserName = (UniqueCode + "_" + item.FName) + GetLatestId.ToString(),
                                                    ContactNo = item.ContactNo,
                                                    Pwd = EncryptPwd,
                                                    UserTypeId = Convert.ToInt16(item.UserTypeId),
                                                    EmailAddress = item.Email,
                                                    IsActive = true,
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow
                                                };
                                                datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                                datacontext.SubmitChanges();

                                                UpdateMsgUser(Obj, "ADD", CompanyId);

                                                TblEmployee EmpObj = new TblEmployee()
                                                {
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    CompanyPlanId = item.CompanyPlanId,
                                                    FirstName = item.FName,
                                                    LastName = item.LName,
                                                    MobileNo = item.ContactNo,
                                                    EmailAddress = item.Email,
                                                    IsActive = true,
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow
                                                };
                                                datacontext.TblEmployees.InsertOnSubmit(EmpObj);
                                                datacontext.SubmitChanges();
                                                CompanyObj.NoOfEmployee = CompanyObj.NoOfEmployee + 1;
                                                mastercontext.SubmitChanges();
                                             
                                        
                                 

                                                #region payment

                                               

                                              
                                                PaymentDetails = "Initial Payment";
                                                PaymentType = CObjects.enumPaymentType.IN.ToString();

                                                TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                {
                                                    TransactionDate = PaymentDate,
                                                    TransactionDetails = PaymentDetails,
                                                    Amount = PlanObj.NetDeposit,
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                                    ClaimId = -1,
                                                    PaymentTypeId = PaymentType,
                                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow
                                                };
                                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                                decimal BalanceAmount = 0;
                                                 #region Apply account setUp fee
                                                TblFeeDetail SetupFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.Title.ToUpper() == "SETUP FEE/INITIAL FEE" select p).Take(1).SingleOrDefault();
                                                if (SetupFeeObj != null)
                                                {
                                                    TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                                    {
                                                        TransactionDate = DateTime.UtcNow,
                                                        TransactionDetails = "Account Set-up Fee",
                                                        Amount = SetupFeeObj.Amount,
                                                        CompanyUserId = Obj.CompanyUserId,
                                                        CompanyPlanId = item.CompanyPlanId,
                                                        ClaimId = -1,
                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow
                                                    };
                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                    {
                                                        CompanyUserId = Obj.CompanyUserId,
                                                        Amount = SetupFeeObj.Amount,
                                                        AdjustmentDT = DateTime.UtcNow,
                                                        Remarks = "Account SetUp Fee",
                                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow,
                                                        DateModified = DateTime.UtcNow
                                                    };
                                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);

                                                    BalanceAmount = Convert.ToDecimal(-SetupFeeObj.Amount);
                                                }
                                                #endregion

                                                TblPayment PaymentObj1 = new TblPayment()
                                                {
                                                    PaymentDate = PaymentDate,
                                                    PaymentDetails = PaymentDetails,
                                                    RecurringDeposit = PlanObj.RecurringDeposit,
                                                    InitialDeposit = PlanObj.InitialDeposit,
                                                    NetDeposit = PlanObj.NetDeposit,
                                                    AdministrationInitialFee = PlanObj.AdministrationInitialFee,
                                                    AdministrationMonthlyFee = PlanObj.AdministrationMonthlyFee,
                                                    HSTInitial = PlanObj.HSTInitial,
                                                    HSTMonthly = PlanObj.HSTMonthly,
                                                    PremiumTaxInitial = PlanObj.PremiumTaxInitial,
                                                    PremiumTaxMonthly = PlanObj.PremiumTaxMonthly,
                                                    RetailSalesTaxInitial = PlanObj.RetailSalesTaxInitial,
                                                    RetailSalesTaxMonthly = PlanObj.RetailSalesTaxMonthly,
                                                    GrossTotalInitial = PlanObj.GrossTotalInitial,
                                                    GrossTotalMonthly = PlanObj.GrossTotalMonthly,
                                                    Amount = PlanObj.GrossTotalInitial + PlanObj.GrossTotalMonthly + PlanObj.NetDeposit,
                                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    PaymentDueDate = PaymentDate,
                                                    PaymentTypeId = PaymentType,
                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    IsRemit = false
                                                };


                                                datacontext.TblPayments.InsertOnSubmit(PaymentObj1);

                                                TblEmpBalance EmpPaymentObj = new TblEmpBalance()
                                                {
                                                    Amount = BalanceAmount,
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    Active = true,
                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                    DateModified = DateTime.UtcNow,
                                                    DateCreated = DateTime.UtcNow
                                                };

                                                datacontext.TblEmpBalances.InsertOnSubmit(EmpPaymentObj);

                                                
                                                datacontext.SubmitChanges();
                                                AmountToBepaid = Convert.ToDecimal(PaymentObj1.Amount);

                                                #endregion payment

                                                #region Broker Commission
                                                TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                                   where p.Active == true && p.CompanyId == CompanyObj.CompanyId
                                                                                   select p).Take(1).SingleOrDefault();
                                                if (CompReferral != null)
                                                {
                                                    
                                                    decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                                    where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                                    select Convert.ToDecimal(p.NetDeposit)).Sum();

                                                    if (CompReferral.BrokerId != -1)
                                                    {
                                                        TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                                               where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                                               select p).Take(1).SingleOrDefault();
                                                        TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                                      where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyObj.CompanyId
                                                                                      select p).Take(1).SingleOrDefault();
                                                        decimal PercentageBrokerCharge = 0;
                                                        if (FeeObj != null)
                                                        {
                                                            if (FeeObj.BrokerFee > 0)
                                                            {
                                                                PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                                            {
                                                                PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                                            }
                                                            else
                                                            {
                                                                PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                                            }
                                                        }


                                                        if (PercentageBrokerCharge > 0)
                                                        {
                                                            decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                                            TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                                               where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                                               p.UserId == BrokerObj.UserId
                                                                                               select p).Take(1).SingleOrDefault();
                                                            if (TransactionObj != null)
                                                            {
                                                                TransactionObj.Amount = Amount;
                                                                TransactionObj.Contribution = BrokerAmountToBepaid;
                                                                TransactionObj.PercentFee = PercentageBrokerCharge;
                                                            }
                                                            else
                                                            {
                                                                TblBrokerPayment TransactionObj1 = new TblBrokerPayment()
                                                                {
                                                                    PaymentDate = DateTime.UtcNow,
                                                                    PaymentDueDate = DateTime.UtcNow,
                                                                    PaymentDetails = "Payments",
                                                                    Amount = Amount,
                                                                    CompanyId = CompanyObj.CompanyId,
                                                                    Contribution = BrokerAmountToBepaid,
                                                                    PercentFee = PercentageBrokerCharge,
                                                                    UserId = BrokerObj.UserId,
                                                                    PaymentTypeId = CObjects.enumPaymentType.DM.ToString(),
                                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                    Active = true,
                                                                    DateCreated = DateTime.UtcNow
                                                                };
                                                                mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj1);
                                                            }
                                                            mastercontext.SubmitChanges();
                                                        }
                                                    }
                                                }
                                                #endregion Broker Commission

                                                #region SendMail

                                                
                                                StringBuilder builder = new StringBuilder();
                                                builder.Append("Hello " + item.FName + ",<br/><br/>");
                                                builder.Append("Welcome to eFlex, your very own health spending account set up by your employer, " + CompanyObj.CompanyName + ". <br/><br/>");
                                                builder.Append("This account provides you with the flexibility to claim medical expenses so you’re not out of pocket. <br/>To view how much money you’re eligible to use, click <a href=" + lstrloginUrl + " >here</a> to complete your online account as well as view your balance.<br/><br/>");
                                                builder.Append("For further convenience, we have SMS Messaging.  Here you will receive updates throughout your claims process so your always aware of what’s happening in your account<br/><br/>");
                                                builder.Append("Please use the following credentials for login to eFlex Portal:<br/><br/>");
                                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                                builder.Append("Username: " + Obj.UserName.Split('_')[1] + "<br/><br/>");
                                                builder.Append("Password: " + Password + "<br/><br/>");
                                                
                                                string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                                CMail Mail = new CMail();
                                                Mail.EmailTo = item.Email;
                                                Mail.Subject = "eFlex Account - Welcome";
                                                Mail.MessageBody = lstrMessage;
                                                Mail.GodaddyUserName = GodaddyUserName;
                                                Mail.GodaddyPassword = GodaddyPwd;
                                                Mail.Server = Server;
                                                Mail.Port = Port;
                                                Mail.CompanyId = CompanyObj.CompanyId;
                                                bool IsSent = Mail.SendEMail(mastercontext, true);

                                                List<TblNotificationDetail> NotificationUserList = null;
                                                string PushNotifyText = "Your eFlex account has been set up by owner. Please login and update your profile. Thanks!";

                                                NotificationUserList = InsertionInNotificationTable(Obj.CompanyUserId, PushNotifyText, datacontext, Obj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());
                                                if (NotificationUserList != null)
                                                {
                                                    NotificationList.AddRange(NotificationUserList);
                                                    NotificationUserList.Clear();
                                                }

                                                #endregion SendMail
                                            }
                                            else
                                            {
                                                if (ErrorString == "")
                                                {
                                                    ErrorString = item.Email + ",";
                                                }
                                                else
                                                {
                                                    ErrorString = ErrorString + "," + item.Email + ",";
                                                }
                                            }
                                        }

                                    }

                                    string Text = "An employee named as " + item.FName + " " + item.LName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                    CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);
                                    string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                    EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                                
                                }//end foreach

                                #region Push Notifications


                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, -1);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications

                                #region UpdateCompanyPayment
                                decimal OwnerId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();
                                TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (CompPaymentObj != null)
                                {
                                    CompPaymentObj.Amount = CompPaymentObj.Amount + AmountToBepaid;
                                }
                                else
                                {
                                    TblCompPayment CompPaymentObj1 = new TblCompPayment()
                                    {
                                        PaymentDueDate = PaymentDate,
                                        PaymentDate = PaymentDate,
                                        PaymentDetails = PaymentDetails,
                                        Amount = AmountToBepaid,
                                        CompanyId = CompanyObj.CompanyId,
                                        CompanyUserId = OwnerId,
                                        PaymentTypeId = PaymentType,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                                    mastercontext.SubmitChanges();
                                    TblCompAdjustment AdjObj = new TblCompAdjustment()
                                    {
                                        CompanyId = CompanyObj.CompanyId,
                                        Amount = AmountToBepaid,
                                        CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                        PaymentTypeId = PaymentType,
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = PaymentDetails,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                                }
                                mastercontext.SubmitChanges();
                                #endregion
                            }
                            #endregion

                            if (ErrorString != "")
                            {
                                ErrorString = ErrorString.TrimEnd(',');
                                lobjResponse.Message = "We couldn't add all user because some email ids are already used by other users (" + ErrorString + ").";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                            else
                            {
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            mastercontext.Connection.Close();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "AddEmployee", "", lobjResponse.Message);
                datacontext.Connection.Close();
              
            }
            return lobjResponse;
        }
        #endregion

        #region GetEStatement
        public CGetEStatementResponse GetEStatement(GetEStatementInput iObj)
        {
            CGetEStatementResponse lobjResponse = new CGetEStatementResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateUser(datacontext))
                    {
                        string contents = "";
                        StringBuilder builder = new StringBuilder();
                        string SamplePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        contents = File.ReadAllText(SamplePath + "eStatementSample.html");
                        MasterDBDataContext mastercontext = new MasterDBDataContext();
                        string lstrDetailHtml = "";
                        System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                        string strMonthName = mfi.GetMonthName(iObj.Month).ToString();
                        List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() && (p.PaymentDueDate.Value.Month == iObj.Month && p.PaymentDueDate.Value.Year == iObj.Year) select p).ToList();
                        decimal TotalContribution = 0;

                        decimal TotalAdminFee = 0;
                        decimal TotalHST = 0;
                        decimal TotalPPT = 0;
                        decimal TotalRST = 0;
                        decimal TotaltaxFees = 0;
                        if (PaymentObj != null && PaymentObj.Count() > 0)
                        {
                            builder.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                            builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td></tr>");
                            foreach (TblPayment item in PaymentObj)
                            {
                                string EmployeeName = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == item.CompanyUserId select p.FirstName + " " + p.LastName).Take(1).SingleOrDefault();
                                TblCompanyPlan Plan = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();

                                builder.Append("<tr style=\"background-color:#eee;\">");
                                builder.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.PaymentDate).ToString("MMM dd, yyyy") + "</b></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"><b>" + EmployeeName + "- " + Plan.Name + "</b></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"><b>$" + item.NetDeposit + "</b></td>");
                                builder.Append("</tr>");
                                if (item.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())//if initial amount is pending
                                {
                                    builder.Append("<tr bgcolor=\"#fff\">");
                                    builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                    builder.Append("<td width=\"30%\" align=\"right\">Initial</td>");
                                    builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                    builder.Append("<td width=\"30%\" align=\"right\">$" + item.InitialDeposit + "</td>");
                                    builder.Append("</tr>");
                                    if (Plan.ContributionSchedule == 1)
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                    else
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                }
                                else //if initial amount is not pending
                                {
                                    if (Plan.ContributionSchedule == 1)
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                    else
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                }
                                TotalContribution = Convert.ToDecimal(TotalContribution + item.NetDeposit);

                                TotalAdminFee = Convert.ToDecimal(TotalAdminFee + item.AdministrationInitialFee + item.AdministrationMonthlyFee);
                                TotalRST = Convert.ToDecimal(TotalRST + item.RetailSalesTaxInitial + item.RetailSalesTaxMonthly);
                                TotalPPT = Convert.ToDecimal(TotalPPT + item.PremiumTaxInitial + item.PremiumTaxMonthly);
                                TotalHST = Convert.ToDecimal(TotalHST + item.HSTInitial + item.HSTMonthly);


                            }

                            TotaltaxFees = TotaltaxFees + (TotalAdminFee + TotalRST + TotalPPT + TotalHST);
                        }
                        lstrDetailHtml = builder.ToString();


                         #region MyRegion
                         string lstrAdjHtml = "";
                         StringBuilder builder1 = new StringBuilder();
                         decimal TotalAdjustment = 0;
                         List<TblCompAdjustment> AdjustmentObj = (from p in mastercontext.TblCompAdjustments where p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                      && (p.DateCreated.Value.Month == iObj.Month && p.DateCreated.Value.Year == iObj.Year) && p.CompanyId == CompanyId
                                                                  select p).ToList();
                         if (AdjustmentObj != null && AdjustmentObj.Count() > 0)
                         {
                             builder1.Append("<tr style=\"background-color: #eee;\">");
                             builder1.Append("<td colspan=\"4\" height=\"40\" align=\"left\"><h2>Adjustments</h2></td></tr>");


                            builder1.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                            builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                            builder1.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                            builder1.Append("<td width=\"30%\" align=\"right\"><b>Type</b></td>");
                            builder1.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td>");
                            builder1.Append("</tr>");
        
                             foreach (TblCompAdjustment item in AdjustmentObj)
                             {
                                 builder1.Append("<tr style=\"background-color:#fff;\">");
                                 builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.DateCreated).ToString("MMM dd, yyyy") + "</b></td>");
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>" + item.Remarks + "</b></td>");
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>" + (item.TypeId == 2 ? "Debit" : "Credit") + "</b></td>");
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>$" + item.Amount + "</b></td>");
                                 builder1.Append("</tr>");
                             }
                             decimal TotalCredit = 0;
                             decimal TotalDebit = 0;
                             TotalCredit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                             TotalDebit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Debit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                             TotalAdjustment= TotalCredit-TotalDebit;
                             builder1.Append("<tr style=\"background-color:#eee;\">");
                             builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\" ></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\">Total</td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>$" + TotalAdjustment + "</b></td>");
                             builder1.Append("</tr>");
                         }
                         lstrAdjHtml = builder1.ToString();
                         contents = contents.Replace("lstrAdjustmentContent", lstrAdjHtml);
                        #endregion
                        contents = contents.Replace("lstrBodyContent", lstrDetailHtml);
                        contents = contents.Replace("lstrGrandTotal", String.Format("{0:0.00}", TotalContribution));

                        decimal pctAdminFee = 0;
                        decimal pctHST = 0;
                        decimal pctPPT = 0;
                        decimal pctRST = 0;


                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (CompanyObj != null)
                        {
                            TblProvince ProvinceObj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == CompanyObj.Province select p).Take(1).SingleOrDefault();
                            if (ProvinceObj != null)
                            {
                                pctAdminFee = Convert.ToDecimal(ProvinceObj.AdminFeePercent);
                                pctRST = Convert.ToDecimal(ProvinceObj.RST);
                                pctPPT = Convert.ToDecimal(ProvinceObj.PPT);
                                pctHST = Convert.ToDecimal(ProvinceObj.HST) == 0 ? Convert.ToDecimal(ProvinceObj.GST) : Convert.ToDecimal(ProvinceObj.HST);
                            }

                            TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                            if(AdminFeeObj != null)
                            {
                                decimal BrokerCommission = GetBrokerComissionCharges(CompanyId);
                                if(BrokerCommission > 0)
                                {
                                    pctAdminFee = Convert.ToDecimal(BrokerCommission + AdminFeeObj.AdminMin);
                                }
                                else
                                {
                                    pctAdminFee = Convert.ToDecimal(AdminFeeObj.AdminMax);
                                }
                            }


                            contents = contents.Replace("lstrInvoiceNo", CompanyObj.UniqueCode + "_" + DateTime.UtcNow.Month + DateTime.UtcNow.Year);
                            contents = contents.Replace("lstrCompanyName", CompanyObj.CompanyName);
                            contents = contents.Replace("lstrAddress", CompanyObj.Address1 + ((CompanyObj.Address2 == null || CompanyObj.Address2 == "") ? "" : " ," + CompanyObj.Address2) + ((CompanyObj.City == null || CompanyObj.City == "") ? "" : " ," + CompanyObj.City));
                            contents = contents.Replace("lstrEmail", CompanyObj.OwnerEmail);
                            contents = contents.Replace("lstrYear", iObj.Year.ToString());
                            contents = contents.Replace("LstrMonth", strMonthName);

                        }
                        contents = contents.Replace("lstrAdminFee", (String.Format("{0:0.00}", pctAdminFee)));
                        contents = contents.Replace("lstrHST", (String.Format("{0:0.00}", pctHST)));
                        contents = contents.Replace("lstrPPT", (String.Format("{0:0.00}", pctPPT)));
                        contents = contents.Replace("lstrRST", (String.Format("{0:0.00}", pctRST)));
                        contents = contents.Replace("TotalAdminFee", "$" + (String.Format("{0:0.00}", TotalAdminFee)));
                        contents = contents.Replace("TotalHST", "$" + (String.Format("{0:0.00}", TotalHST)));
                        contents = contents.Replace("TotalPPT", "$" + (String.Format("{0:0.00}", TotalPPT)));
                        contents = contents.Replace("TotalRST", "$" + (String.Format("{0:0.00}", TotalRST)));
                        contents = contents.Replace("TotalFeeTax", "$" + (String.Format("{0:0.00}", TotaltaxFees)));
                        contents = contents.Replace("lstrNetTotal", "$" + (String.Format("{0:0.00}", TotalContribution + TotaltaxFees)));
                        
                        TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select p).Take(1).SingleOrDefault();
                        if (CreditObj != null)
                        {
                            decimal AvaiableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select Convert.ToDecimal(p.Credits)).Sum();

                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", AvaiableCredits));

                        }
                        else
                        {
                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", 0));

                        }
                       
                        string FileName = "Estatement_" + DateTime.UtcNow.Month + DateTime.UtcNow.Year;
                        if (File.Exists(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf"))
                        {
                            System.IO.File.Delete(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf");
                        }
                        createPDF(FileName, contents, CompanyObj.CompanyId);
                        

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.FileName = FileName + ".pdf";
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "GetEStatement", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion GetEStatement

        //Jasmeet Kaur
        private void createPDF(string FileName, string HtmlContents, decimal CompanyId)
        {
            byte[] pdf; // result will be here
            string SamplePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            var cssText = File.ReadAllText(HostingEnvironment.MapPath("~/style.css"));
            var html = HtmlContents.ToString();

            using (var memoryStream = new MemoryStream())
            {
                var document = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 10f);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(cssText)))
                {
                    using (var htmlMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                    {
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, htmlMemoryStream, cssMemoryStream);
                    }
                }

                document.Close();
                pdf = memoryStream.ToArray();
                File.WriteAllBytes(SamplePath + CompanyId + "\\" + FileName + ".pdf", pdf);
            }
        }

        #region ListOfNotificationLog
        //JK:250618
        public CNotificationLogResponse ListOfNotificationLog(LazyLoadingSearchInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CNotificationLogResponse lobjResponse = new CNotificationLogResponse();
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        List<NotificationLog> obj = (from p in datacontext.sp_ListOfNotifyLog(iObj.ChunkStart, iObj.SearchString, Convert.ToInt16(iObj.ChunkSize), Convert.ToInt16(iObj.Id))
                                                     select new NotificationLog()
                                                     {
                                                         NotifyDate = Convert.ToString(p.DateCreated),
                                                         NotifyLogId = p.NotifyLogId,
                                                         NotifyText = p.NotifyText,
                                                         NotifyType = p.NotifyType
                                                     }).ToList();
                        lobjResponse.NotificationLog = obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, UserId, "ListOfNotificationLog", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddPlan
        public CResponse AddPlan(AddPlanInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            string ConString = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            try
            {

                using (EflexDBDataContext datacontext = new EflexDBDataContext(ConString))
                {
                    if (IsValidateUser(datacontext))
                    {
                       
                        TblCompanyPlan Obj = new TblCompanyPlan()
                        {
                            PlanTypeId = iObj.PlanTypeId,
                            PlanStartDt = DateTime.ParseExact(iObj.PlanStartDt, "MM/dd/yyyy", null),
                            Name = iObj.Name,
                            ContributionSchedule = iObj.ContributionSchedule,
                            RecurringDeposit = iObj.RecurringDeposit,
                            InitialDeposit = iObj.InitialDeposit,
                            NetDeposit = iObj.NetDeposit,
                            AdministrationInitialFee = iObj.AdministrationInitialFee,
                            AdministrationMonthlyFee = iObj.AdministrationMonthlyFee,
                            HSTInitial = iObj.HSTInitial,
                            HSTMonthly = iObj.HSTMonthly,
                            PremiumTaxInitial = iObj.PremiumTaxInitial,
                            PremiumTaxMonthly = iObj.PremiumTaxMonthly,
                            RetailSalesTaxInitial = iObj.RetailSalesTaxInitial,
                            RetailSalesTaxMonthly = iObj.RetailSalesTaxMonthly,
                            GrossTotalInitial = iObj.GrossTotalInitial,
                            GrossTotalMonthly = iObj.GrossTotalMonthly,
                            PlanDesign = iObj.PlanDesign,
                            RollOver = iObj.RollOver,
                            CarryForwardPeriod = iObj.CarryForwardPeriod,
                            Termination = iObj.Termination,
                            Active = true,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow,
                           
                        };
                        datacontext.TblCompanyPlans.InsertOnSubmit(Obj);
                        datacontext.SubmitChanges();



                        string Colorname = GetRandomColor(iObj.CompanyId, Obj.CompanyPlanId);

                        Obj.Color = Colorname;
                        datacontext.SubmitChanges();
                           

                       using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                        {
                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                            if (CompanyObj != null)
                            {
                                string Text = "Plan named as  " + iObj.Name + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);


                                string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) select p.UserId).ToList());
                                EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                            }
                            mastercontext.Connection.Close();
                        }   


                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                    lobjResponse.IsLogOut = IsLogOut;
                    CreateResponseLog(datacontext, StartTime, lobjResponse.ID, "AddPlan", lobjResponse.Message, lobjResponse.Message);
                    datacontext.Connection.Close();
                }
                   
            }
            catch (Exception ex)
            {
                lobjResponse.Message = ex.Message;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
            }
            return lobjResponse;
        }
        #endregion

        #region GetNotiMsgCount
        public CNotiMsgCountResponse GetNotiMsgCount(NotiMsgCountInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            decimal EventId = -1;
           
            string ActionTaken = "";
            string ServiceName = "GetNotiMsgCount";
            CNotiMsgCountResponse lobjResponse = new CNotiMsgCountResponse();
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.UserId;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    
                    if (IsValidateUser(datacontext))
                    {
                        decimal MasterCardId = -1;
                        TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == UserId select p).Take(1).SingleOrDefault();
                        if (EmpObj != null)
                        {
                            
                           List<TblMasterCard> MasterCardList = (from p in mastercontext.TblMasterCards where p.Active == true && p.CompanyId == CompanyId && p.UserId == EmpObj.CompanyUserId select p).ToList();
                            if (MasterCardList != null && MasterCardList.Count() > 0)
                            {
                                TblMasterCard  CardObj = MasterCardList.FirstOrDefault(x => x.CardStatus.ToUpper() == "PENDING FOR ACTIVATION");
                                if (CardObj != null)
                                {
                                    MasterCardId = CardObj.MasterCardId; 
                                    lobjResponse.IsCardActive = false;
                                    lobjResponse.MasterCardId = MasterCardId;
                                    
                                }
                                else
                                {
                                    TblMasterCard CardObj1 = MasterCardList.FirstOrDefault(x => x.IsActive == true);

                                    if (CardObj1 != null)
                                    {
                                        lobjResponse.MasterCardId = CardObj1.MasterCardId;
                                        lobjResponse.IsCardActive = true;
                                    }
                                    else
                                    {
                                        lobjResponse.IsCardActive = false;
                                    }
                                }
                            }
                            lobjResponse.AccessCode = Convert.ToDecimal(EmpObj.SIN);
                        }


                        decimal MessageCount = -1;
                        decimal UnreadNotificationCount = -1;

                        using (EflexMsgDBDataContext msgContext = new EflexMsgDBDataContext())
                        {
                            var Obj = (from p in msgContext.sp_GetUnreadCount(iObj.UserId, Convert.ToInt32(iObj.UserTypeId), iObj.CompanyId) select p).Take(1).SingleOrDefault();
                            MessageCount = Convert.ToDecimal(Obj.UnreadCount);
                        }

                        UnreadNotificationCount = datacontext.TblNotifyLogs.Where(x => x.Active == true && x.ForUserId == iObj.UserId && x.IsRead == false).Count();
                        lobjResponse.MessageCount = MessageCount;
                        lobjResponse.UnreadNotificationCount = UnreadNotificationCount;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }

                TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments
                                                 where p.CompanyId == CompanyId
                                                     && p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString()
                                                     && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                 select p).Take(1).SingleOrDefault();
                if(CompPaymentObj != null)
                {
                    lobjResponse.IsFreeze = false;
                }
                else
                {
                    lobjResponse.IsFreeze = true;
                }

                lobjResponse.IsLogOut = IsLogOut;
             
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region UpdateCompanyStatus
       public CResponse UpdateCompanyStatus(UpdateStatusIdInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (CompanyObj != null)
                        {
                            CompanyObj.IsBlocked = iObj.IsStatus;
                            CompanyObj.DateModified = DateTime.UtcNow;

                            if(!string.IsNullOrEmpty(iObj.Reason))
                            {
                                TblCompBlockReason ReasonObj = new TblCompBlockReason()
                                {
                                    CompanyId = CompanyObj.CompanyId,
                                    Reason = iObj.Reason,
                                    IsBlock = iObj.IsStatus,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow
                                };
                                mastercontext.TblCompBlockReasons.InsertOnSubmit(ReasonObj);
                                mastercontext.SubmitChanges();
                            }
                            mastercontext.SubmitChanges();

                            string text = "";
                            if(iObj.IsStatus)
                            {
                                text = "blocked";



                            }
                            else
                            {
                                text = "unblocked";
                            }
                            string Text = "Company named as " + CompanyObj.CompanyName + " has been marked as "+text+" on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow) + ((iObj.Reason != null && iObj.Reason != "") ? " due to " + iObj.Reason  :"" );
                            CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);

                            string ForUserIds = String.Join(",", (from p in mastercontext.TblUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) && p.UserId != iObj.LoggedInUserId select p.UserId).ToList());
                            EflexNotifyLog(mastercontext, DateTime.UtcNow, Text, ForUserIds);

                            if(iObj.IsStatus)
                            {
                                string Constring = GetConnection(CompanyObj.DBCon);
                                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                                {

                                    List<TblCompanyUser> UserList = (from p in datacontext.TblCompanyUsers where p.Active == true && (p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) || p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin)) select p).ToList();
                                    if(UserList != null)
                                    {
                                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                        foreach (var item in UserList)
	                                    {
		                                      
                                               #region SendMail
                                                StringBuilder builder = new StringBuilder();

                                                builder.Append("Hello " +item.FirstName+",<br/><br/>");
                                                builder.Append("Company has been blocked by admin due to " + iObj.Reason + ". Please contact eFlex for further details.<br/><br/>");
                                                builder.Append("Thanks,<br/><br/>");
                                                builder.Append("The eclipse EIO Team<br/><br/>");

                                              
                                                //send mail
                                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                              
                                                CMail Mail = new CMail();
                                                Mail.EmailTo = item.EmailAddress;
                                                Mail.Subject = "Company Blocked";
                                                Mail.MessageBody = lstrMessage;
                                                Mail.GodaddyUserName = GodaddyUserName;
                                                Mail.GodaddyPassword = GodaddyPwd;
                                                Mail.Server = Server;
                                                Mail.Port = Port;
                                                Mail.CompanyId = CompanyObj.CompanyId;
                                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                                #endregion SendMail
	                                    }
                                    }
                                    datacontext.Connection.Close();
                                }
                                  
                            }



                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "Company status updated successfully.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                  
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "UpdateCompanyStatus", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListCompAllTransactions
       public CListCompAllTransactionsResponse ListCompAllTransactions(ListCompAllTransactionsInput iObj)
       {
           string InputJson = "";
           DateTime CurrentTime = DateTime.UtcNow;
           UserId = iObj.LoggedInUserId;
           bool IsLogOut = false;
           CListCompAllTransactionsResponse lobjResponse = new CListCompAllTransactionsResponse();
           using (MasterDBDataContext datacontext = new MasterDBDataContext())
           {
               try
               {

                   if (IsValidateEflexUser(datacontext))
                   {
                       DateTime? FromDT = null;
                       DateTime? ToDT = null;
                       if (iObj.FromDt != null || iObj.FromDt != "")
                       {
                           FromDT = Convert.ToDateTime(iObj.FromDt);
                       }
                       if (iObj.ToDt != null || iObj.ToDt != "")
                       {
                           ToDT = Convert.ToDateTime(iObj.ToDt);
                       }

                       List<CompAllTransactionsData> Obj = (from p in datacontext.sp_ListCompAllTransactions(iObj.ChunckStart, iObj.ChunckSize, iObj.SearchString, FromDT, ToDT, iObj.CompanyId)
                                                       select new CompAllTransactionsData()
                                                       {
                                                           RowId = Convert.ToDecimal(p.RowId),
                                                           CompAdjustmentId = Convert.ToDecimal(p.CompPaymentId),
                                                           CompanyPaymentId = Convert.ToDecimal(p.CompPaymentId),
                                                           CompanyName = p.CompanyName,
                                                           PaymentDate = Convert.ToString(p.PaymentDate),
                                                           Amount = Convert.ToDecimal(p.Amount),
                                                           PaymentDetails = p.PaymentDetails,
                                                           PaymentStatus = Convert.ToDecimal(p.PaymentStatus),
                                                           PaymentType = p.PaymentTypeId,
                                                           Type = p.TypeId == 1 ? "Credit" : "Debit"
                                                       }).ToList();

                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                       lobjResponse.CompAllTransactionsData = Obj;
                   }
                   else
                   {
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                       lobjResponse.Message =  CObjects.SessionExpired.ToString();
                       IsLogOut = true;
                      
                   }
               }
               catch (Exception ex)
               {

                   InputJson = new JavaScriptSerializer().Serialize(iObj);
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   lobjResponse.Message = ex.ToString();
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateMasterResponseLog(datacontext, CurrentTime, -1, "ListCompAllTransactions", InputJson, lobjResponse.Message);
               datacontext.Connection.Close();
           }
           return lobjResponse;
       }
       #endregion

        #region DeleteCompany
       public CResponse DeleteCompany(IdInputv2 iObj)
       {
           DateTime StartTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           CResponse lobjResponse = new CResponse();
           UserId = iObj.LoggedInUserId;
           bool IsLogOut = false;
           using (MasterDBDataContext mastercontext = new MasterDBDataContext())
           {
               if (IsValidateEflexUser(mastercontext))
               {
                   try
                   {
                       TblCompany CompObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                       if (CompObj != null)
                       {
                           CompObj.Active = false;
                           CompObj.DateModified = DateTime.UtcNow;
                           mastercontext.SubmitChanges();

                           lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                       }
                   }
                   catch (Exception ex)
                   {

                       lobjResponse.Message = ex.Message;
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   }
               }
               else
               {
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   lobjResponse.Message =  CObjects.SessionExpired.ToString();
                   IsLogOut = true;
                  
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateMasterResponseLog(mastercontext, StartTime, iObj.LoggedInUserId, "DeleteCompany", InputJson, lobjResponse.Message);
               mastercontext.Connection.Close();
           }
           return lobjResponse;
       }
       #endregion

        #region GetDashboardTransactions
       public CGetDashboardTransactionsResponse GetDashboardTransactions(DbConIdInput iObj)
       {
           CGetDashboardTransactionsResponse lobjResponse = new CGetDashboardTransactionsResponse();
           DateTime StartTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           string Constring = GetConnection(iObj.DbCon);
           UserId = iObj.Id;
           bool IsLogOut = false;
           decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
           using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
           {
               try
               {
                   MasterDBDataContext mastercontext = new MasterDBDataContext();
                   if (IsValidateUser(datacontext))
                   {
                       #region Company Transaction Data
                       List<PendingTransaction> PendingObj = (from p in mastercontext.TblCompPayments
                                                              where p.Active == true && p.CompanyId == CompanyId &&  p.PaymentTypeId != CObjects.enumPaymentType.WDR.ToString()
                                                              && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                              orderby p.DateCreated descending
                                                              select new PendingTransaction()
                                                              {
                                                                  Amount = Convert.ToDecimal(p.Amount),
                                                                  Date = Convert.ToString(p.PaymentDueDate),
                                                                  Details = p.PaymentDetails,
                                                                  PaymentType = p.PaymentTypeId,
                                                                  TypeId = p.TypeId == 1 ? "Credit" : "Debit"
                                                              }).Take(10).ToList();
                       List<CurrentTransaction> CurrentObj = (from p in mastercontext.TblCompPayments
                                                              where p.Active == true && p.CompanyId == CompanyId && p.PaymentTypeId != CObjects.enumPaymentType.WDR.ToString()
                                                              && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                              orderby p.DateCreated descending
                                                              select new CurrentTransaction()
                                                              {
                                                                  Amount = Convert.ToDecimal(p.Amount),
                                                                  Date = Convert.ToString(p.PaymentDueDate),
                                                                  Details = p.PaymentDetails,
                                                                  PaymentType = p.PaymentTypeId,
                                                                  TypeId = p.TypeId == 1 ? "Credit" : "Debit"
                                                              }).Take(10).ToList();
                       #endregion

                       #region Company Figures
                       List<TblCompPayment> DueAmt = (from p in mastercontext.TblCompPayments
                                                      where p.Active == true &&
                                                          p.CompanyId == CompanyId && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString()
                                                          && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                      select p).ToList();
                       decimal TotalDueAmt = 0;
                       if (DueAmt != null && DueAmt.Count() > 0)
                       {
                           TotalDueAmt = (from p in mastercontext.TblCompPayments
                                          where p.Active == true &&
                                              p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                          select Convert.ToDecimal(p.Amount)).Sum();
                           lobjResponse.AmountDue = TotalDueAmt < 0 ? 0 : TotalDueAmt;

                       }
                       decimal AvailableCredits = 0;

                       TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();

                       if (CreditObj != null)
                       {
                           AvailableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyId select Convert.ToDecimal(p.Credits)).Sum();
                       }

                       GetCompDashboardFigures FiguresObj = new GetCompDashboardFigures()
                       {
                           TotalAdmin = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Admin) select p).Count(),
                           TotalMembers = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee) select p).Count(),
                           TotalClaimed = (from p in datacontext.TblEmpClaims where p.Active == true select p).Count() > 0 ?
                                            (from p in datacontext.TblEmpClaims where p.Active == true select Convert.ToDecimal(p.ClaimAmt)).Sum() : 0,
                           AmountDue = TotalDueAmt,
                           AvailableCredits = AvailableCredits,
                       };
                       #endregion

                       #region ServiceClaimAmt
                       List<ServiceClaimAmt> ServiceClaimAmtObj = (from p in mastercontext.TblServices.AsEnumerable()
                                                                   where p.Active == true
                                                                   select new ServiceClaimAmt()
                                                                       {
                                                                           ServiceId = p.ServiceId,
                                                                           ServiceName = p.Title,
                                                                           Amount = (from q in datacontext.TblEmpClaims where q.Active == true
                                                                                     && q.ServiceId == p.ServiceId select q).Count() > 0 ?
                                                                                     (from q in datacontext.TblEmpClaims where q.Active == true
                                                                                     && q.ServiceId == p.ServiceId select Convert.ToDecimal(q.ClaimAmt ?? 0)).Sum() : 0,
                                                                       }).ToList();
                       lobjResponse.ServiceClaimAmt = ServiceClaimAmtObj.Where(x=>x.Amount !=0).Select(x=>x).ToList();
                       #endregion ServiceClaimAmt

                    
                       lobjResponse.GetCompDashboardFigures = FiguresObj;
                       lobjResponse.AvailableCredits = AvailableCredits;
                       lobjResponse.PendingTransaction = PendingObj;
                       lobjResponse.CurrentTransaction = CurrentObj;
                       lobjResponse.Message = "SUCCESS";
                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                   }
                   else
                   {
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                       lobjResponse.Message =  CObjects.SessionExpired.ToString();
                       IsLogOut = true;
                     
                   }
               }
               catch (Exception ex)
               {
                   lobjResponse.Message = ex.Message;
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateResponseLog(datacontext, StartTime, UserId, "GetDashboardTransactions", InputJson, lobjResponse.Message);
               datacontext.Connection.Close();
           }
           return lobjResponse;
       }
       #endregion

        #region GetRecentClaimDashboardTransactions
       public CGetRecentClaimDashboardResponse GetRecentClaimDashboard(GetDashboardTransactionsInput iObj)
       {
           CGetRecentClaimDashboardResponse lobjResponse = new CGetRecentClaimDashboardResponse();
           DateTime StartTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           string Constring = GetConnection(iObj.DbCon);
           UserId = iObj.Id;
           bool IsLogOut = false;
           decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
           using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
           {
               try
               {
                   MasterDBDataContext mastercontext = new MasterDBDataContext();
                   if (IsValidateUser(datacontext))
                   {
                       #region RecentClaimList
                       
                       List<RecentClaimList> RecentClaimListObj = (from p in datacontext.TblEmpClaims.AsEnumerable()
                                                                   join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                                   join c in mastercontext.TblServices on p.ServiceId equals c.ServiceId
                                                                   where p.Active == true && q.Active == true && c.Active == true && c.ServiceTypeId == iObj.ServiceTypeId
                                                                   group p by new {q.FirstName,q.LastName,q.CompanyUserId} into g
                                                                   select new RecentClaimList()
                                                                   {
                                                                       EmpName = g.Key.FirstName + " " + g.Key.LastName,
                                                                       ClaimAmt = g.Sum(x => Convert.ToDecimal(x.ClaimAmt == null ? 0 : x.ClaimAmt)),
                                                                       PctPremium = GetPctPremium(datacontext, g.Sum(x => Convert.ToDecimal(x.ClaimAmt == null ? 0 : x.ClaimAmt)), g.Key.CompanyUserId)
                                                                   }).ToList();
                       lobjResponse.RecentClaimList = RecentClaimListObj;
                       #endregion
                 

                       lobjResponse.Message = "SUCCESS";
                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                   }
                   else
                   {
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                       lobjResponse.Message =  CObjects.SessionExpired.ToString();
                       IsLogOut = true;
                       
                   }
               }
               catch (Exception ex)
               {
                   lobjResponse.Message = ex.Message;
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateResponseLog(datacontext, StartTime, UserId, "GetRecentClaimDashboard", InputJson, lobjResponse.Message);
               datacontext.Connection.Close();
           }
           return lobjResponse;
       }

     
       #endregion

        #region GetEmpPlanDashboardDetails
       public CGetEmpPlanDashboardDetailsResponse GetEmpPlanDashboardDetails(GetEmpPlanDashboardDetailsInput iObj)
       {
           CGetEmpPlanDashboardDetailsResponse lobjResponse = new CGetEmpPlanDashboardDetailsResponse();
           DateTime StartTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           string Constring = GetConnection(iObj.DbCon);
           UserId = iObj.Id;
           bool IsLogOut = false;
           decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
           using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
           {
               try
               {
                   MasterDBDataContext mastercontext = new MasterDBDataContext();
                   if (IsValidateUser(datacontext))
                   {
                       #region EmployeesDetails
                       List<EmployeesDetails> EmployeesDetailsObj = (from p in datacontext.TblEmployees
                                                                     join q in datacontext.TblCompanyPlans on p.CompanyPlanId equals q.CompanyPlanId
                                                                     where p.Active == true && q.Active == true
                                                                     select new EmployeesDetails()
                                                                         {
                                                                             EmpName = p.FirstName + " " + p.LastName,
                                                                             PlanName = q.Name,
                                                                             TotalClaimed = (from a in datacontext.TblEmpClaims
                                                                                           where a.Active == true && a.CompanyUserId == p.CompanyUserId
                                                                                           select a).Count() > 0 ? (from a in datacontext.TblEmpClaims
                                                                                           where a.Active == true && a.CompanyUserId == p.CompanyUserId
                                                                                           select Convert.ToDecimal(a.ClaimAmt ?? 0)).Sum() : 0,
                                                                         }).ToList();
                       lobjResponse.EmployeesDetails = EmployeesDetailsObj;

                         #endregion EmployeesDetails

                       #region Plan Details
                       List<PlanDashboardDetails> PlanObj = (from p in datacontext.TblCompanyPlans
                                                              where p.Active == true 
                                                              select new PlanDashboardDetails()
                                                                  {
                                                                      PlanName = p.Name,
                                                                      NoOfEmployees = (from q in datacontext.TblEmployees 
                                                                                       where q.Active == true && q.CompanyPlanId == p.CompanyPlanId select p).Count()
                                                                  }).ToList();
                       lobjResponse.PlanDashboardDetails = PlanObj;
                       #endregion

                       #region ClaimOverview
                       ClaimOverview ClaimOverviewObj = new ClaimOverview();

                        if (iObj.FilterType.ToUpper() == "MONTH")
                        {
                            List<string> AllMonths = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames.ToList();
                            List<decimal> ClaimAmtReceived = new List<decimal>();
                            List<decimal> TotalClaimCount = new List<decimal>();
                            List<string> Date = new List<string>();
                            List<string> Months = new List<string>();
                            Int32 TotalMonts = 12;
                            Int32 CurrentMonth = DateTime.UtcNow.Month;
                            List<EmpDashboardDataForAdmin> Result = new List<EmpDashboardDataForAdmin>();
                            for (int i = 0; i < CurrentMonth; i++)
                            {
                                int month = i + 1;
                                DateTime StartDate = new DateTime(DateTime.Now.Year, month, 1);
                                DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);

                                List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims
                                                                where p.Active == true &&
                                                                p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                                select p).ToList();
                                decimal ClaimCount = 0;
                                decimal ClaimAmountReceived = 0;
                                if (ClaimObj != null && ClaimObj.Count() > 0)
                                {
                                    ClaimCount = ClaimObj.Count();
                                    ClaimAmountReceived = ClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                    TotalClaimCount.Add(ClaimCount);
                                    ClaimAmtReceived.Add(ClaimAmountReceived);
                                }
                                else
                                {
                                    TotalClaimCount.Add(ClaimCount);
                                    ClaimAmtReceived.Add(ClaimAmountReceived);
                                }
                                string MonthName = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(month);
                                Months.Add(MonthName);
                            }
                            ClaimOverviewObj.NoOfClaims = TotalClaimCount;
                            ClaimOverviewObj.Months = Months;
                            ClaimOverviewObj.TotalClaimAmt = ClaimAmtReceived;
                            
                        }
                        if (iObj.FilterType.ToUpper() == "YEAR")
                        {
                            DateTime FirstActionDate = System.DateTime.Now;
                            List<string> YearData = new List<string>();
                            int Currentyear = FirstActionDate.Year;
                            //year = year - 4;
                            List<decimal> ClaimAmtReceived = new List<decimal>();
                            List<decimal> TotalClaimCount = new List<decimal>();
                            List<string> Date = new List<string>();
                            for (int i = 3; i >= 0; i--)
                            {
                                int year = Currentyear - i;
                                DateTime StartDate = new DateTime(year, 1, 1);
                                DateTime EndDate = new DateTime(year, 12, 31);
                                List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims
                                                              where p.Active == true &&
                                                              p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                              select p).ToList();
                                decimal ClaimCount = 0;
                                decimal ClaimAmountReceived = 0;
                                if (ClaimObj != null && ClaimObj.Count() > 0)
                                {
                                    ClaimCount = ClaimObj.Count();
                                    ClaimAmountReceived = ClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                    TotalClaimCount.Add(ClaimCount);
                                    ClaimAmtReceived.Add(ClaimAmountReceived);
                                }
                                else
                                {
                                    TotalClaimCount.Add(ClaimCount);
                                    ClaimAmtReceived.Add(ClaimAmountReceived);
                                }
                                YearData.Add(Convert.ToString(year));
                                //  year = year - i;
                            }
                            ClaimOverviewObj.NoOfClaims = TotalClaimCount;
                            ClaimOverviewObj.Months = YearData;
                            ClaimOverviewObj.TotalClaimAmt = ClaimAmtReceived;
                        }
                       #endregion

                       lobjResponse.ClaimOverview = ClaimOverviewObj;
                       lobjResponse.Message = "SUCCESS";
                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                   }
                   else
                   {
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                       lobjResponse.Message =  CObjects.SessionExpired.ToString();
                       IsLogOut = true;
                     
                   }
               }
               catch (Exception ex)
               {
                   lobjResponse.Message = ex.Message;
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateResponseLog(datacontext, StartTime, UserId, "GetEmpPlanDashboardDetails", InputJson, lobjResponse.Message);
               datacontext.Connection.Close();

           }
           return lobjResponse;
       }
       #endregion

        #region EmployeeClaimListForCompany
       public CEmployeeClaimListResponse EmployeeClaimListForCompany(EmployeeClaimListInput iObj)
       {
           DateTime CurrentTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           CEmployeeClaimListResponse lobjResponse = new CEmployeeClaimListResponse();
           UserId = iObj.LoggedInUserId;
           bool IsLogOut = false;
           string DbCon = DBFlag + iObj.CompanyId;
           string Constring = GetConnection(DbCon);
           MasterDBDataContext mastercontext = new MasterDBDataContext();
           using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
           {
               if (IsValidateUser(datacontext))
               {
                   try
                   {
                       decimal? StatusId = null;
                       if (iObj.StatusId != -1)
                       {
                           StatusId = iObj.StatusId;
                       }
                       List<EmployeeClaimList> Obj = (from p in datacontext.sp_EmployeeClaimList(iObj.ChunckStart, iObj.SearchString, Convert.ToInt32(iObj.ChunckSize), StatusId, iObj.CompanyUserId)
                                                      select new EmployeeClaimList()
                                                      {
                                                          EmpClaimId = p.EmpClaimId,
                                                          Employee = p.Employee,
                                                          Dependent = p.Dependant,
                                                          ClaimDt = Convert.ToString(p.ClaimDT),
                                                          ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                          Status = p.ClaimStatus,
                                                          Receipt = p.ReceiptNo,
                                                          ReceiptDT = Convert.ToString(p.ReceiptDate),
                                                          FileName = p.UploadFileName,
                                                          CRA = Convert.ToDecimal(p.CRA),
                                                          NonCRA = Convert.ToDecimal(p.NonCRA),
                                                          IsManual = Convert.ToBoolean(p.IsManual),
                                                          Note = p.Note,
                                                          ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                                 join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                                 where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                                 select new ClaimServiceDetails()
                                                                                 {
                                                                                     ServiceName = a.Title,
                                                                                     ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                     Address = q.Address,
                                                                                     PhoneNo = q.ContactNo,
                                                                                     Email = q.Email,
                                                                                     ServiceProvider = q.Name
                                                                                 }).Take(1).SingleOrDefault()
                                                      }).ToList();
                       lobjResponse.EmployeeClaimList = Obj;
                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                   }
                   catch (Exception ex)
                   {

                       lobjResponse.Message = ex.Message;
                       lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   }
               }
               else
               {
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   lobjResponse.Message =  CObjects.SessionExpired.ToString();
                   IsLogOut = true;
               }
               lobjResponse.IsLogOut = IsLogOut;
               CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "EmployeeClaimList", InputJson, lobjResponse.Message);
               datacontext.Connection.Close();
               mastercontext.Connection.Close();
           }
           return lobjResponse;
       }
       #endregion

        #region ResentOwnerCredentials
        public CResponse ResentOwnerCredentials(ResentOwnerCredentialsInput iObj)
       {
           CResponse lobjResponse = new CResponse();
           DateTime StartTime = DateTime.UtcNow;
           string InputJson = new JavaScriptSerializer().Serialize(iObj);
           string DbCon = DBFlag + iObj.Id;
           string Constring = GetConnection(DbCon);
           MasterDBDataContext mastercontext = new MasterDBDataContext();
           using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
           {
               try
               {
                   TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                   if(CompanyObj != null)
                   {
                       CompanyObj.OwnerEmail = iObj.EmailId;
                       mastercontext.SubmitChanges();

                       TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                       if(UserObj != null)
                       {
                           UserObj.EmailAddress = iObj.EmailId;
                           datacontext.SubmitChanges();


                           #region SendMail

                           StringBuilder builder = new StringBuilder();
                           string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                           string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                           string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                           Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                           CMail Mail = new CMail();
                           builder.Append("Hello  " + UserObj.FirstName + ",<br/><br/>");
                           builder.Append("Your company, " + CompanyObj.CompanyName + ", has been successfully registered with eFlex!<br/>");
                           builder.Append("To access your company profile, please login <a href=" + lstrUrl + " target='_blank'>" + lstrUrl + "</a>.<br/><br/>");
                           builder.Append("Your credentials are:<br/>");
                           builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/>");
                           builder.Append("Username: " + UserObj.UserName.Split('_')[1] + "<br/>");
                           builder.Append("Password: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/><br/>");
                           builder.Append("If you have any questions or concerns about your eFlex account, please reach our to our team anytime at 416-748-9879 or eFlex@eclipseEIO.com <br/><br/>");
                           builder.Append("Thanks, and enjoy your eFlex account(s)!<br/>");
                           builder.Append("The eclipse EIO Team");
                           string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                           lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                           
                           
                           Mail.EmailTo = iObj.EmailId.Trim();
                           Mail.Subject = "Welcome to eFlex! Your Account Is Ready";
                           Mail.MessageBody = lstrMessage;
                           Mail.GodaddyUserName = GodaddyUserName;
                           Mail.GodaddyPassword = GodaddyPwd;
                           Mail.Server = Server;
                           Mail.Port = Port;
                           Mail.CompanyId = iObj.Id;
                           bool IsSent = Mail.SendEMail(datacontext, true);


                           #endregion SendMail
                       }
                   }
                   
                       lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                  
               }
               catch (Exception ex)
               {
                   lobjResponse.Message = ex.Message;
                   lobjResponse.Status = CObjects.enmStatus.Error.ToString();
               }
               datacontext.Connection.Close();
                mastercontext.Connection.Close();
           }
           return lobjResponse;
       }
        #endregion UpdatePAPDetails

        #region Import Excel
        #region ImportEmployeesExcel
        public CResponse ImportEmployeesExcelOnRegister(ImportUsersExcelInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            using (MasterDBDataContext dbContext = new MasterDBDataContext())
            {
                UniqueCode = (from p in dbContext.TblCompanies where p.CompanyId == iObj.CompanyId && p.Active == true select p.UniqueCode).Take(1).SingleOrDefault();
                dbContext.Connection.Close();
            }
            try
            {
                if (iObj.FileName != null)
                {
                    string FileName = Path.GetFileName(iObj.FileName);
                    string FileName1 = Path.GetFileNameWithoutExtension(iObj.FileName);
                    string Extension = Path.GetExtension(iObj.FileName);
                    string FolderPath = ConfigurationManager.AppSettings["ConPath"].ToString();
                    FolderPath = FolderPath + iObj.CompanyId + "\\";

                    DataTable datatab = new DataTable();
                    string FilePath = FolderPath + FileName;
                    if (File.Exists(FilePath))
                    {
                        datatab = ImportEmployeeData(FilePath, Extension, iObj.DbCon);
                        string Message = AddXLSEmployeeDetails(datatab, iObj.DbCon, UniqueCode, iObj.CompanyId);
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "FAIL";
                    }




                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    lobjResponse.Message = "FAIL";
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return lobjResponse;
        }


        #endregion ImportEmployeeData

        #region ImportEmpData
        private DataTable ImportEmployeeData(string FilePath, string Extension, string DbCon)
        {
            try
            {
                string conStr = "";
                string isHDR = "Yes";
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conStr = String.Format(conStr, FilePath, isHDR);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();

                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw new Exception("An error ocurred while executing ImportVehicles(). " + ex.Message);
            }
        }


        #endregion ImportEmpData

        #region AddXLSEmpDetails
        /// <summary>
        /// Author:Jasmeet kaur
        /// Date:180618
        /// Function used to insert the data from excel in database.
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="DbCon"></param>
        private string AddXLSEmployeeDetails(DataTable datatab, string DbCon, string UniqueCode, decimal CompanyId)
        {
            string lstrFName = "";
            string lstrLName = "";
            string lstrEmail = "";
            string lstrPlan = "";
            string ErrorString = "";
            string Constring = GetConnection(DbCon);
            decimal AmountToBepaid = 0;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {

                   
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    for (int i = 0; i < datatab.Rows.Count; i++)
                    {
                        if (datatab.Rows[i]["FIRST NAME"].ToString() != "")
                        {
                            lstrFName = Convert.ToString(datatab.Rows[i]["FIRST NAME"]).ToString();

                        }
                        if (datatab.Rows[i]["LAST NAME"].ToString() != "")
                        {
                            lstrLName = Convert.ToString(datatab.Rows[i]["LAST NAME"]).ToString();

                        }
                        if (datatab.Rows[i]["EMAIL"].ToString() != "")
                        {
                            lstrEmail = Convert.ToString(datatab.Rows[i]["EMAIL"]).ToString();

                        }
                        if (datatab.Rows[i]["PLAN"].ToString() != "")
                        {
                            lstrPlan = Convert.ToString(datatab.Rows[i]["PLAN"]).ToString();

                        }
                        TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.Name.ToUpper() == lstrPlan.ToUpper() select p).Take(1).SingleOrDefault();
                        if (PlanObj != null)
                        {
                            string PaymentType = "";
                            DateTime? PaymentDate = null;
                            string PaymentDetails = "";
                            if (PlanObj.ContributionSchedule == 1)
                            {
                                PaymentType = CObjects.enumPaymentType.DM.ToString();
                            }
                            else
                            {
                                PaymentType = CObjects.enumPaymentType.DA.ToString();

                            }

                            string Password = RandomString();
                            string EncryptPwd = CryptorEngine.Encrypt(Password);
                            #region Add Employees
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.EmailAddress == lstrEmail && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee) select p).Take(1).SingleOrDefault();
                            if (UserObj == null)
                            {

                                decimal GetLatestId = 0;
                                GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                if (GetLatestId == 0)
                                {
                                    GetLatestId = 1;
                                }
                                else
                                {
                                    GetLatestId = GetLatestId + 1;
                                }
                                TblCompanyUser Obj = new TblCompanyUser()
                                {
                                    FirstName = lstrFName,
                                    LastName = lstrLName,
                                    UserName = (UniqueCode + "_" + lstrFName) + GetLatestId.ToString(),
                                    Pwd = EncryptPwd,
                                    UserTypeId = Convert.ToInt16(CObjects.enmUserType.Employee),
                                    EmailAddress = lstrEmail,
                                    IsActive = true,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                datacontext.SubmitChanges();
                                UpdateMsgUser(Obj, "ADD", CompanyId);

                                TblEmployee EmpObj = new TblEmployee()
                                {
                                    CompanyUserId = Obj.CompanyUserId,
                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                    FirstName = lstrFName,
                                    LastName = lstrLName,
                                    EmailAddress = lstrEmail,
                                    IsActive = true,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblEmployees.InsertOnSubmit(EmpObj);
                                datacontext.SubmitChanges();

                                decimal BalanceAmount = 0;
                                #region Apply account setUp fee
                                TblFeeDetail SetUpFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.Setupfee) select p).Take(1).SingleOrDefault();
                                if (SetUpFeeObj != null)
                                {
                                    TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                    {
                                        TransactionDate = DateTime.UtcNow,
                                        TransactionDetails = "Account Set-up Fee",
                                        Amount = SetUpFeeObj.Amount,
                                        CompanyUserId = EmpObj.CompanyUserId,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        ClaimId = -1,
                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                    {
                                        CompanyUserId = Obj.CompanyUserId,
                                        Amount = SetUpFeeObj.Amount,
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = "Account SetUp Fee",
                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                    datacontext.SubmitChanges();
                                    BalanceAmount = Convert.ToDecimal(-SetUpFeeObj.Amount);
                                }
                                #endregion

                                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                if (CompanyObj != null)
                                {
                                    CompanyObj.NoOfEmployee = CompanyObj.NoOfEmployee + 1;
                                    mastercontext.SubmitChanges();
                                }

                               

                                #region payment
                                 #region Set Payment date


                            TblCompPayment CompanyPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                            if (CompanyPaymentObj != null)
                            {
                                if (CompanyPaymentObj.PaymentDate >= DateTime.UtcNow)
                                {
                                    PaymentDate = CompanyPaymentObj.PaymentDate;
                                }
                                else
                                {
                                    DateTime now = DateTime.UtcNow;
                                    var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                    var SevenDT = new DateTime(now.Year, now.Month, 7);
                                    DateTime date = DateTime.Now;

                                    DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                    string dayToday = DayOnTwelve.ToString();

                                    // compare enums
                                    if (DayOnTwelve == DayOfWeek.Saturday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(2);
                                    }
                                    else if (DayOnTwelve == DayOfWeek.Sunday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(1);
                                    }
                                    else
                                    {
                                        PaymentDate = TweleveDT.Date;
                                    }


                                    if (now.Date > PaymentDate)
                                    {
                                        PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                    }

                                }
                            }
                            else
                            {
                                DateTime now = DateTime.UtcNow;
                                var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                var SevenDT = new DateTime(now.Year, now.Month, 7);
                                DateTime date = DateTime.Now;

                                DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                string dayToday = DayOnTwelve.ToString();

                                // compare enums
                                if (DayOnTwelve == DayOfWeek.Saturday)
                                {
                                    PaymentDate = TweleveDT.Date.AddDays(2);
                                }
                                else if (DayOnTwelve == DayOfWeek.Sunday)
                                {
                                    PaymentDate = TweleveDT.Date.AddDays(1);
                                }
                                else
                                {
                                    PaymentDate = TweleveDT.Date;
                                }


                                if (now.Date > PaymentDate)
                                {
                                    PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                }

                            }

                            #endregion

                                #region Apply account setUp fee
                                TblFeeDetail SetupFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.Title.ToUpper() == "SETUP FEE/INITIAL FEE" select p).Take(1).SingleOrDefault();
                                if (SetupFeeObj != null)
                                {
                                    TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                    {
                                        TransactionDate = DateTime.UtcNow,
                                        TransactionDetails = "Account Set-up Fee",
                                        Amount = SetupFeeObj.Amount,
                                        CompanyUserId = Obj.CompanyUserId,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        ClaimId = -1,
                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                    {
                                        CompanyUserId = Obj.CompanyUserId,
                                        Amount = SetupFeeObj.Amount,
                                        AdjustmentDT = DateTime.UtcNow,// PaymentDate,
                                        Remarks = "Account SetUp Fee",
                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);

                                    BalanceAmount = Convert.ToDecimal(-SetupFeeObj.Amount);
                                }
                                #endregion

                                PaymentDetails = "Initial Payment";
                                PaymentType = CObjects.enumPaymentType.IN.ToString();

                                TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                {
                                    TransactionDate = DateTime.UtcNow,
                                    TransactionDetails = PaymentDetails,
                                    Amount = PlanObj.NetDeposit,
                                    CompanyUserId = Obj.CompanyUserId,
                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                    ClaimId = -1,
                                    PaymentTypeId = PaymentType,
                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow
                                };

                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                
                                TblPayment PaymentObj1 = new TblPayment()
                                {
                                    PaymentDate = PaymentDate,
                                    PaymentDetails = PaymentDetails,
                                    RecurringDeposit = PlanObj.RecurringDeposit,
                                    InitialDeposit = PlanObj.InitialDeposit,
                                    NetDeposit = PlanObj.NetDeposit,
                                    AdministrationInitialFee = PlanObj.AdministrationInitialFee,
                                    AdministrationMonthlyFee = PlanObj.AdministrationMonthlyFee,
                                    HSTInitial = PlanObj.HSTInitial,
                                    HSTMonthly = PlanObj.HSTMonthly,
                                    PremiumTaxInitial = PlanObj.PremiumTaxInitial,
                                    PremiumTaxMonthly = PlanObj.PremiumTaxMonthly,
                                    RetailSalesTaxInitial = PlanObj.RetailSalesTaxInitial,
                                    RetailSalesTaxMonthly = PlanObj.RetailSalesTaxMonthly,
                                    GrossTotalInitial = PlanObj.GrossTotalInitial,
                                    GrossTotalMonthly = PlanObj.GrossTotalMonthly,
                                    Amount = PlanObj.GrossTotalInitial + PlanObj.GrossTotalMonthly + PlanObj.NetDeposit,
                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                    CompanyUserId = Obj.CompanyUserId,
                                    PaymentDueDate = PaymentDate,
                                    PaymentTypeId = PaymentType,
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    IsRemit = false
                                };


                                datacontext.TblPayments.InsertOnSubmit(PaymentObj1);
                                TblEmpBalance EmpPaymentObj = new TblEmpBalance()
                                {
                                    Amount = BalanceAmount,
                                    CompanyUserId = Obj.CompanyUserId,
                                    Active = true,
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                    DateModified = DateTime.UtcNow,
                                    DateCreated = DateTime.UtcNow
                                };

                                datacontext.TblEmpBalances.InsertOnSubmit(EmpPaymentObj);

                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                string NotifyText = "Your eFlex account has been set up by Admin. Please login and update your profile. Thanks!";
                                NotificationList = InsertionInNotificationTable(EmpObj.CompanyUserId, NotifyText, datacontext, EmpObj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());


                                datacontext.SubmitChanges();

                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, EmpObj.CompanyUserId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications


                                
                                datacontext.SubmitChanges();
                                AmountToBepaid = Convert.ToDecimal(PaymentObj1.Amount);

                                CompanyPaymentObj.Amount = CompanyPaymentObj.Amount + AmountToBepaid;
                                mastercontext.SubmitChanges();


                                #endregion

                                #region Broker Commission
                                TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                   where p.Active == true && p.CompanyId == CompanyObj.CompanyId
                                                                   select p).Take(1).SingleOrDefault();
                                if (CompReferral != null)
                                {
                                    
                                    decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                    where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                    select Convert.ToDecimal(p.NetDeposit)).Sum();

                                    if (CompReferral.BrokerId != -1)
                                    {
                                        TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                               where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                               select p).Take(1).SingleOrDefault();
                                        TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                      where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyObj.CompanyId
                                                                      select p).Take(1).SingleOrDefault();
                                        decimal PercentageBrokerCharge = 0;
                                        if (FeeObj != null)
                                        {
                                            if (FeeObj.BrokerFee > 0)
                                            {
                                                PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                            }
                                        }
                                        else
                                        {
                                            if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                            {
                                                PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                            }
                                            else
                                            {
                                                PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                            }
                                        }


                                        if (PercentageBrokerCharge > 0)
                                        {
                                            decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                            TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                               where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                               p.UserId == BrokerObj.UserId
                                                                               select p).Take(1).SingleOrDefault();
                                            if (TransactionObj != null)
                                            {
                                                TransactionObj.Amount = Amount;
                                                TransactionObj.Contribution = BrokerAmountToBepaid;
                                                TransactionObj.PercentFee = PercentageBrokerCharge;
                                            }
                                            else
                                            {
                                                TblBrokerPayment TransactionObj1 = new TblBrokerPayment()
                                                {
                                                    PaymentDate = DateTime.UtcNow,
                                                    PaymentDueDate = DateTime.UtcNow,
                                                    PaymentDetails = "Payments",
                                                    Amount = Amount,
                                                    CompanyId = CompanyObj.CompanyId,
                                                    Contribution = BrokerAmountToBepaid,
                                                    PercentFee = PercentageBrokerCharge,
                                                    UserId = BrokerObj.UserId,
                                                    PaymentTypeId = CObjects.enumPaymentType.DM.ToString(),
                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow
                                                };
                                                mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj1);
                                            }
                                            mastercontext.SubmitChanges();
                                        }
                                    }
                                }
                                #endregion Broker Commission

                            


                                #region UpdateCompanyPayment
                                decimal OwnerId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();
                                TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (CompPaymentObj != null)
                                {
                                    CompPaymentObj.Amount = CompPaymentObj.Amount + AmountToBepaid;
                                }
                                else
                                {
                                    TblCompPayment CompPaymentObj1 = new TblCompPayment()
                                    {
                                        PaymentDueDate = PaymentDate,
                                        PaymentDate = PaymentDate,
                                        PaymentDetails = PaymentDetails,
                                        Amount = AmountToBepaid,
                                        CompanyId = CompanyObj.CompanyId,
                                        CompanyUserId = OwnerId,
                                        PaymentTypeId = PaymentType,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                                    mastercontext.SubmitChanges();
                                    TblCompAdjustment AdjObj = new TblCompAdjustment()
                                    {
                                        CompanyId = CompanyObj.CompanyId,
                                        Amount = AmountToBepaid,
                                        CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                        PaymentTypeId = PaymentType,
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = PaymentDetails,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                                }
                                mastercontext.SubmitChanges();
                                #endregion

                            }
                            else
                            {
                                if (ErrorString == "")
                                {
                                    ErrorString = lstrEmail + ",";
                                }
                                else
                                {
                                    ErrorString = ErrorString + "," + lstrEmail + ",";
                                }
                            }

                            #endregion

                            if (ErrorString != "")
                            {
                                ErrorString = ErrorString.TrimEnd(',');
                                ErrorString = "We couldn't add all user because some email ids are already used by other users (" + ErrorString + ").";

                            }

                        }
                    }

                }
                catch (Exception)
                {

                    throw;
                }

                datacontext.Connection.Close();

            }
            

            return ErrorString;
        }
        #endregion
        #endregion
    }
}