﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace EflexServices
{
    public partial interface IEflexService
    {
        #region CreateCard
        [WebInvoke(UriTemplate = "CreateCard", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse CreateCard(CreateCardInfo iObj);
        #endregion CreateCard

        #region ChangeCardStatus
        [WebInvoke(UriTemplate = "ChangeCardStatus", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ChangeCardStatus(ChangeCardStatusInfo iObj);
        #endregion ChangeCardStatus

        #region LoadValue
        [WebInvoke(UriTemplate = "LoadValue", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoadValueResponse LoadValue(LoadValueInfo iObj);
        #endregion LoadValue

        #region ActivateCardStatus
        [WebInvoke(UriTemplate = "ActivateCardStatus", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ActivateCardStatus(ActivateCardStatusInfo iObj);
        #endregion ActivateCardStatus

        #region BlockCard
        [WebInvoke(UriTemplate = "BlockCard", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse BlockCard(BlockCardStatusInfo iObj);
        #endregion BlockCard

        #region GetMasterCardList
        [WebInvoke(UriTemplate = "GetMasterCardList", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetMasterCardListResponse GetMasterCardList(GetMasterCardListInput iObj);
        #endregion GetMasterCardList

        #region ImportServicesExcel
        [WebInvoke(UriTemplate = "ImportServicesExcel", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ImportServicesExcel(ImportServiceInput iObj);
        #endregion ImportServicesExcel

        #region UploadReceipt
        [WebInvoke(UriTemplate = "UploadReceipt", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UploadReceipt(UploadReceiptInput iObj);
        #endregion UploadReceipt

        #region UnSuspendCard
        [WebInvoke(UriTemplate = "UnSuspendCard", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UnSuspendCard(UnSuspendStatusInfo iObj);
        #endregion UnSuspendCard

        #region AddService
        [WebInvoke(UriTemplate = "AddService", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddService(AddServiceInput iObj);
        #endregion AddService

        #region EditService
        [WebInvoke(UriTemplate = "EditService", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse EditService(EditServiceInput iObj);
        #endregion EditService

        #region DeleteService
        [WebInvoke(UriTemplate = "DeleteService", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteService(IdInputv2 iObj);
        #endregion DeleteService

        #region ListOfServices
        [WebInvoke(UriTemplate = "ListOfServices", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfServicesResponse ListOfServices(ListOfServicesIdInput iObj);
        #endregion ListOfServices

        #region PopulateServiceTypes
        [WebInvoke(UriTemplate = "PopulateServiceTypes", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateServiceTypesResponse PopulateServiceTypes();
        #endregion PopulateServiceTypes

        #region ListOfUnApprovedServices
        [WebInvoke(UriTemplate = "ListOfUnApprovedServices", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfServicesResponse ListOfUnApprovedServices(ListOfServicesIdInput iObj);
        #endregion ListOfUnApprovedServices

        #region AddServiceProvider
        [WebInvoke(UriTemplate = "AddServiceProvider", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddServiceProvider(AddServiceProviderInput iObj);
        #endregion AddServiceProvider

        #region ListOfServicesProvider
        [WebInvoke(UriTemplate = "ListOfServicesProvider", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfServicesProviderResponse ListOfServicesProvider(ListOfServiceProviderIdInput iObj);
        #endregion ListOfServicesProvider

        #region PopulateServices
        [WebInvoke(UriTemplate = "PopulateServices", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateServicesResponse PopulateServices(IdInput iObj);
        #endregion PopulateServices

        #region ListOfServicesForAdmin
        [WebInvoke(UriTemplate = "ListOfServicesForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfServicesResponse ListOfServicesForAdmin(ListOfServicesIdInput iObj);
        #endregion ListOfServicesForAdmin

        #region ListCardTransactions
        [WebInvoke(UriTemplate = "ListCardTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListCardTransactionsResponse ListCardTransactions(ListCardTransactionsInput iObj);
        #endregion ListCardTransactions

        #region GetAccountSummaryData
        [WebInvoke(UriTemplate = "GetAccountSummaryData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAccountSummaryResponse GetAccountSummaryData(GetAccountDataInput iObj);
        #endregion GetAccountSummaryData

        #region UploadReceiptV2
        [WebInvoke(UriTemplate = "UploadReceiptV2", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UploadReceiptV2(UploadReceiptInputV2 iObj);
        #endregion UploadReceiptV2

        

        #region LoadValue_New
        [WebInvoke(UriTemplate = "LoadValue_New", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoadValueResponse LoadValue_New(LoadValueInfoNew iObj);
        #endregion LoadValue_New

        #region UpdateLoadValueDetails
        [WebInvoke(UriTemplate = "UpdateLoadValueDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateLoadValueDetails(UpdateLoadValueDetailsInput iObj);
        #endregion UpdateLoadValueDetails

        #region UpdateLoadValueDetailsForApp
        [WebInvoke(UriTemplate = "UpdateLoadValueDetailsForApp", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateLoadValueDetailsForApp(UpdateLoadValueDetailsForAppInput iObj);
        #endregion UpdateLoadValueDetailsForApp
    }
}