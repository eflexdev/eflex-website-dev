﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CCardObjects
    {
    }

    public class CreateCardInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class LoadValueInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal ClaimAmount { get; set; }
        public decimal ServiceTypeId { get; set; }
        public decimal ServiceId { get; set; }
        public decimal Dependant { get; set; }
        public string Note { get; set; }
        public decimal ServiceProviderId { get; set; }
        public string ServiceProvider { get; set; }
        public string ProviderAddress { get; set; }
        public bool IsManual { get; set; }
        public string ProviderEmail { get; set; }
        public string ProviderContactNo { get; set; }
        public string GoogleUniqueId { get; set; }

    }

    public class ChangeCardStatusInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal MasterCardId { get; set; }
        public decimal CompanyId { get; set; }
        public string Status { get; set; }
    }

    public class ActivateCardStatusInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal MasterCardId { get; set; }
        public decimal CompanyId { get; set; }
        public string Status { get; set; }
        public string CVV { get; set; }
        public string AccessCode { get; set; }
        public string CardNumber { get; set; }
    }

    public class BlockCardStatusInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal MasterCardId { get; set; }
        public decimal CompanyId { get; set; }
        public string Status { get; set; }
        public string BlockReason { get; set; }
    }

    public class BlockAllCardStatusInfo
    {
        public string Status { get; set; }
        public string BlockReason { get; set; }
    }

    public class CGetMasterCardListResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<GetMasterCardList> GetMasterCardList { get; set; }
    }

    public class GetMasterCardListInput
    {
        public Int32 ChunckSize { get; set; }
        public Int32 ChunckStart { get; set; }
        public string SearchString { get; set; }
        public decimal LoggedInUserId { get; set; }
        public string Status { get; set; }
    }

    public class GetMasterCardList
    {
        public int RowId { get; set; }
        public decimal MasterCardId { get; set; }
        public string SessionId { get; set; }
        public string UniqueId { get; set; }
        public string OrderItemId { get; set; }
        public string OrderId { get; set; }
        public string AccountId { get; set; }
        public string CardId { get; set; }
        public string CardHolderId { get; set; }
        public string CountryCode { get; set; }
        public string CountrySubDivisionCode { get; set; }
        public string FourthLine { get; set; }
        public decimal UserId { get; set; }
        public decimal CompanyId { get; set; }
        public decimal CourierShipping { get; set; }
        public decimal Quantity { get; set; }
        public string IsActive { get; set; }
        public string BlockDate { get; set; }
        public string DateCreated { get; set; }
        public string CardStatus { get; set; }
        public decimal CVV { get; set; }
        public decimal ExpMonth { get; set; }
        public decimal ExpYear { get; set; }
        public string CardNumber { get; set; }
        public string BlockReason { get; set; }
        public string BlockDT { get; set; }
        public string EmpName { get; set; }
        public string LastFourDigit { get; set; }
        public string ExpiryDt { get; set; }

       
    }

    public class AccountSummary
    {
        public decimal Balance { get; set; }
        public string ExpiryDate { get; set; }
        public Int16 Status { get; set; }
        public string LastFourDigitOfCardNo { get; set; }
        public string Purses { get; set; }
    }
    public class ImportServiceInput
    {
        public string FileName { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class UploadReceiptInput
    {
        public decimal LoggedInUserId { get; set; }
        public decimal ClaimId { get; set; }
        public decimal CompanyId { get; set; }
        public string FileName { get; set; }
    }

    public class UnSuspendStatusInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal MasterCardId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CPopulateServicesResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<PopulateServices> PopulateServices { get; set; }
    }

    public class PopulateServices
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public decimal ServiceId { get; set; }
        public string ServiceName { get; set; }
    }

    public class ListCardTransactionsInput
    {
        public string StartDate { get; set; }
        public decimal Days { get; set; }
        public string TransType { get; set; }
        public decimal CardId { get; set; }
    }

    
    public class CListCardTransactionsResponse
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public List<ListCardTransactions> ListCardTransactions { get; set; }
    }

    public class ListCardTransactions
    {
        public string CardCurrencyCode { get; set; }
        public string LocalCurrencyAmount { get; set; }
        public string LocalCurrencyCode { get; set; }
        public string MerchantName { get; set; }
        public string Reference { get; set; }
        public string SettlementDate { get; set; }
        public string TransactionAmount { get; set; }
        public string TransactionCode { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionDescription { get; set; }

    }

    public class GetAccountDataInput
    {
        public string AccountId { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal MasterCardId { get; set; }

    }
      public class CAccountSummaryResponse
      {
          public string Status { get; set; }
          public string Message { get; set; }
          public AccountSummary AccountSummaryData { get; set; }
      }

      public class UploadReceiptInputV2
      {
          public decimal LoggedInUserId { get; set; }
          public decimal ClaimId { get; set; }
          public decimal CompanyId { get; set; }
          public string FileName { get; set; }
          public string ReceiptNo { get; set; }
          public string ReceiptDt { get; set; }
      }

      public class LoadValueInfoNew
      {
          public decimal UserId { get; set; }
          public decimal CompanyId { get; set; }
          public decimal ClaimAmount { get; set; }
       
      }

      public class UpdateLoadValueDetailsInput
      {
          public decimal UserId { get; set; }
          public decimal CompanyId { get; set; }
          public decimal ServiceTypeId { get; set; }
          public decimal ServiceId { get; set; }
          public decimal Dependant { get; set; }
          public string Note { get; set; }
          public decimal ServiceProviderId { get; set; }
          public string ServiceProvider { get; set; }
          public string ProviderAddress { get; set; }
          public string ProviderEmail { get; set; }
          public string ProviderContactNo { get; set; }
          public string GoogleUniqueId { get; set; }
          public decimal ClaimId { get; set; }


      }

      public class UpdateLoadValueDetailsForAppInput
      {
          public decimal UserId { get; set; }
          public decimal CompanyId { get; set; }
          public decimal ServiceTypeId { get; set; }
          public decimal ServiceId { get; set; }
          public decimal Dependant { get; set; }
          public string Note { get; set; }
          public decimal ServiceProviderId { get; set; }
          public string ServiceProvider { get; set; }
          public string ProviderAddress { get; set; }
          public string ProviderEmail { get; set; }
          public string ProviderContactNo { get; set; }
          public string GoogleUniqueId { get; set; }
          public decimal ClaimId { get; set; }

          public string FileName { get; set; }
          public string ReceiptNo { get; set; }
          public string ReceiptDt { get; set; }

      }
}