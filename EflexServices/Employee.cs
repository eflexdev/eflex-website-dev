﻿using EnCryptDecrypt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region Import Excel
        #region ImportEmployeesExcel
        public CResponse ImportEmployeesExcel(ImportUsersExcelInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            using (MasterDBDataContext dbContext = new MasterDBDataContext())
            {
                UniqueCode = (from p in dbContext.TblCompanies where p.CompanyId == iObj.CompanyId && p.Active == true select p.UniqueCode).Take(1).SingleOrDefault();
                dbContext.Connection.Close();
            }
            try
            {
                if (iObj.FileName != null)
                {
                    string FileName = Path.GetFileName(iObj.FileName);
                    string FileName1 = Path.GetFileNameWithoutExtension(iObj.FileName);
                    string Extension = Path.GetExtension(iObj.FileName);
                    string FolderPath = ConfigurationManager.AppSettings["ConPath"].ToString();
                    FolderPath = FolderPath + iObj.CompanyId + "\\";

                    DataTable datatab = new DataTable();
                    string FilePath = FolderPath + FileName;
                    if (File.Exists(FilePath))
                    {
                        datatab = ImportEmpData(FilePath, Extension, iObj.DbCon);
                        string Message = AddXLSEmpDetails(datatab, iObj.DbCon, UniqueCode, iObj.CompanyId);
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message = "FAIL";
                    }




                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                    lobjResponse.Message = "FAIL";
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return lobjResponse;
        }


        #endregion ImportSyllabusData

        #region ImportEmpData
        private DataTable ImportEmpData(string FilePath, string Extension, string DbCon)
        {
            try
            {
                string conStr = "";
                string isHDR = "Yes";
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conStr = String.Format(conStr, FilePath, isHDR);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();

                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw new Exception("An error ocurred while executing ImportVehicles(). " + ex.Message);
            }
        }


        #endregion ImportEmpData

        #region AddXLSEmpDetails
        /// <summary>
        /// Author:Jasmeet kaur
        /// Date:180618
        /// Function used to insert the data from excel in database.
        /// </summary>
        /// <param name="datatab"></param>
        /// <param name="DbCon"></param>
        private string AddXLSEmpDetails(DataTable datatab, string DbCon, string UniqueCode, decimal CompanyId)
        {
            string lstrFName = "";
            string lstrLName = "";
            string lstrEmail = "";
            string lstrPlan = "";
            string ErrorString = "";
            string Constring = GetConnection(DbCon);
            decimal AmountToBepaid = 0;
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
                                
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {

               
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    for (int i = 0; i < datatab.Rows.Count; i++)
                    {
                        if (datatab.Rows[i]["FIRST NAME"].ToString() != "")
                        {
                            lstrFName = Convert.ToString(datatab.Rows[i]["FIRST NAME"]).ToString();

                        }
                        if (datatab.Rows[i]["LAST NAME"].ToString() != "")
                        {
                            lstrLName = Convert.ToString(datatab.Rows[i]["LAST NAME"]).ToString();

                        }
                        if (datatab.Rows[i]["EMAIL"].ToString() != "")
                        {
                            lstrEmail = Convert.ToString(datatab.Rows[i]["EMAIL"]).ToString();

                        }
                        if (datatab.Rows[i]["PLAN"].ToString() != "")
                        {
                            lstrPlan = Convert.ToString(datatab.Rows[i]["PLAN"]).ToString();

                        }
                        TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.Name.ToUpper() == lstrPlan.ToUpper() select p).Take(1).SingleOrDefault();
                        if (PlanObj != null)
                        {
                            string PaymentType = "";
                            DateTime? PaymentDate = null;
                            string PaymentDetails = "";
                            if (PlanObj.ContributionSchedule == 1)
                            {
                                PaymentType = CObjects.enumPaymentType.DM.ToString();
                            }
                            else
                            {
                                PaymentType = CObjects.enumPaymentType.DA.ToString();

                            }

                            string Password = RandomString();
                            string EncryptPwd = CryptorEngine.Encrypt(Password);
                            #region Add Employees
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.EmailAddress == lstrEmail && p.UserTypeId == Convert.ToInt16(CObjects.enmUserType.Employee) select p).Take(1).SingleOrDefault();
                            if (UserObj == null)
                            {

                                decimal GetLatestId = 0;
                                GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                if (GetLatestId == 0)
                                {
                                    GetLatestId = 1;
                                }
                                else
                                {
                                    GetLatestId = GetLatestId + 1;
                                }
                                TblCompanyUser Obj = new TblCompanyUser()
                                {
                                    FirstName = lstrFName,
                                    LastName = lstrLName,
                                    UserName = (UniqueCode + "_" + lstrFName) + GetLatestId.ToString(),
                                    Pwd = EncryptPwd,
                                    UserTypeId = Convert.ToInt16(CObjects.enmUserType.Employee),
                                    EmailAddress = lstrEmail,
                                    IsActive = true,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                datacontext.SubmitChanges();
                                UpdateMsgUser(Obj, "ADD", CompanyId);

                                TblEmployee EmpObj = new TblEmployee()
                                {
                                    CompanyUserId = Obj.CompanyUserId,
                                    CompanyPlanId = PlanObj.CompanyPlanId,
                                    FirstName = lstrFName,
                                    LastName = lstrLName,
                                    EmailAddress = lstrEmail,
                                    IsActive = true,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblEmployees.InsertOnSubmit(EmpObj);
                                datacontext.SubmitChanges();

                                decimal BalanceAmount = 0;
                                #region Apply account setUp fee
                                TblFeeDetail SetUpFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.Setupfee) select p).Take(1).SingleOrDefault();
                                if (SetUpFeeObj != null)
                                {
                                    TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                    {
                                        TransactionDate = DateTime.UtcNow,
                                        TransactionDetails = "Account Set-up Fee",
                                        Amount = SetUpFeeObj.Amount,
                                        CompanyUserId = EmpObj.CompanyUserId,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        ClaimId = -1,
                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                    {
                                        CompanyUserId = Obj.CompanyUserId,
                                        Amount = SetUpFeeObj.Amount,
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = "Account SetUp Fee",
                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                    datacontext.SubmitChanges();
                                    BalanceAmount = Convert.ToDecimal(-SetUpFeeObj.Amount);
                               }
                                #endregion

                                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                                if (CompanyObj != null)
                                {
                                    CompanyObj.NoOfEmployee = CompanyObj.NoOfEmployee + 1;
                                    mastercontext.SubmitChanges();
                                }

                                #region payment commented

                              
                                #endregion payment


                                #region payment
                                #region Set Payment date


                                TblCompPayment CompanyPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (CompanyPaymentObj != null)
                                {
                                    if (CompanyPaymentObj.PaymentDate >= DateTime.UtcNow)
                                    {
                                        PaymentDate = CompanyPaymentObj.PaymentDate;
                                    }
                                    else
                                    {
                                        DateTime now = DateTime.UtcNow;
                                        var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                        var SevenDT = new DateTime(now.Year, now.Month, 7);
                                        DateTime date = DateTime.Now;

                                        DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                        string dayToday = DayOnTwelve.ToString();

                                        // compare enums
                                        if (DayOnTwelve == DayOfWeek.Saturday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(2);
                                        }
                                        else if (DayOnTwelve == DayOfWeek.Sunday)
                                        {
                                            PaymentDate = TweleveDT.Date.AddDays(1);
                                        }
                                        else
                                        {
                                            PaymentDate = TweleveDT.Date;
                                        }


                                        if (now.Date > PaymentDate)
                                        {
                                            PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                        }

                                    }
                                }
                                else
                                {
                                    DateTime now = DateTime.UtcNow;
                                    var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                    var SevenDT = new DateTime(now.Year, now.Month, 7);
                                    DateTime date = DateTime.Now;

                                    DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                    string dayToday = DayOnTwelve.ToString();

                                    // compare enums
                                    if (DayOnTwelve == DayOfWeek.Saturday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(2);
                                    }
                                    else if (DayOnTwelve == DayOfWeek.Sunday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(1);
                                    }
                                    else
                                    {
                                        PaymentDate = TweleveDT.Date;
                                    }


                                    if (now.Date > PaymentDate)
                                    {
                                        PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                    }

                                }

                                #endregion


                                #region Apply account setUp fee
                                TblFeeDetail SetupFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.Title.ToUpper() == "SETUP FEE/INITIAL FEE" select p).Take(1).SingleOrDefault();
                                if (SetupFeeObj != null)
                                {
                                    TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                    {
                                        TransactionDate = DateTime.UtcNow,
                                        TransactionDetails = "Account Set-up Fee",
                                        Amount = SetupFeeObj.Amount,
                                        CompanyUserId = Obj.CompanyUserId,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        ClaimId = -1,
                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                    {
                                        CompanyUserId = Obj.CompanyUserId,
                                        Amount = SetupFeeObj.Amount,
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = "Account Set-up Fee",
                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);

                                    BalanceAmount = Convert.ToDecimal(-SetupFeeObj.Amount);
                                }
                                #endregion

                                    PaymentDetails = "Initial Payment";
                                    PaymentType = CObjects.enumPaymentType.IN.ToString();

                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                    {
                                        TransactionDate = DateTime.UtcNow,
                                        TransactionDetails = PaymentDetails,
                                        Amount = PlanObj.NetDeposit,
                                        CompanyUserId = Obj.CompanyUserId,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        ClaimId = -1,
                                        PaymentTypeId = PaymentType,
                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };

                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                   

                                 
                                    TblPayment PaymentObj1 = new TblPayment()
                                    {
                                        PaymentDate = PaymentDate,
                                        PaymentDetails = PaymentDetails,
                                        RecurringDeposit = PlanObj.RecurringDeposit,
                                        InitialDeposit = PlanObj.InitialDeposit,
                                        NetDeposit = PlanObj.NetDeposit,
                                        AdministrationInitialFee = PlanObj.AdministrationInitialFee,
                                        AdministrationMonthlyFee = PlanObj.AdministrationMonthlyFee,
                                        HSTInitial = PlanObj.HSTInitial,
                                        HSTMonthly = PlanObj.HSTMonthly,
                                        PremiumTaxInitial = PlanObj.PremiumTaxInitial,
                                        PremiumTaxMonthly = PlanObj.PremiumTaxMonthly,
                                        RetailSalesTaxInitial = PlanObj.RetailSalesTaxInitial,
                                        RetailSalesTaxMonthly = PlanObj.RetailSalesTaxMonthly,
                                        GrossTotalInitial = PlanObj.GrossTotalInitial,
                                        GrossTotalMonthly = PlanObj.GrossTotalMonthly,
                                        Amount = PlanObj.GrossTotalInitial + PlanObj.GrossTotalMonthly + PlanObj.NetDeposit,
                                        CompanyPlanId = PlanObj.CompanyPlanId,
                                        CompanyUserId = Obj.CompanyUserId,
                                        PaymentDueDate = PaymentDate,
                                        PaymentTypeId = PaymentType,
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        IsRemit = false
                                    };


                                    datacontext.TblPayments.InsertOnSubmit(PaymentObj1);
                                    TblEmpBalance EmpPaymentObj = new TblEmpBalance()
                                    {
                                        Amount = BalanceAmount,
                                        CompanyUserId = Obj.CompanyUserId,
                                        Active = true,
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        DateModified = DateTime.UtcNow,
                                        DateCreated = DateTime.UtcNow
                                    };

                                    datacontext.TblEmpBalances.InsertOnSubmit(EmpPaymentObj);
                                        
                                    
                                    datacontext.SubmitChanges();
                                    AmountToBepaid = Convert.ToDecimal(PaymentObj1.Amount);

                                    CompanyPaymentObj.Amount = CompanyPaymentObj.Amount + AmountToBepaid;
                                    mastercontext.SubmitChanges();
                                
                                
                                #endregion

                                #region Broker Commission
                                    TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                       where p.Active == true && p.CompanyId == CompanyObj.CompanyId
                                                                       select p).Take(1).SingleOrDefault();
                                    if (CompReferral != null)
                                    {
                                        
                                        decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                        where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                        select Convert.ToDecimal(p.NetDeposit)).Sum();

                                        if (CompReferral.BrokerId != -1)
                                        {
                                            TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                                   where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                                   select p).Take(1).SingleOrDefault();
                                            TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                          where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyObj.CompanyId
                                                                          select p).Take(1).SingleOrDefault();
                                            decimal PercentageBrokerCharge = 0;
                                            if (FeeObj != null)
                                            {
                                                if (FeeObj.BrokerFee > 0)
                                                {
                                                    PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                                }
                                            }
                                            else
                                            {
                                                if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                                {
                                                    PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                                }
                                                else
                                                {
                                                    PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                                }
                                            }


                                            if (PercentageBrokerCharge > 0)
                                            {
                                                decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                                TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                                   where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                                   p.UserId == BrokerObj.UserId
                                                                                   select p).Take(1).SingleOrDefault();
                                                if (TransactionObj != null)
                                                {
                                                    TransactionObj.Amount = Amount;
                                                    TransactionObj.Contribution = BrokerAmountToBepaid;
                                                    TransactionObj.PercentFee = PercentageBrokerCharge;
                                                }
                                                else
                                                {
                                                    TblBrokerPayment TransactionObj1 = new TblBrokerPayment()
                                                    {
                                                        PaymentDate = DateTime.UtcNow,
                                                        PaymentDueDate = DateTime.UtcNow,
                                                        PaymentDetails = "Payments",
                                                        Amount = Amount,
                                                        CompanyId = CompanyObj.CompanyId,
                                                        Contribution = BrokerAmountToBepaid,
                                                        PercentFee = PercentageBrokerCharge,
                                                        UserId = BrokerObj.UserId,
                                                        PaymentTypeId = CObjects.enumPaymentType.DM.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow
                                                    };
                                                    mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj1);
                                                }
                                                mastercontext.SubmitChanges();
                                            }
                                        }
                                    }
                                    #endregion Broker Commission

                                #region SendMail

                                
                                StringBuilder builder = new StringBuilder();
                               
                                

                                builder.Append("Hello " + lstrFName + ",<br/><br/>");
                                builder.Append("Welcome to eFlex, your very own health spending account set up by your employer, " + CompanyObj.CompanyName + " <br/><br/>");
                                builder.Append("This account provides you with the flexibility to claim medical expenses so you’re not out of pocket.<br/> To view how much money you’re eligible to use, click <a href=" + lstrloginUrl + " >here</a> to complete your online account as well as view your balance.<br/><br/>");
                                builder.Append("For further convenience, we have SMS Messaging.  Here you will receive updates throughout your claims process so your always aware of what’s happening in your account<br/><br/>");
                                builder.Append("Please use the following credentials for login to eFlex Portal:<br/><br/>");
                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                builder.Append("Username: " + Obj.UserName.Split('_')[1] + "<br/><br/>");
                                builder.Append("Password: " + Password + "<br/><br/>");
                                          
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
    
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = lstrEmail;
                                Mail.Subject = "eFlex Account - Welcome";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyId;
                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                #endregion SendMail


                                #region UpdateCompanyPayment
                                decimal OwnerId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();
                                TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                if (CompPaymentObj != null)
                                {
                                    CompPaymentObj.Amount = CompPaymentObj.Amount + AmountToBepaid;
                                }
                                else
                                {
                                    TblCompPayment CompPaymentObj1 = new TblCompPayment()
                                    {
                                        PaymentDueDate = PaymentDate,
                                        PaymentDate = PaymentDate,
                                        PaymentDetails = PaymentDetails,
                                        Amount = AmountToBepaid,
                                        CompanyId = CompanyObj.CompanyId,
                                        CompanyUserId = OwnerId,
                                        PaymentTypeId = PaymentType,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                                    mastercontext.SubmitChanges();
                                    TblCompAdjustment AdjObj = new TblCompAdjustment()
                                    {
                                        CompanyId = CompanyObj.CompanyId,
                                        Amount = AmountToBepaid,
                                        CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                        PaymentTypeId = PaymentType,
                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                        AdjustmentDT = DateTime.UtcNow,
                                        Remarks = PaymentDetails,
                                        TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                                }
                                mastercontext.SubmitChanges();
                                #endregion

                                List<TblNotificationDetail> NotificationUserList = null;
                                string PushNotifyText = "Your eFlex account has been set up by owner. Please login and update your profile. Thanks!";
                                           
                                NotificationUserList = InsertionInNotificationTable(Obj.CompanyUserId, PushNotifyText, datacontext, Obj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());
                                if (NotificationUserList != null)
                                {
                                    NotificationList.AddRange(NotificationUserList);
                                    NotificationUserList.Clear();
                                }
                                                          
                               
                            }
                            else
                            {
                                if (ErrorString == "")
                                {
                                    ErrorString = lstrEmail + ",";
                                }
                                else
                                {
                                    ErrorString = ErrorString + "," + lstrEmail + ",";
                                }
                            }

                            #endregion

                            if (ErrorString != "")
                            {
                                ErrorString = ErrorString.TrimEnd(',');
                                ErrorString = "We couldn't add all user because some email ids are already used by other users (" + ErrorString + ").";

                            }

                        }
                    }



                    #region Push Notifications


                    if (NotificationList != null && NotificationList.Count() > 0)
                    {
                        SendNotification(datacontext, NotificationList,-1);
                        NotificationList.Clear();

                    }
                    #endregion Push Notifications

                }
                catch (Exception)
                {

                    throw;
                }
                datacontext.Connection.Close();

            }

            return ErrorString;
        }
        #endregion 
        #endregion

        #region DeleteUser
        public CResponse DeleteUser(DeleteUserInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateUser(datacontext))
                    {
                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            UserObj.Active = false;
                            UserObj.DateModified = DateTime.UtcNow;
                            if (UserObj.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Employee))
                            {
                                TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.UserId select p).Take(1).SingleOrDefault();
                                if (EmpObj != null)
                                {
                                    EmpObj.Active = false;
                                    EmpObj.DateModified = DateTime.UtcNow;
                                    UpdateMsgUser(UserObj, "DELETE", CompanyId);
                                }
                            }

                            datacontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                     
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "DeleteUser", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }

            return lobjResponse;
        }
        #endregion DeleteUser

        #region AddAdmin
        public CResponse AddAdmin(AddAdminInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.DBCon == iObj.DbCon select p).Take(1).SingleOrDefault();
                if (CompanyObj != null)
                {
                    UniqueCode = CompanyObj.UniqueCode;

                }
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        if (IsValidateUser(datacontext))
                        {
                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.UserTypeId == iObj.UserTypeId && p.EmailAddress == iObj.Email select p).Take(1).SingleOrDefault();

                            if (UserObj == null)
                            {
                                string Password = RandomString();
                                string EncryptPwd = CryptorEngine.Encrypt(Password);
                                decimal GetLatestId = 0;
                                GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                if (GetLatestId == 0)
                                {
                                    GetLatestId = 1;
                                }
                                else
                                {
                                    GetLatestId = GetLatestId + 1;
                                }
                                TblCompanyUser Obj = new TblCompanyUser()
                                {
                                    FirstName = iObj.FName,
                                    LastName = iObj.LName,
                                    UserName = UniqueCode + "_" + iObj.FName + GetLatestId.ToString(),
                                    ContactNo = iObj.ContactNo,
                                    Pwd = EncryptPwd,
                                    UserTypeId = Convert.ToInt16(iObj.UserTypeId),
                                    EmailAddress = iObj.Email,
                                    IsActive = true,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                datacontext.SubmitChanges();

                                UpdateMsgUser(Obj, "ADD", CompanyObj.CompanyId);
                                CompanyObj.NoOfAdmins = CompanyObj.NoOfAdmins + 1;
                                mastercontext.SubmitChanges();

                                #region SendMail

                                
                                StringBuilder builder = new StringBuilder();

                                builder.Append("Hello " + iObj.FName + ",<br/><br/>");
                                builder.Append("Welcome to eFlex, You has been added as an Admin for the company named as ," + CompanyObj.CompanyName + " <br/><br/>");
                                builder.Append("Please use the following credentials for login to eFlex Portal:<br/><br/>");
                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                builder.Append("Username: " + Obj.UserName.Split('_')[1] + "<br/><br/>");
                                builder.Append("Password: " + Password + "<br/><br/>");
                                builder.Append("Enjoy your eFlex account,<br/><br/>");
                                builder.Append("The eclipse EIO Team<br/><br/>");

                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = iObj.Email;
                                Mail.Subject = "eFlex Corporate Account Administrator - Welcome";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyObj.CompanyId;
                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                #endregion SendMail

                                lobjResponse.ID = Obj.CompanyUserId;
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                lobjResponse.Message = "Kindly change your email address because this email address already used by another user.";
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                            lobjResponse.Message =  CObjects.SessionExpired.ToString();
                            IsLogOut = true;
                          
                        }
                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    lobjResponse.IsLogOut = IsLogOut;
                    CreateResponseLog(datacontext, StartTime, -1, "AddAdmin", InputJson, lobjResponse.Message);
                    datacontext.Connection.Close();
                }
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion AddAdmin

        #region GetEmpDashboardTransactions
        public CGetEmpDashboardTransactionsResponse GetEmpDashboardTransactions(DbConIdInput iObj)
        {
            CGetEmpDashboardTransactionsResponse lobjResponse = new CGetEmpDashboardTransactionsResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        List<EmpPendingTransaction> PendingObj = (from p in datacontext.TblEmpTransactions
                                                                  where p.CompanyUserId == iObj.Id &&
                                                                      (p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) || p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.OverDue) || p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.PartialPaid))
                                                                      && p.Active == true orderby p.DateCreated descending
                                                                  select new EmpPendingTransaction()
                                                                  {
                                                                      Amount = Convert.ToDecimal(p.Amount),
                                                                      Date = Convert.ToString(p.TransactionDate),
                                                                      Details = p.TransactionDetails,
                                                                      PaymentType = p.PaymentTypeId,
                                                                      TypeId = p.TypeId == 1 ? "Credit" : "Debit",
                                                                      ClaimId = Convert.ToDecimal(p.ClaimId),
                                                                      ClaimReceipt = (p.ClaimId == -1 ? "" : (from q in datacontext.TblEmpClaims where q.Active == true && q.EmpClaimId == p.ClaimId select q.UploadFileName).Take(1).SingleOrDefault())
                                                                  }).Take(10).ToList();
                        List<EmpCurrentTransaction> CurrentObj = (from p in datacontext.TblEmpTransactions
                                                                  where p.CompanyUserId == iObj.Id && 
                                                                  p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                  && p.Active == true
                                                                  orderby p.DateCreated descending
                                                                  select new EmpCurrentTransaction()
                                                                  {
                                                                      Amount = Convert.ToDecimal(p.Amount),
                                                                      Date = Convert.ToString(p.TransactionDate),
                                                                      Details = p.TransactionDetails,
                                                                      PaymentType = p.PaymentTypeId,
                                                                      TypeId = p.TypeId == 1 ? "Credit" : "Debit",
                                                                      ClaimId = Convert.ToDecimal(p.ClaimId),
                                                                      ClaimReceipt = (p.ClaimId == -1 ? "" : (from q in datacontext.TblEmpClaims where q.Active == true && q.EmpClaimId == p.ClaimId select q.UploadFileName).Take(1).SingleOrDefault())

                                                                  }).Take(10).ToList();
                        decimal Count = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select p).Count();
                        decimal TotalDueAmt = 0;
                        if (Count > 0)
                        {
                            TotalDueAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select Convert.ToDecimal(p.Amount)).Sum();
                        }

                        lobjResponse.AmountDue = TotalDueAmt < 0 ? 0 : TotalDueAmt;

                        lobjResponse.PendingTransaction = PendingObj;
                        lobjResponse.CurrentTransaction = CurrentObj;
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        List<TblCompanyUserDevice> DeviceObj = (from p in datacontext.TblCompanyUserDevices where p.Active == true && p.CompanyUserId == iObj.Id select p).ToList();
                        if (DeviceObj != null && DeviceObj.Count > 0)
                        {
                            DeviceObj.ForEach(x => x.Active = false);
                            datacontext.SubmitChanges();
                        }

                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpDashboardTransactions", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetEmpTransactionData
        public CGetEmpTransactionDataResponse GetEmpTransactionData(GetEmpTransactionInput iObj)
        {
            CGetEmpTransactionDataResponse lobjResponse = new CGetEmpTransactionDataResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            UserId = iObj.UserId;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        AmountDetails AmountObj = (from p in datacontext.sp_GetEmpTransactionTypeDetails(Convert.ToDateTime(iObj.Period), iObj.UserId)
                                                   select new AmountDetails()
                                                   {
                                                       Withdrawal = Convert.ToDecimal(p.Withdrawal) == null ? 0 : Convert.ToDecimal(p.Withdrawal),
                                                       CurrentBalance = Convert.ToDecimal(p.Balance) == null ? 0 : Convert.ToDecimal(p.Balance),
                                                       Contributions = Convert.ToDecimal(p.Contribution) == null ? 0 : Convert.ToDecimal(p.Contribution),
                                                   }).Take(1).SingleOrDefault();
                        List<EmpTransactionList> EmpTransactionObj = (from p in datacontext.sp_GetEmpTransactionList(iObj.ChunkStart, iObj.SearchString, iObj.ChunkSize, Convert.ToDateTime(iObj.Period), iObj.Id, iObj.UserId)
                                                                      select new EmpTransactionList()
                                                                      {
                                                                          TransactionDate = Convert.ToString(p.TransactionDate),
                                                                          ContributionType = Convert.ToString(p.PaymentTypeId),
                                                                          Description = p.TransactionDetails,
                                                                          Amount = Convert.ToDecimal(p.Amount),
                                                                          TypeId = p.TypeId == 1 ? "Credit" : "Debit",
                                                                          PaymentStatus = Convert.ToDecimal(p.PaymentStatus)
                                                                      }).ToList();


                        lobjResponse.EmpTransactionList = EmpTransactionObj;
                        lobjResponse.AmountDetails = AmountObj;

                        
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpTransactionData", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetActivityData

        #region GetEmpDashboardData
        public CGetEmpDashboardDataResponse GetEmpDashboardData(DbConIdInput iObj)
        {
            CGetEmpDashboardDataResponse lobjResponse = new CGetEmpDashboardDataResponse();
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    decimal PlandesignId = -1;
                    UserId = iObj.Id;
                    if (IsValidateUser(datacontext))
                    {
                        List<GetEmpDashboardData> Obj = new List<GetEmpDashboardData>();
                        string PlanName = (from p in datacontext.TblEmpTransactions
                                           join q in datacontext.TblCompanyPlans on p.CompanyPlanId equals q.CompanyPlanId
                                           where p.Active == true && p.CompanyUserId == iObj.Id && q.Active == true
                                           select q.Name).Take(1).SingleOrDefault();
                        PlandesignId = (from p in datacontext.TblCompanyPlans
                                                join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                where p.Active == true && q.CompanyUserId == iObj.Id && q.Active == true
                                                select Convert.ToDecimal(p.PlanDesign)).Take(1).SingleOrDefault();

                        #region Claim Data
                        decimal ClaimAmt = 0;
                         List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true &&
                                                (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress))
                                                && p.CompanyUserId == iObj.Id
                                                select p).ToList();
                        if (ClaimObj != null && ClaimObj.Count() > 0)
                        {
                            ClaimAmt = (from p in datacontext.TblEmpClaims
                                        where p.Active == true &&
                                         (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress))
                                         && p.CompanyUserId == iObj.Id select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                            if (ClaimAmt != 0)
                            {
                                GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                                {
                                    Type = "Claim",
                                    Amount = ClaimAmt,

                                };
                                Obj.Add(EmpDashboardObj);
                            }
                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Claim",
                                Amount = ClaimAmt
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion

                        #region Balance
                        TblEmpBalance BalanceObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select p).Take(1).SingleOrDefault();

                        decimal Balance = 0;
                        if (BalanceObj != null)
                        {
                            Balance = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select Convert.ToDecimal(p.Amount ?? 0)).Sum();

                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Balance",
                                Amount = Balance < 0 ? 0 : Balance
                            };
                            Obj.Add(EmpDashboardObj);

                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Balance",
                                Amount = Balance < 0 ? 0 : Balance
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion

                        #region Adjustment
                        List<TblEmpTransaction> AdjustmentObj = (from p in datacontext.TblEmpTransactions
                                                                 where  p.PaymentStatus==Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) && p.Active == true && p.PaymentTypeId==CObjects.enumPaymentType.ADJ.ToString() && p.CompanyUserId == iObj.Id 
                                                                 select p).ToList();
                        decimal Adjustment = 0;
                        if (AdjustmentObj != null && AdjustmentObj.Count() > 0)
                        {
                            Adjustment = (from p in datacontext.TblEmpTransactions
                                          where p.Active == true && p.PaymentStatus==Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                          && p.CompanyUserId == iObj.Id && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                          select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Adjustments",
                                Amount = Adjustment
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Adjustments",
                                Amount = Adjustment < 0 ? 0 : Adjustment
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion

                        lobjResponse.GetEmpDashboardData = Obj;
                        lobjResponse.PlanDesign = PlandesignId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.PlanName = PlanName;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpDashboardData", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }


        #endregion

        #region GetEmpDetailsById
        public CGetEmpDetailsByIdResponse GetEmpDetailsById(DbConIdInput iObj)
        {
            CGetEmpDetailsByIdResponse lobjResponse = new CGetEmpDetailsByIdResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            UserId = iObj.Id;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        GetEmpBasicDetails Obj = (from p in datacontext.TblEmployees
                                                  join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                  where p.Active == true && p.CompanyUserId == iObj.Id && q.Active == true
                                                  select new GetEmpBasicDetails()
                                                  {
                                                      UserName = q.UserName == null ? "" : q.UserName,
                                                      FirstName = p.FirstName == null ? "" : p.FirstName,
                                                      LastName = p.LastName  == null ? "" :p.LastName,
                                                      EmailAddress = p.EmailAddress  == null ? "" :p.EmailAddress,
                                                      DOB = Convert.ToString(p.DOB)  == null ? "" :Convert.ToString(p.DOB),
                                                      Gender = p.Gender  == null ? "" :p.Gender,
                                                      MobileNo = p.MobileNo  == null ? "" :p.MobileNo,
                                                      EveningPhone = p.EveningPhone  == null ? "" :p.EveningPhone,
                                                      MorningPhone = p.MorningPhone == null ? "" :p.MorningPhone,
                                                      Address = p.Address1 == null ? "" : p.Address1,
                                                      Address2 = p.Address2 == null ? "" :p.Address2,
                                                      City = p.City == null ? "" :p.City,
                                                      Province = p.Province == null ? "" : p.Province ,
                                                      PostalCode = p.PostalCode == null ? "" :p.PostalCode,
                                                      IsActive = Convert.ToBoolean(p.IsActive),
                                                      InActiveDate = Convert.ToString(p.InActiveDate) == null ? "" :Convert.ToString(p.InActiveDate),
                                                      CellNotificationDetails = (from a in datacontext.TblUserCarriers
                                                                                 where a.Active == true
                                                                                     && a.CompanyUserId == p.CompanyUserId
                                                                                 select new CellNotificationDetails()
                                                                                 {
                                                                                     CarrierName = Convert.ToString(a.CarrierID),
                                                                                     ContactNo = a.ContactNo,
                                                                                     IsNotify = Convert.ToString(a.IsSMS),
                                                                                 }).Take(1).SingleOrDefault(),
                                                  }).Take(1).SingleOrDefault();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.GetEmpBasicDetails = Obj;
                        lobjResponse.Message = "SUCCESS";
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpDetailsById", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region UpdateEmpBasicDetails
        public CResponse UpdateEmpBasicDetails(UpdateEmpBasicDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            UserId = iObj.CompanyUserId;
            bool IsLogOut = false;
            string EmpMoblieNo = "";
                                      
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    decimal RandomCode = getRandomID(mastercontext, iObj.DbCon, iObj.CompanyUserId);
                    string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                    string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                    string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                    Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                    CMail Mail = new CMail();
                                                         
                    
                        decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);

                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId  select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            EmpMoblieNo = UserObj.ContactNo;

                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                            UserObj.FirstName = iObj.FirstName;
                            UserObj.LastName = iObj.LastName;
                            UserObj.ContactNo = iObj.MobileNo;
                            UserObj.EmailAddress = iObj.EmailAddress;
                            TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (EmpObj != null)
                            {
                                EmpObj.FirstName = iObj.FirstName;
                                EmpObj.LastName = iObj.LastName;
                                EmpObj.EmailAddress = iObj.EmailAddress;
                                EmpObj.DOB = Convert.ToDateTime(iObj.DOB);
                                EmpObj.Gender = iObj.Gender;
                                EmpObj.MobileNo = iObj.MobileNo;
                                EmpObj.EveningPhone = iObj.EveningPhone;
                                EmpObj.MorningPhone = iObj.MorningPhone;
                                EmpObj.Address1 = iObj.Address;
                                EmpObj.Address2 = iObj.Address2;
                                EmpObj.City = iObj.City;
                                EmpObj.Province = iObj.Province;
                                EmpObj.PostalCode = iObj.PostalCode;
                                EmpObj.HomePhone = iObj.HomePhone;
                                EmpObj.BusinessPhone = iObj.BusinessPhone;
                                if (iObj.Sin != -1)
                                {
                                    EmpObj.SIN = iObj.Sin;
                                }
                                datacontext.SubmitChanges();
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    if (!string.IsNullOrEmpty(iObj.Carrier) && iObj.Carrier !="-1")
                                    {
                                        TblCarrier CarrierObj1 = (from p in mastercontext.TblCarriers where p.Active == true && p.CarrierId == Convert.ToDecimal(iObj.Carrier) select p).Take(1).SingleOrDefault();

                                        if (CarrierObj1 != null)
                                        {
                                            CarrierObj.ContactNo = iObj.MobileNo;
                                            CarrierObj.IsSMS = iObj.IsNotify == "" ? false : Convert.ToBoolean(iObj.IsNotify);
                                            
                                            CarrierObj.Carrier = CarrierObj1.Carrier;
                                            CarrierObj.CarrierID = CarrierObj1.CarrierId;

                                            if (EmpMoblieNo != iObj.MobileNo)
                                            {
                                                #region SendOTPMail



                                                string lstrMessage = "";
                                                StringBuilder builder = new StringBuilder();
                                                builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                                                lstrMessage = builder.ToString();
                                                //send mail

                                                string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
                                                string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

                                                TwilioClient.Init(AccountSid, AuthToken);
                                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                                                   | SecurityProtocolType.Tls11
                                                                                   | SecurityProtocolType.Tls12
                                                                                   | SecurityProtocolType.Ssl3;
                                                var message = MessageResource.Create(
                                                    body: lstrMessage,
                                                    from: new Twilio.Types.PhoneNumber("+12897993854"),
                                                    to: new Twilio.Types.PhoneNumber("+1"+iObj.MobileNo)
                                                );
                                                
                                                #endregion SendMail
                                            }
                                        }


                                    }


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(iObj.Carrier) && iObj.Carrier != "-1")
                                    {
                                       TblCarrier CarrierObj1 = (from p in mastercontext.TblCarriers where p.Active == true && p.CarrierId == Convert.ToDecimal(iObj.Carrier) select p).Take(1).SingleOrDefault();
                               

                                        TblUserCarrier Obj = new TblUserCarrier()
                                        {
                                            Carrier = CarrierObj1.Carrier,
                                            CompanyUserId = iObj.CompanyUserId,
                                            IsSMS = true,
                                            ContactNo = iObj.MobileNo,
                                            IsVerified = false,
                                            RandomCode = RandomCode,
                                            Active = true,
                                            DateCreated = DateTime.UtcNow,
                                            CarrierID = CarrierObj1.CarrierId
                                        };
                                        datacontext.TblUserCarriers.InsertOnSubmit(Obj);
                                        datacontext.SubmitChanges();
                                        #region SendOTPMail



                                        string lstrMessage = "";
                                        StringBuilder builder = new StringBuilder();
                                        builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                                        lstrMessage = builder.ToString();

                                        string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
                                        string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

                                        TwilioClient.Init(AccountSid, AuthToken);
                                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                                           | SecurityProtocolType.Tls11
                                                                           | SecurityProtocolType.Tls12
                                                                           | SecurityProtocolType.Ssl3;
                                        var message = MessageResource.Create(
                                            body: lstrMessage,
                                            from: new Twilio.Types.PhoneNumber("+12897993854"),
                                            to: new Twilio.Types.PhoneNumber("+1"+iObj.MobileNo)
                                        );
                                        

                                        #endregion SendMail
                                    }
                                }
                            }
                            UpdateMsgUser(UserObj, "Update", CompanyId);

                            #region Update Details in Berkely

                            TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.CompanyUserId && p.CompanyId== CompanyId select p).Take(1).SingleOrDefault();
                            if (CardObj != null)
                            {
                                string SessionId = GetUserSessionId();
                                if (SessionId != null && SessionId != "")
                                {
                                    string UpdateDemographicDetailsUri = BaseUrl + "/UpdateDemographicInfoInstant/?sessionID=" + SessionId + "&ProgramID=" + ProgramId;
                                    HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(UpdateDemographicDetailsUri)) as HttpWebRequest;
                                    webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                    webRequest2.ContentType = "application/json; charset=utf-8";
                                    string data = "{\"ID\":\"" + CardObj.UniqueId + "\",\"CardHolderDetails\":{\"AddressLine1\":\"" + iObj.Address + "\",\"AddressLine2\":\"" + iObj.Address2 + "\",\"BusinessPhone\":\"" + iObj.BusinessPhone + "\",\"CardHolderID\":\"\",\"City\":\"" + iObj.City + "\",\"Company\":\"" + CompanyObj.CompanyName + "\",\"CountryCode\":\"CA\",\"CountrySubdivisionCode\":\"ON\",\"Email\":\"" + iObj.EmailAddress + "\",\"FirstName\":\"" + iObj.FirstName + "\",\"HomePhone\":\"" + iObj.HomePhone + "\",\"LastName\":\"" + iObj.LastName + "\",\"PostalCode\":\"" + iObj.PostalCode + "\",\"SIN\":\"" + CardObj.SIN + "\"},\"Reference\":{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                    byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                    // Set the content length in the request headers  
                                    webRequest2.ContentLength = byteData.Length;
                                    try
                                    {
                                        //// Write data  
                                        using (Stream postStream = webRequest2.GetRequestStream())
                                        {
                                            postStream.Write(byteData, 0, byteData.Length);
                                        }

                                        // Get response  
                                        using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                        {
                                            StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                            string text3 = sr2.ReadToEnd().ToString();
                                            dynamic UpdateDetailsRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                            if (UpdateDetailsRes.Result["Success"] == true)
                                            {
                                                datacontext.SubmitChanges();
                                                lobjResponse.Message = "SUCCESS";
                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                            }
                                            else
                                            {
                                                CreateCardApiResponseLog(iObj.CompanyUserId, "UpdateDemographicInfoInstant", "FAILURE", UpdateDetailsRes.Result["Success"].ToString(), CompanyId, -1, UpdateDetailsRes.Result["ErrorMessage"].ToString(), InputJson);

                                                lobjResponse.Message = UpdateDetailsRes.Result["ErrorMessage"].ToString();
                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        lobjResponse.Message = ex.Message;
                                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                    }
                                }
                                else
                                {
                                    CreateCardApiResponseLog(iObj.CompanyUserId, "Login", "FAILURE", SessionId, CompanyId, -1, SessionId, InputJson);
                                    lobjResponse.Message = "The service is not available.";
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                               
                                datacontext.SubmitChanges();
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                datacontext.SubmitChanges();
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            #endregion Update Details in Berkely
                           
                        }
                        else
                        {
                            lobjResponse.Message = "User doesn't exist";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, iObj.CompanyUserId, "UpdateEmpBasicDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddEmpDependents
        public CResponse AddEmpDependents(AddEmpDependentsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            string ErrorString = "";
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
            
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    
                        if (iObj.EmpDependentList != null && iObj.EmpDependentList.Count() > 0)
                        {
                            foreach (EmpDependentList item in iObj.EmpDependentList)
                            {
                                TblEmployeeDependant Obj = (from p in datacontext.TblEmployeeDependants where p.Active == true && p.CompanyUserId == item.CompanyUserId && p.FirstName == item.FName && p.DOB == Convert.ToDateTime(item.DOB) select p).Take(1).SingleOrDefault();
                                if (Obj == null)
                                {
                                    TblEmployeeDependant DependantObj = new TblEmployeeDependant()
                                    {
                                        CompanyUserId = item.CompanyUserId,
                                        FirstName = item.FName,
                                        LastName = item.LName,
                                        RelationshipId = item.RelationshipId,
                                        Gender = item.Gender,
                                        DOB = Convert.ToDateTime(item.DOB),
                                        OtherRelation = item.OtherRelation,
                                        Active = true,
                                        DateCreated = DateTime.UtcNow,
                                        DateModified = DateTime.UtcNow
                                    };
                                    datacontext.TblEmployeeDependants.InsertOnSubmit(DependantObj);
                                    datacontext.SubmitChanges();

                                    List<TblNotificationDetail> NotificationUserList = null;
                                    string PushNotifyText = "Your dependant " + item.FName + " " + item.LName + " has been added succcessfully.";
                                    NotifyUser1(datacontext,PushNotifyText,DependantObj.CompanyUserId, CObjects.enmNotifyType.Dependant.ToString());
                                    NotificationUserList = InsertionInNotificationTable(DependantObj.CompanyUserId, PushNotifyText, datacontext, DependantObj.CompanyUserId, CObjects.enmNotifyType.Dependant.ToString());
                                    if (NotificationUserList != null)
                                    {
                                        NotificationList.AddRange(NotificationUserList);
                                        NotificationUserList.Clear();
                                    }


                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                    lobjResponse.Message = "SUCCESS";
                                    lobjResponse.ID = DependantObj.DependantId;
                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "Already exist.";
                                }

                            }

                            #region Push Notifications


                            if (NotificationList != null && NotificationList.Count() > 0)
                            {
                                SendNotification(datacontext, NotificationList, UserId);
                                NotificationList.Clear();

                            }
                            #endregion Push Notifications

                        }
                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "UpdateEmpBasicDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion AddEmpDependents

        #region GetEmpDependentDetails
        public CGetEmpDependentDetailsResponse GetEmpDependentDetails(DbConLoggedInUIdInput iObj)
        {
            CGetEmpDependentDetailsResponse lobjResponse = new CGetEmpDependentDetailsResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        EmpDependentsListDetails Obj = (from p in datacontext.TblEmployeeDependants
                                                        where p.Active == true && p.DependantId == iObj.Id
                                                        select new EmpDependentsListDetails()
                                                        {
                                                            DependentId = p.DependantId,
                                                            FName = p.FirstName,
                                                            LName = p.LastName,
                                                            RelationshipId = Convert.ToDecimal(p.RelationshipId),
                                                            Gender = p.Gender,
                                                            DOB = Convert.ToString(p.DOB),
                                                            OtherRelation = p.OtherRelation
                                                        }).Take(1).SingleOrDefault();
                        lobjResponse.EmpDependentsListDetails = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpDependentDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;

        }
        #endregion GetEmpDependentDetails

        #region EditEmpDependents
        public CResponse EditEmpDependents(EditEmpDependentsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.CompanyUserId;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        TblEmployeeDependant Obj = (from p in datacontext.TblEmployeeDependants where p.Active == true && p.DependantId == iObj.DependentId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            TblEmployeeDependant Obj1 = (from p in datacontext.TblEmployeeDependants where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.FirstName == iObj.FName && p.DOB == Convert.ToDateTime(iObj.DOB) select p).Take(1).SingleOrDefault();
                            if (Obj1 == null)
                            {

                                Obj.CompanyUserId = iObj.CompanyUserId;
                                Obj.FirstName = iObj.FName;
                                Obj.LastName = iObj.LName;
                                Obj.RelationshipId = iObj.RelationshipId;
                                Obj.Gender = iObj.Gender;
                                Obj.OtherRelation = iObj.OtherRelation;
                                Obj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "SUCCESS";
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Dependent already exists.";
                            }
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "No record found.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "EditEmpDependents", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }

        #endregion

        #region DeleteDependents
        public CResponse DeleteDependents(DbConLoggedInUIdInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    
                        TblEmployeeDependant Obj = (from p in datacontext.TblEmployeeDependants where p.Active == true && p.DependantId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            Obj.Active = false;
                            Obj.DateModified = DateTime.UtcNow;
                            datacontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                        }
                    
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "DeleteDependents", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion DeleteDependents

        #region ListOfDependents
        public CListOfDependentsResponse ListOfDependents(LazyLoadingSearchInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CListOfDependentsResponse lobjResponse = new CListOfDependentsResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {

                        List<ListOfDependents> Obj = (from p in datacontext.sp_ListOfDependents(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString, iObj.Id)
                                                      select new ListOfDependents()
                                                      {
                                                          DependentId = Convert.ToDecimal(p.DependantId),
                                                          FName = p.FirstName,
                                                          LName = p.LastName,
                                                          Relationship = p.Title,
                                                          RelationShipId = Convert.ToDecimal(p.RelationshipId),
                                                          Gender = p.Gender,
                                                          OtherRelation = p.OtherRelation,
                                                          
                                                          DOB = Convert.ToDateTime(p.DOB).ToString("MM/dd/yyyy"),
                                                      }).ToList();

                        lobjResponse.ListOfDependents = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                     
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "ListOfDependents", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region PopulateDependants
        public CPopulateDependantsResponse PopulateDependants(DbConIdInput iObj)
        {
            CPopulateDependantsResponse lobjResponse = new CPopulateDependantsResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    List<PopulateDependants> Obj = (from p in datacontext.TblEmployeeDependants
                                                    where p.Active == true && p.CompanyUserId == iObj.Id
                                                    select new PopulateDependants()
                                                    {
                                                        FirstName = p.FirstName,
                                                        LastName = p.LastName,
                                                        DependantId = p.DependantId
                                                    }).ToList();
                    lobjResponse.PopulateDependants = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "PopulateDependants", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion PopulateDependants

        #region PopulateClaimType
        public CPopulateClaimTypeResponse PopulateClaimType(DbConInput iObj)
        {
            CPopulateClaimTypeResponse lobjResponse = new CPopulateClaimTypeResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    List<PopulateClaimType> Obj = (from p in datacontext.TblClaimTypes
                                                   where p.Active == true
                                                   select new PopulateClaimType()
                                                   {
                                                       ClaimTypeId = p.ClaimTypeId,
                                                       ClaimType = p.Title,
                                                   }).ToList();
                    lobjResponse.PopulateClaimType = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                CreateResponseLog(datacontext, StartTime, -1, "PopulateClaimType", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion PopulateClaimType

        #region AddClaim commented
        public CResponse AddClaim1(AddClaimInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            
            return lobjResponse;
        }
        #endregion

        #region ListOfClaims
        public CListOfClaimsResponse ListOfClaims(ListOfClaimsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CListOfClaimsResponse lobjResponse = new CListOfClaimsResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    MasterDBDataContext mastercontext = new MasterDBDataContext();
                    if (IsValidateUser(datacontext))
                    {
                        DateTime? FromDate = null;
                        DateTime? ToDate = null;

                        if (iObj.FromDate != "")
                        {
                            FromDate = Convert.ToDateTime(iObj.FromDate);
                        }
                        if (iObj.ToDate != "")
                        {
                            ToDate = Convert.ToDateTime(iObj.ToDate);
                        }
                        decimal ClaimCount = (from p in datacontext.TblEmpClaims where p.Active == true && 
                                                  (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid)) && p.CompanyUserId == iObj.Id select p).Count();
                        decimal TotalClaimed = 0;
                        if (ClaimCount > 0)
                        {
                            TotalClaimed = (from p in datacontext.TblEmpClaims where p.Active == true && 
                                           (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid)) && p.CompanyUserId == iObj.Id select Convert.ToDecimal(p.ClaimAmt ?? 0)).Sum();
                        }
                        List<ListOfClaimsData> Obj = (from p in datacontext.sp_ListOfClaims(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString, iObj.Id, FromDate, ToDate)
                                                      select new ListOfClaimsData()
                                                      {
                                                          EmpClaimId = p.EmpClaimId,
                                                          CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                          ReceiptDate = Convert.ToString(p.ReceiptDate),
                                                          ReceiptNo = p.ReceiptNo,
                                                          UploadFileName = p.UploadFileName,
                                                          ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                          ReimburseableAmt = p.ReimburseableAmt == null ? 0 : Convert.ToDecimal(p.ReimburseableAmt),
                                                          Dependants = p.Dependant,
                                                          ClainStatus = p.Title,
                                                          ClaimDate = Convert.ToString(p.DateCreated),
                                                          CRA = Convert.ToDecimal(p.CRA),
                                                          NonCRA = Convert.ToDecimal(p.NonCRA),
                                                          Note = p.Note,
                                                          IsManual = Convert.ToBoolean(p.IsManual),
                                                          IsExpired = Convert.ToBoolean(p.IsExpired),
                                                          ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                                 join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                                 where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                                 select new ClaimServiceDetails()
                                                                                 {
                                                                                      ServiceName = a.Title,
                                                                                      ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                      Address = q.Address,
                                                                                      PhoneNo = q.ContactNo,
                                                                                      Email = q.Email,
                                                                                      ServiceProvider = q.Name
                                                                                 }).Take(1).SingleOrDefault()
                                                      }).ToList();
                        List<TblEmpClaim> ClaimList = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.Id && (p.UploadFileName == null || p.UploadFileName == "") select p).ToList();
                        if (ClaimList.Count() > 0 && ClaimList != null)
                        {
                            lobjResponse.IsReceipt = false;
                        }
                        else
                        {
                            lobjResponse.IsReceipt = true;
                        }
                        lobjResponse.ListOfClaimsData = Obj;
                        lobjResponse.ClaimedAmt = TotalClaimed;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "ListOfClaims", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion ListOfClaims

        #region AddUserCarrier
        public CAddUserCarrierResponse AddUserCarrier(AddUserCarrierInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CAddUserCarrierResponse lobjResponse = new CAddUserCarrierResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {
                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                                
                        if (iObj.IsSMS)
                        {
                            decimal RandomCode = getRandomID(mastercontext, iObj.DbCon, iObj.LoggedInUserId);
                            string Carrier = (from p in mastercontext.TblCarriers where p.Active == true && p.CarrierId == Convert.ToDecimal(iObj.Carrier) select p.Carrier).Take(1).SingleOrDefault();
                            TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                             if (CarrierObj == null)
                             {

                                 TblUserCarrier Obj = new TblUserCarrier()
                                 {
                                     CarrierID = Convert.ToDecimal(iObj.Carrier),
                                     Carrier = Carrier,
                                     CompanyUserId = iObj.LoggedInUserId,
                                     IsSMS = iObj.IsSMS,
                                     ContactNo = iObj.ContactNo,
                                     IsVerified = false,
                                     RandomCode = RandomCode,
                                     Active = true,
                                     DateCreated = DateTime.UtcNow,
                                 };
                                 datacontext.TblUserCarriers.InsertOnSubmit(Obj);

                                 TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                                 if (EmpObj != null)
                                 {
                                     EmpObj.MobileNo = iObj.ContactNo;
                                 }
                                 if (UserObj != null)
                                 {
                                     UserObj.ContactNo = iObj.ContactNo;
                                   
                                 }

                                 datacontext.SubmitChanges();

                                 #region SendOTPMail



                                 string lstrMessage = "";
                                 StringBuilder builder = new StringBuilder();
                                 builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                                 lstrMessage = builder.ToString();

                                 string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
                                 string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

                                 TwilioClient.Init(AccountSid, AuthToken);
                                 ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                                    | SecurityProtocolType.Tls11
                                                                    | SecurityProtocolType.Tls12
                                                                    | SecurityProtocolType.Ssl3;
                                 var message = MessageResource.Create(
                                     body: lstrMessage,
                                     from: new Twilio.Types.PhoneNumber("+12897993854"),
                                     to: new Twilio.Types.PhoneNumber("+1"+iObj.ContactNo)
                                 );
                                 //send mail
                                 string EmailTo = iObj.ContactNo + "@" + Carrier;
                                 string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                 string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                 string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                 Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                 CMail Mail = new CMail();
                                 


                                 #endregion SendMail
                             }
                             else
                             {
                                 CarrierObj.ContactNo = iObj.ContactNo;
                                 CarrierObj.IsSMS = Convert.ToBoolean(iObj.IsSMS);
                                 CarrierObj.Carrier = Carrier;
                                 CarrierObj.CarrierID = Convert.ToDecimal(iObj.Carrier);
                             }
                        }
                        if (UserObj != null)
                        {
                            UserObj.NotifyType = iObj.NotifyType;
                        }
                        datacontext.SubmitChanges();
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                       
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime,iObj.LoggedInUserId, "AddUserCarrier", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ValidatePasscode
        /// <summary>
        /// Author:Jasmeet kaur
        /// Date:1580618
        /// Function used to validate passcode.
        /// </summary>
        /// <param name="iObj"></param>
        /// <returns></returns>
        public CResponse ValidatePasscode(ValidatePasscodeInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                {
                    try
                    {
                        TblRandomCodeLog RandomCodeObj;
                        RandomCodeObj = (from p in mastercontext.TblRandomCodeLogs where p.Active == true && p.RandomCode == iObj.OTP && p.ValidTill >= DateTime.UtcNow select p).Take(1).SingleOrDefault();
                        if (RandomCodeObj != null)
                        {
                            string Constring = GetConnection(iObj.DbCon);
                            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                                {
                                    TblUserCarrier UserObj;
                                    UserObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == Convert.ToDecimal(RandomCodeObj.UserId) && p.RandomCode == Convert.ToDecimal(iObj.OTP) select p).Take(1).SingleOrDefault();
                                    if (UserObj != null)
                                    {
                                        UserObj.IsVerified = true;
                                        datacontext.SubmitChanges();
                                        lobjResponse.Message = "SUCCESS";
                                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                      
                                    }
                                    else
                                    {
                                        lobjResponse.Message = "Please enter valid OTP.";
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                                    }
                                    datacontext.Connection.Close();
                                }
                         
                        }
                        else
                        {
                            lobjResponse.Message = "Please regenerate code. Your OTP is expired.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    mastercontext.Connection.Close();
                }
           
            

            return lobjResponse;
        }
        #endregion ValidatePasscode

        #region ResentOTP
        public CResponse ResentOTP(DbConIdInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.Id;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateUser(datacontext))
                    {

                        TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == iObj.Id select p).Take(1).SingleOrDefault();
                        if(CarrierObj != null)
                        {
                            decimal RandomCode = getRandomID(mastercontext, iObj.DbCon, iObj.Id);

                            CarrierObj.IsVerified = false;
                            CarrierObj.RandomCode = RandomCode;


                            #region SendOTPMail



                            string lstrMessage = "";
                            StringBuilder builder = new StringBuilder();
                            builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                            lstrMessage = builder.ToString();

                            string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
                            string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

                            TwilioClient.Init(AccountSid, AuthToken);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                               | SecurityProtocolType.Tls11
                                                               | SecurityProtocolType.Tls12
                                                               | SecurityProtocolType.Ssl3;
                            var message = MessageResource.Create(
                                body: lstrMessage,
                                from: new Twilio.Types.PhoneNumber("+12897993854"),
                                to: new Twilio.Types.PhoneNumber("+1" + CarrierObj.ContactNo)
                            );
                            //send mail
                            string EmailTo = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                            CMail Mail = new CMail();
                            
                            #endregion SendMail

                            datacontext.SubmitChanges();
                        
                        }
                           

                          
                         

                         
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message = CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, iObj.Id, "ResentOTP", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

      
        #region AddEmpAdjustments
        public CResponse AddEmpAdjustments(AddEmpAdjustmentsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            decimal Amount = 0;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                    {
                       
                        TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                        {
                            CompanyUserId = iObj.CompanyUserId,
                            Amount = iObj.Amount,
                            AdjustmentDT = Convert.ToDateTime(iObj.AdjustmentDT),
                            Remarks = iObj.Remarks,
                            TypeId = Convert.ToInt16(iObj.TypeId),
                            Active = true,
                            DateCreated = DateTime.UtcNow,
                            DateModified = DateTime.UtcNow
                        };
                        datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                        if(iObj.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit))
                        {
                            Amount = (iObj.Amount);

                            TblEmpTransaction EmpTransObj = (from p in datacontext.TblEmpTransactions
                                                             where p.Active == true &&
                                                                 p.TransactionDetails == "Account Set-up Fee" && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                 && p.CompanyUserId == iObj.CompanyUserId
                                                             select p).Take(1).SingleOrDefault();
                            if (EmpTransObj != null)
                            {
                                if (Amount >= EmpTransObj.Amount)
                                {
                                    EmpTransObj.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                                }
                            }

                        }
                        else
                        {
                            Amount = -(iObj.Amount);
                        }

                        TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                        {
                            TransactionDate = Convert.ToDateTime(iObj.AdjustmentDT),
                            TransactionDetails = iObj.Remarks,
                            Amount = iObj.Amount,
                            CompanyUserId = iObj.CompanyUserId,
                            CompanyPlanId = -1,
                            ClaimId = -1,
                            TypeId = Convert.ToInt16(iObj.TypeId),
                            PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                            PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                            Active = true,
                            DateCreated = DateTime.UtcNow
                        };
                        datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                        TblEmpBalance EmpBalance = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if(EmpBalance != null)
                        {
                            decimal Balance = Convert.ToDecimal(EmpBalance.Amount);
                            EmpBalance.Amount = Balance + Amount;
                            EmpBalance.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                        }
                            
                        #region Notify code
                        string NotifyText = "";
                        if (iObj.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit))
                        {
                            NotifyText = "$" + Amount + " was deposited to your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ".";
                        }
                        else
                        {
                            NotifyText = "$" + Amount + " was withdrawn from your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ".";
                        }
                        NotifyUser1(datacontext, NotifyText, iObj.CompanyUserId, CObjects.enmNotifyType.Adjustment.ToString());
                       
                      
                        datacontext.SubmitChanges();
                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                            if (CarrierObj != null)
                            {
                                string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                string lstrMessage = "";
                                StringBuilder builder = new StringBuilder();
                                builder.Append(NotifyText + "<br/><br/>");
                               
                                lstrMessage = builder.ToString();
                                //send mail

                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = CarrierEmails;
                                Mail.Subject = "Your Adjustment";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);

                            }


                            #region Push Notifications
                            List<TblNotificationDetail> NotificationList = null;
                            NotificationList = InsertionInNotificationTable(iObj.CompanyUserId, NotifyText, datacontext, AdjObj.EmpAdjustmentId, CObjects.enmNotifyType.Adjustment.ToString());
                         

                            datacontext.SubmitChanges();

                            if (NotificationList != null && NotificationList.Count() > 0)
                            {
                                SendNotification(datacontext, NotificationList, AdjObj.EmpAdjustmentId);
                                NotificationList.Clear();

                            }
                            #endregion Push Notifications
                        }
                        #endregion
                   
                          

                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            datacontext.Connection.Close();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;

                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "AddEmpAdjustments", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            
               
         
            return lobjResponse;
        }
        #endregion

        #region ListEmpAllTransactions
        public CListEmpAllTransactionsResponse ListEmpAllTransactions(ListEmpAllTransactionsInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            CListEmpAllTransactionsResponse lobjResponse = new CListEmpAllTransactionsResponse();
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {

                    if (IsValidateEflexUser(mastercontext))
                    {
                        DateTime? FromDT = null;
                        DateTime? ToDT = null;
                        if (iObj.FromDt != null || iObj.FromDt != "")
                        {
                            FromDT = Convert.ToDateTime(iObj.FromDt);
                        }
                        if (iObj.ToDt != null || iObj.ToDt != "")
                        {
                            ToDT = Convert.ToDateTime(iObj.ToDt);
                        }

                        List<EmpAllTransactionsData> Obj = (from p in datacontext.sp_ListEmpAllTransactions(iObj.ChunckStart, iObj.ChunckSize, iObj.SearchString, FromDT, ToDT, iObj.CompUserId)
                                                             select new EmpAllTransactionsData()
                                                             {
                                                                 RowId = Convert.ToDecimal(p.RowId),
                                                                 TransactionId = p.EmpTransactionId,
                                                                 EmpName = p.EmpName,
                                                                 TransactionDate = Convert.ToString(p.TransactionDate),
                                                                 Amount = Convert.ToDecimal(p.Amount),
                                                                 TransactionDetails = p.TransactionDetails,
                                                                 PaymentStatus = Convert.ToDecimal(p.PaymentStatus),
                                                                 PaymentType = p.PaymentTypeId,
                                                                 Type = p.TypeId == 1 ? "Credit" : "Debit"
                                                             }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.EmpAllTransactionsData = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetEmpCardList
        public CGetEmpCardListResponse GetEmpCardList(DbConLoggedInUIdInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            CGetEmpCardListResponse lobjResponse = new CGetEmpCardListResponse();
            string Constring = GetConnection(iObj.DbCon);
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
                    if (IsValidateEflexUser(mastercontext))
                    {

                        TblMasterCard MasterCardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.Id && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if(MasterCardObj != null)
                        {
                            if (MasterCardObj.IsActive == true)
                            {
                                lobjResponse.AssignNewCard = false;
                            }
                            else
                            {
                                lobjResponse.AssignNewCard = true;
                            }
                        }
                        else
                        {
                            lobjResponse.AssignNewCard = false;
                        }
                        List<GetEmpCardList> Obj = (from p in  mastercontext.TblMasterCards where p.Active == true 
                                                    && p.UserId == iObj.Id && p.CompanyId== CompanyId
                                                    select new GetEmpCardList()
                                                            {
                                                                 MasterCardId = p.MasterCardId,
                                                                 SessionId = p.SessionId,
                                                                 UniqueId = p.UniqueId,
                                                                 OrderItemId = p.OrderItemId,
                                                                 OrderId = p.OrderId,
                                                                 AccountId = p.AccountId,
                                                                 CardId = p.CardId,
                                                                 CardHolderId = p.CardHolderId,
                                                                 CountryCode = p.CountryCode,
                                                                 CountrySubDivisionCode = p.CountrySubDivisionCode,
                                                                 FourthLine = p.FourthLine,
                                                                 UserId = Convert.ToDecimal(p.UserId),
                                                                 CompanyId = Convert.ToDecimal(p.CompanyId),
                                                                 CourierShipping = Convert.ToDecimal(p.CourierShipping),
                                                                 Quantity = Convert.ToDecimal(p.Quantity),
                                                                 IsActive = Convert.ToString(p.IsActive),
                                                                 DateCreated = Convert.ToString(p.DateCreated),
                                                                 BlockDate = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                                                 CardStatus = p.CardStatus,
                                                                 CardNumber = p.CardNumber,
                                                                 CVV = p.CVV == null ? -1 : Convert.ToDecimal(p.CVV),
                                                                 ExpMonth = p.ExpMonth == null ? -1 : Convert.ToDecimal(p.ExpMonth),
                                                                 ExpYear = p.ExpYear == null ? -1 : Convert.ToDecimal(p.ExpYear),
                                                                 BlockReason = p.BlockReason,
                                                                 BlockDT = p.BlockDate == null ? "" : Convert.ToString(p.BlockDate),
                                                                 EmpName = GetEmpName(p.CompanyId, p.UserId),
                                                                 ExpiryDt = p.ExpiryDt == null ? "" : Convert.ToString(p.ExpiryDt),
                                                                 LastFourDigit = p.LastFourDigitOfCard == null ? -1: Convert.ToDecimal(p.LastFourDigitOfCard),
                                                                 AccountSummaryData = GetAccountData(mastercontext, p.UserId, p.AccountId, p.MasterCardId,p.CompanyId)
                                                           
                                                            }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.GetEmpCardList = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      
                    }
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddClaim
        public CLoadValueResponse AddClaimV1(AddClaimInput iObj)
        {
            CLoadValueResponse lobjResponse = new CLoadValueResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsSuccess = false;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            decimal ClaimId = -1;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                try
                {
                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                    DateTime PlanStartDate = (from p in datacontext.TblCompanyPlans join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId where p.Active == true && q.Active == true && q.CompanyUserId == iObj.LoggedInUserId select Convert.ToDateTime(p.PlanStartDt)).Take(1).SingleOrDefault();
                    if (IsValidateUser(datacontext))
                    {
                        string Text = "A claim request has been submitted by an employee named as " + UserObj.FirstName + " " + UserObj.LastName + " on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                        string NotiText = "Your claim request has been received for amount $" + iObj.ClaimAmount + ". For further queries you can contact with ADMIN.";
                                                            
                                decimal ClaimAmt = 0;
                                decimal ClaimFee = 0;
                                decimal BalanceAmount =0;
                                decimal CheckAmount = 0;
                                bool IsClaimFeeCharge = false;
                                TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                if (EmpPayment != null)
                                {
                                    decimal ServiceProviderId = -1;
                                    #region Add ServiceProvider
                                    if (iObj.ServiceProviderId == -1)
                                    {
                                        TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Address.ToUpper() == iObj.ProviderAddress.ToUpper() select p).Take(1).SingleOrDefault();
                                        if (ObjExists == null)
                                        {
                                            TblServiceProvider Obj = new TblServiceProvider()
                                            {
                                                Name = iObj.ServiceProvider,
                                                Address = iObj.ProviderAddress,
                                                ContactNo = iObj.ProviderContactNo,
                                                Email = iObj.ProviderEmail,
                                                ServiceId = iObj.ServiceId,
                                                CompanyId = CompanyId,
                                                GoogleId = iObj.GoogleUniqueId,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                DateModified = DateTime.UtcNow
                                            };
                                            mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                            mastercontext.SubmitChanges();
                                            ServiceProviderId = Obj.ServiceProviderId;


                                        }
                                        else
                                        {
                                            ServiceProviderId = ObjExists.ServiceProviderId;

                                        }
                                    }
                                    else
                                    {
                                        ServiceProviderId = iObj.ServiceProviderId;

                                    }
                                    #endregion

                                  
                                    if (Convert.ToDateTime(iObj.ReceiptDate).Date > PlanStartDate.Date || Convert.ToDateTime(iObj.ReceiptDate).ToString("MM/dd/yyyy") == PlanStartDate.ToString("MM/dd/yyyy"))
                                    {


                                        TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.ReceiptNo == iObj.ReceiptNo  select p).Take(1).SingleOrDefault();
                                         if (ClaimObj == null || string.IsNullOrEmpty(iObj.ReceiptNo))
                                         {

                                             #region Claim Fee Charges
                                             TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                             if (ClaimObj1 != null)
                                             {//
                                                 ClaimAmt = iObj.ClaimAmount;
                                                 BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt));
                                                 CheckAmount = iObj.ClaimAmount;
                                               
                                             }
                                             else
                                             {
                                                 TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                               where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                                   p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                               select p).Take(1).SingleOrDefault();
                                                 if (FeeDetailsObj != null)
                                                 {
                                                     IsClaimFeeCharge = true;
                                                     ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                                     ClaimAmt = iObj.ClaimAmount;
                                                     CheckAmount = iObj.ClaimAmount + ClaimFee;
                                                    
                                                 }
                                             }
                                             #endregion

                                             //check balance if balance is greater than load amount
                                             if (EmpPayment.Amount >= CheckAmount)
                                             {
                                                 TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                                           join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                                           where p.Active == true && q.Active == true && q.CompanyUserId == iObj.CompanyUserId
                                                                           select p).Take(1).SingleOrDefault();
                                                 if (PlanObj != null)
                                                 {

                                                     #region Charge Claim Fee
                                                     if (IsClaimFeeCharge)
                                                     {
                                                         
                                                         TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                         {
                                                             TransactionDate = DateTime.UtcNow,
                                                             TransactionDetails = "Claim Processing Fee",
                                                             Amount = ClaimFee,
                                                             CompanyUserId = iObj.CompanyUserId,
                                                             CompanyPlanId = -1,
                                                             ClaimId = -1,
                                                             TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                             PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                             PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                             Active = true,
                                                             DateCreated = DateTime.UtcNow
                                                         };
                                                         datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                         
                                                         TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                         {
                                                             CompanyUserId = iObj.CompanyUserId,
                                                             Amount = ClaimFee,
                                                             AdjustmentDT = DateTime.UtcNow,
                                                             Remarks = "Claim Processing Fee",
                                                             TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                             Active = true,
                                                             DateCreated = DateTime.UtcNow,
                                                             DateModified = DateTime.UtcNow
                                                         };
                                                         datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                                         BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt + ClaimFee));
                                                     }
                                                     #endregion

                                                     #region Monthly
                                                     if (PlanObj.ContributionSchedule == 1)//Monthly
                                                     {

                                                         TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                         if (CardFeeRangeObj != null)
                                                         {
                                                             List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.CompanyUserId && p.Active == true select p).ToList();
                                                             if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                             {
                                                                 //check yearly limit
                                                                 int year = DateTime.Now.Year;
                                                                 DateTime firstDay = new DateTime(year, 1, 1);
                                                                 DateTime lastDay = new DateTime(year, 12, 31);
                                                                 List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.CompanyUserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                                 if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                 {
                                                                     decimal YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                     if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                                     {
                                                                         //check monthly limit
                                                                         var now = DateTime.Now;
                                                                         var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                         var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                         List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                                         decimal MonthlyClaimAmt = 0;
                                                                         if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                         {
                                                                             MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                         }
                                                                         if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                                         {
                                                                             //check day limit
                                                                             decimal DailyClaimAmt = 0;
                                                                             List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                             if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                             {
                                                                                 DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                             }
                                                                             if (CardFeeRangeObj.DayMax > DailyClaimAmt)
                                                                             {

                                                                                 #region Add Claim

                                                                                      
                                                                                         try
                                                                                         {
                                                                                                    TblEmpClaim Obj = new TblEmpClaim()
                                                                                                     {
                                                                                                        CompanyUserId = iObj.CompanyUserId,
                                                                                                        ServiceId = iObj.ServiceId,
                                                                                                        ServiceProvider = iObj.ServiceProvider,
                                                                                                        ServiceProviderId = ServiceProviderId,
                                                                                                        ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                                        ReceiptNo = iObj.ReceiptNo,
                                                                                                        UploadFileName = iObj.UploadFileName,
                                                                                                        Amount = iObj.ClaimAmount,
                                                                                                        ClaimAmt = iObj.ClaimAmount,
                                                                                                        ClaimFee = ClaimFee,
                                                                                                        DependantId = iObj.Dependant,
                                                                                                        Note= iObj.Note,
                                                                                                        ReimburseableAmt = ClaimAmt,
                                                                                                        IsManual = iObj.IsManual,
                                                                                                        ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow,
                                                                                                        DateModified = DateTime.UtcNow


                                                                                                    };
                                                                                                    datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                                    datacontext.SubmitChanges();

                                                                                                    ClaimId = Obj.EmpClaimId;
                                                                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                                    {
                                                                                                        TransactionDate = DateTime.UtcNow,
                                                                                                      
                                                                                                        TransactionDetails = "Claim Amount",
                                                                                                        Amount = ClaimAmt,
                                                                                                        CompanyUserId = iObj.CompanyUserId,
                                                                                                        CompanyPlanId = -1,
                                                                                                        ClaimId = Obj.EmpClaimId,
                                                                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                                        PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                                        Active = true,
                                                                                                        DateCreated = DateTime.UtcNow
                                                                                                    };
                                                                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                                    
                                                                                                    EmpPayment.Amount = BalanceAmount;
                                                                                                    datacontext.SubmitChanges();
                                                                                                     

                                                                                                     CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);

                                                                                                     lobjResponse.ID = Obj.EmpClaimId;
                                                                                                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                                     IsSuccess = true;
                                                                                         }
                                                                                         catch (WebException ex)
                                                                                         {
                                                                                             lobjResponse.Message = ex.Message;
                                                                                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                             IsSuccess = false;
                                                                                         }

                                                                                  
                                                                               
                                                                                 #endregion Add Claim
                                                                             }
                                                                             else
                                                                             {
                                                                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                                 lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                                 IsSuccess = false;
                                                                             }


                                                                         }
                                                                         else
                                                                         {
                                                                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                             lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                             IsSuccess = false;
                                                                         }
                                                                         
                                                                     }
                                                                     else
                                                                     {
                                                                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                         lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                         IsSuccess = false;
                                                                     }
                                                                 }
                                                             }
                                                             else
                                                             {
                                                                 #region Add Claim
 
                                                                         try
                                                                         {
                                                                             TblEmpClaim Obj = new TblEmpClaim()
                                                                             {
                                                                                 CompanyUserId = iObj.CompanyUserId,
                                                                                 
                                                                                 ServiceId = iObj.ServiceId,
                                                                                 ServiceProvider = iObj.ServiceProvider,
                                                                                 ServiceProviderId = ServiceProviderId,
                                                                                 ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                 ReceiptNo = iObj.ReceiptNo,
                                                                                 UploadFileName = iObj.UploadFileName,
                                                                                 Amount = iObj.ClaimAmount,
                                                                                 ClaimAmt = iObj.ClaimAmount,
                                                                                 ClaimFee = ClaimFee,
                                                                                 DependantId = iObj.Dependant,
                                                                                 Note = iObj.Note,
                                                                                 ReimburseableAmt = ClaimAmt,
                                                                                 IsManual = iObj.IsManual,
                                                                                 ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                 Active = true,
                                                                                 DateCreated = DateTime.UtcNow,
                                                                                 DateModified = DateTime.UtcNow


                                                                             };
                                                                             datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                             datacontext.SubmitChanges();

                                                                             ClaimId = Obj.EmpClaimId;
                                                                             TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                             {
                                                                                 TransactionDate = DateTime.UtcNow,
                                                                                 TransactionDetails = "Claim Amount",
                                                                                 Amount = ClaimAmt,
                                                                                 CompanyUserId = iObj.CompanyUserId,
                                                                                 CompanyPlanId = -1,
                                                                                 ClaimId = Obj.EmpClaimId,
                                                                                 TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                 PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                 PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                 Active = true,
                                                                                 DateCreated = DateTime.UtcNow
                                                                             };
                                                                             datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                             
                                                                             EmpPayment.Amount = BalanceAmount;
                                                                             datacontext.SubmitChanges();
                                                                             lobjResponse.ID = Obj.EmpClaimId;

                                                                            CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);

                                                                                    
                                                                            mastercontext.SubmitChanges();

                                                                            IsSuccess = true;
                                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                               
                                                                         }
                                                                         catch (WebException ex)
                                                                         {
                                                                             lobjResponse.Message = ex.Message;
                                                                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                             IsSuccess = false;
                                                                         }

                                                               
                                                                 #endregion Add Claim
                                                             }

                                                         }
                                                         else
                                                         {
                                                             #region Add Claim

                                                             try
                                                             {
                                                                 TblEmpClaim Obj = new TblEmpClaim()
                                                                 {
                                                                     CompanyUserId = iObj.CompanyUserId,
                                                                     
                                                                     ServiceId = iObj.ServiceId,
                                                                     ServiceProvider = iObj.ServiceProvider,
                                                                     ServiceProviderId = ServiceProviderId,
                                                                     ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                     ReceiptNo = iObj.ReceiptNo,
                                                                     UploadFileName = iObj.UploadFileName,
                                                                     Amount = iObj.ClaimAmount,
                                                                     ClaimAmt = iObj.ClaimAmount,
                                                                     ClaimFee = ClaimFee,
                                                                     DependantId = iObj.Dependant,
                                                                     Note = iObj.Note,
                                                                     ReimburseableAmt = ClaimAmt,
                                                                     IsManual = iObj.IsManual,
                                                                     ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                     Active = true,
                                                                     DateCreated = DateTime.UtcNow,
                                                                     DateModified = DateTime.UtcNow


                                                                 };
                                                                 datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                 datacontext.SubmitChanges();
                                                                 ClaimId = Obj.EmpClaimId;

                                                                 TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                 {
                                                                     TransactionDate = DateTime.UtcNow,
                                                                     TransactionDetails = "Claim Amount",
                                                                     Amount = ClaimAmt,
                                                                     CompanyUserId = iObj.CompanyUserId,
                                                                     CompanyPlanId = -1,
                                                                     ClaimId = Obj.EmpClaimId,
                                                                     TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                     PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                     PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                     Active = true,
                                                                     DateCreated = DateTime.UtcNow
                                                                 };
                                                                 datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                
                                                                 EmpPayment.Amount = BalanceAmount;
                                                                 datacontext.SubmitChanges();
                                                                 

                                                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                                                                 CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);

                                                                
                                                                 mastercontext.SubmitChanges();
                                                                 lobjResponse.ID = Obj.EmpClaimId;
                                                                 IsSuccess = true;
                                                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                             }
                                                             catch (WebException ex)
                                                             {
                                                                 lobjResponse.Message = ex.Message;
                                                                 lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                 IsSuccess = false;
                                                             }


                                                             #endregion Add Claim
                                                         }
                                                     }
                                                     #endregion Monthly

                                                     #region Annual
                                                     if (PlanObj.ContributionSchedule == 2)//Annual
                                                     {

                                                         TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                         if (CardFeeRangeObj != null)
                                                         {
                                                             List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.CompanyUserId && p.Active == true select p).ToList();
                                                             if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                             {
                                                                 //check yearly limit
                                                                 int year = DateTime.Now.Year;
                                                                 DateTime firstDay = new DateTime(year, 1, 1);
                                                                 DateTime lastDay = new DateTime(year, 12, 31);
                                                                 decimal YearClaimAmt = 0;
                                                                 List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.CompanyUserId == iObj.CompanyUserId && p.Active == true && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDay).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDay).Date) select p).ToList();
                                                                 if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                                 {
                                                                     YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                 }

                                                                 if (CardFeeRangeObj.OverAllMax > YearClaimAmt)
                                                                 {
                                                                     //check monthly limit
                                                                     var now = DateTime.Now;
                                                                     var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                     var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                     decimal MonthlyClaimAmt = 0;
                                                                     List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date) select p).ToList();
                                                                     if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                     {
                                                                         MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                     }
                                                                     if (CardFeeRangeObj.MonthlyMax > MonthlyClaimAmt)
                                                                     {
                                                                         //check day limit
                                                                         decimal DailyClaimAmt = 0;
                                                                         List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).ToList();
                                                                         if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                         {
                                                                             DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                         }
                                                                         if (CardFeeRangeObj.DayMax > MonthlyClaimAmt)
                                                                         {

                                                                             #region Add Claim
    
                                                                                     try
                                                                                     {

                                                                                         TblEmpClaim Obj = new TblEmpClaim()
                                                                                         {
                                                                                             CompanyUserId = iObj.CompanyUserId,
                                                                                             
                                                                                             ServiceId = iObj.ServiceId,
                                                                                             ServiceProvider = iObj.ServiceProvider,
                                                                                             ServiceProviderId = ServiceProviderId,
                                                                                             ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                             ReceiptNo = iObj.ReceiptNo,
                                                                                             UploadFileName = iObj.UploadFileName,
                                                                                             Amount = iObj.ClaimAmount,
                                                                                             ClaimAmt = iObj.ClaimAmount,
                                                                                             ClaimFee = ClaimFee,
                                                                                             DependantId = iObj.Dependant,
                                                                                             Note = iObj.Note,
                                                                                             ReimburseableAmt = ClaimAmt,
                                                                                             IsManual = iObj.IsManual,
                                                                                             ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                             Active = true,
                                                                                             DateCreated = DateTime.UtcNow,
                                                                                             DateModified = DateTime.UtcNow


                                                                                         };
                                                                                         datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                         datacontext.SubmitChanges();
                                                                                         ClaimId = Obj.EmpClaimId;

                                                                                         TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                         {
                                                                                             TransactionDate = DateTime.UtcNow,
                                                                                             TransactionDetails = "Claim Amount",
                                                                                             Amount = ClaimAmt,
                                                                                             CompanyUserId = iObj.CompanyUserId,
                                                                                             CompanyPlanId = -1,
                                                                                             ClaimId = Obj.EmpClaimId,
                                                                                             TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                             PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                             PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                             Active = true,
                                                                                             DateCreated = DateTime.UtcNow
                                                                                         };
                                                                                         datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                         
                                                                                         EmpPayment.Amount = BalanceAmount;
                                                                                         datacontext.SubmitChanges();
                                                                                         lobjResponse.ID = Obj.EmpClaimId;

                                                                                         CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);

                                                                                       
                                                                                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                         IsSuccess = true;

                                                                                           
                                                                                     }
                                                                                     catch (WebException ex)
                                                                                     {
                                                                                         lobjResponse.Message = ex.Message;
                                                                                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                         IsSuccess = false;
                                                                                     }

                                                                             #endregion Add Claim
                                                                         }
                                                                         else
                                                                         {
                                                                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                             lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                             IsSuccess = false;
                                                                         }


                                                                         
                                                                     }
                                                                 }
                                                                 else
                                                                 {
                                                                     lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                     lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                     IsSuccess = false;
                                                                 }

                                                             }
                                                             else
                                                             {
                                                                 #region Add Claim

                                                                         try
                                                                         {
                                                                             TblEmpClaim Obj = new TblEmpClaim()
                                                                             {
                                                                                 CompanyUserId = iObj.CompanyUserId,
                                                                                 
                                                                                 ServiceId = iObj.ServiceId,
                                                                                 ServiceProvider = iObj.ServiceProvider,
                                                                                 ServiceProviderId = ServiceProviderId,
                                                                                 ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                 ReceiptNo = iObj.ReceiptNo,
                                                                                 UploadFileName = iObj.UploadFileName,
                                                                                 Amount = iObj.ClaimAmount,
                                                                                 ClaimAmt = iObj.ClaimAmount,
                                                                                 ClaimFee = ClaimFee,
                                                                                 DependantId = iObj.Dependant,
                                                                                 Note = iObj.Note,
                                                                                 ReimburseableAmt = ClaimAmt,
                                                                                 IsManual = iObj.IsManual,
                                                                                 ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                 Active = true,
                                                                                 DateCreated = DateTime.UtcNow,
                                                                                 DateModified = DateTime.UtcNow


                                                                             };
                                                                             datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                             datacontext.SubmitChanges();

                                                                             ClaimId = Obj.EmpClaimId;
                                                                             TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                             {
                                                                                 TransactionDate = DateTime.UtcNow,
                                                                                 TransactionDetails = "Claim Amount",
                                                                                 Amount = ClaimAmt,
                                                                                 CompanyUserId = iObj.CompanyUserId,
                                                                                 CompanyPlanId = -1,
                                                                                 ClaimId = Obj.EmpClaimId,
                                                                                 TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                 PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                 PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                 Active = true,
                                                                                 DateCreated = DateTime.UtcNow
                                                                             };
                                                                             datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                             
                                                                             EmpPayment.Amount = BalanceAmount;
                                                                             datacontext.SubmitChanges();
                                                                             CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);
                                                                             mastercontext.SubmitChanges();

                                                                             lobjResponse.ID = Obj.EmpClaimId;

                                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                            IsSuccess = true;
                                                                         }
                                                                         catch (WebException ex)
                                                                         {
                                                                             lobjResponse.Message = ex.Message;
                                                                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                             IsSuccess = false;
                                                                         }

                                                                  
                                                             
                                                                 #endregion Add Claim
                                                             }

                                                         }
                                                         else
                                                         {
                                                             #region If Card fee range is not applicable
                                                             #region Add Claim

                                                             try
                                                             {

                                                                 TblEmpClaim Obj = new TblEmpClaim()
                                                                 {
                                                                     CompanyUserId = iObj.CompanyUserId,
                                                                     
                                                                     ServiceId = iObj.ServiceId,
                                                                     ServiceProvider = iObj.ServiceProvider,
                                                                     ServiceProviderId = ServiceProviderId,
                                                                     ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                     ReceiptNo = iObj.ReceiptNo,
                                                                     UploadFileName = iObj.UploadFileName,
                                                                     Amount = iObj.ClaimAmount,
                                                                     ClaimAmt = iObj.ClaimAmount,
                                                                     ClaimFee = ClaimFee,
                                                                     DependantId = iObj.Dependant,
                                                                     Note = iObj.Note,
                                                                     ReimburseableAmt = ClaimAmt,
                                                                     IsManual = iObj.IsManual,
                                                                     ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                     Active = true,
                                                                     DateCreated = DateTime.UtcNow,
                                                                     DateModified = DateTime.UtcNow


                                                                 };
                                                                 datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                 datacontext.SubmitChanges();
                                                                 ClaimId = Obj.EmpClaimId;

                                                                 TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                 {
                                                                     TransactionDate = DateTime.UtcNow,
                                                                     TransactionDetails = "Claim Amount",
                                                                     Amount = ClaimAmt,
                                                                     CompanyUserId = iObj.CompanyUserId,
                                                                     CompanyPlanId = -1,
                                                                     ClaimId = Obj.EmpClaimId,
                                                                     TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                     PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                     PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                     Active = true,
                                                                     DateCreated = DateTime.UtcNow
                                                                 };
                                                                 datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                 
                                                                 EmpPayment.Amount = BalanceAmount;
                                                                 datacontext.SubmitChanges();
                                                                 CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);
                                                                 lobjResponse.ID = Obj.EmpClaimId;
                                                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                 IsSuccess = true;


                                                             }
                                                             catch (WebException ex)
                                                             {
                                                                 lobjResponse.Message = ex.Message;
                                                                 lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                 IsSuccess = false;
                                                             }

                                                             #endregion Add Claim
                                                             #endregion
                                                         }
                                                        
                                                         
                                                      
                                                     }
                                                     #endregion Monthly

                                                     
                                                    
                                                 }
                                               
                                             }
                                             else
                                             {
                                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                 lobjResponse.Message = "You have not sufficient credits in your account. Because your account balanace is " + EmpPayment.Amount + " while yours claim amount including claim processingfee is $ " + CheckAmount + " .So, you are not eligible applicant for claim.";
                                                 IsSuccess = false;
                                             }
                                             lobjResponse.ClaimProcessingFee = ClaimFee;
                                         }
                                         else
                                         {
                                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                             lobjResponse.Message = "Claim request already raised against this receipt number.";
                                             IsSuccess = false;
                                         }
                                          
                                      
                                    }
                                    else
                                    {
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                        lobjResponse.Message = "You are not eligible for claim, because your plan starts after the receipt date.";
                                        IsSuccess = false;
                                    }
                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You have not sufficient balance for claim.";
                                    IsSuccess = false;
                                }
                            
                                if (IsSuccess)
                                {
                                    string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                    string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                    string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                    Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                    CMail Mail = new CMail();
                                    string EmailTo = "";
                                    TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                    if (CarrierObj != null)
                                    {
                                        string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                        CMail Mail1 = new CMail();
                                       
                                        Mail1.EmailTo = CarrierEmails;
                                        Mail1.Subject = "New Claim Request";
                                        Mail1.MessageBody = NotiText;
                                        Mail1.GodaddyUserName = GodaddyUserName;
                                        Mail1.GodaddyPassword = GodaddyPwd;
                                        Mail1.Server = Server;
                                        Mail1.Port = Port;
                                        Mail1.CompanyId = CompanyId;
                                        bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                    }

                                    if (!string.IsNullOrEmpty(EmailTo))
                                    {
                                        EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                    }
                                    else
                                    {
                                        EmailTo = UserObj.EmailAddress;

                                    }


                                        
                                        StringBuilder builder = new StringBuilder();
                                        builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                        builder.Append(NotiText+"<br/><br/>");
                                        builder.Append("Regards,<br/><br/>");
                                        builder.Append("Eflex Team<br/><br/>");

                                        
                                        //send mail
                                        string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                        lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                      
                                        Mail.EmailTo = EmailTo;
                                        Mail.Subject = "New Claim Request";
                                        Mail.MessageBody = lstrMessage;
                                        Mail.GodaddyUserName = GodaddyUserName;
                                        Mail.GodaddyPassword = GodaddyPwd;
                                        Mail.Server = Server;
                                        Mail.Port = Port;
                                        Mail.CompanyId = CompanyId;
                                        bool IsSent = Mail.SendEMail(datacontext, true);

                                        #region Push Notifications
                                        List<TblNotificationDetail> NotificationList = null;
                                        NotificationList = InsertionInNotificationTable(iObj.CompanyUserId, NotiText, datacontext, ClaimId, CObjects.enmNotifyType.Claim.ToString());

                                        NotifyUser1(datacontext, NotiText, iObj.CompanyUserId, CObjects.enmNotifyType.Claim.ToString());
                                        datacontext.SubmitChanges();

                                        if (NotificationList != null && NotificationList.Count() > 0)
                                        {
                                            SendNotification(datacontext, NotificationList, ClaimId);
                                            NotificationList.Clear();

                                        }
                                        #endregion Push Notifications

                                    
                                }
                                decimal BalanceAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select Convert.ToDecimal(p.Amount)).Take(1).SingleOrDefault();
                                lobjResponse.BalanceAmount = BalanceAmt;
                        
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                   
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, iObj.LoggedInUserId, "AddClaim", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetEmpCardDetails
        public CGetEmpCardDetailsResponse GetEmpCardDetails(DbConLoggedInUIdInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            CGetEmpCardDetailsResponse lobjResponse = new CGetEmpCardDetailsResponse();
            string Constring = GetConnection(iObj.DbCon);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
                    if (IsValidateUser(datacontext))
                    {

                        TblMasterCard MasterCardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.Id && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (MasterCardObj != null)
                        {
                            if (MasterCardObj.IsActive == true)
                            {
                                lobjResponse.AssignNewCard = false;
                            }
                            else
                            {
                                lobjResponse.AssignNewCard = true;
                            }
                        }
                        else
                        {
                            lobjResponse.AssignNewCard = false;
                        }
                        List<GetEmpCardDetails> Obj = (from p in mastercontext.TblMasterCards
                                                       where p.Active == true
                                                       && p.UserId == iObj.Id && p.CompanyId == CompanyId && (p.IsActive==true || (p.IsActive == false && p.CardStatus == "Pending for activation"))
                                                       select new GetEmpCardDetails()
                                                       {
                                                        MasterCardId = p.MasterCardId,
                                                        FourthLine = p.FourthLine,
                                                        CardStatus = p.CardStatus,
                                                        LastFourDigit = p.LastFourDigitOfCard == null ? -1 : Convert.ToDecimal(p.LastFourDigitOfCard),
                                                        AccountSummaryData = GetAccountData(mastercontext,p.UserId,p.AccountId,p.MasterCardId,p.CompanyId)
                                                     }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.CardData = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message = CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, CurrentTime, UserId, "GetEmpCardDetails", InputJson, new JavaScriptSerializer().Serialize(lobjResponse.CardData));
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region DeleteEmployee
        public CResponse DeleteEmployee(DeleteEmployeeInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            UserId = iObj.CompanyUserId;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                   
                        decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);

                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                             TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                             if (CompPaymentObj != null)
                             {
                                  TblPayment PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                                  if (PaymentObj != null)
                                  {
                                      CompPaymentObj.Amount = CompPaymentObj.Amount - PaymentObj.Amount;
                                      mastercontext.SubmitChanges();
                                  }
                                  #region Broker Commission
                                  
                                  #region Broker Commission
                                  TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                     where p.Active == true && p.CompanyId == CompanyId
                                                                     select p).Take(1).SingleOrDefault();
                                  if (CompReferral != null)
                                  {
                                      
                                      decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                      where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                      select Convert.ToDecimal(p.NetDeposit)).Sum();

                                      if (CompReferral.BrokerId != -1)
                                      {
                                          TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                                 where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                                 select p).Take(1).SingleOrDefault();
                                          TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                        where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyId
                                                                        select p).Take(1).SingleOrDefault();
                                          decimal PercentageBrokerCharge = 0;
                                          if (FeeObj != null)
                                          {
                                              if (FeeObj.BrokerFee > 0)
                                              {
                                                  PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                              }
                                          }
                                          else
                                          {
                                              if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                              {
                                                  PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                              }
                                              else
                                              {
                                                  PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                              }
                                          }


                                          if (PercentageBrokerCharge > 0)
                                          {
                                              decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                              TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                                 where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                                 p.UserId == BrokerObj.UserId
                                                                                 select p).Take(1).SingleOrDefault();
                                              if (TransactionObj != null)
                                              {

                                                  TransactionObj.Contribution = TransactionObj.Contribution - BrokerAmountToBepaid;
                                                  TransactionObj.Amount = TransactionObj.Amount - Amount;
                                              }
                                              
                                              mastercontext.SubmitChanges();
                                          }
                                      }
                                  }
                                  #endregion Broker Commission



                                  #endregion Broker Commission
                             }



                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                            UserObj.Active = false;
                            UserObj.DateModified = DateTime.UtcNow;
                            TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (EmpObj != null)
                            {
                                EmpObj.Active = false;
                                EmpObj.DateModified = DateTime.UtcNow;
                            }

                            List<TblEmpTransaction> EmpTranList = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).ToList();
                            if (EmpTranList != null && EmpTranList.Count() > 0)
                            {
                                EmpTranList.ForEach(x => x.Active = false);

                            }
                            List<TblPayment> PaymentList = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).ToList();
                            if (PaymentList != null && PaymentList.Count() > 0)
                            {
                                PaymentList.ForEach(x => x.Active = false);

                            }

                            TblEmpBalance EmpBalObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if(EmpBalObj != null)
                            {
                                EmpBalObj.Active = false;
                            }

                            UpdateMsgUser(UserObj, "Delete", CompanyId);

                            datacontext.SubmitChanges();
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Message = "User doesn't exist";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                  
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, iObj.CompanyUserId, "DeleteEmployee", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddClaim
        public CLoadValueResponse AddClaim(AddClaimInput iObj)
        {
            CLoadValueResponse lobjResponse = new CLoadValueResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsSuccess = false;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                try
                {
                    TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();
                    DateTime PlanStartDate = (from p in datacontext.TblCompanyPlans join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId where p.Active == true && q.Active == true && q.CompanyUserId == iObj.LoggedInUserId select Convert.ToDateTime(p.PlanStartDt)).Take(1).SingleOrDefault();
                    if (IsValidateUser(datacontext))
                    {
                        TblSPReceipt ReceiptObj = (from p in mastercontext.TblSPReceipts where p.Active == true && p.ReceiptNo.ToUpper() == iObj.ReceiptNo.ToUpper() && p.ServiceProviderId == iObj.ServiceProviderId select p).Take(1).SingleOrDefault();
                        if (ReceiptObj == null)
                        {


                            string Text = "Your eFlex claim has been received and will be processed shortly. If you have any questions please contact the eFlex Customer Care team at eflexclaims@eclipseeio.com";
                            decimal ClaimAmt = 0;
                            decimal ClaimFee = 0;
                            decimal BalanceAmount = 0;
                            decimal CheckAmount = 0;
                            bool IsClaimFeeCharge = false;
                            decimal ClaimId = -1;
                            TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (EmpPayment != null)
                            {
                                decimal ServiceProviderId = -1;
                                #region Add ServiceProvider
                                if (iObj.ServiceProviderId == -1)
                                {
                                    TblServiceProvider ObjExists = (from p in mastercontext.TblServiceProviders where p.ServiceId == iObj.ServiceId && p.Active == true && p.Name.ToUpper() == iObj.ServiceProvider.ToUpper() && p.Address.ToUpper() == iObj.ProviderAddress.ToUpper() select p).Take(1).SingleOrDefault();
                                    if (ObjExists == null)
                                    {
                                        TblServiceProvider Obj = new TblServiceProvider()
                                        {
                                            Name = iObj.ServiceProvider,
                                            Address = iObj.ProviderAddress,
                                            ContactNo = iObj.ProviderContactNo,
                                            Email = iObj.ProviderEmail,
                                            ServiceId = iObj.ServiceId,
                                            CompanyId = CompanyId,
                                            GoogleId = iObj.GoogleUniqueId,
                                            Active = true,
                                            DateCreated = DateTime.UtcNow,
                                            DateModified = DateTime.UtcNow
                                        };
                                        mastercontext.TblServiceProviders.InsertOnSubmit(Obj);
                                        mastercontext.SubmitChanges();

                                        
                                        ServiceProviderId = Obj.ServiceProviderId;


                                    }
                                    else
                                    {
                                        ServiceProviderId = ObjExists.ServiceProviderId;

                                    }
                                }
                                else
                                {
                                    ServiceProviderId = iObj.ServiceProviderId;

                                }


                                TblSPReceipt RcptObj = new TblSPReceipt()
                                {
                                    ServiceProviderId = ServiceProviderId,
                                    ReceiptNo = iObj.ReceiptNo,
                                    CompanyId = CompanyId,
                                    UserId = iObj.CompanyUserId,
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow,

                                };
                                mastercontext.TblSPReceipts.InsertOnSubmit(RcptObj);


                                #endregion

                                
                                if (Convert.ToDateTime(iObj.ReceiptDate).Date > PlanStartDate.Date || Convert.ToDateTime(iObj.ReceiptDate).ToString("MM/dd/yyyy") == PlanStartDate.ToString("MM/dd/yyyy"))
                                {


                                    TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.ReceiptNo == iObj.ReceiptNo select p).Take(1).SingleOrDefault();
                                      if (ClaimObj == null || string.IsNullOrEmpty(iObj.ReceiptNo))
                                      {

                                        #region Claim Fee Charges
                                        TblEmpClaim ClaimObj1 = (from p in datacontext.TblEmpClaims where p.Active == true && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date select p).Take(1).SingleOrDefault();
                                        if (ClaimObj1 != null)
                                        {
                                            ClaimAmt = iObj.ClaimAmount;
                                            BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt));
                                            CheckAmount = iObj.ClaimAmount;
                                           
                                        }
                                        else
                                        {
                                            TblFeeDetail FeeDetailsObj = (from p in mastercontext.TblFeeDetails
                                                                          where p.Active == true && p.FeeDetailId == Convert.ToDecimal(CObjects.enumFeeDetails.ClaimFee) &&
                                                                              p.FeeTypeId == Convert.ToDecimal(CObjects.enumFeeType.Claim) && p.IsApplicable == true
                                                                          select p).Take(1).SingleOrDefault();
                                            if (FeeDetailsObj != null)
                                            {
                                                IsClaimFeeCharge = true;
                                                ClaimFee = Convert.ToDecimal(FeeDetailsObj.Amount);
                                                ClaimAmt = iObj.ClaimAmount;
                                                CheckAmount = iObj.ClaimAmount + ClaimFee;
                                                BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (CheckAmount));
                                            }
                                        }
                                        #endregion

                                        //check balance if balance is greater than load amount
                                        if (EmpPayment.Amount >= CheckAmount)
                                        {
                                            TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans
                                                                      join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                                      where p.Active == true && q.Active == true && q.CompanyUserId == iObj.CompanyUserId
                                                                      select p).Take(1).SingleOrDefault();
                                            if (PlanObj != null)
                                            {


                                                #region Monthly
                                                if (PlanObj.ContributionSchedule == 1)//Monthly
                                                {

                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                    if (CardFeeRangeObj != null)
                                                    {
                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                            where p.CompanyUserId == iObj.CompanyUserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                && p.Active == true
                                                                                            select p).ToList();
                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                        {

                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                   where p.CompanyUserId == iObj.CompanyUserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                       && p.Active == true
                                                                                                   select p).ToList();
                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                            {
                                                                decimal YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                                
                                                                if (CheckAmount <= CheckYearAmt)
                                                                {
                                                                    //check monthly limit
                                                                    var now = DateTime.Now;
                                                                    var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                    var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                    List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                            where p.Active == true && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                && p.CompanyUserId == iObj.CompanyUserId && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date)
                                                                                                            select p).ToList();
                                                                    decimal MonthlyClaimAmt = 0;


                                                                    if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                    {
                                                                        MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                    }

                                                                    decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                    
                                                                    if (CheckAmount <= CheckMonthlyAmt)
                                                                    {
                                                                        //check day limit
                                                                        decimal DailyClaimAmt = 0;
                                                                        List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                              where p.Active == true && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                                  && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date
                                                                                                              select p).ToList();
                                                                        if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                        {
                                                                            DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                        }
                                                                        
                                                                        decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                        
                                                                        if (CheckAmount <= CheckDailyAmt)
                                                                        {


                                                                            #region Add Claim


                                                                            try
                                                                            {
                                                                                TblEmpClaim Obj = new TblEmpClaim()
                                                                                {
                                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                                    
                                                                                    ServiceId = iObj.ServiceId,
                                                                                    ServiceProvider = iObj.ServiceProvider,
                                                                                    ServiceProviderId = ServiceProviderId,
                                                                                    ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                    ReceiptNo = iObj.ReceiptNo,
                                                                                    UploadFileName = iObj.UploadFileName,
                                                                                    Amount = iObj.ClaimAmount,
                                                                                    ClaimAmt = iObj.ClaimAmount,
                                                                                    ClaimFee = ClaimFee,
                                                                                    DependantId = iObj.Dependant,
                                                                                    Note = iObj.Note,
                                                                                    ReimburseableAmt = ClaimAmt,
                                                                                    IsManual = iObj.IsManual,
                                                                                    ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                    Active = true,
                                                                                    DateCreated = DateTime.UtcNow,
                                                                                    DateModified = DateTime.UtcNow


                                                                                };
                                                                                datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                                datacontext.SubmitChanges();
                                                                                ClaimId = Obj.EmpClaimId;

                                                                                TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                                {
                                                                                    TransactionDate = DateTime.UtcNow,
                                                                                    TransactionDetails = "Claim Processing Fee",
                                                                                    Amount = ClaimAmt,
                                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                                    CompanyPlanId = -1,
                                                                                    ClaimId = Obj.EmpClaimId,
                                                                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                    PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                    Active = true,
                                                                                    DateCreated = DateTime.UtcNow
                                                                                };
                                                                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                                
                                                                                EmpPayment.Amount = BalanceAmount;
                                                                                datacontext.SubmitChanges();


                                                                                CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);

                                                                                lobjResponse.ID = Obj.EmpClaimId;
                                                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                                IsSuccess = true;
                                                                            }
                                                                            catch (WebException ex)
                                                                            {
                                                                                lobjResponse.Message = ex.Message;
                                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                                IsSuccess = false;
                                                                            }



                                                                            #endregion Add Claim
                                                                        }
                                                                        else
                                                                        {
                                                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                            lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                            IsSuccess = false;
                                                                        }


                                                                    }
                                                                    else
                                                                    {
                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                        lobjResponse.Message = "Your are not eligible because monthly claim amount limit exceed to its max limit.";
                                                                        IsSuccess = false;
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                    lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                    IsSuccess = false;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            #region Add Claim

                                                            try
                                                            {
                                                                TblEmpClaim Obj = new TblEmpClaim()
                                                                {
                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                    
                                                                    ServiceId = iObj.ServiceId,
                                                                    ServiceProvider = iObj.ServiceProvider,
                                                                    ServiceProviderId = ServiceProviderId,
                                                                    ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                    ReceiptNo = iObj.ReceiptNo,
                                                                    UploadFileName = iObj.UploadFileName,
                                                                    Amount = iObj.ClaimAmount,
                                                                    ClaimAmt = iObj.ClaimAmount,
                                                                    ClaimFee = ClaimFee,
                                                                    DependantId = iObj.Dependant,
                                                                    Note = iObj.Note,
                                                                    ReimburseableAmt = ClaimAmt,
                                                                    IsManual = iObj.IsManual,
                                                                    ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                    Active = true,
                                                                    DateCreated = DateTime.UtcNow,
                                                                    DateModified = DateTime.UtcNow


                                                                };
                                                                datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                datacontext.SubmitChanges();
                                                                ClaimId = Obj.EmpClaimId;

                                                                TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                {
                                                                    TransactionDate = DateTime.UtcNow,
                                                                    TransactionDetails = "Claim Submitted - Manual",
                                                                    Amount = ClaimAmt,
                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                    CompanyPlanId = -1,
                                                                    ClaimId = Obj.EmpClaimId,
                                                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                    PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                    Active = true,
                                                                    DateCreated = DateTime.UtcNow
                                                                };
                                                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                
                                                                EmpPayment.Amount = BalanceAmount;
                                                                datacontext.SubmitChanges();
                                                                lobjResponse.ID = Obj.EmpClaimId;

                                                                CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);


                                                                mastercontext.SubmitChanges();

                                                                IsSuccess = true;
                                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                            }
                                                            catch (WebException ex)
                                                            {
                                                                lobjResponse.Message = ex.Message;
                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                IsSuccess = false;
                                                            }


                                                            #endregion Add Claim
                                                        }

                                                    }
                                                    else
                                                    {
                                                        #region Add Claim

                                                        try
                                                        {
                                                            TblEmpClaim Obj = new TblEmpClaim()
                                                            {
                                                                CompanyUserId = iObj.CompanyUserId,
                                                                
                                                                ServiceId = iObj.ServiceId,
                                                                ServiceProvider = iObj.ServiceProvider,
                                                                ServiceProviderId = ServiceProviderId,
                                                                ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                ReceiptNo = iObj.ReceiptNo,
                                                                UploadFileName = iObj.UploadFileName,
                                                                Amount = iObj.ClaimAmount,
                                                                ClaimAmt = iObj.ClaimAmount,
                                                                ClaimFee = ClaimFee,
                                                                DependantId = iObj.Dependant,
                                                                Note = iObj.Note,
                                                                ReimburseableAmt = ClaimAmt,
                                                                IsManual = iObj.IsManual,
                                                                ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                Active = true,
                                                                DateCreated = DateTime.UtcNow,
                                                                DateModified = DateTime.UtcNow


                                                            };
                                                            datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                            datacontext.SubmitChanges();

                                                            ClaimId = Obj.EmpClaimId;
                                                            TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                            {
                                                                TransactionDate = DateTime.UtcNow,
                                                                TransactionDetails = "Claim Submitted - Manual",
                                                                Amount = ClaimAmt,
                                                                CompanyUserId = iObj.CompanyUserId,
                                                                CompanyPlanId = -1,
                                                                ClaimId = Obj.EmpClaimId,
                                                                TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                Active = true,
                                                                DateCreated = DateTime.UtcNow
                                                            };
                                                            datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                            
                                                            EmpPayment.Amount = BalanceAmount;
                                                            datacontext.SubmitChanges();


                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                                                            CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);


                                                            mastercontext.SubmitChanges();
                                                            lobjResponse.ID = Obj.EmpClaimId;
                                                            IsSuccess = true;
                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                        }
                                                        catch (WebException ex)
                                                        {
                                                            lobjResponse.Message = ex.Message;
                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                            IsSuccess = false;
                                                        }


                                                        #endregion Add Claim
                                                    }
                                                }
                                                #endregion Monthly

                                                #region Annual
                                                if (PlanObj.ContributionSchedule == 2)//Annual
                                                {

                                                    TblCardFeeRange CardFeeRangeObj = (from p in mastercontext.TblCardFeeRanges where p.Active == true && (p.MinValue <= PlanObj.RecurringDeposit && p.MaxValue >= PlanObj.RecurringDeposit) && p.ContributionTypeId == PlanObj.ContributionSchedule && p.IsApplicable == true select p).Take(1).SingleOrDefault();
                                                    if (CardFeeRangeObj != null)
                                                    {
                                                        List<TblEmpClaim> AllEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                            where p.CompanyUserId == iObj.CompanyUserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                && p.Active == true
                                                                                            select p).ToList();
                                                        if (AllEmpClaimObj != null && AllEmpClaimObj.Count() > 0)
                                                        {
                                                            //check yearly limit

                                                            decimal YearClaimAmt = 0;
                                                            List<TblEmpClaim> YearlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                   where p.CompanyUserId == iObj.CompanyUserId && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                       && p.Active == true
                                                                                                   select p).ToList();
                                                            if (YearlyEmpClaimObj != null && YearlyEmpClaimObj.Count() > 0)
                                                            {
                                                                YearClaimAmt = YearlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                            }
                                                            decimal CheckYearAmt = Convert.ToDecimal(CardFeeRangeObj.OverAllMax - YearClaimAmt);
                                                            
                                                            if (CheckAmount >= CheckYearAmt)
                                                            {

                                                                //check monthly limit
                                                                var now = DateTime.Now;
                                                                var firstDayOfmonth = new DateTime(now.Year, now.Month, 1);
                                                                var lastDayOfmonth = firstDayOfmonth.AddMonths(1).AddDays(-1);
                                                                decimal MonthlyClaimAmt = 0;
                                                                List<TblEmpClaim> MonthlyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                        where p.Active == true && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                            && p.CompanyUserId == iObj.CompanyUserId && (p.DateCreated.Value.Date >= Convert.ToDateTime(firstDayOfmonth).Date && p.DateCreated.Value.Date <= Convert.ToDateTime(lastDayOfmonth).Date)
                                                                                                        select p).ToList();
                                                                if (MonthlyEmpClaimObj != null && MonthlyEmpClaimObj.Count() > 0)
                                                                {
                                                                    MonthlyClaimAmt = MonthlyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                }
                                                                decimal CheckMonthlyAmt = Convert.ToDecimal(CardFeeRangeObj.MonthlyMax - MonthlyClaimAmt);

                                                                
                                                                if (CheckAmount >= CheckMonthlyAmt)
                                                                {
                                                                    //check day limit
                                                                    decimal DailyClaimAmt = 0;
                                                                    List<TblEmpClaim> DailyEmpClaimObj = (from p in datacontext.TblEmpClaims
                                                                                                          where p.Active == true && (p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                                                                                              && p.CompanyUserId == iObj.CompanyUserId && p.DateCreated.Value.Date == Convert.ToDateTime(DateTime.UtcNow).Date
                                                                                                          select p).ToList();
                                                                    if (DailyEmpClaimObj != null && DailyEmpClaimObj.Count() > 0)
                                                                    {
                                                                        DailyClaimAmt = DailyEmpClaimObj.Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                                                    }
                                                                    decimal CheckDailyAmt = Convert.ToDecimal(CardFeeRangeObj.DayMax - DailyClaimAmt);

                                                                    
                                                                    if (CheckAmount >= CheckDailyAmt)
                                                                    {

                                                                        #region Add Claim

                                                                        try
                                                                        {

                                                                            TblEmpClaim Obj = new TblEmpClaim()
                                                                            {
                                                                                CompanyUserId = iObj.CompanyUserId,
                                                                                
                                                                                ServiceId = iObj.ServiceId,
                                                                                ServiceProvider = iObj.ServiceProvider,
                                                                                ServiceProviderId = ServiceProviderId,
                                                                                ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                                ReceiptNo = iObj.ReceiptNo,
                                                                                UploadFileName = iObj.UploadFileName,
                                                                                Amount = iObj.ClaimAmount,
                                                                                ClaimAmt = iObj.ClaimAmount,
                                                                                ClaimFee = ClaimFee,
                                                                                DependantId = iObj.Dependant,
                                                                                Note = iObj.Note,
                                                                                ReimburseableAmt = ClaimAmt,
                                                                                IsManual = iObj.IsManual,
                                                                                ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                                Active = true,
                                                                                DateCreated = DateTime.UtcNow,
                                                                                DateModified = DateTime.UtcNow


                                                                            };
                                                                            datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                            datacontext.SubmitChanges();

                                                                            ClaimId = Obj.EmpClaimId;
                                                                            TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                            {
                                                                                TransactionDate = DateTime.UtcNow,
                                                                                TransactionDetails = "Claim Submitted - Manual",
                                                                                Amount = ClaimAmt,
                                                                                CompanyUserId = iObj.CompanyUserId,
                                                                                CompanyPlanId = -1,
                                                                                ClaimId = Obj.EmpClaimId,
                                                                                TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                                PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                                Active = true,
                                                                                DateCreated = DateTime.UtcNow
                                                                            };
                                                                            datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                            
                                                                            EmpPayment.Amount = BalanceAmount;
                                                                            datacontext.SubmitChanges();
                                                                            lobjResponse.ID = Obj.EmpClaimId;

                                                                            CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);


                                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                                            IsSuccess = true;


                                                                        }
                                                                        catch (WebException ex)
                                                                        {
                                                                            lobjResponse.Message = ex.Message;
                                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                            IsSuccess = false;
                                                                        }

                                                                        #endregion Add Claim
                                                                    }
                                                                    else
                                                                    {
                                                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                        lobjResponse.Message = "Your are not eligible because day claim amount limit exceed to its max limit.";
                                                                        IsSuccess = false;
                                                                    }


                                                                    
                                                                }
                                                            }
                                                            else
                                                            {
                                                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                                lobjResponse.Message = "Your are not eligible because annual claim amount limit exceed to its max limit.";
                                                                IsSuccess = false;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            #region Add Claim

                                                            try
                                                            {
                                                                TblEmpClaim Obj = new TblEmpClaim()
                                                                {
                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                    
                                                                    ServiceId = iObj.ServiceId,
                                                                    ServiceProvider = iObj.ServiceProvider,
                                                                    ServiceProviderId = ServiceProviderId,
                                                                    ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                    ReceiptNo = iObj.ReceiptNo,
                                                                    UploadFileName = iObj.UploadFileName,
                                                                    Amount = iObj.ClaimAmount,
                                                                    ClaimAmt = iObj.ClaimAmount,
                                                                    ClaimFee = ClaimFee,
                                                                    DependantId = iObj.Dependant,
                                                                    Note = iObj.Note,
                                                                    ReimburseableAmt = ClaimAmt,
                                                                    IsManual = iObj.IsManual,
                                                                    ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                    Active = true,
                                                                    DateCreated = DateTime.UtcNow,
                                                                    DateModified = DateTime.UtcNow


                                                                };
                                                                datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                                datacontext.SubmitChanges();

                                                                ClaimId = Obj.EmpClaimId;
                                                                TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                                {
                                                                    TransactionDate = DateTime.UtcNow,
                                                                    TransactionDetails = "Claim Submitted - Manual",
                                                                    Amount = ClaimAmt,
                                                                    CompanyUserId = iObj.CompanyUserId,
                                                                    CompanyPlanId = -1,
                                                                    ClaimId = Obj.EmpClaimId,
                                                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                    PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                    Active = true,
                                                                    DateCreated = DateTime.UtcNow
                                                                };
                                                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                                
                                                                EmpPayment.Amount = BalanceAmount;
                                                                datacontext.SubmitChanges();
                                                                CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);
                                                                mastercontext.SubmitChanges();

                                                                lobjResponse.ID = Obj.EmpClaimId;

                                                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                                                IsSuccess = true;
                                                            }
                                                            catch (WebException ex)
                                                            {
                                                                lobjResponse.Message = ex.Message;
                                                                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                                IsSuccess = false;
                                                            }



                                                            #endregion Add Claim
                                                        }

                                                    }
                                                    else
                                                    {
                                                        #region If Card fee range is not applicable
                                                        #region Add Claim

                                                        try
                                                        {

                                                            TblEmpClaim Obj = new TblEmpClaim()
                                                            {
                                                                CompanyUserId = iObj.CompanyUserId,
                                                                
                                                                ServiceId = iObj.ServiceId,
                                                                ServiceProvider = iObj.ServiceProvider,
                                                                ServiceProviderId = ServiceProviderId,
                                                                ReceiptDate = Convert.ToDateTime(iObj.ReceiptDate),
                                                                ReceiptNo = iObj.ReceiptNo,
                                                                UploadFileName = iObj.UploadFileName,
                                                                Amount = iObj.ClaimAmount,
                                                                ClaimAmt = iObj.ClaimAmount,
                                                                ClaimFee = ClaimFee,
                                                                DependantId = iObj.Dependant,
                                                                Note = iObj.Note,
                                                                ReimburseableAmt = ClaimAmt,
                                                                IsManual = iObj.IsManual,
                                                                ClaimStatusId = Convert.ToInt16(CObjects.enumClaimStatus.InProgress),
                                                                Active = true,
                                                                DateCreated = DateTime.UtcNow,
                                                                DateModified = DateTime.UtcNow


                                                            };
                                                            datacontext.TblEmpClaims.InsertOnSubmit(Obj);
                                                            datacontext.SubmitChanges();
                                                            ClaimId = Obj.EmpClaimId;

                                                            TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                            {
                                                                TransactionDate = DateTime.UtcNow,
                                                                TransactionDetails = "Claim Submitted - Manual",
                                                                Amount = ClaimAmt,
                                                                CompanyUserId = iObj.CompanyUserId,
                                                                CompanyPlanId = -1,
                                                                ClaimId = Obj.EmpClaimId,
                                                                TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                                PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                Active = true,
                                                                DateCreated = DateTime.UtcNow
                                                            };
                                                            datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                            
                                                            EmpPayment.Amount = BalanceAmount;
                                                            datacontext.SubmitChanges();
                                                            CreateHistoryLog(mastercontext, CompanyId, UserId, UserId, Text);
                                                            lobjResponse.ID = Obj.EmpClaimId;
                                                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                            IsSuccess = true;


                                                        }
                                                        catch (WebException ex)
                                                        {
                                                            lobjResponse.Message = ex.Message;
                                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                                            IsSuccess = false;
                                                        }

                                                        #endregion Add Claim
                                                        #endregion
                                                    }



                                                }
                                                #endregion Monthly

                                                #region Charge Claim Fee
                                                if (IsClaimFeeCharge)
                                                {
                                                    
                                                    TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                                    {
                                                        TransactionDate = DateTime.UtcNow,
                                                        TransactionDetails = "Claim Processing Fee",
                                                        Amount = ClaimFee,
                                                        CompanyUserId = iObj.CompanyUserId,
                                                        CompanyPlanId = -1,
                                                        ClaimId = ClaimId,
                                                        TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                        PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                        PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow
                                                    };
                                                    datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);
                                                    
                                                    TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                    {
                                                        CompanyUserId = iObj.CompanyUserId,
                                                        Amount = ClaimFee,
                                                        AdjustmentDT = DateTime.UtcNow,
                                                        Remarks = "Claim Processing Fee",
                                                        TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                        Active = true,
                                                        DateCreated = DateTime.UtcNow,
                                                        DateModified = DateTime.UtcNow
                                                    };
                                                    datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);
                                                    datacontext.SubmitChanges();
                                                    BalanceAmount = Convert.ToDecimal(EmpPayment.Amount - (ClaimAmt + ClaimFee));
                                                }
                                                #endregion

                                            }

                                        }
                                        else
                                        {
                                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                            lobjResponse.Message = "You have not sufficient credits in your account. Because your account balanace is " + EmpPayment.Amount + " while yours claim amount including claim processingfee is $ " + CheckAmount + " .So, you are not eligible applicant for claim.";
                                            IsSuccess = false;
                                        }
                                        lobjResponse.ClaimProcessingFee = ClaimFee;
                                    }
                                    else
                                    {
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                        lobjResponse.Message = "Claim request already raised against this receipt number.";
                                        IsSuccess = false;
                                    }

                                    mastercontext.SubmitChanges();
                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    lobjResponse.Message = "You are not eligible for claim, because your plan starts after the receipt date.";
                                    IsSuccess = false;
                                }
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "You have not sufficient balance for claim.";
                                IsSuccess = false;
                            }
                            

                            #region Send Mail


                            if (IsSuccess)
                            {
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                string EmailTo = "";
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                string NotiText = "Your eFlex claim has been received and will be processed shortly. If you have any questions please contact the eFlex Customer Care team at eflexclaims@eclipseeio.com";

                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = EmailTo;
                                    Mail1.Subject = "New Claim Request";
                                    Mail1.MessageBody = NotiText;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);

                                }

                                if (!string.IsNullOrEmpty(EmailTo))
                                {
                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                }
                                else
                                {
                                    EmailTo = UserObj.EmailAddress;

                                }


                                
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");

                                builder.Append(NotiText + "<br/><br/>");
                                builder.Append("Regards,<br/><br/>");
                                builder.Append("Eflex Team<br/><br/>");

                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());


                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;// CarrierEmails;
                                Mail.Subject = "New Claim Request";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyId;
                                bool IsSent = Mail.SendEMail(datacontext, true);

                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                NotificationList = InsertionInNotificationTable(iObj.CompanyUserId, NotiText, datacontext, iObj.CompanyUserId, CObjects.enmNotifyType.Claim.ToString());
                                NotifyUser1(datacontext, NotiText, iObj.CompanyUserId, CObjects.enmNotifyType.Claim.ToString());


                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, ClaimId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications
                            }
                            #endregion
                            decimal BalanceAmt = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select Convert.ToDecimal(p.Amount)).Take(1).SingleOrDefault();
                            lobjResponse.BalanceAmount = BalanceAmt;
                            
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "This receipt number is already in use.Please another receipt for submit claim request.";
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, iObj.LoggedInUserId, "AddClaim", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
                mastercontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion

        #region ResentEmployeeCredentials
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Date:230219
        /// Function used to send login credentials to employees.
        /// </summary>
        /// <param name="iObj"></param>
        /// <returns></returns>
        public CResponse ResentEmployeeCredentials(ResentEmployeeCredentialsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CompanyObj != null)
                    {
                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true  && p.CompanyUserId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            UserObj.EmailAddress = iObj.EmailId;
                            datacontext.SubmitChanges();


                            #region SendMail

                            StringBuilder builder = new StringBuilder();
                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                            CMail Mail = new CMail();
                            
                            builder.Append("Hello " + UserObj.FirstName + ",<br/><br/>");
                            builder.Append("Welcome to eFlex, your very own health spending account set up by your employer," + CompanyObj.CompanyName + " <br/><br/>");
                            builder.Append("This account provides you with the flexibility to claim medical expenses so you’re not out of pocket. To view how much money you’re eligible to use, click <a href=" + lstrUrl + " >here</a> to complete your online account as well as view your balance.<br/><br/>");
                            builder.Append("For further convenience, we have SMS Messaging.  Here you will receive updates throughout your claims process so your always aware of what’s happening in your account<br/><br/>");
                            builder.Append("Please use the following credentials for login to eFlex Portal:<br/><br/>");
                            builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                            builder.Append("Username: " + UserObj.UserName.Split('_')[1] + "<br/><br/>");
                            builder.Append("Password: " + Password + "<br/><br/>");
                            
                            //send mail
                            string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                            lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());



                            Mail.EmailTo = iObj.EmailId.Trim();
                            Mail.Subject = "Welcome to eFlex! Your Account Is Ready";
                            Mail.MessageBody = lstrMessage;
                            Mail.GodaddyUserName = GodaddyUserName;
                            Mail.GodaddyPassword = GodaddyPwd;
                            Mail.Server = Server;
                            Mail.Port = Port;
                            Mail.CompanyId = iObj.CompanyId;
                            bool IsSent = Mail.SendEMail(datacontext, true);
                            #region Push Notifications
                            List<TblNotificationDetail> NotificationList = null;
                            string NotiText = "Your eFlex account's login credentials has been sent to your email account.";
                            NotificationList = InsertionInNotificationTable(UserObj.CompanyUserId, NotiText, datacontext, UserObj.CompanyUserId, CObjects.enmNotifyType.Password.ToString());
                            NotifyUser1(datacontext, NotiText, UserObj.CompanyUserId, CObjects.enmNotifyType.Password.ToString()); 


                            if (NotificationList != null && NotificationList.Count() > 0)
                            {
                                SendNotification(datacontext, NotificationList, UserObj.CompanyUserId);
                                NotificationList.Clear();

                            }
                            #endregion Push Notifications

                            #endregion SendMail
                        }
                    }

                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                datacontext.Connection.Close();
                mastercontext.Connection.Close();

            }


            return lobjResponse;
        }
        #endregion

        #region GetEmpDashboardDataForApp
        public CGetEmpDashboardDataForAppResponse GetEmpDashboardDataForApp(DbConIdInput iObj)
        {
            CGetEmpDashboardDataForAppResponse lobjResponse = new CGetEmpDashboardDataForAppResponse();
            DateTime StartTime = DateTime.UtcNow;
            bool IsLogOut = false;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    decimal PlandesignId = -1;
                    UserId = iObj.Id;
                    if (IsValidateUser(datacontext))
                    {
                        List<GetEmpDashboardData> Obj = new List<GetEmpDashboardData>();
                        string PlanName = (from p in datacontext.TblEmpTransactions
                                           join q in datacontext.TblCompanyPlans on p.CompanyPlanId equals q.CompanyPlanId
                                           where p.Active == true && p.CompanyUserId == iObj.Id && q.Active == true
                                           select q.Name).Take(1).SingleOrDefault();
                        PlandesignId = (from p in datacontext.TblCompanyPlans
                                        join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                        where p.Active == true && q.CompanyUserId == iObj.Id && q.Active == true
                                        select Convert.ToDecimal(p.PlanDesign)).Take(1).SingleOrDefault();

                 

                        #region Claim Data
                        decimal ClaimAmt = 0;
                        List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims
                                                      where p.Active == true &&
                                                          (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress))
                                                          && p.CompanyUserId == iObj.Id
                                                      select p).ToList();
                        if (ClaimObj != null && ClaimObj.Count() > 0)
                        {
                            ClaimAmt = (from p in datacontext.TblEmpClaims
                                        where p.Active == true &&
                                         (p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.Paid) || p.ClaimStatusId == Convert.ToDecimal(CObjects.enumClaimStatus.InProgress))
                                         && p.CompanyUserId == iObj.Id
                                        select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                            if (ClaimAmt != 0)
                            {
                                GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                                {
                                    Type = "Claim",
                                    Amount = ClaimAmt,

                                };
                                Obj.Add(EmpDashboardObj);
                            }
                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Claim",
                                Amount = ClaimAmt
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion

                        #region Balance
                        TblEmpBalance BalanceObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select p).Take(1).SingleOrDefault();

                        decimal Balance = 0;
                        if (BalanceObj != null)
                        {
                            Balance = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == iObj.Id select Convert.ToDecimal(p.Amount ?? 0)).Sum();

                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Balance",
                                Amount = Balance < 0 ? 0 : Balance
                            };
                            Obj.Add(EmpDashboardObj);

                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Balance",
                                Amount = Balance < 0 ? 0 : Balance
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion

                        #region Adjustment
                        List<TblEmpTransaction> AdjustmentObj = (from p in datacontext.TblEmpTransactions
                                                                 where p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) && p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString() && p.CompanyUserId == iObj.Id
                                                                 select p).ToList();
                        decimal Adjustment = 0;
                        if (AdjustmentObj != null && AdjustmentObj.Count() > 0)
                        {
                            Adjustment = (from p in datacontext.TblEmpTransactions
                                          where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                          && p.CompanyUserId == iObj.Id && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                          select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                           GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Adjustments",
                                Amount = Adjustment
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        else
                        {
                            GetEmpDashboardData EmpDashboardObj = new GetEmpDashboardData()
                            {
                                Type = "Adjustments",
                                Amount = Adjustment < 0 ? 0 : Adjustment
                            };
                            Obj.Add(EmpDashboardObj);
                        }
                        #endregion



                        #region CardData

                        MasterDBDataContext mastercontext = new MasterDBDataContext();
                        TblMasterCard MasterCardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.Id && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (MasterCardObj != null)
                        {
                            if (MasterCardObj.IsActive == true)
                            {
                                lobjResponse.AssignNewCard = false;
                            }
                            else
                            {
                                lobjResponse.AssignNewCard = true;
                            }
                        }
                        else
                        {
                            lobjResponse.AssignNewCard = false;
                        }
                        List<GetEmpCardDetails> EmpCardObj = (from p in mastercontext.TblMasterCards
                                                       where p.Active == true
                                                       && p.UserId == iObj.Id && p.CompanyId == CompanyId
                                                       select new GetEmpCardDetails()
                                                       {
                                                           MasterCardId = p.MasterCardId,
                                                           FourthLine = p.FourthLine,
                                                           CardStatus = p.CardStatus,
                                                           LastFourDigit = p.LastFourDigitOfCard == null ? -1 : Convert.ToDecimal(p.LastFourDigitOfCard),
                                                           AccountSummaryData = GetAccountData(mastercontext, p.UserId, p.AccountId, p.MasterCardId,p.CompanyId)
                                                       }).ToList();

                        lobjResponse.CardData = EmpCardObj;
                        #endregion

                        lobjResponse.GetEmpDashboardData = Obj;
                        lobjResponse.PlanDesign = PlandesignId;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.PlanName = PlanName;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "GetEmpDashboardDataForApp", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();


            }
            return lobjResponse;
        }


        #endregion

        #region UpdateDependant
        public CResponse UpdateDependant(UpdateDependantInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();

            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {

                            TblEmployeeDependant DependantObj = (from p in datacontext.TblEmployeeDependants where p.Active == true && p.DependantId == iObj.DependantId select p).Take(1).SingleOrDefault();
                            if (DependantObj != null)
                            {
                                DependantObj.FirstName = iObj.FName;
                                DependantObj.LastName = iObj.LName;
                                DependantObj.RelationshipId = iObj.RelationshipId;
                                DependantObj.Gender = iObj.Gender;
                                DependantObj.DOB = Convert.ToDateTime(iObj.DOB);
                                DependantObj.OtherRelation = iObj.OtherRelation;
                                DependantObj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();

                                List<TblNotificationDetail> NotificationUserList = null;
                                string PushNotifyText = iObj.FName + " " +iObj.LName +"'s details has been updated successfully.";
                                NotifyUser1(datacontext, PushNotifyText, DependantObj.CompanyUserId, CObjects.enmNotifyType.Dependant.ToString());
                                NotificationUserList = InsertionInNotificationTable(DependantObj.CompanyUserId, PushNotifyText, datacontext, DependantObj.CompanyUserId, CObjects.enmNotifyType.Dependant.ToString());
                                if (NotificationUserList != null)
                                {
                                    NotificationList.AddRange(NotificationUserList);
                                    NotificationUserList.Clear();
                                }


                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "SUCCESS";
                                
                                
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "No record Found.";
                            }

                      
                        

                   
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateResponseLog(datacontext, StartTime, -1, "UpdateDependant", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();

            }
            return lobjResponse;
        }
        #endregion UpdateDependant

        public CResponse SendMailToUnUpdatedUser(SendMailToUnUpdatedUserInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            try
            {
                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                string DbCon = DBFlag + iObj.CompanyId;
                string Constring = GetConnection(DbCon);
                using(EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
	            {
                    if(iObj.CompanyUserIds != null && iObj.CompanyUserIds.Count() > 0)
                    {
                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                        foreach (int item in iObj.CompanyUserIds)
                        {
                            TblCompanyUser CompanyUserObj = (from p in datacontext.TblCompanyUsers
                                                             join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                             where p.Active == true && q.Active == true
                                                             && p.CompanyUserId == item
                                                             select p).Take(1).SingleOrDefault();

                            if(CompanyUserObj != null)
                            {
                                #region SendMail

                                
                                StringBuilder builder = new StringBuilder();

                                builder.Append("Hello " + CompanyUserObj.FirstName + ",<br/><br/>");
                                builder.Append("Congratulations! Your employer " + CompanyObj.CompanyName + " has set-up an eFlex Health Spending account for you.<br/><br/>");
                                builder.Append("Please activate your account now:<br/><br/>");
                                builder.Append("1. Login to: <a href=" + lstrloginUrl + " ></a><br/><br/>");
                                builder.Append("Company Code: " + CompanyObj.UniqueCode + "<br/><br/>");
                                builder.Append("Username: " + CompanyUserObj.UserName.Split('_')[1] + "<br/><br/>");
                                builder.Append("Password: " + CryptorEngine.Decrypt(CompanyUserObj.Pwd) + "<br/><br/>");
                                builder.Append("2. Follow the onscreen instructions to activate your account, check your account balance and start submitting claims online.<br/><br/>");
                                builder.Append("3. We'll send you a personalized eFlex card so you can use the money in your account to pay for eligible health and wellness expenses anywhere Mastercard is accepted.<br/><br/>");
                                builder.Append("If you need help activating your account, or have a question about your health spending account and how to use it simply reply to this email or give us a call at 1-855-440-3993. We're here to help!<br/><br/>");

                                
                                //send mail
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());


                                CMail Mail = new CMail();
                                Mail.EmailTo = CompanyUserObj.EmailAddress;
                                Mail.Subject = "Your eFlex Health Spending Account";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = CompanyObj.CompanyId;
                                bool IsSent = Mail.SendEMail(mastercontext, true);


                                #endregion SendMail
                            }
                        }

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS"; 
                        
                    }
                    

                    
                              
                       
	            }
                      
                   

                  

               
            }
            catch (Exception ex)
            {
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                lobjResponse.Message = ex.Message; 
            }
            return lobjResponse;
        }
    }
}