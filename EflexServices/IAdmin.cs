﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Script.Serialization;

namespace EflexServices
{
    public partial interface IEflexService
    {
        #region LoginEflexUser
        [WebInvoke(UriTemplate = "LoginEflexUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CLoginEflexUserResponse LoginEflexUser(LoginUserInput iObj);
        #endregion LoginEflexUser

        #region GetDashboardData
        [WebInvoke(UriTemplate = "GetDashboardData", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetDashboardDataResponse GetDashboardData(IdInput iObj);
        #endregion GetDashboardData

        #region PopulateCompany
        [WebInvoke(UriTemplate = "PopulateCompany", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateCompanyResponse PopulateCompany();
        #endregion PopulateCompany

        #region AdminClaimList
        [WebInvoke(UriTemplate = "AdminClaimList", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAdminClaimListResponse AdminClaimList(AdminClaimListInput iObj);
        #endregion AdminClaimList

        #region UpdateClaimStatus
        [WebInvoke(UriTemplate = "UpdateClaimStatus", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateClaimStatus(UpdateClaimStatusInput iObj);
        #endregion UpdateClaimStatus

        #region EmployeeClaimList
        [WebInvoke(UriTemplate = "EmployeeClaimList", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CEmployeeClaimListResponse EmployeeClaimList(EmployeeClaimListInput iObj);
        #endregion EmployeeClaimList

        #region Companies
        #region ListOfCompanies
        [WebInvoke(UriTemplate = "ListOfCompanies", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfCompaniesResponse ListOfCompanies(LazyLoadingInput iObj);
        #endregion ListOfCompanies

        #region GetCompanyDetailsForAdmin
        [WebInvoke(UriTemplate = "GetCompanyDetailsForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetCompanyDetailsForAdminResponse GetCompanyDetailsForAdmin(CompanyDetailsInput iObj);
        #endregion GetCompanyDetailsForAdmin

        #region ListCompanyEmployees
        [WebInvoke(UriTemplate = "ListCompanyEmployees", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListCompEmployeesResponse ListCompanyEmployees(ListCompanyEmployeesInput iObj);
        #endregion ListCompanyEmployees

        #region AddEditAdminPAPDetails
        [WebInvoke(UriTemplate = "AddEditAdminPAPDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEditAdminPAPDetails(AddEditAdminPAPDetailsInput iObj);
        #endregion AddEditAdminPAPDetails

        #endregion

        #region ListOfAllReceivedPayment
        #region ListOfAllReceivedPayment
        [WebInvoke(UriTemplate = "ListOfAllReceivedPayment", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfAllReceivedPayment ListOfAllReceivedPayment(PaymentsInput iObj);
        #endregion

        #region ListOfAllDuePayment
        [WebInvoke(UriTemplate = "ListOfAllDuePayment", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfAllDuePayment ListOfAllDuePayment(PaymentsInput iObj);
        #endregion

        #region MarkPaymentReceived
        [WebInvoke(UriTemplate = "MarkPaymentReceived", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse MarkPaymentReceived(MarkPaymentReceivedInput iObj);
        #endregion
        #endregion

        #region AddUser
        [WebInvoke(UriTemplate = "AddUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddUser(AddUserInput iObj);

        #endregion AddUser

        #region DeleteAdminUser
        [WebInvoke(UriTemplate = "DeleteAdminUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteAdminUser(DeleteAdminUserInput iObj);
        #endregion DeleteAdminUser

        #region ListAdminUser
        [WebInvoke(UriTemplate = "ListAdminUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListAdminUserResponse ListAdminUser(IdInput iObj);
        #endregion ListAdminUser

        #region GetEmpDetailsForAdmin
        [WebInvoke(UriTemplate = "GetEmpDetailsForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDetailsForAdminResponse GetEmpDetailsForAdmin(GetEmpDetailsForAdminInput iObj);
        #endregion GetEmpDetailsForAdmin

        #region UpdatePlanDate
        [WebInvoke(UriTemplate = "UpdatePlanDate", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdatePlanDate(UpdatePlanDateInput iObj);
        #endregion UpdatePlanDate

        #region UpdateEmpBasicDetailsByAdmin
        [WebInvoke(UriTemplate = "UpdateEmpBasicDetailsByAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateEmpBasicDetailsByAdmin(UpdateEmpBasicDetailsByAdminInput iObj);
        #endregion UpdateEmpBasicDetailsByAdmin

        #region AddEmployeeByadmin
        [WebInvoke(UriTemplate = "AddEmployeeByadmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEmployeeByadmin(AddEmployeeByadminInput iObj);
        #endregion AddEmployeeByadmin

        #region GetEmpDashboardDataForAdmin
        [WebInvoke(UriTemplate = "GetEmpDashboardDataForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEmpDashboardResponse GetEmpDashboardDataForAdmin(GetEmpDashboardInput iObj);
        #endregion GetEmpDashboardDataForAdmin

        #region GetEStatementForAdmin
        [WebInvoke(UriTemplate = "GetEStatementForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetEStatementResponse GetEStatementForAdmin(GetEStatementInput iObj);
        #endregion GetEStatementForAdmin

        #region ListOfProviceTaxDetails
        [WebInvoke(UriTemplate = "ListOfProviceTaxDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfProviceTaxDetailsResponse ListOfProviceTaxDetails(IdInput iObj);
        #endregion ListOfProviceTaxDetails

        #region UpdateProvinceTaxDetails
        [WebInvoke(UriTemplate = "UpdateProvinceTaxDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateProvinceTaxDetails(UpdateProvinceTaxDetailsInput iObj);
        #endregion UpdateProvinceTaxDetails

        #region ListOfEflexNotificationLog
        [WebInvoke(UriTemplate = "ListOfEflexNotificationLog", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CNotificationLogResponse ListOfEflexNotificationLog(EflexNotificationLogInput iObj);
        #endregion ListOfEflexNotificationLog

        

        #region AddProvinceTaxDetails
        [WebInvoke(UriTemplate = "AddProvinceTaxDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddProvinceTaxDetails(AddProvinceTaxDetailsInput iObj);
        #endregion AddProvinceTaxDetails

        #region DeleteProvinceTaxDetails
        [WebInvoke(UriTemplate = "DeleteProvinceTaxDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteProvinceTaxDetails(IdInputv2 iObj);
        #endregion DeleteProvinceTaxDetails

        #region AddFeeDetails
        [WebInvoke(UriTemplate = "AddFeeDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddFeeDetails(AddFeeDetailsInput iObj);
        #endregion AddFeeDetails

        #region UpdateFeeDetails
        [WebInvoke(UriTemplate = "UpdateFeeDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateFeeDetails(UpdateFeeDetailsInput iObj);
        #endregion UpdateFeeDetails

        #region DeleteFeeDetails
        [WebInvoke(UriTemplate = "DeleteFeeDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteFeeDetails(IdInputv2 iObj);
        #endregion DeleteFeeDetails

        #region ListFeeDetails
        [WebInvoke(UriTemplate = "ListFeeDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListFeeDetailsResponse ListFeeDetails(IdInput iObj);
        #endregion ListFeeDetails

        #region PopulateFeeType
        [WebInvoke(UriTemplate = "PopulateFeeType", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateFeeTypeResponse PopulateFeeType(IdInput iObj);
        #endregion PopulateFeeType

        #region UpdateAdminFeeDetails
        [WebInvoke(UriTemplate = "UpdateAdminFeeDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateAdminFeeDetails(UpdateAdminFeeDetailsInput iObj);
        #endregion UpdateAdminFeeDetails

        #region GetAdminFee
        [WebInvoke(UriTemplate = "GetAdminFee", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetAdminFeeResponse GetAdminFee(IdInput iObj);
        #endregion GetAdminFee

        #region GetEflexNotiMsgCount
        [WebInvoke(UriTemplate = "GetEflexNotiMsgCount", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CNotiMsgCountResponse GetEflexNotiMsgCount(EflexNotiMsgCountInfo iObj);
        #endregion GetEflexNotiMsgCount

        #region AddCompAdjustments
        [WebInvoke(UriTemplate = "AddCompAdjustments", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddCompAdjustments(AddCompAdjustmentsInput iObj);
        #endregion AddCompAdjustments

        #region AddLateFeeToCompany
        [WebInvoke(UriTemplate = "AddLateFeeToCompany", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddLateFeeToCompany(AddLateFeeToCompanyInput iObj);
        #endregion AddLateFeeToCompany

        #region UpdateCompanyCredits
        [WebInvoke(UriTemplate = "UpdateCompanyCredits", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse UpdateCompanyCredits(UpdateCompanyCreditsInput iObj);
        #endregion UpdateCompanyCredits

        #region ResetPassword
        [WebInvoke(UriTemplate = "ResetPassword", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResetPassword(ResetPasswordInput iObj);
        #endregion ResetPassword

        #region ListTaxPot
        [WebInvoke(UriTemplate = "ListTaxPot", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListTaxPotResponse ListTaxPot(IdInput iObj);
        #endregion ListTaxPot

        #region ListRemittedTaxPot
        [WebInvoke(UriTemplate = "ListRemittedTaxPot", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListTaxPotResponse ListRemittedTaxPot(IdInput iObj);
        #endregion ListRemittedTaxPot

        #region RemitTax
        [WebInvoke(UriTemplate = "RemitTax", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse RemitTax(IdInputv2 iObj);
        #endregion RemitTax

        #region MasterTransaction
        [WebInvoke(UriTemplate = "ListMasterTransactions", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListMasterTransactionsResponse ListMasterTransactions(ListMasterTransactionsInput iObj);
        #endregion MasterTransaction

        #region RollOverPot
        [WebInvoke(UriTemplate = "RollOverPot", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListRollOverPotResponse RollOverPot(IdInput iObj);
        #endregion RollOverPot

        #region ChangePasswordEflexUser
        [WebInvoke(UriTemplate = "ChangePasswordEflexUser", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ChangePasswordEflexUser(ChangePasswordEflexUserInput iObj);
        #endregion ChangePasswordEflexUser

        #region ResetBrokerPassword
        [WebInvoke(UriTemplate = "ResetBrokerPassword", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse ResetBrokerPassword(ResetBrokerPasswordInput iObj);
        #endregion ResetBrokerPassword

        #region ListOfBrokerCompaniesForAdmin
        [WebInvoke(UriTemplate = "ListOfBrokerCompaniesForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokerCompaniesResponse ListOfBrokerCompaniesForAdmin(ListOfBrokerCompaniesForAdminInput iObj);
        #endregion ListOfBrokerCompaniesForAdmin

        #region GetPlanDesignDetails
        [WebInvoke(UriTemplate = "GetPlanDesignDetails", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CGetPlanDesignDetailsResponse GetPlanDesignDetails(GetPlanDesignDetailsInput iObj);
        #endregion GetPlanDesignDetails

        #region AddCardFeeRange
        [WebInvoke(UriTemplate = "AddCardFeeRange", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddCardFeeRange(AddCardFeeRangeInput iObj);
        #endregion AddCardFeeRange

        #region ListCardFeeRange
        [WebInvoke(UriTemplate = "ListCardFeeRange", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListCardFeeRangeResponse ListCardFeeRange(IdInput iObj);
        #endregion ListCardFeeRange

        #region EditCardFeeRange
        [WebInvoke(UriTemplate = "EditCardFeeRange", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse EditCardFeeRange(EditCardFeeRangeInput iObj);
        #endregion EditCardFeeRange

        #region DeleteCardFeeRange
        [WebInvoke(UriTemplate = "DeleteCardFeeRange", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteCardFeeRange(IdInputv2 iObj);
        #endregion DeleteCardFeeRange

        #region DeleteCompanyV2
        [WebInvoke(UriTemplate = "DeleteCompanyV2", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse DeleteCompanyV2(IdInputv2 iObj);
        #endregion DeleteCompanyV2

        #region AdminClaimList1
        [WebInvoke(UriTemplate = "AdminClaimList1", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CAdminClaimListResponse AdminClaimList1(AdminClaimListInput iObj);
        #endregion AdminClaimList1

        #region PopulateCarrier
        [WebInvoke(UriTemplate = "PopulateCarrier", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CPopulateCarrierResponse PopulateCarrier();
        #endregion PopulateCarrier

        #region AddEditCarrier
        [WebInvoke(UriTemplate = "AddEditCarrier", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse AddEditCarrier(AddEditCarrierInput iObj);
        #endregion AddEditCarrier

        #region ListCompanyEmployeesV2
        [WebInvoke(UriTemplate = "ListCompanyEmployeesV2", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListCompEmployeesResponse ListCompanyEmployeesV2(ListCompanyEmployeesInput iObj);
        #endregion ListCompanyEmployeesV2

        

        #region CheckOTPAPI
        [WebInvoke(UriTemplate = "CheckOTPAPI", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CResponse CheckOTPAPI();
        #endregion CheckOTPAPI

        #region ListOfBrokerReferralsForAdmin
        [WebInvoke(UriTemplate = "ListOfBrokerReferralsForAdmin", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        CListOfBrokerReferralsResponse ListOfBrokerReferralsForAdmin(LazyLoadingInputV2 iObj);
        #endregion ListOfBrokerReferrals

    }
}