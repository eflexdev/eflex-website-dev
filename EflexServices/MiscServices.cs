﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Xml;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
        #region CreateResponseLog
        private void CreateResponseLog(EflexDBDataContext datacontext, DateTime StartTime, dynamic Id, string ServiceName, string InputJson, string Message)
        {
            TblResponselog Log = new TblResponselog()
            {
                StartTime = StartTime,
                EndTime = DateTime.UtcNow,
                UserID = Id,
                ServiceName = ServiceName,
                Input = InputJson,
                Error = Message
            };
            datacontext.TblResponselogs.InsertOnSubmit(Log);
            datacontext.SubmitChanges();
        }

        #endregion CreateResponseLog

        #region GetConnection
        private string GetConnection(string DbCon)
        {
            string Constring = "";
            try
            {
                ConnectionxmlDoc.Load(ConStrPath);
                XmlNodeList nodes = ConnectionxmlDoc.SelectNodes("/connectionStrings/add[@name='" + DbCon + "']");
                Constring = nodes[0].Attributes["connectionString"].Value.ToString();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return Constring;
        }
        #endregion

        #region GetRandomColor

        private string GetRandomColor1(decimal CompanyId)
        {

            
            Random rnd = new Random();
            Color clr = Color.FromArgb(rnd.Next(200, 255), rnd.Next(150, 255), rnd.Next(150, 255));
            string strColor = "#" + clr.Name.Substring(2);
            try
            {
                if (strColor != null && strColor != "" && strColor != "#FFFFFF")
                {
                    using (MasterDBDataContext datacontext = new MasterDBDataContext())
                    {
                        TblColor lobjClr = (from p in datacontext.TblColors where p.Active == true && p.CompanyId == CompanyId && p.ColorHex == strColor select p).Take(1).SingleOrDefault();
                        if (lobjClr == null)
                        {
                            TblColor obj = new TblColor()
                            {
                                ColorHex = strColor,
                                CompanyId = CompanyId,
                                IsPicked = true,
                                DateCreated = DateTime.UtcNow,
                                Active = true
                            };
                            datacontext.TblColors.InsertOnSubmit(obj);
                            datacontext.SubmitChanges();
                            strColor = obj.ColorHex;
                        }
                        else
                        {
                            GetRandomColor1(CompanyId);
                        }
                        datacontext.Connection.Close();
                    }
                }
                else
                {
                    GetRandomColor1(CompanyId);
                }
            }
            catch { }



            return strColor;
        }

        private string GetRandomColor(decimal CompanyId, decimal CompanyPlanId)
        {

            Random rnd = new Random();
            Color clr = Color.FromArgb(rnd.Next(200, 255), rnd.Next(150, 255), rnd.Next(150, 255));
            string strColor = "#" + clr.Name.Substring(2);
            try
            {
                using (MasterDBDataContext datacontext = new MasterDBDataContext())
                {
                    TblColor lobjClr = datacontext.TblColors.Where(x => x.IsPicked == true && x.CompanyPlanId == CompanyPlanId && x.CompanyId == CompanyId && x.ColorHex == strColor).OrderBy(y => y.ColorId).Take(1).FirstOrDefault();
                    if (lobjClr == null)
                    {
                        TblColor obj = new TblColor()
                        {
                            ColorHex = strColor,
                            CompanyId = CompanyId,
                            CompanyPlanId = CompanyPlanId,
                            IsPicked = true,
                            DateCreated = DateTime.UtcNow,
                            Active = true
                        };
                        datacontext.TblColors.InsertOnSubmit(obj);
                        datacontext.SubmitChanges();
                        strColor = obj.ColorHex;
                    }
                    else
                    {
                        GetRandomColor(CompanyId, CompanyPlanId);
                    }
                    datacontext.Connection.Close();
                }
            }
            catch { }

            return strColor;
        }
        #endregion

        #region GetCompanyEmpData
        private List<CompanyEmpData> GetCompanyEmpData(EflexDBDataContext datacontext, decimal CompanyPlanId)
        {
            List<CompanyEmpData> EmpList = new List<CompanyEmpData>();
            List<TblEmployee> EmployeeList = (from p in datacontext.TblEmployees where p.IsActive == true && p.Active == true && p.CompanyPlanId == CompanyPlanId select p).ToList();
            if (EmployeeList.Count() > 0)
            {
                foreach (TblEmployee item in EmployeeList)
                {
                    List<CompanyEmpData> EmpList1 = (from p in datacontext.TblEmployees
                                                     where p.Active == true && p.IsActive == true &&
                                                     p.CompanyPlanId == CompanyPlanId && p.EmployeeId == item.EmployeeId
                                                     select new CompanyEmpData()
                                                     {
                                                         EmpName = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                                         Contibution = ((from q in datacontext.TblPayments
                                                                         where q.Active == true && q.CompanyUserId == p.CompanyUserId && q.CompanyPlanId == CompanyPlanId
                                                                         && q.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                         select q).Count() > 0 ? ((from q in datacontext.TblPayments
                                                                                                   where q.Active == true && q.CompanyUserId == p.CompanyUserId && q.CompanyPlanId == CompanyPlanId
                                                                                                   && q.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                                   select Convert.ToDecimal(q.Amount ?? 0)).Sum()) : 0)
                                                         }).ToList();

                    EmpList.AddRange(EmpList1);
                }

            }
            return EmpList;
        }
        #endregion

        #region CreateHistoryLog
        private void CreateHistoryLog(MasterDBDataContext datacontext, decimal CompanyId, decimal LoginUserId, decimal CompanyUserId, string Text)
        {
            Tblhistory Obj = new Tblhistory()
            {
                CompanyId = CompanyId,
                LogedUserId = LoginUserId,
                CompanyUserId = CompanyUserId,
                Historytext = Text,
                Active = true,
                DateCreated = DateTime.UtcNow
            };
            datacontext.Tblhistories.InsertOnSubmit(Obj);
            datacontext.SubmitChanges();
        }
        #endregion

        #region EflexNotifyLog
        private void EflexNotifyLog(MasterDBDataContext datacontext, DateTime NotifyDT, string NotifyText, string ForUserIds)
        {
            TblEflexNotifyLog Obj = new TblEflexNotifyLog()
            {
                IsRead = false,
                ForUserId = ForUserIds,
                NotifyText = NotifyText,
                NotifyDate = NotifyDT,
                Active = true,
                DateCreated = DateTime.UtcNow
            };
            datacontext.TblEflexNotifyLogs.InsertOnSubmit(Obj);
            datacontext.SubmitChanges();
        }
        #endregion

        #region CreateMasterResponseLog
        private void CreateMasterResponseLog(MasterDBDataContext datacontext, DateTime StartTime, dynamic Id, string ServiceName, string InputJson, string Message)
        {
            TblMasterResponselog Log = new TblMasterResponselog()
            {
                StartTime = StartTime,
                EndTime = DateTime.UtcNow,
                UserID = Id,
                ServiceName = ServiceName,
                Input = InputJson,
                Error = Message
            };
            datacontext.TblMasterResponselogs.InsertOnSubmit(Log);
            datacontext.SubmitChanges();
        }
        #endregion

        #region GetPctPremium
        private decimal GetPctPremium(EflexDBDataContext datacontext, decimal? ClaimAmt, decimal UserId)
        {
            decimal GetPercentagePremium = 0;
            if (ClaimAmt != 0)
            {
                decimal TotalAmount = 0;

                decimal EmpTranCount = (from p in datacontext.TblEmpTransactions
                                        where p.Active == true &&
                                        (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                        p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() || p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                        && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) && p.CompanyUserId == UserId
                                        select p).Count();

                if (EmpTranCount > 0)
                {

                    TotalAmount = (from p in datacontext.TblEmpTransactions
                                   where p.Active == true &&
                                   (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                   p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() || p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                   && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) && p.CompanyUserId == UserId
                                   select Convert.ToDecimal(p.Amount ?? 0)).Sum();
                    GetPercentagePremium = Convert.ToDecimal((ClaimAmt / TotalAmount) * 100);
                }
            }
            return GetPercentagePremium;
        }
        #endregion

        #region GetIsRefund
        private bool GetIsRefund(decimal ClaimId, EflexDBDataContext datacontext)
        {
            bool Isrefund = false;
            decimal ClaimCount = 0;
            TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == ClaimId select p).Take(1).SingleOrDefault();
            if (ClaimObj != null)
            {
                ClaimCount = (from p in datacontext.TblEmpClaims
                              where p.Active == true
                              && p.DateCreated.Value.Date == ClaimObj.DateCreated.Value.Date
                              && p.CompanyUserId == ClaimObj.CompanyUserId && p.ClaimFee > 0
                              select p).Count();

                if (ClaimCount > 1)
                {
                    Isrefund = true;
                }
                else
                {
                    Isrefund = false;
                }
            }
            return Isrefund;
        }
        #endregion

        #region CreateCardApiResponseLog
        
        private void CreateCardApiResponseLog(decimal? UserId, string ServiceName, string ResponseStatus, dynamic UniqueId, decimal CompanyId, decimal MasterCardId, dynamic ErrorMessage, string InputJson)
        {
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            TblCardApisResponse ApiObj = new TblCardApisResponse()
            {
                UserID = UserId,
                ServiceName = ServiceName,
                Response = ResponseStatus,
                UniqueId = UniqueId,
                CompanyId = CompanyId,
                MasterCardId = MasterCardId,
                Error = ErrorMessage,
                Input = InputJson,
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                Active = true
            };

            mastercontext.TblCardApisResponses.InsertOnSubmit(ApiObj);
            mastercontext.SubmitChanges();
        }
        #endregion

        private List<EmpTransactionData> GetCompEmployeeTransactions(decimal CompanyId, DateTime StartDt, DateTime EndDt, string SearchString)
        {
            List<EmpTransactionData> Obj = new List<EmpTransactionData>();
            string DbCon = DBFlag + CompanyId;
            string Constring = GetConnection(DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    Obj = (from p in datacontext.TblEmpTransactions
                           join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                           where p.Active == true && q.Active == true &&
                           (p.TransactionDate.Value.Date >= StartDt.Date && p.TransactionDate.Value.Date <= EndDt.Date)
                           && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                           select new EmpTransactionData()
                           {
                               EmpName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                               Amount = Convert.ToDecimal(p.Amount),
                               AdjustmentDT = Convert.ToString(p.TransactionDate),
                               Remarks = p.TransactionDetails,
                               Type = (p.TypeId == 1) ? "Credit" : "Debit",
                               FilterType = p.PaymentTypeId
                           }).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
                datacontext.Connection.Close();

            }
            return Obj;
        }

       
        private List<MasterCompDetails> GetCompEmployeeTransactions(decimal CompanyId, string SearchString)
        {
            List<MasterCompDetails> Obj = new List<MasterCompDetails>();
            string DbCon = DBFlag + CompanyId;
            string Constring = GetConnection(DbCon);
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    MasterDBDataContext mastercontext = new MasterDBDataContext();

                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                    List<MasterCompDetails> Obj1 = (from p in datacontext.TblEmpTransactions
                                                    join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                    where p.Active == true && q.Active == true &&
                                                    (p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited))
                                                    select new MasterCompDetails()
                                                    {
                                                        CompanyName = CompanyObj.CompanyName,
                                                        Id = p.EmpTransactionId,
                                                        EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                        Amount = Convert.ToDecimal(p.Amount),
                                                        AdjustmentDT = Convert.ToString(p.TransactionDate),
                                                        Remarks = p.TransactionDetails,
                                                        Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                        FilterType = p.PaymentTypeId,
                                                        PaymentStatus = Convert.ToDecimal(p.PaymentStatus)   
                                                    }).ToList();
                    Obj.AddRange(Obj1);
                    List<MasterCompDetails> Obj2 = (from p in datacontext.TblEmpTransactions
                                                    join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                    where p.Active == true && q.Active == true &&
                                                    p.PaymentTypeId == CObjects.enumPaymentType.WDR.ToString()
                                                    select new MasterCompDetails()
                                                    {
                                                        CompanyName = CompanyObj.CompanyName,
                                                        Id = p.EmpTransactionId,
                                                        EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                        Amount = Convert.ToDecimal(p.Amount),
                                                        AdjustmentDT = Convert.ToString(p.TransactionDate),
                                                        Remarks = p.TransactionDetails,
                                                        Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                        FilterType = p.PaymentTypeId,
                                                        PaymentStatus = Convert.ToDecimal(p.PaymentStatus)   
                                                    }).ToList();
                    Obj.AddRange(Obj2);
                }
                catch (Exception)
                {

                    throw;
                }
                datacontext.Connection.Close();

            }
            return Obj.Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
        }


        private void UpdateEflexMsgUser(TblUser UserObj, string Mode, decimal CompanyId)
        {
            try
            {
                using (EflexMsgDBDataContext datacontext = new EflexMsgDBDataContext())
                {
                    if (Mode.ToUpper() == "UPDATE")
                    {
                        TblMsgUser Obj = (from p in datacontext.TblMsgUsers where p.Active == true && p.OrigUserId == UserObj.UserId && p.UserTypeId == UserObj.UserTypeId && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            Obj.FirstName = UserObj.FirstName;
                            Obj.LastName = UserObj.LastName;
                            Obj.EmailAddress = UserObj.EmailAddress;
                            Obj.UserName = UserObj.UserName;
                            Obj.UserTypeId = UserObj.UserTypeId;
                            Obj.ContactNo = UserObj.ContactNo;
                            Obj.DateModified = DateTime.UtcNow;

                            datacontext.SubmitChanges();
                        }
                    }
                    else if (Mode.ToUpper() == "ADD")
                    {
                        TblMsgUser Obj = new TblMsgUser()
                        {
                            OrigUserId = UserObj.UserId,
                            FirstName = UserObj.FirstName,
                            LastName = UserObj.LastName,
                            EmailAddress = UserObj.EmailAddress,
                            UserName = UserObj.UserName,
                            UserTypeId = UserObj.UserTypeId,
                            ContactNo = UserObj.ContactNo,
                            IsActive = true,
                            DateModified = DateTime.UtcNow,
                            DateCreated = DateTime.UtcNow,
                            Active = true,
                            CompanyId = CompanyId
                        };
                        datacontext.TblMsgUsers.InsertOnSubmit(Obj);
                        datacontext.SubmitChanges();
                    }
                    else
                    {
                        if (UserObj != null)
                        {
                            TblMsgUser Obj = (from p in datacontext.TblMsgUsers where p.Active == true && p.OrigUserId == UserObj.UserId && p.CompanyId == CompanyId && p.UserTypeId == UserObj.UserTypeId select p).Take(1).SingleOrDefault();
                            if (Obj != null)
                                Obj.Active = false;

                            List<TblConversation> ConversationObj = (from p in datacontext.TblConversations
                                                                     where p.Active == true
                                                                         && (p.User1 == Obj.UserId || p.User2 == Obj.UserId)
                                                                     select p).ToList();
                            if (ConversationObj != null && ConversationObj.Count() > 0)
                                ConversationObj.ForEach(x => x.Active = false);

                            datacontext.SubmitChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message + "Update");
            }
        }
        public bool IsValidateEflexUser(MasterDBDataContext datacontext)
        {
            bool IsRequestValid = false;
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            string HeaderToken = woc.Headers["TokenHeader"];
            TblUser UserObj = (from p in datacontext.TblUsers where p.Active == true && p.UserId == UserId select p).Take(1).SingleOrDefault();
            if (UserObj != null)
            {
                if (UserObj.UserToken == HeaderToken)
                {
                    IsRequestValid = true;
                }
                else
                {
                    IsRequestValid = false;
                }
            }

            return IsRequestValid;
        }
        private void NotifyUser1(EflexDBDataContext datacontext, string NotifyText, decimal? userId,string NotifyType)
        {
            TblNotifyLog lobj = new TblNotifyLog
            {
                NotifyText = NotifyText,
                IsRead = false,
                DateCreated = DateTime.UtcNow,
                NotifyType = NotifyType,
                Active = true,
                ForUserId = Convert.ToDecimal(userId)
            };

            datacontext.TblNotifyLogs.InsertOnSubmit(lobj);
            datacontext.SubmitChanges();

        }
        private OtherUser GetOtherUserDetails(EflexMsgDBDataContext datacontext, decimal CId, decimal MsgUserId)
        {
            OtherUser User = null;
            User = (from p in datacontext.sp_GetOtherUserDetails(CId, MsgUserId)
                    select new OtherUser
                    {
                        Name = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                        Username = p.Username,
                        Email = p.EmailAddress,
                        Phone = p.ContactNo,
                        Pic = p.Pic,
                        UserTypeId = Convert.ToInt32(p.UserTypeId),
                        CompanyId = Convert.ToDecimal(p.CompanyId)
                    }).Take(1).FirstOrDefault();

            return User;
        }
        private List<ConvMsgs> GetMessages(EflexMsgDBDataContext datacontext, decimal CId, decimal MsgUserId, decimal ChunkStart, int ChunkSize, decimal CompCId)
        {
            List<ConvMsgs> Msgs = null;

            if (CId == CompCId)
            {
                Msgs = (from a in datacontext.sp_ListMessages(CId, MsgUserId, ChunkStart, ChunkSize)
                        select new ConvMsgs
                        {
                            MsgId = a.MsgId,
                            Msg = a.Msg,
                            MsgFile = a.MsgFile,
                            MsgDate = a.MsgDate.ToString(),
                            FileType = a.FileType,
                            SenderName = a.Name,
                            AmISender = Convert.ToBoolean(a.AmISender),
                            SenderPic = a.Pic,
                            SenderUserTypeId = Convert.ToInt16(a.UserTypeId)
                        }).ToList();

            }
            return Msgs;
        }
        public bool IsBlock(string DbCon)
        {
            bool IsBlock = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                decimal CompanyId = Convert.ToDecimal(DbCon.Split('_')[1]);
                TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                if (CompanyObj != null)
                {
                    if (!Convert.ToBoolean(CompanyObj.IsBlocked))
                    {
                        IsBlock = true;
                    }

                }
                mastercontext.Connection.Close();
            }
            return IsBlock;
        }

        #region GetRandomPassword
        public static string RandomString()
        {
            int length = 10;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion GetRandomPassword

        private string GetCompanyName(decimal? CompanyId)
        {
            string CompanyName = "";
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            CompanyName = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p.CompanyName).Take(1).SingleOrDefault();
            return CompanyName;
        }

        private decimal GetTotalCommission(decimal? UserId)
        {
            decimal Commission = 0;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                decimal Count = (from a in mastercontext.TblBrokerPayments where a.Active == true && a.UserId == UserId select a).Count();
                if (Count > 0)
                {
                    Commission = (from a in mastercontext.TblBrokerPayments where a.Active == true && a.UserId == UserId select Convert.ToDecimal(a.Amount)).Sum();
                }
                mastercontext.Connection.Close();
            }
            return Commission;
        }

        private string GetEmpName(decimal? CompanyId, decimal? UserId)
        {
            string DbCon = DBFlag + CompanyId;
            string Constring = GetConnection(DbCon);
            string EmpName = "";
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                EmpName = (from p in datacontext.TblEmployees where p.CompanyUserId == UserId && p.Active == true select p.FirstName + " " + p.LastName).Take(1).SingleOrDefault();
                datacontext.Connection.Close();
           
            }
            return EmpName;
        }

        private void AddCompanyUserDevice(EflexDBDataContext dc, decimal DeviceTypeId, decimal UserId, string FCMToken, string Ver, string DeviceId, string DeviceName, string OSVersion)
        {
            List<TblCompanyUserDevice> userDevObj = new List<TblCompanyUserDevice>();

            try
            {
                ///// CHECK IF DEVICE ALREADY EXIST
                userDevObj = (from p in dc.TblCompanyUserDevices where p.Active == true && (p.DeviceId == null ? (p.CompanyUserId == UserId && p.FCMToken == FCMToken) : p.DeviceId == DeviceId) select p).ToList();

                if (userDevObj != null)
                {
                    dc.TblCompanyUserDevices.DeleteAllOnSubmit(userDevObj);
                    dc.SubmitChanges();
                }

                ///// ADD A NEW DEVICE FOR THE USER
                //Adding User Device
                TblCompanyUserDevice DeviceObj = new TblCompanyUserDevice()
                {
                    CompanyUserId = UserId,
                    DeviceTypeId = DeviceTypeId,
                    DeviceId = DeviceId,
                    FCMToken = FCMToken,
                    Ver = Ver,
                    OSVversion = OSVersion,
                    DeviceName = DeviceName,
                    DateModified = DateTime.UtcNow,
                    DateCreated = DateTime.UtcNow,
                    Active = true
                };
                dc.TblCompanyUserDevices.InsertOnSubmit(DeviceObj);
                dc.SubmitChanges();

            }//End try
            catch (Exception ex)
            {
                throw new Exception("Error while adding device info." + ex.Message);
            }

        }

        public static string DecodeBase64(string encodedString)
        {
            if (encodedString != null)
            {
                byte[] data = Convert.FromBase64String(encodedString);
                string decodedString = Encoding.UTF8.GetString(data);
                return decodedString;
            }
            else
            {
                return null;
            }

        }

        #region getRandomID
        private decimal getRandomID(MasterDBDataContext datacontext, string DbCon, decimal userid)
        {
            Random r = new Random();
            Int32 RandomCode = r.Next(100000, 999999);
            TblRandomCodeLog RandomCodeObj;

            List<TblRandomCodeLog> ranCodelog = new List<TblRandomCodeLog>();

            ranCodelog = (from p in datacontext.TblRandomCodeLogs where p.Active == true && p.ValidTill <= DateTime.UtcNow select p).ToList();
            if (ranCodelog.Count > 0)
            {
                datacontext.TblRandomCodeLogs.DeleteAllOnSubmit(ranCodelog);
                datacontext.SubmitChanges();
            }

            ranCodelog = new List<TblRandomCodeLog>();
            ranCodelog = (from p in datacontext.TblRandomCodeLogs where p.Active == true && p.UserId == userid select p).ToList();
            if (ranCodelog.Count > 0)
            {
                datacontext.TblRandomCodeLogs.DeleteAllOnSubmit(ranCodelog);
                datacontext.SubmitChanges();
            }
            RandomCodeObj = (from p in datacontext.TblRandomCodeLogs where p.Active == true && p.RandomCode == Convert.ToString(RandomCode) select p).Take(1).SingleOrDefault();
            if (RandomCodeObj != null)
            {
                
                getRandomID(datacontext, DbCon, userid);
                
            }
            else
            {
                RandomCodeObj = new TblRandomCodeLog()
                {
                    DbCon = DbCon,
                    RandomCode = RandomCode.ToString(),
                    UserId = userid,
                    ValidTill = DateTime.UtcNow.AddDays(1),
                    DateCreated = DateTime.UtcNow,
                    Active = true,
                };
                datacontext.TblRandomCodeLogs.InsertOnSubmit(RandomCodeObj);
                datacontext.SubmitChanges();


            }
            return RandomCode;

        }

        #endregion getRandomID

        private string GetUserSessionId()
        {
            string SessionId = "";
            try
            {

                string Pingstruri = "";
                Pingstruri = BaseUrl + "Ping/";
                HttpWebRequest webRequest = HttpWebRequest.Create(new Uri(Pingstruri)) as HttpWebRequest;
                webRequest.Method = System.Net.WebRequestMethods.Http.Get;
                webRequest.ContentLength = 0;
                webRequest.ContentType = "application/json; charset=utf-8";
                webRequest.KeepAlive = false;
                HttpWebResponse webresponse;
                webresponse = (HttpWebResponse)webRequest.GetResponse();
                StreamReader sr = new StreamReader(webresponse.GetResponseStream());
                string text = sr.ReadToEnd().ToString();
                dynamic Pingresult = JsonConvert.DeserializeObject<dynamic>(text);
                if (Pingresult.Result == true)
                {
                    //call login api
                    
                    string LoginUri = BaseUrl + "Login/" + UserName + "/" + Password;
                    HttpWebRequest webRequest1 = HttpWebRequest.Create(new Uri(LoginUri)) as HttpWebRequest;
                    webRequest1.Method = System.Net.WebRequestMethods.Http.Get;
                    webRequest1.ContentLength = 0;
                    webRequest1.ContentType = "application/json; charset=utf-8";
                    webRequest1.KeepAlive = false;
                    HttpWebResponse webresponse1;
                    webresponse1 = (HttpWebResponse)webRequest1.GetResponse();
                    StreamReader sr1 = new StreamReader(webresponse1.GetResponseStream());
                    string text1 = sr1.ReadToEnd().ToString();
                    dynamic Loginresult = JsonConvert.DeserializeObject<dynamic>(text1);
                    if (Loginresult.Result.ToString() != null && Loginresult.Result.ToString() != "")
                    {
                        SessionId = Loginresult.Result;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return SessionId;
        }

        private decimal GetBrokerComissionCharges(decimal CompanyId)
        {
            string DbCon = DBFlag + CompanyId;
            string Constring = GetConnection(DbCon);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            decimal PercentageBrokerCharge = 0;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    #region Broker Commission
                    TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                       where p.Active == true && p.CompanyId == CompanyId
                                                       select p).Take(1).SingleOrDefault();
                    if (CompReferral != null)
                    {
                       
                        if (CompReferral.BrokerId != -1)
                        {
                            TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                   where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                   select p).Take(1).SingleOrDefault();
                            TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                          where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId==CompanyId
                                                          select p).Take(1).SingleOrDefault();
                            
                            if (FeeObj != null)
                            {
                                if (FeeObj.BrokerFee > 0)
                                {
                                    PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                }
                            }
                            else
                            {
                                if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                {
                                    PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                }
                                else
                                {
                                    PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                }
                            }


                          
                        }
                    }

                    #endregion Broker Commission
                }
                catch (Exception)
                {

                    throw;
                }
                datacontext.Connection.Close();

            }

            return PercentageBrokerCharge;

        }

        private decimal GetBrokerCompanyComission(decimal CompanyId)
        {
            string DbCon = DBFlag + CompanyId;
            string Constring = GetConnection(DbCon);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            decimal PercentageBrokerCharge = 0;
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    #region Broker Commission
                    TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                       where p.Active == true && p.CompanyId == CompanyId
                                                       select p).Take(1).SingleOrDefault();
                    if (CompReferral != null)
                    {

                        if (CompReferral.BrokerId != -1)
                        {
                            TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                   where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                   select p).Take(1).SingleOrDefault();
                            TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                          where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyId
                                                          select p).Take(1).SingleOrDefault();

                            if (FeeObj != null)
                            {
                                if (FeeObj.BrokerFee > 0)
                                {
                                    PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                }
                            }
                            else
                            {
                                if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                {
                                    PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                }
                                else
                                {
                                    PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                }
                            }



                        }
                    }
                    else
                    {
                        PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                              
                    }

                    #endregion Broker Commission
                }
                catch (Exception)
                {

                    throw;
                }
                datacontext.Connection.Close();

            }

            return PercentageBrokerCharge;

        }

        #region InsertionInTaskNotificationTable

        private List<TblNotificationDetail> InsertionInNotificationTable(decimal? userId, string lstrMessage, EflexDBDataContext datacontext, decimal? TypeId, string Type)
        {
            List<TblCompanyUserDevice> UserDeviceObj = new List<TblCompanyUserDevice>();
            List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
            TblNotificationDetail NotificationDetailsObj;
            decimal UnreadNotificationCount = -1;
            try
            {
                UserDeviceObj = (from p in datacontext.TblCompanyUserDevices where p.Active == true && p.CompanyUserId == userId select p).Distinct().ToList();
                if (UserDeviceObj != null && UserDeviceObj.Count > 0)
                {
                    UnreadNotificationCount = datacontext.TblNotifyLogs.Where(x => x.Active == true && x.ForUserId == userId && x.IsRead == false).Count();
                    foreach (var userDevicedata in UserDeviceObj)
                    {
                        if (!string.IsNullOrEmpty(userDevicedata.FCMToken))
                        {
                            NotificationDetailsObj = new TblNotificationDetail()
                            {
                                NotificationText = lstrMessage,
                                Id = TypeId,
                                UserId = Convert.ToDecimal(userId),
                                BadgeCount = UnreadNotificationCount,
                                FCMToken = userDevicedata.FCMToken,
                                DeviceTypeId = userDevicedata.DeviceTypeId,
                                Details = Type,
                                IsSent = 0,
                                Type = Type,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                            };
                            NotificationList.Add(NotificationDetailsObj);
                        }
                    }

                    datacontext.TblNotificationDetails.InsertAllOnSubmit(NotificationList);
                    datacontext.SubmitChanges();
                }

                return NotificationList;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion




        #region SendNotification
        private void SendNotification(EflexDBDataContext datacontext, List<TblNotificationDetail> NotificationList, decimal? TypeId)
        {
            string ApiKey = "";
            try
            {
                ApiKey = WebConfigurationManager.AppSettings["FCMKey"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error while getting FCMkey." + ex.Message);
            }

            string FCMTokens = "";
            string heading = "";

            int NoOfFCMTokens = 0;
            decimal BadgeCount = -1;
            string Type = "";
            List<decimal> UserIds = NotificationList.Select(x => x.UserId).Distinct().ToList();
            if (UserIds != null && UserIds.Count() > 0)
            {
                foreach (decimal uid in UserIds)
                {
                    FCMTokens = "";
                    List<TblNotificationDetail> NotiList = NotificationList.Where(x => x.UserId == uid).Select(x => x).ToList();
                    foreach (TblNotificationDetail item in NotiList)
                    {
                        FCMTokens += item.FCMToken + ",";
                        NoOfFCMTokens++;
                    }

                    if (FCMTokens != "")
                    {
                        FCMTokens = FCMTokens.Substring(0, FCMTokens.Length - 1);
                        heading = NotiList[0].NotificationText;
                        BadgeCount = Convert.ToDecimal(NotiList[0].BadgeCount);
                        Type = NotiList[0].Type;
                        try
                        {
                            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                            tRequest.Method = "post";
                            tRequest.ContentType = "application/json";
                           
                            tRequest.Headers.Add("Authorization: key=" + ApiKey + "");

                            var data = new
                            {
                                registration_ids = new List<String>(FCMTokens.Split(',')),
                                notification = new
                                {
                                    body = heading,
                                    title = "eFlex",
                                    icon = "ic_app_logo",
                                    sound = "Default",
                                    badge = BadgeCount,
                                    type = Type,
                                    click_action= Type

                                },
                                data = new
                                {
                                    heading = heading,
                                    name = "eFlex",
                                    detail = heading,
                                    id = TypeId,
                                    type = Type
                                }

                            };

                            var serializer = new JavaScriptSerializer();
                            var json = serializer.Serialize(data);

                            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                            tRequest.ContentLength = byteArray.Length;

                            using (Stream dataStream = tRequest.GetRequestStream())
                            {
                                dataStream.Write(byteArray, 0, byteArray.Length);

                                using (WebResponse tResponse = tRequest.GetResponse())
                                {
                                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                    {
                                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                        {
                                            String sResponseFromServer = tReader.ReadToEnd();
                                            ResponseResult res = (ResponseResult)JsonConvert.DeserializeObject(sResponseFromServer, typeof(ResponseResult));

                                            //If Notification has been delivered to all devices successfully
                                            if (res.failure == 0)
                                            {
                                                UpdateNotificationDetail(datacontext, NotiList, 1);

                                            }
                                            else
                                            {
                                                //Else if there is failure for any device
                                                for (int i = 0; i < res.results.Count; i++)
                                                {
                                                    if (res.results[i].error != null)
                                                    {
                                                        TblNotificationDetail lobj = datacontext.TblNotificationDetails.Where(p => p.NotificationDetailId == NotiList[i].NotificationDetailId).Take(1).FirstOrDefault();
                                                        lobj.Details += res.results[i].error;
                                                        lobj.IsSent = 2;
                                                        lobj.DateCreated = DateTime.UtcNow;
                                                    }
                                                    else
                                                    {
                                                        TblNotificationDetail lobj = datacontext.TblNotificationDetails.Where(p => p.NotificationDetailId == NotiList[i].NotificationDetailId).Take(1).FirstOrDefault();
                                                        lobj.IsSent = 1;
                                                        lobj.DateCreated = DateTime.UtcNow;
                                                    }
                                                }

                                            }


                                            if (res.canonical_ids > 0)
                                            {
                                                for (int i = 0; i < res.results.Count; i++)
                                                {
                                                    if (res.results[i].registration_id != null)
                                                    {
                                                        TblCompanyUserDevice lobj = datacontext.TblCompanyUserDevices.Where(p => p.FCMToken == NotiList[i].FCMToken).Take(1).FirstOrDefault();
                                                        lobj.FCMToken = res.results[i].registration_id;
                                                        lobj.DateCreated = DateTime.UtcNow;
                                                    }

                                                }
                                            }
                                            datacontext.SubmitChanges();



                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            
                        }
                    }
                    NotiList.Clear();
                }
            }



        }

        #endregion

        private void UpdateNotificationDetail(EflexDBDataContext datacontext, List<TblNotificationDetail> NotificationList, int issent)
        {
            foreach (TblNotificationDetail item in NotificationList)
            {
                item.IsSent = issent;
                datacontext.SubmitChanges();
            }
        }
  
    }
}