﻿using EnCryptDecrypt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Xml;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace EflexServices
{
    public partial class EflexService : IEflexService
    {
      
        #region LoginEflexUser
        //JK:260518
        public CLoginEflexUserResponse LoginEflexUser(LoginUserInput iObj)
        {

            DateTime StartTime = DateTime.UtcNow;
            CLoginEflexUserResponse lobjResponse = new CLoginEflexUserResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                decimal CompanyId = -1;
                string UserToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                bool IsApproaved = true;
                try
                {
                    TblUser UserObj = (from p in datacontext.TblUsers where p.Active == true && p.UserName == iObj.UserName && p.Pwd == CryptorEngine.Encrypt(iObj.Password) select p).Take(1).SingleOrDefault();
                    if (UserObj != null)
                    {

                        if (UserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) || UserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin))
                        {
                            UserObj.UserToken = UserToken;
                            datacontext.SubmitChanges();
                            string FirstName = "";
                            string LastName = "";
                            bool IsPersonal = false;

                            FirstName = UserObj.FirstName;
                            LastName = UserObj.LastName;
                            UserId = UserObj.UserId;
                            lobjResponse.FirstName = FirstName;
                            lobjResponse.LastName = LastName;
                            lobjResponse.IsPersonal = IsPersonal;
                            lobjResponse.IsApproaved = IsApproaved;
                            lobjResponse.ID = UserObj.UserId;
                            lobjResponse.UserName = UserObj.FirstName + " " + UserObj.LastName;
                            lobjResponse.UserTypeId = Convert.ToDecimal(UserObj.UserTypeId);
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.CompanyId = CompanyId;
                            lobjResponse.UserToken = UserToken;
                            lobjResponse.Message = "Login Successfully.";
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {

                            lobjResponse.Message = "Please enter valid login credentials.";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                        
                    }
                    else
                    {

                        lobjResponse.Message = "Please input correct Username/Password.";
                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        
                    }

                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

                CreateMasterResponseLog(datacontext, StartTime, UserId, "LoginEflexUser", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }



        #endregion LoginEflexUser

        #region GetDashboardData
        /// <summary>
        /// Author:Jasmeet Kaur
        /// Get dashboard figures data
        /// </summary>
        /// <returns></returns>
        public CGetDashboardDataResponse GetDashboardData(IdInput iObj)
        {
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CGetDashboardDataResponse lobjResponse = new CGetDashboardDataResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                UserId = iObj.Id;
                if (IsValidateEflexUser(mastercontext))
                {
                    decimal TotalCompanies = 0;
                    decimal TotalEmloyees = 0;
                    decimal TotalBrokers = 0;
                    decimal TotalClaims = 0;
                    decimal PendingTotalClaims = 0;
                    decimal TotalPayments = 0;

                    List<TblCompany> CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true select p).ToList();
                    TotalBrokers = (from p in mastercontext.TblBrokers where p.Active == true select p).Count();

                    if (CompanyObj != null && CompanyObj.Count() > 0)
                    {
                        TotalCompanies = CompanyObj.Count();
                        foreach (TblCompany item in CompanyObj)
                        {
                            string Constring = GetConnection(item.DBCon);
                            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                            {
                                try
                                {
                                    #region Figures
                                    List<TblEmployee> EmpObj = (from p in datacontext.TblEmployees where p.Active == true select p).ToList();
                                    if (EmpObj != null && EmpObj.Count() > 0)
                                    {
                                        decimal CompEmpCount = (from p in datacontext.TblEmployees where p.Active == true && p.IsActive == true select p).Count();
                                        TotalEmloyees = TotalEmloyees + CompEmpCount;
                                    }
                                    List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true select p).ToList();
                                    if (ClaimObj != null && ClaimObj.Count() > 0)
                                    {
                                        decimal ClaimCount = (from p in datacontext.TblEmpClaims where p.Active == true select p).Count();
                                        TotalClaims = TotalClaims + ClaimCount;
                                    }
                                    List<TblEmpClaim> PendingClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress) select p).ToList();
                                    if (PendingClaimObj != null && PendingClaimObj.Count() > 0)
                                    {
                                        decimal PendingClaimCount = (from p in datacontext.TblEmpClaims where p.Active == true && p.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress) select p).Count();
                                        PendingTotalClaims = PendingTotalClaims + PendingClaimCount;
                                    }
                                    List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) select p).ToList();
                                    if (PaymentObj != null && PaymentObj.Count() > 0)
                                    {
                                        decimal Payments = (from p in datacontext.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) select Convert.ToDecimal(p.Amount)).Sum();
                                        TotalPayments = TotalPayments + Payments;
                                    }
                                    #endregion

                                    
                                }
                                catch (Exception ex)
                                {

                                    lobjResponse.Message = ex.Message;
                                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                }
                                datacontext.Connection.Close();
                            }
                        }

                        List<ReceivedPaymentData> RecPaymentObj = (from p in mastercontext.TblCompPayments 
                                                                   join q in mastercontext.TblCompanies on p.CompanyId equals q.CompanyId
                                                                   where p.Active == true && q.Active == true
                                                                   && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited) 
                                                                   orderby p.CompPaymentId descending
                                                                   select new ReceivedPaymentData()
                                                                   {
                                                                       PaymentDate = Convert.ToString(p.PaymentDate),
                                                                       PaymentDueDate = Convert.ToString(p.PaymentDueDate),
                                                                       PaymentDetails = p.PaymentDetails,
                                                                       CompanyName = q.CompanyName,
                                                                       Amount = Convert.ToDecimal(p.Amount)
                                                                   }).Take(10).ToList();

                        List<DuePaymentData> DuePaymentObj = (from p in mastercontext.TblCompPayments
                                                              join q in mastercontext.TblCompanies on p.CompanyId equals q.CompanyId
                                                              where p.Active == true && q.Active == true
                                                              && (p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) || p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.PartialPaid))
                                                              orderby p.CompPaymentId descending     
                                                              select new DuePaymentData()
                                                                   {
                                                                       PaymentDate = Convert.ToString(p.PaymentDate),
                                                                       PaymentDueDate = Convert.ToString(p.PaymentDueDate),
                                                                       PaymentDetails = p.PaymentDetails,
                                                                       CompanyName = q.CompanyName,
                                                                       Amount = Convert.ToDecimal(p.Amount)
                                                                   }).Take(10).ToList();
                        lobjResponse.TotalCompany = TotalCompanies;
                        lobjResponse.TotalBrokers = TotalBrokers;
                        lobjResponse.TotalEmployess = TotalEmloyees;
                        lobjResponse.TotalClaims = TotalClaims;
                        lobjResponse.TotalPendingClaims = PendingTotalClaims;
                        lobjResponse.ReceivedPaymentData = RecPaymentObj;
                        lobjResponse.DuePaymentData = DuePaymentObj;
                       lobjResponse.TotalPayments = Convert.ToDecimal(String.Format("{0:0.00}", TotalPayments));

                       
                    }
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
              
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region PopulateCompany
        public CPopulateCompanyResponse PopulateCompany()
        {
            CPopulateCompanyResponse lobjResponse = new CPopulateCompanyResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    List<PopulateCompany> Obj = (from p in datacontext.TblCompanies
                                                 where p.Active == true
                                                 select new PopulateCompany()
                                                 {
                                                     CompanyId = p.CompanyId,
                                                     CompanyName = p.CompanyName,
                                                     DbCon = p.DBCon
                                                 }).Distinct().ToList();
                    lobjResponse.PopulateCompany = Obj;
                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion PopulateReferrals

        #region AdminClaimList
        public CAdminClaimListResponse AdminClaimList(AdminClaimListInput iObj)
        {
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CAdminClaimListResponse lobjResponse = new CAdminClaimListResponse();
            string DbCon = DBFlag + iObj.Id;
            string Constring = GetConnection(DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            if (IsValidateEflexUser(mastercontext))
            {
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        string CompanyName = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p.CompanyName).Take(1).SingleOrDefault();
                        decimal? StatusId = null;
                        if (iObj.StatusId != -1)
                        {
                            StatusId = iObj.StatusId;
                        }
                        List<AdminClaimList> Obj = (from p in datacontext.sp_AdminClaimList(iObj.ChunckStart, iObj.SearchString, Convert.ToInt32(iObj.ChunckSize), StatusId,iObj.Ismanual)
                                                    select new AdminClaimList()
                                                    {
                                                        EmpClaimId = p.EmpClaimId,
                                                        Employee = p.Employee,
                                                        Dependent = p.Dependant,
                                                        ClaimDt = Convert.ToString(p.ClaimDT),
                                                        ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                        Company = CompanyName,
                                                        Receipt = p.ReceiptNo,
                                                        ReceiptDT = Convert.ToString(p.ReceiptDate),
                                                        FileName = p.UploadFileName,
                                                        Note = p.Note,
                                                        Status = p.ClaimStatus,
                                                        CRA = Convert.ToDecimal(p.CRA),
                                                        NonCRA = Convert.ToDecimal(p.NonCRA),
                                                        IsManual = Convert.ToBoolean(p.IsManual),
                                                        IsRefund = p.ClaimFee > 0 ? true :false,
                                                        GetPAPDetails = GetEmpBankDetails(datacontext,p.CompanyUserId),
                                                        ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                               join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                               where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                               select new ClaimServiceDetails()
                                                                               {
                                                                                   ServiceName = a.Title,
                                                                                   ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                   Address = q.Address,
                                                                                   PhoneNo = q.ContactNo,
                                                                                   Email = q.Email,
                                                                                   ServiceProvider = q.Name
                                                                               }).Take(1).SingleOrDefault()
                                                    }).ToList();
                        lobjResponse.AdminClaimList = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    datacontext.Connection.Close();
                }
              
             
            }
            else
            {
                IsLogOut = true;
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                lobjResponse.Message =  CObjects.SessionExpired.ToString();
              
            }
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }

       
        private GetPAPDetails GetEmpBankDetails(EflexDBDataContext datacontext,decimal? CompanyUserId)
        {
            GetPAPDetails Obj = new GetPAPDetails();
            try
            {

                TblPAPDetail PAPObj = (from p in datacontext.TblPAPDetails where p.Active == true && p.CompanyUserId == CompanyUserId select p).Take(1).SingleOrDefault();
                if (PAPObj != null)
                {

                    Obj.PAPDetailId = Convert.ToDecimal(PAPObj.PAPDetailId);
                    Obj.CompanyUserId = Convert.ToDecimal(PAPObj.CompanyUserId);
                    Obj.BankName = PAPObj.BankName;
                    Obj.TransitId = PAPObj.TransitId;
                    Obj.InstitutionId = PAPObj.InstitutionId;
                    Obj.ChequeFile = PAPObj.ChequeFile;
                    Obj.AccountNumber = PAPObj.AccountNumber;
                    Obj.IsCheque = Convert.ToString(PAPObj.IsCheque);
                    Obj.IsAgree = Convert.ToString(PAPObj.IsAgree);

                }

            }
            catch (Exception)
            {

                throw;
            }
           
            return Obj;
        }

        private GetPAPDetails GetUserBankDetails(MasterDBDataContext datacontext, decimal? UserId)
        {
            GetPAPDetails Obj = new GetPAPDetails();
            try
            {

                TblUserPAPDetail PAPObj = (from p in datacontext.TblUserPAPDetails where p.Active == true && p.UserId == UserId select p).Take(1).SingleOrDefault();
                if (PAPObj != null)
                {

                    Obj.PAPDetailId = Convert.ToDecimal(PAPObj.PAPDetailId);
                    Obj.CompanyUserId = Convert.ToDecimal(PAPObj.UserId);
                    Obj.BankName = PAPObj.BankName;
                    Obj.TransitId = PAPObj.TransitId;
                    Obj.InstitutionId = PAPObj.InstitutionId;
                    Obj.AccountNumber = PAPObj.AccountNumber;
                   
                }
                else
                {
                    Obj = new GetPAPDetails();
                }

            }
            catch (Exception)
            {

                throw;
            }

            return Obj;
        }
        #endregion

        #region UpdateClaimStatus
        public CResponse UpdateClaimStatus(UpdateClaimStatusInput iObj)
        {
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        string DbCon = DBFlag + iObj.CompanyId;
                        string Constring = GetConnection(DbCon);
                        bool IsRefund = false;
                        string CompanyName = (from a in mastercontext.TblCompanies where a.CompanyId == iObj.CompanyId && a.Active == true select a.CompanyName).Take(1).SingleOrDefault();
                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            TblEmpClaim ClaimObj = (from p in datacontext.TblEmpClaims where p.Active == true && p.EmpClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                            if (ClaimObj != null)
                            {
                                ClaimObj.ClaimStatusId = iObj.StatusId;
                                ClaimObj.DateModified = DateTime.UtcNow;
                                bool IsEmailSent = false;
                                string NotifyText = "";
                                string EmailSubject = "";
                                string EmailContent = "";
                                string Text = "";
                                TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == ClaimObj.CompanyUserId select p).Take(1).SingleOrDefault();

                                if(iObj.StatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress))
                                {
                                    EmailSubject = "Claim In-Progress";
                                    NotifyText = "Your claim has been processed.";
                                    Text = "Claim is in progress for employee named as " + UserObj.FirstName + " " + UserObj.LastName + ".";
                                    EmailContent = "Your claim has been processed.For futher queries you can contact with ADMIN.";                                }
                                else if(iObj.StatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid))
                                {
                                    TblEmpTransaction TranObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.ClaimId == ClaimObj.EmpClaimId select p).Take(1).SingleOrDefault();
                                    if(TranObj != null)
                                    {
                                        TranObj.TransactionDetails = "Claim Paid";
                                        TranObj.PaymentStatus = Convert.ToInt16(CObjects.enumPaymentStatus.Deposited);
                                        datacontext.SubmitChanges();
                                    }

                                    EmailSubject = "Claim amount credited";
                                    NotifyText = "Your recent eFlex claim for " + ClaimObj.Amount + " has been paid. For details, please login to your account.";
                                    
                                    Text = "Claim amount has been credited to employee " + UserObj.FirstName + " " + UserObj.LastName + "'s account.";
                                    EmailContent = "Claim amount has been credited in your account. If you have any query, you can contact with ADMIN.";
                                    
                                }
                                else if(iObj.StatusId == Convert.ToInt16(CObjects.enumClaimStatus.IneligibleClaim))
                                {
                                    EmailSubject = "Ineligible Claim Request";
                                    NotifyText = "You are not eligible for claim.";
                                    Text = "Employee named as " + UserObj.FirstName + " " + UserObj.LastName + " is marked as ineleigible for claim.";
                                    IsRefund = true;
                                    EmailContent = "Your are not eligible for this claim. For futher queries you can contact with ADMIN.";

                                }
                                else{
                                    NotifyText = "Your claim request has been cancelled on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                                     Text = "Claim request has been cancelled by Admin for employee named as " + UserObj.FirstName + " " + UserObj.LastName + " is marked as ineleigible for claim.";
                                     IsRefund = true;
                                     EmailSubject = "Claim request Cancelled";
                                     EmailContent = "Your claim request has been cancelled by the ADMIN . For futher queries you can contact with ADMIN.";

                                   
                                }

                                #region Refund
                                 if(IsRefund)
                                 {
                                     decimal ClaimAmount = 0;
                                     if(iObj.IsRefundClaimFee)
                                     {
                                         ClaimAmount = Convert.ToDecimal(ClaimObj.Amount);

                                         TblEmpBalance BalanceObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == ClaimObj.CompanyUserId select p).Take(1).SingleOrDefault();

                                         TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                         {
                                             TransactionDate = DateTime.UtcNow,
                                             TransactionDetails = "Claim fee charged for the claim has been refunded to your account.",
                                             Amount = ClaimObj.ClaimFee,
                                             CompanyUserId = ClaimObj.CompanyUserId,
                                             CompanyPlanId = -1,
                                             ClaimId = ClaimObj.EmpClaimId,
                                             TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                             PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                             PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                             Active = true,
                                             DateCreated = DateTime.UtcNow
                                         };
                                         datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                         TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                         {
                                             CompanyUserId = ClaimObj.CompanyUserId,
                                             Amount = ClaimObj.ClaimFee,
                                             AdjustmentDT = DateTime.UtcNow,
                                             Remarks = "Claim fee charged for the claim has been refunded to your account.",
                                             TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Credit),
                                             Active = true,
                                             DateCreated = DateTime.UtcNow,
                                             DateModified = DateTime.UtcNow
                                         };
                                         datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);

                                         BalanceObj.Amount = BalanceObj.Amount + ClaimObj.ClaimFee;
                                     }
                                     else
                                     {
                                         ClaimAmount = Convert.ToDecimal(ClaimObj.Amount);
                                     }
                                     TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                     {
                                         TransactionDate = DateTime.UtcNow,
                                         TransactionDetails = "Claim amount has been refunded to your account.",
                                         Amount = ClaimObj.Amount,
                                         CompanyUserId = ClaimObj.CompanyUserId,
                                         CompanyPlanId = -1,
                                         ClaimId = ClaimObj.EmpClaimId,
                                         TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                         PaymentTypeId = CObjects.enumPaymentType.WDR.ToString(),
                                         PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                         Active = true,
                                         DateCreated = DateTime.UtcNow
                                     };
                                     datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                     TblEmpAdjustment AdjObj1 = new TblEmpAdjustment()
                                     {
                                         CompanyUserId = ClaimObj.CompanyUserId,
                                         Amount = ClaimObj.Amount,
                                         AdjustmentDT = DateTime.UtcNow,

                                         Remarks = "Claim amount has been refunded to your account.",
                                         TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Credit),
                                         Active = true,
                                         DateCreated = DateTime.UtcNow,
                                         DateModified = DateTime.UtcNow
                                     };
                                     datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj1);

                                     TblEmpTransaction TransactionObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.ClaimId == iObj.ClaimId select p).Take(1).SingleOrDefault();
                                     if(TransactionObj != null)
                                     {
                                         TransactionObj.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                                     }

                                     TblEmpBalance EmpPayment = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == ClaimObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                     if (EmpPayment != null)
                                     {
                                         EmpPayment.Amount = EmpPayment.Amount + ClaimAmount;
                                     }
                                 }
                                #endregion

                                 NotifyUser1(datacontext, NotifyText, ClaimObj.CompanyUserId, CObjects.enmNotifyType.Claim.ToString());
                                
                                 #region Push Notifications

                                
                                 List<TblNotificationDetail> NotificationList = null;

                                 NotificationList = InsertionInNotificationTable(ClaimObj.CompanyUserId, NotifyText, datacontext, ClaimObj.EmpClaimId, CObjects.enmNotifyType.Claim.ToString());
                               

                                 datacontext.SubmitChanges();

                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, ClaimObj.EmpClaimId);
                                    NotificationList.Clear();

                                }

                                 #endregion Push Notifications

                                    StringBuilder builder = new StringBuilder();
                                    builder.Append("Hello " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                    builder.Append(NotifyText + "<br/><br/>");
                                    builder.Append("Regards,<br/><br/>");
                                    builder.Append("Eflex Team<br/><br/>");

                                    

                                    string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                    lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    

                                    //send mail

                                    string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                    string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                    string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                    Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);

                                    string EmailTo = "";
                                    TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                    if (CarrierObj != null)
                                    {
                                        string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                       
                                        CMail Mail1 = new CMail();
                                        Mail1.EmailTo = CarrierEmails;
                                        Mail1.Subject = EmailSubject;
                                        Mail1.MessageBody = NotifyText;
                                        Mail1.GodaddyUserName = GodaddyUserName;
                                        Mail1.GodaddyPassword = GodaddyPwd;
                                        Mail1.Server = Server;
                                        Mail1.Port = Port;
                                        Mail1.CompanyId = iObj.CompanyId;
                                        bool IsSent1 = Mail1.SendEMail(datacontext, true);
                                    }

                                    if (!string.IsNullOrEmpty(EmailTo))
                                    {
                                        EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                    }
                                    else
                                    {
                                        EmailTo = UserObj.EmailAddress;

                                    }
                          
                                    CMail Mail = new CMail();
                                    Mail.EmailTo = EmailTo;
                                    Mail.Subject = EmailSubject;
                                    Mail.MessageBody = lstrMessage;
                                    Mail.GodaddyUserName = GodaddyUserName;
                                    Mail.GodaddyPassword = GodaddyPwd;
                                    Mail.Server = Server;
                                    Mail.Port = Port;
                                    Mail.CompanyId = iObj.CompanyId;
                                    bool IsSent = Mail.SendEMail(datacontext, true);

                                
                                CreateHistoryLog(mastercontext, iObj.CompanyId, iObj.LoggedInUserId, Convert.ToDecimal(ClaimObj.CompanyUserId), Text);
                                   

                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            }
                            datacontext.Connection.Close();
                        }
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "UpdateClaimStatus", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }

        #endregion UpdateClaimStatus

        #region EmployeeClaimList
        public CEmployeeClaimListResponse EmployeeClaimList(EmployeeClaimListInput iObj)
        {
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CEmployeeClaimListResponse lobjResponse = new CEmployeeClaimListResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                       decimal? StatusId = null;
                        if (iObj.StatusId != -1)
                        {
                            StatusId = iObj.StatusId;
                        }
                        List<EmployeeClaimList> Obj = (from p in datacontext.sp_EmployeeClaimList(iObj.ChunckStart, iObj.SearchString, Convert.ToInt32(iObj.ChunckSize), StatusId, iObj.CompanyUserId)
                                                       select new EmployeeClaimList()
                                                    {
                                                        EmpClaimId = p.EmpClaimId,
                                                        Employee = p.Employee,
                                                        Dependent = p.Dependant,
                                                        ClaimDt = Convert.ToString(p.ClaimDT),
                                                        ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                        Status = p.ClaimStatus,
                                                        Receipt = p.ReceiptNo,
                                                        ReceiptDT = Convert.ToString(p.ReceiptDate),
                                                        FileName = p.UploadFileName,
                                                        CRA = Convert.ToDecimal(p.CRA),
                                                        NonCRA = Convert.ToDecimal(p.NonCRA),
                                                        IsManual = Convert.ToBoolean(p.IsManual),
                                                        Note = p.Note,
                                                        ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                               join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                               where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                               select new ClaimServiceDetails()
                                                                               {
                                                                                   ServiceName = a.Title,
                                                                                   ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                   Address = q.Address,
                                                                                   PhoneNo = q.ContactNo,
                                                                                   Email = q.Email,
                                                                                   ServiceProvider = q.Name
                                                                               }).Take(1).SingleOrDefault()
                                                    }).ToList();
                        lobjResponse.EmployeeClaimList = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
              
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region ListOfCompanies
        public CListOfCompaniesResponse ListOfCompanies(LazyLoadingInput iObj)
        {
            CListOfCompaniesResponse lobjResponse = new CListOfCompaniesResponse();
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        List<ListOfCompany> Obj = (from p in mastercontext.sp_ListOfCompany(iObj.ChunckStart, Convert.ToInt32(iObj.ChunckSize), iObj.SearchString)
                                                   select new ListOfCompany()
                                                   {
                                                       CompanyId = Convert.ToDecimal(p.CompanyId),
                                                       Name = p.CompanyName,
                                                       Code = p.UniqueCode,
                                                       Email = p.OwnerEmail,
                                                       Phone = p.OwnerContactNo,
                                                       OwnerName = p.FirstName + " " + p.LastName,
                                                       Province = p.Province,
                                                       PlanType = p.PlanType== null ? "-":p.PlanType,
                                                       DbCon = p.DBCon,
                                                       OwnerId=1,
                                                       IsApproved = Convert.ToString(p.IsApproved),
                                                       BlockReason = (from a in mastercontext.TblCompBlockReasons where a.Active == true && a.CompanyId == p.CompanyId select new BlockReason()
                                                       {
                                                           Reason = a.Reason,
                                                           Date = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                           IsBlock = Convert.ToBoolean(a.IsBlock)
                                                       }).ToList(),
                                                       IsBlocked = Convert.ToBoolean(p.IsBlocked)
                                                   }).ToList();

                        lobjResponse.ListOfCompany = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
              
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion ListOfCompanies

        #region GetCompanyDetailsForAdmin
        public CGetCompanyDetailsForAdminResponse GetCompanyDetailsForAdmin(CompanyDetailsInput iObj)
        {
            CGetCompanyDetailsForAdminResponse lobjResponse = new CGetCompanyDetailsForAdminResponse();
            DateTime CurrentTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                        string Constring = GetConnection(iObj.DbCon);

                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            CompanyFiguresData FiguresObj = (from p in datacontext.sp_GetCompanyFigures(iObj.CompanyId)
                                                             select new CompanyFiguresData()
                                                                 {
                                                                     TotalEmployees = Convert.ToDecimal(p.TotalEmployees),
                                                                     TotalClaims = Convert.ToDecimal(p.TotalClaims),
                                                                     DuePayments = Convert.ToDecimal(p.TotalDueAmt),
                                                                     AvailableCredits = Convert.ToDecimal(p.TotalCredits),
                                                                 }).Take(1).SingleOrDefault();

                            CompanyBasicaData CompanyDetailObj = (from p in mastercontext.TblCompanies
                                                                  join q in mastercontext.TblProvinces on p.Province equals q.ProvinceId
                                                                  where p.Active == true && p.CompanyId == iObj.CompanyId
                                                                  select new CompanyBasicaData()
                                                                  {
                                                                      CompanyName = p.CompanyName,
                                                                      FirstName = p.FirstName,
                                                                      LastName = p.LastName,
                                                                      Address1 = p.Address1,
                                                                      Address2 = p.Address2,
                                                                      City = p.City,
                                                                      ProvinceId = Convert.ToDecimal(p.Province),
                                                                      PostalCode = p.PostalCode,
                                                                      Country = p.Country,
                                                                      OwnerContactNo = p.OwnerContactNo,
                                                                      OwnerEmail = p.OwnerEmail,
                                                                      ServiceUrl = p.ServiceUrl,
                                                                      HostUrl = p.HostUrl,
                                                                      Code = p.UniqueCode,
                                                                      UserName = p.UserName,
                                                                      Password = CryptorEngine.Decrypt(p.Pwd),
                                                                      IsCorporation = Convert.ToString(p.IsCorporation),
                                                                      PlanTypeId = Convert.ToDecimal(p.PlanTypeId),
                                                                      StagesCovered = p.StagesCovered,
                                                                      DbCon = p.DBCon,
                                                                      Province = q.Name,


                                                                  }).Take(1).SingleOrDefault();

                            

                            #region CompanyReferrals
                            CompReferralData ReferralObj = (from p in datacontext.TblCompanyReferrals
                                                           join q in datacontext.TblReferralTypes on p.ReferralTypeId equals q.ReferralTypeId
                                                           where p.Active == true && p.CompanyId == iObj.CompanyId && q.Active == true
                                                           select new CompReferralData()
                                                           {
                                                               ReferralType = q.Title,
                                                               Name = p.Name,
                                                               OtherDetails = p.OtherDetails,
                                                               ReferralTypeId = Convert.ToDecimal(p.ReferralTypeId),
                                                               BrokerId = Convert.ToDecimal(p.BrokerId)
                                                              }).Take(1).SingleOrDefault();
                            #endregion CompanyReferrals

                            CompanyPAPDetails PAPDetails = (from p in datacontext.TblPAPDetails
                                                            where p.Active == true && p.CompanyUserId == 1
                                                            select new CompanyPAPDetails()
                                                            {
                                                                BankName = p.BankName,
                                                                TransitId = p.TransitId,
                                                                InstitutionName = p.InstitutionId,
                                                                AccountNumber = p.AccountNumber,
                                                                Cheque = p.ChequeFile,
                                                                CompFinancialdetailId = Convert.ToDecimal(p.PAPDetailId)
                                                            }).Take(1).SingleOrDefault();

                            List<PlanListOfCompany> PlanList = (from p in datacontext.TblCompanyPlans
                                                                where p.Active == true
                                                                select new PlanListOfCompany()
                                                                {
                                                                    PlanStartDT = Convert.ToString(p.PlanStartDt),
                                                                    PlanId = Convert.ToDecimal(p.CompanyPlanId),
                                                                    PlanName = p.Name,
                                                                    NoOfEmployees = (from a in datacontext.TblEmployees where a.Active == true && a.IsActive == true && a.CompanyPlanId == p.CompanyPlanId select p).Count()
                                                                }).ToList();
                            lobjResponse.CompanyFiguresData = FiguresObj;
                            lobjResponse.CompanyBasicaData = CompanyDetailObj;
                            lobjResponse.CompanyPAPDetails = PAPDetails;
                            lobjResponse.PlanListOfCompany = PlanList;
                            lobjResponse.CompReferralData = ReferralObj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            datacontext.Connection.Close();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
             
                mastercontext.Connection.Close();
            }

            return lobjResponse;
        }
        #endregion

        #region ListCompanyEmployees
        public CListCompEmployeesResponse ListCompanyEmployees(ListCompanyEmployeesInput iObj)
        {
            DateTime CurrentTime = DateTime.UtcNow;
            CListCompEmployeesResponse lobjResponse = new CListCompEmployeesResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     string DbCon = DBFlag + iObj.CompanyId;
                     string Constring = GetConnection(DbCon);
                     using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                     {
                         try
                         {
                             List<CompEmpDetails> Obj = (from p in datacontext.sp_ListCompEmployees(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString)
                                                         select new CompEmpDetails()
                                                         {
                                                            CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                            FName = p.FirstName,
                                                            LName = p.LastName,
                                                            ContactNo = p.MobileNo,
                                                            Email = p.EmailAddress,
                                                            PlanName = p.PlanName,
                                                            AvailableAmount = Convert.ToDecimal(p.Amount) < 0 ? 0 : Convert.ToDecimal(p.Amount),
                                                            PlanId = Convert.ToDecimal(p.CompanyPlanId),
                                                            IsActive = Convert.ToString(p.IsActive),
                                                            IsDeleteable = GetIsDeleteable(datacontext, Convert.ToDecimal(iObj.CompanyId))
                                                        }).ToList();

                             lobjResponse.CompEmpDetails = Obj;
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         }
                         catch (Exception ex)
                         {
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                             lobjResponse.Message = ex.Message;
                         }
                         datacontext.Connection.Close();
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                
                 mastercontext.Connection.Close();
             }
           
            return lobjResponse;
        }

        private bool GetIsDeleteable(EflexDBDataContext datacontext, decimal CompanyId)
        {
            bool IsDeleteable = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            TblCompPayment Obj = (from p in mastercontext.TblCompPayments
                                  where p.Active == true
                                  && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                  && p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString()
                                  select p).FirstOrDefault();

            if (Obj != null)
            {
                IsDeleteable = true;
            }
            else
            {
                IsDeleteable = false;
            }
            return IsDeleteable;
        }
        #endregion

        #region ListOfAllReceivedPayment
        public CListOfAllReceivedPayment ListOfAllReceivedPayment(PaymentsInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            CListOfAllReceivedPayment lobjResponse = new CListOfAllReceivedPayment();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {

                    if (IsValidateEflexUser(datacontext))
                    {
                        DateTime? FromDT = null;
                        DateTime? ToDT = null;
                        if (iObj.FromDt != null || iObj.FromDt != "")
                        {
                            FromDT = Convert.ToDateTime(iObj.FromDt);
                        }
                        if (iObj.ToDt != null || iObj.ToDt != "")
                        {
                            ToDT = Convert.ToDateTime(iObj.ToDt);
                        }

                        List<CompPaymentDetails> Obj = (from p in datacontext.sp_ListReceivedPayments(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString, FromDT, ToDT)
                                                        select new CompPaymentDetails()
                                                             {
                                                                 CompanyPaymentId = p.CompPaymentId,
                                                                 CompanyName = p.CompanyName,
                                                                 PaymentDueDate = Convert.ToString(p.PaymentDueDate),
                                                                 PaymentDate = Convert.ToString(p.PaymentDate),
                                                                 Amount = Convert.ToDecimal(p.Amount),
                                                                 PaymentDetails = p.PaymentDetails
                                                             }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.ReceivedPaymentData = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    IsLogOut = true;
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
              
                datacontext.Connection.Close();
              }
            return lobjResponse;
        }
        #endregion

        #region ListOfAllDuePayment
        public CListOfAllDuePayment ListOfAllDuePayment(PaymentsInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            CListOfAllDuePayment lobjResponse = new CListOfAllDuePayment();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    DateTime? FromDT = null;
                    DateTime? ToDT = null;
                    if(iObj.FromDt != null || iObj.FromDt !="")
                    {
                        FromDT = Convert.ToDateTime(iObj.FromDt);
                    }
                    if (iObj.ToDt != null || iObj.ToDt != "")
                    {
                        ToDT = Convert.ToDateTime(iObj.ToDt);
                    }
                    if (IsValidateEflexUser(datacontext))
                    {
                     
                        List<CompPaymentDetails> Obj = (from p in datacontext.sp_ListDuePayments(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString, FromDT,ToDT)
                                                        select new CompPaymentDetails()
                                                        {
                                                            CompanyPaymentId = p.CompPaymentId,
                                                            CompanyName = p.CompanyName,
                                                            PaymentDueDate = Convert.ToString(p.PaymentDueDate),
                                                            PaymentDate = Convert.ToString(p.PaymentDate),
                                                            Amount = Convert.ToDecimal(p.Amount),
                                                            PaymentDetails = p.PaymentDetails,
                                                            GetPAPDetails = GetCompBankDetails(p.CompanyId)
                                                        }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.DuePaymentData = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
              
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }

        private GetPAPDetails GetCompBankDetails(decimal CompanyId)
        {
            string DbCon = DBFlag + CompanyId;
            string ConString = GetConnection(DbCon);
             GetPAPDetails Obj = new GetPAPDetails();
            using (EflexDBDataContext datacontext= new EflexDBDataContext(ConString))
            {
                try
                {
                   
                    TblPAPDetail PAPObj =(from p in datacontext.TblPAPDetails where p.Active == true && p.CompanyUserId == 1 select p).Take(1).SingleOrDefault();
                    if(PAPObj != null)
                    {

                        Obj.PAPDetailId = Convert.ToDecimal(PAPObj.PAPDetailId);
                        Obj.CompanyUserId = Convert.ToDecimal(PAPObj.CompanyUserId);
                        Obj.BankName = PAPObj.BankName;
                        Obj.TransitId = PAPObj.TransitId;
                        Obj.InstitutionId = PAPObj.InstitutionId;
                        Obj.ChequeFile = PAPObj.ChequeFile;
                        Obj.AccountNumber = PAPObj.AccountNumber;
                        Obj.IsCheque = Convert.ToString(PAPObj.IsCheque);
                        Obj.IsAgree = Convert.ToString(PAPObj.IsAgree);

                    }
          
                }
                catch (Exception)
                {
                    
                    throw;
                }

                datacontext.Connection.Close();
            }

            return Obj;
        }
        #endregion

        #region MarkPaymentReceived1
        public CResponse MarkPaymentReceived1(MarkPaymentReceivedInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompPaymentId == iObj.CompPaymentId select p).Take(1).SingleOrDefault();
                        if(CompPaymentObj != null)
                        {
                            CompPaymentObj.PaymentStatus = iObj.StatusId;
                            CompPaymentObj.PaymentDate = DateTime.UtcNow;
                            List<TblCompAdjustment> CompAdj = (from p in mastercontext.TblCompAdjustments where p.Active == true && p.CompanyPaymentId == iObj.CompPaymentId select p).ToList();
                            if(CompAdj != null && CompAdj.Count() > 0)
                            {
                                CompAdj.ForEach(x => x.PaymentStatus = iObj.StatusId);
                            }
                            string DbCon = DBFlag + CompPaymentObj.CompanyId;
                            string Constring = GetConnection(DbCon);

                            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                            {
                               

                                List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                                if(PaymentObj != null && PaymentObj.Count() > 0)
                                {
                                    PaymentObj.ForEach(x => { x.PaymentStatus = iObj.StatusId; x.PaymentDueDate = DateTime.UtcNow; });

                                }
                                List<TblEmpTransaction> EmpObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.ClaimId==-1 && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                                if (EmpObj != null && EmpObj.Count() > 0)
                                {
                                    EmpObj.ForEach(x => { x.PaymentStatus = iObj.StatusId; x.TransactionDate = DateTime.UtcNow; });
                                   
                                }
                                

                                List<TblCompanyPlan> PlanList = (from p in datacontext.TblCompanyPlans where p.Active == true select p).Distinct().ToList();
                                if(PlanList != null && PlanList.Count() > 0)
                                {
                                    foreach (TblCompanyPlan item in PlanList)
                                    {
                                        List<TblEmployee> EmpListObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).ToList();

                                        if(EmpListObj != null && EmpListObj.Count() > 0)
                                        {
                                            foreach (TblEmployee empitem in EmpListObj)
                                            {
                                               TblEmpTransaction EmpPendingObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.ClaimId == -1 && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();

                                               if (EmpPendingObj != null)
                                               {
                                                   TblEmpBalance EmpBalObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == empitem.CompanyUserId select p).Take(1).SingleOrDefault();
                                                   if (EmpBalObj != null)
                                                   {

                                                       if (EmpBalObj.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited))
                                                       {
                                                           EmpBalObj.Amount = EmpBalObj.Amount + item.RecurringDeposit;
                                                           datacontext.SubmitChanges();

                                                       }
                                                       else
                                                       {
                                                           EmpBalObj.Amount = EmpBalObj.Amount + item.NetDeposit;
                                                           EmpBalObj.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                                                           datacontext.SubmitChanges();

                                                       }
                                                   }
                                               }
                                            }
                                        }
                                      
                                    }
                                }




                                TblCompanyUser CompUserObj1 = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == 1 select p).Take(1).SingleOrDefault();
                                TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();

                                string Text = "Payment has been received by " + UserObj.FirstName + " " + UserObj.LastName + ".";
                                CreateHistoryLog(mastercontext, CompPaymentObj.CompanyId, iObj.LoggedInUserId, -1, Text);
                                #region SendMail



                                
                                StringBuilder builder = new StringBuilder();
                                builder.Append("Hello " + CompUserObj1.FirstName + ((CompUserObj1.LastName == "" || CompUserObj1.LastName == null) ? "" : " " + CompUserObj1.LastName) + ",<br/><br/>");
                                builder.Append("Due amount has been received by eFlex Admin (" + UserObj.FirstName + " " + UserObj.LastName + ") on " + string.Format("{0:MM/dd/yyyy}", CompPaymentObj.PaymentDate) + ". <br/><br/>");
                                builder.Append("Regards,<br/><br/>");
                                builder.Append("eFlex Team<br/><br/>");
                                string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                //send mail
                                string EmailTo = CompUserObj1.EmailAddress;
                                string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                CMail Mail = new CMail();
                                Mail.EmailTo = EmailTo;
                                Mail.Subject = "Payment Received";
                                Mail.MessageBody = lstrMessage;
                                Mail.GodaddyUserName = GodaddyUserName;
                                Mail.GodaddyPassword = GodaddyPwd;
                                Mail.Server = Server;
                                Mail.Port = Port;
                                Mail.CompanyId = UserId;
                                bool IsSent = Mail.SendEMail(datacontext, true);


                                #endregion SendMail


                                datacontext.SubmitChanges();
                                datacontext.Connection.Close();
                            }
                           


                            mastercontext.SubmitChanges();



                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "MarkPaymentReceived", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region User
        #region AddUser
        public CResponse AddUser(AddUserInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.EmailAddress == iObj.EmailAddress && p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin) select p).Take(1).SingleOrDefault();
                        if (UserObj == null)
                        {
                            string Password = RandomString();
                            string EncryptPwd = CryptorEngine.Encrypt(Password);
                            TblUser Obj = new TblUser()
                            {
                                FirstName = iObj.FirstName,
                                LastName = iObj.LastName,
                                UserName = iObj.UserName,
                                ContactNo = iObj.ContactNo,
                                Pwd = EncryptPwd,
                                EmailAddress = iObj.EmailAddress,
                                UserTypeId = Convert.ToInt16(CObjects.enumMasterUserType.Admin),
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            mastercontext.TblUsers.InsertOnSubmit(Obj);
                            mastercontext.SubmitChanges();

                            UpdateEflexMsgUser(Obj, "ADD", -1);


                            #region SendMail

                           
                            StringBuilder builder = new StringBuilder();
                            builder.Append("Hello " + iObj.FirstName + ",<br/><br/>");
                            builder.Append("Welcome to eFlex. Please use the following credentials for login to eFlex Portal:<br/><br/>");
                            
                            builder.Append("Username: " + iObj.UserName + "<br/><br/>");
                            builder.Append("Password: " + Password + "<br/><br/>");
                            builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                            builder.Append("Regards,<br/><br/>");
                            builder.Append("eFlex Team<br/><br/>");

                           
                            //send mail
                            string lstrMessage = File.ReadAllText(EmailFormats + "WelcomeEmailTemplate.txt");
                            lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                            CMail Mail = new CMail();
                            Mail.EmailTo = iObj.EmailAddress;
                            Mail.Subject = "Welcome to eFlex! Your Account Is Ready";
                            Mail.MessageBody = lstrMessage;
                            Mail.GodaddyUserName = GodaddyUserName;
                            Mail.GodaddyPassword = GodaddyPwd;
                            Mail.Server = Server;
                            Mail.Port = Port;
                            Mail.CompanyId = -1;
                            bool IsSent = Mail.SendEMail(mastercontext, true);


                            #endregion SendMail


                            string Text = "A new user named as " + iObj.FirstName + " " + iObj.LastName + " has been added in the Eclipse on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow);
                            CreateHistoryLog(mastercontext, -1, iObj.LoggedInUserId, Obj.UserId, Text);
                                   
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "User already exists with this email address.";

                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                   
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "AddUser", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region DeleteAdminUser
        public CResponse DeleteAdminUser(DeleteAdminUserInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.LoggedInUserId;
            InputJson = new JavaScriptSerializer().Serialize(iObj);
            bool IsLogOut = false;
            CResponse lobjResponse = new CResponse();
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    UserId = iObj.LoggedInUserId;
                    if (IsValidateEflexUser(mastercontext))
                    {
                        TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.UserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            UserObj.Active = false;
                            UserObj.DateModified = DateTime.UtcNow;
                            UpdateEflexMsgUser(UserObj, "DELETE", -1);
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        }

                        CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "DeleteAdminUser", InputJson, lobjResponse.Message);
              
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
           
            return lobjResponse;
        }
        #endregion

        #region ListAdminUser
        public CListAdminUserResponse ListAdminUser(IdInput iObj)
        {
            string InputJson = "";
            DateTime CurrentTime = DateTime.UtcNow;
            UserId = iObj.Id;
            DateTime StartTime = DateTime.UtcNow;
            InputJson = new JavaScriptSerializer().Serialize(iObj);
            bool IsLogOut = false;
            CListAdminUserResponse lobjResponse = new CListAdminUserResponse();
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
               
                    if (IsValidateEflexUser(datacontext))
                    {

                        List<ListAdminUser> Obj = (from p in datacontext.TblUsers where p.Active == true 
                                                       && (p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Admin)
                                                      || p.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.SuperAdmin)) && p.UserId != iObj.Id
                                                   select new ListAdminUser()
                                                        {
                                                            UserId = p.UserId,
                                                            FirstName = p.FirstName,
                                                            LastName= p.LastName,
                                                            UserName = p.UserName,
                                                            ContactNo=p.ContactNo,
                                                            Pwd = CryptorEngine.Decrypt(p.Pwd),
                                                            EmailAddress = p.EmailAddress
                                                        }).ToList();

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.ListAdminUser = Obj;
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {

                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.ToString();
                }
                lobjResponse.IsLogOut = IsLogOut;
              
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion
        #endregion

        #region AddEditAdminPAPDetails
        public CResponse AddEditAdminPAPDetails(AddEditAdminPAPDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            MasterDBDataContext masterContext = new MasterDBDataContext();
            using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
            {
                try
                {
                    if (IsValidateEflexUser(masterContext))
                    {
                        decimal CompanyUserId = -1;
                        if (iObj.CompanyUserId == -1)
                        {
                            CompanyUserId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();

                        }
                        else
                        {
                            CompanyUserId = iObj.CompanyUserId;
                        }

                        if (iObj.CompFinancialDetailId == -1)
                        {
                            TblPAPDetail Obj = new TblPAPDetail()
                            {
                                InitialPayment = iObj.InitialPayment,
                                RecurringPayment = iObj.RecurringPayment,
                                CompanyUserId = CompanyUserId,
                                BankName = iObj.BankName,
                                InstitutionId = iObj.InstitutionNumber,
                                TransitId = iObj.TransitNumber,
                                AccountNumber = iObj.AccountNumber,
                                IsCheque = iObj.IsCheque,
                                ChequeFile = iObj.FileName,
                                IsAgree = iObj.IsAgree,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            datacontext.TblPAPDetails.InsertOnSubmit(Obj);
                            datacontext.SubmitChanges();

                            if (iObj.CompanyId != -1)
                            {
                                using (MasterDBDataContext mastercontext = new MasterDBDataContext())
                                {
                                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                                    if (CompanyObj != null)
                                    {
                                        CompanyObj.StagesCovered = "5";
                                        mastercontext.SubmitChanges();
                                    }
                                    mastercontext.Connection.Close();
                                }
                            }

                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            lobjResponse.Message = "Financial details added successfully.";
                        }
                        else
                        {
                            TblPAPDetail FinancialObj = (from p in datacontext.TblPAPDetails where p.Active == true && p.PAPDetailId == iObj.CompFinancialDetailId select p).Take(1).SingleOrDefault();
                            if (FinancialObj != null)
                            {
                                FinancialObj.InstitutionId = iObj.InstitutionNumber;
                                FinancialObj.TransitId = iObj.TransitNumber;
                                FinancialObj.BankName = iObj.BankName;
                                FinancialObj.AccountNumber = iObj.AccountNumber;
                                FinancialObj.IsCheque = iObj.IsCheque;
                                FinancialObj.ChequeFile = iObj.FileName;
                                FinancialObj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                lobjResponse.Message = "Financial details updated successfully.";
                            }

                        }

                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;

                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;

                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(masterContext, StartTime, UserId, "AddEditAdminPAPDetails", InputJson, lobjResponse.Message);
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetEmpDetailsForAdmin
        public CGetEmpDetailsForAdminResponse GetEmpDetailsForAdmin(GetEmpDetailsForAdminInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CGetEmpDetailsForAdminResponse lobjResponse = new CGetEmpDetailsForAdminResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(mastercontext))
                    {
                        string DbCon = DBFlag + iObj.CompanyId;
                        string Constring = GetConnection(DbCon);

                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                          EmpFiguresData FiguresObj = (from p in datacontext.sp_GetEmpFigures(iObj.UserId)
                                                             select new EmpFiguresData()
                                                             {
                                                                 TotalClaimAmount = Convert.ToDecimal(p.TotalClaimAmount),
                                                                 TotalClaims = Convert.ToDecimal(p.TotalClaims),
                                                                 TotalBalance = Convert.ToDecimal(p.TotalBalance) < 0 ? 0 : Convert.ToDecimal(p.TotalBalance) ,
                                                                 TotalAdjustments = Convert.ToDecimal(p.TotalAdjustments),
                                                                 CreditAdjustments = Convert.ToDecimal(p.TotalCreditAdjustments),
                                                                 DebitAdjustments = Convert.ToDecimal(p.TotalDebitAdjustments),
                                                             }).Take(1).SingleOrDefault();

                          EmpBasicData EmpDetailObj = (from p in datacontext.TblEmployees
                                                       join q in datacontext.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                       where p.Active == true && p.CompanyUserId == iObj.UserId && q.Active == true
                                                       select new EmpBasicData()
                                                           {
                                                               UserName = q.UserName,
                                                               FirstName = p.FirstName,
                                                               LastName = p.LastName,
                                                               EmailAddress = p.EmailAddress,
                                                               DOB = Convert.ToString(p.DOB),
                                                               Gender = p.Gender,
                                                               MobileNo = p.MobileNo,
                                                               EveningPhone = p.EveningPhone,
                                                               MorningPhone = p.MorningPhone,
                                                               Address = p.Address1,
                                                               Address2 = p.Address2,
                                                               City = p.City,
                                                               Province = p.Province,
                                                               PostalCode = p.PostalCode,
                                                               IsActive = Convert.ToBoolean(p.IsActive),
                                                               InActiveDate = Convert.ToString(p.InActiveDate),
                                                               Password = CryptorEngine.Decrypt(q.Pwd),
                                                               CellNotificationDetails = (from a in datacontext.TblUserCarriers
                                                                                          where a.Active == true
                                                                                              && a.CompanyUserId == p.CompanyUserId
                                                                                          select new CellNotificationDetails()
                                                                                              {
                                                                                                  CarrierName = Convert.ToString(a.CarrierID),
                                                                                                  ContactNo = a.ContactNo,
                                                                                                  IsNotify = Convert.ToString(a.IsSMS),
                                                                                              }).Take(1).SingleOrDefault(),
                                                           }).Take(1).SingleOrDefault();

                            CompReferralData ReferralObj = (from a in datacontext.TblCompanyReferrals
                                                            join q in datacontext.TblReferralTypes on a.ReferralTypeId equals q.ReferralTypeId
                                                            where a.Active == true && a.CompanyId == iObj.CompanyId
                                                            select new CompReferralData()
                                                            {
                                                                ReferralType = q.Title,
                                                                Name = a.Name,
                                                                OtherDetails = a.OtherDetails
                                                            }).Take(1).SingleOrDefault();

                            EmpPAPDetails PAPDetails = (from p in datacontext.TblPAPDetails
                                                            where p.Active == true && p.CompanyUserId == iObj.UserId
                                                            select new EmpPAPDetails()
                                                            {
                                                                BankName = p.BankName,
                                                                TransitId = p.TransitId,
                                                                InstitutionId = p.InstitutionId,
                                                                AccountNumber = p.AccountNumber,
                                                                ChequeFile = p.ChequeFile,
                                                                PAPDetailId = p.PAPDetailId
                                                            }).Take(1).SingleOrDefault();

                            List<PlanListOfEmp> PlanList = (from p in datacontext.TblCompanyPlans
                                                            join q in datacontext.TblEmployees on p.CompanyPlanId equals q.CompanyPlanId
                                                                where p.Active == true && q.Active==true && q.CompanyUserId==iObj.UserId
                                                                select new PlanListOfEmp()
                                                                {
                                                                    PlanStartDT = Convert.ToString(p.PlanStartDt),
                                                                    PlanId = p.CompanyPlanId,
                                                                    PlanName = p.Name,
                                                                  }).ToList();
                            lobjResponse.EmployeeFiguresData = FiguresObj;
                            lobjResponse.EmpBasicData = EmpDetailObj;
                            lobjResponse.EmpPAPDetails = PAPDetails;
                            lobjResponse.PlanListOfEmp = PlanList;
                            
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            datacontext.Connection.Close();
                        }
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {

                    lobjResponse.Message = ex.Message;
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                }

             
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }

            return lobjResponse;
        }
        #endregion

        #region UpdatePlanDate
        public CResponse UpdatePlanDate(UpdatePlanDateInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        string DbCon = DBFlag + iObj.CompanyId;
                        string Constring = GetConnection(DbCon);
                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            foreach (var item in iObj.Plans)
                            {
                                TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.PlanId select p).Take(1).SingleOrDefault();
                                if (PlanObj != null)
                                {
                                    DateTime PlanStDt = Convert.ToDateTime(PlanObj.PlanStartDt);
                                    if (PlanObj.PlanStartDt.Value.Date != Convert.ToDateTime(item.PlanStartDT).Date)
                                    {
                                        
                                        PlanObj.PlanStartDt = Convert.ToDateTime(item.PlanStartDT);
                                        PlanObj.DateModified = DateTime.UtcNow;
                                        string Text = PlanObj.Name + " plan start date has been changed from " + PlanStDt.ToString("MM/dd/yyyy") + " to " + Convert.ToDateTime(item.PlanStartDT).ToString("MM/dd/yyyy") + " .";
                                        CreateHistoryLog(mastercontext, iObj.CompanyId, iObj.LoggedInUserId, iObj.LoggedInUserId, Text);

                                        datacontext.SubmitChanges();
                                    }
                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                }
                                else
                                {
                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                }
                            }
                            datacontext.Connection.Close();
                        }
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }

                CreateMasterResponseLog(mastercontext, StartTime, UserId, "UpdatePlanDate", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region UpdateEmpBasicDetailsByAdmin
        public CResponse UpdateEmpBasicDetailsByAdmin(UpdateEmpBasicDetailsByAdminInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CResponse lobjResponse = new CResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            string EmpMoblieNo = "";
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            if (IsValidateEflexUser(mastercontext))
            {
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        decimal RandomCode = getRandomID(mastercontext, iObj.DbCon, iObj.CompanyUserId);
                                       
                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                        CMail Mail = new CMail();

                        DateTime? DOB = null;
                        if(!string.IsNullOrEmpty(iObj.DOB))
                        {
                            DOB = Convert.ToDateTime(iObj.DOB);
                        }
                        
                        decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                        if (UserObj != null)
                        {
                            EmpMoblieNo = UserObj.ContactNo;
                            TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                           
                            UserObj.FirstName = iObj.FirstName;
                            UserObj.LastName = iObj.LastName;
                            UserObj.ContactNo = iObj.MobileNo;
                            UserObj.EmailAddress = iObj.EmailAddress;
                            TblEmployee EmpObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (EmpObj != null)
                            {
                                EmpObj.FirstName = iObj.FirstName;
                                EmpObj.LastName = iObj.LastName;
                                EmpObj.EmailAddress = iObj.EmailAddress;
                                EmpObj.DOB = DOB;
                                EmpObj.Gender = iObj.Gender;
                                EmpObj.MobileNo = iObj.MobileNo;
                                EmpObj.EveningPhone = iObj.EveningPhone;
                                EmpObj.MorningPhone = iObj.MorningPhone;
                                EmpObj.Address1 = iObj.Address;
                                EmpObj.Address2 = iObj.Address2;
                                EmpObj.City = iObj.City;
                                EmpObj.Province = iObj.Province;
                                EmpObj.PostalCode = iObj.PostalCode;
                                
                                    TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                                    if (CarrierObj != null)
                                    {
                                        if (!string.IsNullOrEmpty(iObj.Carrier))
                                        {
                                            TblCarrier CarrierObj1 = (from p in mastercontext.TblCarriers where p.Active == true && p.CarrierId == Convert.ToDecimal(iObj.Carrier) select p).Take(1).SingleOrDefault();
                                            if (CarrierObj1 != null)
                                            {
                                                CarrierObj.ContactNo = iObj.MobileNo;
                                                CarrierObj.IsSMS = Convert.ToBoolean(iObj.IsNotify);
                                                CarrierObj.Carrier = CarrierObj1.Carrier;
                                                CarrierObj.CarrierID = CarrierObj1.CarrierId;
                                                if (EmpMoblieNo != iObj.MobileNo)
                                                {
                                                    #region SendOTPMail
                                                    string lstrMessage = "";
                                                    StringBuilder builder = new StringBuilder();
                                                    builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                                                    lstrMessage = builder.ToString();

                                                    string AccountSid = WebConfigurationManager.AppSettings["AccountSid"].ToString();
                                                    string AuthToken = WebConfigurationManager.AppSettings["AuthToken"].ToString();

                                                    TwilioClient.Init(AccountSid, AuthToken);
                                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                                                       | SecurityProtocolType.Tls11
                                                                                       | SecurityProtocolType.Tls12
                                                                                       | SecurityProtocolType.Ssl3;
                                                    var message = MessageResource.Create(
                                                        body: lstrMessage,
                                                        from: new Twilio.Types.PhoneNumber("+12897993854"),
                                                        to: new Twilio.Types.PhoneNumber("+1"+iObj.MobileNo)
                                                    );
                                                        

                                                    #endregion SendMail
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(iObj.Carrier))
                                        {
                                            TblCarrier CarrierObj1 = (from p in mastercontext.TblCarriers where p.Active == true && p.CarrierId == Convert.ToDecimal(iObj.Carrier) select p).Take(1).SingleOrDefault();

                                            TblUserCarrier Obj = new TblUserCarrier()
                                            {
                                                Carrier = CarrierObj1.Carrier,
                                                CompanyUserId = iObj.CompanyUserId,
                                                IsSMS = true,
                                                ContactNo = iObj.MobileNo,
                                                IsVerified = false,
                                                RandomCode = RandomCode,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                CarrierID = CarrierObj1.CarrierId
                                            };
                                            datacontext.TblUserCarriers.InsertOnSubmit(Obj);
                                            datacontext.SubmitChanges();

                                            #region SendOTPMail
                                            string lstrMessage = "";
                                            StringBuilder builder = new StringBuilder();
                                            builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) " + RandomCode + " to verify your mobile number.Thanks!");
                                            lstrMessage = builder.ToString();

                                            string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
                                            string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

                                            TwilioClient.Init(AccountSid, AuthToken);
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                                               | SecurityProtocolType.Tls11
                                                                               | SecurityProtocolType.Tls12
                                                                               | SecurityProtocolType.Ssl3;
                                            var message = MessageResource.Create(
                                                body: lstrMessage,
                                                from: new Twilio.Types.PhoneNumber("+12897993854"),
                                                to: new Twilio.Types.PhoneNumber("+1"+iObj.MobileNo)
                                            );
                                                   




                                            #endregion SendMail
                                        }

                                    }
                                

                                #region Push Notifications
                                List<TblNotificationDetail> NotificationList = null;
                                string NotifyText = "Your profile has been updated by admin on "+DateTime.UtcNow.ToString("MM/dd/yyyy");
                                NotificationList = InsertionInNotificationTable(EmpObj.CompanyUserId, NotifyText, datacontext, EmpObj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());
                                NotifyUser1(datacontext, NotifyText, EmpObj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());

                                datacontext.SubmitChanges();

                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, EmpObj.CompanyUserId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications



                            }
                            UpdateMsgUser(UserObj, "Update", CompanyId);
                            #region Update Details in Berkely

                            TblMasterCard CardObj = (from p in mastercontext.TblMasterCards where p.Active == true && p.UserId == iObj.CompanyUserId select p).Take(1).SingleOrDefault();
                            if (CardObj != null)
                            {
                                  string SessionId = GetUserSessionId();
                                    if (SessionId != null && SessionId != "")
                                    {
                                        
                                        string UpdateDemographicDetailsUri = BaseUrl + "/UpdateDemographicInfoInstant/?sessionID=" + SessionId + "&ProgramID=" + ProgramId;
                                        HttpWebRequest webRequest2 = HttpWebRequest.Create(new Uri(UpdateDemographicDetailsUri)) as HttpWebRequest;
                                        webRequest2.Method = System.Net.WebRequestMethods.Http.Post;
                                        webRequest2.ContentType = "application/json; charset=utf-8";
                                        string data = "{\"ID\":\"" + CardObj.UniqueId + "\",\"CardHolderDetails\":{\"AddressLine1\":\"" + iObj.Address + "\",\"AddressLine2\":\"" + iObj.Address2 + "\",\"BusinessPhone\":\"" + EmpObj.BusinessPhone + "\",\"CardHolderID\":\"\",\"City\":\"" + iObj.City + "\",\"Company\":\"" + CompanyObj.CompanyName + "\",\"CountryCode\":\"CA\",\"CountrySubdivisionCode\":\"ON\",\"Email\":\"" + iObj.EmailAddress + "\",\"FirstName\":\"" + iObj.FirstName + "\",\"HomePhone\":\"" + EmpObj.HomePhone + "\",\"LastName\":\"" + iObj.LastName + "\",\"PostalCode\":\"" + iObj.PostalCode + "\",\"SIN\":\"" + CardObj.SIN + "\"},\"Reference\":{\"BusinessEntityID\":\"" + BusinessEntityID + "\",\"ReferenceID\":\"" + CardObj.AccountId + "\",\"ReferenceType\":0}}";
                                        byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

                                        webRequest2.ContentLength = byteData.Length;
                                        try
                                        {
                                            //// Write data  
                                            using (Stream postStream = webRequest2.GetRequestStream())
                                            {
                                                postStream.Write(byteData, 0, byteData.Length);
                                            }

                                            // Get response  
                                            using (HttpWebResponse response2 = webRequest2.GetResponse() as HttpWebResponse)
                                            {
                                                StreamReader sr2 = new StreamReader(response2.GetResponseStream());
                                                string text3 = sr2.ReadToEnd().ToString();
                                                dynamic UpdateDetailsRes = JsonConvert.DeserializeObject<dynamic>(text3);
                                                if (UpdateDetailsRes.Result["Success"] == true)
                                                {
                                                    datacontext.SubmitChanges();
                                                    lobjResponse.Message = "SUCCESS";
                                                    lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                                                }
                                                else
                                                {
                                                    CreateCardApiResponseLog(iObj.CompanyUserId, "CreateCard", "FAILURE", UpdateDetailsRes.Result["Success"].ToString(), CompanyId, -1, UpdateDetailsRes.Result["ErrorMessage"].ToString(), InputJson);
                                                    lobjResponse.Message = UpdateDetailsRes.Result["ErrorMessage"].ToString();
                                                    lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            lobjResponse.Message = ex.Message;
                                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                                        }
                                    }
                                    else
                                    {
                                        CreateCardApiResponseLog(iObj.CompanyUserId, "Login", "FAILURE", "", CompanyId, -1, "", InputJson);

                                        lobjResponse.Message = "The service is not available.";
                                        lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                    }
                               
                                datacontext.SubmitChanges();
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            else
                            {
                                datacontext.SubmitChanges();
                                lobjResponse.Message = "SUCCESS";
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                            #endregion Update Details in Berkely
                        }
                        else
                        {
                            lobjResponse.Message = "User doesn't exist";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }

                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    datacontext.Connection.Close();
                }

                CreateMasterResponseLog(mastercontext, StartTime, UserId, "UpdateEmpBasicDetailsByAdmin", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            else
            {
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                lobjResponse.Message =  CObjects.SessionExpired.ToString();
                IsLogOut = true;
            }
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }
        #endregion

        #region AddEmployeeByadmin
        public CResponse AddEmployeeByadmin(AddEmployeeByadminInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            string UniqueCode = "";
            string ErrorString = "";
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            string PaymentType = "";
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            if (IsValidateEflexUser(mastercontext))
            {
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.DBCon == iObj.DbCon select p).Take(1).SingleOrDefault();
                        if (CompanyObj != null)
                        {
                            UniqueCode = CompanyObj.UniqueCode;
                            mastercontext.SubmitChanges();
                        }

                        #region Add Employees
                        DateTime? PaymentDate = null;
                        string PaymentDetails = "";
                        decimal AmountToBepaid = 0;
                        if (iObj.EmployeesList != null && iObj.EmployeesList.Count() > 0)
                        {
                            List<ClasswisePayments> ClasswisePaymentsObj = new List<ClasswisePayments>();
                             #region Set Payment date


                            TblCompPayment CompanyPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                            if (CompanyPaymentObj != null)
                            {
                                if (CompanyPaymentObj.PaymentDate >= DateTime.UtcNow)
                                {
                                    PaymentDate = CompanyPaymentObj.PaymentDate;
                                }
                                else
                                {
                                    DateTime now = DateTime.UtcNow;
                                    var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                    var SevenDT = new DateTime(now.Year, now.Month, 7);
                                    DateTime date = DateTime.Now;

                                    DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                    string dayToday = DayOnTwelve.ToString();

                                    // compare enums
                                    if (DayOnTwelve == DayOfWeek.Saturday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(2);
                                    }
                                    else if (DayOnTwelve == DayOfWeek.Sunday)
                                    {
                                        PaymentDate = TweleveDT.Date.AddDays(1);
                                    }
                                    else
                                    {
                                        PaymentDate = TweleveDT.Date;
                                    }


                                    if (now.Date > PaymentDate)
                                    {
                                        PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                    }

                                }
                            }
                            else
                            {
                                DateTime now = DateTime.UtcNow;
                                var TweleveDT = new DateTime(now.Year, now.Month, 12);
                                var SevenDT = new DateTime(now.Year, now.Month, 7);
                                DateTime date = DateTime.Now;

                                DayOfWeek DayOnTwelve = TweleveDT.DayOfWeek;
                                string dayToday = DayOnTwelve.ToString();

                                //- compare enums
                                if (DayOnTwelve == DayOfWeek.Saturday)
                                {
                                    PaymentDate = TweleveDT.Date.AddDays(2);
                                }
                                else if (DayOnTwelve == DayOfWeek.Sunday)
                                {
                                    PaymentDate = TweleveDT.Date.AddDays(1);
                                }
                                else
                                {
                                    PaymentDate = TweleveDT.Date;
                                }


                                if (now.Date > PaymentDate)
                                {
                                    PaymentDate = PaymentDate.Value.Date.AddMonths(1);
                                }

                            }

                            #endregion
                            foreach (EmployeesList item in iObj.EmployeesList)
                            {
                                if (item.CompanyUserId == -1)
                                {
                                    TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();
                                    if (PlanObj != null)
                                    {

                                        if (PlanObj.ContributionSchedule == 1)
                                        {
                                            PaymentType = CObjects.enumPaymentType.DM.ToString();
                                        }
                                        else
                                        {
                                            PaymentType = CObjects.enumPaymentType.DA.ToString();

                                        }
                                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.IsActive == true && p.EmailAddress == item.Email select p).Take(1).SingleOrDefault();
                                        if (UserObj == null)
                                        {
                                            string Password = RandomString();
                                            string EncryptPwd = CryptorEngine.Encrypt(Password);
                                            decimal GetLatestId = 0;
                                            GetLatestId = (from p in datacontext.TblCompanyUsers orderby p.CompanyUserId descending select p.CompanyUserId).Take(1).SingleOrDefault();
                                            if (GetLatestId == 0)
                                            {
                                                GetLatestId = 1;
                                            }
                                            else
                                            {
                                                GetLatestId = GetLatestId + 1;
                                            }
                                            TblCompanyUser Obj = new TblCompanyUser()
                                            {
                                                FirstName = item.FName,
                                                LastName = item.LName,
                                                UserName = (UniqueCode + "_" + item.FName) + GetLatestId.ToString(),
                                                ContactNo = item.ContactNo,
                                                Pwd = EncryptPwd,
                                                UserTypeId = Convert.ToInt16(item.UserTypeId),
                                                EmailAddress = item.Email,
                                                IsActive = true,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                DateModified = DateTime.UtcNow
                                            };
                                            datacontext.TblCompanyUsers.InsertOnSubmit(Obj);
                                            datacontext.SubmitChanges();

                                            UpdateMsgUser(Obj, "ADD", CompanyId);
                                            TblEmployee EmpObj = new TblEmployee()
                                            {
                                                CompanyUserId = Obj.CompanyUserId,
                                                CompanyPlanId = item.CompanyPlanId,
                                                FirstName = item.FName,
                                                LastName = item.LName,
                                                MobileNo = item.ContactNo,
                                                EmailAddress = item.Email,
                                                IsActive = true,
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                DateModified = DateTime.UtcNow
                                            };
                                            datacontext.TblEmployees.InsertOnSubmit(EmpObj);
                                            datacontext.SubmitChanges();
                                            CompanyObj.NoOfEmployee = CompanyObj.NoOfEmployee + 1;
                                            mastercontext.SubmitChanges();

                                            #region payment

                                         
                                            PaymentDetails = "Initial Payment";
                                            PaymentType = CObjects.enumPaymentType.IN.ToString();

                                            TblEmpTransaction EmpTransactionObj = new TblEmpTransaction()
                                            {
                                                TransactionDate = PaymentDate,
                                                TransactionDetails = PaymentDetails,
                                                Amount = PlanObj.NetDeposit,
                                                CompanyUserId = Obj.CompanyUserId,
                                                CompanyPlanId = PlanObj.CompanyPlanId,
                                                ClaimId = -1,
                                                PaymentTypeId = PaymentType,
                                                TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Credit),
                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                Active = true,
                                                DateCreated = DateTime.UtcNow
                                            };
                                            datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj);

                                            decimal BalanceAmount = 0;
                                            #region Apply account setUp fee
                                            TblFeeDetail SetupFeeObj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.IsApplicable == true && p.Title.ToUpper() == "SETUP FEE/INITIAL FEE" select p).Take(1).SingleOrDefault();
                                            if (SetupFeeObj != null)
                                            {
                                                TblEmpTransaction EmpTransactionObj1 = new TblEmpTransaction()
                                                {
                                                    TransactionDate = DateTime.UtcNow,
                                                    TransactionDetails = "Account Set-up Fee",
                                                    Amount = SetupFeeObj.Amount,
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    CompanyPlanId = item.CompanyPlanId,
                                                    ClaimId = -1,
                                                    TypeId = Convert.ToDecimal(CObjects.enumAdjustmentType.Debit),
                                                    PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow
                                                };
                                                datacontext.TblEmpTransactions.InsertOnSubmit(EmpTransactionObj1);

                                                TblEmpAdjustment AdjObj = new TblEmpAdjustment()
                                                {
                                                    CompanyUserId = Obj.CompanyUserId,
                                                    Amount = SetupFeeObj.Amount,
                                                    AdjustmentDT = DateTime.UtcNow,
                                                    Remarks = "Account SetUp Fee",
                                                    TypeId = Convert.ToInt16(CObjects.enumAdjustmentType.Debit),
                                                    Active = true,
                                                    DateCreated = DateTime.UtcNow,
                                                    DateModified = DateTime.UtcNow
                                                };
                                                datacontext.TblEmpAdjustments.InsertOnSubmit(AdjObj);

                                                BalanceAmount = Convert.ToDecimal(-SetupFeeObj.Amount);
                                            }
                                            #endregion

                                            TblPayment PaymentObj1 = new TblPayment()
                                            {
                                                PaymentDate = PaymentDate,
                                                PaymentDetails = PaymentDetails,
                                                RecurringDeposit = PlanObj.RecurringDeposit,
                                                InitialDeposit = PlanObj.InitialDeposit,
                                                NetDeposit = PlanObj.NetDeposit,
                                                AdministrationInitialFee = PlanObj.AdministrationInitialFee,
                                                AdministrationMonthlyFee = PlanObj.AdministrationMonthlyFee,
                                                HSTInitial = PlanObj.HSTInitial,
                                                HSTMonthly = PlanObj.HSTMonthly,
                                                PremiumTaxInitial = PlanObj.PremiumTaxInitial,
                                                PremiumTaxMonthly = PlanObj.PremiumTaxMonthly,
                                                RetailSalesTaxInitial = PlanObj.RetailSalesTaxInitial,
                                                RetailSalesTaxMonthly = PlanObj.RetailSalesTaxMonthly,
                                                GrossTotalInitial = PlanObj.GrossTotalInitial,
                                                GrossTotalMonthly = PlanObj.GrossTotalMonthly,
                                                Amount = PlanObj.GrossTotalInitial + PlanObj.GrossTotalMonthly + PlanObj.NetDeposit,
                                                CompanyPlanId = PlanObj.CompanyPlanId,
                                                CompanyUserId = Obj.CompanyUserId,
                                                PaymentDueDate = PaymentDate,
                                                PaymentTypeId = PaymentType,
                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                Active = true,
                                                DateCreated = DateTime.UtcNow,
                                                IsRemit = false
                                            };


                                            datacontext.TblPayments.InsertOnSubmit(PaymentObj1);

                                            TblEmpBalance EmpPaymentObj = new TblEmpBalance()
                                            {
                                                Amount = BalanceAmount,
                                                CompanyUserId = Obj.CompanyUserId,
                                                Active = true,
                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                DateModified = DateTime.UtcNow,
                                                DateCreated = DateTime.UtcNow
                                            };

                                            datacontext.TblEmpBalances.InsertOnSubmit(EmpPaymentObj);
                                        
                                            
                                            datacontext.SubmitChanges();
                                            AmountToBepaid = Convert.ToDecimal(PaymentObj1.Amount);

                                            #endregion payment

                                            #region Broker Commission
                                            TblCompanyReferral CompReferral = (from p in datacontext.TblCompanyReferrals
                                                                               where p.Active == true && p.CompanyId == CompanyObj.CompanyId
                                                                               select p).Take(1).SingleOrDefault();
                                            if (CompReferral != null)
                                            {
                                                
                                                decimal BrokerAmountToBepaid = (from p in datacontext.TblPayments
                                                                                where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                                                select Convert.ToDecimal(p.NetDeposit)).Sum();

                                                if (CompReferral.BrokerId != -1)
                                                {
                                                    TblBroker BrokerObj = (from p in mastercontext.TblBrokers
                                                                           where p.Active == true && p.BrokerId == CompReferral.BrokerId
                                                                           select p).Take(1).SingleOrDefault();
                                                    TblBrokerCompanyFee FeeObj = (from p in mastercontext.TblBrokerCompanyFees
                                                                                  where p.Active == true && p.BrokerId == CompReferral.BrokerId && p.CompanyId == CompanyObj.CompanyId
                                                                                  select p).Take(1).SingleOrDefault();
                                                    decimal PercentageBrokerCharge = 0;
                                                    if (FeeObj != null)
                                                    {
                                                        if (FeeObj.BrokerFee > 0)
                                                        {
                                                            PercentageBrokerCharge = Convert.ToDecimal(FeeObj.BrokerFee);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (BrokerObj.CommissionCharges != null && BrokerObj.CommissionCharges > 0)
                                                        {
                                                            PercentageBrokerCharge = Convert.ToDecimal(BrokerObj.CommissionCharges);
                                                        }
                                                        else
                                                        {
                                                            PercentageBrokerCharge = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select Convert.ToDecimal(p.BrokerMin)).Take(1).SingleOrDefault();
                                                        }
                                                    }


                                                    if (PercentageBrokerCharge > 0)
                                                    {
                                                        decimal Amount = Convert.ToDecimal((BrokerAmountToBepaid * PercentageBrokerCharge) / 100);
                                                        TblBrokerPayment TransactionObj = (from p in mastercontext.TblBrokerPayments
                                                                                           where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) &&
                                                                                           p.UserId == BrokerObj.UserId
                                                                                           select p).Take(1).SingleOrDefault();
                                                        if (TransactionObj != null)
                                                        {
                                                            TransactionObj.Amount = Amount;
                                                            TransactionObj.Contribution = BrokerAmountToBepaid;
                                                            TransactionObj.PercentFee = PercentageBrokerCharge;
                                                        }
                                                        else
                                                        {
                                                            TblBrokerPayment TransactionObj1 = new TblBrokerPayment()
                                                            {
                                                                PaymentDate = DateTime.UtcNow,
                                                                PaymentDueDate = DateTime.UtcNow,
                                                                PaymentDetails = "Payments",
                                                                Amount = Amount,
                                                                CompanyId = CompanyObj.CompanyId,
                                                                Contribution = BrokerAmountToBepaid,
                                                                PercentFee = PercentageBrokerCharge,
                                                                UserId = BrokerObj.UserId,
                                                                PaymentTypeId = CObjects.enumPaymentType.DM.ToString(),
                                                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                                                Active = true,
                                                                DateCreated = DateTime.UtcNow
                                                            };
                                                            mastercontext.TblBrokerPayments.InsertOnSubmit(TransactionObj1);
                                                        }
                                                        mastercontext.SubmitChanges();
                                                    }
                                                }
                                            }
                                            #endregion Broker Commission

                                            #region SendMail

                                            
                                            //send mail
                                            StringBuilder builder = new StringBuilder();
                                            builder.Append("Hello " + item.FName + ",<br/><br/>");
                                            builder.Append("<p style=\"margin:0px;color:#000\"> <strong>Welcome to eFlex, your very own health spending account set up by your employer, " + CompanyObj.CompanyName + ".</strong> <br>");
                                            builder.Append("This account lets you manage your health, your way with unparalleled flexibility. Plus, with our eFlex app");
                                            builder.Append(" and unique payment card, you never have to be out of pocket!</p><br>");

                                            builder.Append("<p style=\"margin:0px;color:#000\"> First things first, please login and complete your online account <a href=" + lstrloginUrl + ">here</a></p><br>");
                                             builder.Append("<p style=\"margin:0px;color:#000\"><strong>Company Code: </strong>" + CompanyObj.UniqueCode + "</p>");
                                             builder.Append("<p style=\"margin:0px;color:#000\"><strong>Username: </strong>" + Obj.UserName.Split('_')[1] + "</p>");
                                             builder.Append("<p style=\"margin:0px;color:#000\"><strong>Password: </strong>" + Password + "</p>");
								             builder.Append("<br>");
								             builder.Append("<p style=\"margin:0px;color:#000\">Once you login, you can manage your account, view your balance, submit claims, and more.</p>");
								             builder.Append("<p style=\"margin:0px;color:#000\">For further convenience, we have SMS notifications to keep you up-to-date about your claims and other important aspects of your account.</p>");
                                             builder.Append("<p style=\"margin:0px;color:#000\">And be sure to download our eFlex app for on-the-go account management and point-of-purchase claim submission!</p>");

                                             string lstrMessage = File.ReadAllText(EmailFormats + "EmployeeEmailTemplate.txt");
                                             lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                  


                                            string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                            string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                            string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                            Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                            CMail Mail = new CMail();
                                            Mail.EmailTo = item.Email;
                                            Mail.Subject = "eFlex Account - Welcome";
                                            Mail.MessageBody = lstrMessage;
                                            Mail.GodaddyUserName = GodaddyUserName;
                                            Mail.GodaddyPassword = GodaddyPwd;
                                            Mail.Server = Server;
                                            Mail.Port = Port;
                                            Mail.CompanyId = CompanyObj.CompanyId;
                                            bool IsSent = Mail.SendEMail(mastercontext, true);


                                            #endregion SendMail

                                            #region Push Notifications
                                            List<TblNotificationDetail> NotificationList = null;
                                            string NotifyText = "Your eFlex account has been set up by Admin. Please login and update your profile. Thanks!";
                                            NotificationList = InsertionInNotificationTable(EmpObj.CompanyUserId, NotifyText, datacontext, EmpObj.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());


                                            datacontext.SubmitChanges();

                                            if (NotificationList != null && NotificationList.Count() > 0)
                                            {
                                                SendNotification(datacontext, NotificationList, EmpObj.CompanyUserId);
                                                NotificationList.Clear();

                                            }
                                            #endregion Push Notifications




                                            TblUser AdminUserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();

                                            string Text = "An employee named as " + item.FName + " " + item.LName + " has been added on " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.UtcNow) + " by " + AdminUserObj.FirstName + " " + AdminUserObj.LastName + ".";
                                            CreateHistoryLog(mastercontext, CompanyObj.CompanyId, iObj.LoggedInUserId, Convert.ToDecimal(EmpObj.CompanyUserId), Text);

                                        }
                                        else
                                        {
                                            if (ErrorString == "")
                                            {
                                                ErrorString = item.Email + ",";
                                            }
                                            else
                                            {
                                                ErrorString = ErrorString + "," + item.Email + ",";
                                            }
                                        }
                                    }

                                }

                            }//end foreach


                     

                            decimal OwnerId = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p.CompanyUserId).Take(1).SingleOrDefault();
                            TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompanyId == CompanyObj.CompanyId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();
                            if (CompPaymentObj != null)
                            {
                                CompPaymentObj.Amount = CompPaymentObj.Amount + AmountToBepaid;
                            }
                            else
                            {
                                TblCompPayment CompPaymentObj1 = new TblCompPayment()
                                {
                                    PaymentDueDate = PaymentDate,
                                    PaymentDate = PaymentDate,
                                    PaymentDetails = PaymentDetails,
                                    Amount = AmountToBepaid,
                                    CompanyId = CompanyObj.CompanyId,
                                    CompanyUserId = OwnerId,
                                    PaymentTypeId = PaymentType,
                                    TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow
                                };
                                mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                                mastercontext.SubmitChanges();
                                TblCompAdjustment AdjObj = new TblCompAdjustment()
                                {
                                    CompanyId = CompanyObj.CompanyId,
                                    Amount = AmountToBepaid,
                                    CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                    PaymentTypeId = PaymentType,
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                    AdjustmentDT = DateTime.UtcNow,
                                    Remarks = PaymentDetails,
                                    TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                            }
                            mastercontext.SubmitChanges();

                        }
                        #endregion

                        if (ErrorString != "")
                        {
                            ErrorString = ErrorString.TrimEnd(',');
                            lobjResponse.Message = "We couldn't add this user because this email id is already used by other user (" + ErrorString + ").";
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                        else
                        {
                            lobjResponse.Message = "SUCCESS";
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }


                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                    datacontext.Connection.Close();
                }

                CreateMasterResponseLog(mastercontext, StartTime, UserId, "AddEmployeeByadmin", "", lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            else
            {
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                lobjResponse.Message =  CObjects.SessionExpired.ToString();
                IsLogOut = true;
            }
            lobjResponse.IsLogOut = IsLogOut;
            return lobjResponse;
        }
        #endregion

        #region GetEStatementForAdmin
        public CGetEStatementResponse GetEStatementForAdmin(GetEStatementInput iObj)
        {
            CGetEStatementResponse lobjResponse = new CGetEStatementResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string Constring = GetConnection(iObj.DbCon);
            decimal CompanyId = Convert.ToDecimal(iObj.DbCon.Split('_')[1]);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            MasterDBDataContext mastercontext = new MasterDBDataContext();
            if (IsValidateEflexUser(mastercontext))
            {
                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                {
                    try
                    {
                        string contents = "";
                        StringBuilder builder = new StringBuilder();
                        string SamplePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        contents = File.ReadAllText(SamplePath + "eStatementSample.html");
                        string lstrDetailHtml = "";
                        System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                        string strMonthName = mfi.GetMonthName(iObj.Month).ToString();
                        List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() && (p.PaymentDueDate.Value.Month == iObj.Month && p.PaymentDueDate.Value.Year == iObj.Year) select p).ToList();
                        decimal TotalContribution = 0;

                        decimal TotalAdminFee = 0;
                        decimal TotalHST = 0;
                        decimal TotalPPT = 0;
                        decimal TotalRST = 0;
                        decimal TotaltaxFees = 0;
                        if (PaymentObj != null && PaymentObj.Count() > 0)
                        {

                            builder.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                            builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"></td>");
                            builder.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td></tr>");

                           
                          
                            foreach (TblPayment item in PaymentObj)
                            {
                                string EmployeeName = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == item.CompanyUserId select p.FirstName + " " + p.LastName).Take(1).SingleOrDefault();
                                TblCompanyPlan Plan = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();
                                builder.Append("<tr style=\"background-color:#eee;\">");
                                builder.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.PaymentDate).ToString("MMM dd, yyyy") + "</b></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"><b>" + EmployeeName + "- " + Plan.Name + "</b></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                builder.Append("<td width=\"30%\" align=\"right\"><b>$" + item.NetDeposit + "</b></td>");
                                builder.Append("</tr>");
                                if (item.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())//if initial amount is pending
                                {
                                    builder.Append("<tr bgcolor=\"#fff\">");
                                    builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                    builder.Append("<td width=\"30%\" align=\"right\">Initial</td>");
                                    builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                    builder.Append("<td width=\"30%\" align=\"right\">$" + item.InitialDeposit + "</td>");
                                    builder.Append("</tr>");
                                    if (Plan.ContributionSchedule == 1)
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                    else
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                }
                                else //if initial amount is not pending
                                {
                                    if (Plan.ContributionSchedule == 1)
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                    else
                                    {
                                        builder.Append("<tr bgcolor=\"#fff\">");
                                        builder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                        builder.Append("<td width=\"30%\" align=\"right\"></td>");
                                        builder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                        builder.Append("</tr>");
                                    }
                                }
                                TotalContribution = Convert.ToDecimal(TotalContribution + item.NetDeposit);

                                TotalAdminFee = Convert.ToDecimal(TotalAdminFee + item.AdministrationInitialFee + item.AdministrationMonthlyFee);
                                TotalRST = Convert.ToDecimal(TotalRST + item.RetailSalesTaxInitial + item.RetailSalesTaxMonthly);
                                TotalPPT = Convert.ToDecimal(TotalPPT + item.PremiumTaxInitial + item.PremiumTaxMonthly);
                                TotalHST = Convert.ToDecimal(TotalHST + item.HSTInitial + item.HSTMonthly);


                            }

                            TotaltaxFees = TotaltaxFees + (TotalAdminFee + TotalRST + TotalPPT + TotalHST);
                        }
                        lstrDetailHtml = builder.ToString();
                        contents = contents.Replace("lstrBodyContent", lstrDetailHtml);
                        contents = contents.Replace("lstrGrandTotal", String.Format("{0:0.00}", TotalContribution));

                         #region Adjustment
                         string lstrAdjHtml = "";
                         StringBuilder builder1 = new StringBuilder();
                         decimal TotalAdjustment = 0;
                         List<TblCompAdjustment> AdjustmentObj = (from p in mastercontext.TblCompAdjustments where p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                      && (p.DateCreated.Value.Month == iObj.Month && p.DateCreated.Value.Year == iObj.Year) && p.CompanyId == CompanyId
                                                                  select p).ToList();
                         if (AdjustmentObj != null && AdjustmentObj.Count() > 0)
                         {
                             builder1.Append("<tr style=\"background-color: #eee;\">");
                             builder1.Append("<td colspan=\"4\" height=\"40\" align=\"left\"><h2>Adjustments</h2></td></tr>");


                             builder1.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                             builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>Type</b></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td>");
                             builder1.Append("</tr>");
                             foreach (TblCompAdjustment item in AdjustmentObj)
                             {
                                 builder1.Append("<tr style=\"background-color:#fff;\">");
                                 builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.DateCreated).ToString("MMM dd, yyyy") + "</b></td>");
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>" + item.Remarks + "</b></td>");
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>" + (item.TypeId == 2 ? "Debit" : "Credit") + "</b></td>");
                                
                                 builder1.Append("<td width=\"30%\" align=\"right\"><b>$" + item.Amount + "</b></td>");
                                 builder1.Append("</tr>");
                             }
                             decimal TotalCredit = 0;
                             decimal TotalDebit = 0;
                             TotalCredit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                             TotalDebit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Debit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                             TotalAdjustment = TotalCredit - TotalDebit;
                            
                             builder1.Append("<tr style=\"background-color:#eee;\">");
                             builder1.Append("<td width=\"20%\" height=\"40\" align=\"right\" ></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>Total</b></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"></td>");
                             builder1.Append("<td width=\"30%\" align=\"right\"><b>$" + TotalAdjustment + "</b></td>");
                             builder1.Append("</tr>");
                            
                         }
                         lstrAdjHtml = builder1.ToString();
                         contents = contents.Replace("lstrAdjustmentContent", lstrAdjHtml);
                         #endregion

                        decimal pctAdminFee = 0;
                        decimal pctHST = 0;
                        decimal pctPPT = 0;
                        decimal pctRST = 0;

                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == CompanyId select p).Take(1).SingleOrDefault();
                        if (CompanyObj != null)
                        {
                            TblProvince ProvinceObj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == CompanyObj.Province select p).Take(1).SingleOrDefault();
                            if (ProvinceObj != null)
                            {
                                pctAdminFee = Convert.ToDecimal(ProvinceObj.AdminFeePercent);
                                pctRST = Convert.ToDecimal(ProvinceObj.RST);
                                pctPPT = Convert.ToDecimal(ProvinceObj.PPT);
                                pctHST = Convert.ToDecimal(ProvinceObj.HST) == 0 ? Convert.ToDecimal(ProvinceObj.GST) : Convert.ToDecimal(ProvinceObj.HST);
                            }
                            TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                            if (AdminFeeObj != null)
                            {
                                decimal BrokerCommission = GetBrokerComissionCharges(CompanyId);
                                if (BrokerCommission > 0)
                                {
                                    pctAdminFee = Convert.ToDecimal(BrokerCommission + AdminFeeObj.AdminMin);
                                }
                                else
                                {
                                    pctAdminFee = Convert.ToDecimal(AdminFeeObj.AdminMax);
                                }
                            }
                            contents = contents.Replace("lstrInvoiceNo", CompanyObj.UniqueCode + "_"+DateTime.UtcNow.Month + DateTime.UtcNow.Year);
                            contents = contents.Replace("lstrCompanyName", CompanyObj.CompanyName);
                            contents = contents.Replace("lstrAddress", CompanyObj.Address1 + ((CompanyObj.Address2 == null || CompanyObj.Address2 == "") ? "" : " ," + CompanyObj.Address2) + ((CompanyObj.City == null || CompanyObj.City == "") ? "" : " ," + CompanyObj.City));
                            contents = contents.Replace("lstrEmail", CompanyObj.OwnerEmail);
                            contents = contents.Replace("lstrYear", iObj.Year.ToString());
                            contents = contents.Replace("LstrMonth", strMonthName);

                        }
                        contents = contents.Replace("lstrAdminFee", (String.Format("{0:0.00}", pctAdminFee)));
                        contents = contents.Replace("lstrHST", (String.Format("{0:0.00}", pctHST)));
                        contents = contents.Replace("lstrPPT", (String.Format("{0:0.00}", pctPPT)));
                        contents = contents.Replace("lstrRST", (String.Format("{0:0.00}", pctRST)));
                        contents = contents.Replace("TotalAdminFee", "$" + (String.Format("{0:0.00}", TotalAdminFee)));
                        contents = contents.Replace("TotalHST", "$" + (String.Format("{0:0.00}", TotalHST)));
                        contents = contents.Replace("TotalPPT", "$" + (String.Format("{0:0.00}", TotalPPT)));
                        contents = contents.Replace("TotalRST", "$" + (String.Format("{0:0.00}", TotalRST)));
                        contents = contents.Replace("TotalFeeTax", "$" + (String.Format("{0:0.00}", TotaltaxFees)));
                        contents = contents.Replace("lstrNetTotal", "$" + (String.Format("{0:0.00}", TotalContribution + TotaltaxFees)));
                        
                        TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select p).Take(1).SingleOrDefault();
                        if (CreditObj != null)
                        {
                            decimal AvaiableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select Convert.ToDecimal(p.Credits)).Sum();

                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", AvaiableCredits));

                        }
                        else
                        {
                            contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", 0));

                        }

                        string FileName = "Estatement_" + DateTime.UtcNow.Month+DateTime.UtcNow.Year;
                        if (File.Exists(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf"))
                        {
                            System.IO.File.Delete(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf");
                        }
                        createPDF(FileName, contents, CompanyObj.CompanyId);
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        lobjResponse.Message = "SUCCESS";
                        lobjResponse.FileName = FileName + ".pdf";

                    }
                    catch (Exception ex)
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message = ex.Message;
                    }
                    datacontext.Connection.Close();
                  
                }
            }
            else
            {
                lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                lobjResponse.Message =  CObjects.SessionExpired.ToString();
                IsLogOut = true;
            }
            lobjResponse.IsLogOut = IsLogOut;
            mastercontext.Connection.Close();
            return lobjResponse;
        }
        #endregion GetEStatement

        #region ListOfProviceTaxDetails
        public CListOfProviceTaxDetailsResponse ListOfProviceTaxDetails(IdInput iObj)
        {
            CListOfProviceTaxDetailsResponse lobjResponse = new CListOfProviceTaxDetailsResponse();
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        List<ListOfProviceTaxDetails> Obj = (from p in mastercontext.TblProvinces where p.Active == true 
                                                             select new ListOfProviceTaxDetails()
                                                               {
                                                                  ProvinceId = p.ProvinceId,
                                                                  Name= p.Name,
                                                                  AdminFeePercent = Convert.ToDecimal(p.AdminFeePercent),
                                                                  RST = Convert.ToDecimal(p.RST),
                                                                  HST = Convert.ToDecimal(p.HST),
                                                                  GST = Convert.ToDecimal(p.GST),
                                                                  PPT = Convert.ToDecimal(p.PPT),
                                                               }).ToList();

                        lobjResponse.ListOfProviceTaxDetails = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion ListOfCompanies

        #region UpdateProvinceTaxDetails
        public CResponse UpdateProvinceTaxDetails(UpdateProvinceTaxDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        TblProvince Obj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == iObj.ProvinceId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            Obj.Name= iObj.Name;
                          
                            Obj.RST = Convert.ToDecimal(iObj.RST);
                            Obj.HST = Convert.ToDecimal(iObj.HST);
                            Obj.GST = Convert.ToDecimal(iObj.GST);
                            Obj.PPT = Convert.ToDecimal(iObj.PPT);
                            Obj.DateModified = DateTime.UtcNow;


                            mastercontext.SubmitChanges();
                           lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }
                        

                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, -1, "UpdateProvinceTaxDetails", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region AddProvinceTaxDetails
        public CResponse AddProvinceTaxDetails(AddProvinceTaxDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        TblProvince Obj = (from p in mastercontext.TblProvinces where p.Active == true && p.Name == iObj.Name select p).Take(1).SingleOrDefault();
                        if (Obj == null)
                        {
                            TblProvince ProvinceObj = new TblProvince()
                            {
                                Name = iObj.Name,
                               
                                RST = Convert.ToDecimal(iObj.RST),
                                HST = Convert.ToDecimal(iObj.HST),
                                GST = Convert.ToDecimal(iObj.GST),
                                PPT = Convert.ToDecimal(iObj.PPT),
                                DateModified = DateTime.UtcNow,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                            };
                            mastercontext.TblProvinces.InsertOnSubmit(ProvinceObj);
                            mastercontext.SubmitChanges();


                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }


                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }

                CreateMasterResponseLog(mastercontext, StartTime, UserId, "AddProvinceTaxDetails", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region DeleteProvinceTaxDetails
        public CResponse DeleteProvinceTaxDetails(IdInputv2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {
                        TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.Province == iObj.Id select p).Take(1).SingleOrDefault();
                        if(CompanyObj != null)
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                            lobjResponse.Message = "Unable to delete this province as it is linked with an active company.";
                        }
                        else
                        {
                            TblProvince Obj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == iObj.Id select p).Take(1).SingleOrDefault();
                            if (Obj != null)
                            {
                                Obj.Active = false;
                                Obj.DateModified = DateTime.UtcNow;


                                mastercontext.SubmitChanges();
                                CreateMasterResponseLog(mastercontext, StartTime, -1, "DeleteProvinceTaxDetails", InputJson, lobjResponse.Message);
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            }
                        }
                
                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region ListOfEflexNotificationLog
        //JK:250618
        public CNotificationLogResponse ListOfEflexNotificationLog(EflexNotificationLogInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CNotificationLogResponse lobjResponse = new CNotificationLogResponse();
            UserId = Convert.ToDecimal(iObj.LoggedInUserId);
            bool IsLogOut = false;
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {
                    if (IsValidateEflexUser(datacontext))
                    {
                        List<NotificationLog> obj = (from p in datacontext.sp_ListOfEflexNotifyLog(iObj.ChunkStart, iObj.SearchString, Convert.ToInt16(iObj.ChunkSize),iObj.LoggedInUserId)
                                                     select new NotificationLog()
                                                     {
                                                         NotifyDate = Convert.ToString(p.DateCreated),
                                                         NotifyLogId = p.EflexNotifyLogId,
                                                         NotifyText = p.NotifyText
                                                     }).ToList();
                        lobjResponse.NotificationLog = obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                    }
                }
                catch (Exception ex)
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
               
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region GetEmpDashboardData
        public CGetEmpDashboardResponse GetEmpDashboardDataForAdmin(GetEmpDashboardInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            CGetEmpDashboardResponse lobjResponse = new CGetEmpDashboardResponse();
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            string DbCon = DBFlag + iObj.CompanyId;
            string Constring = GetConnection(DbCon);
            string UniqueCode = "";
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                    {
                        try
                        {
                            if (iObj.Type.ToUpper() == "MONTH")
                            {
                                List<string> AllMonths = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames.ToList();
                                List<decimal> ClaimAmtReceived = new List<decimal>();
                                List<decimal> TotalClaimCount = new List<decimal>();
                                List<string> Date = new List<string>();
                                List<string> Months = new List<string>();
                                Int32 TotalMonts = 12;
                                Int32 CurrentMonth = DateTime.UtcNow.Month;
                                List<EmpDashboardDataForAdmin> Result = new List<EmpDashboardDataForAdmin>();
                                for (int i = 0; i < CurrentMonth; i++)
                                {
                                    int month = i + 1;
                                    DateTime StartDate = new DateTime(DateTime.Now.Year, month, 1);
                                    DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);

                                    List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims
                                                                  where p.Active == true && p.CompanyUserId == iObj.UserId &&
                                                                  p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                                  select p).ToList();
                                    decimal ClaimCount = 0;
                                    decimal ClaimAmountReceived = 0;
                                    if (ClaimObj != null && ClaimObj.Count() > 0)
                                    {
                                        ClaimCount = ClaimObj.Count();
                                        ClaimAmountReceived = ClaimObj.Where(x=>(x.ClaimStatusId==Convert.ToInt16(CObjects.enumClaimStatus.InProgress) || x.ClaimStatusId==Convert.ToInt16(CObjects.enumClaimStatus.Paid))).Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                        TotalClaimCount.Add(ClaimCount);
                                        ClaimAmtReceived.Add(ClaimAmountReceived);
                                    }
                                    else
                                    {
                                        TotalClaimCount.Add(ClaimCount);
                                        ClaimAmtReceived.Add(ClaimAmountReceived);
                                    }
                                    string MonthName = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(month);
                                    Months.Add(MonthName);
                                }

                                lobjResponse.ClaimCount = TotalClaimCount;
                                lobjResponse.ClaimAmtReceived = ClaimAmtReceived;
                                lobjResponse.Data = Months;
                            }
                            if (iObj.Type.ToUpper() == "YEAR")
                            {
                                DateTime FirstActionDate = System.DateTime.Now;
                                List<string> YearData = new List<string>();
                                int Currentyear = FirstActionDate.Year;
                                
                                List<decimal> ClaimAmtReceived = new List<decimal>();
                                List<decimal> TotalClaimCount = new List<decimal>();
                                List<string> Date = new List<string>();
                                for (int i = 3; i >= 0; i--)
                                {
                                    int year = Currentyear - i;
                                    DateTime StartDate = new DateTime(year, 1, 1);
                                    DateTime EndDate = new DateTime(year, 12, 31);
                                    List<TblEmpClaim> ClaimObj = (from p in datacontext.TblEmpClaims
                                                                  where p.Active == true && p.CompanyUserId == iObj.UserId &&
                                                                  p.DateCreated.Value.Date >= StartDate.Date && p.DateCreated.Value.Date <= EndDate.Date
                                                                  select p).ToList();
                                    decimal ClaimCount = 0;
                                    decimal ClaimAmountReceived = 0;
                                    if (ClaimObj != null && ClaimObj.Count() > 0)
                                    {
                                        ClaimCount = ClaimObj.Count();
                                        ClaimAmountReceived = ClaimObj.Where(x => (x.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.InProgress) || x.ClaimStatusId == Convert.ToInt16(CObjects.enumClaimStatus.Paid))).Select(x => Convert.ToDecimal(x.ClaimAmt)).Sum();
                                        TotalClaimCount.Add(ClaimCount);
                                        ClaimAmtReceived.Add(ClaimAmountReceived);
                                    }
                                    else
                                    {
                                        TotalClaimCount.Add(ClaimCount);
                                        ClaimAmtReceived.Add(ClaimAmountReceived);
                                    }
                                    YearData.Add(Convert.ToString(year));
                                    
                                }
                                lobjResponse.ClaimCount = TotalClaimCount;
                                lobjResponse.ClaimAmtReceived = ClaimAmtReceived;
                                lobjResponse.Data = YearData;
                            }
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        }
                        catch (Exception ex)
                        {
                            lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                            lobjResponse.Message = ex.Message;
                        }
                        datacontext.Connection.Close();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   

                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "GetEmpDashboardDataForAdmin", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddFeeDetails
        public CResponse AddFeeDetails(AddFeeDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            bool IsLogOut = false;
            UserId = iObj.LoggedInUserId;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        TblFeeDetail Obj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.FeeTypeId== iObj.FeeTypeId && p.Title == iObj.Title select p).Take(1).SingleOrDefault();
                        if (Obj == null)
                        {
                            TblFeeDetail FeeObj = new TblFeeDetail()
                            {
                                Title = iObj.Title,
                                FeeTypeId = Convert.ToDecimal(iObj.FeeTypeId),
                                Amount = Convert.ToDecimal(iObj.Amount),
                                Frequency = Convert.ToDecimal(iObj.Frequency),
                                IsApplicable = Convert.ToBoolean(iObj.IsApplicable),
                                UserTypeId = Convert.ToDecimal(iObj.UserTypeId),
                                DateModified = DateTime.UtcNow,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                            };
                            mastercontext.TblFeeDetails.InsertOnSubmit(FeeObj);
                            mastercontext.SubmitChanges();


                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }


                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   
                }
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "AddFeeDetails", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region UpdateFeeDetails
        public CResponse UpdateFeeDetails(UpdateFeeDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        TblFeeDetail Obj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.FeeDetailId == iObj.FeeDetailId select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                            TblFeeDetail Obj1 = (from p in mastercontext.TblFeeDetails where p.Active == true && p.FeeDetailId != iObj.FeeDetailId && p.Title == iObj.Title && p.FeeTypeId == iObj.FeeTypeId select p).Take(1).SingleOrDefault();
                            if (Obj1 == null)
                            {
                                Obj.FeeTypeId = Convert.ToDecimal(iObj.FeeTypeId);
                                Obj.Amount = Convert.ToDecimal(iObj.Amount);
                                Obj.Frequency = Convert.ToDecimal(iObj.Frequency);
                                Obj.IsApplicable = Convert.ToBoolean(iObj.IsApplicable);
                                Obj.UserTypeId = Convert.ToDecimal(iObj.UserTypeId);
                                Obj.DateModified = DateTime.UtcNow;
                                mastercontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                lobjResponse.Message = "Fee details already exixts with this title and type.";

                            }
                            
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }


                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }

                CreateMasterResponseLog(mastercontext, StartTime, UserId, "UpdateFeeDetails", InputJson, lobjResponse.Message);
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region DeleteFeeDetails
        public CResponse DeleteFeeDetails(IdInputv2 iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        TblFeeDetail Obj = (from p in mastercontext.TblFeeDetails where p.Active == true && p.FeeDetailId == iObj.Id select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {
                               Obj.Active = false;
                               Obj.DateModified = DateTime.UtcNow;
                               mastercontext.SubmitChanges();
                               lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            
                        }
                        else
                        {
                            lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        }


                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "DeleteFeeDetails", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region ListFeeDetails
        public CListFeeDetailsResponse ListFeeDetails(IdInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CListFeeDetailsResponse lobjResponse = new CListFeeDetailsResponse();
            bool IsLogOut = false;
            UserId = iObj.Id;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        List<ListFeeDetails> Obj = (from p in mastercontext.TblFeeTypes
                                                    where p.Active == true
                                                    select new ListFeeDetails()
                                                    {
                                                        FeeType = p.Title,
                                                        FeeTypeId = p.FeeTypeId,
                                                        FeeDetailData = (from a in mastercontext.TblFeeDetails where a.Active == true && a.FeeTypeId == p.FeeTypeId select new FeeDetailData()
                                                        {
                                                            Title = a.Title,
                                                            Amount = Convert.ToDecimal(a.Amount),
                                                            Frequency = Convert.ToDecimal(a.Frequency),
                                                            IsApplicable = Convert.ToBoolean(a.IsApplicable),
                                                            UserTypeId = Convert.ToDecimal(a.UserTypeId),
                                                            FeeDetailId = Convert.ToDecimal(a.FeeDetailId)
                                                        }).ToList()
                                                        
                                                    }).ToList();

                            lobjResponse.ListFeeDetails = Obj;
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            CreateMasterResponseLog(mastercontext, StartTime, UserId, "ListFeeDetails", InputJson, lobjResponse.Message);

                       

                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                  
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region PopulateFeeType
        public CPopulateFeeTypeResponse PopulateFeeType(IdInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CPopulateFeeTypeResponse lobjResponse = new CPopulateFeeTypeResponse();
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        List<PopulateFeeType> Obj = (from p in mastercontext.TblFeeTypes
                                                    where p.Active == true
                                                     select new PopulateFeeType()
                                                    {
                                                        Title = p.Title,
                                                        FeeTypeId = Convert.ToDecimal(p.FeeTypeId),
                                                    }).ToList();

                        lobjResponse.PopulateFeeType = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        CreateMasterResponseLog(mastercontext, StartTime, UserId, "ListFeeDetails", InputJson, lobjResponse.Message);



                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region UpdateAdminFeeDetails
        public CResponse UpdateAdminFeeDetails(UpdateAdminFeeDetailsInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CResponse lobjResponse = new CResponse();
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                       TblAdminFeeDetail Obj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                        if (Obj != null)
                        {

                            Obj.Active = false; 
                            Obj.DateModified = DateTime.UtcNow;
                            mastercontext.SubmitChanges();
                        }
                            TblAdminFeeDetail Obj1 = new TblAdminFeeDetail()
                            {
                                AdminMax = iObj.AdminMax,
                                AdminMin = iObj.AdminMin,
                                BrokerMax = iObj.BrokerMax,
                                BrokerMin = iObj.BrokerMin,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            mastercontext.TblAdminFeeDetails.InsertOnSubmit(Obj1);
                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                            CreateMasterResponseLog(mastercontext, StartTime,UserId, "UpdateAdminFeeDetails", InputJson, lobjResponse.Message);

                       


                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion UpdateClaimStatus

        #region GetAdminFee
        public CGetAdminFeeResponse GetAdminFee(IdInput iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            CGetAdminFeeResponse lobjResponse = new CGetAdminFeeResponse();
            UserId = iObj.Id;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    try
                    {

                        GetAdminFeeData Obj = (from p in mastercontext.TblAdminFeeDetails
                                                    where p.Active == true
                                               select new GetAdminFeeData()
                                                    {
                                                        AdminMax = Convert.ToDecimal(p.AdminMax),
                                                        AdminMin = Convert.ToDecimal(p.AdminMin),
                                                        BrokerMax = Convert.ToDecimal(p.BrokerMax),
                                                        BrokerMin = Convert.ToDecimal(p.BrokerMin),
                                                       }).Take(1).SingleOrDefault();

                        lobjResponse.GetAdminFeeData = Obj;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                        CreateMasterResponseLog(mastercontext, StartTime, UserId, "GetAdminFee", InputJson, lobjResponse.Message);



                    }
                    catch (Exception ex)
                    {

                        lobjResponse.Message = ex.Message;
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   
                }
                lobjResponse.IsLogOut = IsLogOut;
                mastercontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion GetAdminFee

        #region GetEflexNotiMsgCount
        public CNotiMsgCountResponse GetEflexNotiMsgCount(EflexNotiMsgCountInfo iObj)
        {
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = "";
            decimal EventId = -1;
            
            string ActionTaken = "";
            string ServiceName = "GetEflexNotiMsgCount";
            CNotiMsgCountResponse lobjResponse = new CNotiMsgCountResponse();
            UserId = iObj.UserId;
            bool IsLogOut = false;
            using (MasterDBDataContext datacontext = new MasterDBDataContext())
            {
                try
                {

                    if (IsValidateEflexUser(datacontext))
                    {
                        decimal MessageCount = -1;
                        decimal UnreadNotificationCount = -1;

                        using (EflexMsgDBDataContext msgContext = new EflexMsgDBDataContext())
                        {
                            var Obj = (from p in msgContext.sp_GetUnreadCount(iObj.UserId, Convert.ToInt32(iObj.UserTypeId), -1) select p).Take(1).SingleOrDefault();
                            MessageCount = Convert.ToDecimal(Obj.UnreadCount);
                        }

                        UnreadNotificationCount = datacontext.TblEflexNotifyLogs.Where(x => x.Active == true && (","+x.ForUserId+",").Contains(Convert.ToString(","+iObj.UserId+",")) && x.IsRead == false).Count();
                        lobjResponse.MessageCount = MessageCount;
                        lobjResponse.UnreadNotificationCount = UnreadNotificationCount;
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                    }
                    else
                    {
                        lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                        lobjResponse.Message =  CObjects.SessionExpired.ToString();
                        IsLogOut = true;
                      

                    }
                }
                catch (Exception ex)
                {
                    InputJson = new JavaScriptSerializer().Serialize(iObj);
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message = ex.Message;
                }
                lobjResponse.IsLogOut = IsLogOut;
                datacontext.Connection.Close();
            }
            return lobjResponse;
        }
        #endregion

        #region AddCompAdjustments
        public CResponse AddCompAdjustments(AddCompAdjustmentsInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {
                    string NotifyText = "";
                    decimal Amount = 0;

                    TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                    if (CompanyObj != null)
                    {
                        string DbCon = DBFlag + iObj.CompanyId;
                        string Constring = GetConnection(DbCon);
                        string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                        string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                        string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                        Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);


                        if (iObj.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit))
                        {
                            Amount = -(iObj.Amount);
                            NotifyText = "$" + Amount + " was deposited into your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ". For details please login to your account.";

                        }
                        else
                        {
                            Amount = (iObj.Amount);
                            NotifyText = "$" + Amount + " was withdrawn from your eFlex Spending Account on " + Convert.ToDateTime(iObj.AdjustmentDT).ToString("MM/dd/yyyy") + ". For details, please login to your account.";

                        }

                        TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments
                                                         where p.Active == true && p.CompanyId == iObj.CompanyId
                                                         && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                         select p).Take(1).SingleOrDefault();
                        if (CompPaymentObj != null)
                        {
                            if (CompPaymentObj.PaymentDate.Value.Date != DateTime.UtcNow.Date)
                            {
                                CompPaymentObj.Amount = CompPaymentObj.Amount + Amount;

                                TblCompPayment CompPaymentObj1 = new TblCompPayment()
                                {
                                    PaymentDate = Convert.ToDateTime(iObj.AdjustmentDT),
                                    PaymentDueDate = Convert.ToDateTime(iObj.AdjustmentDT),
                                    PaymentDetails = iObj.Remarks,
                                    Amount = iObj.Amount,
                                    CompanyId = iObj.CompanyId,
                                    CompanyUserId = 1,
                                    TypeId = Convert.ToInt32(iObj.TypeId),
                                    PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow
                                };
                                mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                                mastercontext.SubmitChanges();

                                TblCompAdjustment AdjObj = new TblCompAdjustment()
                                {
                                    CompanyId = iObj.CompanyId,
                                    Amount = iObj.Amount,
                                    CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                    PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                    PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                    AdjustmentDT = DateTime.UtcNow,
                                    Remarks = iObj.Remarks,
                                    TypeId = Convert.ToInt16(iObj.TypeId),
                                    Active = true,
                                    DateCreated = DateTime.UtcNow,
                                    DateModified = DateTime.UtcNow
                                };
                                mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                                

                                #region SendInvoiceMail

                                using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                                {
                                    List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                                    if (PaymentObj != null && PaymentObj.Count() > 0)
                                    {
                                        decimal TotalDueAmount = PaymentObj.Select(x => Convert.ToDecimal(x.Amount)).Sum();
                                        DateTime PaymentDate = PaymentObj.Select(x => Convert.ToDateTime(x.PaymentDate)).Take(1).SingleOrDefault();
                                        string EmailTo = "";
                                        TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                                        if (UserObj != null)
                                        {
                                            string NotiText = "Due date of Plan(s) Initial amount " + TotalDueAmount.ToString() + " is " + Convert.ToDateTime(PaymentDate).ToString("MM/dd/yyyy") + ".";

                                            TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == UserObj.CompanyUserId && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                            if (CarrierObj != null)
                                            {
                                                string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                                CMail Mail1 = new CMail();
                                                Mail1.EmailTo = EmailTo;
                                                Mail1.Subject = "Initial Payment";
                                                Mail1.MessageBody = NotiText;
                                                Mail1.GodaddyUserName = GodaddyUserName;
                                                Mail1.GodaddyPassword = GodaddyPwd;
                                                Mail1.Server = Server;
                                                Mail1.Port = Port;
                                                Mail1.CompanyId = CompPaymentObj.CompanyId;
                                                bool IsSent1 = Mail1.SendEMail(datacontext, true);


                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(EmailTo))
                                                {
                                                    EmailTo = EmailTo + "," + UserObj.EmailAddress;
                                                }
                                                else
                                                {
                                                    EmailTo = UserObj.EmailAddress;

                                                }
                                            }


                                            NotifyUser1(datacontext, NotiText, UserObj.CompanyUserId,CObjects.enmNotifyType.Payment.ToString());

                                            
                                            StringBuilder builder1 = new StringBuilder();//asas

                                          
                                            builder1.Append("Hello  " + UserObj.FirstName + " " + UserObj.LastName + ",<br/><br/>");
                                            builder1.Append("Payment due date: " + Convert.ToDateTime(PaymentDate).ToString("MM/dd/yyyy") + ".<br/><br/>");
                                            builder1.Append("You and your employees will receive information on activating your individual accounts momentarily,");
                                            builder1.Append("and your customized eFlex Access Cards will be mailed to you within the next 10 business days.<br/><br/><br/><br/>");
                                            builder1.Append("If you have any questions or concerns about your eFlex account, please reach our to our team anytime at 416-748-9879 or eFlex@eclipseEIO.com <br/><br/>");
                                            builder1.Append("Thanks, and enjoy your eFlex account(s)!<br/>");
                                            builder1.Append("The eclipse EIO Team");





                                            string lstratchMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                            lstratchMessage = lstratchMessage.Replace("lstrEmailContent", builder1.ToString());

                                            #region AddAttachment
                                            string contents = "";
                                            StringBuilder attchbuilder = new StringBuilder();
                                            string SamplePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                            contents = File.ReadAllText(SamplePath + "eStatementSample.html");
                                            string lstrDetailHtml = "";
                                            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                                            decimal TotalContribution = 0;

                                            decimal TotalAdminFee = 0;
                                            decimal TotalHST = 0;
                                            decimal TotalPPT = 0;
                                            decimal TotalRST = 0;
                                            decimal TotaltaxFees = 0;
                                            if (PaymentObj != null && PaymentObj.Count() > 0)
                                            {
                                                attchbuilder.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                                                attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"></td>");
                                                attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td></tr>");

                                                foreach (TblPayment item in PaymentObj)
                                                {
                                                    string EmployeeName = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == item.CompanyUserId select p.FirstName + " " + p.LastName).Take(1).SingleOrDefault();
                                                    TblCompanyPlan Plan = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).Take(1).SingleOrDefault();

                                                    attchbuilder.Append("<tr style=\"background-color:#eee;\">");
                                                    attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.PaymentDate).ToString("MMM dd, yyyy") + "</b></td>");
                                                    attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>" + EmployeeName + "- " + Plan.Name + "</b></td>");
                                                    attchbuilder.Append("<td width=\"30%\" align=\"right\"><b>$" + item.NetDeposit + "</b></td>");
                                                    attchbuilder.Append("</tr>");
                                                    if (item.PaymentTypeId == CObjects.enumPaymentType.IN.ToString())//if initial amount is pending
                                                    {
                                                        attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                        attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">Initial</td>");
                                                        attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.InitialDeposit + "</td>");
                                                        attchbuilder.Append("</tr>");
                                                        if (Plan.ContributionSchedule == 1)
                                                        {
                                                            attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                            attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                            attchbuilder.Append("<td width=\"30%\" align=\"right\">Monthly</td>");
                                                            attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                                            attchbuilder.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            attchbuilder.Append("<tr bgcolor=\"#fff\">");
                                                            attchbuilder.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b></b></td>");
                                                            attchbuilder.Append("<td width=\"30%\" align=\"right\">Annual</td>");
                                                            attchbuilder.Append("<td width=\"30%\" align=\"right\">$" + item.RecurringDeposit + "</td>");
                                                            attchbuilder.Append("</tr>");
                                                        }
                                                    }

                                                    TotalContribution = Convert.ToDecimal(TotalContribution + item.NetDeposit);

                                                    TotalAdminFee = Convert.ToDecimal(TotalAdminFee + item.AdministrationInitialFee + item.AdministrationMonthlyFee);
                                                    TotalRST = Convert.ToDecimal(TotalRST + item.RetailSalesTaxInitial + item.RetailSalesTaxMonthly);
                                                    TotalPPT = Convert.ToDecimal(TotalPPT + item.PremiumTaxInitial + item.PremiumTaxMonthly);
                                                    TotalHST = Convert.ToDecimal(TotalHST + item.HSTInitial + item.HSTMonthly);


                                                }

                                                TotaltaxFees = TotaltaxFees + (TotalAdminFee + TotalRST + TotalPPT + TotalHST);
                                            }
                                            lstrDetailHtml = attchbuilder.ToString();


                                            contents = contents.Replace("lstrBodyContent", lstrDetailHtml);
                                            contents = contents.Replace("lstrGrandTotal", String.Format("{0:0.00}", TotalContribution));

                                            #region Adjustment
                                            string lstrAdjHtml = "";
                                            StringBuilder builder2 = new StringBuilder();
                                            decimal TotalAdjustment = 0;
                                            List<TblCompAdjustment> AdjustmentObj = (from p in mastercontext.TblCompAdjustments
                                                                                     where p.Active == true && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                                         && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) && p.CompanyId == iObj.CompanyId
                                                                                     select p).ToList();
                                            if (AdjustmentObj != null && AdjustmentObj.Count() > 0)
                                            {
                                                builder2.Append("<tr style=\"background-color: #eee;\">");
                                                builder2.Append("<td colspan=\"4\" height=\"40\" align=\"left\"><h2>Adjustments</h2></td></tr>");


                                                builder2.Append("<tr bgcolor=\"#ffbb33\" style=\"background-color:#ffbb33; color:#fff\">");
                                                builder2.Append("<td width=\"20%\" height=\"40\" align=\"right\"><b>Date</b></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"><b>Description</b></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"><b>Type</b></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"><b>Amount</b></td>");
                                                builder2.Append("</tr>");
                                                foreach (TblCompAdjustment item in AdjustmentObj)
                                                {
                                                    builder2.Append("<tr style=\"background-color:#fff;\">");
                                                    builder2.Append("<td width=\"20%\" height=\"40\" align=\"right\" ><b>" + Convert.ToDateTime(item.DateCreated).ToString("MMM dd, yyyy") + "</b></td>");
                                                    builder2.Append("<td width=\"30%\" align=\"right\"><b>" + item.Remarks + "</b></td>");
                                                    builder2.Append("<td width=\"30%\" align=\"right\"><b>" + (item.TypeId == 2 ? "Debit" : "Credit") + "</b></td>");

                                                    builder2.Append("<td width=\"30%\" align=\"right\"><b>$" + item.Amount + "</b></td>");
                                                    builder2.Append("</tr>");
                                                }
                                                decimal TotalCredit = 0;
                                                decimal TotalDebit = 0;
                                                TotalCredit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Credit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                                                TotalDebit = AdjustmentObj.Where(x => x.TypeId == Convert.ToDecimal(CObjects.enumAdjustmentType.Debit)).Select(x => Convert.ToDecimal(x.Amount)).Sum();
                                                TotalAdjustment = TotalCredit - TotalDebit;

                                                builder2.Append("<tr style=\"background-color:#eee;\">");
                                                builder2.Append("<td width=\"20%\" height=\"40\" align=\"right\" ></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"><b>Total</b></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"></td>");
                                                builder2.Append("<td width=\"30%\" align=\"right\"><b>$" + TotalAdjustment + "</b></td>");
                                                builder2.Append("</tr>");

                                            }
                                            lstrAdjHtml = builder2.ToString();
                                            contents = contents.Replace("lstrAdjustmentContent", lstrAdjHtml);
                                            #endregion


                                            decimal pctAdminFee = 0;
                                            decimal pctHST = 0;
                                            decimal pctPPT = 0;
                                            decimal pctRST = 0;


                                            if (CompanyObj != null)
                                            {
                                                TblProvince ProvinceObj = (from p in mastercontext.TblProvinces where p.Active == true && p.ProvinceId == CompanyObj.Province select p).Take(1).SingleOrDefault();
                                                if (ProvinceObj != null)
                                                {
                                                    pctAdminFee = Convert.ToDecimal(ProvinceObj.AdminFeePercent);
                                                    pctRST = Convert.ToDecimal(ProvinceObj.RST);
                                                    pctPPT = Convert.ToDecimal(ProvinceObj.PPT);
                                                    pctHST = Convert.ToDecimal(ProvinceObj.HST) == 0 ? Convert.ToDecimal(ProvinceObj.GST) : Convert.ToDecimal(ProvinceObj.HST);
                                                }

                                                TblAdminFeeDetail AdminFeeObj = (from p in mastercontext.TblAdminFeeDetails where p.Active == true select p).Take(1).SingleOrDefault();
                                                if (AdminFeeObj != null)
                                                {
                                                    decimal BrokerCommission = GetBrokerComissionCharges(iObj.CompanyId);
                                                    if (BrokerCommission > 0)
                                                    {
                                                        pctAdminFee = Convert.ToDecimal(BrokerCommission + AdminFeeObj.AdminMin);
                                                    }
                                                    else
                                                    {
                                                        pctAdminFee = Convert.ToDecimal(AdminFeeObj.AdminMax);
                                                    }
                                                }


                                                contents = contents.Replace("lstrInvoiceNo", CompanyObj.UniqueCode + "_" + DateTime.UtcNow.Month + DateTime.UtcNow.Year);
                                                contents = contents.Replace("lstrCompanyName", CompanyObj.CompanyName);
                                                contents = contents.Replace("lstrAddress", CompanyObj.Address1 + ((CompanyObj.Address2 == null || CompanyObj.Address2 == "") ? "" : " ," + CompanyObj.Address2) + ((CompanyObj.City == null || CompanyObj.City == "") ? "" : " ," + CompanyObj.City));
                                                contents = contents.Replace("lstrEmail", CompanyObj.OwnerEmail);

                                                string strMonthName = mfi.GetMonthName(PaymentDate.Month).ToString();
                                                contents = contents.Replace("lstrYear", PaymentDate.Year.ToString());
                                                contents = contents.Replace("LstrMonth", strMonthName);

                                            }
                                            contents = contents.Replace("lstrAdminFee", (String.Format("{0:0.00}", pctAdminFee)));
                                            contents = contents.Replace("lstrHST", (String.Format("{0:0.00}", pctHST)));
                                            contents = contents.Replace("lstrPPT", (String.Format("{0:0.00}", pctPPT)));
                                            contents = contents.Replace("lstrRST", (String.Format("{0:0.00}", pctRST)));
                                            contents = contents.Replace("TotalAdminFee", "$" + (String.Format("{0:0.00}", TotalAdminFee)));
                                            contents = contents.Replace("TotalHST", "$" + (String.Format("{0:0.00}", TotalHST)));
                                            contents = contents.Replace("TotalPPT", "$" + (String.Format("{0:0.00}", TotalPPT)));
                                            contents = contents.Replace("TotalRST", "$" + (String.Format("{0:0.00}", TotalRST)));
                                            contents = contents.Replace("TotalFeeTax", "$" + (String.Format("{0:0.00}", TotaltaxFees)));
                                            contents = contents.Replace("lstrNetTotal", "$" + (String.Format("{0:0.00}", TotalContribution + TotaltaxFees)));
                                            
                                            TblMasterCompCredit CreditObj = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select p).Take(1).SingleOrDefault();
                                            if (CreditObj != null)
                                            {
                                                decimal AvaiableCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == CompanyObj.CompanyId select Convert.ToDecimal(p.Credits)).Sum();

                                                contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", AvaiableCredits));

                                            }
                                            else
                                            {
                                                contents = contents.Replace("lstAvaiableCredits", String.Format("{0:0.00}", 0));

                                            }    


                                            string FileName = "Estatement_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                                            createPDF(FileName, contents, CompanyObj.CompanyId);


                                            #endregion



                                            CMail atchMail = new CMail();
                                            atchMail.EmailTo = EmailTo;// CarrierEmails;
                                            atchMail.Subject = "Initial Payment";
                                            atchMail.MessageBody = lstratchMessage;
                                            if (File.Exists(ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf"))
                                            {
                                                atchMail.AttachmentFiles = new List<string> { ConPath + CompanyObj.CompanyId + "\\" + FileName + ".pdf" };

                                            }

                                            atchMail.GodaddyUserName = GodaddyUserName;
                                            atchMail.GodaddyPassword = GodaddyPwd;
                                            atchMail.Server = Server;
                                            atchMail.Port = Port;
                                            atchMail.CompanyId = iObj.CompanyId;
                                            bool IsSent2 = atchMail.SendEMail(datacontext, true);


                                        }



                                    }
                                    datacontext.Connection.Close();
                                }
                                #endregion

                            }
                        }
                        else
                        {
                           
                            TblCompPayment CompPaymentObj1 = new TblCompPayment()
                            {
                                PaymentDate = Convert.ToDateTime(iObj.AdjustmentDT),
                                PaymentDueDate = Convert.ToDateTime(iObj.AdjustmentDT),
                                PaymentDetails = iObj.Remarks,
                                Amount = iObj.Amount,
                                CompanyId = iObj.CompanyId,
                                CompanyUserId = 1,
                                TypeId = Convert.ToInt32(iObj.TypeId),
                                PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                Active = true,
                                DateCreated = DateTime.UtcNow
                            };
                            mastercontext.TblCompPayments.InsertOnSubmit(CompPaymentObj1);
                            mastercontext.SubmitChanges();

                            TblCompAdjustment AdjObj = new TblCompAdjustment()
                             {
                                 CompanyId = iObj.CompanyId,
                                 Amount = iObj.Amount,
                                 CompanyPaymentId = CompPaymentObj1.CompPaymentId,
                                 PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                 PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited),
                                 AdjustmentDT = DateTime.UtcNow,
                                 Remarks = iObj.Remarks,
                                 TypeId = Convert.ToInt16(iObj.TypeId),
                                 Active = true,
                                 DateCreated = DateTime.UtcNow,
                                 DateModified = DateTime.UtcNow
                             };
                            mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                            mastercontext.SubmitChanges();


                        }
                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            #region send Notification
                            NotifyUser1(datacontext, NotifyText, 1, CObjects.enmNotifyType.Adjustment.ToString());
                               
                            
                            datacontext.SubmitChanges();


                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                            if (UserObj != null)
                            {
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == 1 && p.IsSMS == true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    string lstrMessage = "";
                                    StringBuilder builder = new StringBuilder();
                                    
                                    builder.Append(NotifyText + "<br/><br/>");
                                    

                                    lstrMessage = builder.ToString();
                                    //send mail


                                    CMail Mail = new CMail();
                                    Mail.EmailTo = CarrierEmails;
                                    Mail.Subject = "Your Adjustment";
                                    Mail.MessageBody = lstrMessage;
                                    Mail.GodaddyUserName = GodaddyUserName;
                                    Mail.GodaddyPassword = GodaddyPwd;
                                    Mail.Server = Server;
                                    Mail.Port = Port;
                                    Mail.CompanyId = iObj.CompanyId;
                                    bool IsSent = Mail.SendEMail(datacontext, true);

                                }
                            }
                            #endregion
                            datacontext.Connection.Close();

                        }
                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();


                        if (lobjResponse.Status == CObjects.enmStatus.Success.ToString())
                        {
                            CreateHistoryLog(mastercontext, iObj.CompanyId, iObj.LoggedInUserId, 1, NotifyText);

                        }
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   

                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "AddCompAdjustments", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
                }
            


            return lobjResponse;
        }
        #endregion

        #region AddLateFeeToCompany
        public CResponse AddLateFeeToCompany(AddLateFeeToCompanyInput iObj)
        {
            CResponse lobjResponse = new CResponse();
            DateTime StartTime = DateTime.UtcNow;
            string InputJson = new JavaScriptSerializer().Serialize(iObj);
            UserId = iObj.LoggedInUserId;
            bool IsLogOut = false;
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
            {
                if (IsValidateEflexUser(mastercontext))
                {

                    decimal Amount = 0;
                    
                    TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments
                                                     where p.Active == true && p.CompPaymentId == iObj.CompanyPaymentId
                                                     && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending)
                                                     select p).Take(1).SingleOrDefault();
                    if (CompPaymentObj != null)
                    {


                        if (iObj.IsLateFee)
                        {
                            Amount = Amount + (iObj.LateFee);
                            TblCompAdjustment AdjObj = new TblCompAdjustment()
                            {
                                CompanyId = CompPaymentObj.CompanyId,
                                Amount = Amount,
                                CompanyPaymentId = CompPaymentObj.CompPaymentId,
                                PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                AdjustmentDT = Convert.ToDateTime(CompPaymentObj.PaymentDate),
                                Remarks = "Late fee charges",
                                TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj);
                        }
                        if (iObj.IsESFFee)
                        {
                            Amount = Amount + (iObj.ESFFee);

                            TblCompAdjustment AdjObj1 = new TblCompAdjustment()
                            {
                                CompanyId = CompPaymentObj.CompanyId,
                                Amount = Amount,
                                CompanyPaymentId = CompPaymentObj.CompPaymentId,
                                PaymentTypeId = CObjects.enumPaymentType.ADJ.ToString(),
                                PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Pending),
                                AdjustmentDT = Convert.ToDateTime(CompPaymentObj.PaymentDate),
                                Remarks = "Charges added due to NSF.",
                                TypeId = Convert.ToInt32(CObjects.enumAdjustmentType.Debit),
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow


                            };
                            mastercontext.TblCompAdjustments.InsertOnSubmit(AdjObj1);
                            CompPaymentObj.Amount = CompPaymentObj.Amount + Amount;


                            mastercontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                        }


                        string NotifyText = "";
                        
                        NotifyText = "As your payment is not on time so you need to pay $" + Amount + " extra while payment.";
                        
                        string DbCon = DBFlag + CompPaymentObj.CompanyId;
                        string Constring = GetConnection(DbCon);
                        using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                        {
                            NotifyUser1(datacontext, NotifyText, 1, CObjects.enmNotifyType.Adjustment.ToString());
                           
                            datacontext.SubmitChanges();


                            TblCompanyUser UserObj = (from p in datacontext.TblCompanyUsers where p.Active == true && p.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner) select p).Take(1).SingleOrDefault();
                            if (UserObj != null)
                            {
                                TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == 1 && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                    string lstrMessage = "";
                                    StringBuilder builder = new StringBuilder();
                                   
                                    builder.Append(NotifyText + "<br/><br/>");
                                    
                                    lstrMessage = builder.ToString();
                                    //send mail

                                    string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                    string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                    string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                    Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                    CMail Mail = new CMail();
                                    Mail.EmailTo = CarrierEmails;
                                    Mail.Subject = "Late Fee charges";
                                    Mail.MessageBody = lstrMessage;
                                    Mail.GodaddyUserName = GodaddyUserName;
                                    Mail.GodaddyPassword = GodaddyPwd;
                                    Mail.Server = Server;
                                    Mail.Port = Port;
                                    Mail.CompanyId = CompPaymentObj.CompanyId;
                                    bool IsSent = Mail.SendEMail(datacontext, true);

                                }
                            }
                            datacontext.Connection.Close();
                        }
                    }
                }
                else
                {
                    lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                    lobjResponse.Message =  CObjects.SessionExpired.ToString();
                    IsLogOut = true;
                   

                }
                lobjResponse.IsLogOut = IsLogOut;
                CreateMasterResponseLog(mastercontext, StartTime, UserId, "AddLateFeeToCompany", InputJson, lobjResponse.Message);
                mastercontext.Connection.Close();
            }



            return lobjResponse;
        }
        #endregion

        #region UpdateCompanyCredits
         public CResponse UpdateCompanyCredits(UpdateCompanyCreditsInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     try
                     {
                         string Text = "";
                         TblCompany CompanyObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                         TblMasterCompCredit CompCredits = (from p in mastercontext.TblMasterCompCredits where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                         if(CompCredits != null)
                         {
                             CompCredits.Credits = iObj.Credits;
                             CompCredits.DateCreated = DateTime.UtcNow;
                            
                             mastercontext.SubmitChanges();
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                            
                         }
                         else
                         {
                             TblMasterCompCredit CreditsObj = new TblMasterCompCredit()
                             {
                                 CompanyId = iObj.CompanyId,
                                 Credits = iObj.Credits,
                                 DateCreated = DateTime.UtcNow,
                                 DateModified = DateTime.UtcNow,
                                 Active = true
                             };
                             mastercontext.TblMasterCompCredits.InsertOnSubmit(CreditsObj);
                             mastercontext.SubmitChanges();
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         }

                         if (lobjResponse.Status == CObjects.enmStatus.Success.ToString())
                         {
                             Text = "$" + iObj.Credits + " has been credited to " + CompanyObj.CompanyName + "'s account.";
                             CreateHistoryLog(mastercontext, iObj.CompanyId, iObj.LoggedInUserId, -1, Text);
                                
                         }

                     }
                     catch (Exception ex)
                     {

                         lobjResponse.Message = ex.Message;
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                     
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, -1, "UpdateCompanyCredits", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

         #region ResetPassword
         public CResponse ResetPassword(ResetPasswordInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     if (iObj.CompanyId != -1)
                     {
                         string DbCon = DBFlag + iObj.CompanyId;
                         string Constring = GetConnection(DbCon);
                         using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                         {
                             try
                             {
                                 TblCompanyUser CompUserObj = (from p in datacontext.TblCompanyUsers
                                                               where p.Active == true &&
                                                                   p.CompanyUserId == iObj.CompanyUserId
                                                               select p).Take(1).SingleOrDefault();
                                 if (CompUserObj != null)
                                 {
                                     string Password = RandomString();
                                     if (CompUserObj.UserTypeId == Convert.ToDecimal(CObjects.enmUserType.Owner))
                                     {
                                         CompUserObj.Pwd = CryptorEngine.Encrypt(RandomString());
                                         datacontext.SubmitChanges();
                                         TblCompany CompObj = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                                         if (CompObj != null)
                                         {
                                             CompObj.Pwd = CompUserObj.Pwd;
                                         }
                                         mastercontext.SubmitChanges();

                                     }
                                     #region SendMail

                                     string lstrFName = CompUserObj.FirstName + ((CompUserObj.LastName == null || CompUserObj.LastName == "") ? "" : " " + CompUserObj.LastName);
                                     
                                     StringBuilder builder = new StringBuilder();
                                     builder.Append("Hello " + lstrFName + ",<br/><br/>");
                                     builder.Append("Your password has been reset as: " + CryptorEngine.Decrypt(CompUserObj.Pwd) + "<br/><br/>");
                                     builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                                     builder.Append("Regards,<br/><br/>");
                                     builder.Append("Eflex Team<br/><br/>");

                                     
                                     //send mail
                                     string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                     lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                     string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                     string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                     string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                     Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                     CMail Mail = new CMail();
                                     Mail.EmailTo = CompUserObj.EmailAddress;
                                     Mail.Subject = "eFlex Spending Account Password Reset";
                                     Mail.MessageBody = lstrMessage;
                                     Mail.GodaddyUserName = GodaddyUserName;
                                     Mail.GodaddyPassword = GodaddyPwd;
                                     Mail.Server = Server;
                                     Mail.Port = Port;
                                     Mail.CompanyId = iObj.CompanyId;
                                     bool IsSent = Mail.SendEMail(mastercontext, true);


                                     #endregion SendMail

                                     string NotifyText = "Your password has been reset as: " + CryptorEngine.Decrypt(CompUserObj.Pwd);
                                     NotifyUser1(datacontext, NotifyText, iObj.CompanyUserId, CObjects.enmNotifyType.Password.ToString());

                                     #region Push Notifications


                                     List<TblNotificationDetail> NotificationList = null;

                                     NotificationList = InsertionInNotificationTable(iObj.CompanyUserId, NotifyText, datacontext, iObj.CompanyUserId, CObjects.enmNotifyType.Password.ToString());


                                     datacontext.SubmitChanges();

                                     if (NotificationList != null && NotificationList.Count() > 0)
                                     {
                                         SendNotification(datacontext, NotificationList, iObj.CompanyUserId);
                                         NotificationList.Clear();

                                     }

                                     #endregion Push Notifications
                                     datacontext.SubmitChanges();

                                     string Text = CompUserObj.FirstName + " " + CompUserObj.LastName + "'s password has been reset by the admin on " + DateTime.UtcNow.ToString("MM/dd/yyyy");
                                     CreateHistoryLog(mastercontext, iObj.CompanyId, UserId, CompUserObj.CompanyUserId, Text);
                                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                                 }
                                 else
                                 {
                                     lobjResponse.Message = "No record found.";
                                     lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                                 }

                             }
                             catch (Exception ex)
                             {

                                 lobjResponse.Message = ex.Message;
                                 lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                             }
                             datacontext.Connection.Close();
                         }
                     }
                     else
                     {
                         try
                         {
                             TblUser UserObj = (from p in mastercontext.TblUsers
                                                where p.Active == true &&
                                                    p.UserId == iObj.CompanyUserId
                                                select p).Take(1).SingleOrDefault();
                             if (UserObj != null)
                             {
                                 string Password = RandomString();

                                 UserObj.Pwd = CryptorEngine.Encrypt(RandomString());
                                 mastercontext.SubmitChanges();


                                 #region SendMail

                                 string lstrFName = UserObj.FirstName + ((UserObj.LastName == null || UserObj.LastName == "") ? "" : " " + UserObj.LastName);
                                 
                                 StringBuilder builder = new StringBuilder();
                                 builder.Append("Hello " + lstrFName + ",<br/><br/>");
                                 builder.Append("Your password has been reset as: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                                 builder.Append("Click here for login: <a href=" + lstrAdminUrl + " >" + lstrAdminUrl + "</a><br/><br/>");
                                 builder.Append("Regards,<br/><br/>");
                                 builder.Append("Eflex Team<br/><br/>");

                                 
                                 //send mail
                                 string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                 lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());

                                 string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                 string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                 string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                 Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                 CMail Mail = new CMail();
                                 Mail.EmailTo = UserObj.EmailAddress;
                                 Mail.Subject = "eFlex Spending Account Password Reset";
                                 Mail.MessageBody = lstrMessage;
                                 Mail.GodaddyUserName = GodaddyUserName;
                                 Mail.GodaddyPassword = GodaddyPwd;
                                 Mail.Server = Server;
                                 Mail.Port = Port;
                                 Mail.CompanyId = iObj.CompanyId;
                                 bool IsSent = Mail.SendEMail(mastercontext, true);


                                 #endregion SendMail


                                 string Text = UserObj.FirstName + " " + UserObj.LastName + "'s password has been reset by the admin on " + DateTime.UtcNow.ToString("MM/dd/yyyy");
                                 CreateHistoryLog(mastercontext, iObj.CompanyId, UserId, UserObj.UserId, Text);
                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                             }
                             else
                             {
                                 lobjResponse.Message = "No record found.";
                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             }

                         }
                         catch (Exception ex)
                         {

                             lobjResponse.Message = ex.Message;
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         }
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = CObjects.SessionExpired.ToString();
                     IsLogOut = true;

                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region ListTaxPot
         public CListTaxPotResponse ListTaxPot(IdInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             bool IsLogOut = false;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListTaxPotResponse lobjResponse = new CListTaxPotResponse();
             UserId = iObj.Id;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     List<ListTaxPot> Obj = new List<ListTaxPot>();
                     List<TblCompany> CompList = (from p in mastercontext.TblCompanies where p.Active == true && p.IsBlocked == false select p).ToList();
                     if(CompList != null && CompList.Count() > 0)
                     {
                         foreach (TblCompany item in CompList)
                         {
                             decimal AdminFee = 0;
                             decimal RST = 0;
                             decimal HST = 0;
                             decimal PPT = 0;
                             decimal Total = 0;
                             string DbCon = DBFlag + item.CompanyId;
                             string Constring = GetConnection(DbCon);
                             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                             {
                                 List<TblPayment> PaymentObj = (from p in datacontext.TblPayments
                                                          join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                          where p.Active == true && q.Active == true && p.IsRemit == false 
                                                          
                                                          select p).ToList();
                                 if(PaymentObj != null && PaymentObj.Count() > 0)
                                 {

                                      AdminFee = PaymentObj.Select(x => Convert.ToDecimal(x.AdministrationInitialFee)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.AdministrationMonthlyFee)).Sum();
                                      RST = PaymentObj.Select(x => Convert.ToDecimal(x.RetailSalesTaxInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.RetailSalesTaxMonthly)).Sum(); 
                                      HST = PaymentObj.Select(x=> Convert.ToDecimal(x.HSTInitial)).Sum() + PaymentObj.Select(x=> Convert.ToDecimal(x.HSTMonthly)).Sum(); 
                                      PPT = PaymentObj.Select(x=> Convert.ToDecimal(x.PremiumTaxInitial)).Sum() + PaymentObj.Select(x=> Convert.ToDecimal(x.PremiumTaxMonthly)).Sum();
                                      Total = PaymentObj.Select(x => Convert.ToDecimal(x.GrossTotalInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.GrossTotalMonthly)).Sum();
                                      Obj.Add(new ListTaxPot()
                                      {
                                          CompanyId = item.CompanyId,
                                          CompanyName = item.CompanyName,
                                          AdminFee = AdminFee,
                                          RST = RST,
                                          HST = HST,
                                          PPT = PPT,
                                          Total = Total
                                      });
                                 }
                                 datacontext.Connection.Close();
                             }
                         }

                         lobjResponse.ListTaxPotData = Obj;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                         lobjResponse.Message = "No record Found.";
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                 
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, -1, "ListTaxPot", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region ListRemittedTaxPot
         public CListTaxPotResponse ListRemittedTaxPot(IdInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListTaxPotResponse lobjResponse = new CListTaxPotResponse();
             UserId = iObj.Id;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     List<ListTaxPot> Obj = new List<ListTaxPot>();
                     List<TblCompany> CompList = (from p in mastercontext.TblCompanies where p.Active == true && p.IsBlocked == false select p).ToList();
                     if (CompList != null && CompList.Count() > 0)
                     {
                         foreach (TblCompany item in CompList)
                         {
                             decimal AdminFee =0;
                             decimal RST =0; 
                             decimal HST =0;
                             decimal PPT =0;
                             decimal Total = 0;
                             string DbCon = DBFlag + item.CompanyId;
                             string Constring = GetConnection(DbCon);
                             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                             {
                                 List<TblPayment> PaymentObj = (from p in datacontext.TblPayments
                                                                join q in datacontext.TblEmployees on p.CompanyUserId equals q.CompanyUserId
                                                                where p.Active == true && q.Active == true && p.IsRemit == true 
                                                                
                                                                select p).ToList();
                                 if (PaymentObj != null && PaymentObj.Count() > 0)
                                 {

                                     AdminFee = PaymentObj.Select(x => Convert.ToDecimal(x.AdministrationInitialFee)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.AdministrationMonthlyFee)).Sum();
                                     RST = PaymentObj.Select(x => Convert.ToDecimal(x.RetailSalesTaxInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.RetailSalesTaxMonthly)).Sum();
                                     HST = PaymentObj.Select(x => Convert.ToDecimal(x.HSTInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.HSTMonthly)).Sum();
                                     PPT = PaymentObj.Select(x => Convert.ToDecimal(x.PremiumTaxInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.PremiumTaxMonthly)).Sum();
                                     Total = PaymentObj.Select(x => Convert.ToDecimal(x.GrossTotalInitial)).Sum() + PaymentObj.Select(x => Convert.ToDecimal(x.GrossTotalMonthly)).Sum();
                                     Obj.Add(new ListTaxPot()
                                     {
                                         CompanyId = item.CompanyId,
                                         CompanyName = item.CompanyName,
                                         AdminFee = AdminFee,
                                         RST = RST,
                                         HST = HST,
                                         PPT = PPT,
                                         Total = Total,
                                         RemitDate = PaymentObj.Select(x=>Convert.ToString(x.RemitDate)).Take(1).SingleOrDefault(),
                                     });
                                 }
                                 datacontext.Connection.Close();
                             }
                         }

                         lobjResponse.ListTaxPotData = Obj;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                         lobjResponse.Message = "No record Found.";
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                    
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, UserId, "ListRemittedTaxPot", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region RemitTax
         public CResponse RemitTax(IdInputv2 iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                    TblCompany CompList = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id && p.IsBlocked == false select p).Take(1).SingleOrDefault();
                     if (CompList != null )
                     {
                             string DbCon = DBFlag + iObj.Id;
                             string Constring = GetConnection(DbCon);
                             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                             {
                                 List<TblPayment> PaymentObj = (from p in datacontext.TblPayments
                                                                where p.Active == true && p.IsRemit == false
                                                                
                                                                select p).ToList();
                                 if (PaymentObj != null && PaymentObj.Count() > 0)
                                 {
                                     foreach (TblPayment item in PaymentObj)
                                     {
                                         item.IsRemit = true;
                                         item.RemitDate = DateTime.UtcNow;
                                         datacontext.SubmitChanges();
                                     }
                                    
                                 }

                                 datacontext.Connection.Close();
                         }

                        lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         lobjResponse.Message ="No record found.";

                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, UserId, "RemitTax", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus 

   

        #region RollOverPot
         public CListRollOverPotResponse RollOverPot(IdInput iObj)
         {  
             string InputJson = "";
             DateTime CurrentTime = DateTime.UtcNow;
             UserId = iObj.Id;
             bool IsLogOut = false;
             CListRollOverPotResponse lobjResponse = new CListRollOverPotResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {

                     if (IsValidateEflexUser(datacontext))
                     {
                         List<ListRollOverPot> MastCompObjList = new List<ListRollOverPot>();
                         List<TblCompany> CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.IsBlocked == false select p).ToList();
                         if (CompanyObj != null && CompanyObj.Count() > 0)
                         {
                             foreach (TblCompany CompItem in CompanyObj)
                             {
                                 decimal CompanyRollOverCount =(from q in datacontext.TblMasterCompCredits where q.Active == true && q.CompanyId == CompItem.CompanyId select q).Count();
                                 if(CompanyRollOverCount > 0)
                                 {

                                     List<ListRollOverPot> CompData = (from p in datacontext.TblCompanies
                                                                       where p.Active == true && p.CompanyId == CompItem.CompanyId
                                                                       select new ListRollOverPot()
                                                                               {
                                                                                   CompanyName = CompItem.CompanyName,
                                                                                   TotalCredits = (from q in datacontext.TblMasterCompCredits where p.Active == true && q.CompanyId == CompItem.CompanyId select Convert.ToDecimal(q.Credits ?? 0)).Sum(),
                                                                                   EmployeeCredits = GetEmployeeCredits(CompItem.CompanyId),
                                                                                  }).ToList();
                                     MastCompObjList.AddRange(CompData);
                                 }
                                                                 
                                 
                             }
                         }
                         lobjResponse.ListRollOverPot = MastCompObjList;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                      
                     }
                 }
                 catch (Exception ex)
                 {

                     InputJson = new JavaScriptSerializer().Serialize(iObj);
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.ToString();
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(datacontext, CurrentTime, UserId, "RollOverPot", InputJson, lobjResponse.Message);
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }

         private List<EmployeeCredits> GetEmployeeCredits(decimal CompanyId)
         {
             List<EmployeeCredits> Obj = new List<EmployeeCredits>();
             string DbCon = DBFlag + CompanyId;
             string Constring = GetConnection(DbCon);
             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
             {
                 try
                 {

                     Obj = (from p in datacontext.TblCompanyUsers
                            join r in datacontext.TblEmployees on p.CompanyUserId equals r.CompanyUserId
                            join q in datacontext.TblEmpBalances on p.CompanyUserId equals q.CompanyUserId
                            where p.Active == true && q.Active == true && r.IsActive==false && r.InActiveDate <= DateTime.UtcNow
                            select new EmployeeCredits()
                            {
                                EmployeeName = p.FirstName + ((p.LastName == null || p.LastName == "") ? "" : " " + p.LastName),
                                InActiveDate = p.InActiveDate == null ? "" : Convert.ToString(p.InActiveDate),
                                TotalBalance = Convert.ToDecimal(q.Amount ?? 0),
                            }).ToList();
                 }
                 catch (Exception)
                 {

                     throw;
                 }
                 datacontext.Connection.Close();
             }
             return Obj;
         }


         #endregion

        #region ChangePassword
         //JK:270518 
         public CResponse ChangePasswordEflexUser(ChangePasswordEflexUserInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = "";
             decimal UserId = iObj.UserId;
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {
                     TblUser UserObj = (from p in datacontext.TblUsers where p.Active == true && p.UserId == iObj.UserId && p.Pwd == CryptorEngine.Encrypt(iObj.PrevPassword) select p).FirstOrDefault();
                     if (UserObj != null)
                     {
                         string EncryptPwd = CryptorEngine.Encrypt(iObj.NewPassword);
                         UserObj.Pwd = EncryptPwd;
                         UserObj.DateCreated = DateTime.UtcNow;
                         datacontext.SubmitChanges();
                         lobjResponse.Message = "SUCCESS";
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     else
                     {
                         lobjResponse.Message = "Invalid Old Password.";
                         lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                     }
                 }
                 catch (Exception ex)
                 {
                     InputJson = new JavaScriptSerializer().Serialize(iObj);
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }
                 CreateMasterResponseLog(datacontext, StartTime, UserId, "ChangePasswordEflexUser", InputJson, lobjResponse.Message);
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }

         #endregion ChangePassword

        #region ResetPassword
         public CResponse ResetBrokerPassword(ResetBrokerPasswordInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(datacontext))
                 {
                    
                         try
                         {
                             TblUser UserObj = (from p in datacontext.TblUsers
                                                           where p.Active == true &&
                                                               p.UserId == iObj.UserId
                                                           select p).Take(1).SingleOrDefault();
                             if (UserObj != null)
                             {
                                 string Password = RandomString();
                                 if (UserObj.UserTypeId == Convert.ToDecimal(CObjects.enumMasterUserType.Broker))
                                 {
                                     UserObj.Pwd = RandomString();
                                     datacontext.SubmitChanges();

                                     #region SendMail

                                     string lstrFName = UserObj.FirstName + ((UserObj.LastName == null || UserObj.LastName == "") ? "" : " " + UserObj.LastName);
                                     
                                     StringBuilder builder = new StringBuilder();
                                     builder.Append("Hello " + lstrFName + ",<br/><br/>");
                                     builder.Append("Your new password for eFlex account has been reset as: " + CryptorEngine.Decrypt(UserObj.Pwd) + "<br/><br/>");
                                     builder.Append("Click here for login: <a href=" + lstrUrl + " >" + lstrUrl + "</a><br/><br/>");
                                     builder.Append("Regards,<br/><br/>");
                                     builder.Append("Eflex Team<br/><br/>");

                                     
                                     //send mail
                                     string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                     lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                     string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                     string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                     string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                     Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                     CMail Mail = new CMail();
                                     Mail.EmailTo = UserObj.EmailAddress;
                                     Mail.Subject = "eFlex Spending Account Password Reset";
                                     Mail.MessageBody = lstrMessage;
                                     Mail.GodaddyUserName = GodaddyUserName;
                                     Mail.GodaddyPassword = GodaddyPwd;
                                     Mail.Server = Server;
                                     Mail.Port = Port;
                                     Mail.CompanyId = -1;
                                     bool IsSent = Mail.SendEMail(datacontext, true);


                                     #endregion SendMail

                                 }
                                  
                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                             }
                             else
                             {
                                 lobjResponse.Message = "No record fopund.";
                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             }

                         }
                         catch (Exception ex)
                         {

                             lobjResponse.Message = ex.Message;
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         }
                    
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                    
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(datacontext, StartTime, -1, "ResetBrokerPassword", InputJson, lobjResponse.Message);
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region ListOfBrokerCompaniesForAdmin
         public CListOfBrokerCompaniesResponse ListOfBrokerCompaniesForAdmin(ListOfBrokerCompaniesForAdminInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListOfBrokerCompaniesResponse lobjResponse = new CListOfBrokerCompaniesResponse();
             UserId = iObj.LoggedInUserId;
              
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 List<ListOfBrokerCompanies> Result = new List<ListOfBrokerCompanies>();
                 if (IsValidateEflexUser(mastercontext))
                 {
                     try
                     {
                         List<ListOfBrokerCompanies> Obj = (from p in mastercontext.TblCompanies.AsEnumerable()
                                                            join q in mastercontext.TblBrokerCompanies on p.CompanyId equals q.CompanyId
                                                            join r in mastercontext.TblBrokers on q.BrokerId equals r.BrokerId
                                                            join x in mastercontext.TblPlanTypes on Convert.ToDecimal(p.PlanTypeId) equals x.PlanTypeId
                                                            join s in mastercontext.TblProvinces on p.Province equals s.ProvinceId
                                                            where p.Active == true && q.Active == true && r.Active == true && s.Active==true && r.Active == true
                                                            && r.UserId == iObj.UserId
                                                            select new ListOfBrokerCompanies()
                                                            {
                                                                CompanyId = Convert.ToDecimal(p.CompanyId),
                                                                Name = p.CompanyName,
                                                                Code = p.UniqueCode,
                                                                Email = p.OwnerEmail,
                                                                Phone = p.OwnerContactNo,
                                                                OwnerName = p.FirstName + " " + p.LastName,
                                                                Province = s.Name,
                                                                PlanType = x.Title,
                                                                DbCon = p.DBCon,
                                                                OwnerId = 1,
                                                                CommissionCharges = (from a in mastercontext.TblBrokerCompanyFees 
                                                                                     where a.Active == true && a.BrokerId == r.BrokerId && a.CompanyId == p.CompanyId
                                                                                         select Convert.ToDecimal(a.BrokerFee)).Take(1).SingleOrDefault(),
                                                                CreationDt = Convert.ToString(p.DateCreated),
                                                                IsApproved = Convert.ToString(p.IsApproved),
                                                                BlockReason = (from a in mastercontext.TblCompBlockReasons
                                                                               where a.Active == true && a.CompanyId == p.CompanyId
                                                                               select new BlockReason()
                                                                               {
                                                                                   Reason = a.Reason,
                                                                                   Date = String.Format("{0:MM/dd/yyyy}", a.DateCreated),
                                                                                   IsBlock = Convert.ToBoolean(a.IsBlock)
                                                                               }).ToList(),
                                                                IsBlocked = Convert.ToBoolean(p.IsBlocked)
                                                            }).ToList();

                         if (Obj != null && Obj.Count > 0)
                         {
                             if (iObj.ChunckStart == -1)
                             {
                                 Result = Obj.OrderByDescending(i => i.CompanyId).Take(iObj.ChunckSize).ToList();
                             }
                             else
                             {
                                 Result = Obj.OrderByDescending(i => i.CompanyId).Where(i => i.CompanyId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                             }
                         }
                        
                        
                         lobjResponse.ListOfBrokerCompanies = Result;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                     catch (Exception ex)
                     {

                         lobjResponse.Message = ex.Message;
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                   
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfBrokerCompaniesForAdmin", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region GetPlanDesignDetails
         public CGetPlanDesignDetailsResponse GetPlanDesignDetails(GetPlanDesignDetailsInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             CGetPlanDesignDetailsResponse lobjResponse = new CGetPlanDesignDetailsResponse();
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
            using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                    string DbCon = DBFlag + iObj.CompanyId;
                     string Constring = GetConnection(DbCon);
                     using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                     {
                         try
                         {
                             TblCompanyPlan PlanObj = (from p in datacontext.TblCompanyPlans where p.Active == true && p.CompanyPlanId == iObj.PlanId select p).Take(1).SingleOrDefault();
                             if(PlanObj != null)
                             {
                                 PlanDetails1 Obj = (from p in datacontext.TblCompanyPlans
                                                    where p.Active == true && p.CompanyPlanId == iObj.PlanId
                                                    select new PlanDetails1()
                                                    {
                                                            PlanStartDt = Convert.ToString(p.PlanStartDt),
                                                            Name = p.Name,
                                                            ContributionSchedule = Convert.ToInt32(p.ContributionSchedule),
                                                            RecurringDeposit = Convert.ToDecimal(p.RecurringDeposit),
                                                            InitialDeposit = Convert.ToDecimal(p.InitialDeposit),
                                                            NetDeposit = Convert.ToDecimal(p.NetDeposit),
                                                            AdministrationInitialFee = Convert.ToDecimal(p.AdministrationInitialFee),
                                                            AdministrationMonthlyFee = Convert.ToDecimal(p.AdministrationMonthlyFee),
                                                            HSTInitial = Convert.ToDecimal(p.HSTInitial),
                                                            HSTMonthly = Convert.ToDecimal(p.HSTMonthly),
                                                            PremiumTaxInitial = Convert.ToDecimal(p.PremiumTaxInitial),
                                                            PremiumTaxMonthly = Convert.ToDecimal(p.PremiumTaxMonthly),
                                                            RetailSalesTaxInitial = Convert.ToDecimal(p.RetailSalesTaxInitial),
                                                            RetailSalesTaxMonthly = Convert.ToDecimal(p.RetailSalesTaxMonthly),
                                                            GrossTotalInitial = Convert.ToDecimal(p.GrossTotalInitial),
                                                            GrossTotalMonthly = Convert.ToDecimal(p.GrossTotalMonthly),
                                                            PlanDesign = (from a in datacontext.TblPlanDesigns where a.Active == true && a.PlanDesignId == p.PlanDesign select a.Title).Take(1).SingleOrDefault(),
                                                            RollOver = (from a in datacontext.TblPlanDesigns where a.Active == true && a.PlanDesignId == p.RollOver select a.Title).Take(1).SingleOrDefault(),
                                                            CarryForwardPeriod = p.CarryForwardPeriod,
                                                            Termination =(from a in datacontext.TblPlanDesigns where a.Active == true && a.PlanDesignId == p.Termination select a.Title).Take(1).SingleOrDefault()
                                                    }).Take(1).SingleOrDefault();



                                 lobjResponse.PlanDesign = Obj;
                                 lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                             }
                             else
                             {
                                 lobjResponse.Message = "No record Found.";
                                 lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                             }
                          
                         }
                         catch (Exception ex)
                         {
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                             lobjResponse.Message = ex.Message;
                         }

                         datacontext.Connection.Close();
                     }
                     CreateMasterResponseLog(mastercontext, StartTime, UserId, "GetPlanDesignDetails", InputJson, lobjResponse.Message);
                     mastercontext.Connection.Close();
             }

             return lobjResponse;
         }
         #endregion

        #region AddCardFeeRange
         public CResponse AddCardFeeRange(AddCardFeeRangeInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             CResponse lobjResponse = new CResponse();
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             UserId = iObj.UserId;
             bool IsLogOut = false;
             using (MasterDBDataContext masterContext = new MasterDBDataContext())
             {
                 try
                 {
                     if (IsValidateEflexUser(masterContext))
                     {

                            TblCardFeeRange Obj = new TblCardFeeRange()
                             {
                                MaxValue = iObj.MaxValue,
                                MinValue = iObj.MinValue,
                                DayMax = iObj.DayMax,
                                WeeklyMax = iObj.WeeklyMax,
                                MonthlyMax = iObj.MonthlyMax,
                                OverAllMax = iObj.OverAllMax,
	                            UserId = iObj.UserId,
                                IsApplicable = iObj.IsApplicable,
                                ContributionTypeId = iObj.ContributionType,
	                            Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                             };
                             masterContext.TblCardFeeRanges.InsertOnSubmit(Obj);
                             masterContext.SubmitChanges();

                            
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                             lobjResponse.Message = "Card range added successfully";
                     
                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                       
                     }
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(masterContext, StartTime, UserId, "AddCardFeeRange", InputJson, lobjResponse.Message);
                 masterContext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region ListCardFeeRange
         public CListCardFeeRangeResponse ListCardFeeRange(IdInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListCardFeeRangeResponse lobjResponse = new CListCardFeeRangeResponse();
             UserId = iObj.Id;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     try
                     {
                         List<ListCardFeeRange> Obj = (from p in mastercontext.TblCardFeeRanges
                                                         where p.Active == true
                                                         select new ListCardFeeRange()
                                                         {
                                                             CardFeeRangeId = Convert.ToDecimal(p.CardFeeRangeId),
                                                             MaxValue = Convert.ToDecimal(p.MaxValue),
                                                             MinValue = Convert.ToDecimal(p.MinValue),
                                                             DayMax = Convert.ToDecimal(p.DayMax),
                                                             WeeklyMax = Convert.ToDecimal(p.WeeklyMax),
                                                             MonthlyMax = Convert.ToDecimal(p.MonthlyMax),
                                                             OverAllMax = Convert.ToDecimal(p.OverAllMax),
                                                             UserId = Convert.ToDecimal(p.UserId),
                                                             IsApplicable = Convert.ToString(p.IsApplicable),
                                                             ContributionType = Convert.ToDecimal(p.ContributionTypeId),
                                                         }).ToList();


                         lobjResponse.ListCardFeeRange = Obj;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                     

                     }
                     catch (Exception ex)
                     {

                         lobjResponse.Message = ex.Message;
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                   
                 }

                 lobjResponse.IsLogOut = IsLogOut;
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region EditCardFeeRange
         public CResponse EditCardFeeRange(EditCardFeeRangeInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             CResponse lobjResponse = new CResponse();
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             UserId = iObj.UserId;
             bool IsLogOut = false;
             using (MasterDBDataContext masterContext = new MasterDBDataContext())
             {
                 try
                 {
                     if (IsValidateEflexUser(masterContext))
                     {
                         TblCardFeeRange Obj = (from p in masterContext.TblCardFeeRanges where p.CardFeeRangeId == iObj.CardFeeRangeId && p.Active == true select p).Take(1).SingleOrDefault();
                         if(Obj != null)
                         {
                             Obj.MaxValue = iObj.MaxValue;
                             Obj.MinValue = iObj.MinValue;
                             Obj.DayMax = iObj.DayMax;
                             Obj.WeeklyMax = iObj.WeeklyMax;
                             Obj.MonthlyMax = iObj.MonthlyMax;
                             Obj.OverAllMax = iObj.OverAllMax;
                             Obj.IsApplicable = iObj.IsApplicable;
                             Obj.ContributionTypeId = iObj.ContributionType;
                             Obj.DateModified = DateTime.UtcNow;
                         }
                         masterContext.SubmitChanges();
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         lobjResponse.Message = "Card range added successfully";

                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                        

                     }
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }
                 CreateMasterResponseLog(masterContext, StartTime, UserId, "EditCardFeeRange", InputJson, lobjResponse.Message);
                 lobjResponse.IsLogOut = IsLogOut;
                 masterContext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region DeleteCardFeeRange
         public CResponse DeleteCardFeeRange(IdInputv2 iObj)
         {
             CResponse lobjResponse = new CResponse();
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext masterContext = new MasterDBDataContext())
             {
                 try
                 {
                     if (IsValidateEflexUser(masterContext))
                     {
                         TblCardFeeRange Obj = (from p in masterContext.TblCardFeeRanges where p.CardFeeRangeId == iObj.Id && p.Active == true select p).Take(1).SingleOrDefault();
                         if (Obj != null)
                         {
                             Obj.Active = false;
                             Obj.DateModified = DateTime.UtcNow;
                             masterContext.SubmitChanges();
                         }
                         
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         lobjResponse.Message = "Card range added successfully";

                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;
                        
                     }
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }
                 CreateMasterResponseLog(masterContext, StartTime, UserId, "DeleteCardFeeRange", InputJson, lobjResponse.Message);
                 lobjResponse.IsLogOut = IsLogOut;
                 masterContext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion

        #region ListMasterTransactions
      
         public CListMasterTransactionsResponse ListMasterTransactions(ListMasterTransactionsInput iObj)
         {
             string InputJson = "";
             DateTime CurrentTime = DateTime.UtcNow;
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             CListMasterTransactionsResponse lobjResponse = new CListMasterTransactionsResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {

                     if (IsValidateEflexUser(datacontext))
                     {
                         #region FilterAll
                       
                         List<MasterCompDetails> MastCompObjList = new List<MasterCompDetails>();
                         if (iObj.CompanyId == -1)
                         {
                             List<TblCompany> CompanyObj = (from p in datacontext.TblCompanies where p.Active == true && p.IsBlocked == false select p).ToList();
                             if (CompanyObj != null && CompanyObj.Count() > 0)
                             {
                                 foreach (TblCompany CompItem in CompanyObj)
                                 {
                                     #region MyRegion
                                     if (iObj.FilterType.ToUpper() == "ADJ" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region ADJ
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && p.CompanyId == CompItem.CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                                       select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompItem.CompanyName,
                                                                                           Id = p.CompAdjustmentId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus = Convert.ToDecimal(p.PaymentStatus)                                                                                           
                                                                                       }).Distinct().ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompItem.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.ADJ.ToString() || x.FilterType == CObjects.enumPaymentType.WDR.ToString())).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if(EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         else
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && (p.AdjustmentDT.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date 
                                                                                       && p.AdjustmentDT.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                                       && p.CompanyId == CompItem.CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompItem.CompanyName,
                                                                                           Id = p.CompAdjustmentId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus =  Convert.ToDecimal(p.PaymentStatus)
                                                                                           
                                                                                       }).Distinct().ToList();

                                               List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompItem.CompanyId, iObj.SearchString);
                                               List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.ADJ.ToString() || x.FilterType == CObjects.enumPaymentType.WDR.ToString()) && ((Convert.ToDateTime(x.AdjustmentDT).Date > Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && Convert.ToDateTime(x.AdjustmentDT).Date < Convert.ToDateTime(iObj.EndDT).Date))).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                               if (EmpTranData != null)
                                               {
                                                   CompTranData.AddRange(EmpTranData);
                                               }


                                               MastCompObjList.AddRange(CompTranData);
                                         }
                                         
                                         #endregion

                                     }
                                     if (iObj.FilterType.ToUpper() == "FEE" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region FEE
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                     where p.Active == true &&
                                                                                     p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                     && p.CompanyId == CompItem.CompanyId &&
                                                                                     (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                                                                     p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() ||
                                                                                     p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                                                                     select new MasterCompDetails()
                                                                                     {
                                                                                         CompanyName = CompItem.CompanyName,
                                                                                         Id = CompItem.CompanyId,
                                                                                         Amount = Convert.ToDecimal(p.Amount),
                                                                                         AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                         Remarks = p.Remarks,
                                                                                         Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                         FilterType = p.PaymentTypeId,
                                                                                         PaymentStatus =  Convert.ToDecimal(p.PaymentStatus)
                                                                                         
                                                                                     }).Distinct().ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompItem.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.IN.ToString() || x.FilterType == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       x.FilterType == CObjects.enumPaymentType.DM.ToString())).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         else
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && p.CompanyId == CompItem.CompanyId &&
                                                                                       (p.AdjustmentDT.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && p.AdjustmentDT.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date) &&
                                                                                       (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompItem.CompanyName,
                                                                                           Id = CompItem.CompanyId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus = Convert.ToDecimal(p.PaymentStatus)
                                                                                           
                                                                                       }).Distinct().ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompItem.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.IN.ToString() || x.FilterType == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       x.FilterType == CObjects.enumPaymentType.DM.ToString()) && ((Convert.ToDateTime(x.AdjustmentDT).Date > Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && Convert.ToDateTime(x.AdjustmentDT).Date < Convert.ToDateTime(iObj.EndDT).Date))).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         
                                         #endregion

                                     }
                                     if (iObj.FilterType.ToUpper() == "CREDIT" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region CREDIT
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> EmpTranData = new List<MasterCompDetails>();
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblMasterCompCredits
                                                                                       where p.Active == true &&
                                                                                       p.CompanyId == CompItem.CompanyId
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompItem.CompanyName,
                                                                                           Id = p.MasterCompCreditsId,
                                                                                           Amount = Convert.ToDecimal(p.Credits),
                                                                                           AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                                           Remarks = "Amount credited",
                                                                                           Type = "Credit",
                                                                                           FilterType = "CREDIT",
                                                                                           PaymentStatus = 3
                                                                                           
                                                                                       }).Distinct().ToList();
                                             string DbCon = DBFlag + CompItem.CompanyId;
                                             string Constring = GetConnection(DbCon);
                                             using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                             {
                                                 EmpTranData = (from p in dc.TblCompanyCredits
                                                                join q in dc.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                                where p.Active == true && q.Active == true
                                                                
                                                                select new MasterCompDetails()
                                                                {
                                                                    Id=p.CompanyCreditId,
                                                                    EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                                    Amount = Convert.ToDecimal(p.Credits),
                                                                    AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                    Remarks = p.Credits + " has been credited from " + q.FirstName + "'s account.",
                                                                    Type = "Credit",
                                                                    FilterType = "CREDIT",
                                                                    PaymentStatus = 3
                                                                }).Distinct().ToList();
                                                 dc.Connection.Close();
                                             }
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         else{
                                             List<MasterCompDetails> EmpTranData = new List<MasterCompDetails>();
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblMasterCompCredits
                                                                                       where p.Active == true &&
                                                                                       (p.DateCreated.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && p.DateCreated.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                                       && p.CompanyId == CompItem.CompanyId
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompItem.CompanyName,
                                                                                           Id = p.MasterCompCreditsId,
                                                                                           Amount = Convert.ToDecimal(p.Credits),
                                                                                           AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                                           Remarks = "Amount credited",
                                                                                           Type = "Credit",
                                                                                           FilterType = "CREDIT",
                                                                                           PaymentStatus = 3
                                                                                           
                                                                                       }).Distinct().ToList();
                                             string DbCon = DBFlag + CompItem.CompanyId;
                                             string Constring = GetConnection(DbCon);
                                             using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                             {
                                                 EmpTranData = (from p in dc.TblCompanyCredits
                                                                join q in dc.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                                where p.Active == true && q.Active == true
                                                                && (p.DateCreated.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date
                                                                && p.DateCreated.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                
                                                                select new MasterCompDetails()
                                                                {
                                                                    Id = p.CompanyCreditId,
                                                                    EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                                    Amount = Convert.ToDecimal(p.Credits),
                                                                    AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                    Remarks = p.Credits + " has been credited from " + q.FirstName + "'s account.",
                                                                    Type = "Credit",
                                                                    FilterType = "CREDIT",
                                                                    PaymentStatus = 3
                                                                }).Distinct().ToList();
                                                 dc.Connection.Close();
                                             }
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }

                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         
                                         #endregion CREDIT

                                     }
                                     #endregion

                                 }
                             }
                         }
                         else
                         {

                             #region particular company


                             TblCompany CompanyObj1 = (from p in datacontext.TblCompanies where p.Active == true && p.CompanyId == iObj.CompanyId select p).Take(1).SingleOrDefault();
                             if (CompanyObj1 != null)
                             {
                                  #region MyRegion
                                     if (iObj.FilterType.ToUpper() == "ADJ" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region ADJ
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                        where p.Active == true &&
                                                                                        p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                        && p.CompanyId == CompanyObj1.CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                                        select new MasterCompDetails()
                                                                                           {
                                                                                               CompanyName = CompanyObj1.CompanyName,
                                                                                             
                                                                                               Id = p.CompAdjustmentId,
                                                                                               Amount = Convert.ToDecimal(p.Amount),
                                                                                               AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                               Remarks = p.Remarks,
                                                                                               Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                               FilterType = p.PaymentTypeId,
                                                                                               PaymentStatus = Convert.ToDecimal(p.PaymentStatus)
                                                                                             }).Distinct().ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompanyObj1.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => x.FilterType == CObjects.enumPaymentType.ADJ.ToString()).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         else
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && (p.AdjustmentDT.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date 
                                                                                       && p.AdjustmentDT.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                                       && p.CompanyId == CompanyObj1.CompanyId && p.PaymentTypeId == CObjects.enumPaymentType.ADJ.ToString()
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompanyObj1.CompanyName,
                                                                                           Id = p.CompAdjustmentId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus = 3
                                                                                          }).ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompanyObj1.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => x.FilterType == CObjects.enumPaymentType.ADJ.ToString() && ((Convert.ToDateTime(x.AdjustmentDT).Date > Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && Convert.ToDateTime(x.AdjustmentDT).Date < Convert.ToDateTime(iObj.EndDT).Date))).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                        
                                         #endregion

                                     }
                                     if (iObj.FilterType.ToUpper() == "FEE" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region FEE
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && p.CompanyId == CompanyObj1.CompanyId &&
                                                                                       (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompanyObj1.CompanyName,
                                                                                           Id = p.CompAdjustmentId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus = Convert.ToDecimal(p.PaymentStatus)
                                                                                       }).ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompanyObj1.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.IN.ToString() || x.FilterType == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       x.FilterType == CObjects.enumPaymentType.DM.ToString())).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                          }
                                         else
                                         {
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblCompAdjustments
                                                                                       where p.Active == true &&
                                                                                       p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited)
                                                                                       && p.CompanyId == CompanyObj1.CompanyId &&
                                                                                       (p.AdjustmentDT.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date 
                                                                                       && p.AdjustmentDT.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                                       &&
                                                                                       (p.PaymentTypeId == CObjects.enumPaymentType.IN.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       p.PaymentTypeId == CObjects.enumPaymentType.DM.ToString())
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompanyObj1.CompanyName,
                                                                                           Id = p.CompAdjustmentId,
                                                                                           Amount = Convert.ToDecimal(p.Amount),
                                                                                           AdjustmentDT = Convert.ToString(p.AdjustmentDT),
                                                                                           Remarks = p.Remarks,
                                                                                           Type = (p.TypeId == 1) ? "Credit" : "Debit",
                                                                                           FilterType = p.PaymentTypeId,
                                                                                           PaymentStatus = Convert.ToDecimal(p.PaymentStatus)
                                                                                        }).ToList();

                                             List<MasterCompDetails> EmpTranData1 = GetCompEmployeeTransactions(CompanyObj1.CompanyId, iObj.SearchString);
                                             List<MasterCompDetails> EmpTranData = EmpTranData1.Where(x => (x.FilterType == CObjects.enumPaymentType.IN.ToString() || x.FilterType == CObjects.enumPaymentType.DA.ToString() ||
                                                                                       x.FilterType == CObjects.enumPaymentType.DM.ToString()) && ((Convert.ToDateTime(x.AdjustmentDT).Date > Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && Convert.ToDateTime(x.AdjustmentDT).Date < Convert.ToDateTime(iObj.EndDT).Date))).Distinct().GroupBy(x => x.Id).Select(y => y.First()).ToList();
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         
                                         #endregion

                                     }
                                     if (iObj.FilterType.ToUpper() == "CREDIT" || iObj.FilterType.ToUpper() == "ALL")
                                     {
                                         #region CREDIT
                                         if (string.IsNullOrEmpty(iObj.FromDT) && string.IsNullOrEmpty(iObj.EndDT))
                                         {
                                             List<MasterCompDetails> EmpTranData = new List<MasterCompDetails>();
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblMasterCompCredits
                                                                                       where p.Active == true &&
                                                                                       p.CompanyId == CompanyObj1.CompanyId
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompanyObj1.CompanyName,
                                                                                           Id = p.MasterCompCreditsId,
                                                                                           Amount = Convert.ToDecimal(p.Credits),
                                                                                           AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                                           Remarks = "Amount credited",
                                                                                           Type = "Credit",
                                                                                           FilterType = "CREDIT",
                                                                                           PaymentStatus = 3
                                                                                          }).Distinct().ToList();
                                             string DbCon = DBFlag + CompanyObj1.CompanyId;
                                             string Constring = GetConnection(DbCon);
                                             using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                             {
                                                 EmpTranData = (from p in dc.TblCompanyCredits
                                                                join q in dc.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                                where p.Active == true && q.Active == true
                                                                
                                                                select new MasterCompDetails()
                                                                {
                                                                    CompanyName = CompanyObj1.CompanyName,
                                                                    Id = p.CompanyCreditId,
                                                                    EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                                    Amount = Convert.ToDecimal(p.Credits),
                                                                    AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                    Remarks = p.Credits + " has been credited from " + q.FirstName + "'s account.",
                                                                    Type = "Credit",
                                                                    FilterType = "CREDIT",
                                                                    PaymentStatus = 3
                                                                }).Distinct().ToList();
                                                 dc.Connection.Close();
                                             }
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         else{
                                             List<MasterCompDetails> EmpTranData = new List<MasterCompDetails>();
                                             List<MasterCompDetails> CompTranData = (from p in datacontext.TblMasterCompCredits
                                                                                       where p.Active == true &&
                                                                                       (p.DateCreated.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date
                                                                                       && p.DateCreated.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                                                       &&
                                                                                       p.CompanyId == CompanyObj1.CompanyId
                                                                                     select new MasterCompDetails()
                                                                                       {
                                                                                           CompanyName = CompanyObj1.CompanyName,
                                                                                           Id = p.MasterCompCreditsId,
                                                                                           Amount = Convert.ToDecimal(p.Credits),
                                                                                           AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                                           Remarks = "Amount credited",
                                                                                           Type = "Credit",
                                                                                           FilterType = "CREDIT",
                                                                                           PaymentStatus = 3
                                                                                        }).Distinct().ToList();
                                             string DbCon = DBFlag + CompanyObj1.CompanyId;
                                             string Constring = GetConnection(DbCon);
                                             using (EflexDBDataContext dc = new EflexDBDataContext(Constring))
                                             {
                                                 EmpTranData = (from p in dc.TblCompanyCredits
                                                                join q in dc.TblCompanyUsers on p.CompanyUserId equals q.CompanyUserId
                                                                where p.Active == true && q.Active == true
                                                                && (p.DateCreated.Value.Date >= Convert.ToDateTime(iObj.FromDT).Date
                                                                && p.DateCreated.Value.Date <= Convert.ToDateTime(iObj.EndDT).Date)
                                                              select new MasterCompDetails()
                                                                {
                                                                    CompanyName = CompanyObj1.CompanyName,
                                                                    Id = p.CompanyCreditId,
                                                                    EmployeeName = q.FirstName + ((q.LastName == null || q.LastName == "") ? "" : " " + q.LastName),
                                                                    Amount = Convert.ToDecimal(p.Credits),
                                                                    AdjustmentDT = Convert.ToString(p.DateCreated),
                                                                    Remarks = p.Credits + " has been credited from " + q.FirstName + "'s account.",
                                                                    Type = "Credit",
                                                                    FilterType = "CREDIT",
                                                                    PaymentStatus = 3
                                                                }).Distinct().ToList();
                                                 dc.Connection.Close();
                                             }
                                             if (EmpTranData != null)
                                             {
                                                 CompTranData.AddRange(EmpTranData);
                                             }


                                             MastCompObjList.AddRange(CompTranData);
                                         }
                                         
                                         #endregion CREDIT

                                     }
                                     #endregion
                             }
                             #endregion
                         }


                         List<MasterCompDetails> Obj1 = new List<MasterCompDetails>();

                         if (iObj.ChunckStart == -1)
                         {
                             List<MasterCompDetails> Obj = MastCompObjList.OrderByDescending(x=>Convert.ToDateTime(x.AdjustmentDT)).Distinct().ToList();
                             Obj1 = Obj.Select((o, i) => { o.RowId = i; return o; }).Take(iObj.ChunckSize).ToList();
                         
                         }
                         else
                         {
                             
                             Int32 Skip = iObj.ChunckStart + 1;
                             List<MasterCompDetails> Obj = MastCompObjList.OrderByDescending(x => Convert.ToDateTime(x.AdjustmentDT)).Distinct().ToList();
                             Obj1 = Obj.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(iObj.ChunckSize).ToList();
                       
                         }


                         lobjResponse.MasterCompData = Obj1;
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         #endregion

                     }
                     else
                     {
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         lobjResponse.Message =  CObjects.SessionExpired.ToString();
                         IsLogOut = true;

                     }
                 }
                 catch (Exception ex)
                 {

                     InputJson = new JavaScriptSerializer().Serialize(iObj);
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.ToString();
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(datacontext, CurrentTime, UserId, "ListMasterTransactions", InputJson, lobjResponse.Message);
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }




         #endregion

        #region EncryptPasswordsInCompany
         public CResponse EncryptPasswordsInCompany()
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 try
                 {
                     List<TblCompany> CompanyList = (from p in mastercontext.TblCompanies where p.Active == true select p).ToList();
                     if (CompanyList != null && CompanyList.Count() > 0)
                     {
                         foreach (TblCompany item in CompanyList)
                         {
                             string Constring = GetConnection(item.DBCon);
                             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                             {
                                 List<TblCompanyUser> UserListObj = (from p in datacontext.TblCompanyUsers where p.Active == true select p).ToList();
                                 if (UserListObj != null && UserListObj.Count() > 0)
                                 {
                                     foreach (TblCompanyUser UserItem in UserListObj)
                                     {
                                         UserItem.Pwd = CryptorEngine.Encrypt(UserItem.Pwd);
                                         datacontext.SubmitChanges();
                                     }
                                 }
                                 datacontext.Connection.Close();
                             }
                         }
                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                 }
                 catch (Exception)
                 {

                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }

                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }


         #endregion

        #region EncryptPasswordsInMaster
         public CResponse EncryptPasswordsInMaster()
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 try
                 {
                     List<TblUser> UserList = (from p in mastercontext.TblUsers where p.Active == true select p).ToList();
                     if (UserList != null && UserList.Count() > 0)
                     {
                         foreach (TblUser item in UserList)
                         {

                             item.Pwd = CryptorEngine.Encrypt(item.Pwd);
                             mastercontext.SubmitChanges();

                         }

                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }

                     List<TblCompany> CompanyList = (from p in mastercontext.TblCompanies where  p.Active == true select p).ToList();
                     if (UserList != null && UserList.Count() > 0)
                     {
                         foreach (TblCompany item in CompanyList)
                         {

                             item.Pwd = CryptorEngine.Encrypt(item.Pwd);
                             mastercontext.SubmitChanges();

                         }

                         lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     }
                 }
                 catch (Exception)
                 {

                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }


         #endregion

        #region AdminClaimList
         public CAdminClaimListResponse AdminClaimList1(AdminClaimListInput iObj)
         {
             DateTime CurrentTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CAdminClaimListResponse lobjResponse = new CAdminClaimListResponse();
             string DbCon = "";
             string Constring = "";
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             MasterDBDataContext mastercontext = new MasterDBDataContext();
             if (IsValidateEflexUser(mastercontext))
             {
                 if (iObj.Id == -1)
                 {
                     List<AdminClaimList> ResultObj = new List<AdminClaimList>();
                     decimal? StatusId = null;
                     if (iObj.StatusId != -1)
                     {
                         StatusId = iObj.StatusId;
                     }
                      List<TblCompany> CompList = (from p in mastercontext.TblCompanies where p.Active == true && p.IsBlocked == false select p).ToList();
                      if (CompList != null && CompList.Count() > 0)
                      {
                          foreach (TblCompany item in CompList)
                          {
                              DbCon = DBFlag + item.CompanyId;
                              Constring = GetConnection(DbCon);
                              using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                              {
                                  List<AdminClaimList> Obj = (from p in datacontext.sp_AdminAllClaimList(iObj.SearchString, StatusId, Convert.ToInt32(iObj.Ismanual))
                                                              select new AdminClaimList()
                                                              {
                                                                  EmpClaimId = p.EmpClaimId,
                                                                  Employee = p.Employee,
                                                                  Dependent = p.Dependant,
                                                                  ClaimDt = Convert.ToString(p.ClaimDT),
                                                                  ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                                  Company = item.CompanyName,
                                                                  CompanyCode = item.UniqueCode,
                                                                  Receipt = p.ReceiptNo,
                                                                  ReceiptDT = Convert.ToString(p.ReceiptDate),
                                                                  FileName = p.UploadFileName,
                                                                  Note = p.Note,
                                                                  CompanyId = item.CompanyId,
                                                                  Status = p.ClaimStatus,
                                                                  CRA = Convert.ToDecimal(p.CRA),
                                                                  NonCRA = Convert.ToDecimal(p.NonCRA),
                                                                  IsManual = Convert.ToBoolean(p.IsManual),
                                                                  GetPAPDetails = GetEmpBankDetails(datacontext, p.CompanyUserId),
                                                                  IsRefund = p.ClaimFee > 0 ? true : false,
                                                                  ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                                         join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                                         where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                                         select new ClaimServiceDetails()
                                                                                         {
                                                                                             ServiceName = a.Title,
                                                                                             ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                             Address = q.Address,
                                                                                             PhoneNo = q.ContactNo,
                                                                                             Email = q.Email,
                                                                                             ServiceProvider = q.Name
                                                                                         }).Take(1).SingleOrDefault()
                                                              }).ToList();
                                  if (Obj != null && Obj.Count() > 0)
                                  {
                                      ResultObj.AddRange(Obj);
                                  }
                                  datacontext.Connection.Close();
                              }
                          }
                          List<AdminClaimList> Result = new List<AdminClaimList>();
                          if(iObj.ChunckStart == -1)
                          {
                              Result = ResultObj.Select((o, i) => { o.RowId = i; return o; }).Take(iObj.ChunckSize).ToList();
                          
                          }
                          else
                          {
                           
                              Int32 Skip =iObj.ChunckStart + 1;
                              Result = ResultObj.Select((o, i) => { o.RowId = i; return o; }).Skip(Skip).Take(iObj.ChunckSize).ToList();
                          
                          }
                          lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                          lobjResponse.AdminClaimList = Result;

                      }
                      else
                      {
                          lobjResponse.Status = CObjects.enmStatus.Failure.ToString();

                      }
                 }
                 else
                 {
                     DbCon = DBFlag + iObj.Id;
                     Constring = GetConnection(DbCon);
                     using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                     {
                         try
                         {
                             string CompanyName = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p.CompanyName).Take(1).SingleOrDefault();
                             string CompanyCode = (from p in mastercontext.TblCompanies where p.Active == true && p.CompanyId == iObj.Id select p.UniqueCode).Take(1).SingleOrDefault();
                             decimal? StatusId = null;
                             if (iObj.StatusId != -1)
                             {
                                 StatusId = iObj.StatusId;
                             }
                             List<AdminClaimList> Obj = (from p in datacontext.sp_AdminClaimList(iObj.ChunckStart, iObj.SearchString, Convert.ToInt32(iObj.ChunckSize), StatusId, Convert.ToInt16(iObj.Ismanual))
                                                         select new AdminClaimList()
                                                         {
                                                             RowId = Convert.ToInt32(p.RowId),
                                                             EmpClaimId = p.EmpClaimId,
                                                             Employee = p.Employee,
                                                             Dependent = p.Dependant,
                                                             ClaimDt = Convert.ToString(p.ClaimDT),
                                                             ClaimAmt = Convert.ToDecimal(p.ClaimAmt),
                                                             Company = CompanyName,
                                                             CompanyCode = CompanyCode,
                                                             Receipt = p.ReceiptNo,
                                                             ReceiptDT = Convert.ToString(p.ReceiptDate),
                                                             FileName = p.UploadFileName,
                                                             Note = p.Note,
                                                             Status = p.ClaimStatus,
                                                             CRA = Convert.ToDecimal(p.CRA),
                                                             NonCRA = Convert.ToDecimal(p.NonCRA),
                                                             CompanyId = iObj.Id,
                                                             IsManual = Convert.ToBoolean(p.IsManual),
                                                             GetPAPDetails = GetEmpBankDetails(datacontext, p.CompanyUserId),
                                                             IsRefund = p.ClaimFee > 0 ? true : false,
                                                             ClaimServiceDetails = (from q in mastercontext.TblServiceProviders
                                                                                    join a in mastercontext.TblServices on q.ServiceId equals a.ServiceId
                                                                                    where q.Active == true && a.Active == true && q.ServiceProviderId == p.ServiceProviderId
                                                                                    select new ClaimServiceDetails()
                                                                                    {
                                                                                        ServiceName = a.Title,
                                                                                        ServiceProviderId = Convert.ToString(q.ServiceProviderId),
                                                                                        Address = q.Address,
                                                                                        PhoneNo = q.ContactNo,
                                                                                        Email = q.Email,
                                                                                        ServiceProvider = q.Name
                                                                                    }).Take(1).SingleOrDefault()
                                                         }).ToList();
                             lobjResponse.AdminClaimList = Obj;
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();

                         }
                         catch (Exception ex)
                         {

                             lobjResponse.Message = ex.Message;
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                         }
                         datacontext.Connection.Close();

                     }
                 }
                 CreateMasterResponseLog(mastercontext, CurrentTime, UserId, "AdminClaimList", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             else
             {
                 IsLogOut = true;
                 lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 lobjResponse.Message =  CObjects.SessionExpired.ToString();

             }
             lobjResponse.IsLogOut = IsLogOut;
             return lobjResponse;
         }


         #endregion

        #region DeleteCompanyV2
         /// <summary>
         /// Author:Jasmeet Kaur
         /// Function used to register new company
         /// </summary>
         /// <param name="iObj"></param>
         /// <returns></returns>
         public CResponse DeleteCompanyV2(IdInputv2 iObj)
         {
             string InputJson = "";
             DateTime StartTime = DateTime.UtcNow;
             CResponse lobjResponse = new CResponse();

             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {

                     InputJson = new JavaScriptSerializer().Serialize(iObj);
                     if (iObj.Id != -1)
                     {
                         TblCompany CompanyObj = (from p in datacontext.TblCompanies where p.CompanyId == iObj.Id select p).Take(1).SingleOrDefault();
                         if (CompanyObj != null)
                         {
                             CompanyObj.Active = false;
                             List<TblBrokerCompany> BrokerCompanyObj = (from p in datacontext.TblBrokerCompanies where p.CompanyId == iObj.Id select p).ToList();
                             if(BrokerCompanyObj != null)
                             {
                                 BrokerCompanyObj.ForEach(x => x.Active = false);
                             }

                             List<TblBrokerPayment> BrokerPaymentsObj = (from p in datacontext.TblBrokerPayments where p.CompanyId == iObj.Id select p).ToList();
                             if (BrokerPaymentsObj != null)
                             {
                                 BrokerPaymentsObj.ForEach(x => x.Active = false);
                             }
                        
                             #region Clear Message Data
                             EflexMsgDBDataContext msgcontext = new EflexMsgDBDataContext();
                             List<TblMsgUser> MsgUserList = (from p in msgcontext.TblMsgUsers where p.Active == true && p.CompanyId == iObj.Id select p).ToList();
                             if (MsgUserList != null && MsgUserList.Count() > 0)
                             {
                                 foreach (TblMsgUser item in MsgUserList)
                                 {

                                     item.Active = false;

                                     List<TblMsg> MsgObj = (from p in msgcontext.TblMsgs
                                                            where p.Active == true
                                                            && (p.SenderId == item.UserId || p.RecipientId == item.UserId)
                                                            select p).ToList();
                                     if (MsgObj != null && MsgObj.Count() > 0)
                                         MsgObj.ForEach(x => x.Active = false);

                                     List<TblConversation> ConversationObj = (from p in msgcontext.TblConversations
                                                                              where p.Active == true
                                                                                  && (p.User1 == item.UserId || p.User2 == item.UserId)
                                                                              select p).ToList();
                                     if (ConversationObj != null && ConversationObj.Count() > 0)
                                         ConversationObj.ForEach(x => x.Active = false);

                                     msgcontext.SubmitChanges();
                                 }
                             }

                             #endregion

                             #region Delete Folders
                             if (Directory.Exists(ConPath + iObj.Id))
                             {
                                 DirectoryInfo dir = new DirectoryInfo(ConPath + iObj.Id);

                                foreach(FileInfo fi in dir.GetFiles())
                                {
                                    fi.Delete();
                                }
                                   Directory.Delete(ConPath + iObj.Id);

                             }
                             #endregion Delete Folders

                             string DBName = CompanyNameFlag + iObj.Id;
                             string DbCon = DBFlag + iObj.Id;
                             string ConnStrPath = ConStrPath;
                             string Constring = GetConnection(DbCon);
                             EflexDBDataContext context = new EflexDBDataContext(Constring);
                             

                             #region Delete Database
                             
                             string DbPatch = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;


                             string contents = "";
                             contents = File.ReadAllText(DbPatch + "DeleteCompany.txt");
                             string Patch = contents.Replace("<CompId>", iObj.Id.ToString());
                             datacontext.ExecuteCommand(Patch.ToString());
                             datacontext.SubmitChanges();
                             #endregion Delete Database
                             datacontext.ExecuteCommand("ALTER DATABASE [" + DBName + "] set MULTI_USER ");
                             datacontext.SubmitChanges();
                             datacontext.ExecuteCommand("DROP DATABASE [" + DBName + "]");
                             datacontext.SubmitChanges();

                             XmlDocument xmlDoc = new XmlDocument();
                             xmlDoc.Load(ConnStrPath);
                             XmlNode nodeToDelete = xmlDoc.SelectSingleNode("//connectionStrings/add[@name='" + DbCon + "']");
                             if (nodeToDelete != null)
                             {
                                 nodeToDelete.ParentNode.RemoveChild(nodeToDelete);
                             }
                             xmlDoc.Save(ConnStrPath);


                          

                         

                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                             lobjResponse.Message = "SUCCESS";
                         }

                     }

                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }

                 CreateMasterResponseLog(datacontext, StartTime, UserId, "DeleteCompanyV2", InputJson, lobjResponse.Message);
                 datacontext.Connection.Close();
             }

             return lobjResponse;
         }


         #endregion DeleteCompanyV2

        #region MarkPaymentReceived
         public CResponse MarkPaymentReceived(MarkPaymentReceivedInput iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CResponse lobjResponse = new CResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             List<TblNotificationDetail> NotificationList = new List<TblNotificationDetail>();
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 if (IsValidateEflexUser(mastercontext))
                 {
                     try
                     {
                         TblCompPayment CompPaymentObj = (from p in mastercontext.TblCompPayments where p.Active == true && p.CompPaymentId == iObj.CompPaymentId select p).Take(1).SingleOrDefault();
                         if (CompPaymentObj != null)
                         {
                             CompPaymentObj.PaymentStatus = iObj.StatusId;
                             CompPaymentObj.PaymentDate = DateTime.UtcNow;
                             List<TblCompAdjustment> CompAdj = (from p in mastercontext.TblCompAdjustments where p.Active == true && p.CompanyId == CompPaymentObj.CompanyId select p).ToList();
                             if (CompAdj != null && CompAdj.Count() > 0)
                             {//jasmeet 23/03/2019 update adjudtment status for a company
                                 CompAdj.ForEach(x => x.PaymentStatus = iObj.StatusId);
                             }
                             string DbCon = DBFlag + CompPaymentObj.CompanyId;
                             string Constring = GetConnection(DbCon);

                             using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                             {


                                 List<TblCompanyPlan> PlanList = (from p in datacontext.TblCompanyPlans where p.Active == true select p).Distinct().ToList();
                                 if (PlanList != null && PlanList.Count() > 0)
                                 {
                                     foreach (TblCompanyPlan item in PlanList)
                                     {
                                         List<TblEmployee> EmpListObj = (from p in datacontext.TblEmployees where p.Active == true && p.CompanyPlanId == item.CompanyPlanId select p).ToList();

                                         if (EmpListObj != null && EmpListObj.Count() > 0)
                                         {
                                             foreach (TblEmployee empitem in EmpListObj)
                                             {
                                                 TblEmpTransaction EmpPendingObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == empitem.CompanyUserId && p.ClaimId == -1 && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).Take(1).SingleOrDefault();

                                                 if (EmpPendingObj != null)
                                                 {
                                                     if (EmpPendingObj.PaymentTypeId != CObjects.enumPaymentType.ADJ.ToString() || EmpPendingObj.PaymentTypeId != CObjects.enumPaymentType.WDR.ToString() || EmpPendingObj.PaymentTypeId != CObjects.enumPaymentType.FEE.ToString())
                                                     {
                                                         TblEmpBalance EmpBalObj = (from p in datacontext.TblEmpBalances where p.Active == true && p.CompanyUserId == empitem.CompanyUserId select p).Take(1).SingleOrDefault();
                                                         if (EmpBalObj != null)
                                                         {
                                                             EmpBalObj.Amount = EmpBalObj.Amount + EmpPendingObj.Amount;
                                                             EmpBalObj.PaymentStatus = Convert.ToDecimal(CObjects.enumPaymentStatus.Deposited);
                                                             EmpPendingObj.PaymentStatus = iObj.StatusId;
                                                             
                                                             List<TblPayment> PaymentObj = (from p in datacontext.TblPayments where p.Active == true && p.CompanyUserId == empitem.CompanyUserId && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                                                             if (PaymentObj != null && PaymentObj.Count() > 0)
                                                             {
                                                                 PaymentObj.ForEach(x => { x.PaymentStatus = iObj.StatusId; x.PaymentDueDate = DateTime.UtcNow; });

                                                             }
                                                             List<TblEmpTransaction> EmpObj = (from p in datacontext.TblEmpTransactions where p.Active == true && p.CompanyUserId == empitem.CompanyUserId && p.ClaimId == -1 && p.PaymentStatus == Convert.ToDecimal(CObjects.enumPaymentStatus.Pending) select p).ToList();
                                                             if (EmpObj != null && EmpObj.Count() > 0)
                                                             {
                                                                 EmpObj.ForEach(x => { x.PaymentStatus = iObj.StatusId; x.TransactionDate = DateTime.UtcNow; });

                                                             }
                                                            

                                                             List<TblNotificationDetail> NotificationUserList = null;
                                                             string PushNotifyText = "Your plan amount has been credited to your account. Thanks!";
                                                             NotifyUser1(datacontext, PushNotifyText, empitem.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());
                                                             NotificationUserList = InsertionInNotificationTable(empitem.CompanyUserId, PushNotifyText, datacontext, empitem.CompanyUserId, CObjects.enmNotifyType.Employee.ToString());
                                                             if (NotificationUserList != null)
                                                             {
                                                                 NotificationList.AddRange(NotificationUserList);
                                                                NotificationUserList.Clear();
                                                             }
                                                          


                                                             datacontext.SubmitChanges();
                                                         }
                                                     }

                                                 }
                                             }
                                         }

                                     }
                                     datacontext.SubmitChanges();
                                 }





                                 TblCompanyUser CompUserObj1 = (from p in datacontext.TblCompanyUsers where p.Active == true && p.CompanyUserId == 1 select p).Take(1).SingleOrDefault();
                                 TblUser UserObj = (from p in mastercontext.TblUsers where p.Active == true && p.UserId == iObj.LoggedInUserId select p).Take(1).SingleOrDefault();

                                 string Text = "Payment has been received by " + UserObj.FirstName + " " + UserObj.LastName + " on " +DateTime.UtcNow.ToString("MM/dd/yyyy")+ ".";
                                 CreateHistoryLog(mastercontext, CompPaymentObj.CompanyId, iObj.LoggedInUserId, -1, Text);

                                 #region SendMail



                                 
                                 StringBuilder builder = new StringBuilder();
                                 builder.Append("Hello " + CompUserObj1.FirstName + ((CompUserObj1.LastName == "" || CompUserObj1.LastName == null) ? "" : " " + CompUserObj1.LastName) + ",<br/><br/>");
                                builder.Append("Due amount has been received by eFlex Admin (" + UserObj.FirstName + " " + UserObj.LastName + ") on " + string.Format("{0:MM/dd/yyyy}", CompPaymentObj.PaymentDate) + ". <br/><br/>");
                                 builder.Append("Regards,<br/><br/>");
                                 builder.Append("Eflex Team<br/><br/>");
                                 //send mail

                                 string lstrMessage = File.ReadAllText(EmailFormats + "EmailTemplate.txt");
                                 lstrMessage = lstrMessage.Replace("lstrEmailContent", builder.ToString());
                                    
                                 string EmailTo = CompUserObj1.EmailAddress;
                                 string GodaddyUserName = WebConfigurationManager.AppSettings["EmailFrom"].ToString();
                                 string GodaddyPwd = WebConfigurationManager.AppSettings["Password"].ToString();
                                 string Server = WebConfigurationManager.AppSettings["EmailServer"].ToString();
                                 Int32 Port = Convert.ToInt32(WebConfigurationManager.AppSettings["Port"]);
                                 CMail Mail = new CMail();
                                 Mail.EmailTo = EmailTo;
                                 Mail.Subject = "Payment Received";
                                 Mail.MessageBody = lstrMessage;
                                 Mail.GodaddyUserName = GodaddyUserName;
                                 Mail.GodaddyPassword = GodaddyPwd;
                                 Mail.Server = Server;
                                 Mail.Port = Port;
                                 Mail.CompanyId = UserId;
                                 bool IsSent = Mail.SendEMail(datacontext, true);
                                 List<TblNotificationDetail> NotificationUserList1 = null;
                                 NotifyUser1(datacontext, Text,1, CObjects.enmNotifyType.Payment.ToString());
                                 NotificationUserList1 = InsertionInNotificationTable(1, Text, datacontext, 1, CObjects.enmNotifyType.Employee.ToString());
                                 if (NotificationUserList1 != null)
                                 {
                                     NotificationList.AddRange(NotificationUserList1);
                                     NotificationUserList1.Clear();
                                 }
                                 #endregion SendMail

                                 #region SendMessage
		                         TblUserCarrier CarrierObj = (from p in datacontext.TblUserCarriers where p.Active == true && p.CompanyUserId == 1 && p.IsSMS==true select p).Take(1).SingleOrDefault();
                                if (CarrierObj != null)
                                {
                                    string CarrierEmails = CarrierObj.ContactNo + "@" + CarrierObj.Carrier;
                                           
                                    CMail Mail1 = new CMail();
                                    Mail1.EmailTo = CarrierEmails;
                                    Mail1.Subject = "Due Payment Received";
                                    Mail1.MessageBody = Text;
                                    Mail1.GodaddyUserName = GodaddyUserName;
                                    Mail1.GodaddyPassword = GodaddyPwd;
                                    Mail1.Server = Server;
                                    Mail1.Port = Port;
                                    Mail1.CompanyId = CompPaymentObj.CompanyId;
                                    bool IsSent1 = Mail1.SendEMail(datacontext, true);

                                }
	                            #endregion

                                 #region Push Notifications
                               

                                if (NotificationList != null && NotificationList.Count() > 0)
                                {
                                    SendNotification(datacontext, NotificationList, CompPaymentObj.CompPaymentId);
                                    NotificationList.Clear();

                                }
                                #endregion Push Notifications

                                datacontext.Connection.Close();
                             }



                             mastercontext.SubmitChanges();

                            
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         }
                         else
                         {
                             lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                         }
                     }
                     catch (Exception ex)
                     {

                         lobjResponse.Message = ex.Message;
                         lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     }
                 }
                 else
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message =  CObjects.SessionExpired.ToString();
                     IsLogOut = true;
                 }
                 lobjResponse.IsLogOut = IsLogOut;
                 CreateMasterResponseLog(mastercontext, StartTime, UserId, "MarkPaymentReceived", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion UpdateClaimStatus

        #region PopulateCarrier
         public CPopulateCarrierResponse PopulateCarrier()
         {
             CPopulateCarrierResponse lobjResponse = new CPopulateCarrierResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {
                     List<PopulateCarrier> Obj = (from p in datacontext.TblCarriers
                                                  where p.Active == true
                                                  select new PopulateCarrier()
                                                  {
                                                      CarrierId = p.CarrierId,
                                                      Carrier = p.Carrier,
                                                      Title = p.Title
                                                  }).Distinct().ToList();
                     lobjResponse.PopulateCarrierData = Obj;
                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                 }
                 catch (Exception ex)
                 {

                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion PopulateReferrals

        #region AddEditCarrier
         public CResponse AddEditCarrier(AddEditCarrierInput iObj)
         {
             CResponse lobjResponse = new CResponse();
             using (MasterDBDataContext datacontext = new MasterDBDataContext())
             {
                 try
                 {
                      if(iObj.CarrierId ==-1)
                        {
                            TblCarrier CarrierObj = new TblCarrier()
                            {
                                Carrier = iObj.Carrier,
                                Title = iObj.Title,
                                Active = true,
                                DateCreated = DateTime.UtcNow,
                                DateModified = DateTime.UtcNow
                            };
                            datacontext.TblCarriers.InsertOnSubmit(CarrierObj);
                            datacontext.SubmitChanges();
                            lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                      
                        }
                        else
                        {
                            TblCarrier CarrierObj = (from p in datacontext.TblCarriers where p.Active == true && p.CarrierId == iObj.CarrierId select p).Take(1).SingleOrDefault();
                            if(CarrierObj != null)
                            {
                                CarrierObj.Carrier = iObj.Carrier;
                                CarrierObj.Title = iObj.Title;
                                CarrierObj.DateModified = DateTime.UtcNow;
                                datacontext.SubmitChanges();
                                lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                     
                            }
                            else
                            {
                                lobjResponse.Status = CObjects.enmStatus.Failure.ToString();
                        
                            }
                        }
                 }
                 catch (Exception ex)
                 {
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                     lobjResponse.Message = ex.Message;
                 }
                 datacontext.Connection.Close();
             }
             return lobjResponse;
         }
        #endregion

        #region ListCompanyEmployeesV2
         public CListCompEmployeesResponse ListCompanyEmployeesV2(ListCompanyEmployeesInput iObj)
         {
             DateTime CurrentTime = DateTime.UtcNow;
             CListCompEmployeesResponse lobjResponse = new CListCompEmployeesResponse();
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 
                     string DbCon = DBFlag + iObj.CompanyId;
                     string Constring = GetConnection(DbCon);
                     using (EflexDBDataContext datacontext = new EflexDBDataContext(Constring))
                     {
                         try
                         {
                             List<CompEmpDetails> Obj = (from p in datacontext.sp_ListCompEmployeesV2(iObj.ChunkStart, iObj.ChunkSize, iObj.SearchString,iObj.Filter)
                                                         select new CompEmpDetails()
                                                         {
                                                             CompanyUserId = Convert.ToDecimal(p.CompanyUserId),
                                                             FName = p.FirstName,
                                                             LName = p.LastName,
                                                             ContactNo = p.MobileNo,
                                                             Email = p.EmailAddress,
                                                             PlanName = p.PlanName,
                                                             AvailableAmount = Convert.ToDecimal(p.Amount) < 0 ? 0 : Convert.ToDecimal(p.Amount),
                                                             PlanId = Convert.ToDecimal(p.CompanyPlanId),
                                                             IsActive = Convert.ToString(p.IsActive),
                                                             IsDeleteable = GetIsDeleteable(datacontext, Convert.ToDecimal(iObj.CompanyId))
                                                         }).ToList();

                             lobjResponse.CompEmpDetails = Obj;
                             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                         }
                         catch (Exception ex)
                         {
                             lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                             lobjResponse.Message = ex.Message;
                         }

                         datacontext.Connection.Close();
                     }
                 
                 lobjResponse.IsLogOut = IsLogOut;
                  mastercontext.Connection.Close();
             }

             return lobjResponse;
         }

       
         #endregion

         public CResponse CheckOTPAPI()
         {
             CResponse lobjResponse = new CResponse();
             #region SendOTPMail
             string lstrMessage = "";
             StringBuilder builder = new StringBuilder();
             builder.Append("Welcome to eclipse EIOs eFlex SMS Messaging. Please enter one time password(OTP) 12345 to verify your mobile number.Thanks!");
             lstrMessage = builder.ToString();

             string AccountSid = "ACc7c88a50fe5dc57ffc8d6c51190afbfc";
             string AuthToken = "d89e99a6b1c71f7ec75e490b0745808b";

             TwilioClient.Init(AccountSid, AuthToken);
             ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                | SecurityProtocolType.Tls11
                                                | SecurityProtocolType.Tls12
                                                | SecurityProtocolType.Ssl3;
             var message = MessageResource.Create(
                 body: lstrMessage,
                 from: new Twilio.Types.PhoneNumber("+12897993854"),
                 to: new Twilio.Types.PhoneNumber("+12892423583")
             );

             lobjResponse.Status = CObjects.enmStatus.Success.ToString();
             lobjResponse.Message = "SUCCESS";

             return lobjResponse;
             #endregion SendMail
         }

         #region ListOfBrokerReferralsForAdmin
         public CListOfBrokerReferralsResponse ListOfBrokerReferralsForAdmin(LazyLoadingInputV2 iObj)
         {
             DateTime StartTime = DateTime.UtcNow;
             string InputJson = new JavaScriptSerializer().Serialize(iObj);
             CListOfBrokerReferralsResponse lobjResponse = new CListOfBrokerReferralsResponse();
             UserId = iObj.LoggedInUserId;
             bool IsLogOut = false;
             using (MasterDBDataContext mastercontext = new MasterDBDataContext())
             {
                 List<BrokerReferralData> Result = new List<BrokerReferralData>();
                 try
                 {
                     List<BrokerReferralData> Obj = (from p in mastercontext.TblBrokerReferrals.AsEnumerable()
                                                     where p.Active == true
                                                     select new BrokerReferralData()
                                                     {
                                                         BrokerReferralId = p.BrokerReferralId,
                                                         CompanyName = p.CompanyName,
                                                         CompanyWebsite = p.CompanyWebsite,
                                                         Industry = p.Industry,
                                                         NoOfEmployees = Convert.ToDecimal(p.NoOfEmployees),
                                                         HaveGroupBenefits = Convert.ToString(p.HaveGroupBenefits),
                                                         ContactName = p.ContactName,
                                                         ContactTitle = p.ContactTitle,
                                                         EmailAddress = p.EmailAddress,
                                                         Phone = p.Phone,
                                                         ReferralFile = p.DocFile,
                                                         Status = Convert.ToDecimal(p.Status)

                                                     }).ToList();

                     if (Obj != null && Obj.Count > 0)
                     {
                         if (iObj.ChunckStart == -1)
                         {
                             Result = Obj.OrderByDescending(i => i.BrokerReferralId).Take(iObj.ChunckSize).ToList();
                         }
                         else
                         {
                             Result = Obj.OrderByDescending(i => i.BrokerReferralId).Where(i => i.BrokerReferralId < iObj.ChunckSize).Take(iObj.ChunckSize).ToList();
                         }
                     }


                     lobjResponse.BrokerReferralData = Result;
                     lobjResponse.Status = CObjects.enmStatus.Success.ToString();
                 }
                 catch (Exception ex)
                 {

                     lobjResponse.Message = ex.Message;
                     lobjResponse.Status = CObjects.enmStatus.Error.ToString();
                 }

                 CreateMasterResponseLog(mastercontext, StartTime, -1, "ListOfBrokerReferralsForAdmin", InputJson, lobjResponse.Message);
                 mastercontext.Connection.Close();
             }
             return lobjResponse;
         }
         #endregion
    }
}