﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EflexServices
{
    public class CCompanyObjects
    {
    }

    public class CGetCompanyDashboardDataResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<GetDashboardData> GetDashboardData { get; set; }
    }

    public class GetDashboardData
    {
        public string Plan { get; set; }
        public decimal GrossAmount { get; set; }
        public string Color { get; set; }
        public List<CompanyEmpData> CompanyEmpData { get; set; }

    }

    public class CompanyEmpData 
    {
        public string EmpName { get; set; }
        public decimal Contibution { get; set; }
    }

    public class CGetActivityDataResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public decimal TotalBalance { get; set; }
        public decimal Adjustments { get; set; }
        public decimal AvailableCredits { get; set; }
        public List<PendingTransaction> PendingTransaction { get; set; }
        public List<ActivityList> ActivityList { get; set; }

    }

    public class GetActivityInput
    {
        public int ChunkSize { get; set; }
        public decimal ChunkStart { get; set; }
        public string SearchString { get; set; }
        public string DbCon { get; set; }
        public decimal Id { get; set; }//1  payment, 2   Adjustment, -1  All
        public string Period { get; set; }
        public decimal LoggedInUserId { get; set; }

    }

    public class ActivityList
    {
        public string TransactionDate { get; set; }
        public string PostedDate { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string TypeId { get; set; }
    }

    public class CGetPAPDetailsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public GetPAPDetails GetPAPDetails { get; set; }


    }

    public class GetPAPDetails
    {
         public decimal PAPDetailId { get; set; }
         public decimal CompanyUserId { get; set; } 
	     public string BankName { get; set; }
	     public string TransitId { get; set; }
	     public string InstitutionId { get; set; }
	     public string AccountNumber { get; set; }
	     public string ChequeFile { get; set; }
         public string IsCheque { get; set; }
         public string IsAgree { get; set; }
    }

    public class UpdatePAPDetailsInput
    {
        public string DbCon { get; set; }
        public decimal PAPDetailId { get; set; }
        public decimal CompanyId { get; set; }
        public string BankName { get; set; }
        public string TransitId { get; set; }
        public string InstitutionId { get; set; }
        public string AccountNumber { get; set; }
        public string ChequeFile { get; set; }
        public string IsCheque { get; set; }
        public string IsAgree { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class AddEmployeeInput
    {
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
        public List<EmployeesList> EmployeesList { get; set; }
        
    }

    public class GetEStatementInput
    {
        public Int32 Year { get; set; }
        public Int32 Month { get; set; }
        public string DbCon { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class CompanyEStatementDetails
    {
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public List<InvoiceList> InvoiceList { get; set; }
        public decimal SubTotal { get; set; }
        public decimal GrandTotal { get; set; }
    }

    public class InvoiceList
    {
        public string InvoiceDetail { get; set; }
        public string PaymentDueDate { get; set; }
        public decimal Amount { get; set; }
        public decimal Total { get; set; }
    }

    public class CNotificationLogResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public List<NotificationLog> NotificationLog { get; set; }
    }
    public class NotificationLog
    {
        public decimal NotifyLogId { get; set; }
        public string NotifyText { get; set; }
        public string NotifyDate { get; set; }
        public string NotifyType { get; set; }

    }

    public class CNotiMsgCountResponse
    {
        public bool IsLogOut { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public decimal MessageCount { get; set; }
        public decimal UnreadNotificationCount { get; set; }
        public bool IsCardActive { get; set; }
        public decimal MasterCardId { get; set; }
        public decimal AccessCode { get; set; }
        public bool IsFreeze { get; set; }
       
    }
    public class AddPlanInput
    {
        public string DbCon { get; set; }
        public decimal CompanyId { get; set; }
        public decimal PlanTypeId { get; set; }
        public string PlanStartDt { get; set; }
        public string Name { get; set; }
        public int ContributionSchedule { get; set; }
        public decimal RecurringDeposit { get; set; }
        public decimal InitialDeposit { get; set; }
        public decimal NetDeposit { get; set; }
        public decimal AdministrationInitialFee { get; set; }
        public decimal AdministrationMonthlyFee { get; set; }
        public decimal HSTInitial { get; set; }
        public decimal HSTMonthly { get; set; }
        public decimal PremiumTaxInitial { get; set; }
        public decimal PremiumTaxMonthly { get; set; }
        public decimal RetailSalesTaxInitial { get; set; }
        public decimal RetailSalesTaxMonthly { get; set; }
        public decimal GrossTotalInitial { get; set; }
        public decimal GrossTotalMonthly { get; set; }
        public decimal PlanDesign { get; set; }
        public decimal RollOver { get; set; }
        public string CarryForwardPeriod { get; set; }
        public decimal Termination { get; set; }
        public decimal LoggedInUserId { get; set; }
      
    }

    public class NotiMsgCountInfo
    {
        public string DbCon { get; set; }
        public decimal UserId { get; set; }
        public decimal UserTypeId { get; set; }
        public decimal CompanyId { get; set; }
    }
    public class UpdateStatusIdInput
    {
        public decimal Id { get; set; }
        public bool IsStatus { get; set; }
        public string Reason { get; set; }
        public decimal LoggedInUserId { get; set; }
    }

    public class ListCompAllTransactionsInput 
    {
        public int ChunckSize { get; set; }
        public decimal ChunckStart { get; set; }
        public string SearchString { get; set; }
        public string FromDt { get; set; }
        public string ToDt { get; set; }
        public decimal LoggedInUserId { get; set; }
        public decimal CompanyId { get; set; }
    }

    public class CListCompAllTransactionsResponse
    {
        public bool IsLogOut { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public List<CompAllTransactionsData> CompAllTransactionsData { get; set; }
    }
    public class CompAllTransactionsData
    {
        public decimal RowId { get; set; }
        public decimal CompAdjustmentId { get; set; }
        public decimal CompanyPaymentId { get; set; }
        public string CompanyName { get; set; }
        public string PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentDetails { get; set; }
        public decimal PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string Type { get; set; }
    }

    public class GetDashboardTransactionsInput
    {
        public string DbCon { get; set; }
        public decimal ServiceTypeId { get; set; }
        public decimal Id { get; set; }

    }
    public class GetEmpPlanDashboardDetailsInput
    {
        public string DbCon { get; set; }
        public string FilterType { get; set; }
        public decimal Id { get; set; }
    }

    public class ResentOwnerCredentialsInput
    {
        public decimal Id { get; set; }
        public string EmailId { get; set; }
    }

    public class ResentEmployeeCredentialsInput
    {
        public decimal CompanyId { get; set; }
        public decimal Id { get; set; }
        public string EmailId { get; set; }
    }

    public class ResentBrokerCredentialsInput
    {
        public decimal Id { get; set; }
        public string EmailId { get; set; }
    }
}