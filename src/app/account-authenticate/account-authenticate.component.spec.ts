import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountAuthenticateComponent } from './account-authenticate.component';

describe('AccountAuthenticateComponent', () => {
  let component: AccountAuthenticateComponent;
  let fixture: ComponentFixture<AccountAuthenticateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountAuthenticateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountAuthenticateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
