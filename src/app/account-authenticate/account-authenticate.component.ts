import { Component, OnInit, HostListener } from '@angular/core';
import {CompanyService} from '../company.service';
import { Router,ActivatedRoute }              from '@angular/router';
@Component({
  selector: 'app-account-authenticate',
  templateUrl: './account-authenticate.component.html',
  styleUrls: ['./account-authenticate.component.css']
})
export class AccountAuthenticateComponent implements OnInit {

  companyId: any = -1;
  userId: any= -1;
  loading: boolean = false;
  confirm_password: any = '';
  password: any = '';

  screenHeight: any;
  minusHeight: any = 0;

  constructor(
    private service: CompanyService,
    private router: Router,
    private acitvateRoute: ActivatedRoute) {
      this.acitvateRoute.params.subscribe((param)=>{
        console.log(param);
        this.companyId = param['companyId'];
        this.userId = param['userId'];
      })
   }

  ngOnInit() {

  }

  login(form){
    let pwd = form.controls.password.value;
    let confirmPwd = form.controls.confirm_password.value;

    if(pwd != confirmPwd){
      this.service.alertMessage("Password and confrim password did not match",'error','');
      return false;
    }
    const _input ={
      "CompanyId": this.companyId,//.replace(/\s/g, ""),
      "CompanyUserId": this.userId,
      "Password": pwd//.replace(/\s/g, "")
    }
    this.loading = true;
    this.service.authorizeUser(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.loading = false;
        if(res['InActive'] == true){
          localStorage.setItem('c_code', res['UniqueCode']);
          //this.service.alertMessage("You are Login successfully",'error','');
          sessionStorage.setItem("companyId",res['CompanyId']);
          sessionStorage.setItem("stage",res['StagesCovered']);
          sessionStorage.setItem("DbCon",res['DbCon']);
          sessionStorage.setItem("companyName", res['CompanyName']);
          sessionStorage.setItem("userName", res['Name']);
          sessionStorage.setItem("userTypeId", res['UserTypeId']);
          sessionStorage.setItem("uniqueCode", res['UniqueCode']);
          sessionStorage.setItem("amountDue", res['AmountDue']);
          sessionStorage.setItem("loginUserId", res['LoggedInUserId']);
          sessionStorage.setItem("compnayPlanTypeId", res['PlanTypeId']);
          sessionStorage.setItem("isCorporation", res['IsCorporation']);
          sessionStorage.setItem("UserToken", res['UserToken']);
          sessionStorage.setItem("CompanyRegDate", res['CompanyRegDate']);
          this.service.setActiveToken(res['UserToken']);
          if(res['UserTypeId'] == 1){
            if(res['StagesCovered'] == 1 && res['UserTypeId'] == 1){
              this.router.navigate(['/register/personal']);
            }
            if(res['StagesCovered'] == 2  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/plan']);
            }
            if(res['StagesCovered'] == 3  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/member-detail']);
            }
            if(res['StagesCovered'] == 4  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/plan-review']);
            }
            if(res['StagesCovered'] == 5  && res['UserTypeId'] == 1){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
            }
            if(res['StagesCovered'] == 5  && res['UserTypeId'] == 1){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
              //this.router.navigate(['/employee-detail']);
            }
          }
          //admin
          else if(res['UserTypeId'] == 2){
            if(res['StagesCovered'] == 4){
              this.router.navigate(['/register/plan-review']);
            }
            if(res['StagesCovered'] == 5){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
            }
          }
        }else{
          //this.inactiveModal = true
        }
    }else{
      this.loading = false;
      this.service.alertMessage(res['Message'],'error','');
    }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }

  textDecode(pvarTxt){
    if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
        return this.service.decodeText(pvarTxt);
    }
  }

  togglePassword(){
       var x = document.getElementById("loginPassword");
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
  }

  toggleConfirmPassword(){
       var x = document.getElementById("loginConfirmPassword");
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
  }

}
