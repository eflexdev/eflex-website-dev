import { Component, OnInit, Pipe,PipeTransform,ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
import {DatePipe} from '@angular/common';
import * as jsPDF from 'jspdf';
declare var $ :any;
import {CsvService} from 'angular2-json2csv';
import {environment} from '../../../environments/environment.prod'
import { utils, write, WorkBook } from 'xlsx';
import {IMyDpOptions} from 'mydatepicker';
import { saveAs } from 'file-saver';
import {MemberService} from '../../member.service';

@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.css']
})
export class ClaimsComponent implements OnInit {


    chart: Chart;
    capacity_chart: any;
    NetGrossInitial: any;
    NetGrossRecurring: any;
    plansData: any=[];
    plansChart: any;
    companyName: string;
    isPending: boolean = true;
    isCurrent: boolean = true;
    isContribution: boolean= true;
    companyCode:string;
    amountDue: any = 0;
    colorArr:any = [];
    currentTransaction: any;
    pendingTransaction: any;


    periodData: any[];
    period: string = 'current';
    filterByType: number = -1
    searchString: string = '';
    adjustments: any = 0;
    payments: any = 0;
    claimedAmt: any = 0;
    paymentToDate: any = '';
    paymentFromDate: any ='';
    companyId: any = ''
    hostUrl: string = '';
    @ViewChild('transactions') transactions:ElementRef
    constructor(private service: CompanyService, private router : Router,
    private CsvService: CsvService, private memberService: MemberService, private el: ElementRef) {
        this.hostUrl = environment.hostUrl;
        this.companyId = sessionStorage.getItem("companyId");
     }

    d = new Date();
    public myDatePickerOptions: IMyDpOptions = {
       dateFormat: 'mm/dd/yyyy',
        editableDateField: false,
       //dateFormat: 'yyyy/mm/dd',
       //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1},
       disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+1}
   };

    ngOnInit() {
      if(sessionStorage.getItem("DbCon") != null){
          this.companyName = sessionStorage.getItem("companyName");
          this.companyCode = sessionStorage.getItem("uniqueCode");
          this.amountDue = sessionStorage.getItem("amountDue");

          this.getTransaction();

          //this.getAccessToken()

      }else{
        this.router.navigate(['/login']);
      }
    }

    onPeriodChange(){
      this.period  = this.period;
      this.pendingTransaction = [];
      this.currentTransaction = [];
      this.getTransaction();
    }

    onTypeChange(){
      this.filterByType = this.filterByType;
      this.pendingTransaction = [];
      this.currentTransaction = [];
      this.getTransaction();
    }

    searchEmpety(){
    if(this.searchString.length == 0){
        this.pendingTransaction = [];
        this.currentTransaction = [];
        this.getTransaction();
    }
  }

    getClimByDate(){
      if(this.paymentFromDate == '' ){
        this.service.alertMessage('Please select "from" date',"error");
        return false;
      }
      if(this.paymentToDate == '' ){
        this.service.alertMessage('Please select "to" date',"error");
        return false;
      }

      this.getTransaction();
    }

    getTransaction(){
      var datePipe = new DatePipe("en-US");
      let periodDate = ''
      if(this.period == 'current'){
         periodDate = datePipe.transform(new Date().toDateString(), 'MM/dd/yyyy');
      }else{
        periodDate = datePipe.transform(new Date(this.period).toDateString(), 'MM/dd/yyyy');
      }

      let fromDate = '';
      let toDate = '';

      if(this.paymentFromDate){
          fromDate = this.paymentFromDate.formatted;
      }
      if(this.paymentToDate){
         toDate = this.paymentToDate.formatted;
      }

      const _input={
        "ChunkSize":500,
      	"ChunkStart":-1,
      	"DbCon":sessionStorage.getItem("DbCon"),
      	"Id": sessionStorage.getItem('loginUserId'),
      	// "Period":periodDate,
      	"SearchString":this.searchString,
        "FromDate":fromDate,
        "ToDate":toDate
      };
      console.log(_input);
      this.memberService.listOfClaims(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
          console.log(res);
          this.claimedAmt = res['ClaimedAmt'];
          this.currentTransaction = res['ListOfClaimsData'];
          //this.pendingTransaction = res['PendingTransaction'];
        }else{
          this.service.alertMessage(res['Message'],'error');
        }
      })
    }

    downLoadPDF(){
      // Landscape export, 2×4 inches
      let doc = new jsPDF();
      // We'll make our own renderer to skip this editor
      let specialElementHandlers = {
      	'#editor': function(element, renderer){
      		return true;
      	},
      	'.controls': function(element, renderer){
      		return true;
      	}
      };

    let content = this.transactions.nativeElement;
    // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
    doc.fromHTML(content.innerHTML, 10, 10, {
    	'width': 190,
    	'elementHandlers': specialElementHandlers
    });
    //console.log(specialElementHandlers);
      //doc.text('Hello world!', 1, 1)
    let pdfName= "current-transactions_"+new Date()+".pdf";
    doc.setFontType("normal");
    doc.save(pdfName)
  }

    transform(value){
      var s=value;
      value=s.replace("AM"," AM")
      var datePipe = new DatePipe("en-US");
      //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      return value;
    }

    downloadCSV(){
        var i=1;
        var newArr = [];
        this.currentTransaction.forEach((res)=>{
          let obj = {};
          obj['Sr.No'] = i++;
          obj['Claim Date'] = this.transform(res['ReceiptDate']);
          obj['Type'] = res['IsManual'] == true ? 'Manual' : 'Load  Card';
          obj['Dependant Name'] = res['Dependants'];
          obj['Claim Status'] = res['ClainStatus'];
          obj['Amount'] = res['ClaimAmt'];
          newArr.push(obj);
        })

        const ws_name = 'card_transactions';
        const wb: WorkBook = { SheetNames: [], Sheets: {} };
        const ws: any = utils.json_to_sheet(newArr);
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = ws;
        const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

        function s2ab(s) {
          const buf = new ArrayBuffer(s.length);
          const view = new Uint8Array(buf);
          for (let i = 0; i !== s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
          };
          return buf;
        }
        saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), 'card_transactions.xlsx');

    }

    receiptModal: boolean = false;
    selectedClaims: any = '';
    openReceiptModal(pvarData){
      this.selectedClaims = pvarData;
      this.receiptModal = true;
      this.el.nativeElement.querySelector('#file').value= '';
      this.el.nativeElement.value = '';
    }

    uploadMsg = '';
    fileName: string= '';
    importLoaderModal: boolean;
    fileType: string = '';
    uploadFile(event) {
      this.uploadMsg = '';
      let file = event.target.files[0];
      let inputEl = event.target.files;
      //let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
      //get the total amount of files attached to the file input.
      let fileCount: number = inputEl.length;
      //create a new fromdata instance
      let formData = new FormData();
      if (fileCount > 0) { // a file was selected
        var strFileName = String(inputEl[0].name);
        if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
          this.uploadMsg = "Please wait receipt uploading…";
          formData.append('file', file);
          formData.append('CompanyId', sessionStorage.getItem('companyId'));
          this.service.uploadFile(formData)
          .subscribe((result)=>{
                this.uploadMsg = '';
                var respnose = result._body;
                if(respnose){
                  const img_name = respnose.split("###");
                    if(img_name[0] == 'Success'){
                        this.fileName = img_name[1];
                        this.fileType = img_name[1].split('.').pop().toLowerCase();

                    }else{
                        //this.service.alertMessage("Receipt upload encountered an error. Please try again.","error");
                        this.receiptModal = false;
                        this.service.alertMessage(img_name[1],"error");
                    }
                }
              },
              (error) =>{  console.log(error);  });
          }else{
            this.service.alertMessage("Please select a file with correct extension .jpg |.pdf |.png |.jpeg","error");
            return false;
          }
        }else{
            this.uploadMsg =" Please choose file";
        }
   }

   isClaimDetailModal: boolean = false;
   claimDetails: any = [];
   openClaimDetail(pvarObj){
     console.log(pvarObj);
     this.isClaimDetailModal = true;
     this.claimDetails = pvarObj;
   }

   LC_ReceiptNo: any = '';
   LC_ReceiptDate: any = '';
   submitLoadCardReceipt(){
     if(this.LC_ReceiptDate == ''){
       this.service.alertMessage("Please choose receipt date","error");
       return false;
     }
     // if(this.LC_ReceiptNo == ''){
     //   this.service.alertMessage("Please enter receipt number","error");
     //   return false;
     // }
     if(this.fileName == ''){
       this.service.alertMessage("Please upload receipt","error");
       return false;
     }

     let _input = {
       "ClaimId":this.selectedClaims['EmpClaimId'],
       "CompanyId":this.companyId,
       "FileName":  this.fileName,
       "LoggedInUserId":sessionStorage.getItem('loginUserId'),
       "ReceiptDt":this.LC_ReceiptDate.formatted,
       "ReceiptNo":this.LC_ReceiptNo
     }
     this.memberService.uploadReceiptV2(_input)
     .subscribe((respnose:Response)=>{
         if(respnose['Status'] == "Success"){
             this.receiptModal = false;
             this.selectedClaims['UploadFileName'] = this.fileName;
             this.fileName = '';
             this.LC_ReceiptNo = '';
             this.LC_ReceiptDate = '';
             this.service.alertMessage("Receipt successfully uploaded","success");
         }else{
           this.service.alertMessage(respnose['Message'], "error");
           this.receiptModal = false;
         }
     })

   }

   reciptModalClose(){
     this.receiptModal = false;
     this.fileName = '';
     this.LC_ReceiptNo = '';
     this.LC_ReceiptDate = '';
     this.el.nativeElement.querySelector('#file').value= '';
     this.el.nativeElement.value = '';
   }

   sortingName: string;
   isDesc: boolean;
   sort(name: string): void {
     if (name && this.sortingName !== name) {
       this.isDesc = false;
     } else {
       this.isDesc = !this.isDesc;
     }
     this.sortingName = name;
   }


}
