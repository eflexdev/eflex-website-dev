import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod';
declare var CountUp : any;
@Component({
  selector: 'app-add-claim',
  templateUrl: './add-claim.component.html',
  styleUrls: ['./add-claim.component.css']
})
export class AddClaimComponent implements OnInit {
  step:number=1;
  claimId: any = -1;
  isFormValid: boolean = false;
  addClaimForm: FormGroup;
  isLoading: boolean = false;
  serviceData:any[];
  dependntsList:any[];
  modal: boolean;
  companyId: any = -1;
  hostUrl:string='';
  planDesign: any = 1;

  name: string ='';
  email:string = '';
  contact: string = '';
  address:string = '';
  serviceName: string = '';
  previousSelectedServiceText: string = '';

  availableBalance: any = 0;
  isCardSuspended : boolean = false;
  openAddDepandentModal: boolean = false;
  isCardActive : boolean = false;
  receiptModal: boolean = false;
  daysRemaining: any = 0;
  isExpired:boolean = false;
  applyClaimfee: boolean = false;
  cpfAmount: any = 0;
  IsCoverAll: boolean = false;

  @ViewChild('fileInput') fileInput: ElementRef;
  d = new Date();
  plandate: any = '';
  public myDatePickerOptions: IMyDpOptions = {
      dateFormat: 'mm/dd/yyyy',
      editableDateField: false,
      //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+2},
      //disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+2, day: this.d.getDate()+1}
      disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };
 public dobDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     editableDateField: false,
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
};
 emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(
    private fb:FormBuilder,
    private service: CompanyService,
    private memberService: MemberService,
    private router: Router, private el : ElementRef) {
      this.hostUrl = environment.hostUrl;
      this.addClaimForm = this.fb.group({
          "Amount":['',[Validators.required]],
          //"ClaimAmt": ['',[Validators.required]],
          //"ClaimTypeId":['',[Validators.required]],
          "CompanyUserId":sessionStorage.getItem('loginUserId'),
          "DbCon":sessionStorage.getItem('DbCon'),
          "DependantId":[''],
          "Note":[''],
        //  "OtherClaimType":[''],
          "ReceiptDate":[''],
          "ReceiptNo":[''],
          "ServiceTypeId":[''],
          "ServiceProvider":[''],
          "isDepandent":[''],
          "UploadFileName":[''],
          "searchServiceText":[''],
          "ProviderAddress": [''], //this.serviceProviderName,
          "ProviderContactNo":[''],
          "ProviderEmail":['', Validators.pattern(this.emailPattern)]
      });
      if(sessionStorage.getItem('companyId') != null){
          this.companyId = sessionStorage.getItem("companyId");
      }else{
        this.service.alertMessage("Please login first","error");
        this.router.navigate(['/']);
      }

      if(sessionStorage.getItem('loginUserId') != null){
        this.getCardDetail();
      }
    }

    getCardDetail(){
      let _input = {
        "DbCon":"DbCon_"+sessionStorage.getItem('companyId'),
      	"Id":sessionStorage.getItem('loginUserId'),
      	"LoggedInUserId":sessionStorage.getItem('loginUserId')
      }
      this.memberService.getCardData(_input)
      .subscribe((response:Response)=>{
        if(response['Status'] == "Success"){
          this.availableBalance = response['Data']['AccountBalance'];
          this.isCardSuspended = response['Data']['IsSuspended'];
          this.isCardActive = sessionStorage.getItem('isCardActive') == 'true' ? true : false;
          this.daysRemaining = response['DaysRemaining'];
          this.isExpired = response['IsExpired'];
          this.applyClaimfee = response['ApplyClaimfee'];
          this.cpfAmount = response['ClaimFee'];

          this.IsCoverAll = response['IsCoverAll'];

          this.plandate = new Date(response['PlanStartDate']);
          this.myDatePickerOptions={
            disableUntil: {year: this.plandate.getFullYear(), month: this.plandate.getMonth()+1, day: this.plandate.getDate()},
            disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+1}
          }
          //alert(response['Data']['IsSuspended'])
        }else{
          this.service.alertMessage(response['Message'],"error");
        }
      },(error:Error)=>{
        this.isServiceCall = false;
        this.service.alertMessage(error,"error");
      })
    }

    ngOnInit() {
        if(sessionStorage.getItem("acctBalnValue") == '0'){
          this.service.alertMessage("Sorry, you dont have the available balance in your account.","error");
          this.router.navigate(['/member-portal/dashboard']);
        }
        this.planDesign = sessionStorage.getItem('planDesign');
        this.getDependant();
        this.getCurrentLocation();
    }

    getCurrentLocation(){
      if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
      }else{
        //alert("Location not get");
      }
    }
    latLong: any = '';
    setPosition(position){
      this.latLong = position.coords.latitude +","+ position.coords.longitude;
    }

    getDependant(){
        let _input ={
           "ChunkSize":1000,
           "ChunkStart":-1,
           "DbCon":sessionStorage.getItem('DbCon'),
           "Id":sessionStorage.getItem('loginUserId'),
           "SearchString":""
         }
        this.memberService.listOfDependents(_input) //populateDependants
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
              this.dependntsList = res['ListOfDependents'];
          }else{
            this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          this.service.alertMessage(error,'error','')
        })
    }

    myClaims: any = [];
    claimsReview: boolean = false;

    saveClaimsToLocal(){
      if(this.addClaimForm.valid){
        this.addClaimForm.value['ReceiptDate']= '';  //this.addClaimForm.value['ReceiptDate'].formatted
        let _input = this.addClaimForm.value;
        this.myClaims.push(_input);
        console.log(this.myClaims)
        this.addClaimForm.reset();
        this.claimsReview = true;
        this.goToStep(2);
      }
    }

    selectedServiceTypeId: any = -1;
    setServiceType(pvarData,serviceTitle){
      this.selectedServiceTypeId = pvarData;
      // this.searchServiceProviderText = serviceTitle;
      this.previousSelectedServiceText = serviceTitle;
      this.addClaimForm.patchValue({
          "ServiceProvider": '',
          "ServiceTypeId":pvarData //pvarData['ServiceId']
      })
      this.serviceProviderData = [];
      this.getServiceProvider();
      this.goToStep(5);
    }

    selectedCalimType : any;
    initialClaimType: any;
    errorModal: boolean = false;
    currentDate: any = new Date();
    uploadTillDate: any = '';
    setClaimType(pvarTypeId){
      this.selectedCalimType = pvarTypeId; // 1 for laod card, 2 for claim submit
      this.initialClaimType = pvarTypeId;
      // this.myDatePickerOptions = {
      //       dateFormat: 'mm/dd/yyyy',
      //       editableDateField: false,
      //       disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+1},
      //       disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+1}
      //   };
      //   let  date = new Date();
      //   this.addClaimForm.patchValue({
      //     "ReceiptDate": {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
      //   })
      if(pvarTypeId == 1){
        if(this.isExpired == false){

            if(this.daysRemaining < 0 && this.isExpired == false){ //for -1 case
                this.goToStep(2);
            }
            else if(this.daysRemaining == 0 && this.isExpired == false){
               this.currentDate.setDate(this.currentDate.getDate() + 2);
               this.uploadTillDate = this.currentDate;
               this.errorModal = true;
               return false;
              //this.goToStep(2);
            }
            else if(this.daysRemaining > 0 && this.isExpired == false){
                this.currentDate.setDate(this.currentDate.getDate() + this.daysRemaining);
                this.uploadTillDate = this.currentDate;
                this.errorModal = true;
                return false;
            }
        }else{
            this.errorModal = true;
        }
      }else{
        this.selectedCalimType = pvarTypeId; // 1 for laod card, 2 for claim submit
        this.initialClaimType = pvarTypeId;
        this.goToStep(2);
      }

    }

    uploadLater(){
        this.errorModal = false;
        this.goToStep(2);
    }

    selectedPlanService: any = -1;
    setPlanWhereSpend(pvarTypeId){
      this.serviceData = [];
      this.serviceProviderData = [];
      this.selectedPlanService = pvarTypeId; // 1 health,2 dental, 3 wellness, 4 others
      this.getPlanServices();
      //this.addClaimForm.value.ServiceTypeId = '';
       this.addClaimForm.patchValue({
         "ServiceTypeId" : '',
         "ServiceProvider": '',
         "ProviderAddress": '', //this.serviceProviderName,
         "ProviderContactNo":'',
         "ProviderEmail":''
       })
    //   this.selectedServiceTypeId = pvarTypeId;

       if(this.selectedCalimType == 1){
         this.goToStep(9); //26-june-2019
       }else{
         this.goToStep(4);
       }

    }
    serviceProviderName: string ='';
    serviceProviderAddress: string = '';
    uniqueId: any= -1;
    isProviderInfoModel : boolean = false;
    selectedProviderIndex: string = '';
    setServiceProvider(pvarObj,type,index){
      this.serviceProviderName = pvarObj['Name'];
      this.selectedProviderIndex = pvarObj['Name']+'_'+index+'_'+type;
      this.serviceProviderAddress = pvarObj['Address'];
      this.uniqueId = pvarObj['UniqueId'];
      this.isProviderInfoModel = true;
      //this.serviceProviderId = pvarObj['UniqueId'];
      this.addClaimForm.patchValue({
        "ServiceProvider":  pvarObj['Name'],
        "ProviderAddress": pvarObj['Address'],
        "ProviderContactNo": pvarObj['ContactNo'],
        "ProviderEmail": pvarObj['Email']
      })
    }

    searchServiceText: string = '';
    searchEmpety(){
      this.serviceData = [];
      if( this.addClaimForm.value.searchServiceText.length == 0 || this.addClaimForm.value.searchServiceText.length == 1){
        this.serviceData = [];
        this.addClaimForm.value.searchServiceText = '';
        this.getPlanServices();
      }
    }

    searchServicesByText(){
      //alert(this.addClaimForm.value.searchServiceText.length);
      // if( this.addClaimForm.value.searchServiceText == ''){
      //   this.service.alertMessage("Please enter text for search","error");
      //   return false;
      // }
      this.searchServiceText =  this.searchServiceText;
      this.serviceData = [];
      this.getPlanServices();
    }

    isServiceDataCall: boolean = false;
    noServiceData : boolean = false;
    getPlanServices(){
      this.noServiceData = false;
      const _input = {
        "Id": this.selectedPlanService,
        "SearchString":'', //this.addClaimForm.value.searchServiceText
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      }
      this.isServiceDataCall = true;
      this.memberService.listOfServices(_input)
        .subscribe((res:Response)=>{
          this.isServiceDataCall = false;
          if(res['Status'] == 'Success'){
            this.serviceData= res['ListOfServices'];
            if(res['ListOfServices'].length < 1){
              this.noServiceData = true;
            }
          }else{
              this.service.alertMessage(res['Message'],'error','');
          }
        },(error)=>{
          this.isServiceDataCall = false;
          this.service.alertMessage(error,'error','');
        })
    }

    serviceProviderData: any = [];
    searchServiceProviderText: string = '';
    isProviderDataCall: boolean = false;
    noProviderData: boolean = false;
    otherTypeProvider: string = '';
    nearByProvider: any = [];
    recentProvider: any = [];
    othersProvider: any = [];
    recentAddedProvider: any = [];

    searchProvider(){
      this.serviceProviderData = [];
      this.getServiceProvider();
    }
    searchEmpetyProvider(){
      this.serviceProviderData = [];
      if( this.searchServiceProviderText.length == 0 || this.searchServiceProviderText == ''){
        this.serviceProviderData = [];
        this.searchServiceProviderText = this.previousSelectedServiceText;
        this.getServiceProvider();
      }
    }

    getOtherProvider(){
      this.serviceProviderData = [];
      this.getServiceProvider();
    }
    getServiceProvider(){
      this.noProviderData = false;
      let serviceTypeName = '';
      if(this.selectedPlanService == 1){
        serviceTypeName = 'health';
      }
      if(this.selectedPlanService == 2){
        serviceTypeName = 'dentist';
      }
      if(this.selectedPlanService == 3){
        serviceTypeName = 'gym'; // 'wellness'
      }
      if(this.selectedPlanService == 4){
        serviceTypeName = this.otherTypeProvider;
      }
      const _input = {
        "DbCon":sessionStorage.getItem('DbCon'),
      	"Keyword": this.searchServiceProviderText != '' ? this.searchServiceProviderText : '',
      	"LatLog":this.latLong,
      	"Type":serviceTypeName,
      	"UserId":sessionStorage.getItem('loginUserId'),
        "ServiceId":this.selectedServiceTypeId
      }
      this.isProviderDataCall = true;
      this.memberService.getNearbyServiceProvider(_input)
        .subscribe((res:Response)=>{
          this.isProviderDataCall = false;
          if(res['Status'] == 'Success'){
              this.nearByProvider= res['NearByServiceProvider'];
              this.othersProvider= res['OtherServiceProvider'];
              this.recentProvider= res['RecentServiceProvider'];
              this.recentAddedProvider = res['RecentlyAddedServiceProvider'];
              if(res['NearByServiceProvider'].length < 1 && res['OtherServiceProvider'].length < 1 && res['RecentServiceProvider'].length < 1){
                this.noProviderData = true;
              }
          }else{
              this.service.alertMessage(res['Message'],'error','');
          }
        },(error)=>{
          this.service.alertMessage(error,'error','');
        })
    }

    serviceProviderId: any = -1;
    cardLoadedAmount: any = 0;
    cardLoadSuccess: boolean = false;
    saveClaimDetail(){
      if(this.addClaimForm.valid){
        if(this.addClaimForm.value['isDepandent'] == 'yes'){
          if(this.addClaimForm.value['DependantId'] == ''){
            this.service.alertMessage("Please select dependant","error");
            return false;
          }
        }
      //  this.addClaimForm.value['ReceiptDate']=   this.addClaimForm.value['ReceiptDate'].formatted

        //------------------27-6-19
        if(this.selectedCalimType == 1){
          this.isLoading = true;
          let _input ={
            "ClaimAmount": parseFloat(this.addClaimForm.value.Amount).toFixed(2),
            "CompanyId":parseInt(sessionStorage.getItem('companyId')),
            "UserId":sessionStorage.getItem('loginUserId')
          }
         this.memberService.loadValue_New(_input)
           .subscribe((res:Response)=>{
              this.isLoading = false;
               if(res['Status'] == 'Success'){
                   this.claimId = res['ID'];
                   this.cardLoadedAmount = this.addClaimForm.value.Amount;
                   this.cardLoadSuccess = true;
                   this.isLoading = false;
                   var options = {
                       useEasing: true,
                       useGrouping: true,
                       separator: ',',
                       decimal: '.',
                       prefix: '$',
                   };
                   var demo = new CountUp('myTargetElement', 0, this.cardLoadedAmount, 0, 2.5, options);
                   if (!demo.error) {
                     demo.start();
                   } else {
                     console.error(demo.error);
                   }
                  this.addClaimForm.reset();
               }else{
                   this.service.alertMessage(res['Message'],'error','');
                   this.isLoading = false;
               }
           },(error)=>{
             this.service.alertMessage(error,'error','');
             this.isLoading = false;
           })
        }else{
          let _input ={
            "ClaimAmount": parseFloat(this.addClaimForm.value.Amount).toFixed(2),
            "CompanyId":parseInt(sessionStorage.getItem('companyId')),
            "CompanyUserId": sessionStorage.getItem('loginUserId'),
            "DbCon":this.addClaimForm.value.DbCon,
            "Dependant": this.addClaimForm.value.DependantId != '' ? parseInt(this.addClaimForm.value.DependantId) : -1,
            "ServiceId":  parseInt(this.selectedServiceTypeId),//For service //parseInt(this.addClaimForm.value.ServiceTypeId),
            "ServiceTypeId":this.selectedPlanService,  // Medical health, Dental, Wellness
            "UserId":parseInt(sessionStorage.getItem('loginUserId')),
            "Note":this.addClaimForm.value.Note,
            "IsManual": this.selectedCalimType == 1 ? false : true,
            "ServiceProvider": this.addClaimForm.value.ServiceProvider,
            "ServiceProviderId": this.serviceProviderId, //this.serviceProviderName,
            "ProviderAddress": this.addClaimForm.value.ProviderAddress, //this.serviceProviderName,
            "ProviderContactNo":this.addClaimForm.value.ProviderContactNo,
            "ProviderEmail":this.addClaimForm.value.ProviderEmail,
            "ReceiptDate": this.addClaimForm.value.ReceiptDate.formatted,//this.addClaimForm.value.ReceiptDate,
            "ReceiptNo": this.addClaimForm.value.ReceiptNo,
            "UploadFileName":  this.addClaimForm.value.UploadFileName,
            "GoogleUniqueId": this.uniqueId,
            "LoggedInUserId":sessionStorage.getItem('loginUserId')
          }
          this.isLoading = true;
          this.memberService.addClaim(_input)
            .subscribe((res:Response)=>{
                this.isLoading = false;
              if(res['Status'] == 'Success'){
                  this.claimId = res['ID'];
                  if(this.isOtherService == true && this.initialClaimType != this.selectedCalimType){
                    this.service.alertMessage("Your claim has been converted to a manual claim. It will take 5-7 business days to adjudicate and process before funds are transferred to your personal account", "success")
                  }
                  if(this.isOtherService == false){
                    this.service.alertMessage("Your calim has been submitted successfully. It will take  5-7 business days to adjudicate and process before funds are transferred to your personal account",'success','2000');
                  }
                  //this.cardLoadedAmount = this.addClaimForm.value.Amount;
                  //this.cardLoadSuccess = true;
                  this.addClaimForm.reset();
                  //this.goToStep(2);
                  this.router.navigate(['/member-portal/dashboard']);
              }else{
                  this.service.alertMessage(res['Message'],'error','');
                  this.isLoading = false;
                  //this.router.navigate(['/member-portal/claims']);
              }
            },(error)=>{
              this.service.alertMessage(error,'error','');
              this.isLoading = false;
            });
        }
      }
    }

    openModal(){
      this.modal = true;
    }

    uploadMsg = '';
    fileName: string= '';
    importLoaderModal: boolean;
    fileType: string = '';
    uploadFile(event, type) {
      this.uploadMsg = '';
      let file = event.target.files[0];
      let inputEl = event.target.files;
      //let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
      //get the total amount of files attached to the file input.
      let fileCount: number = inputEl.length;
      //create a new fromdata instance
      let formData = new FormData();
      if (fileCount > 0) { // a file was selected
        var strFileName = String(inputEl[0].name);
        if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
          this.uploadMsg = "Please wait receipt uploading…";
          formData.append('file', file);
          formData.append('CompanyId', sessionStorage.getItem('companyId'));
          this.service.uploadFile(formData)
          .subscribe((result)=>{
                this.uploadMsg = '';
                var respnose = result._body;
                if(respnose){
                    const img_name = respnose.split("###");
                    if(img_name[0] == 'Success'){
                        //this.fileName = this.fileName+"/"+img_name[1];
                        this.fileName = img_name[1];
                        this.fileType = img_name[1].split('.').pop().toLowerCase();
                        if(type != 'load_card_receipt'){
                          this.addClaimForm.patchValue({
                            'UploadFileName': img_name[1]
                          });
                          this.service.alertMessage("Receipt successfully uploaded","success");
                        }
                        // else{
                        //   let _input = {
                        //     "ClaimId":this.claimId,
                        //     "CompanyId":this.companyId,
                        //     "FileName":  this.fileName,
                        //     "LoggedInUserId":sessionStorage.getItem('loginUserId')
                        //   }
                        //   this.memberService.uploadReceipt(_input)
                        //   .subscribe((respnose:Response)=>{
                        //       if(respnose['Status'] == "Success"){
                        //           this.receiptModal = false;
                        //           this.router.navigate(['/member-portal/dashboard']);
                        //           this.service.alertMessage("Receipt successfully uploaded","success");
                        //       }else{
                        //         this.service.alertMessage(respnose['Message'], "error");
                        //         this.receiptModal = false;
                        //       }
                        //   })
                        // }

                    }else{
                      // this.service.alertMessage("Receipt upload encountered an error. Please try again.","error");
                      this.service.alertMessage(img_name[1],"error");
                    }
                }
              },
              (error) =>{  console.log(error);  });
          }else{
            this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
            return false;
          }
        }else{
            this.uploadMsg =" Please choose file";
        }
   }

   goToStep(pvarChoice){
     this.step=pvarChoice;
   }
   errorAmtModal:boolean = false;
   totalEnterdCliamAmount : any = 0;
   goToNextStep(pvarChoice){
     if(this.step==2){
       if(this.addClaimForm.value['Amount'] == ''){
         this.service.alertMessage("Please enter amount","error");
         return false;
       }
       if(parseInt(this.addClaimForm.value['Amount']) < 1){
         this.service.alertMessage("Please enter valid amount","error");
         return false;
       }

       if(this.applyClaimfee ==  true){
         this.totalEnterdCliamAmount = parseFloat(this.addClaimForm.value['Amount'])+ parseFloat(this.cpfAmount);
         if(parseFloat(this.totalEnterdCliamAmount) > this.availableBalance){
           //this.service.alertMessage("Enter sufficient amount. Your available balance is $"+this.availableBalance+"","error");
           this.errorAmtModal = true;
           this.addClaimForm.patchValue({
             "Amount": ''
             })
             return false;
           }
       }else{
          this.totalEnterdCliamAmount = parseFloat(this.addClaimForm.value['Amount']);
         if(parseFloat(this.addClaimForm.value['Amount']) > this.availableBalance){
           //this.service.alertMessage("Enter sufficient amount. Your available balance is $"+this.availableBalance+"","error");
           this.errorAmtModal = true;
           this.addClaimForm.patchValue({
             "Amount": ''
             })
             return false;
           }
       }

     }
     if(this.step==4){
       if(this.addClaimForm.value['ServiceTypeId'] == ''){
         this.service.alertMessage("Please select a service for which a claim was submitted","error");
         return false;
       }
     }
     if(this.step==5){
       if(this.addClaimForm.value['ServiceProvider'] == ''){
         this.service.alertMessage("Please select a service provider or add a new service provider","error");
         return false;
       }
       if(this.addClaimForm.controls.ProviderEmail.valid == false){
         this.service.alertMessage("Please enter valid email address","error");
         return false;
       }
     }

     if(this.step==6){
       if(this.selectedCalimType == '2'){
         if(this.addClaimForm.value['ReceiptDate'] == '' || this.addClaimForm.value['ReceiptDate'] == null ){
           this.service.alertMessage("Please select receipt date","error");
           return false;
         }
         if(this.addClaimForm.value['UploadFileName'] == ''){
           this.service.alertMessage("Please upload claim receipt","error");
           return false;
         }
       }
     }
     if(this.step==7){
       if(this.addClaimForm.value['isDepandent'] == ''){
         this.service.alertMessage("Select one: is this you or your dependant?","error");
         return false;
       }
     }
     if(this.step==8){
       if(this.addClaimForm.value['DependantId'] == ''){
         this.service.alertMessage("Please select any dependant","error");
         return false;
       }
     }
    if(this.step==5 && this.selectedCalimType == 1){
       this.goToStep(pvarChoice+2);
    }

    else if(this.step==7 && this.addClaimForm.value['isDepandent'] == 'no'){
      this.goToStep(pvarChoice+2);
    }else{
        this.isProviderInfoModel = false;
        this.goToStep(pvarChoice+1);
    }
  }

   goToPrevStep(pvarChoice){
      if(pvarChoice > 1){
        if(pvarChoice == 7  && this.selectedCalimType == 1){
          this.goToStep(pvarChoice-2);
        }
        else if(pvarChoice == 9  && this.addClaimForm.value['isDepandent'] == 'no'){
          if(this.selectedCalimType == 1){
              this.goToStep(pvarChoice-5);
          }else{
              this.goToStep(pvarChoice-2);
          }
        }
        else{
            if(pvarChoice == 9 && this.selectedCalimType == 1){
                this.goToStep(pvarChoice-6);
            }else{
              this.goToStep(pvarChoice-1);
            }
        }
      }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

   openAddProviderModal: boolean = false;
   otherProvider(pvarId){
     this.openAddProviderModal = true;
   }

   openAddServiceTypeModal : boolean = false;
   otherServiceType(pvarId){
     this.openAddServiceTypeModal = true;
   }

   isServiceCall: boolean = false;
   saveOtherProviderDetail(form){
     console.log(form.value)
     let _input = {
        "Address":form.value.address,
      	"CompanyId":sessionStorage.getItem('companyId'),
      	"ContactNo":form.value.contact,
      	"Email":form.value.email,
      	"LoggedInUserId":sessionStorage.getItem('loginUserId'),
      	"Name":form.value.name,
      	"ServiceId": this.selectedServiceTypeId //this.selectedPlanService
     }
     this.memberService.addServiceProvider(_input)
     .subscribe((res:Response)=>{
         this.isLoading = false;
         if(res['Status'] == 'Success'){
            this.serviceProviderName = form.value.name;
             this.addClaimForm.patchValue({
               "ServiceProvider": form.value.name, //form.value.name
               "ProviderEmail": form.value.email,
               "ProviderAddress": form.value.address,
               "ProviderContactNo": form.value.contact
             });
             this.uniqueId = -1;

             this.serviceProviderId = res['ID'];
             this.openAddProviderModal = false;
             this.service.alertMessage("Service provider successfully added",'success','2000');
             form.reset();
             this.getServiceProvider();
             if(this.selectedCalimType == 1){
              this.goToStep(this.step+2);
            }else{
              this.goToStep(this.step+1);
            }
         }else{
             this.service.alertMessage(res['Message'],'error','');
             this.isLoading = false;
         }
     },(error)=>{
       this.service.alertMessage(error,'error','');
       this.isLoading = false;
     })
   }

   isOtherService: boolean = false;
   saveOtherService(form){
     let _input = {
        "CRA":100,
       	"LoggedInUserId":sessionStorage.getItem('loginUserId'),
       	"NonCRA":0,
       	"ServiceTypeId":this.selectedPlanService,
       	"Title":form.value.serviceName,
        "IsApproved": false
     }
     this.memberService.addService(_input)
     .subscribe((res:Response)=>{
         this.isLoading = false;
         if(res['Status'] == 'Success'){
             this.openAddServiceTypeModal = false;

             this.selectedServiceTypeId =  res['ID'];
             this.addClaimForm.patchValue({
                 "ServiceProvider": '',
                 "ServiceTypeId":res['ID']
             })
             this.serviceProviderData = [];
             this.getServiceProvider();
             this.isOtherService = true;
             this.selectedCalimType = 2; //if set other services then set claim as manual
             this.service.alertMessage("Your service has been submitted for the approval of CRA and Non CRA coverd",'success','2000');
             this.getPlanServices();
             form.reset();
             this.goToStep(this.step+1);
         }else{
             this.service.alertMessage(res['Message'],'error','');
             this.isLoading = false;
         }
     },(error)=>{
       this.service.alertMessage(error,'error','');
       this.isLoading = false;
     })
   }

   setIsDependant(pvarTypeId){
     this.addClaimForm.patchValue({
       "isDepandent": pvarTypeId == 1 ? 'no' : 'yes',
       "DependantId": ''
     })
     this.selectdDependantId = -1;
     if(pvarTypeId == 1){
         this.goToStep(this.step+2);
     }else{
         this.goToStep(this.step+1);
     }
     //this.goToStep(7);
   }

   selectdDependantId: any = -1;
   setDependant(pvarData){
     this.selectdDependantId = pvarData['DependentId'];
     this.addClaimForm.patchValue({
       "DependantId": pvarData['DependentId']
     })
   }

   dependantType: string = '';
   dependantFName: string = '';
   dependantLName: string = '';
   dependantDob: any = '';
   dependantGender: string = '';
   dependantRelationship: any = '';

   otherDependant(pvarId){
     this.openAddDepandentModal = true;
   }
  saveDependantDetail(form){
      if(this.dependantDob == ''){
        this.service.alertMessage("Please select date of birth","error");
        return false;
      }
      if(form.valid){
         console.log(form.controls)
          let dob = '';
          let formDb = this.dependantDob;
          if(formDb.formatted){
             dob= formDb.formatted;
          }else{
            if(typeof  formDb === 'object'){
              let mm = parseInt(formDb.date.month) < 10 ? '0'+formDb.date.month:formDb.date.month;
              let dd = parseInt(formDb.date.day) < 10 ? '0'+formDb.date.day: formDb.date.day;
              dob= mm+"/"+ dd +"/"+formDb.date.year;
            }else{
              dob= formDb;
            }
          }
          const _input = {
         	 "DbCon":sessionStorage.getItem('DbCon'),
           "LoggedInUserId":sessionStorage.getItem('loginUserId'),
         	 "EmpDependentList":[{
           		"CompanyUserId":sessionStorage.getItem('loginUserId'),
           		"DOB": dob,
           		"FName":this.dependantFName,
           		"Gender":this.dependantGender,
           		"LName":this.dependantLName,
              "OtherRelation": this.dependantRelationship,
           		"RelationshipId":this.dependantType
           	}]
         }
         console.log(_input);
         this.memberService.addEmpDependents(_input)
         .subscribe((res:Response)=>{
           if(res['Status'] == 'Success'){
               this.isServiceCall = false;
               this.selectdDependantId = res['ID'];
               this.addClaimForm.patchValue({
                 "DependantId": res['ID']
               });
               form.reset();
               this.openAddDepandentModal = false;
               this.service.alertMessage("Dependent successfully added.",'error','');
               this.goToStep(this.step+1);
               this.getDependant();
               //this.goToStep(2);
              // this.setActiveTab(4);
           }else{
             this.isServiceCall = false;
             this.service.alertMessage(res['Message'],'error','')
           }
         },(error)=>{
           this.isServiceCall = false;
           this.service.alertMessage(error,'error','')
         })
      }
    }

  loadCardHover(){
    this.service.alertMessage("Your card cannot be loaded. It has either been blocked or is not yet activated. To resolve the issue, please contact eflex admin at 416-748-9879.","error");
    return false;
  }

  uploadClaimReceipt(){
    // this.cardLoadSuccess = false;
    // this.receiptModal = true;
    // this.el.nativeElement.querySelector('#lc_file').value= '';
    // this.el.nativeElement.value = '';
    this.router.navigate(['/member-portal/update-claim-detail/'+this.claimId]);
  }

  reciptModalClose(){
    this.cardLoadSuccess = false;
    this.receiptModal = false;
    this.el.nativeElement.querySelector('#lc_file').value= '';
    this.el.nativeElement.value = '';
    this.router.navigate(['/member-portal/dashboard']);
  }

  LC_ReceiptNo: any = '';
  LC_ReceiptDate: any = '';
  submitLoadCardReceipt(){
    if(this.LC_ReceiptDate == ''){
      this.service.alertMessage("Please choose receipt date","error");
      return false;
    }
    // if(this.LC_ReceiptNo == ''){
    //   this.service.alertMessage("Please enter receipt number","error");
    //   return false;
    // }
    if(this.fileName == ''){
      this.service.alertMessage("Please upload receipt","error");
      return false;
    }
    let _input = {
      "ClaimId":this.claimId,
      "CompanyId":this.companyId,
      "FileName":  this.fileName,
      "LoggedInUserId":sessionStorage.getItem('loginUserId'),
      "ReceiptDt":this.LC_ReceiptDate.formatted,
      "ReceiptNo":this.LC_ReceiptNo
    }
    this.memberService.uploadReceiptV2(_input)
    .subscribe((respnose:Response)=>{
        if(respnose['Status'] == "Success"){
            this.receiptModal = false;
            this.router.navigate(['/member-portal/dashboard']);
            this.service.alertMessage("Receipt successfully uploaded","success");
        }else{
          this.service.alertMessage(respnose['Message'], "error");
          this.receiptModal = false;
        }
    })
  }

}
