import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod'
@Component({
  selector: 'app-claim-type',
  templateUrl: './claim-type.component.html',
  styleUrls: ['./claim-type.component.css']
})
export class ClaimTypeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
