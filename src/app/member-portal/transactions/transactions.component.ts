import { Component, OnInit, Pipe,PipeTransform,ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {MemberService} from '../../member.service';
import {Router} from "@angular/router";
import {DatePipe} from '@angular/common';
import * as jsPDF from 'jspdf';
declare var $ :any;
import {CsvService} from 'angular2-json2csv';

import { utils, write, WorkBook } from 'xlsx';

import { saveAs } from 'file-saver';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  currentTransaction: any=[];
  isPending: boolean = true;
  periodData: any[];
  period: string = 'current';
  filterByType: number = -1
  searchString: string = '';
  contributionAmt: any = 0;
  withdrawalsAmt: any = 0;
  currentAmt: any= 0;

  @ViewChild('transactions') transactions:ElementRef
  constructor(private service: CompanyService, private router : Router,
     private CsvService: CsvService, private memberService: MemberService) { }

  ngOnInit() {
    if(sessionStorage.getItem("DbCon") != null){
        const _input = {
          "DbCon":sessionStorage.getItem("DbCon")
        }
        this.service.getStatementPeriod(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == "Success"){
            this.periodData = res['StatementPeriod'];
          }
      });
      this.getTransaction();
    }else{
      this.router.navigate(['/login']);
    }
  }

  onPeriodChange(){
    this.period  = this.period;
    this.currentTransaction = [];
    this.getTransaction();
  }

  onTypeChange(){
    this.filterByType = this.filterByType;
    this.currentTransaction = [];
    this.getTransaction();
  }

  searchEmpety(){
  if(this.searchString.length == 0){
      this.currentTransaction = [];
      this.getTransaction();
  }
}

chunk_Start: any = -1;
getTransaction(){
    var datePipe = new DatePipe("en-US");
    let periodDate = ''
    if(this.period == 'current'){
       periodDate = datePipe.transform(new Date().toDateString(), 'MM/dd/yyyy');
    }else{
      periodDate = datePipe.transform(new Date(this.period).toDateString(), 'MM/dd/yyyy');
    }
  //  this.chunk_Start = this.currentTransaction.length == 0 ? -1 : this.currentTransaction[this.currentTransaction.length -1]['CompanyId'];

    const _input={
      "ChunkSize":500,
      "ChunkStart":-1,
      "DbCon":sessionStorage.getItem("DbCon"),
      "Id": this.filterByType,
      "Period":periodDate,
      "SearchString":this.searchString,
      "UserId": sessionStorage.getItem('loginUserId')
    };
    console.log(_input);
    this.memberService.getEmpTransactionData(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        console.log(res);
        this.contributionAmt = res['AmountDetails']['Contributions'];
        this.withdrawalsAmt = res['AmountDetails']['Withdrawal'];
        this.currentAmt = res['AmountDetails']['CurrentBalance'];
        this.currentTransaction = res['EmpTransactionList'];
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })
  }


  transform(value){
    var s=value;
    value=s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    return value;
  }

  downloadCSV(){
      var i=1;
      var newArr = [];
      this.currentTransaction.forEach((res)=>{
        let obj = {};
        obj['Sr.no'] = i++;
        obj['Transaction Date'] = this.transform(res['TransactionDate']);
        obj['Description'] = res['Description'];
        obj['Type'] = res['TypeId'];
        obj['Withdrawals/Claims'] = res['ContributionType'] == 'WDR' ? res['Amount'] : '0';
        obj['Contributions'] = res['ContributionType'] != 'WDR' ? res['Amount'] : '0';
        newArr.push(obj);
      })

      const ws_name = 'transactions';
      const wb: WorkBook = { SheetNames: [], Sheets: {} };
      const ws: any = utils.json_to_sheet(newArr);
      wb.SheetNames.push(ws_name);
      wb.Sheets[ws_name] = ws;
      const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

      function s2ab(s) {
        const buf = new ArrayBuffer(s.length);
        const view = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
          view[i] = s.charCodeAt(i) & 0xFF;
        };
        return buf;
      }
      saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), 'transactions.xlsx');

  }

  sortingName: string;
  isDesc: boolean;
  sort(name: string): void {
    if (name && this.sortingName !== name) {
      this.isDesc = false;
    } else {
      this.isDesc = !this.isDesc;
    }
    this.sortingName = name;
  }


}
