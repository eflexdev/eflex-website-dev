import { Component, OnInit, Pipe,PipeTransform,ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {Router, ActivatedRoute} from "@angular/router";
import {DatePipe} from '@angular/common';
import * as jsPDF from 'jspdf';
declare var $ :any;
import {CsvService} from 'angular2-json2csv';
import {environment} from '../../../environments/environment.prod'
import { utils, write, WorkBook } from 'xlsx';
import {IMyDpOptions} from 'mydatepicker';
import { saveAs } from 'file-saver';
import {MemberService} from '../../member.service';

@Component({
  selector: 'app-card-transactions',
  templateUrl: './card-transactions.component.html',
  styleUrls: ['./card-transactions.component.css']
})
export class CardTransactionsComponent implements OnInit {
      d:any = new Date();
      isPending: boolean = true;
      fromDate: any = ''; //{ date: { day: this.d.getDate()+1, month: this.d.getMonth()+1,  year: this.d.getFullYear() } }
      companyId: any = ''
      hostUrl: string = '';
      transactionsData: any = [];
      isServiceCall :boolean =false;
      cardId: any = -1;
      transType: any = 3;
      noOfDays: any = 30;

      @ViewChild('transactions') transactions:ElementRef
      constructor(private service: CompanyService, private router : Router,
      private CsvService: CsvService, private memberService: MemberService,
      private acitvateRoute: ActivatedRoute ) {
          this.hostUrl = environment.hostUrl;
          this.companyId = sessionStorage.getItem("companyId");
       }


      public myDatePickerOptions: IMyDpOptions = {
          //dateFormat: 'mm/dd/yyyy',
          editableDateField: false,
          dateFormat: 'dd/mm/yyyy'
         //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1},
         //disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+2, day: this.d.getDate()+1}
     };

      ngOnInit() {
        //this.fromDate = { date: { year: this.d.getFullYear(), month: month: this.d.getMonth()+1, day:  this.d.getDate()-1 } };
        this.acitvateRoute.params.subscribe((param)=>{
          this.cardId = param['id'];
        })
        let mm: any = parseInt(this.d.getMonth()+1) < 10 ? '0' + parseInt(this.d.getMonth()+1)  : parseInt(this.d.getMonth()+1) ;
        let dd = this.d.getDate() < 10 ? '0' + this.d.getDate(): this.d.getDate();
        this.fromDate =  {
            date: { year: this.d.getFullYear(), month:   this.d.getMonth() + 1, day:this.d.getDate() },
            formatted:dd +"/"+ mm +"/"+ this.d.getFullYear()
        }
        this.getTransactionByType();
      }

      getTransactionByType(){
        if(this.fromDate == "" || this.fromDate == null){
          this.service.alertMessage('Please select "transition from" date',"error");
          return false;
        }
        this.transactionsData = [];
        let toDate = '';
        if(this.fromDate){
           toDate = this.fromDate.formatted;
        }
        let _input={
            "CardId":this.cardId,
            "TransType": this.transType,
            "Days":this.noOfDays,
            "StartDate": toDate//this.fromDate
          }
        this.isServiceCall = true;
        this.memberService.listCardTransactions(_input)
        .subscribe((response:Response)=>{
          if(response['Status'] == "Success"){
              this.isServiceCall = false;
              this.transactionsData = response['ListCardTransactions'];
          }else{
            this.isServiceCall = false;
            this.service.alertMessage(response['Message'],"error");
          }
        },(error:Error)=>{
          this.isServiceCall = false;
          this.service.alertMessage(error,"error");
        })
      }

      transform(value){
        var s=value;
        value=s.replace("AM"," AM")
        var datePipe = new DatePipe("en-US");
        //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
        value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
        return value;
      }

      downloadCSV(){
          var i=1;
          var newArr = [];
          this.transactionsData.forEach((res)=>{
            let obj = {};
            obj['Sr.No'] = i++;
            obj['Reference'] = res['Reference'];
            obj['Merchant Name'] = res['MerchantName'];
            obj['Settlement Date'] = res['SettlementDate'];
            obj['Transaction Amount'] = res['TransactionAmount'];
            obj['Transaction Code'] = res['TransactionCode'];
            obj['Transaction Date'] = res['TransactionDate'];
            obj['Transaction Description'] = res['TransactionDescription'];
            newArr.push(obj);
          })

          const ws_name = 'card_transactions';
          const wb: WorkBook = { SheetNames: [], Sheets: {} };
          const ws: any = utils.json_to_sheet(newArr);
          wb.SheetNames.push(ws_name);
          wb.Sheets[ws_name] = ws;
          const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

          function s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i !== s.length; ++i) {
              view[i] = s.charCodeAt(i) & 0xFF;
            };
            return buf;
          }
          saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), 'card_transactions.xlsx');
      }

      sortingName: string;
      isDesc: boolean;
      sort(name: string): void {
        if (name && this.sortingName !== name) {
          this.isDesc = false;
        } else {
          this.isDesc = !this.isDesc;
        }
        this.sortingName = name;
      }

}
