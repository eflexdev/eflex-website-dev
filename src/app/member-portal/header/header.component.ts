import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CompanyService} from '../../company.service';
import {MemberService} from '../../member.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName: string;
  companyCode:string;
  UnreadNotificationCount: number = 0;
  MessageCount:number= 0;
  toggleMenuClass: boolean = false;
  isCardActive: boolean = true;
  masterCardId: any = -1;
  myAccessCode: any = '';
  isCard: boolean = false;

  isMsgShown : boolean = false;
  constructor(private router: Router, private service: CompanyService, private memberService: MemberService) {

    const ab = this.service.getEmittedValue()
    .subscribe((response: Response) =>{
       this.getMessgeCount()
     });
    this.getMessgeCount();
   }

  ngOnInit() {
      this.userName = sessionStorage.getItem("userName");
      this.companyCode = sessionStorage.getItem("uniqueCode");
      this.getEmpCard();
  }
  toggleMenu(){
    this.toggleMenuClass = !this.toggleMenuClass;
  }

  getMessgeCount(){

    if(sessionStorage.getItem('companyId') != undefined){
      const _input ={
        "CompanyId":sessionStorage.getItem('companyId'),
        "DbCon":sessionStorage.getItem('DbCon'),
        "UserId":sessionStorage.getItem('loginUserId'),
        "UserTypeId":sessionStorage.getItem('userTypeId')
      }

      this.service.getNotiMsgCount(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.userName = sessionStorage.getItem("userName");
          this.MessageCount = res['MessageCount'];
          this.UnreadNotificationCount = res['UnreadNotificationCount'];
          this.isCardActive = res['IsCardActive'];
          this.myAccessCode = res['AccessCode'];
          sessionStorage.setItem('isCardActive',res['IsCardActive']);
          this.masterCardId = res['MasterCardId'];
          if(res['IsLogOut'] == true){
              this.service.alertMessage('You are logout from here because your account login in another device','error');
              this.logOut();
          }
          setTimeout(()=>{
            this.getMessgeCount();
            this.getEmpCard();
            this.isCard = false;
          },5000)
        }else{
          if(res['IsLogOut'] == true){

              if(this.isMsgShown == false){
                this.isMsgShown = true;
                this.service.alertMessage(res['Message'],"error");
              }
              this.logOut();
          }

        }
      },function (parameter) {
          throw new Error("Not implemented yet");
      })
    }
  }
  logOut(){
    sessionStorage.clear();
    //this.service.alertMessage("You are logout successfully","success");
    this.router.navigate(['/join-us']);

  }

  //active card
  openCardActivateModal: boolean = false;
  isServiceCall: boolean = false;
  cardNumber: any = '';
  expireYear: any = '';
  expireMonth: any = '';
  CVV: any = '';
  accessCode: any = '';

  openCardModal(){
    this.openCardActivateModal = true;
    this.accessCode = this.myAccessCode;
  }

  activateCard(form){
    if(form.valid){
      let _input = {
        "CompanyId":sessionStorage.getItem('companyId'),
        "DbCon":"DbCon_"+sessionStorage.getItem('companyId'),
        "MasterCardId":this.masterCardId,
        "Status":'Activate',
        "UserId":sessionStorage.getItem('loginUserId'),
        "CardNumber": form.value.cardNumber,
        "CVV": form.value.CVV,
        "AccessCode": form.value.accessCode,
        //"ExpYear": form.value.expireYear,
        //"ExpMonth": form.value.expireMonth,
      }
      console.log(_input);
      this.isServiceCall = true;
      this.memberService.activateCardStatus(_input)
      .subscribe((response:Response)=>{
        this.isServiceCall = false;
        if(response['Status'] == "Success"){
            form.reset();
            this.openCardActivateModal = false;
            this.isCardActive = true;
            this.getEmpCard();
            this.service.alertMessage("Your card has been successfully activated",'error');
        }else{
          this.service.alertMessage(response['Message'],'error');
        }
      },(error)=>{
          this.isServiceCall = false;
          this.service.alertMessage(error['Message'],'error');
      })
    }else{
      this.service.alertMessage("Plase complete all required fields",'error');
      return false;
    }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }
 toggleCardNav(){
   this.isCard = !this.isCard
 }

 gotoTransaction(pvarData){
      this.isCard = !this.isCard;
      this.router.navigate(['/member-portal/card-transactions/'+pvarData['MasterCardId']]);
 }

 cardData: any=[];

 getEmpCard(){

   if(sessionStorage.getItem('companyId') != null){
     let _input = {
    	"DbCon":"DbCon_"+sessionStorage.getItem('companyId'),
    	"Id":sessionStorage.getItem('loginUserId'),
    	"LoggedInUserId":sessionStorage.getItem('loginUserId')
     }
     this.memberService.getEmpCardDetails(_input)
     .subscribe((response:Response)=>{
       if(response['Status'] == "Success"){
          this.cardData = response['CardData'];
       }else{
         if(response['IsLogOut'] == true){
             if(this.isMsgShown == false){
               this.isMsgShown = true;
               //this.service.alertMessage(response['Message'],'error');
             }
             this.logOut();
         }

       }
     },(error)=>{
         this.service.alertMessage(error['Message'],'error');
     })
   }
 }

 togglePassword(pvarType){
   if(pvarType == 'acc'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'cvv'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'ac'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
 }

 hideSideBar(){
   this.toggleMenuClass = false;
 }

}
