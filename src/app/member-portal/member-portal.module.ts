import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule, ReactiveFormsModule }        from '@angular/forms';
import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import { MemberPortalRoutingModule } from './member-portal-routing.module';
import { MemberPortalComponent } from '../member-portal/member-portal.component';
//import { SummaryComponent } from './summary/summary.component'
import { TextMaskModule } from 'angular2-text-mask';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MyDate,  DateTime,  FilterUserPipe, FilterServicePipe, FilterUniquePipe, TwoDecimalPlacesDirective, OrderByPipe} from "./date-pipe";
import { MyDatePickerModule } from 'mydatepicker';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ClaimsComponent } from './claims/claims.component';
import { AccountComponent } from './account/account.component';
import { MessagesComponent } from './messages/messages.component';
import { AddClaimComponent } from './add-claim/add-claim.component';
import { NotificationComponent } from './notification/notification.component';
import { ClaimTypeComponent } from './claim-type/claim-type.component';
import {EmojiPickerModule} from 'ng-emoji-picker';
import { CardTransactionsComponent } from './card-transactions/card-transactions.component';
import { UpdateClaimDetailComponent } from './update-claim-detail/update-claim-detail.component';

//import { CountUpModule } from 'countup.js-angular2';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { DxSelectBoxModule, DxListModule, DxTemplateModule } from 'devextreme-angular';
// import DataSource from 'devextreme/data/data_source';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MemberPortalRoutingModule,
    FormsModule,
    ChartModule,
    TextMaskModule,
    InfiniteScrollModule,
    //TabsModule.forRoot(),
    ReactiveFormsModule,
    MyDatePickerModule,
    EmojiPickerModule
    //CountUpModule,
    //SettingeModule
    // DxSelectBoxModule,
    // DxListModule,
    // DxTemplateModule
  ],
  declarations: [
    MemberPortalComponent,
    HeaderComponent,
    FooterComponent,
    MyDate,  DateTime,TwoDecimalPlacesDirective,OrderByPipe,
    FilterUserPipe,
    FilterServicePipe,
    FilterUniquePipe,
    DashboardComponent,
    TransactionsComponent,
    ClaimsComponent,
    AccountComponent,
    MessagesComponent,
    AddClaimComponent,
    NotificationComponent,
    ClaimTypeComponent,
    CardTransactionsComponent,
    UpdateClaimDetailComponent,
    //ContactInfoComponent
  ],
  providers: [  ]
})
export class MemberPortalModule { }
