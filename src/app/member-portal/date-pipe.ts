import {Pipe,PipeTransform,Directive, ElementRef, HostListener} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  'name':'mydate'
})
export class MyDate implements PipeTransform{
  transform(value){
    if(value){
      var s=value;
      //value=s.replace("AM"," AM");
      let am= s.includes("AM");
      let pm= s.includes("PM");
      if(am == true){
        var AMPM = value.slice(-2);
        value=s.replace(AMPM," "+AMPM);
      }
      if(pm == true){
        var AMPM = value.slice(-2);
        value=s.replace(AMPM," "+AMPM);
      }
      //console.log(value);
      var datePipe = new DatePipe("en-US");
      //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      let d = new Date(value+" UTC");
      d.toLocaleString().replace(/GMT.*/g,"");
      value = datePipe.transform(d, 'MMM d, y');
      return value;
    }
  }
}
@Pipe({
  'name':'datetime'
})
export class DateTime implements PipeTransform{
  transform(value){
    if(value){
      var s= value;

      let am= s.includes("AM");
      let pm= s.includes("PM");
      if(am == true){
        var AMPM = value.slice(-2);
        value=s.replace(AMPM," "+AMPM);
      }
      if(pm == true){
        var AMPM = value.slice(-2);
        value=s.replace(AMPM," "+AMPM);
      }
      var datePipe = new DatePipe("en-US");
      let d = new Date(value+" UTC");
      d.toLocaleString().replace(/GMT.*/g,"");
      value = datePipe.transform(d, 'MMM d, y hh:mm a');
     return value;
    }
  }
}

@Pipe({
  name: 'filterUser'
})

export class FilterUserPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
    searchText = searchText.toLowerCase();

    return items.filter( it => {
      return it['Name'].toLowerCase().includes(searchText);
    });
   }
}

@Pipe({
  name: 'filterService'
})

export class FilterServicePipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
    searchText = searchText.toLowerCase();

    return items.filter( it => {
      return it['Title'].toLowerCase().includes(searchText);
    });
   }
}


@Pipe({
  name: 'filterUnique',
  pure: false
})
export class FilterUniquePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    // Remove the duplicate elements
    // let uniqueArray = value.filter(function (el, index, array) {
    //   return array.indexOf(el) == index;
    // });
    let uniqueArray = Array.from(new Set(value));
    return uniqueArray;
  }
}


  @Directive({
    selector: '[twoDecimalPlaces]'
  })
  export class TwoDecimalPlacesDirective {
   // Allow decimal numbers and negative values
    private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
   //private regex: RegExp = new RegExp(/^[0-9]\d{0,9}(\.\d{1,3})?%?$/g);
   // Allow key codes for special events. Reflect :
   // Backspace, tab, end, home
   private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', '-' ];

   constructor(private el: ElementRef) { }

   @HostListener('keydown', [ '$event' ])

   onKeyDown(event: KeyboardEvent) {
     // Allow Backspace, tab, end, and home keys
     if (this.specialKeys.indexOf(event.key) !== -1) {
          return;
     }
     let current: string = this.el.nativeElement.value;
     let next: string = current.concat(event.key);

      if (next && !String(next).match(this.regex)) {
          event.preventDefault();
      }
   }
  }


  //----------------------------
  @Pipe({
    name: 'orderBy'
  })
  export class OrderByPipe implements PipeTransform {
      transform( array: Array<any>, orderField: string, orderType: boolean ): Array<string> {
      if (!Array.isArray(array) || array.length <= 0) {
        return null;
      }
      array.sort( ( a: any, b: any ) => {
          let ae = a[ orderField ];
          let be = b[ orderField ];
          if ( ae == undefined && be == undefined ) return 0;
          if ( ae == undefined && be != undefined ) return orderType ? 1 : -1;
          if ( ae != undefined && be == undefined ) return orderType ? -1 : 1;
          if ( ae == be ) return 0;
          if(typeof ae == "string"){
            return orderType ? (ae.toString().toLowerCase() > be.toString().toLowerCase() ? -1 : 1) : (be.toString().toLowerCase() > ae.toString().toLowerCase() ? -1 : 1);
          }else{
            return orderType ? (ae > be ? -1 : 1) : (be > ae ? -1 : 1);
          }

      } );
      return array;
    }
}
