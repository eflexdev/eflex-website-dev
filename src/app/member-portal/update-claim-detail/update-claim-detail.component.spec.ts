import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClaimDetailComponent } from './update-claim-detail.component';

describe('UpdateClaimDetailComponent', () => {
  let component: UpdateClaimDetailComponent;
  let fixture: ComponentFixture<UpdateClaimDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClaimDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClaimDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
