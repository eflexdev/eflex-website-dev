import { Component, OnInit, HostListener } from '@angular/core';
import {Router, NavigationEnd }  from "@angular/router";
import { Idle } from 'idlejs/dist';
@Component({
  selector: 'app-member-portal',
  templateUrl: './member-portal.component.html',
  styleUrls: ['./member-portal.component.css']
})
export class MemberPortalComponent implements OnInit {

  constructor( private router: Router) {   // with predefined events on `document`

    this.getScreenSize()

    const idle = new Idle()
    .whenNotInteractive()
    .within(10)
    .do(() => //console.log('time out')
        this.sessionTimeOut()
    )
    .start();
 }

 sessionTimeOut(){
     //this.service.alertMessage("Session has been timeout","success");
     sessionStorage.clear();
     sessionStorage.removeItem('loginUserId');
     sessionStorage.removeItem('UserToken');
     this.router.navigate(['/']);
 }


  ngOnInit() {
      this.router.events.subscribe((evt) => {
         if (!(evt instanceof NavigationEnd)) {
             return;
         }
         window.scrollTo(0, 0)
     });
  }

  screenHeight: any;
  minusHeight: any = 0;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 10;
        }else{
          this.minusHeight = 300;
        }
        this.screenHeight = window.innerHeight;
    }

}
