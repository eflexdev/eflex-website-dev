import { Component, OnInit, ElementRef } from '@angular/core';
import {CompanyService} from '../../company.service';
import {DatePipe} from "@angular/common";
import {IMyDpOptions} from 'mydatepicker';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { Router }              from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {MemberService} from '../../member.service';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  activeTab: any = 1;
  companyId: any = -1;

  planData: any = [];
  RSTax:number = 0
  PPTax:number= 0;
  HST: number =0;
  AdminFeePct: number =0;
  GST: number =0;
  planType:any;


  NetGrossInitial: any;
  NetGrossRecurring: any;
  planForm: FormGroup;
  addPlanForm: FormGroup;
  ClassName:any= [];
  //contact
  provienceData:any;
  city: string = '';
  state : string = '';
  address : string = '';
  street : string = '';
  companyName: string='';
  zip:string='';
  companyCode:string='';
  planTypeId: any = '';

  firstName:string='';
  lastName:string='';
  loginEmail:string='';
  loginConfirmEmail:string='';
  pwd: string='';
  confirmPwd:string='';
  email: string = '';
  phone: string='';
  userName: string= '';
  dob:any;
  gender:string = 'Male';
  //changePwd
  cploading: boolean;
  oldPwd:string = ''
  newPwd: string= '';
  newCPwd: string= '';

  //PAP
  bankname:any;instId: any;
  transId: any;
  accountNumber: any;
  iagree:boolean;
  btnDisable: boolean= false;
  uploadingFile:boolean;
  fileName: string = '';
  hostUrl: string;
  modal:boolean;
  PAPDetailId: any = -1;
  planModal: boolean;
  openConditionModal: boolean = false;
  sms: boolean = false;
  cell_phone: string='';
  carriers: string = '';
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     editableDateField: false,
     //dateFormat: 'yyyy/mm/dd',
     //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
     //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1},
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
  };

  // data-ng-pattern="/^[a-zA-Z0-9\u00C0-\u017F-'.\s]+$/"
  constructor( private service: CompanyService, private router: Router, private el: ElementRef,
    private fb:FormBuilder, private memberService: MemberService) {
      this.hostUrl = environment.hostUrl;
      this.getCarrier();
   }

  ngOnInit() {
    if(sessionStorage.getItem('companyId') != null){
      this.companyId = sessionStorage.getItem('companyId');
    }else{
      this.service.alertMessage("Please login first","error");
      this.router.navigate(['/']);
    }
    this.service.populateProvince()
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.provienceData = res['ProvinceList'];
        this.getCompanyDetails()
      }else{
        console.log(res['Message']);
      }
    },(error)=>{
      //this.service.alertMessage(error,'error','')
    })
  }

  carrierData: any = [];
  getCarrier(){
    this.service.populateCarrier()
    .subscribe((response:Response)=>{
      if(response['Status'] == 'Success'){
        this.carrierData = response['PopulateCarrierData'];
      }else{
        this.service.alertMessage(response['Message'],'error');
      }
    })
  }

  getCompanyDetails(){
      if(sessionStorage.getItem("loginUserId") != null){
        const _input={
          "DbCon": sessionStorage.getItem("DbCon"),
          "Id": sessionStorage.getItem("loginUserId")
        };
        this.planData = [];
        this.isLoading = true;
        this.memberService.getEmpDetailsById(_input)
        .subscribe((res:Response)=>{
          this.isLoading = false;
          let obj = res['GetEmpBasicDetails'];
          // this.companyName = obj['CompanyName'];
          //this.companyCode = obj['Code'];
          this.address = obj['Address'];
          this.street = obj['Address2'];
          this.city = obj['City'];
          this.state = obj['Province'] ? obj['Province'] : '';
          this.zip = obj['PostalCode'];
          this.gender = obj['Gender'];
          if(obj['DOB']){
              this.dob = this.transformDate(obj['DOB']);
          }

        //  this.planTypeId = obj['PlanTypeId'];
          this.firstName = obj['FirstName'];
          this.lastName = obj['LastName'];
          this.email = obj['EmailAddress'];
          this.phone = obj['MobileNo'];
          let u = obj['UserName'].split("_")
          this.userName = u['1'];
          this.cell_phone =  obj['MobileNo'];
          //notification
          if(obj['CellNotificationDetails'] != null && obj['CellNotificationDetails'] !=''){
            this.sms = obj['CellNotificationDetails']['IsNotify'] == "False" ? false : true ; //obj['CellNotificationDetails'] ? true : false;
            this.cell_phone =  obj['CellNotificationDetails']['ContactNo'];
            this.carriers =  obj['CellNotificationDetails']['CarrierName']
          }
        })
      }else{
        this.router.navigate(['/']);
      }
  }
  //loading: boolean = false;
  updateBasicDetail(form){
      if(form.valid){
        let dob = '';
        if(form.controls.dob.value.formatted){
           dob= form.controls.dob.value.formatted;
        }else{
          if(typeof  form.controls.dob.value === 'object'){
            let mm = parseInt(form.controls.dob.value.date.month) < 10 ? '0'+form.controls.dob.value.date.month:form.controls.dob.value.date.month;
            let dd = parseInt(form.controls.dob.value.date.day) < 10 ? '0'+form.controls.dob.value.date.day: form.controls.dob.value.date.day;
            dob= mm+"/"+ dd +"/"+form.controls.dob.value.date.year;
          }else{
            dob= form.controls.dob.value;
          }
        }
        const _input = {
        	"Address":form.controls.address.value,
        	"Address2":"",
          "City":form.controls.city.value,
        	"CompanyUserId": parseInt(sessionStorage.getItem('loginUserId')),
          "DOB":dob,
          "DbCon":sessionStorage.getItem("DbCon"),
        	"EmailAddress":form.controls.email.value,
        	"EveningPhone":"",
          "FirstName":form.controls.firstName.value,
        	"Gender":form.controls.gender.value,
          "LastName":form.controls.lastName.value,
          "MobileNo":form.controls.cell_phone.value,
        	"MorningPhone":"",
        	"PostalCode":form.controls.zip.value,
        	"ProvinceId":form.controls.state.value,
          "Carrier":form.controls.carriers.value,
	        "Province":form.controls.state.value,
          "IsNotify": form.controls.sms.value,
          "Sin":-1
        };
        console.log(_input);
        this.loading = true;
        this.memberService.updateEmpBasicDetails(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.loading = false;
            sessionStorage.setItem("loginUserName", form.controls.firstName.value);
            sessionStorage.setItem("userName", form.controls.firstName.value);
            this.service.alertMessage("Detail update successfully","success");
          }else{
            this.loading = false;
            this.service.alertMessage(res['Message'],"error");
          }
        });
    }else{
      this.loading = false;
      this.service.alertMessage("Plase complete all required fields","error");
      return false;
    }
  }

  setActiveTab(pvarType){
    this.activeTab = pvarType;
    if(pvarType == '2'){

    }
    if(pvarType == '4'){
      this.getDependents();
    }
    if(pvarType == '5'){
      this.getPaymentDetail();
    }
  }
  menuOption: any = 1;
  setActiveTabOnChange(){
    this.activeTab = this.menuOption;
    if(this.menuOption == '2'){

    }
    if(this.menuOption == '4'){
      this.getDependents();
    }
  }

  //cpwed
  changePwd(form){
    if(form.valid){
      if(form.controls.newPwd.value !=  form.controls.newCPwd.value){
        return false;
      }
      const _input = {
        "CompanyId":sessionStorage.getItem('companyId'),
        "DbCon":sessionStorage.getItem("DbCon"),
        "NewPassword":form.controls.newPwd.value,
        "PrevPassword":form.controls.oldPwd.value,
        "UserId":sessionStorage.getItem("loginUserId")
      }
      this.cploading = true;
      this.service.changePassword(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.cploading = false;
            form.reset();
            this.service.alertMessage("Password successfully changed",'error','');
        }else{
          this.cploading = false;
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.cploading = false;
        this.service.alertMessage(error,'error','')
      })
    }else{
      return false;
    }
  }


  isLoading:boolean;
  updateBtnId: any = -1;
  disabled: boolean = true;
  // Calling this disables the component
   onDisableComponent() {
       this.disabled = true;
   }
  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
  }

  openModal(){
   this.modal = true;
  }
  removeFile(){
    this.fileName = '';
    this.el.nativeElement.querySelector('#file').value= '';
    this.el.nativeElement.value = '';
  }
  closeModal(){
    this.modal = false;
    this.removeFile();
  }

  uploadMsg = '';
  uploadFile(event) {
       this.uploadMsg = '';
       let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
       //get the total amount of files attached to the file input.
       let fileCount: number = inputEl.files.length;
       //create a new fromdata instance
       let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('CompanyId', this.companyId);
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.modal = false;
                        this.fileName = img_name[1];
                         // this.profileForm.patchValue({
                         //   'Img': img_name[1]
                         // });
                         this.service.alertMessage("Void cheque successfully upload","success");
                     }else{
                       this.service.alertMessage("Void cheque upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg ="Please select the file for your void cheque";
         }
  }

  loading: boolean;


  transformDateForFlatpickr(value){
  var datePipe = new DatePipe("en-US");
  value = datePipe.transform(value, 'yyyy-MM-dd');
  return value;
  }

  transformDate(value){
    var s=value;
    value=s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
    let date = new Date(value);
     const cdate= {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
      };
    let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
    return dt;
  }

  step: number = 2;
  goToStep(pvarChoice){
    this.step=pvarChoice;
  }

  dependantType:string = '';
  dependentFq: string = '';
  dependantDob: any = '';
  dependantGender:string = '';
  dependantFName:string= '';
  dependantLName:string= '';
  dependantRelationship: string = '';
  goToNextStep(pvarChoice){
     if(this.step==2){
        if(this.dependantType == ''){
         this.service.alertMessage("Please select dependant type","error");
         return false;
       }
      //  if(this.dependantType == '2' || this.dependantType == '3'){
      //    if(this.dependentFq == ''){
      //      this.service.alertMessage("Enter no of frequency","error");
      //      return false;
      //    }
      // }

        if(this.dependantType == '4'){
          if(this.dependantRelationship == ''){
            this.service.alertMessage("Please enter relationship","error");
            return false;
          }
       }else{
         this.dependantRelationship = '';
       }
     }

     if(this.step==3){
       if(this.dependantFName == ''){
        this.service.alertMessage("Please enter first name","error");
        return false;
      }
     }

     if(pvarChoice < 5){
       this.goToStep(pvarChoice+1);
     }
 }

  goToPrevStep(pvarChoice){
     if(pvarChoice > 2){
       this.goToStep(pvarChoice-1);
     }
 }

 newDependant(id){
   this.activeTab = 3;
   this.dependantType = '';
   this.step = 2;
   this.goToNextStep(2)
 }

 dependantData: any = [];
 deleteConfirmation:boolean;
 getDependents(){
   let _input ={
    	"ChunkSize":500,
    	"ChunkStart":-1,
    	"DbCon":sessionStorage.getItem('DbCon'),
    	"Id":sessionStorage.getItem('loginUserId'),
    	"SearchString":""
    }
   this.memberService.listOfDependents(_input)
   .subscribe((res:Response)=>{
     if(res['Status'] == 'Success'){
        this.dependantData= res['ListOfDependents'];
     }else{
       this.cploading = false;
       this.service.alertMessage(res['Message'],'error','')
     }
   },(error)=>{
     this.cploading = false;
     this.service.alertMessage(error,'error','')
   })
 }

 selectedIndex:any;
 name:string;
 deleteDependant(obj){
     this.loading = false;
     this.selectedIndex = obj;
     this.deleteConfirmation = true;
     this.name  = obj['FName'];
 }

 yesDelete(){
   this.loading = true;
   console.log(this.selectedIndex);
   const _input = {
     "Id":this.selectedIndex['DependentId'],
     "DbCon": sessionStorage.getItem('DbCon')
   }
   console.log(_input);
   this.memberService.deleteDependents(_input)
   .subscribe((res:Response)=>{
     if(res['Status'] == "Success"){
       this.deleteConfirmation = false;
       this.loading = false;
       let index = this.dependantData.lastIndexOf(this.selectedIndex);
       this.dependantData.splice(index, 1);
       this.service.alertMessage("Dependent successfully deleted","error");
     }else{
          this.loading = false;
          this.service.alertMessage(res['Message'],'error');
     }
   }, (error)=>{
      this.loading = false;
      this.service.alertMessage(error,'error');
   })
 }

 addDependent(form, isAddMore){
   if(this.dependantDob == ''){
     this.service.alertMessage("Please select date of birth","error");
     return false;
   }
   if(form.valid){
       let dob = '';
       let formDb = this.dependantDob;
       if(formDb.formatted){
          dob= formDb.formatted;
       }else{
         if(typeof  formDb === 'object'){
           let mm = parseInt(formDb.date.month) < 10 ? '0'+formDb.date.month:formDb.date.month;
           let dd = parseInt(formDb.date.day) < 10 ? '0'+formDb.date.day: formDb.date.day;
           dob= mm+"/"+ dd +"/"+formDb.date.year;
         }else{
           dob= formDb;
         }
       }
       const _input = {
      	"DbCon":sessionStorage.getItem('DbCon'),
        "LoggedInUserId":sessionStorage.getItem('loginUserId'),
      	"EmpDependentList":[{
      		"CompanyUserId":sessionStorage.getItem('loginUserId'),
      		"DOB": dob,
      		"FName":this.dependantFName,
      		"Gender":this.dependantGender,
      		"LName":this.dependantLName,
          "OtherRelation": this.dependantRelationship,
      		"RelationshipId":this.dependantType
      	}]
      }
      console.log(_input);
      this.loading = true;
      this.memberService.addEmpDependents(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.loading = false;
            this.cploading = false;
            this.dependantType = ''
            this.dependentFq = '';
            this.dependantDob = '';
            this.dependantGender = '';
            this.dependantFName = '';
            this.dependantLName = '';
            this.dependantRelationship ='';
            form.reset();

            if(isAddMore == true){
              this.newDependant(3);
            }else{
              //this.goToStep(2);
              this.service.alertMessage("Dependent successfully added.",'error','');
              this.setActiveTab(4);
            }

        }else{
          this.loading = false;
          this.cploading = false;
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.cploading = false;
        this.loading = false;
        this.service.alertMessage(error,'error','')
      })
   }
 }

 getPaymentDetail(){
   if(sessionStorage.getItem("DbCon") != null){
       const _input = {
         "DbCon":sessionStorage.getItem("DbCon"),
         "Id": sessionStorage.getItem("loginUserId")
       }
       this.service.getPAPDetails(_input)
       .subscribe((res:Response)=>{
         if(res['Status'] == "Success"){
           if(res['GetPAPDetails'] !=null){
             let obj = res['GetPAPDetails'];
             this.bankname = this.textDecode(obj['BankName']);
             this.accountNumber = this.textDecode(obj['AccountNumber']);
             this.transId = this.textDecode(obj['TransitId']);
             this.instId = this.textDecode(obj['InstitutionId']);
             this.iagree = obj['IsAgree'];
             this.fileName = obj['ChequeFile'];
             this.PAPDetailId = obj['PAPDetailId'];
           }
         }else{
           this.service.alertMessage(res['Message'],"error")
         }
       });
     }else{
         this.router.navigate(['/login']);
     }
 }

 updatePAPDetail(form){
   if(form.valid){
     if(form.controls.instId.value.length < 3){
       this.service.alertMessage("Institution ID must be at least 3 digits.","error");
       return false
     }
     if(form.controls.transId.value.length < 5){
       this.service.alertMessage("Transit ID must be at least 5 digits.","error");
       return false
     }

   const _input ={
     "AccountNumber": this.textEncode(form.controls.accountNumber.value),
     "BankName":this.textEncode(form.controls.bankname.value),
     "ChequeFile":this.fileName,
     "CompanyId":sessionStorage.getItem('companyId'),
     "DbCon": sessionStorage.getItem('DbCon'),
     "InstitutionId":this.textEncode(form.controls.instId.value),
     "IsAgree":this.iagree,
     "IsCheque": this.fileName !='' ? true : false,
     "PAPDetailId":this.PAPDetailId,
     "TransitId":this.textEncode(form.controls.transId.value),
     "LoggedInUserId": sessionStorage.getItem('loginUserId')
   }
   console.log(_input)
   this.loading = true;
   this.service.updatePAPDetails(_input)
   .subscribe((res:Response)=>{
     if(res['Status'] == 'Success'){
         this.service.alertMessage("Payment detail successfully updated.","success")
         this.loading = false;
     }else{
         this.loading = false;
         this.service.alertMessage(res['Message'],'error','')
     }
   },(error)=>{
       this.loading = false;
       this.service.alertMessage(error,'error','')
   })
 }else{
   this.service.alertMessage("Plase complete all required fields","error","");
   return false;
   }
 }

 togglePassword(pvarType){
   if(pvarType == 'op'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'np'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'cp'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'ti'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
   if(pvarType == 'in'){
      var x = document.getElementById(pvarType);
      if (x['type'] === "password") {
          x['type'] = "text";
      } else {
          x['type'] = "password";
      }
   }
 }

 textDecode(pvarTxt){
   if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
       return this.service.decodeText(pvarTxt);
   }
 }
 textEncode(pvarTxt){
     if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
       return this.service.encodeText(pvarTxt);
     }
 }

 editDepandentModal: boolean = false;
 selectedDepId:any= -1;
 editDepandent(obj){
   console.log(obj);
   this.editDepandentModal = true;
   this.selectedDepId = obj['DependentId'];
   if(obj['DOB']){
       this.dependantDob = this.transformDate(obj['DOB']);
   }
   //this.dependantType = obj['Relationship'];
   this.dependantType= obj['RelationShipId'];
   this.dependantFName = obj['FName'];
   this.dependantLName = obj['LName'];
   this.dependantGender = obj['Gender'];
 }

 updateDepandent(form){
    console.log(form);
     this.loading = true;
     let dob = '';
     let formDb = form.controls.dependantDob.value;
     if(formDb.formatted){
        dob= formDb.formatted;
     }else{
       if(typeof  formDb === 'object'){
         let mm = parseInt(formDb.date.month) < 10 ? '0'+formDb.date.month:formDb.date.month;
         let dd = parseInt(formDb.date.day) < 10 ? '0'+formDb.date.day: formDb.date.day;
         dob= mm+"/"+ dd +"/"+formDb.date.year;
       }else{
         dob= formDb;
       }
     }
     if(form.controls.dependantType.value == 4){
       if(form.controls.dependantRelationship.value == ''){
         this.service.alertMessage("Enter Other Relationship","error");
         return;
       }
     }
     const _input = {
        "DOB":dob,
        "DbCon": sessionStorage.getItem('DbCon'),
        "DependantId": this.selectedDepId,
        "DependantType":form.controls.dependantType.value,
        "FName":form.controls.dependantFName.value,
        "Gender":form.controls.dependantGender.value,
        "LName":form.controls.dependantLName.value,
        "LoggedInUserId": sessionStorage.getItem('loginUserId'),
        "OtherRelation": form.controls.dependantType.value == 4 ? form.controls.dependantRelationship.value : '',
        "RelationshipId":form.controls.dependantType.value
     }
     console.log(_input);
     this.loading = true;
     this.memberService.updateDependant(_input)
     .subscribe((res:Response)=>{
       if(res['Status'] == "Success"){
         this.getDependents()
         this.editDepandentModal = false;
         this.loading = false;
         form.reset();
         this.service.alertMessage("Dependant detail successfully update","success");
       }else{
           this.loading = false;
           this.service.alertMessage(res['Message'],'error');
       }
     }, (error)=>{
       this.loading = false;
       this.service.alertMessage(error,'error');
     })
   }


}
