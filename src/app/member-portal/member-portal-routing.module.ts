import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { EmployeerPortalComponent } from '../employeer-portal/employeer-portal.component';
import { MemberPortalComponent } from '../member-portal/member-portal.component';

//import { SummaryComponent } from './summary/summary.component'

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MyDate} from "../date-pipe";
import { MyDatePickerModule } from 'mydatepicker';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ClaimsComponent } from './claims/claims.component';
import { AccountComponent } from './account/account.component';
import { MessagesComponent } from './messages/messages.component';
import { AddClaimComponent } from './add-claim/add-claim.component';
import { NotificationComponent } from './notification/notification.component';
import { ClaimTypeComponent } from './claim-type/claim-type.component';
import { OffersComponent } from '../offers/offers.component';
import { FAQComponent } from '../faq/faq.component';
import { CardTransactionsComponent } from './card-transactions/card-transactions.component';
import { UpdateClaimDetailComponent } from './update-claim-detail/update-claim-detail.component';

export const memberPortalRoutes: Routes = [
  {
    path: 'member-portal',
    component: MemberPortalComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent },
      {path: 'transactions', component: TransactionsComponent },
      {path: 'claims', component: ClaimsComponent },
      {path: 'account', component: AccountComponent },
      {path: 'messages', component: MessagesComponent },
      {path: 'add-claim', component: AddClaimComponent },
      {path: 'notification', component: NotificationComponent },
      {path: 'load-card', component: ClaimTypeComponent },
      {path: 'offers', component: OffersComponent},
      {path: 'general-faq', component: FAQComponent},
      {path: 'card-transactions/:id', component: CardTransactionsComponent},
      {path: 'update-claim-detail/:id', component: UpdateClaimDetailComponent},
      {path: '',   redirectTo: 'member-portal/dashboard',   pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(memberPortalRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MemberPortalRoutingModule { }
