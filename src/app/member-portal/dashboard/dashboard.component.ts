import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {MemberService} from '../../member.service';

import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  chart: Chart;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  plansData: any=[];
  plansChart: any;
  companyName: string;
  isPending: boolean = true;
  isCurrent: boolean = true;
  isContribution: boolean= true;
  companyCode:string;
  amountDue: any = 0;
  colorArr:any = [];
  currentTransaction: any = [];
  pendingTransaction: any = [];

  //
  claimedAmt: number = 0;
  balanceAmt: number = 0;
  adjAmt: number = 0;
  userName: any='';

  constructor(private service: CompanyService, private memberService: MemberService,private router : Router) { }

  ngOnInit() {
    if(sessionStorage.getItem("DbCon") != null){
      this.companyName = sessionStorage.getItem("companyName");
      this.userName = sessionStorage.getItem("userName");
      this.companyCode = sessionStorage.getItem("uniqueCode");
      this.amountDue = sessionStorage.getItem("amountDue");

      this.getAmountOveriew()
      this.getTransaction();

    }else{
      this.router.navigate(['/login']);
    }
  }

  getAmountOveriew(){
    const _input = {
      "DbCon":sessionStorage.getItem("DbCon"),
      "Id": sessionStorage.getItem('loginUserId')
    }
    this.memberService.getEmpDashboardData(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        sessionStorage.setItem('planDesign', res['PlanDesign']);
        if(res['GetEmpDashboardData'].length > 0){
           for(var k=0; k< res['GetEmpDashboardData'].length; k++){
             const obj=  res['GetEmpDashboardData'][k];
             // const offData={};
             // offData['name'] = obj['PlanName'];
             // offData['y']=obj.NoOfEmployees;
             // offData['sliced']= k == 0 ? true : false;
             // offData['selected']= k == 0 ? true : false;
             if(obj['Type'] == 'Claim'){
               this.claimedAmt =obj['Amount']== 0 ? 0 : obj['Amount'];
               this.colorArr.push('#fa9619');
               this.plansData.push(['Claimed', obj['Amount']== 0 ? 0 : obj['Amount']])
             }
             if(obj['Type'] == 'Adjustments'){
               this.adjAmt = obj['Amount']== 0 ? 0 : obj['Amount'];
               this.colorArr.push('#2b3e4f');
               this.plansData.push(['Adjustment', obj['Amount']== 0 ? 0: obj['Amount'] ])
             }
             if(obj['Type'] == 'Balance'){
               this.balanceAmt =obj['Amount']== 0 ? 0 : obj['Amount'];
               sessionStorage.setItem("acctBalnValue", this.balanceAmt.toString())
               this.colorArr.push('#5f6263');
                let bal =obj['Amount']== 0 ? 0.01 : obj['Amount'];
               this.plansData.push(['Balance', bal])
             }
          };
      }
      this.showAmountChart();
      }
  });
  }
  isShowButton: boolean = false;

  showAmountChart(){
    console.log(JSON.stringify(this.plansData));
    this.isShowButton = true;
    this.plansChart = new Chart({
      chart: {
         type: 'pie',
         options3d: {
             enabled: true,
             alpha: 300,
         },
     },
     title: {
         text: ''
     },
     legend: {
        enabled:false
     },
     subtitle: {
         text: ''
     },
     plotOptions: {
         pie: {
             innerSize: 250,
             depth: 100,
             //showInLegend: false
           showInLegend: true,
           dataLabels: {
             enabled: false,
            // format: 'No of Employees:{point.y}',
             style: {
                 //  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
             }
         },
         },
     },
     credits: {
        enabled: false
    },
     colors: this.colorArr,//['#5f6263', '#fa9619', '#2b3e4f'],
     series: [{
         name: 'Amount',
         data: this.plansData,//[["Balance",1500],["Claims",0],["Adjustment",0]]//this.plansData
         //]
     }]
   });
  }


  planIndex: any = -1;
  toggleClass(object){
    this.planIndex = object.Plan;
  }
  getTransaction(){
    const _input={
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId')
    };
    this.memberService.getEmpDashboardTransactions(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.amountDue = res['AmountDue'];
        this.currentTransaction = res['CurrentTransaction'];
        this.pendingTransaction = res['PendingTransaction'];
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })
  }

}
