import { Component, OnInit, HostListener } from '@angular/core';
import {CompanyService} from '../company.service';
import { Router }              from '@angular/router';
import { ForgetForm }            from '../register/data/formData.model';
declare var document;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading:boolean;
  modal:boolean;
  username:string;password: string;company_code:string;
  forgetLoading:boolean;
  forgetForm: ForgetForm;
  inactiveModal: boolean;

  notificationConfrimationModal: boolean= false;
  notificationModal: boolean = false;
  otpModal: boolean = false;
  isTimeLaps: boolean =  true;

  screenHeight: any;
  minusHeight: any = 0;
  constructor( private service: CompanyService, private router: Router) {
   }

  ngOnInit() {
    this.getScreenSize();
    if(localStorage.getItem('c_code') != null){
      this.company_code = localStorage.getItem('c_code');
    }
  }

  @HostListener('window:resize', ['$event'])

  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 550;
        }else{
          this.minusHeight = 500;
        }
        this.screenHeight = window.innerHeight;
    }

  login(form){
    let u_name = form.controls.company_code.value+"_"+form.controls.username.value;
    const _input ={
      "UserName": u_name,//.replace(/\s/g, ""),
      "Password": form.controls.password.value//.replace(/\s/g, "")
    }
    this.loading = true;
    this.service.loginUser(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.loading = false;
        if(res['InActive'] == true){
          localStorage.setItem('c_code', form.controls.company_code.value);
          //this.service.alertMessage("You are Login successfully",'error','');
          sessionStorage.setItem("companyId",res['CompanyId']);
          sessionStorage.setItem("stage",res['StagesCovered']);
          sessionStorage.setItem("DbCon",res['DbCon']);
          sessionStorage.setItem("companyName", res['CompanyName']);
          sessionStorage.setItem("userName", res['Name']);
          sessionStorage.setItem("userTypeId", res['UserTypeId']);
          sessionStorage.setItem("uniqueCode", res['UniqueCode']);
          sessionStorage.setItem("amountDue", res['AmountDue']);
          sessionStorage.setItem("loginUserId", res['LoggedInUserId']);
          sessionStorage.setItem("compnayPlanTypeId", res['PlanTypeId']);
          sessionStorage.setItem("isCorporation", res['IsCorporation']);
          sessionStorage.setItem("UserToken", res['UserToken']);
          sessionStorage.setItem("CompanyRegDate", res['CompanyRegDate']);
          this.service.setActiveToken(res['UserToken']);
          if(res['UserTypeId'] == 1){
            if(res['StagesCovered'] == 1 && res['UserTypeId'] == 1){
              this.router.navigate(['/register/personal']);
            }
            if(res['StagesCovered'] == 2  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/plan']);
            }
            if(res['StagesCovered'] == 3  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/member-detail']);
            }
            if(res['StagesCovered'] == 4  && res['UserTypeId'] == 1){
              this.router.navigate(['/register/plan-review']);
            }
            if(res['StagesCovered'] == 5  && res['UserTypeId'] == 1){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
            }
            if(res['StagesCovered'] == 5  && res['UserTypeId'] == 1){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
              //this.router.navigate(['/employee-detail']);
            }
          }
          //admin
          else if(res['UserTypeId'] == 2){
            if(res['StagesCovered'] == 4){
              this.router.navigate(['/register/plan-review']);
            }
            if(res['StagesCovered'] == 5){
              //this.router.navigate(['/employeer-portal/summary']);
              this.router.navigate(['/employeer-portal/analytics']);
            }
          }
          //employees or plan members
          else if(res['UserTypeId'] == 3){
            //this.router.navigate(['/employees-portal']);
            // this.router.navigate(['/employee-detail']); //navigate to create card page
            if(res['IsCardExists'] == false){
                this.router.navigate(['/employee-detail']); //navigate to create card page
            }else{
              if(res['IsPAPDetails']== true){
                //check mobile verifyed
                //this.router.navigate(['/member-portal/dashboard']);
                // if(res['IsCarrier'] == false){
                if(res['NotifyType'] == 3 || res['NotifyType'] == 0){
                    this.notificationConfrimationModal = true;
                    this.phone = res['ContactNo'];
                }else if(res['NotifyType'] == 1 && res['IsMobileVerified'] == false){
                    this.otpModal = true;
                    this.startCoutDown();
                }else{
                    this.router.navigate(['/member-portal/dashboard']);
                }
              }else{
                this.router.navigate(['/bank-detail']);
              }
            }
          }
        }else{
          this.inactiveModal = true
        }
    }else{
      this.loading = false;
      // if(res['InActive'] == false){
      //   this.inactiveModal = true
      // }else{
      //   this.service.alertMessage(res['Message'],'error','');
      // }
      this.service.alertMessage(res['Message'],'error','');
    }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }

  openModal(type){
    console.log(type)
    this.modal = true;
  }

  sendMail(form){
    if(form.valid){
      let username = form.controls.fcompanyCode.value +"_"+ form.controls.fusername.value;
      const _input ={
        "UserName": username
      }
      this.forgetLoading = true;
      console.log(_input)
      this.service.forgotPassword(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.modal = false;
            this.forgetLoading = false;
            form.reset();
            this.service.alertMessage("A new password has been sent to your registered email address.",'success');
        }else{
          this.forgetLoading = false;
          this.service.alertMessage(res['Message'],'error')
        }
      },(error)=>{
        this.forgetLoading = false;
        this.service.alertMessage(error,'error','')
      })
    }
  }

  togglePassword(){
       var x = document.getElementById("loginPassword");
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
  }

  otpNumber: any = '';
  carriers: any = '';
  phone: any = '';


  carrierData: any = [];
  getCarrier(){
    this.service.populateCarrier()
    .subscribe((response:Response)=>{
      if(response['Status'] == 'Success'){
        this.carrierData = response['PopulateCarrierData'];
      }else{
        this.service.alertMessage(response['Message'],'error');
      }
    })
  }

  yesConfirmNotify(){
    this.getCarrier();
    this.notificationConfrimationModal = false;
    this.notificationModal = true;
  }
  noConfirmNotify(type){
    const _input = {
      "Carrier":'',
      "CarrierId":-1,
      "IsSMS": false,//form.controls.sms.value,
      "ContactNo":'',
      "DbCon":sessionStorage.getItem("DbCon"),
      "LoggedInUserId": sessionStorage.getItem('loginUserId'),
      "NotifyType":type
    }
    console.log(_input);
    this.service.addUserCarrier(_input)
    .subscribe((response:Response)=>{
      if(response['Status']=="Success"){
        this.notificationConfrimationModal = false;
        this.router.navigate(['/member-portal/dashboard']);
      }else{
        this.service.alertMessage(response['Message'],"error");
        return false;
      }
    },(error:Error)=>{
      this.service.alertMessage(error['Message'],"error");
    })
  }

  submitNotification(form){
    if(form.valid){
      const _input = {
        "Carrier":form.controls.carriers.value,
        "CarrierId":-1,
        "IsSMS": true,//form.controls.sms.value,
        "ContactNo":form.controls.phone.value,
      	"DbCon":sessionStorage.getItem("DbCon"),
        "LoggedInUserId": sessionStorage.getItem('loginUserId'),
        "NotifyType":1
      }
      console.log(_input);
      this.service.addUserCarrier(_input)
      .subscribe((response:Response)=>{
        if(response['Status']=="Success"){
          this.service.alertMessage("OTP has been sent on your registered mobile number:  "+ form.controls.phone.value,"success")
          //this.router.navigate(['/member-portal/dashboard']);
          this.notificationModal = false;
          this.otpModal = true;
          this.startCoutDown();
        }else{
          this.service.alertMessage(response['Message'],"error");
          return false;
        }
      },(error:Error)=>{
        this.service.alertMessage(error['Message'],"error");
      })
    }else{
      this.service.alertMessage("Plase fill all the required fields","error","");
      return false;
    }
  }
  verifyOTP(form){
    if(form.valid){
      let _input = {
        "DbCon":sessionStorage.getItem("DbCon"),
        "OTP":form.value.otpNumber,
        "UserId":sessionStorage.getItem('loginUserId')
      }
      console.log(_input)
      this.loading = true;
      this.service.validatePasscode(_input)
      .subscribe((response:Response)=>{
          this.loading = true;
          if(response['Status']=="Success"){
            this.service.alertMessage("Mobile number has been succssfully verified.","success")
            this.router.navigate(['/member-portal/dashboard']);
            this.otpModal = false;
          }else{
            this.loading = false;
            this.service.alertMessage(response['Message'],"error");
            return false;
          }
      },(error:Error)=>{
        this.service.alertMessage(error['Message'],"error");
      })
    }
  }

  resend_loading: boolean = false;
  resendOTP(){
    let _input = {
      "DbCon":sessionStorage.getItem("DbCon"),
      "Id":sessionStorage.getItem('loginUserId')
    }
    console.log(_input)
    this.resend_loading = true;
    this.service.resentOTP(_input)
    .subscribe((response:Response)=>{
     this.resend_loading = false;
      if(response['Status']=="Success"){
        this.resend_loading = false;
        this.service.alertMessage("OTP has been sent on your registered mobile number: ","success");
        this.startCoutDown();
      }else{
        this.resend_loading = false;
        this.service.alertMessage(response['Message'],"error");
        return false;
      }
    },(error:Error)=>{
      this.service.alertMessage(error['Message'],"error");
    })
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 startCoutDown(){
   this.isTimeLaps = true;
   var timeleft = 30;
   var downloadTimer = setInterval(()=>{
     let a = 0 + --timeleft;
     document.getElementById("countDown").innerHTML = a.toString();

     if(a==0){
         this.isTimeLaps = false;
     }
     if(timeleft <= 0){
       this.isTimeLaps = false;
       clearInterval(downloadTimer);
     }
   },1000);
 }

}
