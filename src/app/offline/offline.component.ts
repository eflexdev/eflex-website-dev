
import { Component, OnInit , AfterViewInit } from '@angular/core';
import {isOnline} from '../app.component';
import {Router} from '@angular/router';

declare var $ :any;
@Component({
  selector: 'app-offline',
  templateUrl: './offline.component.html',
  styleUrls: ['./offline.component.css']
})
export class OfflineComponent implements OnInit,AfterViewInit {

  constructor(private router:Router) { }

  ngOnInit() {
      console.log('offline init');
  }

  ngDoCheck(){
    if(isOnline){
      this.router.navigate(['/']);
    }
  }

  ngAfterViewInit(){
      $('.cd-header').css('backgroundColor','rgb(147, 18, 70)');
  }

}
