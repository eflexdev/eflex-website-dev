import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import {CompanyService} from '../company.service';
import { Router }              from '@angular/router';
import { ForgetForm }            from '../register/data/formData.model';
import {environment} from '../../environments/environment.prod'
@Component({
  selector: 'app-bank-detail',
  templateUrl: './bank-detail.component.html',
  styleUrls: ['./bank-detail.component.css']
})
export class BankDetailComponent implements OnInit {

  loading:boolean;
  modal:boolean;
  bankname:any;instId: any;
  transId: any;
  accountNumber: any;
  forgetLoading:boolean;
  companyId: any;
  dbCon: any;
  initialPay:0;
  recuringPay: 0;
  loginUserName: string;
  iagree:boolean;
  btnDisable: boolean= false;
  uploadingFile:boolean;
  fileName: string = '';
  hostUrl: string;
  confrimModal: boolean;
  ownerEmail:string;
  userTypeId: any = 1;
  notificationModal: boolean = false;
  notificationConfrimationModal: boolean = false;
  openConditionModal: boolean = false;
  sms: boolean= false;
  carriers:string='';
  phone: string='';
  isTimeLaps:boolean = true;

  constructor( private service: CompanyService, private router: Router, private el: ElementRef) {
      this.hostUrl = environment.hostUrl;
      if(sessionStorage.getItem("papSubmited") != null && sessionStorage.getItem("papSubmited") == 'true'){
        if(this.userTypeId != 1){
          this.router.navigate(['/member-portal/dashboard']);
        }else{
          sessionStorage.clear();
          this.router.navigate(['/login']);
        }
      }
      this.getCarrier();
   }

  ngOnInit() {
    if(sessionStorage.getItem("companyId") != null){
      this.phone = sessionStorage.getItem('mobileNo');
      this.userTypeId = sessionStorage.getItem('userTypeId') != undefined ? sessionStorage.getItem('userTypeId') : 1;
      const _input={
        "Id": sessionStorage.getItem("companyId"),
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      };
      this.service.getCompanyDetails(_input)
      .subscribe((res:Response)=>{
        if(this.userTypeId == '1' ){
          this.loginUserName = res['CompanyBasicaData']['CompanyName'];
        }else{
            this.loginUserName = sessionStorage.getItem('userName');
        }
        this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
        this.dbCon = res['CompanyBasicaData']['DbCon'];
        this.companyId = sessionStorage.getItem("companyId")
        this.initialPay= res['PaymentOverview']['InitialPayment'];
        this.recuringPay= res['PaymentOverview']['RecurringPayment'];
    })
    //this.notificationConfrimationModal = true;
  }else{
      this.router.navigate(['/login']);
    }
    this.getScreenSize()
}

  carrierData: any = [];
  getCarrier(){
    this.service.populateCarrier()
    .subscribe((response:Response)=>{
      if(response['Status'] == 'Success'){
        this.carrierData = response['PopulateCarrierData'];
      }else{
        this.service.alertMessage(response['Message'],'error');
      }
    })
  }

screenHeight: any;
minusHeight: any = 0;

@HostListener('window:resize', ['$event'])
getScreenSize(event?) {
      if(window.innerHeight < 1200){
        this.minusHeight = 140;
      }else{
        this.minusHeight = 150;
      }
      this.screenHeight = window.innerHeight;
  }
  addBankDetail(form){
    //this.notificationModal = true;
    if(form.valid){
      if(form.controls.instId.value.length < 3){
        this.service.alertMessage("Institution ID must be at least 3 digits.","error");
        return false
      }
      if(form.controls.transId.value.length < 5){
        this.service.alertMessage("Transit ID must be at least 5 digits.","error");
        return false
      }

    const _input ={
    	"AccountNumber": this.textEncode(form.controls.accountNumber.value),
      "BankName": this.textEncode(form.controls.bankname.value),
    	"CompFinancialDetailId":-1,
    	"CompanyId":this.companyId,
    	"DbCon":this.dbCon,
    	"FileName":this.fileName,
      "InitialPayment": this.userTypeId ==1 ? this.initialPay : 0,
    	"InstitutionNumber":this.textEncode(form.controls.instId.value),
      "IsAgree":this.iagree,
    	"IsCheque": this.fileName !='' ? true : false,
    	"TransitNumber": this.textEncode(form.controls.transId.value),
      "RecurringPayment":this.userTypeId ==1 ? this.recuringPay : 0,
	    "CompanyUserId":  sessionStorage.getItem("loginUserId")
    }
    console.log(_input)
    this.loading = true;
    this.service.addCompanyFinancialDetails(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
          this.loading = false;
          if(this.userTypeId != 1){
             this.service.alertMessage("PAP details submitted successfully.","success");
             this.notificationConfrimationModal = true;
             sessionStorage.setItem("papSubmited", 'true');
             //this.router.navigate(['/member-portal/dashboard']);
             //this.notificationModal = true;
          }else{
              this.confrimModal = true;
          }
      }else{
          this.loading = false;
          this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }else{
    this.service.alertMessage("Plase complete all required fields","error","");
    return false;
  }
  }

  textDecode(pvarTxt){
    if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
        return this.service.decodeText(pvarTxt);
    }
  }

  textEncode(pvarTxt){
      if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
        return this.service.encodeText(pvarTxt);
      }
  }

  confirmPayment(){
    this.confrimModal = false;
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 openModal(){
   this.modal = true;
 }
 removeFile(){
   this.fileName = '';
   this.el.nativeElement.querySelector('#file').value= '';
   this.el.nativeElement.value = '';
 }

 uploadMsg = '';
 uploadFile(event) {
       this.uploadMsg = '';
       let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
       //get the total amount of files attached to the file input.
       let fileCount: number = inputEl.files.length;
       //create a new fromdata instance
       let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('CompanyId', this.companyId);
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.modal = false;
                        this.fileName = img_name[1];
                         // this.profileForm.patchValue({
                         //   'Img': img_name[1]
                         // });
                         this.service.alertMessage("Void cheque successfully upload","success");
                     }else{
                       this.service.alertMessage("Void cheque upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg ="Please select the file for your void cheque";
         }
}

  yesConfirmNotify(){
    this.notificationConfrimationModal = false
    this.notificationModal = true;
  }

  noConfirmNotify(type){
    const _input = {
      "Carrier":'',
      "CarrierId":-1,
      "IsSMS": false,//form.controls.sms.value,
      "ContactNo":'',
      "DbCon":sessionStorage.getItem("DbCon"),
      "LoggedInUserId": sessionStorage.getItem('loginUserId'),
      "NotifyType": type
    }
    console.log(_input);
    this.service.addUserCarrier(_input)
    .subscribe((response:Response)=>{
      if(response['Status']=="Success"){
        this.notificationConfrimationModal = false;
        this.router.navigate(['/member-portal/dashboard']);
      }else{
        this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
      this.service.alertMessage(error['Message'],"error");
    })
  }

  otpModal: boolean = false;
  submitNotification(form){
    if(form.valid){
      const _input = {
        "Carrier":form.controls.carriers.value,
        "CarrierId":-1,
        "IsSMS": true,//form.controls.sms.value,
        "ContactNo":form.controls.phone.value,
      	"DbCon":sessionStorage.getItem("DbCon"),
        "LoggedInUserId": sessionStorage.getItem('loginUserId'),
        "NotifyType":1
      }
      console.log(_input);
      this.service.addUserCarrier(_input)
      .subscribe((response:Response)=>{
        if(response['Status']=="Success"){
          this.service.alertMessage("OTP has been sent on your registered mobile number:  "+ form.controls.phone.value,"success")
          //this.router.navigate(['/member-portal/dashboard']);
          this.notificationModal = false;
          this.otpModal = true;
          this.startCoutDown();
        }else{
          this.service.alertMessage(response['Message'],"error");
        }
      },(error:Error)=>{
        this.service.alertMessage(error['Message'],"error");
      })
    }else{
      this.service.alertMessage("Plase fill all the required fields","error","");
      return false;
    }
  }

  closeNotify(){
    this.router.navigate(['/member-portal/dashboard']);
  }
  logOut(){
    sessionStorage.clear();
    this.service.alertMessage("You are logout successfully","success");
    this.router.navigate(['/join-us']);
  }

  togglePassword(pvarType){
    if(pvarType == 'ti'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
    if(pvarType == 'in'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
    if(pvarType == 'acc'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
  }

  otpNumber: any = '';
  loadingOTP: boolean = false;
  verifyOTP(form){
    if(form.valid){
      let _input = {
        "DbCon":sessionStorage.getItem("DbCon"),
	      "OTP":form.value.otpNumber,
	      "UserId":sessionStorage.getItem('loginUserId')
      }
      console.log(_input)
      this.loadingOTP = true;
      this.service.validatePasscode(_input)
      .subscribe((response:Response)=>{
          this.loadingOTP = true;
          if(response['Status']=="Success"){
            this.service.alertMessage("Mobile number has been succssfully verified.","success")
            this.router.navigate(['/member-portal/dashboard']);
            this.otpModal = false;
          }else{
            this.loadingOTP = false;
            this.service.alertMessage(response['Message'],"error");
          }
      },(error:Error)=>{
        this.loadingOTP = false;
        this.service.alertMessage(error['Message'],"error");
      })
    }
  }

  resend_loading: boolean = false;
  resendOTP(){
    let _input = {
      "DbCon":sessionStorage.getItem("DbCon"),
      "Id":sessionStorage.getItem('loginUserId')
    }
    console.log(_input)
    this.resend_loading = true;
    this.service.resentOTP(_input)
    .subscribe((response:Response)=>{
     this.resend_loading = false;
      if(response['Status']=="Success"){
        this.service.alertMessage("OTP has been sent on your registered mobile number","success");
        this.startCoutDown();
      }else{
          this.resend_loading = false;
         this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
        this.resend_loading = false;
        this.service.alertMessage(error['Message'],"error");
    })
  }


  startCoutDown(){
    this.isTimeLaps = true;
    var timeleft = 30;
    var downloadTimer = setInterval(()=>{
      let a = 0 + --timeleft;
      document.getElementById("countDown").innerHTML = a.toString();
      if(a==0){
          this.isTimeLaps = false;
      }
      if(timeleft <= 0){
        this.isTimeLaps = false;
        clearInterval(downloadTimer);
      }
    },1000);
  }

}
