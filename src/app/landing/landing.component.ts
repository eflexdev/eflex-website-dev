import { Component, OnInit } from '@angular/core';
import {CompanyService}  from "../company.service";

// import '../../assets/js/core.min.js';
// import '../../assets/js/script.js';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
  // styleUrls: ['./landing.component.css','./custom.css',]
  // styleUrls: ['./landing.component.css','../../assets/custom-css/bootstrap.css','../../assets/custom-css/style.css']
})
export class LandingComponent implements OnInit {

  constructor(private service : CompanyService) { }
  url: string;
  isSelected: string = '';
  isMobileMenu: boolean = false;
  isSlideNo: any = 1;
  ngOnInit() {
    this.url= 'http://localhost:4200/assets/images/logo.png';
    this.service.getRandomCode()
    .subscribe((res:Response)=>{
      if(res['Status'] =="Success"){
        sessionStorage.setItem("RandomCode",res['RandomCode']);
      }else{
        this.service.alertMessage("Some thing went worng","error")
      }
    });

    // setTimeout(()=>{
    //   this.startSlide();
    // },7000);
  }


  startSlide(){
      if(this.isSlideNo == 2){
        this.isSlideNo = 1;
      }else{
        this.isSlideNo = this.isSlideNo+1;
      }
      setTimeout(()=>{ this.startSlide() }, 7000);
  }

  toogleUserSection(pvarType){
    this.isSelected = pvarType;
  }

  preSlide(){
    if(this.isSlideNo == 1){
      this.isSlideNo = 2;
    }else{
     this.isSlideNo = this.isSlideNo-1;
   }
  }
  nextSlide(){
    if(this.isSlideNo == 2){
      this.isSlideNo = 1;
    }else{
      this.isSlideNo = this.isSlideNo+1;
    }
  }

 isSlideNoWe: any = 1;
  preSlideWE(){
    if(this.isSlideNoWe == 1){
      this.isSlideNoWe = 2;
    }else{
     this.isSlideNoWe = this.isSlideNoWe-1;
   }
  }
  nextSlideWE(){
    if(this.isSlideNoWe == 2){
      this.isSlideNoWe = 1;
    }else{
      this.isSlideNoWe = this.isSlideNoWe+1;
    }
  }

  selectedFaq: any = -1;
  toggleFaq(pvarId){
    this.selectedFaq = pvarId;
  }

  selectedUserType: string = 'e';
  activeUserType : any = '';
  toogelRightImg(pvarType){
    this.selectedUserType = pvarType;
    this.activeUserType = pvarType;
  }

}
