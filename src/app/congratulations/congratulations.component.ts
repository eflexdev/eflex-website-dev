import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import {CompanyService} from '../company.service';
import { Router }              from '@angular/router';
import {environment} from '../../environments/environment.prod'
@Component({
  selector: 'app-congratulations',
  templateUrl: './congratulations.component.html',
  styleUrls: ['./congratulations.component.css']
})
export class CongratulationsComponent implements OnInit {

  loginUserName: string = '';
  ownerEmail: string = '';
  hostUrl: string = '';
  screenHeight: any;
  minusHeight: any = 0;


  constructor(
    private service: CompanyService,
    private router: Router,
    private el: ElementRef) {
        this.hostUrl = environment.hostUrl;
  }

  worktype :string ='Group';
  ngOnInit() {

      this.getScreenSize();
      this.worktype = sessionStorage.getItem("PlanTypeId");

      if(sessionStorage.getItem("companyId") != null){
        const _input={
          "Id": sessionStorage.getItem("companyId"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.service.getCompanyDetails(_input)
        .subscribe((res:Response)=>{
          this.loginUserName = res['CompanyBasicaData']['CompanyName'];
          this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
          setTimeout(()=>{
            sessionStorage.clear();
            //this.router.navigate(['/join-us']);
          },2000);
      })
      //this.notificationConfrimationModal = true;
      }else{
         this.router.navigate(['/login']);
      }

  }


  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 0;
        }else{
          this.minusHeight = 150;
        }
        this.screenHeight = window.innerHeight;
    }
}
