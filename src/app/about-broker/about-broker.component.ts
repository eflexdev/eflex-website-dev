import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-about-broker',
  templateUrl: './about-broker.component.html',
  styleUrls: ['./about-broker.component.css']
})
export class AboutBrokerComponent implements OnInit {
  screenHeight:any;
  screenWidth:any;

  constructor() { }

  ngOnInit() {
    this.getScreenSize();
  }
  @HostListener('window:resize', ['$event'])

  getScreenSize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
  }
}
