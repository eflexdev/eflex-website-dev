import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutBrokerComponent } from './about-broker.component';

describe('AboutBrokerComponent', () => {
  let component: AboutBrokerComponent;
  let fixture: ComponentFixture<AboutBrokerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutBrokerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutBrokerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
