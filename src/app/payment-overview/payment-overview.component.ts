import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../company.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
@Component({
  selector: 'app-payment-overview',
  templateUrl: './payment-overview.component.html',
  styleUrls: ['./payment-overview.component.css']
})
export class PaymentOverviewComponent implements OnInit {
  paymentData: any = [];
  loading: boolean;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  loginUserName: string='';
  //paymentDate: any;
  public paymentDate;

  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     //dateFormat: 'yyyy/mm/dd',
     editableDateField: false,
     disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1},
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+2, day: this.d.getDate()+1}
 };

  constructor(private service: CompanyService, private router: Router) {
    let  date = new Date();
    this.paymentDate =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
    this.loginUserName = sessionStorage.getItem("userName");
  }

  ngOnInit() {
      if(sessionStorage.getItem("DbCon") != null){
      const _input = {
        "DbCon":sessionStorage.getItem("DbCon")
      }
      this.service.getPaymentOverview(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
          this.NetGrossInitial = res['NetGrossInitial'];
          this.NetGrossRecurring = res['NetGrossRecurring'];
          this.paymentData = res['ClasswisePayments'];
        }
      });
    }else{
      this.router.navigate(['/login']);
    }
  }

  updatePaymenDate(){
    if(this.paymentDate == null){
      this.service.alertMessage("Select when you would like the amount pulled from your account","error");
      return false;
    }
    let date = '';
    if(typeof  this.paymentDate === 'object'){
      let mm = parseInt(this.paymentDate.date.month) < 10 ? '0'+this.paymentDate.date.month: this.paymentDate.date.month;
      let dd = parseInt(this.paymentDate.date.day) < 10 ? '0'+this.paymentDate.date.day: this.paymentDate.date.day;
       date = mm+"/"+ dd +"/"+this.paymentDate.date.year;
    }else{
       date= this.paymentDate;
    }

    if(sessionStorage.getItem("DbCon") != null){
      const _input = {
        "CompanyId": sessionStorage.getItem('companyId'),
        "DbCon":sessionStorage.getItem("DbCon"),
        "PaymentDate":date,
      };
      this.loading = true;
      console.log(_input);
      this.service.updatePaymentDate(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
           this.loading = false;
          this.service.alertMessage('Payment date update successfully',"success");
          this.router.navigate(['/bank-detail']);
        }
      });
    }else{
      this.router.navigate(['/login']);
    }
  }

  logOut(){
    sessionStorage.clear();
    this.service.alertMessage("You are logout successfully","success");
    this.router.navigate(['/join-us']);
  }
}
