export class FormData {
    firstName:string='';
    lastName:string='';
    loginEmail:string='';
    loginConfirmEmail:string='';
    pwd: string='';
    confirmPwd:string='';
    companyName:string='';
    companyAddress:string='';
    email: string = '';
    street: string = '';
    city: string = '';
    state: string = '';
    zip: string = '';
    work: string = '';
    phone:string='';
    businessType:string='';
    companyCode:string='';
    username:string='';

    refTypeId: string='';
    refName: string='';
    refBrokerId:string='';

    refNewsPaper: string = '';
    refRadio:string='';
    refSocialMedia: string='';
    refTV: string = '';
    refFName: string = '';
    refLName: string = '';

    confirm_email: string = '';



    clear() {
        this.email = '';
        this.companyAddress = '';
        this.street = '';
        this.city = '';
        this.state = '';
        this.zip = '';
        this.confirm_email = '';
    }
}

export class Personal {
    city: string = '';
    state : string = '';
    companyAddress : string = '';
    street : string = '';
    email: string = '';
    companyName: string='';
    zip:string='';
    companyCode:string='';
}

export class Address {
  firstName:string='';
  lastName:string='';
  loginEmail:string='';
  //loginConfirmEmail:string='';
  //pwd: string='';
  //confirmPwd:string='';
  email: string = '';
  phone: string='';
  confirm_email: string = '';

}

export class ForgetForm {
  fusername: string='';
  fcompanyCode: string='';
}

export class Admin {
  "CompanyPlanId":number= -1;
  "CompanyUserId":number = -1;
  "ContactNo":string = '';
  "Email":string='';
  "FName":string='';
  "LName":string='';
  "UserTypeId":number= 2;
}

export class Refral {
    refTypeId: string = '';
    refName : string = '';
    refBrokerId : string = '';
    refNewsPaper: string = '';
    refRadio:string='';
    refSocialMedia: string='';
    refTV: string = '';
    refFName: string = '';
    refLName: string = '';
}
