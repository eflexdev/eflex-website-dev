import { Injectable }                        from '@angular/core';

import { FormData, Personal, Address,Refral }       from './formData.model';
import { WorkflowService }                   from '../workflow/workflow.service';
import { STEPS }                             from '../workflow/workflow.model';
import {CompanyService} from '../../company.service'
@Injectable()
export class FormDataService {

    private formData: FormData = new FormData();
    private isPersonalFormValid: boolean = false;
    private isWorkFormValid: boolean = false;
    private isBusinessFormValid: boolean = false;
    private isAddressFormValid: boolean = false;

    constructor(private workflowService: WorkflowService, private service : CompanyService) {

      if(sessionStorage.getItem("companyId") != null){
        const _input={
          "Id": sessionStorage.getItem("companyId"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.service.getCompanyDetails(_input)
        .subscribe((res:Response)=>{
          console.log(res)
          if(res['Status'] == 'Success'){
            //sessionStorage.setItem("companyId",res['CompanyId']);
            // sessionStorage.setItem("RSTax", res['RSTax']);
            // sessionStorage.setItem("PPTax", res['PPTax']);
            sessionStorage.setItem('TaxDetails', JSON.stringify(res['TaxDetails']));
            sessionStorage.setItem('PaymentOverview', JSON.stringify(res['PaymentOverview']));
            let data = res['CompanyBasicaData'];
            sessionStorage.setItem("stage", data['StagesCovered']);
            sessionStorage.setItem("DbCon",data['DbCon']);
            sessionStorage.setItem("PlanTypeId",data['PlanTypeId']);

            if(data['IsCorporation'] == "True"){
              this.formData.businessType = "Corporation";
            }else{
              this.formData.businessType = "Sole Propreitorship";
            }
            if(data['PlanTypeId'] !=''){
              this.formData.work = data['PlanTypeId'] == 2 ? 'Group': 'Personal';
            }
            this.formData.street =data['Address2'];
            this.formData.companyName = data['CompanyName'];
            this.formData.city =data['City'];
            this.formData.state =data['ProvinceId'];
            this.formData.zip =data['PostalCode'];
            this.formData.companyAddress =data['Address1'];
            this.formData.email =data['OwnerEmail'];
            this.formData.confirm_email =data['OwnerEmail'];
            this.formData.companyCode =data['Code'];

            this.formData.firstName  =data['FirstName'];
            this.formData.lastName  =data['LastName'];
            let uname= data['UserName'].split('_')
            console.log(uname);
            this.formData.loginEmail  = uname[1];
            this.formData.loginConfirmEmail  = uname[1];
            this.formData.pwd  =data['Password'];
            this.formData.confirmPwd  =data['Password'];
            this.formData.phone  =data['OwnerContactNo'];

            if(res['CompanyReferral'] != null){
                this.formData.refTypeId = res['CompanyReferral']['ReferralTypeId'];

                if(res['CompanyReferral']['ReferralTypeId'] == 1){
                  this.formData.refBrokerId = res['CompanyReferral']['BrokerId'];
                }else if(res['CompanyReferral']['ReferralTypeId']  == 5){
                    this.formData.refSocialMedia = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 4){
                  this.formData.refNewsPaper = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 3){
                this.formData.refTV = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 2){
                  this.formData.refRadio = res['CompanyReferral']['Name'];
                }
            }
          }else{
            this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          this.service.alertMessage(error,'error','')
        })
      }
    }

    getCompanyDetail(){

      if(sessionStorage.getItem("companyId") != null){
        const _input={
          "Id": sessionStorage.getItem("companyId"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.service.getCompanyDetails(_input)
        .subscribe((res:Response)=>{
          console.log(res)
          if(res['Status'] == 'Success'){
            //sessionStorage.setItem("companyId",res['CompanyId']);

            sessionStorage.setItem("RSTax", res['RSTax']);
            sessionStorage.setItem("PPTax", res['PPTax']);

            let data = res['CompanyBasicaData'];
            sessionStorage.setItem("stage", data['StagesCovered']);
            sessionStorage.setItem("DbCon",data['DbCon']);
            sessionStorage.setItem("PlanTypeId",data['PlanTypeId']);

            if(data['IsCorporation'] == "True"){
              this.formData.businessType = "Corporation";
            }else{
              this.formData.businessType = "Sole Propreitorship";
            }

            if(data['PlanTypeId'] !=''){
              this.formData.work = data['PlanTypeId'] == 2 ? 'Group': 'Personal';
            }

            sessionStorage.setItem("companyPlans", res['PlanDetails']);
            this.formData.street =data['Address2'];
            this.formData.companyName = data['CompanyName'];
            this.service.setCompnayName(true, data['CompanyName']);
            this.formData.city =data['City'];
            this.formData.state =data['ProvinceId'];
            this.formData.zip =data['PostalCode'];
            this.formData.companyAddress =data['Address1'];
            this.formData.email =data['OwnerEmail'];
            this.formData.confirm_email =data['OwnerEmail'];
            this.formData.companyCode =data['Code'];

            this.formData.firstName  =data['FirstName'];
            this.formData.lastName  =data['LastName'];
            let uname= data['UserName'].split('_')
            console.log(uname);
            this.formData.loginEmail  = uname[1];
            this.formData.loginConfirmEmail  = uname[1];
            this.formData.pwd  =data['Password'];
            this.formData.confirmPwd  =data['Password'];
            this.formData.phone  =data['OwnerContactNo'];

            if(res['CompanyReferral'] != null){
                this.formData.refTypeId = res['CompanyReferral']['ReferralTypeId'];

                if(res['CompanyReferral']['ReferralTypeId'] == 1){
                  this.formData.refBrokerId = res['CompanyReferral']['BrokerId'];
                }else if(res['CompanyReferral']['ReferralTypeId']  == 5){
                    this.formData.refSocialMedia = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 4){
                  this.formData.refNewsPaper = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 3){
                this.formData.refTV = res['CompanyReferral']['Name'];
                }
                else if(res['CompanyReferral']['ReferralTypeId']  == 2){
                  this.formData.refRadio = res['CompanyReferral']['Name'];
                }
            }
          }else{
            this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          this.service.alertMessage(error,'error','')
        })
      }
    }

    getPersonal(): Personal {
        // Return the Personal data

        var personal: Personal = {
            street: this.formData.street,
            city: this.formData.city,
            state: this.formData.state,
            zip: this.formData.zip,
            companyAddress: this.formData.companyAddress,
            email: this.formData.email,
            companyName: this.formData.companyName,
            companyCode: this.formData.companyCode,
        };
        return personal;
    }

    setPersonal(data: Personal) {
        // Update the Personal data only when the Personal Form had been validated successfully
        if(data != null){
          this.isPersonalFormValid = true;
          this.formData.street = data.street;
          this.formData.companyAddress = data.companyAddress;
          this.formData.email = data.email;
          this.formData.state = data.state;
          this.formData.city = data.city;
          this.formData.zip = data.zip;
          this.formData.companyName= data.companyName;
          this.formData.companyCode= data.companyCode,
          // Validate Personal Step in Workflow
          this.workflowService.validateStep(STEPS.personal);
      }else{
        this.formData.street = '';
        this.formData.companyAddress = "";
        this.formData.email = '';
        this.formData.state = '';
        this.formData.city = '';
        this.formData.zip = '';
        this.formData.companyName= '';
        this.formData.companyCode= ''
      }

    }

    getWork() : string {
        // Return the work type
        return this.formData.work;
    }

    setWork(data: string) {
        // Update the work type only when the Work Form had been validated successfully
        this.isWorkFormValid = true;
        this.formData.work = data;
        // Validate Work Step in Workflow
        this.workflowService.validateStep(STEPS.work);
    }

    getBusiness() : string {
        // Return the work type
        return this.formData.businessType;
    }
    setBusiness(data: string) {
        // Update the work type only when the Work Form had been validated successfully
        this.isBusinessFormValid = true;
        this.formData.businessType = data;
        // Validate Work Step in Workflow
        this.workflowService.validateStep(STEPS.business);
    }

    getAddress() : Address {
        // Return the Address data
        var address: Address = {
            firstName: this.formData.firstName,
            lastName:this.formData.lastName,
            loginEmail:this.formData.loginEmail,
            //loginConfirmEmail:this.formData.loginConfirmEmail,
            //pwd: this.formData.pwd,
            //confirmPwd:this.formData.confirmPwd,
            email: this.formData.email,
            phone: this.formData.phone,
            confirm_email : this.formData.email
        };
        return address;
    }

    setAddress(data: Address) {
        // Update the Address data only when the Address Form had been validated successfully
        if(data != null){
          this.isAddressFormValid = true;
          this.formData.firstName = data.firstName;
          this.formData.lastName = data.lastName;
          this.formData.loginEmail = data.loginEmail;
          //this.formData.loginConfirmEmail = data.loginConfirmEmail;
          //this.formData.pwd = data.pwd;
          //this.formData.confirmPwd = data.confirmPwd;
          this.formData.email = data.email;
          this.formData.phone = data.phone;
          this.formData.confirm_email = data.confirm_email;
          // Validate Address Step in Workflow
          this.workflowService.validateStep(STEPS.address);
        }else{
          this.formData.firstName = '';
          this.formData.lastName = '';
          this.formData.loginEmail = '';
          this.formData.email = '';
          this.formData.phone = '';
          this.formData.confirm_email = '';
        }
    }

    getFormData(): FormData {
        // Return the entire Form Data
        return this.formData;
    }

    resetFormData(): FormData {
        // Reset the workflow
        this.workflowService.resetSteps();
        // Return the form data after all this.* members had been reset
        this.formData.clear();
        this.isPersonalFormValid = this.isWorkFormValid = this.isBusinessFormValid = this.isAddressFormValid = false;
        return this.formData;
    }

    isFormValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isPersonalFormValid &&
                this.isWorkFormValid && this.isBusinessFormValid &&
                this.isAddressFormValid;
    }


    getRefral() : Refral {
        // Return the work type
        var refRal: Refral = {
            refTypeId: this.formData.refTypeId,
            refName: this.formData.refName,
            refBrokerId:this.formData.refBrokerId,
            refNewsPaper: this.formData.refNewsPaper,
            refRadio:this.formData.refRadio,
            refSocialMedia: this.formData.refSocialMedia,
            refTV: this.formData.refTV,
            refFName: this.formData.refFName,
            refLName: this.formData.refLName
        };
        return refRal;
    }
    setRefral(data: Refral) {
        //this.isRefFormValid = true;
        this.formData.refTypeId = data.refTypeId;
        this.formData.refName = data.refName;
        this.formData.refBrokerId= data.refBrokerId;
        this.formData.refNewsPaper = data.refNewsPaper;
        this.formData.refRadio = data.refRadio;
        this.formData.refSocialMedia= data.refSocialMedia;
        this.formData.refTV = data.refTV;
        this.formData.refFName =data.refFName;
        this.formData.refLName =data.refLName;
        // Validate Work Step in Workflow
        this.workflowService.validateStep(STEPS.address);
    }
}
