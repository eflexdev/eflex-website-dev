import { Component, OnInit, Input,ChangeDetectorRef }   from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {DatePipe} from "@angular/common";
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
import {IMyDpOptions} from 'mydatepicker';
import { Router }              from '@angular/router';
import { FormData }                   from '../data/formData.model';
import { FormDataService }            from '../data/formData.service';
import {CompanyService} from '../../company.service'
declare var $:any;
@Component ({
    selector:     'mt-wizard-result'
    ,templateUrl: './result.component.html',
    styles:[`.mydp .mydpicon {	font-family: mydatepicker !important;}`]
})

export class ResultComponent implements OnInit {
    ClassName:any= [];
    title = 'Plan setup';
    planForm: FormGroup;
    isPlanEdit: any = -1;
    //Plans: any = [];

    taxData: any = '';
    RSTax:number = 0
    PPTax:number= 0;
    HST: number =0;
    AdminFeePct: number =0;
    GST: number =0;

    classes: any='';
    myIndex:any = 0;
    isFormValid: any = false;
    planType: any;
    planTypeId : any;
    loading: true;
    planData : any=[];
    myOptions={};
    isFunded: boolean = true;

    d = new Date();
    public myDatePickerOptions: IMyDpOptions = {
       dateFormat: 'mm/dd/yyyy',
       editableDateField: false,
       disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()},
       disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth(), day: this.d.getDate()-1}
       //disableUntil: {year: 2016, month: 3, day: 5}
   };

    constructor(
      private formDataService: FormDataService,
      private fb:FormBuilder,
      private ref:ChangeDetectorRef,
      private service: CompanyService,
      private router: Router
    ) {
      if(sessionStorage.getItem("companyId") == null){
        //this.service.alertMessage("Please fill company information first","error");
        //  this.router.navigate(['/register/personal']);
      }
      this.ClassName=[{"id":1,"name": "Class A"},
      {"id":2,"name": "Class B"}, {"id":3,"name": "Class C"},{"id":4,"name": "Class D"},{"id":5,"name": "Class E"},
      {"id":6,"name": "Class F"},{"id":7,"name": "Class G"}, {"id":8,"name": "Class H"},{"id":9,"name": "Class I"},
      {"id":10,"name": "Class J"},{"id":11,"name": "Class K"},{"id":12,"name": "Class L"},{"id":13,"name": "Class M"},
      {"id":14,"name": "Class N"},{"id":15,"name": "Class O"},{"id":16,"name": "Class P"},{"id":17,"name": "Class Q"},
      {"id":18,"name": "Class R"},{"id":19,"name": "Class S"},{"id":20,"name": "Class T"},{"id":21,"name": "Class U"},
      {"id":22,"name": "Class V"},{"id":23,"name": "Class W"},{"id":24,"name": "Class X"},{"id":25,"name": "Class Y"},
      {"id":26,"name": "Class Z"}]
      this.planForm = this.fb.group({
            "ID":[''],
            "Plans":  this.fb.array([ ]) // this.createPlanItem(1)
      });
      this.planTypeId =  2; //sessionStorage.getItem("compnayPlanTypeId");
      this.isFunded = sessionStorage.getItem('funded') == '1' ? true : false;
    }
    ngOnInit(){
      if(sessionStorage.getItem("companyId") != null){
          const _input={
            "Id": sessionStorage.getItem("companyId"),
            "LoggedInUserId": sessionStorage.getItem('loginUserId')
          };
          this.service.getCompanyDetails(_input)
          .subscribe((res:Response)=>{
            sessionStorage.setItem("isNewUser",'false');
            sessionStorage.setItem("DbCon",res['CompanyBasicaData']['DbCon']);
            sessionStorage.setItem("PlanTypeId",res['CompanyBasicaData']['PlanTypeId']);
            this.service.setCompnayName(true, res['CompanyBasicaData']['CompanyName']);
            this.taxData = res['TaxDetails'];
            console.log(  this.taxData);
            console.log( this.taxData['HST']);
            this.PPTax =   this.taxData['PPTax'] != 0 ?   this.taxData['PPTax'] : 0;
            this.RSTax =   this.taxData['RSTax'] != 0 ?   this.taxData['RSTax'] : 0;

             if(res['CompanyReferral'] != null){
               if(res['CompanyReferral']['ReferralTypeId'] == 1){
                 //  let a =   this.taxData['BrokerMinFeePct'] +   this.taxData['AdminMinFeePct'];
                  let a =   this.taxData['BrokerComissionCharges'] +   this.taxData['AdminMinFeePct'];
                  this.AdminFeePct = a != 0 ? a : 0;
               }else{
                  this.AdminFeePct =   this.taxData['AdminMaxFeePct'] != 0 ?   this.taxData['AdminMaxFeePct'] : 0;
               }
             }else{
                 this.AdminFeePct =   this.taxData['AdminMaxFeePct'] != 0 ?   this.taxData['AdminMaxFeePct'] : 0;
             }

            this.GST =   this.taxData['GST'] != 0 ?   this.taxData['GST'] : 0;
            this.HST =   this.taxData['HST'] != 0 ?   this.taxData['HST'] : 0;
            this.planType = this.formDataService.getWork();

            this.planData = res['PlanDetails'];

            if(this.planData != null){
                //this.planForm.controls['Plans'] = this.fb.array([]);
                //this.getPlanDetail(planData);
                this.classes = this.planData.length != 0 ?  this.planData.length : '';
                if(this.planData.length > 0){
                  this.planData.forEach((lineItem, index) => {
                    //let  netTotal = (parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit ? lineItem.RecurringDeposit : 0));
                    let TotatFeeAndTaxes = parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit)+ parseFloat(lineItem.GrossTotalInitial) + parseFloat(lineItem.GrossTotalMonthly);
                    let lineItemObj = {
                      "Name": lineItem.Name,
                      "AdministrationInitialFee":lineItem.AdministrationInitialFee,
                      "AdministrationMonthlyFee":lineItem.AdministrationMonthlyFee,
                      "CarryForwardPeriod":lineItem.CarryForwardPeriod,
                      "ContributionSchedule":lineItem.ContributionSchedule,
                      "GrossTotalInitial":lineItem.GrossTotalInitial,
                      "GrossTotalMonthly":lineItem.GrossTotalMonthly,
                      "HSTInitial":lineItem.HSTInitial,
                      "HSTMonthly":lineItem.HSTMonthly,
                      "InitialDeposit":lineItem.InitialDeposit,
                      "NetDeposit":lineItem.NetDeposit,
                      "PlanDesign":lineItem.PlanDesign,
                      "PlanStartDt":this.transformDate(lineItem.PlanStartDt),
                      "PremiumTaxInitial":lineItem.PremiumTaxInitial,
                      "PremiumTaxMonthly":lineItem.PremiumTaxMonthly,
                      "RecurringDeposit":lineItem.RecurringDeposit,
                      "RetailSalesTaxInitial":lineItem.RetailSalesTaxInitial,
                      "RetailSalesTaxMonthly":lineItem.RetailSalesTaxMonthly,
                      "RollOver":lineItem.RollOver,
                      "Termination":lineItem.Termination,
                      "TempPlanId" : lineItem.PlanId,
                      "PlanId":lineItem.PlanId,
                      "TotatFeeAndTaxes": TotatFeeAndTaxes
                    }

                    const control = <FormArray>this.planForm.controls['Plans'];
                    control.push(this.createPlanItem(1));
                    control.at(index).patchValue(lineItemObj);
                    console.log(lineItemObj);
                 });
               }else{
                 if(res['CompanyBasicaData']['PlanTypeId'] == 1){
                   this.classes = 1;
                   this.addClasses();
                 }
               }
            }
          });
          this.planType = this.formDataService.getWork();
          this.formDataService.getCompanyDetail();
      }
    }

    createPlanItem(i): FormGroup {

      let object = {
          "Name": this.returnClassName(i),
          "AdministrationInitialFee":[0],// InitialDeposit/10%
          "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
          "CarryForwardPeriod":['Indefinite Rollover to the benefit of the account holder'], //12 months
          "ContributionSchedule":[1], //Monthly 1, Yearly 2
          "GrossTotalInitial":[0], //over all initial total
          "GrossTotalMonthly":[0], // over all monthly total
          "HSTInitial":[0],// InitialDeposit/13%
          "HSTMonthly":[0], //RecurringDeposit/ 13%
          "InitialDeposit":['', [Validators.required, Validators.minLength(1)]],// InitialDeposit
          "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
          "PlanDesign":new FormControl(2),
          "PlanStartDt":[{date: { year: this.d.getFullYear(), month:  this.d.getMonth() + 1, day: this.d.getDate() }}, [Validators.required]],
          "PremiumTaxInitial":[0], //InitialDeposit/ 2%
          "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
          "RecurringDeposit": new FormControl('', [Validators.required,Validators.minLength(1)]),  //['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
          "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
          "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
          "RollOver":[4],
          "Termination":[6],
          "TempPlanId" : i,
          "PlanId": -1,
          "TotatFeeAndTaxes":[0]
      };
      return this.fb.group(object);
    }

    returnClassName(i){
      let name='';
        this.ClassName.forEach(function(x, index) {
            if (x.id === i) {
              name = x.name
            }
        });
        return name;
    }

    addClasses(){
      //remove all plans from arry
      if(this.classes == ''){
        this.planForm.value['Plans'] = [];
        const arr = <FormArray>this.planForm.controls.Plans;
        arr.controls = [];
        this. service.alertMessage("Please select atleast 1 class","error")
        return false;
      }
      const arr = <FormArray>this.planForm.controls.Plans;
      arr.controls = [];
      //end
      if(this.classes !=1){
        this.planForm.value['Plans'] = [];
        let len = this.classes;
        for (var i = 0; i < len; i++) {
          let Plans = this.planForm.get('Plans') as FormArray;
          Plans.push(this.createPlanItem(i+1));
        }
      }else{
        let Plans = this.planForm.get('Plans') as FormArray;
        Plans.push(this.createPlanItem(1));
      //  this.pager.count = this.planForm.value['Plans'].length;
      //  this.pager.count =this.pager.index + 1;
        //this.goTo(this.pager.index + 1);
      }
    }

    setCarryForward(event, obj){
      this.planForm.get('Plans')['controls'].forEach((plan)=>{
        if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
          plan.controls['CarryForwardPeriod'].patchValue('Indefinite Rollover to the benefit of the account holder');
        }
      })
    }

    //calculate taxes
    RecurringDeposit(obj){
      if(obj.value.RecurringDeposit !=''){
        this.planForm.get('Plans')['controls'].forEach((plan)=>{
          if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
            plan.controls['AdministrationMonthlyFee'].patchValue((obj.value.RecurringDeposit * this.AdminFeePct)/100);

            let pptamt = parseFloat(obj.value.RecurringDeposit) + parseFloat(plan.controls['AdministrationMonthlyFee'].value);
            plan.controls['PremiumTaxMonthly'].patchValue(( pptamt * this.PPTax)/100);

            if(this.HST != 0){
              //plan.controls['HSTMonthly'].patchValue((obj.value.RecurringDeposit *  this.HST)/100);
              plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.HST)/100);
            }else{
               plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.GST)/100);
            }

            if(this.planType == "Group"){
              plan.controls['RetailSalesTaxMonthly'].patchValue((obj.value.RecurringDeposit * this.RSTax)/100);
            }else{
              plan.controls['RetailSalesTaxMonthly'].patchValue(0)
            }

            let GrossTotalMonthly = (plan.controls['AdministrationMonthlyFee'].value + plan.controls['HSTMonthly'].value + plan.controls['PremiumTaxMonthly'].value + plan.controls['RetailSalesTaxMonthly'].value )
            // plan.controls['GrossTotalInitial'].patchValue();
             plan.controls['GrossTotalMonthly'].patchValue(GrossTotalMonthly);

             let netTotal = (parseFloat(obj.value.RecurringDeposit) +  parseFloat(plan.controls['InitialDeposit'].value ? plan.controls['InitialDeposit'].value : 0));
              plan.controls['NetDeposit'].patchValue(netTotal);
              plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);
            }
        })
      }else{
        this.service.alertMessage("Enter contribution amount for monthly or annual schedule","error");
        return false;
      }
    }

    InitialDeposit(obj){
      if(obj.value.InitialDeposit !=''){
      this.planForm.get('Plans')['controls'].forEach((plan)=>{
        console.log(plan.value);
        if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
          plan.controls['AdministrationInitialFee'].patchValue((obj.value.InitialDeposit * this.AdminFeePct)/100);
          //if hst 0 then apply GST
          if(this.HST != 0){
          //  plan.controls['HSTInitial'].patchValue((obj.value.InitialDeposit * this.HST)/100);
          plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.HST)/100);
          }else{
            plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.GST)/100);
          }

        //  let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
          //plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

          let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
          plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

          //if group then apply rst
          if(this.planType == "Group"){
            // plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);
            plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);

          }else{
            plan.controls['RetailSalesTaxInitial'].patchValue(0);
          }

          let GrossTotalInitial = (plan.controls['AdministrationInitialFee'].value + plan.controls['HSTInitial'].value + plan.controls['PremiumTaxInitial'].value + plan.controls['RetailSalesTaxInitial'].value )
          plan.controls['GrossTotalInitial'].patchValue(GrossTotalInitial);
          let  netTotal = (parseFloat(obj.value.InitialDeposit) +  parseFloat(plan.controls['RecurringDeposit'].value ? plan.controls['RecurringDeposit'].value : 0));

          plan.controls['NetDeposit'].patchValue(netTotal)

          plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);

        }
      })
    }else{
      this.service.alertMessage("Please enter initial deposit amount","error");
      return false;
    }
    }

    classA: any;
    setPlanSameAsA(e, obj){
      if(e.target.checked == true){
        this.planForm.get('Plans')['controls'].forEach((plan)=>{
          if(plan.value['Name'] == 'Class A'){
            this.classA =  plan.value;
          }
          if(plan.value['TempPlanId'] == obj.value.TempPlanId){
            console.log(this.classA);

            plan.controls['AdministrationInitialFee'].patchValue(this.classA['AdministrationInitialFee']);
            plan.controls['AdministrationMonthlyFee'].patchValue(this.classA['AdministrationMonthlyFee']);
            plan.controls['RecurringDeposit'].patchValue(this.classA['RecurringDeposit']);

            plan.controls['GrossTotalInitial'].patchValue(this.classA['GrossTotalInitial']);
            plan.controls['GrossTotalMonthly'].patchValue(this.classA['GrossTotalMonthly']);
            plan.controls['HSTInitial'].patchValue(this.classA['HSTInitial']);
            plan.controls['HSTMonthly'].patchValue(this.classA['HSTMonthly']);
            plan.controls['InitialDeposit'].patchValue(this.classA['InitialDeposit']);
            plan.controls['NetDeposit'].patchValue(this.classA['NetDeposit']);

            if(this.classA['PlanDesign'] != plan.value['PlanDesign']){
                plan.controls['PlanDesign'].patchValue(this.classA['PlanDesign']);
            }
            if(this.classA['ContributionSchedule'] != plan.value['ContributionSchedule']){
                  plan.controls['ContributionSchedule'].patchValue(this.classA['ContributionSchedule']);
            }

            if(this.classA['RollOver'] != plan.value['RollOver']){
                  plan.controls['RollOver'].patchValue(this.classA['RollOver']);
            }
            if(this.classA['CarryForwardPeriod'] != plan.value['CarryForwardPeriod']){
                  plan.controls['CarryForwardPeriod'].patchValue(this.classA['CarryForwardPeriod']);
            }
            if(this.classA['Termination'] != plan.value['Termination']){
                  plan.controls['Termination'].patchValue(this.classA['Termination']);
            }
            plan.controls['PlanStartDt'].patchValue(this.classA['PlanStartDt']);
            plan.controls['PremiumTaxInitial'].patchValue(this.classA['PremiumTaxInitial']);
            plan.controls['PremiumTaxMonthly'].patchValue(this.classA['PremiumTaxMonthly']);

            plan.controls['RetailSalesTaxInitial'].patchValue(this.classA['RetailSalesTaxInitial']);
            plan.controls['RetailSalesTaxMonthly'].patchValue(this.classA['RetailSalesTaxMonthly']);

            plan.controls['TotatFeeAndTaxes'].patchValue(this.classA['TotatFeeAndTaxes']);
          }
        })
      }
    }


    //Empty form group array
    getPlanDetail(data){
      if(data.length > 0) {
        this.planForm.controls['Plans'] = this.fb.array([]);
        this.setFormArryValues(data);
      }
    }

    setFormArryValues(data){
      let i=0;
      data.forEach((lineItem, index) => {
       // Create the item obj
       let lineItemObj = {
         "Name": lineItem.Name,
         "AdministrationInitialFee":lineItem.AdministrationInitialFee,
         "AdministrationMonthlyFee":lineItem.AdministrationMonthlyFee,
         "CarryForwardPeriod":lineItem.CarryForwardPeriod,
         "ContributionSchedule":lineItem.ContributionSchedule,
         "GrossTotalInitial":lineItem.GrossTotalInitial,
         "GrossTotalMonthly":lineItem.GrossTotalMonthly,
         "HSTInitial":lineItem.HSTInitial,
         "HSTMonthly":lineItem.HSTMonthly,
         "InitialDeposit":lineItem.InitialDeposit,
         "NetDeposit":lineItem.NetDeposit,
         "PlanDesign":lineItem.PlanDesign,
         "PlanStartDt":lineItem.PlanStartDt,
         "PremiumTaxInitial":lineItem.PremiumTaxInitial,
         "PremiumTaxMonthly":lineItem.PremiumTaxMonthly,
         "RecurringDeposit":lineItem.RecurringDeposit,
         "RetailSalesTaxInitial":lineItem.RetailSalesTaxInitial,
         "RetailSalesTaxMonthly":lineItem.RetailSalesTaxMonthly,
         "RollOver":lineItem.RollOver,
         "Termination":lineItem.Termination,
         "TempPlanId" : lineItem.PlanId,
         "PlanId":lineItem.PlanId
       }
       const control = <FormArray>this.planForm.controls['Plans'];
       control.push(this.createPlanItem(1));
       control.at(index).patchValue(lineItemObj);
     });
    }

    updatePlanItem(): FormGroup {
      return this.fb.group({
          "Name": [''],
          "AdministrationInitialFee":[0],// InitialDeposit/10%
          "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
          "CarryForwardPeriod":[''], //12 months
          "ContributionSchedule":[1], //Monthly 1, Yearly 2
          "GrossTotalInitial":[0], //over all initial total
          "GrossTotalMonthly":[0], // over all monthly total
          "HSTInitial":[0],// InitialDeposit/13%
          "HSTMonthly":[0], //RecurringDeposit/ 13%
          "InitialDeposit":['', [Validators.required, Validators.minLength(1)]],// InitialDeposit
          "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
          "PlanDesign":[1],
          "PlanStartDt":[{date: { year: this.d.getFullYear(), month:  this.d.getMonth() + 1, day: this.d.getDate() }}, [Validators.required]],
          "PremiumTaxInitial":[0], //InitialDeposit/ 2%
          "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
          "RecurringDeposit":['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
          "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
          "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
          "RollOver":[3],
          "Termination":[5],
          "TempPlanId" : [1],
          "PlanId": -1,
          "TotatFeeAndTaxes":[0]
      });
    }

    isLoading: boolean;
    saveDetail(planForm){
      if(this.classes == ''){
        this. service.alertMessage("Please select atleast 1 class","error")
        return false;
      }
      if (this.planForm.valid) {
         let typeId = sessionStorage.getItem("PlanTypeId") == 'Group' ? 2 : 1;
         this.planForm.value['Plans'].forEach((lineItem)=>{
           if(lineItem.PlanStartDt.formatted){
              lineItem['PlanStartDt']= lineItem.PlanStartDt.formatted;
           }else{
             if(typeof  lineItem.PlanStartDt === 'object'){
               let mm = parseInt(lineItem.PlanStartDt.date.month) < 10 ? '0'+lineItem.PlanStartDt.date.month: lineItem.PlanStartDt.date.month;
               let dd = parseInt(lineItem.PlanStartDt.date.day) < 10 ? '0'+lineItem.PlanStartDt.date.day: lineItem.PlanStartDt.date.day;
               lineItem['PlanStartDt']= mm+"/"+ dd +"/"+lineItem.PlanStartDt.date.year;
             }else{
               lineItem['PlanStartDt']= lineItem.PlanStartDt;
             }
           }

         });
         let _input ={
           "CompanyId": parseInt(sessionStorage.getItem("companyId")),
           "LoggedInUserId": sessionStorage.getItem('loginUserId') != null ? sessionStorage.getItem('loginUserId') : 1,
           "DbCon": sessionStorage.getItem("DbCon"),
           "PlanDetails":this.planForm.value['Plans'],
	         "PlanTypeId":typeId
         }
         console.log(_input);
         this.isLoading = true;
         this.service.addCompanyPlan(_input)
         .subscribe((res:Response)=>{
           if(res['Status'] == 'Success'){
             sessionStorage.setItem("stage",'3');
             this.service.alertMessage("Company plan details successfully saved",'error','2000');
             this.isLoading = false;
             this.router.navigate(['/register/member-detail']);
           }else{
             this.isLoading = false;
             this.service.alertMessage(res['Message'],'error','')
           }
         },(error)=>{
            this.service.alertMessage(error,'error','');
            this.isLoading = false;
         })

       } else {
           this.service.alertMessage("Please enter all the required fields","error","")
       }
    }

    _keyPress(event: any) {
       const pattern = /[0-9\+\-\ ]/;
       let inputChar = String.fromCharCode(event.charCode);
       if (event.keyCode != 8 && !pattern.test(inputChar)) {
         event.preventDefault();
       }
   }
   displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

   isFieldValid(field: string) {
      return !this.planForm.get(field).valid && this.planForm.get(field).touched;
   }
   validateAllFormFields(formGroup: FormGroup) {
     Object.keys(formGroup.controls).forEach(field => {
       console.log(field);
       const control = formGroup.get(field);
       if (control instanceof FormControl) {
         control.markAsTouched({ onlySelf: true });
       } else if (control instanceof FormGroup) {
         this.validateAllFormFields(control);
       }
     });
   }

   transformDateForFlatpickr(value){
     var datePipe = new DatePipe("en-US");
     value = datePipe.transform(value, 'yyyy-MM-dd');
     return value;
   }
   transformDate(value){
     var s=value;
     value=s.replace("AM"," AM")
     var datePipe = new DatePipe("en-US");
     //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
     value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
     let date = new Date(value);
      const cdate= {
           year: date.getFullYear(),
           month: date.getMonth() + 1,
           day: date.getDate()
       };
     let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
     return dt;
   }
}
