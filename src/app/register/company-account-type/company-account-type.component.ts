import { Component, OnInit } from '@angular/core';
import { Router }              from '@angular/router';
import { Refral }            from '../data/formData.model';
import { Address, Personal }             from '../data/formData.model';
import { environment } from '../../../environments/environment.prod';
import { FormDataService }     from '../data/formData.service';
import { CompanyService } from '../../company.service'
@Component({
  selector: 'app-company-account-type',
  templateUrl: './company-account-type.component.html',
  styleUrls: ['./company-account-type.component.css']
})
export class CompanyAccountTypeComponent implements OnInit {

  constructor(
    private router: Router,
    private formDataService: FormDataService,
     private service : CompanyService
   ) {

  }

  ngOnInit() {
  }

  setAccountType(pvarTypeId){
    sessionStorage.setItem("funded",pvarTypeId);
    this.router.navigate(['/register/plan']);
  }

}
