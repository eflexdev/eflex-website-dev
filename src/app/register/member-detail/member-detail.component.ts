import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { Admin, Address }                   from '../data/formData.model';
import { FormDataService }            from '../data/formData.service';
import {CompanyService} from '../../company.service'
import { Router }              from '@angular/router';
import {environment} from '../../../environments/environment.prod'
@Component ({
    selector:     'mt-wizard-member'
    ,templateUrl: './member-detail.component.html'
})

export class MemberDetailComponent implements OnInit {
    title = 'Member Details';
    //@Input() formData: FormData;
    isFormValid: boolean = false;
    employeeForm: FormGroup;
    admin: Admin;
    classesData: any=[];
    empData:any = [];
    adminData:any = [];
    planType :any;
    address: Address;
    isLoading: boolean;

    ownerEmail: string = '';
    ownerFirstName: string= '';
    ownerLastName: string = '';
    modal: boolean;
    hostUrl:string='';
    planTypeId: any = '-1';
    @ViewChild('fileInput') fileInput: ElementRef;
    constructor(
      private formDataService: FormDataService,
      private fb:FormBuilder,
      private service: CompanyService,
      private router: Router,
      private el : ElementRef
    ) {
        this.address = this.formDataService.getAddress();
        this.hostUrl = environment.hostUrl;

        this.planType = this.formDataService.getWork();
        this.planTypeId = sessionStorage.getItem("compnayPlanTypeId");

        if(sessionStorage.getItem("PlanTypeId") == '1'){
          this.employeeForm = this.fb.group({
                "ID":[''],
                "Employees": this.fb.array([this.createEmployee()]),
                "Admin": this.fb.array([])
          });
        }else{
          this.employeeForm = this.fb.group({
                "ID":[''],
                "Employees": this.fb.array([ this.createEmployee() ]),
                "Admin": this.fb.array([])
          });
        }
        this.getClasses();
  }

    emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    createEmployee(): FormGroup {
      return this.fb.group({
          "CompanyPlanId":[1],
      		"CompanyUserId":-1,
      		"ContactNo":[''],
      		"Email":['',[Validators.required, Validators.pattern(this.emailPattern)]],
      		"FName":['',[Validators.required]],
      		"LName":['',[Validators.required]],
      		"UserTypeId":[3],
      })
    }

    addAdmin(e){
      if(e.target.checked == true){
        let a = this.employeeForm.get('Admin') as FormArray;
        a.push(this.createAdmin());
      }else{
        // const arr = <FormArray>this.employeeForm.controls.Admin;
        // arr.controls = [];
          //this.employeeForm.controls['Admin'] = this.fb.array([]);
          const control=<FormArray>this.employeeForm.controls['Admin'];
          control.removeAt(0);
        // const control=<FormArray>this.employeeForm.controls['Employees'];
        // control.removeAt(index);
      }
    }

    createAdmin(): FormGroup {
      return this.fb.group({
      		"CompanyUserId":-1,
      		"ContactNo":[''],
      		"Email":[this.address.email,[Validators.required, Validators.pattern(this.emailPattern)]],
      		"FName":[this.address.firstName,[Validators.required]],
      		"LName":[this.address.lastName,[Validators.required]],
      		"UserTypeId":[2],
      })
    }

    ngOnInit() {
        if(sessionStorage.getItem("companyId") == null){
          this.service.alertMessage("Please fill company information first","error");
          this.router.navigate(['/register/personal']);
          return false;
        }
        if(sessionStorage.getItem("stage") != null){
          let StagesCovered = sessionStorage.getItem("stage");
          if(StagesCovered == '1'){
            this.service.alertMessage('Please choose "business type" first',"info","");
            this.router.navigate(['/register/personal']);
              return false;
          }
          if(StagesCovered == '2'){
            this.service.alertMessage('Please enter "plan detail" first',"info","");
            this.router.navigate(['/register/plan']);
            return false;
          }
        }
        const _input={
          "Id": sessionStorage.getItem("companyId"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.service.getCompanyDetails(_input)
        .subscribe((res:Response)=>{
          this.empData =  res['EmployeesData'];
          this.adminData =  res['AdminsData'];
          if(this.empData != null){
              // this.setEmpData(empData);
              //this.employeeForm.controls['Employees'] = this.fb.array([]);
              if(this.empData.length > 0){
                this.empData.forEach((lineItem, index) => {
                   // Create the item obj
                   let lineItemObj = {
                     "CompanyPlanId":lineItem.CompanyPlanId,
                     "ContactNo":'',
                      "CompanyUserId":-1,//lineItem.CompanyUserId,
                      "Email":lineItem.Email,
                      "FName":lineItem.FName,
                      "LName":lineItem.LName,
                      "UserTypeId":lineItem.UserTypeId,
                   }
                   const control = <FormArray>this.employeeForm.controls['Employees'];
                   control.push(this.createEmployee());
                   control.at(index).patchValue(lineItemObj);
                })
                this.deleteEmployee(this.empData.length)
                // ------------admin
                if(this.adminData.length > 0){
                    this.adminData.forEach((lineItem, index) => {
                       // Create the item obj
                       let lineItemObj = {
                         //"CompanyPlanId":lineItem.CompanyPlanId,
                         "ContactNo":'',
                          "CompanyUserId":-1,//lineItem.CompanyUserId,
                          "Email":lineItem.Email,
                          "FName":lineItem.FName,
                          "LName":lineItem.LName,
                          "UserTypeId":2,
                       }
                       const control = <FormArray>this.employeeForm.controls['Admin'];
                       control.push(this.createAdmin());
                       control.at(index).patchValue(lineItemObj);
                    })
                  }
              }else{
                if(res['CompanyBasicaData']['PlanTypeId'] == 1){
                  //this.deleteEmployee(0);
                }
                this.ownerLastName = res['CompanyBasicaData']['LastName'];
                this.ownerFirstName = res['CompanyBasicaData']['FirstName'];
                this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
                let lineItemObj = {
                  "CompanyPlanId":1,
                  "ContactNo":'',
                  "CompanyUserId":-1,
                  "Email":this.ownerEmail,
                  "FName":this.ownerFirstName,
                  "LName":this.ownerLastName,
                  "UserTypeId":3,
                }
                const control = <FormArray>this.employeeForm.controls['Employees'];
                //control.push(this.createEmployee());
                control.at(0).patchValue(lineItemObj);
              }
            }else{

              this.ownerLastName = res['CompanyBasicaData']['LastName'];
              this.ownerFirstName = res['CompanyBasicaData']['FirstName'];
              this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
              let lineItemObj = {
                "CompanyPlanId":1,
                "ContactNo":'',
                 "CompanyUserId":-1,
                 "Email":this.ownerEmail,
                 "FName":this.ownerEmail,
                 "LName":this.ownerEmail,
                 "UserTypeId":3,
              }
              const control = <FormArray>this.employeeForm.controls['Employees'];
              control.push(this.createEmployee());
              control.at(0).patchValue(lineItemObj);
            }
        });


    }


    getClasses(){
        if(sessionStorage.getItem("DbCon") == null){
          return false;
        }
        const _input = {
          "DbCon": sessionStorage.getItem("DbCon")
        }
        this.service.populateClasses(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.classesData = res['ClassList'];
          }else{
            this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          this.service.alertMessage(error,'error','')
        })
    }

    addMoreEmployee(){
      const control=<FormArray>this.employeeForm.controls['Employees'];
      control.push(this.createEmployee());
    }
    deleteEmployee(index:number){
        const control=<FormArray>this.employeeForm.controls['Employees'];
        control.removeAt(index);
    }

    submit() {

      //this.formData = this.formDataService.resetFormData();
        this.isFormValid = false;
    }

    AddEmployee(){
      console.log(this.employeeForm);
      if(this.employeeForm.valid){
        let _input ={
          "DbCon":sessionStorage.getItem("DbCon"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId'),
    	    "EmployeesList": this.employeeForm.value['Employees'],
          "AdminList": this.employeeForm.value['Admin']
        }
        console.log( JSON.stringify(_input, undefined, 2));
         this.isLoading = true;
        this.service.addEditEmployess(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.service.alertMessage("Company employees successfully added",'error','2000');
            this.isLoading = false;
            this.router.navigate(['/register/plan-review']);
            sessionStorage.setItem('employeeAdded','true');
          }else{
              this.service.alertMessage(res['Message'],'error','');
              this.isLoading = false;
          }
        },(error)=>{
          this.service.alertMessage(error,'error','');
          this.isLoading = false;

        })
      }
    }

    openModal(){
      this.modal = true;
    }

    uploadMsg = '';
    fileName: string= '';
    importLoaderModal: boolean;
    uploadFile(event) {
          this.uploadMsg = '';
          let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
          //get the total amount of files attached to the file input.
          let fileCount: number = inputEl.files.length;
          //create a new fromdata instance

          if (fileCount > 0) { // a file was selected
           var strFileName = String(inputEl.files.item(0).name);
           if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.xlsx)/i) != -1) {
              this.uploadMsg = "Please wait file uploading...";
              let formData = new FormData();
              formData.append('file', inputEl.files.item(0));
              formData.append('CompanyId', sessionStorage.getItem("companyId"));
              this.service.uploadFile(formData)
              .subscribe((result)=>{
                    this.uploadMsg = '';
                    var respnose = result._body;
                    if(respnose){
                      const img_name = respnose.split("###");
                        if(img_name[0] == 'Success'){
                            this.fileInput.nativeElement.value = '';
                             this.fileName = img_name[1];
                             //Call to service
                             const fileInput = {
                                "CompanyId": parseInt(sessionStorage.getItem("companyId")),
                                "DbCon": sessionStorage.getItem('DbCon'),
                                "FileName": img_name[1],
                                "LoggedInUserId": sessionStorage.getItem('loginUserId')
                             }
                             this.modal = false;
                             this.importLoaderModal = true;
                             this.service.importEmployeesExcelOnRegister(fileInput)
                             .subscribe((res:Response)=>{
                               if(res['Status'] == 'Success'){
                                    this.modal = false;
                                    this.importLoaderModal = false;
                                    this.service.alertMessage("Members successfully imported","success");
                                    this.router.navigate(['/register/plan-review']);
                                    sessionStorage.setItem('employeeAdded','true');
                               }else{
                                  this.service.alertMessage(res['Message'],"error");
                                  this.importLoaderModal = false;
                               }
                             }, (error:Error)=>{
                                console.log(error)
                                this.importLoaderModal = false;
                             });
                             //end

                        }else{
                          this.fileInput.nativeElement.value = '';
                          this.modal = false;
                          this.service.alertMessage("Members File upload encountered an error. Please try again.","error");
                        }
                    }
                  },
                  (error) =>{  console.log(error);  });
              }else{
                this.service.alertMessage("Please select a file with the extension .xlsx","error");
                return false;
              }
            }else{
                this.uploadMsg =" Please choose file";
            }
   }
}
