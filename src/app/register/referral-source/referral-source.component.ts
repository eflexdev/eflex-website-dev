import { Component, OnInit } from '@angular/core';
import { Router }              from '@angular/router';

import { Refral }            from '../data/formData.model';
import { FormDataService }     from '../data/formData.service';
import {CompanyService} from '../../company.service'
@Component({
  selector: 'app-referral-source',
  templateUrl: './referral-source.component.html',
  styleUrls: ['./referral-source.component.css']
})
export class ReferralSourceComponent implements OnInit {

  title = 'Company Information';
  Refral: Refral;
  form: any;
  provienceData:any[];
  disabledFields: boolean;
  codeDisable: boolean = true;
  borkers:any= [];
  referralData:any=[];

  constructor(private router: Router, private formDataService: FormDataService,
    private service : CompanyService,
  ) {

    if(sessionStorage.getItem("companyId") == null || sessionStorage.getItem("companyId") == undefined){
      this.router.navigate(['/register/personal']);
    }
      //this.formDataService.getCompanyDetail();
  }

  ngOnInit() {
      this.Refral = this.formDataService.getRefral();
      // if(sessionStorage.getItem("stage") != null){
      //   let StagesCovered = sessionStorage.getItem("stage");
      //     if(StagesCovered == '4'){
      //       this.service.alertMessage("You have already fill company detail","info","");
      //       this.router.navigate(['/payment-overview']);
      //     }
      // }
      const input={
        "DbCon": sessionStorage.getItem("DbCon"),
      }
      this.service.populateReferrals(input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.referralData = res['PopulateReferralsData'];
        }else{
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.service.alertMessage(error,'error','')
      })

      this.getBoker();
  }

  save(form: any): boolean {
      if (!form.valid) {
          return false;
      }
      this.formDataService.setRefral(this.Refral);
      return true;
  }
  isLoading:boolean;

  getBoker(){
    this.service.populateBrokers()
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.borkers = res['PopulateBrokers'];
      }else{
        this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      this.service.alertMessage(error,'error','')
    })
  }

  goToPrevious(form: any) {
      if (this.save(form)) {
          // Navigate to the personal page
          this.router.navigate(['/register/address']);
      }
  }

  referralChange(){
    this.Refral.refName = '';
  }

  name: string = '';
  otherDetail: string='';
  isOtherDetail: boolean = false;

  setOtherDetail(pvarType){
      this.otherDetail='';
      this.Refral.refName= '';
      if(pvarType == '6' || pvarType == '-1' || pvarType =='other' ){
        this.isOtherDetail = true;
      }else{
        this.isOtherDetail = false;
      }
   }
  goToNext(form: any) {
    this.name = '';
    this.otherDetail = '';
    if(this.Refral.refTypeId == '1'){
      if(this.Refral.refBrokerId ==  '' || this.Refral.refBrokerId == '-1'){
         if(this.Refral.refFName  == ''){
           this.service.alertMessage("Please enter broker first name","error");
           return false;
         }else{
           if(this.Refral.refLName !='' && this.Refral.refFName !=''){
             this.name = this.Refral.refFName +" "+ this.Refral.refLName;
           }else{
             this.name = this.Refral.refFName;
           }
         }
      }
    }
    if(this.Refral.refTypeId == '2'){
      if(this.Refral.refRadio == ''){
        this.service.alertMessage("Please select radio chanel","error");
        return false;
      }else{
          this.name = this.Refral.refRadio;
          if(this.Refral.refRadio == 'other'){
            if(this.Refral.refName == ''){
              this.service.alertMessage("Please enter other detail","error");
              return false;
            }else{
              this.otherDetail = this.Refral.refName;
            }
          }
      }
    }

    if(this.Refral.refTypeId == '3'){
      if(this.Refral.refTV == ''){
        this.service.alertMessage("Please select TV chanel","error");
        return false;
      }else{
          this.name = this.Refral.refTV;
          if(this.Refral.refTV == 'other'){
            if(this.Refral.refName == ''){
              this.service.alertMessage("Please enter other detail","error");
              return false;
            }else{
              this.otherDetail = this.Refral.refName;
            }
          }else{
            this.name = this.Refral.refTV;
          }
      }
    }
    if(this.Refral.refTypeId == '4'){
      if(this.Refral.refNewsPaper == ''){
        this.service.alertMessage("Please select newspaper","error");
        return false;
      }else{
          this.name = this.Refral.refNewsPaper;
          if(this.Refral.refNewsPaper == 'other'){
            if(this.Refral.refName == ''){
              this.service.alertMessage("Please enter other detail","error");
              return false;
            }else{
              this.otherDetail = this.Refral.refName;
            }
          }else{
            this.name = this.Refral.refNewsPaper;
          }
      }
    }
    if(this.Refral.refTypeId == '5'){
      if(this.Refral.refSocialMedia == ''){
        this.service.alertMessage("Please select social media","error");
        return false;
      }else{
          this.name = this.Refral.refSocialMedia;
          if(this.Refral.refSocialMedia == 'other'){
            if(this.Refral.refName == ''){
              this.service.alertMessage("Please enter other detail","error");
              return false;
            }else{
              this.otherDetail = this.Refral.refName;
            }
          }else{
            this.name = this.Refral.refSocialMedia;
          }
      }
    }
    if(this.Refral.refTypeId == '6'){
      if(this.Refral.refName == ''){
        this.service.alertMessage("Please enter other detail","error");
        return false;
      }else{
          this.otherDetail = this.Refral.refName;
      }
    }

    if (this.save(form)) {
          // Navigate to the work page
          //this.router.navigate(['/register/address']);
          const _input ={
          	"CompanyId": sessionStorage.getItem("companyId"),
          	"CompanyUserId":sessionStorage.getItem("loginUserId"),
          	"DbCon":sessionStorage.getItem("DbCon"),
          	"Email":"",
            "Name": this.name, //this.Refral.refName,
          	"BrokerId":this.Refral.refBrokerId == '' || this.Refral.refBrokerId  == '-1' ? -1 : this.Refral.refBrokerId,
            "OtherDetails":this.otherDetail,
          	"ReferralTypeId":this.Refral.refTypeId
          }
          console.log(_input);
          this.isLoading = true;
          this.service.saveCompanyReferrals(_input)
          .subscribe((res:Response)=>{
            if(res['Status'] == 'Success'){
              this.isLoading = false;
              this.service.alertMessage("Referral detail successfully submitted",'error','');
              this.router.navigate(['/register/business-type']);
            }else{
              this.isLoading = false;
              this.service.alertMessage(res['Message'],'error','')
            }
          },(error)=>{
            this.isLoading = false;
            this.service.alertMessage(error,'error','')
          })
      }
  }
  trackByFun(index, item){
   return item.ProvinceId;
  }
}
