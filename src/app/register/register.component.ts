import { Component, OnInit, HostListener} from '@angular/core';
import {CompanyService} from '../company.service';
import {Router} from "@angular/router";
import { Personal }            from './data/formData.model';
import { FormDataService }     from './data/formData.service';
import {MessagesService} from "../messages.service";
declare var $;
declare var jquery;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  loginUserName: string = '';
  personal: Personal;
  isChat: boolean = false;

  screenHeight:any;
  screenWidth:any;
  minusHeight:any= 0;

  constructor(
    private service: CompanyService, private router: Router,
    private formDataService: FormDataService,
    private msgService: MessagesService,
  ) {
      this.companyId = sessionStorage.getItem('companyId') != null ? sessionStorage.getItem('companyId') : -1;
      this.getScreenSize();
  }
  @HostListener('window:resize', ['$event'])

  getScreenSize(event?) {
        if(window.innerHeight > 500 && window.innerHeight < 1200){
          this.minusHeight = 100;
          this.screenHeight = window.innerHeight;
        }else{
          this.screenHeight = 200;
          this.minusHeight = 150;
        }

        this.screenWidth = window.innerWidth;
    }

  ngOnInit() {
     window.scrollTo(0, 0)
    this.formDataService.setPersonal(null);
    this.formDataService.setAddress(null);

    if(sessionStorage.getItem("stage") != null){
      let StagesCovered = sessionStorage.getItem("stage");
        // if(StagesCovered == '4'){
        //   this.service.alertMessage("You have already setup company","info","");
        //   this.router.navigate(['/payment-overview']);
        // }
        if(StagesCovered == '5'){
          this.service.alertMessage("You have already setup company","info","");
          sessionStorage.getItem("stage");
          this.router.navigate(['/employeer-portal/summary']);
        }
    }
    const ab = this.service.getEmittedValue()
    .subscribe((response: Response) =>{
       this.loginUserName  = response['userName'];
       sessionStorage.setItem("userName", response['userName']);
     });

     this.getMessageUser();
  }

  logOut(){
    sessionStorage.clear();
    this.service.alertMessage("You are logout successfully","success");
    this.router.navigate(['/join-us']);
  }

  adminUsers: any = [];
  eflexUserId: any = -1;
  getMessageUser(){
    this.msgService.listAllAdminUserForMsg()
    .subscribe((response:Response)=>{
      if(response['Status'] == "Success"){
        this.adminUsers = response['ListAdminUser'];
      }else{
          this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
      this.service.alertMessage(error,"error");
    })

  }

  companyId: any= -1;
  messagesData: any= [];
  cId: any  =-1;
  dbCon: any = -1
  loginUserId: any= -1;
  userTypeId: any= 1;
  recipientCompanyId: any=-1;
  messageText: string = '';
  recipientId: any = -1;
  recipientUserTypeId: any = -1;

  getMessage(){

      this.messagesData = [];

      this.adminUsers.forEach(obj => {
        if(obj['UserId'] == this.eflexUserId){
          this.recipientUserTypeId  = obj['UserTypeId'];
        }
      })
    const _input = {
        // "CId":this.cId,
        // "ChunkSize":10,
        // "ChunkStart":-1,
        // "CompanyId":sessionStorage.getItem('companyId'),
        // "UserId":sessionStorage.getItem('loginUserId'),
        // "UserTypeId": this.userTypeId,
      	// "DbCon":sessionStorage.getItem('DbCon'),
        "ChunkSize":1000,
      	"ChunkStart":-1,
      	"CompanyId":sessionStorage.getItem('companyId'),
      	"DbCon":"DbCon_"+ sessionStorage.getItem('companyId'),
      	"LoggedInUserId":sessionStorage.getItem('loginUserId'),
      	"LoggedInUserTypeId":this.userTypeId,
      	"SelectedUserId":this.eflexUserId,
        "SelectedUserTypeId":this.recipientUserTypeId,
      	"SelectedUserCompanyId":-1
    };
  //  console.log(JSON.stringify(_input, undefined, 2));
    //this.msgService.listAllConversationMessages(_input) //listConversationDetailsWithUser
    this.msgService.getConvBetweenTwoUsers(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.cId = res['Conversations']['CId'];
        this.messagesData = res['Conversations'][0]['Messages'];
        console.log(this.messagesData)
      }else{
        this.service.alertMessage(res['Message'],"error");
      }
    }, function (parameter) {
        throw new Error("Not implemented yet");
    });

     setTimeout(()=>{this.refreshChat(); }, 20000);
  }
  refreshChat(){
      if(sessionStorage.getItem('companyId') != null){
         let last = this.messagesData.length == 0 ? -1 : this.messagesData[this.messagesData.length -1]['MsgId'];
         let inputData = {
            "CId": this.cId,
            "CompanyId":sessionStorage.getItem('companyId'),
            "LatestMsgId":last,
            "DbCon": "DbCon_"+sessionStorage.getItem('companyId'),
            "UserId":sessionStorage.getItem('loginUserId'),
            "UserTypeId":1 //this.userTypeId
         }
         console.log(inputData);
         this.msgService.listAllConversationMessagesAjax(inputData)
         .subscribe((response: Response)=>{
           if(response['Status'] == 'Success'){
             let new_arr = [];
             new_arr = response['ConversationMessages'];
             if(new_arr.length > 0){
               for(let i = 0; i < new_arr.length; i++){
                 this.messagesData.push(new_arr[i]);
               }
             }
           }
         })
         setTimeout(()=>{this.refreshChat(); }, 20000);
       }
     }

   sendMessage(){
     let LatestMsgId = -1;
     if(this.messageText == ''){
       this.service.alertMessage("Type your message","error");
       return false;
     }
     if(this.eflexUserId == -1){
       this.service.alertMessage("Please select eflex admin","error");
       return false;
     }

     const _input = {
     	"CId":-1,
     	"DbCon":sessionStorage.getItem('DbCon'),
     	"LatestMsgId":LatestMsgId,
     	"Msg": this.textEncode(this.messageText),
     	"MsgFile":'',
     	"MsgFileType":'',
     	"RecipientCompanyId" : -1,//sessionStorage.getItem('companyId'),
     	"RecipientId": this.eflexUserId,
     	"RecipientUserTypeId":this.recipientUserTypeId,
     	"SenderCompanyId":sessionStorage.getItem('companyId'),
     	"SenderId":sessionStorage.getItem('loginUserId'),
     	"SenderUserTypeId": this.userTypeId
     };
     console.log(_input);
     this.msgService.sendMessage(_input)
     .subscribe((res:Response)=>{
       if(res['Status'] == 'Success'){
           console.log("Message send success");
           this.cId = res['ID'];
           this.messageText= '';
           //this.fileName = '';
           this.refreshChat();
       }else{
         this.service.alertMessage(res['Message'],"error")
       }
     }, function (parameter) {
         throw new Error("Not implemented yet");
     });
   }

   textDecode(pvarTxt){
     if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
         return this.service.decodeText(pvarTxt);
     }
   }

   textEncode(pvarTxt){
         if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
           return this.service.encodeText(pvarTxt);
           // return btoa(encodeURIComponent(pvarTxt).replace(/%([0-9A-F]{2})/g, (match, p1) => {
           //     return String.fromCharCode(("0x" + p1) as any);
           // }));
         }
     }
}
