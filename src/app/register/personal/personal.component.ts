import { Component, OnInit }   from '@angular/core';
import { Router }              from '@angular/router';
import { Refral }            from '../data/formData.model';
import { Address, Personal }             from '../data/formData.model';
import { environment } from '../../../environments/environment.prod';
import { FormDataService }     from '../data/formData.service';
import { CompanyService } from '../../company.service'

@Component ({
    selector:     'mt-wizard-personal'
    ,templateUrl: './personal.component.html'
})

export class PersonalComponent implements OnInit {
    title = 'Company Information';
    personal: Personal;
    form: any;
    provienceData:any[];
    disabledFields: boolean;
    codeDisable: boolean = true;

    address: Address;
    isLoading: boolean = false;;
    hostUrl: string = '';
    serviceUrl: string = '';

    referralData:any=[];
    Refral: Refral;
    workType: string;
    businessType: string;

    public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    constructor(private router: Router, private formDataService: FormDataService, private service : CompanyService ) {
        this.personal = this.formDataService.getPersonal();
        // if(this.personal.companyName == '' || this.personal.companyAddress == '' || this.personal.zip == '' || this.personal.state == ''){
        //   this.service.alertMessage("Please fill company information first","error","");
        //   this.router.navigate(['/register/personal']);
        // }

    }

    ngOnInit() {
      //  this.personal = this.formDataService.getPersonal();
        this.Refral = this.formDataService.getRefral();
        this.address = this.formDataService.getAddress();
        this.hostUrl = environment.hostUrl;
        this.serviceUrl = environment.serviceUrl;
        this.workType = this.formDataService.getWork();
        this.businessType =  this.formDataService.getBusiness();

        if(sessionStorage.getItem("stage") != null){
          let StagesCovered = sessionStorage.getItem("stage");
            if(StagesCovered == '4'){
              this.service.alertMessage("You have already fill company detail","info","");
              this.router.navigate(['/register/plan-review']);
            }
        }

        if(sessionStorage.getItem("companyId") != null){
          this.disabledFields = true;
        }else{
          if(sessionStorage.getItem("RandomCode") != null){
            setTimeout(()=>{
                this.personal.companyCode = sessionStorage.getItem("RandomCode");
            },2000)
          }else{
            this.service.getRandomCode()
            .subscribe((res:Response)=>{
              if(res['Status'] =="Success"){
                sessionStorage.setItem("RandomCode",res['RandomCode']);
                this.personal.companyCode = res['RandomCode'];
              }else{
                this.service.alertMessage("Some thing went worng","error")
              }
            });
          }
        }

        this.service.populateProvince()
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.provienceData = res['ProvinceList'];
          }else{
            //this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          //this.service.alertMessage(error,'error','')
        })

        this.populateReferrals();
    }

    validate(newValue){
      if(sessionStorage.getItem("companyId") != null){
        return false;
      }
      if(this.personal.companyCode ==''){
        this.service.alertMessage("Please enter company code","error",'1000');
        return false;
      }
      const _input={
        "UniqueCode": this.personal.companyCode
      }
      console.log(_input)
      this.service.ValidateUniqueCode(_input)
      .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            if(res['IsAlreadyExists']){
              this.service.alertMessage("This company code already exist. Enter another code","error","200");
              this.personal.companyCode = '';
            }
          }else{
              this.service.alertMessage(res['Message'],"error","200");
          }
      })
    }

    save(form: any): boolean {
        if (!form.valid) {
            this.service.alertMessage("Please fill all the required field","error");
            return false;
        }
        this.formDataService.setPersonal(this.personal);
        this.formDataService.setAddress(this.address);
        this.formDataService.setRefral(this.Refral);
        this.formDataService.setWork(this.workType);
        this.formDataService.setBusiness(this.businessType);
        sessionStorage.setItem("cmpnayInfo", "true");
        return true;
    }

    goToNext(form: any) {
      let companyId = sessionStorage.getItem("companyId") != null ? parseInt(sessionStorage.getItem("companyId")) : -1;

      if (this.save(form)) {
          if(this.address.email != this.address.confirm_email){
            this.service.alertMessage("Confirm Owner/Admin Email did not match with Owner/Admin Email","error");
            return false;
          }


          if(this.Refral.refTypeId !=  '' && this.Refral.refTypeId !=  '-1'){
            this.name = '';
            this.otherDetail = '';

            if(this.Refral.refTypeId == '1'){
              if(this.Refral.refBrokerId ==  '' || this.Refral.refBrokerId == '-1'){
                 if(this.Refral.refFName  == ''){
                   this.service.alertMessage("Please enter broker first name","error");
                   return false;
                 }else{
                   if(this.Refral.refLName !='' && this.Refral.refFName !=''){
                     this.name = this.Refral.refFName +" "+ this.Refral.refLName;
                   }else{
                     this.name = this.Refral.refFName;
                   }
                 }
              }
            }
            if(this.Refral.refTypeId == '2'){
              if(this.Refral.refRadio == ''){
                this.service.alertMessage("Please select radio chanel","error");
                return false;
              }else{
                  this.name = this.Refral.refRadio;
                  if(this.Refral.refRadio == 'other'){
                    if(this.Refral.refName == ''){
                      this.service.alertMessage("Please enter other detail","error");
                      return false;
                    }else{
                      this.otherDetail = this.Refral.refName;
                    }
                  }
              }
            }

            if(this.Refral.refTypeId == '3'){
              if(this.Refral.refTV == ''){
                this.service.alertMessage("Please select TV chanel","error");
                return false;
              }else{
                  this.name = this.Refral.refTV;
                  if(this.Refral.refTV == 'other'){
                    if(this.Refral.refName == ''){
                      this.service.alertMessage("Please enter other detail","error");
                      return false;
                    }else{
                      this.otherDetail = this.Refral.refName;
                    }
                  }else{
                    this.name = this.Refral.refTV;
                  }
              }
            }
            if(this.Refral.refTypeId == '4'){
              if(this.Refral.refNewsPaper == ''){
                this.service.alertMessage("Please select newspaper","error");
                return false;
              }else{
                  this.name = this.Refral.refNewsPaper;
                  if(this.Refral.refNewsPaper == 'other'){
                    if(this.Refral.refName == ''){
                      this.service.alertMessage("Please enter other detail","error");
                      return false;
                    }else{
                      this.otherDetail = this.Refral.refName;
                    }
                  }else{
                    this.name = this.Refral.refNewsPaper;
                  }
              }
            }
            if(this.Refral.refTypeId == '5'){
              if(this.Refral.refSocialMedia == ''){
                this.service.alertMessage("Please select social media","error");
                return false;
              }else{
                  this.name = this.Refral.refSocialMedia;
                  if(this.Refral.refSocialMedia == 'other'){
                    if(this.Refral.refName == ''){
                      this.service.alertMessage("Please enter other detail","error");
                      return false;
                    }else{
                      this.otherDetail = this.Refral.refName;
                    }
                  }else{
                    this.name = this.Refral.refSocialMedia;
                  }
              }
            }
            if(this.Refral.refTypeId == '6'){
              if(this.Refral.refName == ''){
                this.service.alertMessage("Please enter other detail","error");
                return false;
              }else{
                  this.otherDetail = this.Refral.refName;
              }
            }
          }

          const _input ={
            "Address1": this.personal.companyAddress,
            "Address2":'',
            "City":this.personal.city,
            "Code":this.personal.companyCode,
            "CompanyId": companyId,
            "CompanyName":this.personal.companyName,
            "Country":"",
            "FirstName":this.address.firstName,
            "HostUrl": this.hostUrl,
            "LastName":this.address.lastName,
            "OwnerContactNo":this.address.phone,
            "OwnerEmail":this.address.email,
            "PostalCode":this.personal.zip,
            "ProvinceId":this.personal.state,
            
            "ServiceUrl": this.serviceUrl, 
            "UserName": this.personal.companyCode+"_"+this.address.loginEmail
          }
          this.isLoading = true;
          this.service.addCompany(_input)
          .subscribe((res:Response)=>{
            if(res['Status'] == 'Success'){
              this.service.setCompnayName(true, this.personal.companyName);
              sessionStorage.setItem("companyId", res['CompanyId']);
              sessionStorage.setItem("stage", res['StagesCovered']);
              sessionStorage.setItem("loginUserId", res['CompanyUserId']);
              sessionStorage.setItem("DbCon",res['DbCon']);
              sessionStorage.setItem("UserToken", res['UserToken']);
              this.updateBusinessType();
              // this.service.alertMessage("Company registration step-1 completed",'error','');
              //this.router.navigate(['/register/plan']);
            }else{
              this.isLoading = false;
              this.service.alertMessage(res['Message'],'error','')
            }
          },(error)=>{
            this.isLoading = false;
            this.service.alertMessage(error,'error','')
          })
      }
    }


    updateBusinessType(){
      if(sessionStorage.getItem('companyId') != null){
          let businessType =  this.formDataService.getBusiness();
          const _input={
            "CompanyId":sessionStorage.getItem('companyId'),
            "IsCorporation": businessType =='Corporation' ? true : false,
            "PlanTypeId": this.workType == 'Group' ? 2 : 1
          }
          this.service.updateCompanyPlanType(_input)
          .subscribe((res:Response)=>{
            if(res['Status'] == 'Success'){
              sessionStorage.setItem("PlanTypeId",this.workType);
              let typeId = this.workType == 'Group' ? '2' : '1';
              sessionStorage.setItem("compnayPlanTypeId",typeId);
              this.addBroker();
              // this.isLoading = false;
              // this.service.alertMessage("Company registration step-2 completed",'error','');
              // this.router.navigate(['/register/result']);
            }else{
              this.isLoading = false;
              this.service.alertMessage(res['Message'],'error','')
            }
          },(error)=>{
            this.isLoading = false;
            this.service.alertMessage(error,'error','')
          })
      }
    }

    populateReferrals(){
      this.Refral = this.formDataService.getRefral();
      const input={
        "DbCon": ''//sessionStorage.getItem("DbCon"),
      }
      this.service.populateReferrals(input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.referralData = res['PopulateReferralsData'];
        }else{
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.service.alertMessage(error,'error','')
      })

      this.getBoker();
      setTimeout(()=>{
        this.formDataService.getCompanyDetail();
      },1500);

    }

    borkers: any = [];

    getBoker(){
      this.service.populateBrokers()
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.borkers = res['PopulateBrokers'];
        }else{
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.service.alertMessage(error,'error','')
      })
    }

    name: string = '';
    otherDetail: string='';
    isOtherDetail: boolean = false;

    referralChange(){
      this.Refral.refName = '';
    }

    setOtherDetail(pvarType){
        this.otherDetail='';
        this.Refral.refName= '';
        this.Refral.refBrokerId = '-1';
        if(pvarType == '6' || pvarType == '-1' || pvarType =='other' ){
          this.isOtherDetail = true;
        }else{
          this.isOtherDetail = false;
        }
     }

    addBroker() {
      if(this.Refral.refTypeId !=  '' && this.Refral.refTypeId !=  '-1'){
        this.name = '';
        this.otherDetail = '';

        if(this.Refral.refTypeId == '1'){
          if(this.Refral.refBrokerId ==  '' || this.Refral.refBrokerId == '-1'){
             if(this.Refral.refFName  == ''){
               this.service.alertMessage("Please enter broker first name","error");
               return false;
             }else{
               if(this.Refral.refLName !='' && this.Refral.refFName !=''){
                 this.name = this.Refral.refFName +" "+ this.Refral.refLName;
               }else{
                 this.name = this.Refral.refFName;
               }
             }
          }
        }
        if(this.Refral.refTypeId == '2'){
          if(this.Refral.refRadio == ''){
            this.service.alertMessage("Please select radio chanel","error");
            return false;
          }else{
              this.name = this.Refral.refRadio;
              if(this.Refral.refRadio == 'other'){
                if(this.Refral.refName == ''){
                  this.service.alertMessage("Please enter other detail","error");
                  return false;
                }else{
                  this.otherDetail = this.Refral.refName;
                }
              }
          }
        }

        if(this.Refral.refTypeId == '3'){
          if(this.Refral.refTV == ''){
            this.service.alertMessage("Please select TV chanel","error");
            return false;
          }else{
              this.name = this.Refral.refTV;
              if(this.Refral.refTV == 'other'){
                if(this.Refral.refName == ''){
                  this.service.alertMessage("Please enter other detail","error");
                  return false;
                }else{
                  this.otherDetail = this.Refral.refName;
                }
              }else{
                this.name = this.Refral.refTV;
              }
          }
        }
        if(this.Refral.refTypeId == '4'){
          if(this.Refral.refNewsPaper == ''){
            this.service.alertMessage("Please select newspaper","error");
            return false;
          }else{
              this.name = this.Refral.refNewsPaper;
              if(this.Refral.refNewsPaper == 'other'){
                if(this.Refral.refName == ''){
                  this.service.alertMessage("Please enter other detail","error");
                  return false;
                }else{
                  this.otherDetail = this.Refral.refName;
                }
              }else{
                this.name = this.Refral.refNewsPaper;
              }
          }
        }
        if(this.Refral.refTypeId == '5'){
          if(this.Refral.refSocialMedia == ''){
            this.service.alertMessage("Please select social media","error");
            return false;
          }else{
              this.name = this.Refral.refSocialMedia;
              if(this.Refral.refSocialMedia == 'other'){
                if(this.Refral.refName == ''){
                  this.service.alertMessage("Please enter other detail","error");
                  return false;
                }else{
                  this.otherDetail = this.Refral.refName;
                }
              }else{
                this.name = this.Refral.refSocialMedia;
              }
          }
        }
        if(this.Refral.refTypeId == '6'){
          if(this.Refral.refName == ''){
            this.service.alertMessage("Please enter other detail","error");
            return false;
          }else{
              this.otherDetail = this.Refral.refName;
          }
        }

        const _input ={
         "CompanyId": sessionStorage.getItem("companyId"),
         "CompanyUserId":sessionStorage.getItem("loginUserId"),
         "DbCon":sessionStorage.getItem("DbCon"),
         "Email":"",
         "Name": this.name, //this.Refral.refName,
         "BrokerId":this.Refral.refBrokerId == '' || this.Refral.refBrokerId  == '-1' ? -1 : this.Refral.refBrokerId,
         "OtherDetails":this.otherDetail,
         "ReferralTypeId":this.Refral.refTypeId
        }
        console.log(_input);
        this.service.saveCompanyReferrals(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.isLoading = false;
            this.service.alertMessage("Account information submitted successfully",'error','');
            //this.router.navigate(['/register/plan']);
            if(sessionStorage.getItem("isNewUser") != null){
                this.router.navigate(['/register/plan']);
            }else{
              this.router.navigate(['/congratulation']);
            }
          }else{
            this.isLoading = false;
            this.service.alertMessage(res['Message'],'error','')
          }
        },(error)=>{
          this.isLoading = false;
          this.service.alertMessage(error,'error','')
        })
      }else{
        this.isLoading = false;
        this.service.alertMessage("Account information submitted successfully",'error','');
        if(sessionStorage.getItem("isNewUser") != null){
            this.router.navigate(['/register/plan']);
        }else{
          this.router.navigate(['/congratulation']);
        }
      }

     }


    _keyPress(event: any) {
       const pattern = /[0-9\+\-\ ]/;
       let inputChar = String.fromCharCode(event.charCode);
       if (event.keyCode != 8 && !pattern.test(inputChar)) {
         event.preventDefault();
       }
   }
   trackByFun(index, item){
     return item.ProvinceId;
  }

  businessChange(event){
    console.log(event)
    this.workType = '';
  }
}
