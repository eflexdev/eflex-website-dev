import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy }   from '@angular/core';
import { Router }              from '@angular/router';
import { Address, Personal }             from '../data/formData.model';
import { FormDataService }     from '../data/formData.service';
import {CompanyService} from '../../company.service';
import {environment} from '../../../environments/environment.prod';
@Component ({
    selector:     'mt-wizard-address'
    ,templateUrl: './address.component.html',
    styles: ['.default-theme-previous:hover{color:white}']
})

export class AddressComponent implements OnInit {
    title = 'Contact Detail';
    address: Address;
    form: any;
    disabledFields: boolean;
    isLoading: boolean;
    personal:Personal;
    hostUrl: string = '';
    serviceUrl: string = '';

      public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
      constructor(private router: Router, private formDataService: FormDataService, private service : CompanyService ) {
        this.personal = this.formDataService.getPersonal();
        console.log(this.personal)
        if(this.personal.companyName == '' || this.personal.companyAddress == '' || this.personal.zip == '' || this.personal.state == ''){
          this.service.alertMessage("Please fill company information first","error","");
          this.router.navigate(['/register/personal']);
        }
    }

    ngOnInit() {
        this.address = this.formDataService.getAddress();
        this.hostUrl = environment.hostUrl;
        this.serviceUrl = environment.serviceUrl;
        if(sessionStorage.getItem("stage") != null){
          let StagesCovered = sessionStorage.getItem("stage");
            if(StagesCovered == '4'){
              this.service.alertMessage("You have already fill company detail","info","");
              this.router.navigate(['/payment-overview']);
            }
        }
        if(sessionStorage.getItem("companyId") != null){
          this.disabledFields = true;
        }
    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }
        this.formDataService.setAddress(this.address);
        return true;
    }

    goToPrevious(form: any) {
        if (this.save(form)) {
            // Navigate to the work page
            this.router.navigate(['/register/personal']);
        }
    }

    goToNext(form: any) {
        let companyId = sessionStorage.getItem("companyId") != null ? parseInt(sessionStorage.getItem("companyId")) : -1;
        this.isLoading = true;
        if (this.save(form)) {
            const _input ={
            	"Address1": this.personal.companyAddress,
            	"Address2":'',
            	"City":this.personal.city,
            	"Code":this.personal.companyCode,
              "CompanyId": companyId,
            	"CompanyName":this.personal.companyName,
            	"Country":"",
            	"FirstName":this.address.firstName,
            	"HostUrl": this.hostUrl,,
            	"LastName":this.address.lastName,
            	"OwnerContactNo":this.address.phone,
            	"OwnerEmail":this.address.email,
            	"PostalCode":this.personal.zip,
            	"ProvinceId":this.personal.state,
              
            	"ServiceUrl": this.serviceUrl,
            	"UserName": this.personal.companyCode+"_"+this.address.loginEmail
            }
            console.log(_input)

            this.service.addCompany(_input)
            .subscribe((res:Response)=>{
              if(res['Status'] == 'Success'){
                this.isLoading = false;
                this.service.setCompnayName(true, this.personal.companyName);
                sessionStorage.setItem("companyId", res['CompanyId']);
                sessionStorage.setItem("stage", res['StagesCovered']);
                //sessionStorage.setItem("RSTax", res['RSTax']);
                sessionStorage.setItem("loginUserId", res['CompanyUserId']);
                sessionStorage.setItem("DbCon",res['DbCon']);
                sessionStorage.setItem("UserToken", res['UserToken']);
                //sessionStorage.setItem("PPTax", res['PPTax']);
                this.service.alertMessage("Company registration step-1 completed",'error','');
                //this.router.navigate(['/register/business-type']);
                this.router.navigate(['/register/referral-source']);
              }else{
                this.isLoading = false;
                this.service.alertMessage(res['Message'],'error','')
              }
            },(error)=>{
              this.isLoading = false;
              this.service.alertMessage(error,'error','')
            })
        }
    }

    _keyPress(event: any) {
       const pattern = /[0-9\+\-\ ]/;
       let inputChar = String.fromCharCode(event.charCode);
       if (event.keyCode != 8 && !pattern.test(inputChar)) {
         event.preventDefault();
       }
   }


}
