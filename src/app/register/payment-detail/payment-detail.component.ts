import { Component, OnInit, ElementRef } from '@angular/core';
import { Router }              from '@angular/router';
import {CompanyService} from '../../company.service'
import { FormDataService }     from '../data/formData.service';
import { environment } from '../../../environments/environment.prod';
@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.css']
})
export class PaymentDetailComponent implements OnInit {

  loading:boolean;
  modal:boolean;
  bankname:any;instId: any;
  transId: any;
  accountNumber: any;
  forgetLoading:boolean;
  companyId: any;
  dbCon: any;
  initialPay:0;
  recuringPay: 0;
  loginUserName: string;
  iagree:boolean;
  btnDisable: boolean= false;
  uploadingFile:boolean;
  fileName: string = '';
  hostUrl: string;
  confrimModal: boolean;
  ownerEmail:string;
  userTypeId: any = 1;
  notificationModal: boolean = false;
  notificationConfrimationModal: boolean = false;

  sms: boolean= false;
  carriers:string='';
  phone: string='';
  isTimeLaps:boolean = true;

  companyUsername: string = '';
  companyPassword: string = '';
  openConditionModal: boolean = false;
  constructor( private service: CompanyService, private router: Router, private el: ElementRef) {
      this.hostUrl = environment.hostUrl;
   }

  ngOnInit() {
    if(sessionStorage.getItem("companyId") != null && sessionStorage.getItem('paymentDateSet') == 'true'){
      this.phone = sessionStorage.getItem('mobileNo');
      this.userTypeId = sessionStorage.getItem('userTypeId') != undefined ? sessionStorage.getItem('userTypeId') : 1;
      const _input={
        "Id": sessionStorage.getItem("companyId"),
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      };
      this.service.getCompanyDetails(_input)
      .subscribe((res:Response)=>{
        if(this.userTypeId == '1' ){
          this.loginUserName = res['CompanyBasicaData']['CompanyName'];
        }else{
            this.loginUserName = sessionStorage.getItem('userName');
        }
        this.companyUsername = res['CompanyBasicaData']['UserName'];
        this.companyPassword = res['CompanyBasicaData']['Password'];

        this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
        this.dbCon = res['CompanyBasicaData']['DbCon'];
        this.companyId = sessionStorage.getItem("companyId")
        this.initialPay= res['PaymentOverview']['InitialPayment'];
        this.recuringPay= res['PaymentOverview']['RecurringPayment'];
    })
    //this.notificationConfrimationModal = true;
  }else{
      this.service.alertMessage("Please update pyament date in plan review","error");
      this.router.navigate(['/register/plan-review']);
      return false;
    }
  }

  addBankDetail(form){
    //this.notificationModal = true;
    if(form.valid){
      if(form.controls.instId.value.length < 3){
        this.service.alertMessage("Institution ID must be at least 3 digits.","error");
        return false
      }
      if(form.controls.transId.value.length < 5){
        this.service.alertMessage("Transit ID must be at least 5 digits.","error");
        return false
      }

    const _input ={
      "AccountNumber": this.textEncode(form.controls.accountNumber.value),
      "BankName": this.textEncode(form.controls.bankname.value),
      "CompFinancialDetailId":-1,
      "CompanyId":this.companyId,
      "DbCon":this.dbCon,
      "FileName":this.fileName,
      "InitialPayment": this.userTypeId ==1 ? this.initialPay : 0,
      "InstitutionNumber":this.textEncode(form.controls.instId.value),
      "IsAgree":this.iagree,
      "IsCheque": this.fileName !='' ? true : false,
      "TransitNumber": this.textEncode(form.controls.transId.value),
      "RecurringPayment":this.userTypeId ==1 ? this.recuringPay : 0,
      "CompanyUserId":  sessionStorage.getItem("loginUserId")
    }
    console.log(_input)
    this.loading = true;
    this.service.addCompanyFinancialDetails(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
          this.loading = false;
          this.service.alertMessage("PAP details submitted successfully.","success");
          sessionStorage.setItem("papSubmited", 'true');
          //this.router.navigate(['/congratulation']);
          this.sendMailToEmployee();
          this.gotoDashboard();
      }else{
          this.loading = false;
          this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }else{
    this.service.alertMessage("Plase complete all required fields","error","");
    return false;
  }
  }

  textDecode(pvarTxt){
    if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
        return this.service.decodeText(pvarTxt);
    }
  }

  textEncode(pvarTxt){
      if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
        return this.service.encodeText(pvarTxt);
      }
  }

  confirmPayment(){
    this.confrimModal = false;
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 openModal(){
   this.modal = true;
 }
 closeFileModal(){
   this.modal = false;
   this.removeFile();
 }
 removeFile(){
   this.fileName = '';
   this.el.nativeElement.querySelector('#file').value= '';
   this.el.nativeElement.value = '';
 }

 uploadMsg = '';
 uploadFile(event) {
       this.uploadMsg = '';
       let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
       //get the total amount of files attached to the file input.
       let fileCount: number = inputEl.files.length;
       //create a new fromdata instance
       let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('CompanyId', this.companyId);
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.modal = false;
                        this.fileName = img_name[1];
                         // this.profileForm.patchValue({
                         //   'Img': img_name[1]
                         // });
                         this.service.alertMessage("Void cheque successfully upload","success");
                     }else{
                       this.service.alertMessage("Void cheque upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg ="Please select the file for your void cheque";
         }
}

  togglePassword(pvarType){
    if(pvarType == 'ti'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
    if(pvarType == 'in'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
    if(pvarType == 'acc'){
       var x = document.getElementById(pvarType);
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
    }
  }

  sendMailToEmployee(){
    const _input ={
      "Id":this.companyId,
  	  "LoggedInUserId":sessionStorage.getItem("loginUserId")
    }
    this.service.sendFinalMailToEmployee(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){

      }else{
        this.service.alertMessage(res['Message'],'error','');
      }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }
  gotoDashboard(){
      const _input ={
        "UserName": this.companyUsername,//.replace(/\s/g, ""),
        "Password": this.companyPassword//.replace(/\s/g, "")
      }
      this.service.loginUser(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          if(res['InActive'] == true){
            //this.service.alertMessage("You are Login successfully",'error','');
            sessionStorage.setItem("companyId",res['CompanyId']);
            sessionStorage.setItem("stage",res['StagesCovered']);
            sessionStorage.setItem("DbCon",res['DbCon']);
            sessionStorage.setItem("companyName", res['CompanyName']);
            sessionStorage.setItem("userName", res['Name']);
            sessionStorage.setItem("userTypeId", res['UserTypeId']);
            sessionStorage.setItem("uniqueCode", res['UniqueCode']);
            sessionStorage.setItem("amountDue", res['AmountDue']);
            sessionStorage.setItem("loginUserId", res['LoggedInUserId']);
            sessionStorage.setItem("compnayPlanTypeId", res['PlanTypeId']);
            sessionStorage.setItem("isCorporation", res['IsCorporation']);
            sessionStorage.setItem("UserToken", res['UserToken']);
            this.service.setActiveToken(res['UserToken']);
            if(res['UserTypeId'] == 1){
              if(res['StagesCovered'] == 5  && res['UserTypeId'] == 1){
                this.router.navigate(['/employeer-portal/analytics']);
              }
            }
            //admin
            else if(res['UserTypeId'] == 2){
              if(res['StagesCovered'] == 5){
                this.router.navigate(['/employeer-portal/analytics']);
              }
            }
          }
      }else{
        this.loading = false;
        this.service.alertMessage(res['Message'],'error','');
      }
      },(error)=>{
          this.loading = false;
          this.service.alertMessage(error,'error','')
      })
  }

}
