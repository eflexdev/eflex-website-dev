import { Component, OnInit }   from '@angular/core';
import { Router }              from '@angular/router';

import { FormDataService }     from '../data/formData.service';
import {CompanyService} from '../../company.service';
import {IMyDpOptions} from 'mydatepicker';
@Component ({
    selector:     'mt-wizard-work',
    templateUrl: './work.component.html'
})

export class WorkComponent implements OnInit {
  paymentData: any = [];
  loading: boolean;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  loginUserName: string='';
  TotalDueAmount: any = 0;
  //paymentDate: any;
  public paymentDate;

  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     //dateFormat: 'yyyy/mm/dd',
     editableDateField: false,
     disableWeekends: true,
     disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()+2},
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+2, day: this.d.getDate()+1}
 };

  constructor(private service: CompanyService, private router: Router) {

    let  date : any = new Date();
    date.setDate( date.getDate() + 4 );
    let dayOfweek = date.getDay();
    if(dayOfweek == 6){
      date.setDate( date.getDate() + 2 );
      console.log(date);
      this.paymentDate =  {date: { year: date.getFullYear(), month:date.getMonth()+1, day: date.getDate() +1 }}
    }else if(dayOfweek == 7){
        date.setDate( date.getDate() + 1 );
        console.log(date);
        this.paymentDate =  {date: { year: date.getFullYear(), month:date.getMonth()+1, day: date.getDate()+1 }}

    }else{
        date.setDate( date.getDate()+0 );
        console.log(date);
        this.paymentDate =  {date: { year: date.getFullYear(), month:date.getMonth()+1, day: date.getDate()+1 }}
      }
    // if(this.d.getDay()+3 >= 6){
    //   newdt = this.d.getDay()+2;
    // }else{
    //   newdt = this.d.getDay()+3;
    // }
    // let mm = date.getMonth() + 1;
    // if(date.getDate() + newdt >= 30){
    //    mm = date.getMonth() + 2;
    //    newdt = this.d.getDay()+3
    // }


    // this.paymentDate =  {date: { year: date.getFullYear(), month:mm, day: date.getDate() + newdt }}
    this.loginUserName = sessionStorage.getItem("userName");
  }

  ngOnInit() {
    if(sessionStorage.getItem("companyId") != null){

      if(sessionStorage.getItem('employeeAdded') == 'true'){
        const _input = {
          "DbCon":sessionStorage.getItem("DbCon")
        }
        this.service.getPaymentOverview(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == "Success"){
            this.NetGrossInitial = res['NetGrossInitial'];
            this.NetGrossRecurring = res['NetGrossRecurring'];
            this.TotalDueAmount = parseFloat(res['NetGrossInitial'])+ parseFloat(res['NetGrossRecurring']);
            this.paymentData = res['ClasswisePayments'];
          }
        });
      }else{
        this.service.alertMessage("Please add members first","error");
        this.router.navigate(['/register/member-detail']);
        return false;
      }
    }else{
      this.service.alertMessage("Please fill company information first","error");
      this.router.navigate(['/register/personal']);
      return false;
    }
  }
  updatePaymenDate(){
    if(this.paymentDate == null){
      this.service.alertMessage("Select when you would like the amount pulled from your account","error");
      return false;
    }
    let date = '';
    if(typeof  this.paymentDate === 'object'){
      let mm = parseInt(this.paymentDate.date.month) < 10 ? '0'+this.paymentDate.date.month: this.paymentDate.date.month;
      let dd = parseInt(this.paymentDate.date.day) < 10 ? '0'+this.paymentDate.date.day: this.paymentDate.date.day;
       date = mm+"/"+ dd +"/"+this.paymentDate.date.year;
    }else{
       date= this.paymentDate;
    }

    if(sessionStorage.getItem("DbCon") != null){
      const _input = {
        "CompanyId": sessionStorage.getItem('companyId'),
        "DbCon":sessionStorage.getItem("DbCon"),
        "PaymentDate":date,
      };
      this.loading = true;
      console.log(_input);
      this.service.updatePaymentDate(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
           this.loading = false;
           sessionStorage.setItem('paymentDateSet','true');
           this.service.alertMessage('Payment date update successfully',"success");
           this.router.navigate(['/register/payment-detail']);
        }
      });
    }else{
      this.router.navigate(['/login']);
    }
  }
}
