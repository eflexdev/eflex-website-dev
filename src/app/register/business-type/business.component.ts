import { Component, OnInit }   from '@angular/core';
import { Router }              from '@angular/router';
import {CompanyService} from '../../company.service'
import { FormDataService }     from '../data/formData.service';

@Component ({
    selector:     'mt-wizard-business'
    ,templateUrl: './business.component.html'
})

export class BusinessComponent implements OnInit {
    title = 'Business Ownership';
    businessType: string;
    form: any;

    constructor(private router: Router, private formDataService: FormDataService, private service: CompanyService) {
      if(sessionStorage.getItem('companyId') == null){
        this.service.alertMessage("Please fill company contact detail","error");
        this.router.navigate(['/register/personal']);
      }
    }

    ngOnInit() {
      if(sessionStorage.getItem("stage") != null){
        let StagesCovered = sessionStorage.getItem("stage");
          if(StagesCovered == '4'){
            this.service.alertMessage("You have already choose business type","info","");
            this.router.navigate(['/bank-detail']);
          }
        }
        this.businessType =  this.formDataService.getBusiness();
        console.log('Address feature loaded!');

    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }
        this.formDataService.setBusiness(this.businessType);
        return true;
    }

    goToPrevious(form: any) {
        if (this.save(form)) {
            // Navigate to the personal page
            this.router.navigate(['/register/address']);
        }
    }

    goToNext(form: any) {
        if (this.save(form)) {
            // Navigate to the address page
            this.router.navigate(['/register/plan']);
        }
    }
}
