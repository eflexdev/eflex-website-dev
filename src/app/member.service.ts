import { Injectable, Output,EventEmitter } from '@angular/core';
import {environment} from '../environments/environment.prod'
import { Headers, Http, Response } from '@angular/http';
import { Router }              from '@angular/router';
import 'rxjs/Rx';
import { Observable,Subject } from 'rxjs';
declare var Noty: any;
import {CompanyService} from './company.service'

@Injectable()
export class MemberService {
  authToken: any;
  serviceUrl:string;
  public headers; //= new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})

  constructor( private http : Http, private _router: Router, private companyService : CompanyService ) {
     this.serviceUrl = environment.serviceUrl;
     const ab = this.companyService.getUserToken()
     .subscribe((response: Response) =>{
       this.authToken = response; //sessionStorage.getItem('UserToken')
      });
     this.headers = new Headers({'Content-Type': 'application/json','TokenHeader': this.authToken})
  }
  getEmpDashboardTransactions(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetEmpDashboardTransactions',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  getEmpTransactionData(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})

    return this.http.post(this.serviceUrl+'/GetEmpTransactionData',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  getEmpDashboardData(inputData){
    inputData.newId = 1;
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetEmpDashboardData',
    inputData,{headers: headers})
    .map(
      (response: Response) => {
        const data = response.json();
        if(data['Status']=='Error'){
          this.companyService.alertMessage(data['Message'],'error');
          this._router.navigate(['/'])
        }else{
          return data;
        }
    }).catch((error)=>{
        return error;
    })
  }

  getEmpDetailsById(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetEmpDetailsById',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  updateEmpBasicDetails(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/UpdateEmpBasicDetails',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addEmpDependents(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddEmpDependents',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listOfDependents(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListOfDependents',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  populateDependants(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})

    return this.http.post(this.serviceUrl+'/PopulateDependants',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
}


populateClaimType(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/PopulateClaimType',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
addClaim(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/AddClaim',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

listOfClaims(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ListOfClaims',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}


deleteDependents(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/DeleteDependents',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
createCard(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/CreateCard',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
activateCardStatus(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ActivateCardStatus',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
listOfServices(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ListOfServices',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

loadValue(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/LoadValue',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

uploadReceipt(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/UploadReceipt',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

getNearbyServiceProvider(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetNearbyServiceProvider',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

addServiceProvider(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/AddServiceProvider',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

addService(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddService',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listCardTransactions(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
      return this.http.post(this.serviceUrl+'/ListCardTransactions',
      inputData,{headers: headers})
      .map(
        (response: Response) => { const data = response.json();
          return data;
      }).catch((error)=>{
          return error;
      })
    }

    getEmpCardList(inputData){
      let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
        return this.http.post(this.serviceUrl+'/GetEmpCardList',
        inputData,{headers: headers})
        .map(
          (response: Response) => { const data = response.json();
            return data;
        }).catch((error)=>{
            return error;
        })
      }

      getEmpCardDetails(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
          return this.http.post(this.serviceUrl+'/GetEmpCardDetails',
          inputData,{headers: headers})
          .map(
            (response: Response) => { const data = response.json();
              return data;
          }).catch((error)=>{
              return error;
          })
      }

      getCardData(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
          return this.http.post(this.serviceUrl+'/GetCardData',
          inputData,{headers: headers})
          .map(
            (response: Response) => { const data = response.json();
              return data;
          }).catch((error)=>{
              return error;
          })
      }

      getNearByProvider(){
        return this.http.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=51.2538,85.3232&radius=15000&type=hospital&keyword=%27%27&key=',{})
        .map(
          (response: Response) => { const data = response.json();
            return data;
        }).catch((error)=>{
            return error;
        })
      }

      uploadReceiptV2(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
          return this.http.post(this.serviceUrl+'/UploadReceiptV2',
          inputData,{headers: headers})
          .map(
            (response: Response) => { const data = response.json();
              return data;
          }).catch((error)=>{
              return error;
          })
      }

      updateDependant(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
        return this.http.post(this.serviceUrl+'/UpdateDependant',
        inputData,{headers: headers})
        .map(
          (response: Response) => { const data = response.json();
            return data;
        }).catch((error)=>{
            return error;
        })
      }

      loadValue_New(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
        return this.http.post(this.serviceUrl+'/LoadValue_New',
        inputData,{headers: headers})
        .map(
          (response: Response) => { const data = response.json();
            return data;
        }).catch((error)=>{
            return error;
        })
      }
      updateLoadValueDetails(inputData){
        let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
        return this.http.post(this.serviceUrl+'/UpdateLoadValueDetails',
        inputData,{headers: headers})
        .map(
          (response: Response) => { const data = response.json();
            return data;
        }).catch((error)=>{
            return error;
        })
      }
}
