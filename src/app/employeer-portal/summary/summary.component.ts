import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  chart: Chart;
  capacity_chart: any;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  plansData: any=[];
  plansChart: any;
  companyName: string;
  isPending: boolean = true;
  isCurrent: boolean = true;
  isContribution: boolean= true;
  companyCode:string;
  amountDue: any = 0;
  availableCredits: any=0
  colorArr:any = [];
  currentTransaction: any = [];
  pendingTransaction: any = [];

  constructor(private service: CompanyService, private router : Router) { }

  ngOnInit() {

    if(sessionStorage.getItem("DbCon") != null){
      this.companyName = sessionStorage.getItem("companyName");
      this.companyCode = sessionStorage.getItem("uniqueCode");
      this.amountDue = sessionStorage.getItem("amountDue");
      const _input = {
        "DbCon":sessionStorage.getItem("DbCon")
      }

      this.service.getPaymentOverview(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
          this.NetGrossInitial = res['NetGrossInitial'];
          this.NetGrossRecurring = res['NetGrossRecurring'];
          if(res['ClasswisePayments'].length > 0){
             for(var k=0; k< res['ClasswisePayments'].length; k++){
               const obj=  res['ClasswisePayments'][k];
               const offData={};
               offData['name'] = obj['PlanName'];
               offData['y']=obj.NoOfEmployees;
               offData['sliced']= k == 0 ? true : false;
               offData['selected']= k == 0 ? true : false;

               //this.plansData.push(offData)
               let employee = obj['NoOfEmployees']; //obj['NoOfEmployees'] == 0 ? 1 : obj['NoOfEmployees'];
               this.colorArr.push(obj['Color']);
               this.plansData.push([obj['PlanName'], employee])
            };
                   //console.log(this.officeReportData)
        }
        this.showPlnsChart();
        this.getTransaction();
        }
    });

  }else{
    this.router.navigate(['/login']);
  }
  }

  showPlnsChart(){
    console.log(this.plansData);
    this.plansChart = new Chart({
      chart: {
         type: 'pie',
         options3d: {
             enabled: true,
             alpha: 300,

         },

     },
     title: {
         text: 'Plans Overview'
     },
     legend: {
        enabled:false
     },
     subtitle: {
         text: ''
     },
     plotOptions: {
         pie: {
             innerSize: 150,
             depth: 200,
             showInLegend: false
         },

     },
     credits: {
        enabled: false
    },

     colors:this.colorArr, //['#eee', '#fa9619', 'green', 'blue', 'purple', 'brown'],
     series: [{
         name: 'Employees',
         data: this.plansData
         //]
     }]
   });
    // this.plansChart = new Chart({
    //       chart: {
    //           plotBackgroundColor: null,
    //           plotBorderWidth: null,
    //           plotShadow: false,
    //           type: 'pie'
    //       },
    //       title: {
    //           text: 'Plans Overview'
    //       },
    //       tooltip: {
    //           pointFormat: '{series.name}: <b>{point.y}</b>'
    //       },
    //       credits: {
    //           enabled: false
    //       },
    //       exporting: {
    //           enabled: false
    //       },
    //
    //       plotOptions: {
    //           pie: {
    //               allowPointSelect: true,
    //               cursor: 'pointer',
    //               dataLabels: {
    //                   enabled: true,
    //                   format: 'No of Employees:{point.y}',
    //                   style: {
    //                       //  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    //                   }
    //               },
    //               showInLegend: true
    //           }
    //       },
    //       series: [{
    //           name: 'Number Of Rooms',
    //           //colorByPoint: true,
    //           data: this.plansData
    //       }]
    //   });

    this.getContribution();
  }

  contributionData:any;
  getContribution(){
    const _input={
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId')
    };
    this.service.getCompanyDashboardData(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.contributionData = res['GetDashboardData']
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })
  }

  planIndex: any = -1;
  toggleClass(object){
    this.planIndex = object.Plan;
  }
  getTransaction(){
    const _input={
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId')
    };
    this.service.getDashboardTransactions(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.amountDue= res['AmountDue'];
        this.availableCredits = res['AvailableCredits'];
        this.currentTransaction = res['CurrentTransaction'];
        this.pendingTransaction = res['PendingTransaction'];
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })
  }
}
