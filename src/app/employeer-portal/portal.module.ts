import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }        from '@angular/forms';
import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import { PortalRoutingModule } from './portal-routing.module';
import { EmployeerPortalComponent } from '../employeer-portal/employeer-portal.component';

import { SummaryComponent } from './summary/summary.component';
import { ActivityComponent } from './activity/activity.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MyDate, DateTime, FilterUserPipe, dateFormat,OrderByPipe} from "../date-pipe";
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { SettingsComponent } from './settings/settings.component';
import { EStatementsComponent } from './e-statements/e-statements.component';
import { MessagesComponent } from './messages/messages.component';
import { NotificationComponent } from './notification/notification.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { MyDatePickerModule } from 'mydatepicker';
import { TextMaskModule } from 'angular2-text-mask';
import { AnalyticsComponent } from './analytics/analytics.component';
import { OffersComponent } from '../offers/offers.component';
import { FAQComponent } from '../faq/faq.component';
import {EmojiPickerModule} from 'ng-emoji-picker';
import { EmployeeClaimsComponent } from './employee-claims/employee-claims.component';
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    PortalRoutingModule,
    FormsModule,
    ChartModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    //SettingeModule
    TextMaskModule,
    EmojiPickerModule
  ],
  declarations: [
    EmployeerPortalComponent,
    SummaryComponent,
    ActivityComponent,
    HeaderComponent,
    FooterComponent,
    MyDate,
    DateTime,
    FilterUserPipe,
    dateFormat,
    OrderByPipe,
    EmployeesListComponent,
    SettingsComponent,
    EStatementsComponent,
    MessagesComponent,
    NotificationComponent,
    AddEmployeeComponent,
    AnalyticsComponent,
    OffersComponent,
    FAQComponent,
    EmployeeClaimsComponent,
    //ContactInfoComponent
  ],
  providers: [ ]
})
export class PortalModule { }
