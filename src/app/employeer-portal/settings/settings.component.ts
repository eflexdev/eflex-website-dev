import { Component, OnInit, ElementRef } from '@angular/core';
import {CompanyService} from '../../company.service';
import {DatePipe} from "@angular/common";
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
import {IMyDpOptions} from 'mydatepicker';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { Router }              from '@angular/router';
import {environment} from '../../../environments/environment.prod'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  activeTab: any = 1;
  companyId: any = -1;
  openConditionModal: boolean = false;
  planData: any = [];
  RSTax:number = 0
  PPTax:number= 0;
  HST: number =0;
  AdminFeePct: number =0;
  GST: number =0;
  planType:any;


  NetGrossInitial: any;
  NetGrossRecurring: any;
  planForm: FormGroup;
  addPlanForm: FormGroup;
  ClassName:any= [];
  //contact
  provienceData:any;
  city: string = '';
  state : string = '';
  address : string = '';
  street : string = '';
  companyName: string='';
  zip:string='';
  companyCode:string='';
  planTypeId: any = '';

  //----------
  refTypeId: string = '';
  refName: string='';
  refBrokerId: string='';
  OtherDetails:string = '';

  firstName:string='';
  lastName:string='';
  loginEmail:string='';
  loginConfirmEmail:string='';
  pwd: string='';
  confirmPwd:string='';
  email: string = '';
  phone: string='';
  userName: string= '';

  //changePwd
  cploading: boolean;
  oldPwd:string = ''
  newPwd: string= '';
  newCPwd: string= '';

  //PAP
  bankname:any;instId: any;
  transId: any;
  accountNumber: any;
  iagree:boolean;
  btnDisable: boolean= false;
  uploadingFile:boolean;
  fileName: string = '';
  hostUrl: string;
  modal:boolean;
  PAPDetailId: any = -1;
  borkers: any=[];
  referralData: any=[];
  isFreeze: boolean = false;
  isPlanEdit: any = -1;

  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()},
     markCurrentDay: true,
     editableDateField: false,
     disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth(), day: this.d.getDate()-1}
     //dateFormat: 'yyyy/mm/dd',
     //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };

  // data-ng-pattern="/^[a-zA-Z0-9\u00C0-\u017F-'.\s]+$/"
  constructor( private service: CompanyService, private router: Router, private el: ElementRef,   private fb:FormBuilder) {
      this.hostUrl = environment.hostUrl;
      this.isFreeze = sessionStorage.getItem("isFreeze") == 'true' ? true : false;
      this.planForm = this.fb.group({
            "ID":[''],
            "Plans":  this.fb.array([ ]) // this.createPlanItem(1)
      });

      this.addPlanForm = this.fb.group({
            "ID":[''],
            "Plans":  this.fb.array([]) // this.createPlanItem(1)
      });
      this.ClassName=[
        {"id":1,"name": "Class A"},{"id":2,"name": "Class B"}, {"id":3,"name": "Class C"},
        {"id":4,"name": "Class D"}, {"id":5,"name": "Class E"}, {"id":6,"name": "Class F"},
        {"id":7,"name": "Class G"}, {"id":8,"name": "Class H"}, {"id":9,"name": "Class I"},
        {"id":10,"name": "Class J"}, {"id":11,"name": "Class K"},{"id":12,"name": "Class L"},
        {"id":13,"name": "Class M"},{"id":14,"name": "Class N"},{"id":15,"name": "Class O"},
        {"id":16,"name": "Class P"},{"id":17,"name": "Class Q"},{"id":18,"name": "Class R"},
        {"id":19,"name": "Class S"},{"id":20,"name": "Class T"},{"id":21,"name": "Class U"},
        {"id":22,"name": "Class V"},{"id":23,"name": "Class W"},{"id":24,"name": "Class X"},
        {"id":25,"name": "Class Y"},{"id":26,"name": "Class Z"}
     ]

   }

  ngOnInit() {
    if(sessionStorage.getItem('companyId') != null){
      this.companyId = sessionStorage.getItem('companyId');
    }else{
      this.service.alertMessage("Please login first","error");
      this.router.navigate(['/']);
    }
    this.service.populateProvince()
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.provienceData = res['ProvinceList'];
        this.getReferralSource();
        this.getCompanyDetails()
      }else{
        //this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      //this.service.alertMessage(error,'error','')
    })
  }

  getReferralSource(){
    const input={
      "DbCon":sessionStorage.getItem("DbCon"),
    }
    this.service.populateReferrals(input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.referralData = res['PopulateReferralsData'];
        this.getBoker();
      }else{
        this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      this.service.alertMessage(error,'error','')
    })
  }
  getBoker(){
    this.service.populateBrokers()
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.borkers = res['PopulateBrokers'];
      }else{
        this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      this.service.alertMessage(error,'error','')
    })
  }

  isOtherName: boolean = false;
  getCompanyDetails(){
      this.isLoading = true;
      if(sessionStorage.getItem("companyId") != null){
        const _input={
          "Id": sessionStorage.getItem("companyId"),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.planData = [];
        this.service.getCompanyDetails(_input)
        .subscribe((res:Response)=>{
          this.isLoading = false;
          let obj = res['CompanyBasicaData'];
          this.companyName = obj['CompanyName'];
          this.companyCode = obj['Code'];
          this.address = obj['Address1'];
          this.street = obj['Address2'];
          this.city = obj['City'];
          this.state = obj['ProvinceId'];
          this.zip = obj['PostalCode'];

          this.planTypeId = obj['PlanTypeId'];
          this.firstName = obj['FirstName'];
          this.lastName = obj['LastName'];
          this.email = obj['OwnerEmail'];
          this.phone = obj['OwnerContactNo'];
           let u = obj['UserName'].split("_");
           this.userName = u['1'];
          //CompanyReferral
          if(res['CompanyReferral'] != null && res['CompanyReferral'] != ''){
            this.refTypeId= res['CompanyReferral']['ReferralTypeId'];
            this.refName = res['CompanyReferral']['Name'];
            this.refBrokerId= res['CompanyReferral']['BrokerId'];
            this.OtherDetails =  res['CompanyReferral']['OtherDetails'];
            if(res['CompanyReferral']['Name'] == 'other' && res['CompanyReferral']['ReferralTypeId'] != '1'){
              this.isOtherName = true;
            }
            if(res['CompanyReferral']['ReferralTypeId'] == '6'){
              this.isOtherName = true;
            }

           if(res['CompanyReferral']['ReferralTypeId'] == '1' && res['CompanyReferral']['BrokerId'] == '-1')  {
                this.isOtherName = true;
            }


          }
          let taxData = res['TaxDetails'];
          if(res['CompanyReferral'] != null){
            if(res['CompanyReferral']['ReferralTypeId'] == 1){
              //  let a =   this.taxData['BrokerMinFeePct'] +   this.taxData['AdminMinFeePct'];
               let a =   taxData['BrokerComissionCharges'] +   taxData['AdminMinFeePct'];
               this.AdminFeePct = a != 0 ? a : 0;
            }else{
               this.AdminFeePct =   taxData['AdminMaxFeePct'] != 0 ?   taxData['AdminMaxFeePct'] : 0;
            }
          }else{
              this.AdminFeePct =   taxData['AdminMaxFeePct'] != 0 ?   taxData['AdminMaxFeePct'] : 0;
          }

          //plan detail START
          this.PPTax = taxData['PPTax'] != 0 ? taxData['PPTax'] : 0;
          this.RSTax = taxData['RSTax'] != 0 ? taxData['RSTax'] : 0;
          //this.AdminFeePct = taxData['AdminMaxFeePct'] != 0 ? taxData['AdminMaxFeePct'] : 0;
          this.GST = taxData['GST'] != 0 ? taxData['GST'] : 0;
          this.HST = taxData['HST'] != 0 ? taxData['HST'] : 0;
          this.planType = obj['PlanTypeId'] == 2 ? 'Group': 'Personal';


          this.planData = res['PlanDetails'];
          if(this.planData != null){
              //this.planForm.controls['Plans'] = this.fb.array([]);
              //this.getPlanDetail(planData);
            //this.classes = this.planData.length != 0 ?  this.planData.length : '';
              if(this.planData.length > 0){
                this.planData.forEach((lineItem, index) => {
                  //let  netTotal = (parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit ? lineItem.RecurringDeposit : 0));
                  let TotatFeeAndTaxes = parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit)+ parseFloat(lineItem.GrossTotalInitial) + parseFloat(lineItem.GrossTotalMonthly);
                  let RecurringAmount= 0;
                  if(this.HST > 0){
                     RecurringAmount = lineItem.RecurringDeposit + parseFloat(lineItem.RecurringDeposit) * (this.AdminFeePct + this.HST + this.PPTax + this.RSTax) / 100;
                  }else{
                     RecurringAmount = lineItem.RecurringDeposit + parseFloat(lineItem.RecurringDeposit) * (this.AdminFeePct + this.GST + this.PPTax + this.RSTax) / 100;
                  }
                  let lineItemObj = {
                    "Name": lineItem.Name,
                    "AdministrationInitialFee":lineItem.AdministrationInitialFee,
                    "AdministrationMonthlyFee":lineItem.AdministrationMonthlyFee,
                    "CarryForwardPeriod":lineItem.CarryForwardPeriod,
                    "ContributionSchedule":lineItem.ContributionSchedule,
                    "GrossTotalInitial":lineItem.GrossTotalInitial,
                    "GrossTotalMonthly":lineItem.GrossTotalMonthly,
                    "HSTInitial":lineItem.HSTInitial,
                    "HSTMonthly":lineItem.HSTMonthly,
                    "InitialDeposit":lineItem.InitialDeposit,
                    "NetDeposit":lineItem.NetDeposit,
                    "PlanDesign":lineItem.PlanDesign,
                    "PlanStartDt": this.transformDate(lineItem.PlanStartDt),
                    "PremiumTaxInitial":lineItem.PremiumTaxInitial,
                    "PremiumTaxMonthly":lineItem.PremiumTaxMonthly,
                    "RecurringDeposit":lineItem.RecurringDeposit,
                    "RetailSalesTaxInitial":lineItem.RetailSalesTaxInitial,
                    "RetailSalesTaxMonthly":lineItem.RetailSalesTaxMonthly,
                    "RollOver":lineItem.RollOver,
                    "Termination":lineItem.Termination,
                    "TempPlanId" : lineItem.PlanId,
                    "PlanId":lineItem.PlanId,
                    "TotatFeeAndTaxes": TotatFeeAndTaxes,
                    "RecurringAmount": RecurringAmount
                  }

                  const control = <FormArray>this.planForm.controls['Plans'];
                  control.push(this.createPlanItem(1));
                  control.at(index).patchValue(lineItemObj);
                 console.log(lineItemObj);
               });
             }else{
               if(res['CompanyBasicaData']['PlanTypeId'] == 1){
                // this.addClasses();
               }
             }
          }
        })
      }
  }

  updatecompanyInfo(form){
      if(form.valid){
        const _input = {
          "Address1":form.controls.address.value,
        	"Address2":"",
        	"City":form.controls.city.value,
        	"Code":form.controls.companyCode.value,
        	"CompanyId":this.companyId,
        	"CompanyName":form.controls.companyName.value,
        	"Country":"",
        	"FirstName":form.controls.firstName.value,
        	"HostUrl":"",
        	"LastName":form.controls.lastName.value,
        	"OwnerContactNo":form.controls.phone.value,
        	"OwnerEmail":form.controls.email.value,
        	"PostalCode":form.controls.zip.value,
        	"ProvinceId":form.controls.state.value,
        	"ServiceUrl":"",
        	"UserId":sessionStorage.getItem("loginUserId"),
        	"UserName":form.controls.companyCode.value+"_"+ form.controls.userName.value
        };
        console.log(_input);
        this.loading = true;
        this.service.updateCompanyDetails(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            this.loading = false;
            sessionStorage.setItem("userName", form.controls.firstName.value);
            sessionStorage.setItem("companyName", form.controls.companyName.value);
            this.service.alertMessage("Information successfully updated","success");
          }else{
            this.loading = false;
            this.service.alertMessage(res['Message'],"error");
          }
        });
    }else{
      this.loading = false;
      this.service.alertMessage("Plase complete all required fields","error");
      return false;
    }
  }

  returnClassName(i){
    let name='';
      this.ClassName.forEach(function(x, index) {
          if (x.id === i) {
            name = x.name
          }
      });
      return name;
  }

  createPlanItem(i): FormGroup {
    return this.fb.group({
        "Name": this.returnClassName(i),
        "AdministrationInitialFee":[0],// InitialDeposit/10%
        "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
        "CarryForwardPeriod":['Indefinite Rollover to the benefit of the account holder'], //12 months
        "ContributionSchedule":[1], //Monthly 1, Yearly 2
        "GrossTotalInitial":[0], //over all initial total
        "GrossTotalMonthly":[0], // over all monthly total
        "HSTInitial":[0],// InitialDeposit/13%
        "HSTMonthly":[0], //RecurringDeposit/ 13%
        "InitialDeposit":[''],// InitialDeposit
        "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
        "PlanDesign":new FormControl(2),
        "PlanStartDt":['', [Validators.required]],
        "PremiumTaxInitial":[0], //InitialDeposit/ 2%
        "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
        "RecurringDeposit": new FormControl('', [Validators.required,Validators.minLength(1)]),  //['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
        "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
        "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
        "RollOver":[4],
        "Termination":[6],
        "TempPlanId" : i,
        "PlanId": -1,
        "TotatFeeAndTaxes":[0],
        "RecurringAmount":[0]
    });
  }

  setActiveTab(pvarType){
    this.activeTab = pvarType;
    if(pvarType == '3'){
    //  this.getPlan();
    }
    if(pvarType == '4'){
      this.getPaymentDetail();
    }
  }

  menuOption: any = 1;
  setActiveTabOnChange(){
    this.activeTab = this.menuOption;
    if(this.menuOption == '3'){

    }
    if(this.menuOption == '4'){
      this.getPaymentDetail();
    }
  }

  //cpwed
  changePwd(form){
    if(form.valid){
      if(form.controls.newPwd.value !=  form.controls.newCPwd.value){
        return false;
      }
      const _input = {
      	"CompanyId":sessionStorage.getItem('companyId'),
      	"DbCon":sessionStorage.getItem("DbCon"),
      	"NewPassword":form.controls.newPwd.value,
      	"PrevPassword":form.controls.oldPwd.value,
      	"UserId":sessionStorage.getItem("loginUserId")
      }
      this.cploading = true;
      this.service.changePassword(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.cploading = false;
            form.reset();
            this.service.alertMessage("Password successfully changed.",'error','');
        }else{
          this.cploading = false;
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.cploading = false;
        this.service.alertMessage(error,'error','')
      })
    }else{
      return false;
    }
  }

  getPlan(){
    if(sessionStorage.getItem("DbCon") != null){
    // const _input = {
    //   "DbCon":sessionStorage.getItem("DbCon")
    // }
    // this.service.getPaymentOverview(_input)
    // .subscribe((res:Response)=>{
    //   if(res['Status'] == "Success"){
    //     this.NetGrossInitial = res['NetGrossInitial'];
    //     this.NetGrossRecurring = res['NetGrossRecurring'];
    //     this.paymentData = res['ClasswisePayments'];
    //   }
    // });
  }else{
        this.router.navigate(['/login']);
      }
  }

  getPaymentDetail(){
    if(sessionStorage.getItem("DbCon") != null){
        const _input = {
          "DbCon":sessionStorage.getItem("DbCon"),
          "Id": sessionStorage.getItem("loginUserId")
        }
        this.service.getPAPDetails(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == "Success"){
            if(res['GetPAPDetails'] !=null){
              let obj = res['GetPAPDetails'];
              this.bankname = this.textDecode(obj['BankName']);
              this.accountNumber = this.textDecode(obj['AccountNumber']);
              this.transId = this.textDecode(obj['TransitId']);
              this.instId = this.textDecode(obj['InstitutionId']);
              this.iagree = obj['IsAgree'];
              this.fileName = obj['ChequeFile'];
              this.PAPDetailId = obj['PAPDetailId'];
            }
          }
        });
      }else{
          this.router.navigate(['/login']);
      }
  }

  setCarryForward(event, obj){
    this.planForm.get('Plans')['controls'].forEach((plan)=>{
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['CarryForwardPeriod'].patchValue('');
      }
    })
  }
  //calculate taxes
  RecurringDeposit(obj){
    if(obj.value.RecurringDeposit !=''){
      this.planForm.get('Plans')['controls'].forEach((plan)=>{
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['AdministrationMonthlyFee'].patchValue((obj.value.RecurringDeposit * this.AdminFeePct)/100);

        let pptamt = parseFloat(obj.value.RecurringDeposit) + parseFloat(plan.controls['AdministrationMonthlyFee'].value);
        plan.controls['PremiumTaxMonthly'].patchValue(( pptamt * this.PPTax)/100);

        if(this.HST != 0){
          //plan.controls['HSTMonthly'].patchValue((obj.value.RecurringDeposit *  this.HST)/100);
          plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.HST)/100);
        }else{
           plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.GST)/100);
        }

        if(this.planType == "Group"){
          plan.controls['RetailSalesTaxMonthly'].patchValue((obj.value.RecurringDeposit * this.RSTax)/100);
        }else{
          plan.controls['RetailSalesTaxMonthly'].patchValue(0)
        }

        let GrossTotalMonthly = (plan.controls['AdministrationMonthlyFee'].value + plan.controls['HSTMonthly'].value + plan.controls['PremiumTaxMonthly'].value + plan.controls['RetailSalesTaxMonthly'].value )
        // plan.controls['GrossTotalInitial'].patchValue();
         plan.controls['GrossTotalMonthly'].patchValue(GrossTotalMonthly);

         let netTotal = (parseFloat(obj.value.RecurringDeposit) +  parseFloat(plan.controls['InitialDeposit'].value ? plan.controls['InitialDeposit'].value : 0));
          plan.controls['NetDeposit'].patchValue(netTotal);
          plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);

          //FOR Recurring Amount
          if(this.HST > 0){
             let RecurringAmt =  parseFloat(obj.value.RecurringDeposit) + parseFloat(obj.value.RecurringDeposit) * ( this.AdminFeePct + this.HST + this.PPTax + this.RSTax) / 100;
             plan.controls['RecurringAmount'].patchValue( RecurringAmt );
          }else{
            let RecurringAmt =  parseFloat(obj.value.RecurringDeposit) + parseFloat(obj.value.RecurringDeposit) * ( this.AdminFeePct + this.GST + this.PPTax + this.RSTax) / 100;
            plan.controls['RecurringAmount'].patchValue( RecurringAmt);
          }
        }


    })
  }else{
    this.service.alertMessage("Enter contribution amount for monthly or annual schedule","error");
    return false;
  }
  }

  InitialDeposit(obj){
      if(obj.value.InitialDeposit !=''){
        this.planForm.get('Plans')['controls'].forEach((plan)=>{
      console.log(plan.value);
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['AdministrationInitialFee'].patchValue((obj.value.InitialDeposit * this.AdminFeePct)/100);
        //if hst 0 then apply GST
        if(this.HST != 0){
        //  plan.controls['HSTInitial'].patchValue((obj.value.InitialDeposit * this.HST)/100);
        plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.HST)/100);
        }else{
          plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.GST)/100);
        }

      //  let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        //plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        //if group then apply rst
        if(this.planType == "Group"){
          // plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);
          plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);

        }else{
          plan.controls['RetailSalesTaxInitial'].patchValue(0);
        }

        let GrossTotalInitial = (plan.controls['AdministrationInitialFee'].value + plan.controls['HSTInitial'].value + plan.controls['PremiumTaxInitial'].value + plan.controls['RetailSalesTaxInitial'].value )
        plan.controls['GrossTotalInitial'].patchValue(GrossTotalInitial);
        let  netTotal = (parseFloat(obj.value.InitialDeposit) +  parseFloat(plan.controls['RecurringDeposit'].value ? plan.controls['RecurringDeposit'].value : 0));

        plan.controls['NetDeposit'].patchValue(netTotal)

        plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);

        //FOR Recurring Amount
        if(this.HST > 0){
         let RecurringAmt =  parseFloat(obj.value.InitialDeposit) + parseFloat(obj.value.InitialDeposit) * ( this.AdminFeePct + this.HST + this.PPTax + this.RSTax) / 100;
         plan.controls['RecurringAmount'].patchValue(RecurringAmt);
        }else{
          let RecurringAmt =  parseFloat(obj.value.InitialDeposit) + parseFloat(obj.value.InitialDeposit) * ( this.AdminFeePct + this.HST + this.PPTax + this.RSTax) / 100;
          plan.controls['RecurringAmount'].patchValue(RecurringAmt);
        }
      }
    })
    }else{
      this.service.alertMessage("Enter initial deposit amount","error");
      return false;
    }
}

  isLoading:boolean;
  updateBtnId: any = -1;
  disabled: boolean = true;
  // Calling this disables the component
   onDisableComponent() {
       this.disabled = true;
   }
  updatePlanDetail(obj){
    console.log(obj);
    //this.isLoading= true;
    if(obj.value.PlanStartDt.formatted){
       obj.value['PlanStartDt']= obj.value.PlanStartDt.formatted;
    }else{
      if(typeof  obj.value.PlanStartDt === 'object'){
        let mm = parseInt(obj.value.PlanStartDt.date.month) < 10 ? '0'+obj.value.PlanStartDt.date.month: obj.value.PlanStartDt.date.month;
        let dd = parseInt(obj.value.PlanStartDt.date.day) < 10 ? '0'+obj.value.PlanStartDt.date.day: obj.value.PlanStartDt.date.day;
        obj.value['PlanStartDt']= mm+"/"+ dd +"/"+obj.value.PlanStartDt.date.year;
      }else{
        obj.value['PlanStartDt']= obj.value.PlanStartDt;
      }
    }
    obj.value['DbCon'] = sessionStorage.getItem('DbCon');
    this.updateBtnId = obj.value.TempPlanId;
    const _input = obj.value;
    console.log(JSON.stringify(_input, undefined, 2));
    this.isLoading = true;
    this.service.updatePlanDetails(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
          this.updateBtnId  = -1;
          this.service.alertMessage("Plan detail successfully update.","success")
          this.isLoading = false;
      }else{
          this.isLoading = false;
          this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
        this.isLoading = false;
        this.service.alertMessage(error,'error','')
    })
  }
  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 openModal(){
   this.modal = true;
 }
 closeFileModal(){
   this.modal = false;
   this.removeFile();
 }

 removeFile(){
   this.fileName = '';
   this.el.nativeElement.querySelector('#file').value= '';
   this.el.nativeElement.value = '';
 }

 uploadMsg = '';
 uploadFile(event) {
       this.uploadMsg = '';
       let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
       //get the total amount of files attached to the file input.
       let fileCount: number = inputEl.files.length;
       //create a new fromdata instance
       let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('CompanyId', this.companyId);
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.modal = false;
                        this.fileName = img_name[1];
                         // this.profileForm.patchValue({
                         //   'Img': img_name[1]
                         // });
                         this.service.alertMessage("Void cheque successfully upload","success");
                     }else{
                       this.service.alertMessage("Void cheque upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg ="Please select the file for your void cheque";
         }
}

loading: boolean;
updatePAPDetail(form){
  if(form.valid){
    if(form.controls.instId.value.length < 3){
      this.service.alertMessage("Institution ID must be at least 3 digits.","error");
      return false
    }
    if(form.controls.transId.value.length < 5){
      this.service.alertMessage("Transit ID must be at least 5 digits.","error");
      return false
    }

  const _input ={
    "AccountNumber":this.textEncode( form.controls.accountNumber.value),
    "BankName":this.textEncode( form.controls.bankname.value),
    "ChequeFile":this.fileName,
    "CompanyId":sessionStorage.getItem('companyId'),
    "DbCon": sessionStorage.getItem('DbCon'),
    "InstitutionId": this.textEncode(form.controls.instId.value),
    "IsAgree":this.iagree,
    "IsCheque": this.fileName !='' ? true : false,
    "PAPDetailId":this.PAPDetailId,
    "TransitId":this.textEncode( form.controls.transId.value),
  }
  console.log(_input)
  this.loading = true;
  this.service.updatePAPDetails(_input)
  .subscribe((res:Response)=>{
    if(res['Status'] == 'Success'){
        this.service.alertMessage("Payment detail successfully updated.","success")
        this.loading = false;
    }else{
        this.loading = false;
        this.service.alertMessage(res['Message'],'error','')
    }
  },(error)=>{
      this.loading = false;
      this.service.alertMessage(error,'error','')
  })
}else{
  this.service.alertMessage("Plase complete all required fields","error","");
  return false;
  }
}

transformDateForFlatpickr(value){
  var datePipe = new DatePipe("en-US");
  value = datePipe.transform(value, 'yyyy-MM-dd');
  return value;
}

transformDate(value){
  var s=value;
  value=s.replace("AM"," AM")
  var datePipe = new DatePipe("en-US");
  //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
  value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
  let date = new Date(value);
   const cdate= {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()
    };
  let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
  return dt;
}
planModal: boolean;
openAddPlan(){
  this.planModal = true;
  this.addPlanForm = this.fb.group({
        "ID":[''],
        "Plans":  this.fb.array([this.addNewPlanItem(this.planData.length+1)]) // this.createPlanItem(1)
  });
}

addNewPlanItem(i): FormGroup {
  return this.fb.group({
      "Name": this.returnClassName(i),
      "AdministrationInitialFee":[0],// InitialDeposit/10%
      "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
      "CarryForwardPeriod":['Indefinite Rollover to the benefit of the account holder'], //12 months
      "ContributionSchedule":[1], //Monthly 1, Yearly 2
      "GrossTotalInitial":[0], //over all initial total
      "GrossTotalMonthly":[0], // over all monthly total
      "HSTInitial":[0],// InitialDeposit/13%
      "HSTMonthly":[0], //RecurringDeposit/ 13%
      "InitialDeposit": new FormControl('', [Validators.required,Validators.minLength(1)]),
      "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
      "PlanDesign":new FormControl(2),
      "PlanTypeId": this.planTypeId,
      "PlanStartDt":['', [Validators.required]],
      "PremiumTaxInitial":[0], //InitialDeposit/ 2%
      "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
      "RecurringDeposit": new FormControl('', [Validators.required,Validators.minLength(1)]),  //['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
      "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
      "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
      "RollOver":[4],
      "Termination":[6],
      "TempPlanId" : i,
      "PlanId": -1,
      "TotatFeeAndTaxes":[0]
  });
}


//calculate taxes
RecurringDepositV2(obj){
  if(obj.value.RecurringDeposit !=''){
    this.addPlanForm.get('Plans')['controls'].forEach((plan)=>{
    if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
      plan.controls['AdministrationMonthlyFee'].patchValue((obj.value.RecurringDeposit * this.AdminFeePct)/100);

      let pptamt = parseFloat(obj.value.RecurringDeposit) + parseFloat(plan.controls['AdministrationMonthlyFee'].value);
      plan.controls['PremiumTaxMonthly'].patchValue(( pptamt * this.PPTax)/100);

      if(this.HST != 0){
        //plan.controls['HSTMonthly'].patchValue((obj.value.RecurringDeposit *  this.HST)/100);
        plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.HST)/100);
      }else{
         plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.GST)/100);
      }

      if(this.planType == "Group"){
        plan.controls['RetailSalesTaxMonthly'].patchValue((obj.value.RecurringDeposit * this.RSTax)/100);
      }else{
        plan.controls['RetailSalesTaxMonthly'].patchValue(0)
      }

      let GrossTotalMonthly = (plan.controls['AdministrationMonthlyFee'].value + plan.controls['HSTMonthly'].value + plan.controls['PremiumTaxMonthly'].value + plan.controls['RetailSalesTaxMonthly'].value )
      // plan.controls['GrossTotalInitial'].patchValue();
       plan.controls['GrossTotalMonthly'].patchValue(GrossTotalMonthly);

       let netTotal = (parseFloat(obj.value.RecurringDeposit) +  parseFloat(plan.controls['InitialDeposit'].value ? plan.controls['InitialDeposit'].value : 0));
        plan.controls['NetDeposit'].patchValue(netTotal);
        plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);
      }
  })
  }else{
    this.service.alertMessage("Enter contribution amount for monthly or annual schedule","error");
    return false;
  }
}

InitialDepositV2(obj){
    if(obj.value.InitialDeposit !=''){
      this.addPlanForm.get('Plans')['controls'].forEach((plan)=>{
      console.log(plan.value);
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['AdministrationInitialFee'].patchValue((obj.value.InitialDeposit * this.AdminFeePct)/100);
        //if hst 0 then apply GST
        if(this.HST != 0){
        //  plan.controls['HSTInitial'].patchValue((obj.value.InitialDeposit * this.HST)/100);
        plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.HST)/100);
        }else{
          plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.GST)/100);
        }

      //  let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        //plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        //if group then apply rst
        if(this.planType == "Group"){
          // plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);
          plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);

        }else{
          plan.controls['RetailSalesTaxInitial'].patchValue(0);
        }

        let GrossTotalInitial = (plan.controls['AdministrationInitialFee'].value + plan.controls['HSTInitial'].value + plan.controls['PremiumTaxInitial'].value + plan.controls['RetailSalesTaxInitial'].value )
        plan.controls['GrossTotalInitial'].patchValue(GrossTotalInitial);
        let  netTotal = (parseFloat(obj.value.InitialDeposit) +  parseFloat(plan.controls['RecurringDeposit'].value ? plan.controls['RecurringDeposit'].value : 0));

        plan.controls['NetDeposit'].patchValue(netTotal)

        plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);

      }
    })
  }else{
    this.service.alertMessage("Please enter initial deposit amount","error");
    return false;
  }
}

submitNewPlan(obj){
  //console.log(JSON.stringify(obj.value, undefined, 2));

  //this.isLoading= true;
  if(obj.value.PlanStartDt.formatted){
     obj.value['PlanStartDt']= obj.value.PlanStartDt.formatted;
  }else{
    if(typeof  obj.value.PlanStartDt === 'object'){
      let mm = parseInt(obj.value.PlanStartDt.date.month) < 10 ? '0'+obj.value.PlanStartDt.date.month: obj.value.PlanStartDt.date.month;
      let dd = parseInt(obj.value.PlanStartDt.date.day) < 10 ? '0'+obj.value.PlanStartDt.date.day: obj.value.PlanStartDt.date.day;
      obj.value['PlanStartDt']= mm+"/"+ dd +"/"+obj.value.PlanStartDt.date.year;
    }else{
      obj.value['PlanStartDt']= obj.value.PlanStartDt;
    }
  }
  obj.value['DbCon'] = sessionStorage.getItem('DbCon');
  obj.value['LoggedInUserId']=sessionStorage.getItem('loginUserId');
  const _input = obj.value;
  console.log(JSON.stringify(_input, undefined, 2));
  this.isLoading = true;
  this.service.addPlan(_input)
  .subscribe((res:Response)=>{
    if(res['Status'] == 'Success'){
        this.isLoading = false;
        this.planForm.controls['Plans'] = this.fb.array([]);
        this.getCompanyDetails();
        this.service.alertMessage("New plan successfully added.","success")
        this.planModal = false;
    }else{
        this.isLoading = false;
        this.service.alertMessage(res['Message'],'error','')
    }
  },(error)=>{
      this.isLoading = false;
      this.service.alertMessage(error,'error','')
  })
}

togglePassword(pvarType){
  if(pvarType == 'op'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'np'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'cp'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'ti'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'in'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
}

textDecode(pvarTxt){
  if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
      return this.service.decodeText(pvarTxt);
  }
}
textEncode(pvarTxt){
    if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
      return this.service.encodeText(pvarTxt);
    }
}

}
