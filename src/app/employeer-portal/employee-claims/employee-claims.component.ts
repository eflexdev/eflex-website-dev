import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {Router, ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';
import {environment} from '../../../environments/environment.prod'

@Component({
  selector: 'app-employee-claims',
  templateUrl: './employee-claims.component.html',
  styleUrls: ['./employee-claims.component.css']
})
export class EmployeeClaimsComponent implements OnInit {

  modal:boolean = false;
  empid: any = -1;
  companyId: any = -1;
  name: string= '';
  claimsData: any=[];
  searchText: any = '';
  isLoading: boolean = false;
  loading: boolean = false;
  statusId: any = -1;
  dbCon: string = '';
  claimId: any= -1;
  hostUrl:string = '';
  newArray: any = [];
  claimType: any = -1;

  constructor(
    private service: CompanyService,
    private acitvateRoute : ActivatedRoute,
    private _location: Location

  ) {
    this.hostUrl = environment.hostUrl
    this.acitvateRoute.params.subscribe((param)=>{
      console.log(param);
      this.empid = +param['id'];
      this.name = param['name'];
    })
  }

  ngOnInit() {
    if(sessionStorage.getItem('companyId') != null){
        this.companyId = sessionStorage.getItem('companyId');
        this.getClaimsList();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

  getClaimsByType(){
    this.newArray = [];
    this.claimsData = [];
    this.claimType = this.claimType;
    this.getClaimsList();
  }

  getClaimsList(){
      //this.chunk_Start = this.claimsData.length == 0 ? -1 : this.claimsData[this.claimsData.length-1]['RowId'];
      //console.log("chunk_Start" + this.chunk_Start);
      const _input ={
      	"CompanyId":this.companyId,
      	"CompanyUserId":parseInt(this.empid),
        "ChunckSize":50,
        "ChunckStart":-1,
        "LoggedInUserId": parseInt(sessionStorage.getItem('loginUserId')),
        "SearchString": this.searchText,
        "StatusId":-1
      }
      this.isLoading= true;
      this.service.employeeClaimListForCompany(_input)
      .subscribe((response:Response)=>{
        if(response['Status'] == 'Success'){
          this.isLoading= false;
        //  this.newArray = this.newArray.concat(response['AdminClaimList']);
          this.claimsData = response['EmployeeClaimList']; //response['ListOfBrokers'];
        }else{
          this.isLoading= false;
          this.service.alertMessage(response['Message'],"error");
        }
    }, function (parameter) {
          this.isLoading= false;
          console.log(parameter)
      })
  }

  openClaimModel(pvarObj){
    this.claimId = pvarObj['EmpClaimId'];
    this.modal = true;
  }

  // updateClaimStatus(){
  //   if (this.statusId == -1) {
  //       this.service.alertMessage("Please select status","error");
  //       return false;
  //   }
  //   let _input = {
  //     "ClaimId":this.claimId,
	//     //"DbCon":this.dbCon,
  //     "CompanyId": this.companyId,
	//     "StatusId":parseInt(this.statusId),
  //     "LoggedInUserId": sessionStorage.getItem('loginUserId')
  //   }
  //   console.log(_input);
  //   this.service.updateClaimStatus(_input)
  //   .subscribe((response:Response)=>{
  //     if(response['Status'] == 'Success'){
  //       this.service.alertMessage('Claim status update successfully',"success");
  //       this.claimsData = [];
  //       this.modal = false;
  //       this.getClaimsList();
  //     }else{
  //       this.service.alertMessage(response['Message'],'error');
  //     }
  //   },function (error) {
  //       throw new Error("Not implemented yet");
  //   })
  // }

  backClicked() {
          this._location.back();
  }

  isClaimDetailModal: boolean = false;
  claimDetails: any = [];
  openClaimDetail(pvarObj){
    console.log(pvarObj);
    this.isClaimDetailModal = true;
    this.claimDetails = pvarObj;
  }

}
