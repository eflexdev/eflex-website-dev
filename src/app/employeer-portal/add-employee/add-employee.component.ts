import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service';
import { Router }              from '@angular/router';
import {environment} from '../../../environments/environment.prod';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  isFormValid: boolean = false;
  employeeForm: FormGroup;
  isLoading: boolean;
  classesData:any[];
  modal: boolean;
  companyId: any = -1;
  hostUrl:string='';

  @ViewChild('fileInput') fileInput: ElementRef;
  constructor(
    private fb:FormBuilder,
    private service: CompanyService,
    private router: Router, private el : ElementRef) {
      this.hostUrl = environment.hostUrl;
      this.employeeForm = this.fb.group({
            "ID":[''],
            "Employees": this.fb.array([ this.createEmployee() ])
      });
      if(sessionStorage.getItem('companyId') != null){
          this.companyId = sessionStorage.getItem("companyId");
          this.getClasses();
      }else{
        this.service.alertMessage("Please login first","error");
        this.router.navigate(['/']);
      }
    }

    getClasses(){
        if(sessionStorage.getItem("DbCon") == null){
          return false;
        }
        if(this.classesData == null){
          const _input = {
            "DbCon": sessionStorage.getItem("DbCon")
          }
          this.service.populateClasses(_input)
          .subscribe((res:Response)=>{
            if(res['Status'] == 'Success'){
              this.classesData = res['ClassList'];
            }else{
              this.service.alertMessage(res['Message'],'error','')
            }
          },(error)=>{
            this.service.alertMessage(error,'error','')
          })
        }
    }


    emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    createEmployee(): FormGroup {
      return this.fb.group({
          "CompanyPlanId":[''],
          "CompanyUserId":-1,
          "ContactNo":[''],
          "Email":['',[Validators.required, Validators.pattern(this.emailPattern)]],
          "FName":['',[Validators.required]],
          "LName":[''],
          "UserTypeId":[3],
      })
    }

    addMoreEmployee(){
      const control=<FormArray>this.employeeForm.controls['Employees'];
      control.push(this.createEmployee());
    }
    deleteEmployee(index:number){
        const control=<FormArray>this.employeeForm.controls['Employees'];
        control.removeAt(index);
    }

    ngOnInit() {

    }

    AddEmployee(){
      console.log(this.employeeForm);
      if(this.employeeForm.valid){
        let _input ={
          "DbCon":sessionStorage.getItem("DbCon"),
          "EmployeesList": this.employeeForm.value['Employees']
        }
        console.log( JSON.stringify(_input, undefined, 2));
        this.isLoading = true;
        this.service.addEmployee(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
              this.service.alertMessage("Employees successfully added",'success','2000');
              this.isLoading = false;
              this.router.navigate(['./employeer-portal/employees']);
          }else{
              this.service.alertMessage(res['Message'],'error','');
              this.isLoading = false;
          }
        },(error)=>{
          this.service.alertMessage(error,'error','');
          this.isLoading = false;

        })
      }
    }


    openModal(){
      this.modal = true;
    }

    uploadMsg = '';
    fileName: string= '';
    importLoaderModal: boolean;
    uploadFile(event) {
          this.uploadMsg = '';
          let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
          //get the total amount of files attached to the file input.
          let fileCount: number = inputEl.files.length;
          //create a new fromdata instance
          let formData = new FormData();
          if (fileCount > 0) { // a file was selected
            var strFileName = String(inputEl.files.item(0).name);
           if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.xlsx)/i) != -1) {
              this.uploadMsg = "Please wait file uploading...";
              formData.append('file', inputEl.files.item(0));
              formData.append('CompanyId', this.companyId);
              this.service.uploadFile(formData)
              .subscribe((result)=>{
                    this.uploadMsg = '';
                    var respnose = result._body;
                    if(respnose){
                      const img_name = respnose.split("###");
                        if(img_name[0] == 'Success'){
                            this.fileInput.nativeElement.value = '';
                             this.fileName = img_name[1];
                             //Call to service
                             const fileInput = {
                                "CompanyId": parseInt(this.companyId),
                              	"DbCon": sessionStorage.getItem('DbCon'),
                              	"FileName": img_name[1]
                             }
                             this.modal = false;
                             this.importLoaderModal = true;
                             this.service.importEmployeesExcel(fileInput)
                             .subscribe((res:Response)=>{
                               if(res['Status'] == 'Success'){
                                    this.modal = false;
                                    this.importLoaderModal = false;
                                    this.service.alertMessage("Members successfully imported","success");
                                    this.router.navigate(['./employeer-portal/employees']);
                               }else{
                                  this.service.alertMessage(res['Message'],"error");
                                  this.importLoaderModal = false;
                               }
                             }, (error:Error)=>{
                                console.log(error)
                                this.importLoaderModal = false;
                             });
                             //end

                        }else{
                          this.fileInput.nativeElement.value = '';
                          this.modal = false;
                          this.service.alertMessage("Members File upload encountered an error. Please try again.","error");
                        }
                    }
                  },
                  (error) =>{  console.log(error);  });
              }else{
                this.service.alertMessage("Please select a file with the extension .xlsx","error");
                return false;
              }
            }else{
                this.uploadMsg =" Please choose file";
            }
   }

}
