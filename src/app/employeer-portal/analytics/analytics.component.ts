import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
declare var Highcharts;
@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  chart: Chart;
  plansChart: any;
  pieChart: any;

  capacity_chart: any;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  plansData: any=[];

  companyName: string;
  isPending: boolean = true;
  isCurrent: boolean = true;
  isContribution: boolean= true;
  companyCode:string;
  amountDue: any = 0;
  availableCredits: any=0
  colorArr:any = [];
  currentTransaction: any = [];
  pendingTransaction: any = [];
  getCompDashboardFigures: any = [];

  constructor(private service: CompanyService, private router : Router) {
    this.companyName = sessionStorage.getItem("companyName");
    this.companyCode = sessionStorage.getItem("uniqueCode");
    this.amountDue = sessionStorage.getItem("amountDue");
   }

  ngOnInit() {

  //  this.getContribution();

    //this.newChart();

    this.getTransaction();
  }


  // newChart(){
  //   const _input = {
  //     "DbCon":sessionStorage.getItem("DbCon")
  //   }
  //
  //   this.service.getPaymentOverview(_input)
  //   .subscribe((res:Response)=>{
  //     if(res['Status'] == "Success"){
  //       this.NetGrossInitial = res['NetGrossInitial'];
  //       this.NetGrossRecurring = res['NetGrossRecurring'];
  //       if(res['ClasswisePayments'].length > 0){
  //          for(var k=0; k< res['ClasswisePayments'].length; k++){
  //            const obj=  res['ClasswisePayments'][k];
  //            const offData={};
  //            offData['name'] = obj['PlanName'];
  //            offData['y']=obj.NoOfEmployees;
  //            offData['sliced']= k == 0 ? true : false;
  //            offData['selected']= k == 0 ? true : false;
  //
  //            //this.plansData.push(offData)
  //            let employee = obj['NoOfEmployees']; //obj['NoOfEmployees'] == 0 ? 1 : obj['NoOfEmployees'];
  //            this.colorArr.push(obj['Color']);
  //            this.plansData.push([obj['PlanName'], employee])
  //         };
  //                //console.log(this.officeReportData)
  //     }
  //     //this.showPieChart();
  //     }
  // });
  // }

  // contributionData:any;
  // getContribution(){
  //   const _input={
  //     "DbCon": sessionStorage.getItem('DbCon'),
  //     "Id": sessionStorage.getItem('loginUserId')
  //   };
  //   this.service.getCompanyDashboardData(_input)
  //   .subscribe((res:Response)=>{
  //     if(res['Status'] == "Success"){
  //       this.contributionData = res['GetDashboardData']
  //     }else{
  //       this.service.alertMessage(res['Message'],'error');
  //     }
  //   })
  // }

  planIndex: any = -1;
  toggleClass(object){
    this.planIndex = object.Plan;
  }

  servicesClaimData: any =[];
  getTransaction(){
    const _input={
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId')
    };
    this.service.getDashboardTransactions(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.amountDue= res['AmountDue'];
        this.availableCredits = res['AvailableCredits'];
        this.currentTransaction = res['CurrentTransaction'];
        this.pendingTransaction = res['PendingTransaction'];
        this.getCompDashboardFigures = res['GetCompDashboardFigures'];
        this.servicesClaimData =res['ServiceClaimAmt'];
        this.getRecentClaimsByType();
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })

    this.getOtherDetails()
  }

  isServiceCall_2: boolean =false;
  empData: any = [];
  planData: any = [];

  categoryType: any = [];
  claimAmt: any =[];
  noOfClaims: any =[];

  getOtherDetails(){
    let _input = {
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId'),
      "FilterType":'Month'
    }
    this.isServiceCall_2 = true;
    this.service.getEmpPlanDashboardDetails(_input)
    .subscribe((response:Response)=>{
      this.isServiceCall_2 = false;
      if(response['Status'] == "Success"){
        this.empData = response['EmployeesDetails'];
        this.categoryType = response['ClaimOverview']['Months']
        this.claimAmt = response['ClaimOverview']['TotalClaimAmt']
        this.noOfClaims = response['ClaimOverview']['NoOfClaims']
        //this.planData = response['PlanDashboardDetails'];
          if(response['PlanDashboardDetails'] != null){
            let a = response['PlanDashboardDetails'];
            console.log(a);
            a.forEach(obj => {
              let newObj ={}
              newObj['name'] = obj['PlanName'];
              newObj['y'] = obj['NoOfEmployees'];
              this.planData.push(newObj);

           })
           this.showClaimOverview();
           this.showPlanPieChart();
          }
      }else{
        this.service.alertMessage(response['Message'],'error');
      }
    },(error:Error)=>{
      this.isServiceCall_2 = false;
      this.service.alertMessage(error,"error");
    })
  }

  serviceType:any = 1;
  recentClaimsData: any = [];
  isServiceCall: boolean = false;

  setSelectedServiceType(){
    this.serviceType =  this.serviceType;  
    this.getRecentClaimsByType();
  }
  getRecentClaimsByType(){
    this.recentClaimsData=[];
    let _input = {
      "DbCon": sessionStorage.getItem('DbCon'),
      "Id": sessionStorage.getItem('loginUserId'),
    	"ServiceTypeId":this.serviceType
    }
    this.isServiceCall = true;
    this.service.getRecentClaimDashboard(_input)
    .subscribe((response:Response)=>{
      this.isServiceCall = false;
      if(response['Status'] == "Success"){
        this.recentClaimsData = response['RecentClaimList'];
      }else{
        this.service.alertMessage(response['Message'],'error');
      }
    },(error:Error)=>{
      this.isServiceCall = false;
      this.service.alertMessage(error,"error");
    })
  }

  showClaimOverview(){


        this.plansChart = new Chart({
          chart: {
              zoomType: 'xy'
          },
          title: {
              text: ''
          },
          subtitle: {
              //text: 'Source: WorldClimate.com'
          },
          xAxis: [{
              categories: this.categoryType, //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul'],
                  // 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
              crosshair: true
          }],
          yAxis: [{ // Primary yAxis
              labels: {
                  format: '${value}',
                  style: {
                      //color: Highcharts.getOptions().colors[1]
                  }
              },
              title: {
                  text: 'Claimed Amount',
                  style: {
                      //color: Highcharts.getOptions().colors[1]
                  }
              }
          },
          { // Secondary yAxis
              title: {
                  text: 'No of Claims',
                  style: {
                      color: 'Blue'//Highcharts.getOptions().colors[0]
                  }
              },
              // labels: {
              //     format: '${value}',
              //     style: {
              //         color: 'gray'//Highcharts.getOptions().colors[0]
              //     }
              // },
              opposite: true
          }
        ],
          tooltip: {
              shared: true
          },
          credits: {
             enabled: false
         },
          legend: {
              layout: 'vertical',
              align: 'left',
              x: 120,
              verticalAlign: 'top',
              y: 100,
              floating: true,
              backgroundColor: 'rgba(255,255,255,0.25)'//(Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
          },
          series: [{
              name: 'No of claims',
              type: 'column',
              yAxis: 1,
              data: this.noOfClaims, //[10, 15, 20,18, 25, 35, 45],
              // tooltip: {
              //     valueSuffix: ' mm'
              // }
              //
          }, {
              name: 'Claimed Amount',
              type: 'spline',
              data: this.claimAmt,//[100, 250, 180,300, 150, 120, 250],
              // tooltip: {
              //     valueSuffix: '°C'
              // }
          }]
        });

  }

  showPlanPieChart(){
      this.pieChart = new Chart({
        chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'No of employee'
        }

    },
    legend: {
        enabled: false
    },
    credits: {
       enabled: false
   },
    plotOptions: {
        series: {
          //  borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
                // format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
    },

    "series": [
        {
            "name": "Plan",
            //"colorByPoint": true,
            "data": this.planData
          }],
      })
    }

}
