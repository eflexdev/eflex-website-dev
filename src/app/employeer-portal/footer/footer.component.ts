import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  privacy: boolean = false;
  term_conditions: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  openPrivacy(){
    this.privacy = true;
  }

  openTermCondition(){
    this.term_conditions = true;
  }

}
