import { Component, OnInit, Inject } from '@angular/core';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
import {environment} from '../../../environments/environment.prod'
import { DOCUMENT } from '@angular/platform-browser';
import {BrowserModule, DomSanitizer,SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-e-statements',
  templateUrl: './e-statements.component.html',
  styleUrls: ['./e-statements.component.css']
})
export class EStatementsComponent implements OnInit {
  loading: boolean;
  companyId: any;
  hostUrl: string='';
  year: any = '';
  month: any = '';
  pdfSource: any ;
  pdfModal: boolean = false;
  yearArr: any = [];
  masterMonth: any = [
    	{
    		"value": 1,
    		"name": "January"
    	},
    	{
    		"value": 2,
    		"name": "February"
    	},
    	{
    		"value":3,
    		"name": "March"
    	},
    	{
    		"value": 4,
    		"name": "April"
    	},
    	{
    		"value": 5,
    		"name": "May"
    	},
    	{
    		"value": 6,
    		"name": "June"
    	},
    	{
    		"value": 7,
    		"name": "July"
    	},
    	{
    		"value": 8,
    		"name": "August"
    	},
    	{
    		"value":9,
    		"name": "September"
    	},
    	{
    		"value":10,
    		"name": "October"
    	},
    	{
    		"value":11,
    		"name": "November"
    	},
    	{
    		"value": 12,
    		"name": "December"
    	}
    ];
  constructor(private service: CompanyService, @Inject(DOCUMENT) private document: any,
   private domSanitizer:DomSanitizer
  ) {
    this.hostUrl = environment.hostUrl;
   }

  ngOnInit() {
    if(sessionStorage.getItem('companyId') != null){
        this.companyId = sessionStorage.getItem('companyId');
        let date = new Date();
        this.month = date.getMonth()+1;
        this.year = date.getFullYear();

         // let dt1 = new Date(sessionStorage.getItem('CompanyRegDate'));
         // let dt2 = new Date();
         let dt1 = new Date(sessionStorage.getItem('CompanyRegDate'));
         let dt2 = new Date();
        for(var i=0; i< this.diff_years(dt1, dt2)+1; i++){
          var yy = this.year - i;
          let type = '';
          let monthStart = 1;
          let monthEnd = 12;

            if(this.year - i == this.year){
              type = 'Current';
              monthStart =  dt1.getMonth()+1;
              monthEnd = dt2.getMonth()+1;
            }else if(this.year - i == dt1.getFullYear()){
              type = 'last';
              monthStart = dt1.getMonth();
              monthEnd = 12;
            }else{
              type = 'Inter';
              monthStart =0;
              monthEnd = 12;
            }

            let obj = {
              'year':this.year - i,
              'type' :type,
              'month_start': monthStart,
              'month_end': monthEnd
            }
            this.yearArr.push(obj);
        }
        this.getyearMonth();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

   diff_years(dt2, dt1) {
     var diff =(dt2.getTime() - dt1.getTime()) / 1000;
     diff /= (60 * 60 * 24);
     return Math.abs(Math.round(diff/365.25));
   }
   yearMonths: any = '';
   getyearMonth(){
      this.month = '';
      let year = this.yearArr.filter(item => item.year == this.year);
      this.yearMonths = this.masterMonth.filter(item => item.value >= year[0].month_start &&  item.value <= year[0].month_end);
      console.log(this.yearMonths);
   }

  downLoadEstatement(){
    if(this.year != '' && this.month != ''){
      const _input = {
        "DbCon": sessionStorage.getItem("DbCon"),
        "Month": parseInt(this.month),
        "Year": parseInt(this.year),
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      }
      this.loading = true;
      this.service.getEStatement(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == "Success"){
          this.loading = false;
          //this.service.alertMessage("eStatement download successfully in your default download folder",'error');
          var url = this.hostUrl+"/"+this.companyId+"/"+res['FileName'];
        //  this.document.location.href = link;
          //window.open(url, "_blank");
          this.pdfModal = true;
          this.pdfSource = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        }else{
            this.loading = false;
            this.service.alertMessage(res['Message'],'error');
        }
      }, (error)=>{
          this.loading = false;
          this.service.alertMessage(error,'error');
      })
    }else{
      this.service.alertMessage("Please select year and month","error");
      return false;
    }
}
downloadFile(data: Response){
    var blob = new Blob([data], { type: 'text/pdf' });
    var url= window.URL.createObjectURL(blob);
    window.open(url);
  }

}
