import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CompanyService} from '../../company.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName: string;
  companyCode:string;
  UnreadNotificationCount: number = 0;
  MessageCount:number= 0;
  toggleMenuClass: boolean = false;
  isSwitchToogle:boolean = false;
  isMsgShown: boolean = false;

  constructor(private router: Router, private service: CompanyService) {
      this.getMessgeCount()
   }

  ngOnInit() {
      this.userName = sessionStorage.getItem("userName");
      this.companyCode = sessionStorage.getItem("uniqueCode");
  }

  getMessgeCount(){
    if(sessionStorage.getItem('companyId') != undefined && sessionStorage.getItem('companyId') != null ){
      const _input ={
      	"CompanyId":sessionStorage.getItem('companyId'),
      	"DbCon":sessionStorage.getItem('DbCon'),
      	"UserId":sessionStorage.getItem('loginUserId'),
      	"UserTypeId":sessionStorage.getItem('userTypeId'),
      }
      this.service.getNotiMsgCount(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.userName = sessionStorage.getItem("userName");
          this.MessageCount = res['MessageCount'];
          this.UnreadNotificationCount = res['UnreadNotificationCount'];
          sessionStorage.setItem("isFreeze", res['IsFreeze']);
          setTimeout(()=>{
            this.getMessgeCount();
          },5000)
        }else{
          if(res['IsLogOut'] == true){
              this.logOut();
          }
          if(this.isMsgShown == false){
            this.isMsgShown = true;
            this.service.alertMessage(res['Message'],"error");
          }
        }
      },function (parameter) {
          throw new Error("Not implemented yet");
      })
    }
  }
  logOut(){
    sessionStorage.clear();
    //this.service.alertMessage("You are logout successfully","success");
    this.router.navigate(['/join-us']);

  }
  toggleMenu(){
    this.toggleMenuClass = !this.toggleMenuClass;
  }

  setToggle(){
    this.isSwitchToogle = !this.isSwitchToogle;
  }
  hideSideBar(){
    this.toggleMenuClass = false;
  }
}
