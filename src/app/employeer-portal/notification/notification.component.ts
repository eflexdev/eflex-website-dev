import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notificationData:any = [];
  adminModal: boolean;
  companyId: any = -1;
  memberTypeId: any = 3;
  isLoader:boolean;

  constructor(private service: CompanyService) { }

  ngOnInit() {
    if(sessionStorage.getItem('companyId') != null){
        this.companyId = sessionStorage.getItem('companyId');
        this.getNotifications();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

  getNotifications(){
   const _input = {
    	"ChunkSize":20,
    	"ChunkStart":-1,
    	"DbCon": sessionStorage.getItem("DbCon"),
    	"Id": sessionStorage.getItem('loginUserId'),
    	"SearchString":""
    }
    this.isLoader = true;
    this.service.listOfNotificationLog(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.isLoader = false;
        this.notificationData= res['NotificationLog'];
      }else{
          this.isLoader = false;
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
        this.isLoader = false;
        this.service.alertMessage(error,'error');
    })
  }
}
