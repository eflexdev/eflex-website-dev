import { Component, OnInit, Pipe,PipeTransform,ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
import {DatePipe} from '@angular/common';
import * as jsPDF from 'jspdf';
declare var $ :any;
import {CsvService} from 'angular2-json2csv';

import { utils, write, WorkBook } from 'xlsx';

import { saveAs } from 'file-saver';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})

export class ActivityComponent implements OnInit {
  chart: Chart;
  capacity_chart: any;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  plansData: any=[];
  plansChart: any;
  companyName: string;
  isPending: boolean = true;
  isCurrent: boolean = true;
  isContribution: boolean= true;
  companyCode:string;
  amountDue: any = 0;
  colorArr:any = [];
  currentTransaction: any;
  pendingTransaction: any;


  periodData: any[];
  period: string = 'current';
  filterByType: number = -1
  searchString: string = '';
  adjustments: any = 0;
  payments: any = 0;
  availableCredits: any = 0;

  @ViewChild('transactions') transactions:ElementRef
  constructor(private service: CompanyService, private router : Router,  private CsvService: CsvService) { }

  ngOnInit() {
    if(sessionStorage.getItem("DbCon") != null){
        this.companyName = sessionStorage.getItem("companyName");
        this.companyCode = sessionStorage.getItem("uniqueCode");
        this.amountDue = sessionStorage.getItem("amountDue");

        const _input = {
          "DbCon":sessionStorage.getItem("DbCon")
        }

        this.service.getStatementPeriod(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == "Success"){
            this.periodData = res['StatementPeriod'];

          }
      });

    }else{
      this.router.navigate(['/login']);
    }

    this.getTransaction();
  }


  onPeriodChange(){
    this.period  = this.period;
    this.pendingTransaction = [];
    this.currentTransaction = [];
    this.getTransaction();
  }

  onTypeChange(){
    this.filterByType = this.filterByType;
    this.pendingTransaction = [];
    this.currentTransaction = [];
    this.getTransaction();
  }

  searchEmpety(){
  if(this.searchString.length == 0){
      this.pendingTransaction = [];
      this.currentTransaction = [];
      this.getTransaction();
  }
}

getTransaction(){
    var datePipe = new DatePipe("en-US");
    let periodDate = ''
    if(this.period == 'current'){
       periodDate = datePipe.transform(new Date().toDateString(), 'MM/dd/yyyy');
    }else{
      periodDate = datePipe.transform(new Date(this.period).toDateString(), 'MM/dd/yyyy');
    }
    const _input={
      "ChunkSize":500,
    	"ChunkStart":-1,
    	"DbCon":sessionStorage.getItem("DbCon"),
    	"Id": this.filterByType,
    	"Period":periodDate,
    	"SearchString":this.searchString,
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    };
    console.log(_input);
    this.service.getActivityData(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        console.log(res);
        this.adjustments = res['Adjustments'];
        this.availableCredits = res['AvailableCredits'];
        this.payments = res['TotalBalance'];
        this.currentTransaction = res['ActivityList'];
        this.pendingTransaction = res['PendingTransaction'];
      }else{
        this.service.alertMessage(res['Message'],'error');
      }
    })
  }

  downLoadPDF(){
    // Landscape export, 2×4 inches
    let doc = new jsPDF();
    // We'll make our own renderer to skip this editor
    let specialElementHandlers = {
    	'#editor': function(element, renderer){
    		return true;
    	},
    	'.controls': function(element, renderer){
    		return true;
    	}
    };

  let content = this.transactions.nativeElement;
  // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
  doc.fromHTML(content.innerHTML, 10, 10, {
  	'width': 190,
  	'elementHandlers': specialElementHandlers
  });
  let pdfName= "current-transactions_"+new Date()+".pdf";
  doc.setFontType("normal");
  doc.save(pdfName)
}

transform(value){
  var s=value;
  if(s){
    console.log(s)
    value = s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    return value;
  }
}

downloadCSV(){
    var i=0;
    var newArr = [];
    this.currentTransaction.forEach((res)=>{
      let obj = {};
      obj['Sr.No'] = i+1;
      obj['Transaction Date'] = this.transform(res['TransactionDate']);
      obj['Posted Date'] = this.transform(res['PostedDate']);
      obj['Description'] = res['Details'];
      obj['Amount'] = res['Amount'];

      newArr.push(obj);
    })

    const ws_name = 'activity_transaction';
    const wb: WorkBook = { SheetNames: [], Sheets: {} };
    const ws: any = utils.json_to_sheet(newArr);
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;
    const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

    function s2ab(s) {
      const buf = new ArrayBuffer(s.length);
      const view = new Uint8Array(buf);
      for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
      };
      return buf;
    }
    saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), 'activity_transactions.xlsx');
}
sortingName: string;
isDesc: boolean;
sort(name: string): void {
  if (name && this.sortingName !== name) {
    this.isDesc = false;
  } else {
    this.isDesc = !this.isDesc;
  }
  this.sortingName = name;
}



}
