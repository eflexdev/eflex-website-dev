import { Component, AfterViewChecked, OnInit, NgModule, ElementRef, ViewChild } from '@angular/core';
import {CompanyService} from "../../company.service";
import {MessagesService} from "../../messages.service";
import {environment} from '../../../environments/environment.prod'
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, AfterViewChecked {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  messageText : string =''
  companyId: number = -1;
  dbCon: string= '';
  loginUserId = parseInt(sessionStorage.getItem('loginUserId'));
  userTypeId = parseInt(sessionStorage.getItem('userTypeId'));
  searchUser:string = '';
  usersData: any = [];
  messagesData: any=[];
  activeUser:any =-1;

  hostUrl: string= '';

  openPopup: Function;
  setPopupAction(fn: any) {
    console.log('setPopupAction');
    this.openPopup = fn;
  }

  constructor(
    private service : CompanyService,
    private msgService: MessagesService,
    private el : ElementRef,
  ) {
    this.hostUrl = environment.hostUrl;
  }

  ngAfterViewChecked() {
     //this.scrollToBottom();
  }

  scrollToBottom(): void {
       try {
           this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
       } catch(err) { }
   }


  ngOnInit() {
    this.companyId = parseInt(sessionStorage.getItem('companyId'));
    this.dbCon = sessionStorage.getItem('DbCon');
    if(this.companyId != null && this.companyId != -1){
      this.getAllConversation()
    }
  }

  setFirtCall:boolean = true;
  //get all Conversations user left side
  getAllConversation(){
    if(sessionStorage.getItem('companyId') != null && this.companyId != -1){
    const _input = {
      	"DbCon": this.dbCon,
      	"LoggedInUserId":this.loginUserId,
        "LoggedInUserTypeId": this.userTypeId
    };
    this.msgService.listAllCompanyUsers(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        console.log(res);
        this.usersData = res['OtherUser1'];
        if(this.setFirtCall == true){
          this.getMessages(res['OtherUser1'][0]);
        }
      }else{
        //this.service.alertMessage(res['Message'],"error")
      }
    }, function (parameter) {
        throw new Error("Not implemented yet");
    });

    setTimeout(()=>{this.getAllConversation(); },10000);
  }
  }


  selectedUser: any;
  conversationsUserName: string;
  recipientId: any= -1;
  recipientUserTypeId: any = -1;
  recipientCompanyId: any = -1;
  cId: any = -1;

  getMessages(user){
    console.log(this.companyId);
    if(sessionStorage.getItem('companyId') != null && this.companyId != -1){
    this.setFirtCall = false;
    this.selectedUser=user;
    this.conversationsUserName = user['Name'];
    this.activeUser = user['UserId'];
    this.cId = user['Cid'];
    this.recipientId = user['UserId'];
    this.recipientUserTypeId= user['UserTypeId'];
    this.recipientCompanyId = user['CompanyId'];
    const _input = {
        "CId":this.cId,
      	"ChunkSize":10,
      	"ChunkStart":-1,
        "CompanyId": this.companyId,
        "DbCon": this.dbCon,
        "UserId":this.loginUserId,
        "UserTypeId": this.userTypeId
    };
    console.log(JSON.stringify(_input, undefined, 2));
    this.msgService.listAllConversationMessages(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        console.log(res);
        this.messagesData = res['ConversationMessages'];
        setTimeout(()=>{
          let elem = document.getElementById('fixed');
          elem.scrollTop = elem.scrollHeight;
        },1000);
      }else{
        this.service.alertMessage(res['Message'],"error")
      }
    }, function (parameter) {
        throw new Error("Not implemented yet");
    });

    setTimeout(()=>{this.refreshChat(); },20000);
  }
  }


  refreshChat(){
      if(sessionStorage.getItem('companyId') != null && this.companyId != -1){
   let last = this.messagesData.length == 0 ? -1 : this.messagesData[this.messagesData.length -1]['MsgId'];
   let inputData = {
      "CId": this.cId,
    	"CompanyId":sessionStorage.getItem('companyId'),
    	"LatestMsgId":last,
      "DbCon": this.dbCon,
      "UserId":this.loginUserId,
      "UserTypeId": this.userTypeId
   }
   console.log(inputData);
   this.msgService.listAllConversationMessagesAjax(inputData)
   .subscribe((response: Response)=>{
     if(response['Status'] == 'Success'){
       let new_arr = [];
       new_arr = response['ConversationMessages'];
       if(new_arr.length > 0){
         for(let i = 0; i < new_arr.length; i++){
           this.messagesData.push(new_arr[i]);
         }
       }
     }

   })
   setTimeout(()=>{this.refreshChat(); }, 20000);
 }
 }

  loadPreviousMsg : boolean = true;
  x = 500;
  y = 500;
  onScroll(){
  //if(this.loadPreviousMsg == true){
    if(sessionStorage.getItem('companyId') != null && this.companyId != -1){
    let elmnt = document.getElementById("fixed");
    this.x = elmnt.scrollLeft;
    this.y = elmnt.scrollTop;
    console.log("y " +this.y);
    if(this.x == 0 && this.y < 150){
      let ChunkStart = this.messagesData.length == 0 ? -1 : this.messagesData[0].MsgId;
      let inputData = {
        "CId": this.cId,
      	"ChunkSize":5,
      	"ChunkStart":ChunkStart,
        "CompanyId": this.companyId,
        "DbCon": this.dbCon,
        "UserId":this.loginUserId,
        "UserTypeId": this.userTypeId
      }
      console.log(inputData);
      this.msgService.listAllConversationMessages(inputData)
      .subscribe((response: Response)=>{
        if(response['Status'] == 'Success'){
          console.log(response['ConversationMessages']);
          if(response['ConversationMessages'].length > 0){
            for (let i = response['ConversationMessages'].length - 1; i >= 0; i--) {
              this.messagesData.splice(0, 0, response['ConversationMessages'][i]);
            }
          }else{
            this.loadPreviousMsg = false;
          }
        }
      })
    }
  //}
}
}

  isServiceCall: boolean = false;
  sendMessage(){
    let LatestMsgId = -1;
    if(this.messageText.toString() == "" || this.messageText.trim().length == 0 || this.messageText.toString() == undefined || this.messageText.toString() == 'null'){
      this.service.alertMessage("Enter your message","error");
      return false;
    }
    const _input = {
    	"CId":-1,
    	"DbCon":this.dbCon,
    	"LatestMsgId":LatestMsgId,
      "Msg": this.textEncode(this.messageText),//this.messageText,
    	"MsgFile":this.fileName,
    	"MsgFileType":this.fileType,
    	"RecipientCompanyId" :this.recipientCompanyId,
    	"RecipientId": this.recipientId,
    	"RecipientUserTypeId":this.recipientUserTypeId,
    	"SenderCompanyId":this.companyId,
    	"SenderId":this.loginUserId,
    	"SenderUserTypeId":this.userTypeId
    };
    console.log(_input);
    this.isServiceCall = true;
    this.msgService.sendMessage(_input)
    .subscribe((res:Response)=>{
      this.isServiceCall = false;
      if(res['Status'] == 'Success'){
          this.openAttachmentModal = false;
          this.cId = res['ID'];
          this.messageText= '';
          this.fileName = '';
          this.refreshChat();
          this.scrollToBottom();
      }else{
        this.service.alertMessage(res['Message'],"error")
      }
    }, function (parameter) {
        this.isServiceCall = false;
        throw new Error("Not implemented yet");
    });
  }


  uploadMsg = '';
  fileName = '';
  fileType= '';
  openAttachmentModal : boolean= false;
  uploadFile(event) {
        this.uploadMsg = '';
        let file = event.target.files[0];
        let inputEl = event.target.files;
        //let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
        //get the total amount of files attached to the file input.
        let fileCount: number = inputEl.length;
        //create a new fromdata instance
        let formData = new FormData();
        if (fileCount > 0) { // a file was selected
          this.openAttachmentModal = true;
          var strFileName = String(inputEl[0].name);
          if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
            this.uploadMsg = "Attachment uploading, please wait…";
            formData.append('file', file);
            formData.append('CompanyId', sessionStorage.getItem('companyId'));
            this.service.uploadFile(formData)
            .subscribe((result)=>{
                  this.uploadMsg = '';
                  var respnose = result._body;
                  if(respnose){
                    const img_name = respnose.split("###");
                      if(img_name[0] == 'Success'){
                          //this.fileName = this.fileName+"/"+img_name[1];

                          this.fileName = img_name[1];
                          this.fileType = img_name[1].split('.').pop().toLowerCase();
                          // this.profileForm.patchValue({
                          //   'Img': img_name[1]
                          // });
                          this.service.alertMessage("Attachment successfully uploaded","success");
                      }else{
                          this.openAttachmentModal = false;
                          this.service.alertMessage("Attachment upload encountered an error. Please try again.","error");
                      }
                  }
                },
                (error) =>{  console.log(error);  });
            }else{
                this.openAttachmentModal = false;
                this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
                return false;
            }
          }else{
              this.openAttachmentModal = false;
              this.uploadMsg =" Please choose file";
          }
  }


    trackMessages(index, msg) {
        return msg ? msg.MsgId : undefined;
    }

    textDecode(pvarTxt){
      if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
          return this.service.decodeText(pvarTxt);
      }else{
        return '';
      }
    }

    textEncode(pvarTxt){
      if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
        return this.service.encodeText(pvarTxt);
      }
    }

    cancelAttachment(){
      this.uploadMsg = '';
      this.fileName = '';
      this.fileType= '';
      this.openAttachmentModal = false;
    }

    sendAttachmentMsg(){
      this.messageText = this.messageText == '' ? 'Attachment' : this.messageText;
      this.sendMessage()
    }
}
