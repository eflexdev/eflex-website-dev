import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {Router} from "@angular/router";
import { utils, write, WorkBook } from 'xlsx';

import { saveAs } from 'file-saver';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  empData:any = [];
  adminModal: boolean;
  companyId: any = -1;
  memberTypeId: any = 3;
  isLoader:boolean;
  compnayPlanTypeId : any = 2;
  isFreeze: boolean = false;
  filterByType: any = -1;
  constructor(private service: CompanyService) {
      this.compnayPlanTypeId =  sessionStorage.getItem("compnayPlanTypeId");
      this.isFreeze = sessionStorage.getItem("isFreeze") == 'true' ? true : false;
  }

  ngOnInit() {

    if(sessionStorage.getItem('companyId') != null){
        this.companyId = sessionStorage.getItem('companyId');
        this.getEmployeesList();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

  getEmployeesList(){
   const _input = {
    	"ChunkSize":500,
    	"ChunkStart":-1,
    	"DbCon": sessionStorage.getItem("DbCon"),
    	"Id":this.memberTypeId,
      "Filter": this.memberTypeId == 3 ? this.filterByType : -1,
    	"SearchString":""
    }
    this.isLoader = true;
    // this.service.listEmployees(_input)
    this.service.ListEmployeesV2(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.isLoader = false;
        this.empData= res['ListEmployees'];
      }else{
          this.isLoader = false;
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
        this.isLoader = false;
        this.service.alertMessage(error,'error');
    })
  }

  filterMembers(){
    this.empData = [];
    this.memberTypeId = this.memberTypeId;
    this.filterByType = -1;
    this.getEmployeesList();
  }

  filterEmp(){
    this.filterByType = this.filterByType;
    this.empData = [];
    this.getEmployeesList();
  }

  modal: boolean;
  status: any;
  name: string;
  loading: boolean;
  selectedIndex: any;
  deleteConfirmation: boolean;

  updateEmpStatus(obj, type){
    this.selectedIndex = obj;
    this.modal = true;
    this.status = type; //type == 0 ? 'In active' : 'Active';
    this.name  = obj['FName'];
  }

  isDeleteEmp: string = 'admin';
  deleteAdmin(obj,pvarType){
    this.selectedIndex = obj;
    this.deleteConfirmation = true;
    this.isDeleteEmp = pvarType;
    this.name  = obj['FName'] +" "+ obj['LName'];
  }

  updateConfirm(){
    this.loading = true;
    const _input = {
    	"CompanyUserId":this.selectedIndex['CompanyUserId'],
    	"DbCon": sessionStorage.getItem('DbCon'),
    	"IsActive": this.status,
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    }
    console.log(_input);
    this.service.inActiveEmployee(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.modal = false;
        this.loading = false;
        this.selectedIndex['InActive'] = 'False';
        this.service.alertMessage("Employee successfully marked as inactive","error");
      }else{
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
      this.service.alertMessage(error,'error');
    })
  }

  yesDeleteAdmin(){
    this.loading = true;
    console.log(this.selectedIndex);
    const _input = {
      "UserId":this.selectedIndex['CompanyUserId'],
      "DbCon": sessionStorage.getItem('DbCon'),
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    }
    console.log(_input);
    this.service.deleteUser(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.deleteConfirmation = false;
        this.loading = false;
        this.isDeleteEmp = 'admin';
        let index = this.empData.lastIndexOf(this.selectedIndex);
        this.empData.splice(index, 1);
        this.service.alertMessage("Admin successfully deleted","error");
      }else{
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
      this.service.alertMessage(error,'error');
    })
  }

  adminFName: string ='';
  adminLName: string = '';
  adminEmail: string = '';

  openAdminModal(){
    this.adminModal = true;
  }

  saveAdmin(form){
    this.loading = true;
    const _input = {
      "ContactNo":"",
    	"FName":form.controls.adminFName.value,
      "LName":form.controls.adminLName.value,
      "Email":form.controls.adminEmail.value,
    	"DbCon": sessionStorage.getItem('DbCon'),
      "UserTypeId":2,
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    }
    console.log(_input);
    this.loading = true;
    this.service.addAdmin(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.getEmployeesList()
        this.adminModal = false;
        this.loading = false;
        form.reset();
        this.service.alertMessage("Admin successfully added","error");
      }else{
          this.loading = false;
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
      this.loading = false;
      this.service.alertMessage(error,'error');
    })
  }

  trackByFn(index, item) {
    return index;
  }

  yesDeleteEmp(){
      this.loading = true;
      const _input = {
        "CompanyUserId":this.selectedIndex['CompanyUserId'],
        "DbCon": sessionStorage.getItem('DbCon'),
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      }
      this.service.deleteEmployee(_input)
      .subscribe((response:Response)=>{
        if(response['Status'] == "Success"){
          this.deleteConfirmation = false;
          this.loading = false;
          this.isDeleteEmp = 'admin';
          let index = this.empData.lastIndexOf(this.selectedIndex);
          this.empData.splice(index, 1);
          this.service.alertMessage("Member deleted successfully","error");
        }else{
            this.service.alertMessage(response['Message'],"error");
        }
      })
    }


    selectedIds:any = [];
    sendMailLoading: boolean = false;
    Allchecked: boolean = false;

    changeCheckbox(event, index){
      if(event.target.checked == true){
          this.empData.forEach(obj => {
            obj['checked'] = true;
          });
      }else{
        this.empData.forEach(obj => {
          obj['checked'] = false;
        });
      }
    }

    sendMailToSelected(){
      this.selectedIds = [];
      this.empData.forEach(obj => {
        if(obj['checked'] && obj['checked'] == true){
          this.selectedIds.push(obj['CompanyUserId']);
        }
      });
      if(this.selectedIds.length > 0){
        let _input = {
          "CompanyId":this.companyId,
          "CompanyUserIds":this.selectedIds
        }
        this.sendMailLoading = true;
        this.service.sendMailToUnUpdatedUser(_input)
        .subscribe((response:Response)=>{
          if(response['Status'] == 'Success'){
            this.sendMailLoading = false;
            this.service.alertMessage("Email sent to successfully for selected employees",'success');
            this.selectedIds = [];
            this.Allchecked = false;
            this.empData.forEach(obj => {
              obj['checked'] = false;
            });
          }else{
            this.sendMailLoading = false;
            this.service.alertMessage(response['Message'],'error');
          }
        },function name(error) {
            this.sendMailLoading = false;
            throw new Error("Not implemented yet");
        })
      }else{
        this.service.alertMessage("Please select employee to send mail",'error');
        return false;
      }
    }

    sortingName: string;
    isDesc: boolean;
    sort(name: string): void {
      if (name && this.sortingName !== name) {
        this.isDesc = false;
      } else {
        this.isDesc = !this.isDesc;
      }
      this.sortingName = name;
    }


  downloadCSV(){
      var i=1;
      var newArr = [];
      this.empData.forEach((res)=>{
        let obj = {};
        obj['Sr.No'] = i++;
        obj['Name'] = res['FName']+" "+ res['LName'] //this.transform(res['TransactionDate']);
        obj['Email'] = res['Email'];
        obj['Contact No'] = res['ContactNo'];
        obj['Username'] = res['UserName'];
        obj['Plan'] = res['PlanName'];
        obj['Status'] = res['InActive'] == 'True' ? 'Active' :'Inactive';
        newArr.push(obj);
      })

      const ws_name = 'employees';
      const wb: WorkBook = { SheetNames: [], Sheets: {} };
      const ws: any = utils.json_to_sheet(newArr);
      wb.SheetNames.push(ws_name);
      wb.Sheets[ws_name] = ws;
      const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

      function s2ab(s) {
        const buf = new ArrayBuffer(s.length);
        const view = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
          view[i] = s.charCodeAt(i) & 0xFF;
        };
        return buf;
      }
      saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), 'employess-list.xlsx');

  }



}
