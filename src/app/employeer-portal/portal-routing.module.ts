import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { EmployeerPortalComponent } from '../employeer-portal/employeer-portal.component';
import { SummaryComponent } from './summary/summary.component';
import { ActivityComponent } from './activity/activity.component';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { SettingsComponent } from './settings/settings.component';
import { EStatementsComponent } from './e-statements/e-statements.component';
import { MessagesComponent } from './messages/messages.component';
import { NotificationComponent } from './notification/notification.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { OffersComponent } from '../offers/offers.component';
import { FAQComponent } from '../faq/faq.component';
import { EmployeeClaimsComponent } from './employee-claims/employee-claims.component';

export const portalRoutes: Routes = [
  {
    path: 'employeer-portal',
    component: EmployeerPortalComponent,
    //canActivate: [AuthGuard],
    children: [
      {path: 'summary',  component: SummaryComponent },
      { path: 'activity',   component: ActivityComponent,
        data: {
          breadcrumb: 'Activity Detail'
        }
      },
      {path: 'employees', component: EmployeesListComponent},
      {path: 'settings', component: SettingsComponent},
      {path: 'e-statements', component: EStatementsComponent},
      {path: 'messages', component: MessagesComponent},
      {path: 'notifications', component: NotificationComponent},
      {path: 'add-employee', component: AddEmployeeComponent},
      {path: 'analytics', component: AnalyticsComponent},
      {path: 'offers', component: OffersComponent},
      {path: 'general-faq', component: FAQComponent},
      {path: 'claims/:id/:name', component: EmployeeClaimsComponent},
      { path: '',   redirectTo: '/summary',   pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(portalRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PortalRoutingModule { }
