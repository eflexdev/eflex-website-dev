import { Component, OnInit, HostListener } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../company.service';
import {Router}  from "@angular/router";
import { Idle } from 'idlejs/dist';
@Component({
  selector: 'app-employeer-portal',
  templateUrl: './employeer-portal.component.html',
  styleUrls: ['./employeer-portal.component.css']
})
export class EmployeerPortalComponent implements OnInit {
  chart: Chart;
  capacity_chart: any;
  NetGrossInitial: any;
  NetGrossRecurring: any;
  plansData: any=[];
  plansChart: any;

  constructor(private service: CompanyService, private router : Router) {
      const idle = new Idle()
      .whenNotInteractive()
      .within(10)
      .do(() => //console.log('time out')
          this.sessionTimeOut()
      )
      .start();
   }

   sessionTimeOut(){
       //this.service.alertMessage("Session has been timeout","success");
       sessionStorage.clear();
       sessionStorage.removeItem('loginUserId');
       sessionStorage.removeItem('UserToken');
       this.router.navigate(['/']);
   }
  ngOnInit() {
    this.getScreenSize();
    if(sessionStorage.getItem("DbCon") != null){
    const _input = {
      "DbCon":sessionStorage.getItem("DbCon")
    }
    this.service.getPaymentOverview(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.NetGrossInitial = res['NetGrossInitial'];
        this.NetGrossRecurring = res['NetGrossRecurring'];
        if(res['ClasswisePayments'].length > 0){
           for(var k=0; k< res['ClasswisePayments'].length; k++){
             const obj=  res['ClasswisePayments'][k];
             const offData={};
             offData['name'] = obj['PlanName'];
             offData['y']=obj.NoOfEmployees;
             offData['sliced']= k == 0 ? true : false;
             offData['selected']= k == 0 ? true : false;
             this.plansData.push(offData)
          };
                 //console.log(this.officeReportData)
      }

      this.showPlnsChart();
      }
    });

  }else{
    this.router.navigate(['/login']);
  }
  }

  screenHeight: any;
  minusHeight: any = 0;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 140;
        }else{
          this.minusHeight = 300;
        }
        this.screenHeight = window.innerHeight;
    }

  showPlnsChart(){
    this.plansChart = new Chart({
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: 'Plans Overview'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.y}</b>'
          },
          credits: {
              enabled: false
          },
          exporting: {
              enabled: false
          },

          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: 'No of Employees:{point.y}',
                      style: {
                          //  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  },
                  showInLegend: true
              }
          },
          series: [{
              name: 'Number Of Rooms',
              //colorByPoint: true,
              data: this.plansData
          }]
      });
  }

}
