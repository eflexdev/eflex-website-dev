import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeerPortalComponent } from './employeer-portal.component';

describe('EmployeerPortalComponent', () => {
  let component: EmployeerPortalComponent;
  let fixture: ComponentFixture<EmployeerPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeerPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeerPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
