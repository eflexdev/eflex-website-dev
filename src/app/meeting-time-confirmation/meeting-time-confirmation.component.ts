import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../company.service';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { AmazingTimePickerService } from 'amazing-time-picker'; // this lin
import {IMyDpOptions} from 'mydatepicker';
import {BrokerService} from '../broker.service'
import {Router, ActivatedRoute} from "@angular/router"
@Component({
  selector: 'app-meeting-time-confirmation',
  templateUrl: './meeting-time-confirmation.component.html',
  styleUrls: ['./meeting-time-confirmation.component.css']
})
export class MeetingTimeConfirmationComponent implements OnInit {

  referralId: number;
  isLoading: boolean = false;
  loading: boolean = false;
  employeeForm: FormGroup;
  dateConfirmation: boolean = false;
  timeSlotId: any = -1;
  isOtherTimeSlot: boolean = false;
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     // disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()},
     disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1}
  };
  constructor(
    private service: CompanyService,
    private router : Router,
    private brokerService: BrokerService,
    private fb:FormBuilder,
    private acitvateRoute: ActivatedRoute,
    private atp: AmazingTimePickerService
  ) {
      this.employeeForm = this.fb.group({
        "Attendents":[''],
        "PreferredDateTime":['',[Validators.required]],
        "Location":['',[Validators.required]],
        "Time":['', Validators.required],
        "Notes":['']
      })
   }

   open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
    });
  }


  ngOnInit() {
    this.acitvateRoute.params.subscribe((param)=>{
      this.referralId = +param['id'];
    })
    this.viewTimeSlots();
  }
  referralData: any = [];
  referralStatus: any ='';
  viewTimeSlots(){
        let _input = {
          "Id":this.referralId
        }
        this.brokerService.getPreferredSlotsById(_input)
        .subscribe((response:Response)=>{
          if(response['Status'] =="Success"){
            this.referralStatus = response['BrokerReferralStatus'];
            this.referralData = response['PreferredSlotsList'];
            console.log(this.referralData);
          }else{
            this.service.alertMessage(response['Message'],"error");
          }
        },(error:Error)=>{
           this.service.alertMessage(error['Message'],"error");
        });
    }

    confirmDateTime(pvarData){
      this.timeSlotId = pvarData['PreferredSlotId'];
      this.dateConfirmation = true;
    }
    yesConfirmDate(){
      let input2 = {
        "BrokerReferralId": this.referralId,
        "PreferredSlotId": this.timeSlotId,
        "PreferredDateTime":'',
        "Attendents":'',
        "Location":'',
        "Notes":''
      }
      console.log(input2);
      this.isLoading = true;
      this.brokerService.updatePreferredSlots(input2)
     .subscribe((response:Response)=>{
       if(response['Status'] =="Success"){
           this.isLoading = false;
           this.service.alertMessage("Meeting time confirm successfully","success");
           this.router.navigate(['/']);
           this.employeeForm.reset();
       }else{
            this.service.alertMessage(response['Message'],'error');
            this.isLoading = false;
       }
     })
    }

  submitDetail(){
      if(this.employeeForm.valid){
          let lineItem = this.employeeForm.controls;
          let dateTime = '';
          if(lineItem.PreferredDateTime.value.formatted){
             dateTime= lineItem.PreferredDateTime.value.formatted+" "+this.employeeForm.controls.Time.value;
          }else{
            if(typeof  lineItem.PreferredDateTime === 'object'){
              let mm = parseInt(lineItem.PreferredDateTime.value.date.month) < 10 ? '0'+lineItem.PreferredDateTime.value.date.month: lineItem.PreferredDateTime.value.date.month;
              let dd = parseInt(lineItem.PreferredDateTime.value.date.day) < 10 ? '0'+lineItem.PreferredDateTime.value.date.day: lineItem.PreferredDateTime.value.date.day;
              dateTime = mm+"/"+ dd +"/"+lineItem.PreferredDateTime.value.date.year +" "+this.employeeForm.controls.Time.value;
            }else{
              dateTime = lineItem.PreferredDateTime+" "+this.employeeForm.controls.Time.value;
            }
          }

        let input2 = {
          "BrokerReferralId": this.referralId,
          "PreferredSlotId": -1,
          "PreferredDateTime":dateTime,
          "Attendents":this.employeeForm.controls.Attendents.value,
          "Location":this.employeeForm.controls.Location.value,
          "Notes":this.employeeForm.controls.Notes.value
        }
        // console.log(this.transformDate(dateTime));
        this.isLoading = true;
        this.brokerService.updatePreferredSlots(input2)
       .subscribe((response:Response)=>{
         if(response['Status'] =="Success"){
             this.isLoading = false;
             this.service.alertMessage("Time suggestion successfully submit","success");
             this.router.navigate(['/']);
             this.employeeForm.reset();
         }else{
              this.service.alertMessage(response['Message'],'error');
              this.isLoading = false;
         }
       })
     }else{
       this.service.alertMessage("Please complete all required fields","error");
       return false;
     }
   }

   transformDate(value){
     var s=value;
     value=s.replace("AM"," AM")
     let date = new Date(value);
     console.log(date);
     let d=date.getMonth() + 1+"/"+date.getUTCDate()+"/"+ date.getFullYear()+" "+date.getUTCHours()+":"+date.getUTCMinutes();
     //debugger;
     return d;
   }
}
