import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingTimeConfirmationComponent } from './meeting-time-confirmation.component';

describe('MeetingTimeConfirmationComponent', () => {
  let component: MeetingTimeConfirmationComponent;
  let fixture: ComponentFixture<MeetingTimeConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingTimeConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingTimeConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
