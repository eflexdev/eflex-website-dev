import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service'
import {BrokerService} from '../../broker.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod'

@Component({
  selector: 'app-broker-registration',
  templateUrl: './broker-registration.component.html',
  styleUrls: ['./broker-registration.component.css']
})
export class BrokerRegistrationComponent implements OnInit {
  loginUserId: any = -1;
  step:number=2;
  isFormValid: boolean = false;
  brokerForm: FormGroup;
  isLoading: boolean;
  modal: boolean;
  hostUrl:string='';
  provienceData: any= [];
  @ViewChild('fileInput') fileInput: ElementRef;
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     editableDateField: false,
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };
 emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(  private fb:FormBuilder,
    private service: CompanyService,
    private BrokerService: BrokerService,
    private router: Router, private el : ElementRef) {
      this.hostUrl = environment.hostUrl;
      this.brokerForm = this.fb.group({
          "accountType":['Personal'],
          "fname":['',[Validators.required]],
          "lname": ['',[Validators.required]],
          "username":['',[Validators.required]],
          "emailAddress":['', [Validators.required, Validators.pattern(this.emailPattern)]],
          "address":[''],
          "city":[''],
          "province":[''],
          "contact":[''],
          "postal":[''],
          //"contact":['',[Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
        //  "postal":['',[Validators.required, Validators.minLength(5)]],
          "companyName":[''],
          "contactPerson":[''],
          "companyEmail":[''],
          "companyAddress":[''],
          "companyCity":[''],
          "companyProvince":[''],
          "companyContact":[''],
          "companyPostal":[''],
          "licence":[''],
          "insurance":[''],
          "licenceBack":[''],
          "GSTNo":[''],
          "socialInsurance":['',[Validators.required]],
          "CRA":[''],
          "DOB":['',[Validators.required]],
          "bankname":['',[Validators.required]],
          "accountNumber":['',[Validators.required]],
          "transId":['',[Validators.required]],
          "instId":['',[Validators.required]],
          "iagree":['',[Validators.required]]
      });
       this.formControlValueChanged();
    }

    ngOnInit() {
      this.service.populateProvince()
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.provienceData = res['ProvinceList'];
        }
      },(error)=>{})
      this.getScreenSize();
    }

    screenHeight: any='';
    minusHeight: any = 0;
    @HostListener('window:resize', ['$event'])

    getScreenSize(event?) {
          if(window.innerHeight < 1200){
            this.minusHeight = 0;
          }else{
            this.minusHeight = 150;
          }
          this.screenHeight = window.innerHeight;
      }

    formControlValueChanged() {
        //const phoneControl = this.brokerForm.get('phonenumber');
        this.brokerForm.get('accountType').valueChanges.subscribe(
            (mode: string) => {
                console.log(mode);
                if (mode === 'Agent') {
                  this.brokerForm.get('companyName').setValidators([Validators.required]);
                  //this.brokerForm.get('contactPerson').setValidators([Validators.required]);
                  this.brokerForm.get('companyAddress').setValidators([Validators.required]);
                  this.brokerForm.get('companyCity').setValidators([Validators.required]);
                  this.brokerForm.get('companyProvince').setValidators([Validators.required]);
                  this.brokerForm.get('companyContact').setValidators([Validators.required]);
                  this.brokerForm.get('companyPostal').setValidators([Validators.required]);
                  this.brokerForm.get('companyEmail').setValidators([Validators.required, Validators.pattern(this.emailPattern)]);
                  this.brokerForm.get('CRA').setValidators([Validators.required]);

                  this.brokerForm.get('address').clearValidators();
                  this.brokerForm.get('city').clearValidators();
                  this.brokerForm.get('province').clearValidators();
                  this.brokerForm.get('contact').clearValidators();
                  this.brokerForm.get('postal').clearValidators();
                }
                else if (mode === 'Personal') {
                      this.brokerForm.get('companyName').clearValidators();
                      this.brokerForm.get('contactPerson').clearValidators();
                      this.brokerForm.get('companyAddress').clearValidators();
                      this.brokerForm.get('companyCity').clearValidators();
                      this.brokerForm.get('companyProvince').clearValidators();
                      this.brokerForm.get('companyContact').clearValidators();
                      this.brokerForm.get('companyPostal').clearValidators();
                      this.brokerForm.get('companyEmail').clearValidators();
                      this.brokerForm.get('CRA').clearValidators();
                      //type personal then set validation
                    this.brokerForm.get('address').setValidators([Validators.required]);
                    this.brokerForm.get('city').setValidators([Validators.required]);
                    this.brokerForm.get('province').setValidators([Validators.required]);
                    this.brokerForm.get('contact').setValidators([Validators.required, Validators.minLength(10), Validators.maxLength(10)]);
                    this.brokerForm.get('postal').setValidators([Validators.required]);
                }
                  this.brokerForm.get('companyName').updateValueAndValidity();
                  this.brokerForm.get('contactPerson').updateValueAndValidity();
                  this.brokerForm.get('companyAddress').updateValueAndValidity();
                  this.brokerForm.get('companyCity').updateValueAndValidity();
                  this.brokerForm.get('companyProvince').updateValueAndValidity();
                  this.brokerForm.get('companyContact').updateValueAndValidity();
                  this.brokerForm.get('companyPostal').updateValueAndValidity();
                  this.brokerForm.get('companyEmail').updateValueAndValidity();
                  this.brokerForm.get('CRA').updateValueAndValidity();

                  this.brokerForm.get('address').updateValueAndValidity();
                  this.brokerForm.get('city').updateValueAndValidity();
                  this.brokerForm.get('province').updateValueAndValidity();
                  this.brokerForm.get('contact').updateValueAndValidity();
                  this.brokerForm.get('postal').updateValueAndValidity();

            });
    }
    saveBrokerDetail(brokerForm){
      if(this.brokerForm.valid){
        //this.brokerForm.value['ReceiptDate']=   this.brokerForm.value['ReceiptDate'].formatted
        let formData = this.brokerForm.value;
        //formatted
        let _input = {
          "Address1":formData.address,
        	"Address2":"",
        	"BusinessNo":formData.CRA,
        	"City":formData.city,
        	"CompanyAddress":formData.companyAddress,
        	"CompanyCity":formData.companyCity,
        	"CompanyContactNo":formData.companyContact,
        	"CompanyContactPerson":formData.contactPerson,
        	"CompanyEmail":formData.companyEmail,
        	"CompanyName":formData.companyName,
        	"CompanyPostalCode":formData.companyPostal,
        	"CompanyProvinceId":formData.companyProvince == '' ? -1 :  formData.companyProvince,
        	"Country":"",
        	"DOB":formData.DOB.formatted,
        	"EOInsuranceNo":formData.insurance,
        	"Email":formData.emailAddress,
        	"FirstName":formData.fname,
        	"IsAgrement":formData.accountType == 'Agent' ? true : false,
        	"LandlineNo":"",
        	"LastName":formData.lname,
        	"Licence":formData.licenceBack,
        	"MobileNo":formData.contact,
        	"PostalCode":formData.postal,
        	"ProvinceId":formData.province == '' ? formData.companyProvince : formData.province,
        	"SocialInsuranceNo":formData.socialInsurance,
        	"UserName":formData.username,
          "IsPersonal": formData.accountType == 'Personal' ? true : false,
          "GSTNo": formData.GSTNo,
          "UploadLicenceFront":formData.licence
        }
        console.log( JSON.stringify(_input, undefined, 2));
        this.isLoading = true;
        this.BrokerService.registerBroker(_input)
        .subscribe((response:Response)=>{
          if(response['Status'] == 'Success'){
              this.loginUserId = response['ID'];
              sessionStorage.setItem("UserToken", response['UserToken']);
              this.addPAPDetail();
              //this.goToStep(2);
          }else{
              this.service.alertMessage(response['Message'],'error','');
              this.isLoading = false;
          }
        },(error)=>{
          this.service.alertMessage(error,'error','');
          this.isLoading = false;
        })
      }
    }

    addPAPDetail(){
      let formData = this.brokerForm.value;
      let _input = {
        "AccountNumber": this.textEncode(formData.accountNumber),
      	"BankName":this.textEncode(formData.bankname),
      	"InstitutionId":this.textEncode(formData.instId),
      	"LoggedInUserId":this.loginUserId,
      	"TransitId":this.textEncode(formData.transId),
      	"UserId":this.loginUserId
      }
      this.BrokerService.addEflexUserPAPDetails(_input)
      .subscribe((response:Response)=>{
          if(response['Status'] == "Success"){
            this.service.alertMessage("Your account has been successfully registered with eflex! You should receive a welcome email momentarily.",'success','2000');
            this.isLoading = false;
            this.brokerForm.reset();
            this.router.navigate(['/']);
          }else{
              this.service.alertMessage(response['Message'],'error');
          }
      })
    }

    uploadMsg = '';
    fileName: string= '';

    fileType: string = '';
   uploadInsurance(event, type) {
         this.uploadMsg = '';

         let inputEl: HTMLInputElement = (type == 'licence-back' ? this.el.nativeElement.querySelector('#licenceFileBack') : (type == 'insurance' ? this.el.nativeElement.querySelector('#insuranceFile') : this.el.nativeElement.querySelector('#licenceFile')));
         //get the total amount of files attached to the file input.
         let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
         let formData = new FormData();
         if (fileCount > 0) { // a file was selected
           var strFileName = String(inputEl.files.item(0).name);
           if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
             this.uploadMsg = "File uploading, please wait…";
             formData.append('file', inputEl.files.item(0));
             formData.append('FolderName', 'Broker');
             this.BrokerService.uploadFile(formData)
             .subscribe((result)=>{
                   this.uploadMsg = '';
                   var respnose = result._body;
                   if(respnose){
                     const img_name = respnose.split("###");
                       if(img_name[0] == 'Success'){
                           //this.fileName = this.fileName+"/"+img_name[1];
                          this.modal = false;
                          this.fileName = img_name[1];
                          if(type == "insurance"){
                            this.brokerForm.patchValue({
                              'insurance': img_name[1]
                            });
                          }else if(type == "licence"){
                            this.brokerForm.patchValue({
                              'licence': img_name[1]
                            });
                          }else if(type == "licence-back"){
                            this.brokerForm.patchValue({
                              'licenceBack': img_name[1]
                            });
                          }
                          this.service.alertMessage("File upload successfully","success");
                       }else{
                         this.service.alertMessage("File upload encountered an error. Please try again.","error");
                       }
                   }
                 },
                 (error) =>{  console.log(error);  });
             }else{
               this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
               return false;
             }
           }else{
               this.uploadMsg =" Please choose file";
           }
  }

   uploadChequeFile(event) {
         this.uploadMsg = '';
         let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#chequeFile');
         //get the total amount of files attached to the file input.
         let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
         let formData = new FormData();
         if (fileCount > 0) { // a file was selected
           var strFileName = String(inputEl.files.item(0).name);
           if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
             this.uploadMsg = "File uploading, please wait…";
             formData.append('file', inputEl.files.item(0));
             formData.append('FolderName', 'Broker');
             this.BrokerService.uploadFile(formData)
             .subscribe((result)=>{
                   this.uploadMsg = '';
                   var respnose = result._body;
                   if(respnose){
                     const img_name = respnose.split("###");
                       if(img_name[0] == 'Success'){
                           //this.fileName = this.fileName+"/"+img_name[1];
                          this.modal = false;
                          this.fileName = img_name[1];
                           // this.profileForm.patchValue({
                           //   'Img': img_name[1]
                           // });
                           this.service.alertMessage("Void cheque successfully upload","success");
                       }else{
                         this.service.alertMessage("Void cheque upload encountered an error. Please try again.","error");
                       }
                   }
                 },
                 (error) =>{  console.log(error);  });
             }else{
               this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
               return false;
             }
           }else{
               this.uploadMsg ="Please select the file for your void cheque";
           }
  }
   goToStep(pvarChoice){
     this.step=pvarChoice;
   }

   goToNextStep(pvarChoice){
      if(this.step==3){
        if(this.brokerForm.get('accountType').value == "Agent"){
              if(this.brokerForm.value['companyName'] == '' ||
                this.brokerForm.value['companyAddress'] == '' || this.brokerForm.value['companyPostal'] == ''
               ||  this.brokerForm.value['companyCity'] == ''
                ||  this.brokerForm.value['companyProvince'] == ''){
                  this.service.alertMessage("Plase complete all required fields","error");
                  return false;
              }
        }else{
            if(this.brokerForm.value['fname'] == '' || this.brokerForm.value['lname'] == '' ){
              this.service.alertMessage("Plase complete all required fields","error");
              return false;
            }
            if(this.brokerForm.value['username'] == '' || this.brokerForm.value['emailAddress'] == '' ){
             this.service.alertMessage("Plase complete all required fields","error");
             return false;
           }
           if(this.brokerForm.controls['emailAddress'].valid == false ){
             this.service.alertMessage("Please enter valid email","error");
             return false;
           }
        }

      }
      if(this.step==4){
          if(this.brokerForm.get('accountType').value == "Agent"){
            if(this.brokerForm.value['fname'] == '' || this.brokerForm.value['lname'] == '' ){
              this.service.alertMessage("Plase complete all required fields","error");
              return false;
            }
            if(this.brokerForm.value['username'] == '' || this.brokerForm.value['emailAddress'] == ''
             || this.brokerForm.value['companyEmail'] == '' || this.brokerForm.value['companyContact'] == '' ){
             this.service.alertMessage("Plase complete all required fields","error");
             return false;
           }
           if(this.brokerForm.controls['emailAddress'].valid == false ){
             this.service.alertMessage("Please enter valid owner email","error");
             return false;
           }
           if(this.brokerForm.controls['companyEmail'].valid == false ){
             this.service.alertMessage("Please enter valid company email address","error");
             return false;
           }
          }else{
            if(this.brokerForm.value['address'] == '' || this.brokerForm.value['city'] == '' || this.brokerForm.value['contact'] == '' || this.brokerForm.value['province'] == '' || this.brokerForm.value['postal'] == ''){
              this.service.alertMessage("Plase complete all required fields","error");
              return false;
            }
         }
      }
      // if(this.brokerForm.get('accountType').value == "Agent"){
      //   if(this.step==5){
      //       if(this.brokerForm.value['companyName'] == '' ||
      //       this.brokerForm.value['contactPerson'] == '' || this.brokerForm.value['companyContact'] == ''
      //        || this.brokerForm.value['companyAddress'] == '' || this.brokerForm.value['companyPostal'] == ''
      //        || this.brokerForm.value['companyEmail'] == '' ||  this.brokerForm.value['companyCity'] == ''
      //         ||  this.brokerForm.value['companyProvince'] == ''){
      //           this.service.alertMessage("Plase complete all required fields","error");
      //           return false;
      //       }
      //   }
      // }

      if(this.step==5){
          if(this.brokerForm.value['socialInsurance'] == '' || this.brokerForm.value['DOB'] == '' ){
            this.service.alertMessage("Plase complete all required fields","error");
            return false;
          }
      }

      if(this.step==7){
          if(this.brokerForm.value['bankname'] == '' || this.brokerForm.value['accountNumber'] == '' || this.brokerForm.value['transId'] == '' || this.brokerForm.value['instId'] == '' ){
            this.service.alertMessage("Plase complete all required fields","error");
            return false;
          }
      }
      if(pvarChoice < 8){
        if(pvarChoice == 2){
            //this.goToStep(3);
            this.goToStep(pvarChoice+1);
        }else{
            this.goToStep(pvarChoice+1);
          // if(this.brokerForm.get('accountType').value == "Personal"){
          //   if(pvarChoice == 4){
          //       this.goToStep(6);
          //   }else{
          //     this.goToStep(pvarChoice+1);
          //   }
          // }else{
          //     this.goToStep(pvarChoice+1);
          // }
        }
      }
  }

  goToPrevStep(pvarChoice){
      if(pvarChoice > 2){
        if(this.brokerForm.get('accountType').value == "Personal"){
          this.goToStep(pvarChoice-1);
          // if(this.step==6){
          //   this.goToStep(4);
          // }else{
          //   this.goToStep(pvarChoice-1);
          // }
        }else{
          this.goToStep(pvarChoice-1);
        }
      }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 textEncode(pvarTxt){
     if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
       return this.service.encodeText(pvarTxt);
     }
 }

}
