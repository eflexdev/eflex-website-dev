import { Component, OnInit, HostListener } from '@angular/core';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service';
import { Router }              from '@angular/router';
import { ForgetForm }            from '../../register/data/formData.model';
@Component({
  selector: 'app-broker-login',
  templateUrl: './broker-login.component.html',
  styleUrls: ['./broker-login.component.css']
})
export class BrokerLoginComponent implements OnInit {

  loading:boolean;
  modal:boolean;
  username:string;password: string;company_code:string;
  forgetLoading:boolean;
  forgetForm: ForgetForm;
  inactiveModal: boolean;

  constructor( private service: CompanyService, private BrokerService: BrokerService, private router: Router) {
   }

  ngOnInit() {
    this.getScreenSize();
  }

  screenHeight: any='';
  minusHeight: any = 0;
  @HostListener('window:resize', ['$event'])

  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 550;
        }else{
          this.minusHeight = 600;
        }
        this.screenHeight = window.innerHeight;
    }
  login(form){
    const _input ={
      "UserName": form.controls.username.value,//.replace(/\s/g, ""),
      "Password": form.controls.password.value, //.replace(/\s/g, "")
    }
    this.loading = true;
    this.BrokerService.brokerLogin(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        if(res['IsApproaved'] == true){
          this.loading = false;
          sessionStorage.setItem("loginUserId", res['ID']);
          sessionStorage.setItem("UserToken", res['UserToken']);
          sessionStorage.setItem("userName", res['UserName']);
          sessionStorage.setItem("userTypeId", res['UserTypeId']);
          sessionStorage.setItem("isPersonal", res['IsPersonal']);
          // sessionStorage.setItem("companyId", res['CompanyId']);
          sessionStorage.setItem("brokerId", res['BrokerId']);
          this.service.setActiveToken(res['UserToken']);
          this.router.navigate(['/broker-portal/dashboard']);
        }else{
            this.loading = false;
            this.service.alertMessage("Your account is pending approval. You will receive an email when it is approved by our admin team.","error");
        }

    }else{
      this.loading = false;
      this.service.alertMessage(res['Message'],'error','');
    }
    },(error)=>{
        this.loading = false;
      this.service.alertMessage(error,'error','')
    })
  }

  openModal(type){
    this.modal = true;
  }

  sendMail(form){
    if(form.valid){
      let username = form.controls.fusername.value;
      const _input ={
        "UserName": username
      }
      this.forgetLoading = true;
      console.log(_input)
      this.BrokerService.forgotBrokerPassword(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.modal = false;
            this.forgetLoading = false;
            form.reset();
            this.service.alertMessage("A new password has been sent to your registered email address.",'success');
        }else{
          this.forgetLoading = false;
          this.service.alertMessage(res['Message'],'error')
        }
      },(error)=>{
        this.forgetLoading = false;
        this.service.alertMessage(error,'error','')
      })
    }
  }


  togglePassword(){
       var x = document.getElementById("loginPassword");
       if (x['type'] === "password") {
           x['type'] = "text";
       } else {
           x['type'] = "password";
       }
  }

}
