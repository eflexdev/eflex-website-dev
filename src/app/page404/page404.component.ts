import { Component, OnInit } from '@angular/core';
import { Router }              from '@angular/router';
@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.css']
})
export class Page404Component implements OnInit {
  userTypeId: any = 1;
  constructor( private router: Router) { }

  ngOnInit() {
    this.userTypeId = sessionStorage.getItem('userTypeId') != undefined ? sessionStorage.getItem('userTypeId') : 1;

    if(this.userTypeId != 1){
      this.router.navigate(['/member-portal/dashboard']);
    }else{
      sessionStorage.clear();
      this.router.navigate(['/login']);
    }
  }

}
