import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http';
import { FormsModule, ReactiveFormsModule }        from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import * as $ from 'jquery';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need

import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import { AppDatePickerDirective } from './app-date-picker.directive';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { PortalModule } from './employeer-portal/portal.module';
import { MemberPortalModule } from './member-portal/member-portal.module';
import { BrokerPortalModule } from './broker-portal/broker-portal.module';


import { NavbarComponent }    from './register/navbar/navbar.component';
import { PersonalComponent }    from './register/personal/personal.component';
import { WorkComponent }        from './register/plan-reviews/work.component';
import { AddressComponent }     from './register/address/address.component';
import { ResultComponent }      from './register/result/result.component';

import { MemberDetailComponent }      from './register/member-detail/member-detail.component';
import { BusinessComponent }     from './register/business-type/business.component';
import { WorkflowGuard }        from './register/workflow/workflow-guard.service';

import { WorkflowService }      from './register/workflow/workflow.service';

/* Shared Service */
import { FormDataService }    from './register/data/formData.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

//import { ValidateFieldsSubmitFormComponent } from './validate-fields-submit-form/validate-fields-submit-form.component';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import {CompanyService} from './company.service';
import { BankDetailComponent } from './bank-detail/bank-detail.component';
//import { EmployeerPortalComponent } from './employeer-portal/employeer-portal.component';
import { PaymentOverviewComponent } from './payment-overview/payment-overview.component';
import { Page404Component } from './page404/page404.component';
import { MemberPortalComponent } from './member-portal/member-portal.component';
import {CsvService} from 'angular2-json2csv';

import {MyDate} from "./date-pipe";
import {MessagesService} from './messages.service';
import {BrokerService} from './broker.service';
import {MemberService} from './member.service';
import { OfflineComponent } from './offline/offline.component';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import { ReferralSourceComponent } from './register/referral-source/referral-source.component';
import { BrokerRegistrationComponent } from './broker/broker-registration/broker-registration.component';
import { BrokerLoginComponent } from './broker/broker-login/broker-login.component';
import { EmpCardDetailComponent } from './emp-card-detail/emp-card-detail.component';
//import { BrokerPortalComponent } from './broker-portal/broker-portal.component';
import {PasswordInputComponent} from './password-input-field';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutEmployeeComponent } from './about-employee/about-employee.component';
import { AboutEmployeerComponent } from './about-employeer/about-employeer.component';
import { AboutBrokerComponent } from './about-broker/about-broker.component';
import { RtqFormComponent } from './rtq-form/rtq-form.component';
import { PaymentDetailComponent } from './register/payment-detail/payment-detail.component';
import { CongratulationsComponent } from './congratulations/congratulations.component';
import { AccountAuthenticateComponent } from './account-authenticate/account-authenticate.component';
import { MeetingTimeConfirmationComponent } from './meeting-time-confirmation/meeting-time-confirmation.component';
import { CompanyAccountTypeComponent } from './register/company-account-type/company-account-type.component';
//import { FAQComponent } from './faq/faq.component';
//import { OffersComponent } from './offers/offers.component';


const appRoutes: Routes = [
 { path: 'register', component: RegisterComponent, children:[
   { path: 'personal',  component: PersonalComponent },
    { path: 'plan-review',  component: WorkComponent},
    { path: 'address',  component: AddressComponent },//canActivate: [WorkflowGuard]
    { path: 'plan',  component: ResultComponent },
    { path: 'business-type',  component: BusinessComponent },
    { path: 'member-detail',  component: MemberDetailComponent },
    { path: 'referral-source',  component: ReferralSourceComponent },
    {path: 'payment-detail', component: PaymentDetailComponent},
    {path: 'account-type', component: CompanyAccountTypeComponent},
    { path: '',   redirectTo: 'personal', pathMatch: 'full' }
 ]},
   { path: 'login', component: LoginComponent },
   { path: 'bank-detail', component: BankDetailComponent },
   { path: 'payment-overview', component: PaymentOverviewComponent },
   {
     path: 'join-us',
     component: LandingComponent,
     data: { title: 'eFlex' }
   },
   { path: 'offline', component: OfflineComponent },
   { path: 'about-us', component: AboutUsComponent },
   { path: 'faqs', component: ContactUsComponent },
   { path: 'about-broker', component: AboutBrokerComponent },
   { path: 'about-employee', component: AboutEmployeeComponent },
   { path: 'about-employer', component: AboutEmployeerComponent },
   { path: 'broker', component: BrokerRegistrationComponent },
   { path: 'broker-login', component: BrokerLoginComponent },
   { path: 'employee-detail', component: EmpCardDetailComponent },
   { path: 'rtq-detail/:id', component: RtqFormComponent },
   { path: 'meeting-time-confirmation/:id', component: MeetingTimeConfirmationComponent },
   {path: 'congratulation', component: CongratulationsComponent},
   {path: 'authenticate/:companyId/:userId', component: AccountAuthenticateComponent},
   { path: '',   redirectTo: '/join-us',   pathMatch: 'full'},
   {path: '**', component:Page404Component},
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    LandingComponent,
    NavbarComponent, PersonalComponent, WorkComponent,
    AddressComponent, ResultComponent, HeaderComponent,
    FooterComponent,MemberDetailComponent,BusinessComponent,
    FieldErrorDisplayComponent,
    BankDetailComponent,
    //EmployeerPortalComponent,
    PaymentOverviewComponent,
    AppDatePickerDirective,
    Page404Component,
    OfflineComponent,
    ReferralSourceComponent,
    BrokerRegistrationComponent,
    BrokerLoginComponent,
    EmpCardDetailComponent,
    PasswordInputComponent,
    AboutUsComponent,
    ContactUsComponent,
    AboutEmployeeComponent,
    AboutEmployeerComponent,
    AboutBrokerComponent,
    RtqFormComponent,
    PaymentDetailComponent,
    CongratulationsComponent,
    AccountAuthenticateComponent,
    MeetingTimeConfirmationComponent,
    CompanyAccountTypeComponent,
    //FAQComponent,
    //  BrokerPortalComponent,
  //  MyDate

  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    PortalModule,
    MemberPortalModule,
    BrokerPortalModule,
    BrowserModule,
    HttpModule,
    MyDatePickerModule,
    TrimValueAccessorModule,
    InfiniteScrollModule,
    BrowserAnimationsModule,
    AmazingTimePickerModule,
  //  NgxMyDatePickerModule.forRoot(),
    RouterModule.forRoot(appRoutes,
      { enableTracing: false,useHash: false}
    )
  ],
  providers: [{ provide: FormDataService, useClass: FormDataService },
                   { provide: WorkflowService, useClass: WorkflowService },
                   WorkflowGuard, CompanyService, CsvService,MessagesService,
                 MemberService, BrokerService ],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
