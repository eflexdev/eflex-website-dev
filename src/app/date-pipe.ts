import {Pipe,PipeTransform, Directive, ElementRef, HostListener } from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  'name':'mydate'
})
export class MyDate implements PipeTransform{
  transform(value){
    var s=value;
    value=s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    let d = new Date(value+" UTC");
    d.toLocaleString().replace(/GMT.*/g,"");
    value = datePipe.transform(d, 'MMM d, y');
    return value;
  }
}

@Pipe({
  'name':'datetime'
})
export class DateTime implements PipeTransform{
  transform(value){
    if(value){
      var s= value;
      var AMPM = value.slice(-2);
      value=s.replace(AMPM," "+AMPM)
       var datePipe = new DatePipe("en-US");
      // //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      // value = datePipe.transform(new Date(value).toDateString(), 'yMMMdjms');
      let d = new Date(value+" UTC");
      d.toLocaleString().replace(/GMT.*/g,"");
      value = datePipe.transform(d, 'MMM d, y hh:mm a');
     return value;
    }
  }
}



@Pipe({
  'name':'dateFormat'
})
export class dateFormat implements PipeTransform{
  transform(value){
    if(value){
      var s= value;
      var AMPM = value.slice(-2);
      value=s.replace(AMPM," "+AMPM)
       var datePipe = new DatePipe("en-US");
      // //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      // value = datePipe.transform(new Date(value).toDateString(), 'yMMMdjms');
      let d = new Date(value+" UTC");
      d.toLocaleString().replace(/GMT.*/g,"");
      value = datePipe.transform(d, 'MMM d, y');
     return value;
    }
  }
}

@Pipe({
  name: 'filterUser'
})

export class FilterUserPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
    searchText = searchText.toLowerCase();

    return items.filter( it => {
      return it['Name'].toLowerCase().includes(searchText);
    });
   }
}



  //----------------------------
  @Pipe({
    name: 'orderBy'
  })
  export class OrderByPipe implements PipeTransform {
      transform( array: Array<any>, orderField: string, orderType: boolean ): Array<string> {
      if (!Array.isArray(array) || array.length <= 0) {
        return null;
      }
      array.sort( ( a: any, b: any ) => {
          let ae = a[ orderField ];
          let be = b[ orderField ];
          if ( ae == undefined && be == undefined ) return 0;
          if ( ae == undefined && be != undefined ) return orderType ? 1 : -1;
          if ( ae != undefined && be == undefined ) return orderType ? -1 : 1;
          if ( ae == be ) return 0;
          if(typeof ae == "string"){
            return orderType ? (ae.toString().toLowerCase() > be.toString().toLowerCase() ? -1 : 1) : (be.toString().toLowerCase() > ae.toString().toLowerCase() ? -1 : 1);
          }else{
            return orderType ? (ae > be ? -1 : 1) : (be > ae ? -1 : 1);
          }

      } );
      return array;
    }
}
