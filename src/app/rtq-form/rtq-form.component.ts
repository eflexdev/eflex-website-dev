import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rtq-form',
  templateUrl: './rtq-form.component.html',
  styleUrls: ['./rtq-form.component.css']
})
export class RtqFormComponent implements OnInit {

  questions: any = [];
  constructor() {
    this.questions = [
      {
        "question": 'Do I Have to Provide Small Business Health Insurance',
        "question_id": 1,
        "options": [{
          'name':'option 1',
          'id': 1
          },{
            'name':'option 2',
            'id': 2
          },{
            'name':'option 3',
            'id': 3
          },
          {
            'name':'option 4',
            'id': 4
          }],
        "answer":[]
      },
        {
          "question": 'Secound Question',
          "question_id": 2,
          "options": [{
            'name':'option 1',
            'id': 1
          },{
            'name':'option 2',
            'id': 2
          },{
            'name':'option 3',
            'id': 3
          },
          {
            'name':'option 4',
            'id': 4
          }],
          "answer":[]
        },
        {
          "question": 'Third Question',
          "question_id": 3,
          "options": [{
            'name':'option 1',
            'id': 1
          },{
            'name':'option 2',
            'id': 2
          },{
            'name':'option 3',
            'id': 3
          },
          {
            'name':'option 4',
            'id': 4
          }],
         "answer":[]
      }]
  }

  ngOnInit() {
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev,qid, oid) {
    ev.dataTransfer.setData("text", ev.target.id);
    console.log(oid);
  }

  drop(ev,pvarQid) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    console.log( document.getElementById(data).innerHTML);
    ev.target.appendChild(document.getElementById(data));
    let content = document.getElementById(data).innerHTML;
    let myobj= {};
    myobj['name'] = content;
    this.questions.forEach(obj => {
      if(obj['question_id'] == pvarQid ){
        if(obj['answer'] != ''){
          var index = obj['answer'].findIndex(x => x['name']== content);
          if (index === -1){
               obj['answer'].push(myobj);
          }
        }else{
           obj['answer'].push(myobj);
        }

      }
    });

    console.log(this.questions);
  }
}
