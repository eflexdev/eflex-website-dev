import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RtqFormComponent } from './rtq-form.component';

describe('RtqFormComponent', () => {
  let component: RtqFormComponent;
  let fixture: ComponentFixture<RtqFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RtqFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RtqFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
