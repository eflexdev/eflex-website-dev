import { Injectable, Output,EventEmitter } from '@angular/core';
import {environment} from '../environments/environment.prod'
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable,Subject } from 'rxjs';
declare var Noty: any;
import {CompanyService} from './company.service'
@Injectable()
@Injectable()
export class BrokerService {

  serviceUrl:string;
  fileUploadUrl: string = '';
  public headers; // = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  authToken: any;
  constructor( private http : Http, private companyService: CompanyService ) {
     this.serviceUrl = environment.serviceUrl;
     this.fileUploadUrl = environment.brokerFileUplaodUrl;
  }

  uploadFile(formData){
     return this.http.post(this.fileUploadUrl, formData)
     .map((response: Response) => {
         const data = response;
         return data;
       }).catch(
       (error: Error) => {
         return Observable.throw(error);
       });
  }
  registerBroker(inputData){
    let headers = new Headers({'Content-Type': 'application/json'})
    return this.http.post(this.serviceUrl+'/RegisterBroker',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addEflexUserPAPDetails(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    //let headers = new Headers({'Content-Type': 'application/json'})
    return this.http.post(this.serviceUrl+'/AddEflexUserPAPDetails',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  editEflexUserPAPDetails(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    //let headers = new Headers({'Content-Type': 'application/json'})
    return this.http.post(this.serviceUrl+'/EditEflexUserPAPDetails',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  brokerLogin(inputData){
    // let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    let headers = new Headers({'Content-Type': 'application/json'})
    return this.http.post(this.serviceUrl+'/BrokerLogin',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }


  listOfCompanyBrokers(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/ListOfCompanyBrokers',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addBrokerCompany(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/AddBrokerCompany',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listOfBrokerCompanies(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/ListOfBrokerCompanies',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  editBroker(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/EditBroker',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addBroker(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/AddBroker',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  getBrokerProfile(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/GetBrokerProfile',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listOfChildBrokers(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/ListOfChildBrokers',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  getBrokerFeeSetByEflex(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/GetBrokerFeeSetByEflex',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  deleteBroker(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/DeleteBroker',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  deleteCompany(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/DeleteCompany',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  changePasswordEflexUser(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/ChangePasswordEflexUser',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  updateBrokerCommissionCharges(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/UpdateBrokerCommissionCharges',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  getBrokerCompanyDetails(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')});
    return this.http.post(this.serviceUrl+'/GetBrokerCompanyDetails',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  addCompanyPlan(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddCompanyPlan',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  addEditEmployess(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddEditEmployess',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  getBrokerDashboardData(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetBrokerDashboardData',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  getAllBrokerTransactions(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetAllBrokerTransactions',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  forgotBrokerPassword(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ForgotBrokerPassword',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }


  saveBrokerReferral(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/SaveBrokerReferral',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  savePreferredSlots(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/SavePreferredSlots',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listOfBrokerReferrals(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/listOfBrokerReferrals',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  getPreferredSlotsById(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetPreferredSlotsById',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  updatePreferredSlots(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/UpdatePreferredSlots',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }








}
