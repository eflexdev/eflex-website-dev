import { Injectable, Output,EventEmitter } from '@angular/core';
import {environment} from '../environments/environment.prod'
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable,Subject } from 'rxjs';
declare var Noty: any;
import {CompanyService} from './company.service'
@Injectable()
export class MessagesService {

  serviceUrl:string;
  public headers; // = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  authToken: any;
  constructor( private http : Http, private companyService: CompanyService ) {
     this.serviceUrl = environment.serviceUrl;
     const ab = this.companyService.getUserToken()
     .subscribe((response: Response) =>{
       this.authToken = response; //sessionStorage.getItem('UserToken')
      });
     this.headers = new Headers({'Content-Type': 'application/json','TokenHeader': this.authToken})

  }

  listAllCompanyUsers(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListAllCompanyUsers',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listAllConversationMessages(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListAllConversationMessages',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listAllConversationMessagesAjax(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListAllConversationMessagesAjax',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  sendMessage(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/SendMessage',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listAllAdminUserForMsg(){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.get(this.serviceUrl+'/ListAllAdminUserForMsg',{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  listConversationDetailsWithUser(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListConversationDetailsWithUser',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  getConvBetweenTwoUsers(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetConvBetweenTwoUsers',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
}
