import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrokerPortalComponent } from '../broker-portal/broker-portal.component';
import { DashboardComponent } from '../broker-portal/dashboard/dashboard.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { BrokerListComponent } from './broker-list/broker-list.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { SettingsComponent } from './settings/settings.component';
import { PAPDetailComponent } from './pap-detail/pap-detail.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { AddBrokerComponent } from './add-broker/add-broker.component';
import { MyReferralComponent } from './my-referral/my-referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';

const routes: Routes = [{
  path: 'broker-portal', component:BrokerPortalComponent,
  children: [
    {path: 'dashboard',  component: DashboardComponent },
    {path: 'company-list',  component: CompanyListComponent },
    {path: 'broker-list',  component: BrokerListComponent },
    {path: 'add-broker',  component: AddBrokerComponent },
    {path: 'add-company',  component: AddCompanyComponent },
    {path: 'settings',  component: SettingsComponent },
    {path: 'pap-detail',  component: PAPDetailComponent },
    {path: 'transactions',  component: TransactionsComponent },
    {path: 'my-referral',  component: MyReferralComponent },
    {path: 'add-referral',  component: AddReferralComponent },
    {path: '',   redirectTo: '/dashboard',   pathMatch: 'full'}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrokerPortalRoutingModule { }
