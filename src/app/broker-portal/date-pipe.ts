import {Pipe,PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  'name':'mydate'
})
export class MyDate implements PipeTransform{
  transform(value){
    var s=value;
    //value=s.replace("AM"," AM");
    var AMPM = value.slice(-2);
    value=s.replace(AMPM," "+AMPM);
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    let d = new Date(value+" UTC");
    d.toLocaleString().replace(/GMT.*/g,"");
    value = datePipe.transform(d, 'MMM d, y'); 
    return value;
  }
}
@Pipe({
  'name':'datetime'
})
export class DateTime implements PipeTransform{
  transform(value){
    if(value){
      var s= value;
      var AMPM = value.slice(-2);
      value=s.replace(AMPM," "+AMPM)
       var datePipe = new DatePipe("en-US");
      // //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
      // value = datePipe.transform(new Date(value).toDateString(), 'yMMMdjms');
      let d = new Date(value+" UTC");
      d.toLocaleString().replace(/GMT.*/g,"");
      value = datePipe.transform(d, 'MMM d, y hh:mm a');
     return value;
    }
  }
}
