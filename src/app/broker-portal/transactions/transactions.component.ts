import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service'
import {Router} from "@angular/router"
@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  transactionsData: any=[];
  constructor(
     private service: CompanyService,
     private router : Router,
     private brokerService: BrokerService) { }

  ngOnInit() {
    let _input = {
    	"ChunckSize":1000,
    	"ChunckStart":-1,
    	"LoggedInUserId":sessionStorage.getItem('loginUserId'),
    	"SearchString":''
    }
    this.brokerService.getAllBrokerTransactions(_input)
    .subscribe((response:Response)=>{
      if(response['Status'] =="Success"){
        this.transactionsData = response['GetAllBrokerTransactions'];
      }else{
        this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
       this.service.alertMessage(error['Message'],"error");
    });
  }

}
