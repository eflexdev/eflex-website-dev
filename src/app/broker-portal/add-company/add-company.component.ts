import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { Router }              from '@angular/router';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service';
import {environment} from '../../../environments/environment.prod';

import {DatePipe} from "@angular/common";
import {IMyDpOptions} from 'mydatepicker';
import { FormData }                   from '../../register/data/formData.model';
import { FormDataService }            from '../../register/data/formData.service';
@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {
  isLoading: boolean= false;
  provinceData: any = [];
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  companyCode: string;
  companyPlanType: any = 1;


  //
  address: string = '';
  city: string = '';
  companyName: string = '';
  fname: string = '';
  lname: string = '';
  contact: string = '';
  email: string = '';
  postalCode: string = '';
  province: string = '';
  username: string = '';
  charges: any = '';
  brokerMaxFee :any;
  companyId: any = -1;
  hostUrl: string = '';
  serviceUrl: string = '';

   constructor(
     private service : CompanyService,
     private brokerService: BrokerService,
     private router: Router,
     private fb:FormBuilder,
     private ref:ChangeDetectorRef,
   ){
     this.hostUrl = environment.hostUrl;
     this.serviceUrl = environment.serviceUrl;
     this.companyPlanType = 1;
     this.brokerMaxFee = sessionStorage.getItem('brokerMaxFee');
     this.charges = sessionStorage.getItem('defaultCommission');
     this.ClassName=[{"id":1,"name": "Class A"},{"id":2,"name": "Class B"}, {"id":3,"name": "Class C"}, {"id":4,"name": "Class D"}, {"id":5,"name": "Class E"}, {"id":6,"name": "Class F"}, {"id":7,"name": "Class G"}, {"id":8,"name": "Class H"}, {"id":9,"name": "Class I"}, {"id":10,"name": "Class J"}, {"id":11,"name": "Class K"}]

     // this.employeeForm = this.fb.group({
     //       "ID":[''],
     //       "Employees": this.fb.array([ this.createEmployee() ]),
     //       "Admin": this.fb.array([])
     // });
     //this.getCompanyDetail();
    }

    step:number=1;
    goToPrevStep(pvarChoice){
        if(pvarChoice > 1){
          this.goToStep(pvarChoice-1);
        }
    }
    goToNextStep(pvarChoice){
          this.goToStep(pvarChoice+1);
    }
    goToStep(pvarChoice){
      if(pvarChoice == 4){
        this.planForm = this.fb.group({
              "ID":[''],
              "Plans":  this.fb.array([ ]) // this.createPlanItem(1)
        });
        this.planData = [];
        this.getCompanyDetail();
      }
      if(pvarChoice == 5){
        this.empData = [];
        //this.getCompanyDetail();
        this.getClasses();
        this.employeeForm = this.fb.group({
              "ID":[''],
              "Employees": this.fb.array([ this.createEmployee() ]),
              "Admin": this.fb.array([])
        });
      }
      if(pvarChoice == 6){
        this.getPaymentOverview();
      }
      this.step=pvarChoice;
    }

    ngOnInit() {
      //alert(sessionStorage.getItem("companyId"));
      this.companyId = sessionStorage.getItem("companyId") != null ? sessionStorage.getItem("companyId") : -1
      this.service.populateProvince()
      .subscribe((response:Response)=>{
        if(response['Status'] == 'Success'){
          this.provinceData = response['ProvinceList'];
        }else{}
      });
      if(sessionStorage.getItem('companyId') == null){
          this.service.getRandomCode()
            .subscribe((res:Response)=>{
              if(res['Status'] =="Success"){
                this.companyCode = res['RandomCode'];
              }else{
                this.service.alertMessage("Some thing went worng","error","0")
              }
            });
        }
    }

    submitDetail(form){
      if(form.controls.email.value){
          if(!form.controls.email.valid){
            this.service.alertMessage("Please enter a valid email address","error");
            return false;
          }
      }
      if(form.controls.contact.value.length < 10){
        this.service.alertMessage("Please enter a valid 10-digit contact number","error");
        return false;
      }

      if(form.valid){
      let _input = {
          "Address1":form.controls.address.value,
          "Address2":"",
          "BrokerId":sessionStorage.getItem('brokerId'),
          "City":form.controls.city.value,
          "CompanyId":  this.companyId,
          "Code": this.companyCode,
          "CompanyName":form.controls.companyName.value,
          "Country":"",
          "FirstName":form.controls.fname.value,
          "HostUrl": this.hostUrl,//'https://eflex.app/EflexServices/',
          "LastName":form.controls.lname.value,
          "LoggedInUserId":sessionStorage.getItem('loginUserId'),
          "OwnerContactNo": form.controls.contact.value,
          "OwnerEmail":form.controls.email.value,
          "PostalCode":form.controls.postalCode.value,
          "ProvinceId":form.controls.province.value,
          "ServiceUrl": this.serviceUrl, //'https://eflex.app/EflexServices/EflexService',
          "UserName":this.companyCode+"_"+form.controls.username.value,
          "CommissionCharges": form.controls.charges.value
        }
       this.isLoading = true;
       this.brokerService.addBrokerCompany(_input)
       .subscribe((response:Response)=>{
         if(response['Status'] =="Success"){
           this.charges =  form.controls.charges.value;
           //this.goToNextStep(this.step);
           //this.service.alertMessage("Company successfully added","success");
           sessionStorage.setItem("companyId", response['CompanyId']);
           this.companyId = response['CompanyId'];
           sessionStorage.setItem("stage", response['StagesCovered']);
           sessionStorage.setItem("DbCon",response['DbCon']);
            this.savePlanType();
         }else{
             this.isLoading = false;
             this.service.alertMessage(response['Message'],'error');
         }
       })
      }
      else{
        this.service.alertMessage("Please fill all the required fields","error");
        return false;
      }
    }

    checkAmount(){
      if(parseFloat(this.charges) > parseFloat(this.brokerMaxFee)){
        this.charges = "";
        this.service.alertMessage("You may charge a maximum of  $"+this.brokerMaxFee,"error");

      }
    }

  //------------------organization
  businessType: string = '';
  workType: string = '';
  savePlanType() {
    let businessType = this.businessType;
    const _input={
      "CompanyId":sessionStorage.getItem('companyId'),
      "IsCorporation": businessType =='Corporation' ? true : false,
      "PlanTypeId": this.workType == 'Group' ? 2 : 1
    }
    this.isLoading = true;
    this.service.updateCompanyPlanType(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        // this.planType = this.workType;
        // sessionStorage.setItem("PlanTypeId",this.workType);
        this.isLoading = false;
        this.service.alertMessage("Company successfully added and confirmation email has been send to company owner","success");
        this.router.navigate(['./broker-portal/company-list']);
        //this.goToNextStep(this.step);
      }else{
        this.isLoading = false;
        this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      this.isLoading = false;
      this.service.alertMessage(error,'error','')
    })
}

  ////////////----------------------plan-------
  ClassName:any= [];
  title = 'Plan setup';
  planForm: FormGroup;
  //Plans: any = [];

  RSTax:number = 0
  PPTax:number= 0;
  HST: number =0;
  AdminFeePct: number =0;
  GST: number =0;

  classes: any='';
  myIndex:any = 0;
  isFormValid: any = false;
  planType: any;
  loading: true;
  planData : any=[];
  myOptions={};

  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     //disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };

  getCompanyDetail(){
    if(sessionStorage.getItem("companyId") != null){
      this.planData = [];
      const _input={
        "Id": sessionStorage.getItem("companyId"),
        "LoggedInUserId": sessionStorage.getItem('loginUserId')
      };
      this.brokerService.getBrokerCompanyDetails(_input)
      .subscribe((res:Response)=>{
        console.log(res);
        if(res['Status'] == "Success"){
            sessionStorage.setItem("DbCon",res['CompanyBasicaData']['DbCon']);
            sessionStorage.setItem("PlanTypeId",res['CompanyBasicaData']['PlanTypeId']);
            this.service.setCompnayName(true, res['CompanyBasicaData']['CompanyName']);

            //BASCI Information
            let basicData = res['CompanyBasicaData'];
            this.companyCode = basicData['Code'];
            this.planType = basicData['PlanTypeId'] == 2 ? 'Group': 'Personal'; //basicData['PlanTypeId'];
            this.address = basicData['Address1'];
            this.city = basicData['City'];
            this.companyName = basicData['CompanyName'];
            this.fname = basicData['FirstName'];
            this.lname = basicData['LastName'];
            this.contact = basicData['OwnerContactNo'];
            this.email = basicData['OwnerEmail'];
            this.postalCode = basicData['PostalCode'];
            this.province = basicData['ProvinceId'];
            let uname= basicData['UserName'].split('_');
            this.username = uname['1'];

            if(basicData['IsCorporation'] == true){
              this.businessType = "Corporation";
            }else{
              this.businessType = "Sole Propreitorship";
            }
            this.workType = basicData['PlanTypeId'] == 2 ? 'Group': 'Personal';

            let taxData = res['TaxDetails'];
            this.PPTax = taxData['PPTax'] != 0 ? taxData['PPTax'] : 0;
            this.RSTax = taxData['RSTax'] != 0 ? taxData['RSTax'] : 0;

             // if(res['CompanyReferral']['ReferralTypeId'] == 1){
             //    let a = taxData['BrokerMinFeePct'] + taxData['AdminMinFeePct'];
             //    this.AdminFeePct = a != 0 ? a : 0;
             // }else{
             //    this.AdminFeePct = taxData['AdminMaxFeePct'] != 0 ? taxData['AdminMaxFeePct'] : 0;
             // }
            this.AdminFeePct = this.charges != 0 ? parseFloat(this.charges) +  parseFloat(taxData['AdminMinFeePct']) : + taxData['AdminMinFeePct'];

            this.GST = taxData['GST'] != 0 ? taxData['GST'] : 0;
            this.HST = taxData['HST'] != 0 ? taxData['HST'] : 0;
            if(this.step == 4){
              this.planData = res['PlanDetails'];
              if(this.planData != null){

                  this.classes = this.planData.length != 0 ?  this.planData.length : '';
                  if(this.planData.length > 0){
                    this.planData.forEach((lineItem, index) => {
                      //let  netTotal = (parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit ? lineItem.RecurringDeposit : 0));
                      let TotatFeeAndTaxes = parseFloat(lineItem.InitialDeposit) +  parseFloat(lineItem.RecurringDeposit)+ parseFloat(lineItem.GrossTotalInitial) + parseFloat(lineItem.GrossTotalMonthly);
                      let lineItemObj = {
                        "Name": lineItem.Name,
                        "AdministrationInitialFee":lineItem.AdministrationInitialFee,
                        "AdministrationMonthlyFee":lineItem.AdministrationMonthlyFee,
                        "CarryForwardPeriod":lineItem.CarryForwardPeriod,
                        "ContributionSchedule":lineItem.ContributionSchedule,
                        "GrossTotalInitial":lineItem.GrossTotalInitial,
                        "GrossTotalMonthly":lineItem.GrossTotalMonthly,
                        "HSTInitial":lineItem.HSTInitial,
                        "HSTMonthly":lineItem.HSTMonthly,
                        "InitialDeposit":lineItem.InitialDeposit,
                        "NetDeposit":lineItem.NetDeposit,
                        "PlanDesign":lineItem.PlanDesign,
                        "PlanStartDt":this.transformDate(lineItem.PlanStartDt),
                        "PremiumTaxInitial":lineItem.PremiumTaxInitial,
                        "PremiumTaxMonthly":lineItem.PremiumTaxMonthly,
                        "RecurringDeposit":lineItem.RecurringDeposit,
                        "RetailSalesTaxInitial":lineItem.RetailSalesTaxInitial,
                        "RetailSalesTaxMonthly":lineItem.RetailSalesTaxMonthly,
                        "RollOver":lineItem.RollOver,
                        "Termination":lineItem.Termination,
                        "TempPlanId" : lineItem.PlanId,
                        "PlanId":lineItem.PlanId,
                        "TotatFeeAndTaxes": TotatFeeAndTaxes
                      }
                      const control = <FormArray>this.planForm.controls['Plans'];
                      control.push(this.createPlanItem(1));
                      control.at(index).patchValue(lineItemObj);
                      console.log(lineItemObj);
                   });
                 }else{
                   if(res['CompanyBasicaData']['PlanTypeId'] == 1){
                     this.classes = 1;
                     this.addClasses();
                   }
                 }
              }
            }
            if(this.step == 5 ){
              this.empData =  res['EmployeesData'];
              this.adminData =  res['AdminsData'];
              if(this.empData != null){
                  if(this.empData.length > 0){
                    this.empData.forEach((lineItem, index) => {
                       // Create the item obj
                       let lineItemObj = {
                         "CompanyPlanId":lineItem.CompanyPlanId,
                         "ContactNo":'',
                          "CompanyUserId":-1,//lineItem.CompanyUserId,
                          "Email":lineItem.Email,
                          "FName":lineItem.FName,
                          "LName":lineItem.LName,
                          "UserTypeId":lineItem.UserTypeId,
                       }
                       const control = <FormArray>this.employeeForm.controls['Employees'];
                       control.push(this.createEmployee());
                       control.at(index).patchValue(lineItemObj);
                    })
                  }else{
                    if(res['CompanyBasicaData']['PlanTypeId'] == 1){
                      //this.deleteEmployee(0);
                    }
                    this.ownerLastName = res['CompanyBasicaData']['LastName'];
                    this.ownerFirstName = res['CompanyBasicaData']['FirstName'];
                    this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
                    let lineItemObj = {
                      "CompanyPlanId":'',
                      "ContactNo":'',
                       "CompanyUserId":-1,
                       "Email":this.ownerEmail,
                       "FName":this.ownerFirstName,
                       "LName":this.ownerLastName,
                       "UserTypeId":3,
                    }
                    const control = <FormArray>this.employeeForm.controls['Employees'];
                    //control.push(this.createEmployee());
                    control.at(0).patchValue(lineItemObj);
                  }
                }else{
                  this.ownerLastName = res['CompanyBasicaData']['LastName'];
                  this.ownerFirstName = res['CompanyBasicaData']['FirstName'];
                  this.ownerEmail = res['CompanyBasicaData']['OwnerEmail'];
                  let lineItemObj = {
                    "CompanyPlanId":'',
                    "ContactNo":'',
                     "CompanyUserId":-1,
                     "Email":this.ownerEmail,
                     "FName":this.ownerEmail,
                     "LName":this.ownerEmail,
                     "UserTypeId":3,
                  }
                  const control = <FormArray>this.employeeForm.controls['Employees'];
                  control.push(this.createEmployee());
                  control.at(0).patchValue(lineItemObj);
                }
            }
        }else{
          this.service.alertMessage(res['Message'],'error');
        }
      })
    }
  }

  createPlanItem(i): FormGroup {
      if(this.step == 4){
        let object = {
            "Name": this.returnClassName(i),
            "AdministrationInitialFee":[0],// InitialDeposit/10%
            "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
            "CarryForwardPeriod":['Indefinite Rollover to the benefit of the account holder'], //12 months
            "ContributionSchedule":[1], //Monthly 1, Yearly 2
            "GrossTotalInitial":[0], //over all initial total
            "GrossTotalMonthly":[0], // over all monthly total
            "HSTInitial":[0],// InitialDeposit/13%
            "HSTMonthly":[0], //RecurringDeposit/ 13%
            "InitialDeposit":['', [Validators.required, Validators.minLength(1)]],// InitialDeposit
            "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
            "PlanDesign":new FormControl(2),
            "PlanStartDt":['', [Validators.required]],
            "PremiumTaxInitial":[0], //InitialDeposit/ 2%
            "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
            "RecurringDeposit": new FormControl('', [Validators.required,Validators.minLength(1)]),  //['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
            "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
            "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
            "RollOver":[4],
            "Termination":[6],
            "TempPlanId" : i,
            "PlanId": -1,
            "TotatFeeAndTaxes":[0]
        };
        return this.fb.group(object);
      }
  }

  returnClassName(i){
    let name='';
      this.ClassName.forEach(function(x, index) {
          if (x.id === i) {
            name = x.name
          }
      });
      return name;
  }

  addClasses(){
    //remove all plans from arry
    //alert(this.classes)
    if(this.classes == ''){
      this.planForm.value['Plans'] = [];
      const arr = <FormArray>this.planForm.controls.Plans;
      arr.controls = [];
      this. service.alertMessage("Please select atleast 1 class","error")
      return false;
    }
    const arr = <FormArray>this.planForm.controls.Plans;
    arr.controls = [];
    //end
    if(this.classes !=1){
      this.planForm.value['Plans'] = [];
      let len = this.classes;
      for (var i = 0; i < len; i++) {
        let Plans = this.planForm.get('Plans') as FormArray;
        Plans.push(this.createPlanItem(i+1));
      }
    }else{
      let Plans = this.planForm.get('Plans') as FormArray;
      Plans.push(this.createPlanItem(1));
    //  this.pager.count = this.planForm.value['Plans'].length;
    //  this.pager.count =this.pager.index + 1;
      //this.goTo(this.pager.index + 1);
    }
  }

  setCarryForward(event, obj){
    this.planForm.get('Plans')['controls'].forEach((plan)=>{
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['CarryForwardPeriod'].patchValue('Indefinite Rollover to the benefit of the account holder');
      }
    })
  }

  //calculate taxes
  RecurringDeposit(obj){
    if(obj.value.RecurringDeposit !=''){
      this.planForm.get('Plans')['controls'].forEach((plan)=>{
        if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
          plan.controls['AdministrationMonthlyFee'].patchValue((obj.value.RecurringDeposit * this.AdminFeePct)/100);

          let pptamt = parseFloat(obj.value.RecurringDeposit) + parseFloat(plan.controls['AdministrationMonthlyFee'].value);
          plan.controls['PremiumTaxMonthly'].patchValue(( pptamt * this.PPTax)/100);

          if(this.HST != 0){
            //plan.controls['HSTMonthly'].patchValue((obj.value.RecurringDeposit *  this.HST)/100);
            plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.HST)/100);
          }else{
             plan.controls['HSTMonthly'].patchValue((obj.value.AdministrationMonthlyFee *  this.GST)/100);
          }

          if(this.planType == "Group"){
            plan.controls['RetailSalesTaxMonthly'].patchValue((obj.value.RecurringDeposit * this.RSTax)/100);
          }else{
            plan.controls['RetailSalesTaxMonthly'].patchValue(0)
          }

          let GrossTotalMonthly = (plan.controls['AdministrationMonthlyFee'].value + plan.controls['HSTMonthly'].value + plan.controls['PremiumTaxMonthly'].value + plan.controls['RetailSalesTaxMonthly'].value )
          // plan.controls['GrossTotalInitial'].patchValue();
           plan.controls['GrossTotalMonthly'].patchValue(GrossTotalMonthly);

           let netTotal = (parseFloat(obj.value.RecurringDeposit) +  parseFloat(plan.controls['InitialDeposit'].value ? plan.controls['InitialDeposit'].value : 0));
            plan.controls['NetDeposit'].patchValue(netTotal);
            plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);
          }
      })
    }else{
      this.service.alertMessage("Enter contribution amount for monthly or annual schedule","error");
      return false;
    }
  }

  InitialDeposit(obj){
    if(obj.value.InitialDeposit !=''){
    this.planForm.get('Plans')['controls'].forEach((plan)=>{
      console.log(plan.value);
      if(plan.value['TempPlanId'] ===   obj.value.TempPlanId){
        plan.controls['AdministrationInitialFee'].patchValue((obj.value.InitialDeposit * 10)/100);
        //if hst 0 then apply GST
        if(this.HST != 0){
        //  plan.controls['HSTInitial'].patchValue((obj.value.InitialDeposit * this.HST)/100);
        plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.HST)/100);
        }else{
          plan.controls['HSTInitial'].patchValue((obj.value.AdministrationInitialFee *  this.GST)/100);
        }

      //  let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        //plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        let pptamt = parseFloat(obj.value.InitialDeposit) + parseFloat(plan.controls['AdministrationInitialFee'].value);
        plan.controls['PremiumTaxInitial'].patchValue(( pptamt * this.PPTax)/100);

        //if group then apply rst
        if(this.planType == "Group"){
          // plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);
          plan.controls['RetailSalesTaxInitial'].patchValue((obj.value.InitialDeposit * this.RSTax)/100);

        }else{
          plan.controls['RetailSalesTaxInitial'].patchValue(0);
        }

        let GrossTotalInitial = (plan.controls['AdministrationInitialFee'].value + plan.controls['HSTInitial'].value + plan.controls['PremiumTaxInitial'].value + plan.controls['RetailSalesTaxInitial'].value )
        plan.controls['GrossTotalInitial'].patchValue(GrossTotalInitial);
        let  netTotal = (parseFloat(obj.value.InitialDeposit) +  parseFloat(plan.controls['RecurringDeposit'].value ? plan.controls['RecurringDeposit'].value : 0));

        plan.controls['NetDeposit'].patchValue(netTotal)

        plan.controls['TotatFeeAndTaxes'].patchValue(plan.controls['GrossTotalInitial'].value + plan.controls['GrossTotalMonthly'].value + netTotal);

      }
    })
  }else{
    this.service.alertMessage("Please enter initial deposit amount","error");
    return false;
  }
  }

  classA: any;
  setPlanSameAsA(e, obj){
    if(e.target.checked == true){
      this.planForm.get('Plans')['controls'].forEach((plan)=>{
        if(plan.value['Name'] == 'Class A'){
          this.classA =  plan.value;
        }
        if(plan.value['TempPlanId'] == obj.value.TempPlanId){
          console.log(this.classA);

          plan.controls['AdministrationInitialFee'].patchValue(this.classA['AdministrationInitialFee']);
          plan.controls['AdministrationMonthlyFee'].patchValue(this.classA['AdministrationMonthlyFee']);
          plan.controls['RecurringDeposit'].patchValue(this.classA['RecurringDeposit']);

          plan.controls['GrossTotalInitial'].patchValue(this.classA['GrossTotalInitial']);
          plan.controls['GrossTotalMonthly'].patchValue(this.classA['GrossTotalMonthly']);
          plan.controls['HSTInitial'].patchValue(this.classA['HSTInitial']);
          plan.controls['HSTMonthly'].patchValue(this.classA['HSTMonthly']);
          plan.controls['InitialDeposit'].patchValue(this.classA['InitialDeposit']);
          plan.controls['NetDeposit'].patchValue(this.classA['NetDeposit']);

          if(this.classA['PlanDesign'] != plan.value['PlanDesign']){
              plan.controls['PlanDesign'].patchValue(this.classA['PlanDesign']);
          }
          if(this.classA['ContributionSchedule'] != plan.value['ContributionSchedule']){
                plan.controls['ContributionSchedule'].patchValue(this.classA['ContributionSchedule']);
          }

          if(this.classA['RollOver'] != plan.value['RollOver']){
                plan.controls['RollOver'].patchValue(this.classA['RollOver']);
          }
          if(this.classA['CarryForwardPeriod'] != plan.value['CarryForwardPeriod']){
                plan.controls['CarryForwardPeriod'].patchValue(this.classA['CarryForwardPeriod']);
          }
          if(this.classA['Termination'] != plan.value['Termination']){
                plan.controls['Termination'].patchValue(this.classA['Termination']);
          }
          plan.controls['PlanStartDt'].patchValue(this.classA['PlanStartDt']);
          plan.controls['PremiumTaxInitial'].patchValue(this.classA['PremiumTaxInitial']);
          plan.controls['PremiumTaxMonthly'].patchValue(this.classA['PremiumTaxMonthly']);

          plan.controls['RetailSalesTaxInitial'].patchValue(this.classA['RetailSalesTaxInitial']);
          plan.controls['RetailSalesTaxMonthly'].patchValue(this.classA['RetailSalesTaxMonthly']);

          plan.controls['TotatFeeAndTaxes'].patchValue(this.classA['TotatFeeAndTaxes']);
        }
      })
    }
  }
  //Empty form group array
  getPlanDetail(data){
    if(data.length > 0) {
      this.planForm.controls['Plans'] = this.fb.array([]);
      this.setFormArryValues(data);
    }
  }

  setFormArryValues(data){
    let i=0;
    data.forEach((lineItem, index) => {
     // Create the item obj
     let lineItemObj = {
       "Name": lineItem.Name,
       "AdministrationInitialFee":lineItem.AdministrationInitialFee,
       "AdministrationMonthlyFee":lineItem.AdministrationMonthlyFee,
       "CarryForwardPeriod":lineItem.CarryForwardPeriod,
       "ContributionSchedule":lineItem.ContributionSchedule,
       "GrossTotalInitial":lineItem.GrossTotalInitial,
       "GrossTotalMonthly":lineItem.GrossTotalMonthly,
       "HSTInitial":lineItem.HSTInitial,
       "HSTMonthly":lineItem.HSTMonthly,
       "InitialDeposit":lineItem.InitialDeposit,
       "NetDeposit":lineItem.NetDeposit,
       "PlanDesign":lineItem.PlanDesign,
       "PlanStartDt":lineItem.PlanStartDt,
       "PremiumTaxInitial":lineItem.PremiumTaxInitial,
       "PremiumTaxMonthly":lineItem.PremiumTaxMonthly,
       "RecurringDeposit":lineItem.RecurringDeposit,
       "RetailSalesTaxInitial":lineItem.RetailSalesTaxInitial,
       "RetailSalesTaxMonthly":lineItem.RetailSalesTaxMonthly,
       "RollOver":lineItem.RollOver,
       "Termination":lineItem.Termination,
       "TempPlanId" : lineItem.PlanId,
       "PlanId":lineItem.PlanId
     }
     const control = <FormArray>this.planForm.controls['Plans'];
     control.push(this.createPlanItem(1));
     control.at(index).patchValue(lineItemObj);
   });
  }

  updatePlanItem(): FormGroup {
    return this.fb.group({
        "Name": [''],
        "AdministrationInitialFee":[0],// InitialDeposit/10%
        "AdministrationMonthlyFee":[0], // RecurringDeposit / 10%
        "CarryForwardPeriod":[''], //12 months
        "ContributionSchedule":[1], //Monthly 1, Yearly 2
        "GrossTotalInitial":[0], //over all initial total
        "GrossTotalMonthly":[0], // over all monthly total
        "HSTInitial":[0],// InitialDeposit/13%
        "HSTMonthly":[0], //RecurringDeposit/ 13%
        "InitialDeposit":['', [Validators.required, Validators.minLength(1)]],// InitialDeposit
        "NetDeposit":[0], // NetDeposit = RecurringDeposit+InitialDeposit
        "PlanDesign":[1],
        "PlanStartDt":['', [Validators.required]],
        "PremiumTaxInitial":[0], //InitialDeposit/ 2%
        "PremiumTaxMonthly":[0], // RecurringDeposit/ 2%
        "RecurringDeposit":['',[Validators.required, Validators.minLength(1)]], //Monthly or anullay depost
        "RetailSalesTaxInitial":[0], //InitialDeposit/ 8%
        "RetailSalesTaxMonthly":[0], //InitialDeposit/ 8%
        "RollOver":[3],
        "Termination":[5],
        "TempPlanId" : [1],
        "PlanId": -1,
        "TotatFeeAndTaxes":[0]
    });
  }

  saveDetail(planForm){
    if(this.classes == ''){
      this. service.alertMessage("Please select atleast 1 class","error")
      return false;
    }
    if (this.planForm.valid) {
       let typeId = sessionStorage.getItem("PlanTypeId") == 'Group' ? 2 : 1;
       this.planForm.value['Plans'].forEach((lineItem)=>{
         if(lineItem.PlanStartDt.formatted){
            lineItem['PlanStartDt']= lineItem.PlanStartDt.formatted;
         }else{
           if(typeof  lineItem.PlanStartDt === 'object'){
             let mm = parseInt(lineItem.PlanStartDt.date.month) < 10 ? '0'+lineItem.PlanStartDt.date.month: lineItem.PlanStartDt.date.month;
             let dd = parseInt(lineItem.PlanStartDt.date.day) < 10 ? '0'+lineItem.PlanStartDt.date.day: lineItem.PlanStartDt.date.day;
             lineItem['PlanStartDt']= mm+"/"+ dd +"/"+lineItem.PlanStartDt.date.year;
           }else{
             lineItem['PlanStartDt']= lineItem.PlanStartDt;
           }
         }
       });
       let _input ={
         "CompanyId": parseInt(sessionStorage.getItem("companyId")),
         "LoggedInUserId": 1, //sessionStorage.getItem('loginUserId') != null ? sessionStorage.getItem('loginUserId') : 1,
         "DbCon": sessionStorage.getItem("DbCon"),
         "PlanDetails":this.planForm.value['Plans'],
         "PlanTypeId":typeId
       }
       console.log(_input);
       this.isLoading = true;
       this.brokerService.addCompanyPlan(_input)
       .subscribe((res:Response)=>{
         if(res['Status'] == 'Success'){
           sessionStorage.setItem("stage",'3');
           this.service.alertMessage("Company plan details successfully saved",'error','2000');
            this.isLoading = false;
            this.goToNextStep(this.step);
         }else{
           this.isLoading = false;
           this.service.alertMessage(res['Message'],'error','')
         }
       },(error)=>{
          this.service.alertMessage(error,'error','');
          this.isLoading = false;
       })

     } else {
         this.service.alertMessage("Please enter all the required fields","error","")
     }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

 displayFieldCss(field: string) {
  return {
    'has-error': this.isFieldValid(field),
    'has-feedback': this.isFieldValid(field)
  };
}

 isFieldValid(field: string) {
    return !this.planForm.get(field).valid && this.planForm.get(field).touched;
 }
 validateAllFormFields(formGroup: FormGroup) {
   Object.keys(formGroup.controls).forEach(field => {
     console.log(field);
     const control = formGroup.get(field);
     if (control instanceof FormControl) {
       control.markAsTouched({ onlySelf: true });
     } else if (control instanceof FormGroup) {
       this.validateAllFormFields(control);
     }
   });
 }

 transformDateForFlatpickr(value){
   var datePipe = new DatePipe("en-US");
   value = datePipe.transform(value, 'yyyy-MM-dd');
   return value;
 }
 transformDate(value){
   var s=value;
   value=s.replace("AM"," AM")
   var datePipe = new DatePipe("en-US");
   //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
   value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
   let date = new Date(value);
    const cdate= {
         year: date.getFullYear(),
         month: date.getMonth() + 1,
         day: date.getDate()
     };
   let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
   return dt;
 }


///////////////////////---------------add plan member-----------

employeeForm: FormGroup;
classesData: any=[];
empData:any = [];
adminData:any = [];
ownerEmail: string = '';
ownerFirstName: string= '';
ownerLastName: string = '';
createEmployee(): FormGroup {
  return this.fb.group({
        "CompanyPlanId":[],
        "CompanyUserId":-1,
        "ContactNo":[''],
        "Email":['',[Validators.required, Validators.pattern(this.emailPattern)]],
        "FName":['',[Validators.required]],
        "LName":['',[Validators.required]],
        "UserTypeId":[3],
    })
}

addAdmin(e){
  if(e.target.checked == true){
    let a = this.employeeForm.get('Admin') as FormArray;
    a.push(this.createAdmin());
  }else{
    // const arr = <FormArray>this.employeeForm.controls.Admin;
    // arr.controls = [];
      this.employeeForm.controls['Admin'] = this.fb.array([]);
    // const control=<FormArray>this.employeeForm.controls['Employees'];
    // control.removeAt(index);
  }
}

createAdmin(): FormGroup {
  return this.fb.group({
      "CompanyUserId":-1,
      "ContactNo":[''],
      "Email":[this.email,[Validators.required, Validators.pattern(this.emailPattern)]],
      "FName":[this.fname,[Validators.required]],
      "LName":[this.lname,[Validators.required]],
      "UserTypeId":[2],
  })
}

getClasses(){
    if(sessionStorage.getItem("DbCon") == null){
      return false;
    }
  const _input = {
    "DbCon": sessionStorage.getItem("DbCon")
  }
  this.service.populateClasses(_input)
  .subscribe((res:Response)=>{
    if(res['Status'] == 'Success'){
      this.classesData = res['ClassList'];
      this.getCompanyDetail();
    }else{
      this.service.alertMessage(res['Message'],'error','')
    }
  },(error)=>{
    this.service.alertMessage(error,'error','')
  })
}

addMoreEmployee(){
  const control=<FormArray>this.employeeForm.controls['Employees'];
  control.push(this.createEmployee());
}
deleteEmployee(index:number){
    const control=<FormArray>this.employeeForm.controls['Employees'];
    control.removeAt(index);
}

submit() {
    //this.formData = this.formDataService.resetFormData();
    this.isFormValid = false;
}

AddEmployee(){
  console.log(this.employeeForm);
  if(this.employeeForm.valid){
    let _input ={
      "DbCon":sessionStorage.getItem("DbCon"),
      "LoggedInUserId": 1,//sessionStorage.getItem('loginUserId'),
	    "EmployeesList": this.employeeForm.value['Employees'],
      "AdminList": this.employeeForm.value['Admin']
    }
    console.log( JSON.stringify(_input, undefined, 2));
    //this.goToNextStep(this.step);
    this.isLoading = true;
    this.brokerService.addEditEmployess(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.service.alertMessage("Company employees successfully added",'error','2000');
        this.isLoading = false;
        this.router.navigate(['./broker-portal/company-list']);
        //this.goToNextStep(this.step);

      }else{
          this.service.alertMessage(res['Message'],'error','');
          this.isLoading = false;
      }
    },(error)=>{
      this.service.alertMessage(error,'error','');
      this.isLoading = false;

    })
  }
}

//-----------------------PAYMENT overview
paymentData: any = [];
loadingPayment: boolean;
NetGrossInitial: any;
NetGrossRecurring: any;
loginUserName: string='';
//paymentDate: any;
public paymentDate;

//d = new Date();
public myDatePickerOptionsPayment: IMyDpOptions = {
   dateFormat: 'mm/dd/yyyy',
   //dateFormat: 'yyyy/mm/dd',
   disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1},
   disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+2, day: this.d.getDate()+1}
};
getPaymentOverview() {
    if(sessionStorage.getItem("DbCon") != null){
    const _input = {
      "DbCon":sessionStorage.getItem("DbCon")
    }
    this.service.getPaymentOverview(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.NetGrossInitial = res['NetGrossInitial'];
        this.NetGrossRecurring = res['NetGrossRecurring'];
        this.paymentData = res['ClasswisePayments'];
      }
    });
  }else{
    this.router.navigate(['./broker-portal/company-list']);
  }
}

updatePaymenDate(){
  if(this.paymentDate == null){
    this.service.alertMessage("Select when you would like the amount pulled from your account","error");
    return false;
  }
  let date = '';
  if(typeof  this.paymentDate === 'object'){
    let mm = parseInt(this.paymentDate.date.month) < 10 ? '0'+this.paymentDate.date.month: this.paymentDate.date.month;
    let dd = parseInt(this.paymentDate.date.day) < 10 ? '0'+this.paymentDate.date.day: this.paymentDate.date.day;
     date = mm+"/"+ dd +"/"+this.paymentDate.date.year;
  }else{
     date= this.paymentDate;
  }

  if(sessionStorage.getItem("DbCon") != null){
    const _input = {
      "CompanyId": sessionStorage.getItem('companyId'),
      "DbCon":sessionStorage.getItem("DbCon"),
      "PaymentDate":date,
    };
    this.loadingPayment = true;
    console.log(_input);
    this.service.updatePaymentDate(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
         this.loadingPayment = false;
        this.service.alertMessage('Payment details successfully submitted',"success");
        //this.router.navigate(['/bank-detail']);
        this.router.navigate(['./broker-portal/company-list']);
      }
    });
  }else{
    this.router.navigate(['/login']);
  }

}

businessChange(event){
  console.log(event)
  this.workType = '';
}


}
