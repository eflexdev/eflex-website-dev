import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service';
import {DatePipe} from "@angular/common";
@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {

  companyData:any = [];
  isLoader:boolean;
  constructor(private service: CompanyService, private brokerService: BrokerService) {
    //  this.compnayPlanTypeId =  sessionStorage.getItem("compnayPlanTypeId");
  }

  ngOnInit() {
    sessionStorage.removeItem('companyId');
    if(sessionStorage.getItem('loginUserId') != null){
        this.getBrokerCompany();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

  getBrokerCompany(){
   const _input = {
      "ChunckSize":1000,
      "ChunckStart":-1,
      "LoggedInUserId":sessionStorage.getItem('loginUserId'),
      "SearchString":""
    }
    this.isLoader = true;
    this.brokerService.listOfBrokerCompanies(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.isLoader = false;
        this.companyData= res['ListOfBrokerCompanies'];
      }else{
          this.isLoader = false;
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
        this.isLoader = false;
        this.service.alertMessage(error,'error');
    })
  }
  trackByFn(index, item) {
    return index;
  }

  deleteCompany(obj){
    this.selectedIndex = obj;
    this.deleteConfirmation = true;
    this.name  = obj['Name'];
  }
  name: string;
  loading: boolean;
  selectedIndex: any;
  deleteConfirmation: boolean;

  yesDeleteCompany(){
    this.loading = true;
    console.log(this.selectedIndex);
    const _input = {
      "Id":this.selectedIndex['CompanyId'],
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    }
    console.log(_input);
    this.brokerService.deleteCompany(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.deleteConfirmation = false;
        this.loading = false;
        let index = this.companyData.lastIndexOf(this.selectedIndex);
        this.companyData.splice(index, 1);
        this.service.alertMessage("Company successfully deleted","error");
      }else{
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
      this.service.alertMessage(error,'error');
    })
  }

  transformDate(value){
    var s=value;
    value=s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    //value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
    // let date = new Date(value);
    //  const cdate= {
    //       year: date.getFullYear(),
    //       month: date.getMonth() + 1,
    //       day: date.getDate()
    //   };
    // let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
    return value;
  }

}
