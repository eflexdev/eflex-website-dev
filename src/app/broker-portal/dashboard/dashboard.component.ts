import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service'
import {Router} from "@angular/router"
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  chart: Chart;
  plansChart: any;
  isPersonal: any = '';

  figureData: any;
  otherBrokers: any = [];
  companyData: any = [];
  grapDetail: any = [];
  constructor(
    private service: CompanyService,
     private router : Router,
     private brokerService: BrokerService
   ) {
     this.isPersonal = sessionStorage.getItem("isPersonal");
     this.getFee();
   }

  ngOnInit() {
    let _input = {
      "UserId": sessionStorage.getItem('loginUserId')
    }
    this.brokerService.getBrokerDashboardData(_input)
    .subscribe((response:Response)=>{
      if(response['Status'] =="Success"){
        this.figureData = response['BrokerFiguresData'];
        this.otherBrokers = response['ChildBrokersDetails'];
        this.companyData = response['RecentRegCompany'];
        this.grapDetail = response['GetBrokerGraphDetails'];

        this.monthlyOverview();
      }else{
        if(response['IsLogOut'] ==  true){
          this.service.alertMessage("Your token has been expire","error");
          sessionStorage.clear();
          this.router.navigate(['/join-us']);
        }
        //this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
       this.service.alertMessage(error['Message'],"error");
    });

  }

  monthlyOverview(){
    this.plansChart = new Chart({
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          //text: 'Source: WorldClimate.com'
      },
      xAxis: [{
          categories: this.grapDetail['Months'],//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul'],
          crosshair: true
      }],
      yAxis: [{ // Primary yAxis
          labels: {
              format: '${value}',
              style: {
                  // color: 'rgb(245, 124, 0)'
              }
          },
          title: {
              text: 'Amount Receive',
              style: {
                  // color: 'rgb(245, 124, 0)'
              }
          }
      },
      { // Secondary yAxis
          title: {
              text: 'No of Registration',
              style: {
                  color: 'rgb(245, 124, 0)'
              }
          },
          opposite: true
      }
      ],
      tooltip: {
          shared: true
      },
      credits: {
         enabled: false
     },
      legend: {
          layout: 'vertical',
          align: 'left',
          x: 120,
          verticalAlign: 'top',
          y: 100,
          floating: true,
          backgroundColor: 'rgba(255,255,255,0.25)'//(Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
      },
      series: [{
          name: 'No of company',
          type: 'column',
          yAxis: 1,
          data:  this.grapDetail['NoOfCompanies']//[10, 15, 20,18, 25, 35, 45]
      }, {
          name: 'Commission Amount',
          type: 'spline',
          data: this.grapDetail['ComissionAmt']//[100, 250, 180,300, 150, 120, 250]
      }]
    });
  }

  getFee(){
    let _input ={
      "Id": sessionStorage.getItem('loginUserId')
    }
    this.brokerService.getBrokerFeeSetByEflex(_input)
    .subscribe((response:Response)=>{
        if(response['Status'] == "Success"){
          sessionStorage.setItem('brokerMaxFee', response['BrokerFee']);
          sessionStorage.setItem('defaultCommission',response['BrokerCommissionFee']);
        }else{
          this.service.alertMessage(response['Message'],'error');
        }
    })
  }
}
