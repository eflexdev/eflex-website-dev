import { Component, OnInit, ElementRef } from '@angular/core';
import {CompanyService} from '../../company.service';
import {DatePipe} from "@angular/common";
import {BrokerService} from '../../broker.service'
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
import {IMyDpOptions} from 'mydatepicker';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { Router }              from '@angular/router';
import {environment} from '../../../environments/environment.prod'
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  activeTab: any = 1;
  isPersonalAccount = sessionStorage.getItem("isPersonal");
  isLoadingProfile: boolean = false;
  loading: boolean = false;
  isLoading:boolean;
  //contact
  firstName:string='';
  lastName:string='';
  email: string = '';
  companyEmail: string = '';
  contact: string='';
  userName: string= '';
  provinceData:any;
  city: string = '';
  province : string = '';
  address : string = '';
  companyName: string='';
  postalCode:string='';
  socialInsurance: string='';
  businessNo: string='';
  DOB: any;
  GSTNo: string='';
  licenceFront:string='';
  licenceBack: string='';
  insurance:string='';
  //----------


  //changePwd
  cploading: boolean;
  oldPwd:string = ''
  newPwd: string= '';
  newCPwd: string= '';

  //PAP
  bankname:any;
  instId: any;
  transId: any;
  accountNumber: any;
  iagree:boolean;
  btnDisable: boolean= false;
  uploadingFile:boolean;
  fileName: string = '';
  hostUrl: string;
  modal:boolean;
  PAPDetailId: any = -1;
  borkers: any=[];
  referralData: any=[];

  //broker fee
  brokerMaxFee: any;
  myCharges: any = 0;

  // public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     //dateFormat: 'yyyy/mm/dd',
      editableDateField: false,
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };

  // data-ng-pattern="/^[a-zA-Z0-9\u00C0-\u017F-'.\s]+$/"
  constructor( private service: CompanyService, private router: Router, private el: ElementRef,
     private fb:FormBuilder, private brokerService : BrokerService) {
      this.hostUrl = environment.hostUrl;
   }

  ngOnInit() {
    this.service.populateProvince()
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
        this.provinceData = res['ProvinceList'];
        //this.getReferralSource();
        this.getBrokerProfileDetail()
      }else{
        //this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
      //this.service.alertMessage(error,'error','')
    })
  }

  getBrokerProfileDetail(){
      this.isLoading = true;
        const _input={
          "Id": sessionStorage.getItem('loginUserId'),
          "LoggedInUserId": sessionStorage.getItem('loginUserId')
        };
        this.brokerService.getBrokerProfile(_input)
        .subscribe((res:Response)=>{
          this.isLoading = false;
          let obj = res['GetBrokerProfileData'];
          this.companyName = obj['CompanyName'];
          this.address = obj['Address1'] == '' ? obj['CompanyAddress']: obj['Address1'];
          this.city = obj['City'] == '' ? obj['CompanyCity'] : obj['City'];
          this.province = obj['ProvinceId'];
          this.postalCode = obj['PostalCode'] == '' ? obj['CompanyPostalCode'] : obj['PostalCode'];
          this.firstName = obj['FirstName'];
          this.lastName = obj['LastName'];
          this.email = obj['Email'];
          this.companyEmail = obj['CompanyEmail'];
          this.contact = this.isPersonalAccount == 'true' ? obj['MobileNo'] : obj['CompanyContactNo'];
          this.userName = obj['UserName'];

          this.socialInsurance = obj['SocialInsuranceNo'];
          this.businessNo = obj['BusinessNo'];
          this.DOB = this.transformDate(obj['DOB']);
          this.GSTNo = obj['GSTNo'];

          this.licenceFront=obj['UploadLicenceFront'];
          this.licenceBack=obj['Licence'];
          this.insurance=obj['EOInsuranceNo'];

          //0-----------PAP
          var objPAP = obj['FinancialDetails'];
          this.bankname= this.textDecode(objPAP['BankName']);
          this.instId= this.textDecode(objPAP['InstitutionId']);
          this.transId= this.textDecode(objPAP['TransitId']);
          this.accountNumber= this.textDecode(objPAP['AccountNumber']);
          this.PAPDetailId = objPAP['UserPAPDetailsId'];
        })

  }

  submitDetail(form){
      if(form.valid){
        let formData = form.value;
        //formatted
        if(formData.DOB.formatted){
          formData.DOB = formData.DOB.formatted;
        }else{
          if(typeof  formData.DOB === 'object'){
            let mm = parseInt(formData.DOB.date.month) < 10 ? '0'+formData.DOB.date.month:formData.DOB.date.month;
            let dd = parseInt(formData.DOB.date.day) < 10 ? '0'+formData.DOB.date.day: formData.DOB.date.day;
            formData.DOB= mm+"/"+ dd +"/"+formData.DOB.date.year;
          }else{
            formData.DOB = formData.DOB;
          }
        }
        let _input = {
          "Address1":formData.address,
          "Address2":"",
          "BusinessNo":formData.businessNo,
          "City":formData.city,
          "CompanyAddress":this.isPersonalAccount == 'false' ? formData.address : '',
          "CompanyCity": this.isPersonalAccount == 'false' ? formData.city :'',
          "CompanyContactNo": this.isPersonalAccount == 'false' ? formData.contact : '',
          "CompanyContactPerson":'',
          "CompanyEmail":this.isPersonalAccount == 'false' ? formData.email : '',
          "CompanyName": this.isPersonalAccount == 'false' ? formData.companyName: '',
          "CompanyPostalCode":this.isPersonalAccount == 'false' ? formData.postalCode : '',
          "CompanyProvinceId":this.isPersonalAccount == 'false' ? formData.province : -1,
          "Country":"",
          "DOB":formData.DOB,
          "EOInsuranceNo":this.insurance,
          "Email":formData.email,
          "FirstName":formData.firstName,
          "IsAgrement":true,
          "LandlineNo":"",
          "LastName":formData.lastName,
          "Licence":this.licenceBack,
          "MobileNo":formData.contact,
          "PostalCode":formData.postalCode,
          "ProvinceId":formData.province == '' ? formData.companyProvince : formData.province,
          "SocialInsuranceNo":formData.socialInsurance,
          "UserName":this.userName,
          "IsPersonal": this.isPersonalAccount == 'False' ? false : true,
          "GSTNo": formData.GSTNo,
          "UploadLicenceFront":this.licenceFront,
          "UserId": sessionStorage.getItem('loginUserId'),
          "LoggedInUserId":sessionStorage.getItem('loginUserId')
        }
        console.log( JSON.stringify(_input, undefined, 2));
        this.isLoadingProfile = true;
        this.brokerService.editBroker(_input)
        .subscribe((res:Response)=>{
          if(res['Status'] == 'Success'){
            sessionStorage.setItem("userName", formData.firstName);
            this.isLoadingProfile = false;
            this.service.alertMessage("Detail successfully updated","success");
          }else{
            this.isLoadingProfile = false;
            this.service.alertMessage(res['Message'],"error");
          }
        });
    }else{
      this.isLoadingProfile = false;
      this.service.alertMessage("Plase complete all required fields","error");
      return false;
    }
  }

  setActiveTab(pvarType){
    this.activeTab = pvarType;
    if(pvarType == '3'){
      this.getFee();
    }
    if(pvarType == '4'){
      //this.getPaymentDetail();
    }
  }

  menuOption: any = 1;
  setActiveTabOnChange(){
    this.activeTab = this.menuOption;
    if(this.menuOption == '3'){

    }
    if(this.menuOption == '4'){
    //  this.getPaymentDetail();
    }
  }

  getFee(){
    let _input ={
      "Id": sessionStorage.getItem('loginUserId')
    }
    this.brokerService.getBrokerFeeSetByEflex(_input)
    .subscribe((response:Response)=>{
        if(response['Status'] == "Success"){
          this.brokerMaxFee = response['BrokerFee'];
          this.myCharges = response['BrokerCommissionFee'];
          sessionStorage.setItem('brokerMaxFee', response['BrokerFee']);
          sessionStorage.setItem('defaultCommission',response['BrokerCommissionFee']);
        }else{
          this.service.alertMessage(response['Message'],'error');
        }
    })
  }

  updateFeeCharges(form){
    if(form.valid){
      if(parseFloat(form.controls.myCharges.value) > parseFloat(form.controls.brokerMaxFee.value)){
        this.myCharges = '';
        this.service.alertMessage("You can enter maximum charges $"+ this.brokerMaxFee,"error");
        return false;
      }
      const _input = {
        "CommissionCharges":form.controls.myCharges.value,
        "UserId": sessionStorage.getItem('loginUserId')
      }
      console.log(_input);
      this.brokerService.updateBrokerCommissionCharges(_input)
      .subscribe((response:Response)=>{
          if(response['Status'] == "Success"){
            this.service.alertMessage("Commission charges successfully updated","success");
            this.getFee();
          }else{
            this.service.alertMessage(response['Message'],'error');
          }
      })
    }
  }

  //cpwed
  changePwd(form){
    if(form.valid){
      if(form.controls.newPwd.value !=  form.controls.newCPwd.value){
        return false;
      }
      const _input = {
      	"NewPassword":form.controls.newPwd.value,
      	"PrevPassword":form.controls.oldPwd.value,
      	"UserId":sessionStorage.getItem("loginUserId")
      }
      this.cploading = true;
      this.brokerService.changePasswordEflexUser(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
            this.cploading = false;
            form.reset();
            this.service.alertMessage("Password successfully changed.",'error','');
        }else{
          this.cploading = false;
          this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
        this.cploading = false;
        this.service.alertMessage(error,'error','')
      })
    }else{
      return false;
    }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }

fileType: string = '';
uploadMsg: string = '';
uploadInsurance(event, type) {
     this.uploadMsg = '';
     let inputEl: HTMLInputElement = (type == 'licence-back' ? this.el.nativeElement.querySelector('#licenceFileBack') : (type == 'insurance' ? this.el.nativeElement.querySelector('#insuranceFile') : this.el.nativeElement.querySelector('#licenceFile')));
     //get the total amount of files attached to the file input.
     let fileCount: number = inputEl.files.length;
     //create a new fromdata instance
     let formData = new FormData();
     if (fileCount > 0) { // a file was selected
       var strFileName = String(inputEl.files.item(0).name);
       if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
         this.uploadMsg = "File uploading, please wait…";
         formData.append('file', inputEl.files.item(0));
         formData.append('FolderName', 'Broker');
         this.brokerService.uploadFile(formData)
         .subscribe((result)=>{
               this.uploadMsg = '';
               var respnose = result._body;
               if(respnose){
                 const img_name = respnose.split("###");
                   if(img_name[0] == 'Success'){
                       //this.fileName = this.fileName+"/"+img_name[1];
                      this.fileName = img_name[1];
                      if(type == "insurance"){
                        this.insurance = img_name[1];
                      }else if(type == "licence"){
                        this.licenceFront = img_name[1];
                      }else if(type == "licence-back"){
                        this.licenceBack = img_name[1];
                      }
                      this.service.alertMessage("File successfully uploaded","success");
                   }else{
                     this.service.alertMessage("File upload encountered an error. Please try again.","error");
                   }
               }
             },
             (error) =>{  console.log(error);  });
         }else{
           this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
           return false;
         }
       }else{
           this.uploadMsg =" Please choose file";
       }
}

updatePAPDetail(form){
  if(form.valid){
    if(form.controls.instId.value.length < 3){
      this.service.alertMessage("Institution ID must be at least 3 digits.","error");
      return false
    }
    if(form.controls.transId.value.length < 5){
      this.service.alertMessage("Transit ID must be at least 5 digits.","error");
      return false
    }

  const _input ={
    "AccountNumber": this.textEncode(form.controls.accountNumber.value),
    "BankName": this.textEncode(form.controls.bankname.value),
    "InstitutionId": this.textEncode(form.controls.instId.value),
    "LoggedInUserId":sessionStorage.getItem('loginUserId'),
    "TransitId": this.textEncode(form.controls.transId.value),
    "UserId":sessionStorage.getItem('loginUserId'),
    "UserPAPDetailId":this.PAPDetailId,
  }
  console.log(_input)
  this.loading = true;
  this.brokerService.editEflexUserPAPDetails(_input)
  .subscribe((res:Response)=>{
    if(res['Status'] == 'Success'){
        this.service.alertMessage("PAP detail update successfully.","success")
        this.loading = false;
    }else{
        this.loading = false;
        this.service.alertMessage(res['Message'],'error','')
    }
  },(error)=>{
      this.loading = false;
      this.service.alertMessage(error,'error','')
  })
}else{
  this.service.alertMessage("Plase complete all required fields","error","");
  return false;
  }
}

transformDateForFlatpickr(value){
  var datePipe = new DatePipe("en-US");
  value = datePipe.transform(value, 'yyyy-MM-dd');
  return value;
}

transformDate(value){
  var s=value;
  value=s.replace("AM"," AM")
  var datePipe = new DatePipe("en-US");
  //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
  value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
  let date = new Date(value);
   const cdate= {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()
    };
  let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
  return dt;
}

togglePassword(pvarType){
  if(pvarType == 'ti'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'in'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'acc'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }

  if(pvarType == 'op'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'np'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
  if(pvarType == 'cp'){
     var x = document.getElementById(pvarType);
     if (x['type'] === "password") {
         x['type'] = "text";
     } else {
         x['type'] = "password";
     }
  }
}

textDecode(pvarTxt){
  if(pvarTxt != undefined && pvarTxt != null && pvarTxt != '' && pvarTxt != '###'){
      return this.service.decodeText(pvarTxt);
  }
}

textEncode(pvarTxt){
    if(pvarTxt != undefined && pvarTxt != null && pvarTxt != ''){
      return this.service.encodeText(pvarTxt);
    }
}

}
//
