import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service'
import {BrokerService} from '../../broker.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod'
@Component({
  selector: 'app-broker-list',
  templateUrl: './broker-list.component.html',
  styleUrls: ['./broker-list.component.css']
})
export class BrokerListComponent implements OnInit {
  brokerData:any = [];
  isLoader:boolean;
  constructor(private service: CompanyService, private brokerService: BrokerService) {
    //  this.compnayPlanTypeId =  sessionStorage.getItem("compnayPlanTypeId");
  }

  ngOnInit() {
    if(sessionStorage.getItem('loginUserId') != null){
        this.getBrokers();
    }else{
      this.service.alertMessage("Please login first","error")
    }
  }

  getBrokers(){
   const _input = {
      "ChunckSize":1000,
     	"ChunckStart":-1,
     	"LoggedInUserId":sessionStorage.getItem('loginUserId'),
     	"SearchString":""
    }
    this.isLoader = true;
    this.brokerService.listOfChildBrokers(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.isLoader = false;
        this.brokerData= res['ListOfBrokers'];
      }else{
          this.isLoader = false;
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
        this.isLoader = false;
        this.service.alertMessage(error,'error');
    })
  }

  filterMembers(){
    this.brokerData = [];
    //this.memberTypeId = this.memberTypeId;
    this.getBrokers();
  }

  deleteBroker(obj){
    this.selectedIndex = obj;
    this.deleteConfirmation = true;
    this.name  = obj['FName'] +" "+ obj['LName'];
  }
  name: string;
  loading: boolean;
  selectedIndex: any;
  deleteConfirmation: boolean;

  yesDeleteBroker(){
    this.loading = true;
    console.log(this.selectedIndex);
    const _input = {
      "Id":this.selectedIndex['UserId'],
      "LoggedInUserId": sessionStorage.getItem('loginUserId')
    }
    console.log(_input);
    this.brokerService.deleteBroker(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.deleteConfirmation = false;
        this.loading = false;
        let index = this.brokerData.lastIndexOf(this.selectedIndex);
        this.brokerData.splice(index, 1);
        this.service.alertMessage("Broker successfully deleted","error");
      }else{
          this.service.alertMessage(res['Message'],'error');
      }
    }, (error)=>{
      this.service.alertMessage(error,'error');
    })
  }

  trackByFn(index, item) {
    return index;
  }

}
