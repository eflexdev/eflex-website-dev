import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import {CompanyService} from '../../company.service'
import {BrokerService} from '../../broker.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod'
@Component({
  selector: 'app-add-broker',
  templateUrl: './add-broker.component.html',
  styleUrls: ['./add-broker.component.css']
})
export class AddBrokerComponent implements OnInit {
  isLoading: boolean = false;
  licence: string ='';
  insurance: string = '';
  licenceBack: string = '';
  provinceData: any =[];
  hostUrl:string = '';

  address: string = '';
  city: string = '';
  companyName: string = '';
  fname: string = '';
  lname: string = '';
  contact: string = '';
  email: string = '';
  postalCode: string = '';
  province: string = '';
  username: string = '';
  GSTNo: string = '';
  socialInsurance:string ='';
  DOB:any ='';

  @ViewChild('fileInput') fileInput: ElementRef;
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
 };
 emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(
    private service : CompanyService,
    private router:Router,
    private el:ElementRef,
    private brokerService: BrokerService
  ){
      this.hostUrl = environment.hostUrl
   }

  ngOnInit() {
    this.getProvince()
  }

  getProvince(){
    this.service.populateProvince()
    .subscribe((response:Response)=>{
      if(response['Status'] == 'Success'){
        this.provinceData = response['ProvinceList'];
      }else{

      }
    })
  }
  submitDetail(form){
    if(form.controls.email.value){
        if(!form.controls.email.valid){
          this.service.alertMessage("Please enter a valid email address","error");
          return false;
        }
    }
    if(form.valid){
      // if(this.licence == ''){
      //   this.service.alertMessage("Please upload broker licence","error");
      //   return false;
      // }
    let _input = {
        "Address1":form.controls.address.value,
        "Address2":"",
        "BusinessNo":"",
        "City":form.controls.city.value,
        "Country":"",
        "Email":form.controls.email.value,
        "FirstName":form.controls.fname.value,
        "IsAgrement":true,
        "LandlineNo":'',
        "LastName":form.controls.lname.value,
        "Licence":this.licenceBack,
        "LoggedInUserId":sessionStorage.getItem('loginUserId'),
        "MobileNo": form.controls.contact.value,
        "PostalCode":form.controls.postalCode.value,
        "ProvinceId":form.controls.province.value,
        "UserName":form.controls.username.value,
        "IsPersonal":true,
        "GSTNo": form.controls.GSTNo.value,
        "UploadLicenceFront":this.licence,
        "MasterUserId": sessionStorage.getItem('loginUserId'),
        "SocialInsuranceNo":form.controls.socialInsurance.value,
        "DOB":form.controls.DOB.value.formatted,
        "EOInsuranceNo":this.insurance
      }
      console.log(_input);
      this.isLoading = true;
     this.brokerService.addBroker(_input)
     .subscribe((response:Response)=>{
       if(response['Status'] =="Success"){
           this.isLoading = false;
           this.service.alertMessage("Broker successfully added","success");
           this.router.navigate(['./broker-portal/broker-list']);
       }else{
            this.service.alertMessage(response['Message'],'error');
            this.isLoading = false;
       }
     })
    }else{
      this.service.alertMessage("Plase complete all required fields","error");
      return false;
    }
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
  }

  removeFile(){
    this.licence = '';
  }


uploadMsg = '';
fileName: string= '';
fileType: string = '';
uploadInsurance(event, type) {
       this.uploadMsg = '';
       let inputEl: HTMLInputElement = (type == 'licence-back' ? this.el.nativeElement.querySelector('#licenceFileBack') : (type == 'insurance' ? this.el.nativeElement.querySelector('#insuranceFile') : this.el.nativeElement.querySelector('#licenceFile')));
       //get the total amount of files attached to the file input.
       let fileCount: number = inputEl.files.length;
       //create a new fromdata instance
       let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('FolderName', 'Broker');
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.fileName = img_name[1];
                        if(type == "insurance"){
                          this.insurance = img_name[1];
                        }else if(type == "licence"){
                          this.licence = img_name[1];
                        }else if(type == "licence-back"){
                          this.licenceBack = img_name[1];
                        }
                        this.service.alertMessage("File upload successfully","success");
                     }else{
                       this.service.alertMessage("File upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg =" Please choose file";
         }
}
 //  uploadMsg = '';
 //  uploadFile(event) {
 //        this.uploadMsg = '';
 //        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
 //        //get the total amount of files attached to the file input.
 //        let fileCount: number = inputEl.files.length;
 //        //create a new fromdata instance
 //        let formData = new FormData();
 //        if (fileCount > 0) { // a file was selected
 //          var strFileName = String(inputEl.files.item(0).name);
 //          if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
 //            this.uploadMsg = "File uploading, please wait…";
 //            formData.append('file', inputEl.files.item(0));
 //            formData.append('FolderName', 'Licence');
 //            this.service.uploadFile(formData)
 //            .subscribe((result)=>{
 //                  this.uploadMsg = '';
 //                  var respnose = result._body;
 //                  if(respnose){
 //                    const img_name = respnose.split("###");
 //                      if(img_name[0] == 'Success'){
 //                          //this.fileName = this.fileName+"/"+img_name[1];
 //                         this.licence = img_name[1];
 //                          // this.profileForm.patchValue({
 //                          //   'Img': img_name[1]
 //                          // });
 //                          this.service.alertMessage("Licence uploaded successfully","success");
 //                      }else{
 //                        this.service.alertMessage("Licence did not upload, Please try again","error");
 //                      }
 //                  }
 //                },
 //                (error) =>{  console.log(error);  });
 //            }else{
 //              this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
 //              return false;
 //            }
 //          }else{
 //              this.uploadMsg =" Please choose licence file";
 //          }
 // }

}
