import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
import { AmazingTimePickerService } from 'amazing-time-picker'; // this lin
import {CompanyService} from '../../company.service'
import {BrokerService} from '../../broker.service'
import { Router }              from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../../member.service';
import {environment} from '../../../environments/environment.prod'
import {DatePipe, Location} from '@angular/common';
@Component({
  selector: 'app-add-referral',
  templateUrl: './add-referral.component.html',
  styleUrls: ['./add-referral.component.css']
})
export class AddReferralComponent implements OnInit {

  isLoading: boolean = false;
  licence: string ='';
  insurance: string = '';
  licenceBack: string = '';
  provinceData: any =[];
  hostUrl:string = '';

  address: string = '';
  city: string = '';
  companyName: string = '';
  fname: string = '';
  lname: string = '';
  contact: string = '';
  email: string = '';
  postalCode: string = '';
  province: string = '';
  username: string = '';
  GSTNo: string = '';
  socialInsurance:string ='';
  DOB:any ='';

  @ViewChild('fileInput') fileInput: ElementRef;
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     disableUntil: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()-1}
  };
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  employeeForm: FormGroup;
  constructor(
    private service : CompanyService,
    private router:Router,
    private el:ElementRef,
    private fb:FormBuilder,
    private brokerService: BrokerService,
    private atp: AmazingTimePickerService
  ){
      this.hostUrl = environment.hostUrl;
      this.employeeForm = this.fb.group({
        "BrokerReferralId":[-1],
        "CompanyName":['',[Validators.required]],
        "CompanyWebsite":['',[Validators.required]],
        "ContactName":['',[Validators.required]],
        "ContactTitle":['',[Validators.required]],
        "EmailAddress":['',[Validators.required]],
        "HaveGroupBenefits":['',[Validators.required]],
        "Industry":['',[Validators.required]],
        "NoOfEmployees":['',[Validators.required]],
        "Phone":['',[Validators.required]],
        "ReferralFile":[''],
        "UserId":[-1],
        "Employees": this.fb.array([this.createEmployee()])

      });
   }

   open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
    });
  }

  ngOnInit() {
    //this.getProvince()
  }
  createEmployee(): FormGroup {
    return this.fb.group({
        "Attendents":[''],
        "PreferredDateTime":['',[Validators.required]],
        "Location":['',[Validators.required]],
        "Time":['', Validators.required],
        "Notes":['',[Validators.required]]
    })
  }

  addMoreEmployee(){
    const control=<FormArray>this.employeeForm.controls['Employees'];
    control.push(this.createEmployee());
  }
  deleteEmployee(index:number){
      const control=<FormArray>this.employeeForm.controls['Employees'];
      control.removeAt(index);
  }

  submitDetail(){

    if(this.employeeForm.controls.EmailAddress.value){
        if(!this.employeeForm.controls.EmailAddress.valid){
          this.service.alertMessage("Please enter a valid email address","error");
          return false;
        }
    }
    console.log(this.employeeForm);
    if(this.employeeForm.valid){
      let _input = {
        "BrokerReferralId":-1,
        "CompanyName":this.employeeForm.controls.CompanyName.value,
        "CompanyWebsite":this.employeeForm.controls.CompanyWebsite.value,
        "ContactName":this.employeeForm.controls.ContactName.value,
        "ContactTitle":this.employeeForm.controls.ContactTitle.value,
        "EmailAddress":this.employeeForm.controls.EmailAddress.value,
        "HaveGroupBenefits":this.employeeForm.controls.HaveGroupBenefits.value == 'yes' ? true: false,
        "Industry":this.employeeForm.controls.Industry.value,
        "NoOfEmployees":this.employeeForm.controls.NoOfEmployees.value,
        "Phone":this.employeeForm.controls.Phone.value,
        "ReferralFile":this.fileName,
        "UserId":sessionStorage.getItem('loginUserId')
      }

      this.isLoading = true;
      this.brokerService.saveBrokerReferral(_input)
     .subscribe((response:Response)=>{
       if(response['Status'] =="Success"){
           this.isLoading = false;
           //this.router.navigate(['./broker-portal/broker-list']);
           this.saveTimeSlots(response['ID']);
       }else{
            this.service.alertMessage(response['Message'],'error');
            this.isLoading = false;
       }
     })
     //this.saveTimeSlots(1);
    }else{
      this.service.alertMessage("Please complete all required fields","error");
      return false;
    }
  }

  saveTimeSlots(pvrId){

    this.employeeForm.value['Employees'].forEach((lineItem)=>{
      if(lineItem.PreferredDateTime.formatted){
         lineItem['PreferredDateTime']= lineItem.PreferredDateTime.formatted+" "+lineItem.Time;
      }else{
        if(typeof  lineItem.PreferredDateTime === 'object'){
          let mm = parseInt(lineItem.PreferredDateTime.date.month) < 10 ? '0'+lineItem.PreferredDateTime.date.month: lineItem.PreferredDateTime.date.month;
          let dd = parseInt(lineItem.PreferredDateTime.date.day) < 10 ? '0'+lineItem.PreferredDateTime.date.day: lineItem.PreferredDateTime.date.day;
          lineItem['PreferredDateTime']= mm+"/"+ dd +"/"+lineItem.PreferredDateTime.date.year +" "+lineItem.Time;
        }else{
          lineItem['PreferredDateTime']= lineItem.PreferredDateTime+" "+lineItem.Time;
        }
      }
    //  console.log(this.transformDate(lineItem['PreferredDateTime']));
    });
    let input2 = {
      "BrokerReferralId":pvrId,
      "PreferredSlots":this.employeeForm.value['Employees']
    }
    console.log(input2);
    this.brokerService.savePreferredSlots(input2)
   .subscribe((response:Response)=>{
     if(response['Status'] =="Success"){
         this.isLoading = false;
         this.service.alertMessage("Referral successfully added","success");
         this.router.navigate(['./broker-portal/my-referral']);
         this.employeeForm.reset();
     }else{
          this.service.alertMessage(response['Message'],'error');
          this.isLoading = false;
     }
   })
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
  }

  removeFile(){
    this.licence = '';
  }


  uploadMsg = '';
  fileName: string= '';
  fileType: string = '';
  uploadInsurance(event, type) {
       this.uploadMsg = '';
       //let inputEl: HTMLInputElement = (type == 'licence-back' ? this.el.nativeElement.querySelector('#licenceFileBack') : (type == 'insurance' ? this.el.nativeElement.querySelector('#insuranceFile') : this.el.nativeElement.querySelector('#licenceFile')));
       let inputEl = this.el.nativeElement.querySelector('#licenceFile')
       let fileCount: number = inputEl.files.length;
        let formData = new FormData();
       if (fileCount > 0) { // a file was selected
         var strFileName = String(inputEl.files.item(0).name);
         if (strFileName.substring(strFileName.lastIndexOf('.', strFileName.length)).search(/(.jpg|.pdf|.gif|.png|.jpeg)/i) != -1) {
           this.uploadMsg = "File uploading, please wait…";
           formData.append('file', inputEl.files.item(0));
           formData.append('FolderName', 'Broker');
           this.service.uploadFile(formData)
           .subscribe((result)=>{
                 this.uploadMsg = '';
                 var respnose = result._body;
                 if(respnose){
                   const img_name = respnose.split("###");
                     if(img_name[0] == 'Success'){
                         //this.fileName = this.fileName+"/"+img_name[1];
                        this.fileName = img_name[1];
                        if(type == "insurance"){
                          this.insurance = img_name[1];
                        }else if(type == "licence"){
                          this.licence = img_name[1];
                        }else if(type == "licence-back"){
                          this.licenceBack = img_name[1];
                        }
                        this.service.alertMessage("File upload successfully","success");
                     }else{
                       this.service.alertMessage("File upload encountered an error. Please try again.","error");
                     }
                 }
               },
               (error) =>{  console.log(error);  });
           }else{
             this.service.alertMessage("Please select a file with one of the following extensions: .jpg|.pdf|.gif|.png|.jpeg|.bmp","error");
             return false;
           }
         }else{
             this.uploadMsg =" Please choose file";
         }
  }

  transformDate(value){
    var s=value;
    value=s.replace("AM"," AM")
    //var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    //value = datePipe.transform(new Date(value).toDateString(), 'yyyy-MM-dd hh:mm');
    let date = new Date(value);
    console.log(date);
    let d=date.getMonth() + 1+"/"+date.getUTCDate()+"/"+ date.getFullYear()+" "+date.getUTCHours()+":"+date.getUTCMinutes();
    //debugger;
    return d;
  }

}
