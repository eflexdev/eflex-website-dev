import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PAPDetailComponent } from './pap-detail.component';

describe('PAPDetailComponent', () => {
  let component: PAPDetailComponent;
  let fixture: ComponentFixture<PAPDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PAPDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PAPDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
