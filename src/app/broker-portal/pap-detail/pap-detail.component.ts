import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pap-detail',
  templateUrl: './pap-detail.component.html',
  styleUrls: ['./pap-detail.component.css']
})
export class PAPDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


    // getPaymentDetail(){
    //   if(sessionStorage.getItem("DbCon") != null){
    //       const _input = {
    //         "DbCon":sessionStorage.getItem("DbCon"),
    //         "Id": sessionStorage.getItem("loginUserId")
    //       }
    //       this.service.getPAPDetails(_input)
    //       .subscribe((res:Response)=>{
    //         if(res['Status'] == "Success"){
    //           if(res['GetPAPDetails'] !=null){
    //             let obj = res['GetPAPDetails'];
    //             this.bankname = obj['BankName'];
    //             this.accountNumber = obj['AccountNumber'];
    //             this.transId = obj['TransitId'];
    //             this.instId = obj['InstitutionId'];
    //             this.iagree = obj['IsAgree'];
    //             this.fileName = obj['ChequeFile'];
    //             this.PAPDetailId = obj['PAPDetailId'];
    //           }
    //         }
    //       });
    //     }else{
    //         this.router.navigate(['/login']);
    //     }
    // }

    // loading: boolean;
    // updatePAPDetail(form){
    //   if(form.valid){
    //     if(form.controls.instId.value.length < 3){
    //       this.service.alertMessage("Institution ID must be at least 3 digits.","error");
    //       return false
    //     }
    //     if(form.controls.transId.value.length < 5){
    //       this.service.alertMessage("Transit ID must be at least 5 digits.","error");
    //       return false
    //     }
    //
    //   const _input ={
    //     "AccountNumber": form.controls.accountNumber.value,
    //     "BankName":form.controls.bankname.value,
    //     "ChequeFile":this.fileName,
    //     "CompanyId":sessionStorage.getItem('companyId'),
    //     "DbCon": sessionStorage.getItem('DbCon'),
    //     "InstitutionId":form.controls.instId.value,
    //     "IsAgree":this.iagree,
    //     "IsCheque": this.fileName !='' ? true : false,
    //     "PAPDetailId":this.PAPDetailId,
    //     "TransitId":form.controls.transId.value,
    //   }
    //   console.log(_input)
    //   this.loading = true;
    //   this.service.updatePAPDetails(_input)
    //   .subscribe((res:Response)=>{
    //     if(res['Status'] == 'Success'){
    //         this.service.alertMessage("Payment detail successfully updated.","success")
    //         this.loading = false;
    //     }else{
    //         this.loading = false;
    //         this.service.alertMessage(res['Message'],'error','')
    //     }
    //   },(error)=>{
    //       this.loading = false;
    //       this.service.alertMessage(error,'error','')
    //   })
    // }else{
    //   this.service.alertMessage("Plase complete all required fields","error","");
    //   return false;
    //   }
    // }

}
