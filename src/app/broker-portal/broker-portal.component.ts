import { Component, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {CompanyService} from '../company.service';
@Component({
  selector: 'app-broker-portal',
  templateUrl: './broker-portal.component.html',
  styleUrls: ['./broker-portal.component.css']
})
export class BrokerPortalComponent implements OnInit {

  userName: string;
  userTypeId:string;
  isPersonal: any= false;
  UnreadNotificationCount: number = 0;
  MessageCount:number= 0;
  toggleMenuClass: boolean = false;
  isSwitchToogle:boolean = false;
  screenHeight:any;
  screenWidth:any;
  minusHeight:any= 0;
  constructor(private router: Router, private service: CompanyService) {
      //this.getMessgeCount()
      if(sessionStorage.getItem('loginUserId')== null){
        this.router.navigate(['/join-us']);
      }
   }

  ngOnInit() {
    //alert(sessionStorage.getItem("isPersonal"))
      this.userName = sessionStorage.getItem("userName");
      this.userTypeId = sessionStorage.getItem("userTypeId");
      this.isPersonal = sessionStorage.getItem("isPersonal");
      this.getScreenSize();
  }

  logOut(){
    sessionStorage.clear();
    this.router.navigate(['/join-us']);
  }
  toggleMenu(){
    this.toggleMenuClass = !this.toggleMenuClass;
  }
  setToggle(){
    this.isSwitchToogle = !this.isSwitchToogle;
  }

  privacy: boolean = false;
  term_conditions: boolean = false;
  openPrivacy(){
    this.privacy = true;
  }

  openTermCondition(){
    this.term_conditions = true;
  }

  @HostListener('window:resize', ['$event'])

  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 300;
        }else{
          this.minusHeight = 350;
        }
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
    }

}
