import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// import {CalendarModule} from 'primeng/calendar';
import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need

import { FormsModule, ReactiveFormsModule }        from '@angular/forms';
import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import { BrokerPortalRoutingModule } from './broker-portal-routing.module';
import { MyDatePickerModule } from 'mydatepicker';
import {MyDate, DateTime} from "./date-pipe";
//import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { BrokerPortalComponent } from '../broker-portal/broker-portal.component';
import { DashboardComponent } from '../broker-portal/dashboard/dashboard.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { BrokerListComponent } from './broker-list/broker-list.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { SettingsComponent } from './settings/settings.component';
import { PAPDetailComponent } from './pap-detail/pap-detail.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { AddBrokerComponent } from './add-broker/add-broker.component';
import { MyReferralComponent } from './my-referral/my-referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    MyDatePickerModule,
    BrowserModule,
    BrowserAnimationsModule,
    AmazingTimePickerModule,
    // CalendarModule,
    //OwlDateTimeModule,OwlNativeDateTimeModule,
    BrokerPortalRoutingModule,FormsModule,ReactiveFormsModule
  ],
  declarations: [BrokerPortalComponent,DashboardComponent,
    CompanyListComponent, BrokerListComponent,
    TransactionsComponent,
    SettingsComponent, PAPDetailComponent,
     AddCompanyComponent, AddBrokerComponent,
     MyDate,  DateTime, MyReferralComponent, AddReferralComponent,
   ]
})
export class BrokerPortalModule { }
