import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReferralComponent } from './my-referral.component';

describe('MyReferralComponent', () => {
  let component: MyReferralComponent;
  let fixture: ComponentFixture<MyReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
