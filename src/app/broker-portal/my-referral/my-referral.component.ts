import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../company.service';
import {BrokerService} from '../../broker.service'
import {Router} from "@angular/router"
@Component({
  selector: 'app-my-referral',
  templateUrl: './my-referral.component.html',
  styleUrls: ['./my-referral.component.css']
})
export class MyReferralComponent implements OnInit {

  referralData: any=[];
  isViewReferralModal: boolean = false;
  constructor(
     private service: CompanyService,
     private router : Router,
     private brokerService: BrokerService
   ) { }

  ngOnInit() {
    let _input = {
      "ChunckSize":1000,
      "ChunckStart":-1,
      "LoggedInUserId":sessionStorage.getItem('loginUserId'),
      "SearchString":''
    }
    this.brokerService.listOfBrokerReferrals(_input)
    .subscribe((response:Response)=>{
      if(response['Status'] =="Success"){
        this.referralData = response['BrokerReferralData'];
      }else{
        this.service.alertMessage(response['Message'],"error");
      }
    },(error:Error)=>{
       this.service.alertMessage(error['Message'],"error");
    });
  }

  referralDetail: any[];
  viewTimeSlots(pvarData){
      this.isViewReferralModal = true;
      let _input = {
        "Id": pvarData['BrokerReferralId']
      }
      this.brokerService.getPreferredSlotsById(_input)
      .subscribe((response:Response)=>{
        if(response['Status'] =="Success"){
          this.referralDetail = response['PreferredSlotsList'];
        }else{
          this.service.alertMessage(response['Message'],"error");
        }
      },(error:Error)=>{
         this.service.alertMessage(error['Message'],"error");
      });
  }

}
