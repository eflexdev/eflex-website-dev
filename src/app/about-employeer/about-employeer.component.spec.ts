import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutEmployeerComponent } from './about-employeer.component';

describe('AboutEmployeerComponent', () => {
  let component: AboutEmployeerComponent;
  let fixture: ComponentFixture<AboutEmployeerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutEmployeerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutEmployeerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
