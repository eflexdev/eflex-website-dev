import { Injectable, Output,EventEmitter } from '@angular/core';
import {environment} from '../environments/environment.prod'
import { Headers, Http, Response } from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/Rx';
import { Observable,Subject } from 'rxjs';
declare var Noty: any;
@Injectable()
export class CompanyService {
  isConnected:Observable<boolean>
  @Output() fire: EventEmitter<any> = new EventEmitter();
  @Output() userToken: EventEmitter<any> = new EventEmitter();

   serviceUrl:string;
   public storageSub= new Subject<string>();
   fileUploadUrl:string;
   authToken: string;
   //authToken: string; // sessionStorage.getItem('UserToken')
   public headers;
    constructor(
       private http : Http,
       private _router: Router
    ) {
        this.serviceUrl = environment.serviceUrl;
        this.fileUploadUrl = environment.fileUplaodUrl;
        this.headers = new Headers({'Content-Type': 'application/json','TokenHeader': this.authToken})
     }

   setCompnayName(value, userName) {
    console.log('change started');
     const obj ={
       login : true,
       userName: userName
     }
     this.fire.emit(obj);
   }

   setActiveToken(value){
       console.log('change started'+ value);
      //this.authToken.emit(value);
      this.authToken = value;
      this.userToken.emit(value);
   }

   getEmittedValue() {
     return this.fire;
  }
  getUserToken() {
    return this.userToken;
 }

  watchStorage():Observable<any>{
     return this.storageSub.asObservable();
}

  alertMessage(msg, type,time?){
      let timeFixed=time;
      if(timeFixed==undefined){
        timeFixed=2000
      }
      var n = new Noty({
          theme:'stratgist',
          text: '<strong>'+ msg +'</strong>',
          type   : type,
          progressBar: true,
          closeWith: ['click', 'button'],
          timeout:2000

        }).show();
      Noty.setMaxVisible(1,'notification');
    }

    getRandomCode(){
      // let headers = new Headers();
      // headers.append('Content-Type', 'application/json');
      // headers.append('TokenHeader', 'qwe');
      //const headers = new Headers({'Content-Type': 'application/json'})
      // "Access-Control-Allow-Headers": "Content-Type",
      let headers = new Headers({'Content-Type': 'application/json'});
      return this.http.get(this.serviceUrl+'/GetRandomCode'
      ,{headers: this.headers})
      .map(
        (response: Response) => { const data = response.json();
          return data;
      }).catch((error)=>{
          return error;
      })
    }

  populateProvince(){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.get(this.serviceUrl+'/PopulateProvince',{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addCompany(inputData){
    //inputData.LoggedInUserId = sessionStorage.getItem("loginUserId")  == null ?  sessionStorage.getItem("loginUserId") : 1;
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddCompany',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  ValidateUniqueCode(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json'})
    return this.http.post(this.serviceUrl+'/ValidateUniqueCode',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  getCompanyDetails(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetCompanyDetails',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  addCompanyPlan(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddCompanyPlan',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  updateCompanyPlanType(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/UpdateCompanyPlanType',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }
  loginUser(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/LoginUser',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  forgotPassword(inputData){
    //inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ForgotPassword',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  populateClasses(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/PopulateClasses',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

  addEditEmployess(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddEditEmployess',
    inputData,{headers: headers})
    .map(
      (response: Response) => { const data = response.json();
        return data;
    }).catch((error)=>{
        return error;
    })
  }

addCompanyFinancialDetails(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/AddCompanyFinancialDetails',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}


getPaymentOverview(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetPaymentOverview',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

getCompanyDashboardData(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  //const headers = new Headers({'Content-Type': 'application/json'}) = new Headers({'Content-Type': 'application/json'})
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetCompanyDashboardData',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

getDashboardTransactions(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetDashboardTransactions',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

uploadFile(formData){
   return this.http.post(this.fileUploadUrl, formData)
   .map((response: Response) => {
       const data = response;
       return data;
     }).catch(
     (error: Error) => {
       return Observable.throw(error);
     });
}

listEmployees(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ListEmployees',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

inActiveEmployee(inputData){
   inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
   let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
   return this.http.post(this.serviceUrl+'/InActiveEmployee',inputData,{headers:headers})
  .map((response:Response)=>{
     return response.json();
  })
  .catch((error)=>{
    return error;
  })
}


changePassword(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
   return this.http.post(this.serviceUrl+'/ChangePassword',inputData,{headers:headers})
  .map((response:Response)=>{
     return response.json();
  })
  .catch((error)=>{
    return error;
  })
}

getStatementPeriod(inputData){
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetStatementPeriod',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

getActivityData(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetActivityData',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}


getPAPDetails(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetPAPDetails',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

updatePAPDetails(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
      let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
      return this.http.post(this.serviceUrl+'/UpdatePAPDetails',inputData,{headers:headers})
     .map((response:Response)=>{
        return response.json();
     })
     .catch((error)=>{
       return error;
     })
}

updateCompanyDetails(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
      let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
      return this.http.post(this.serviceUrl+'/UpdateCompanyDetails',inputData,{headers:headers})
     .map((response:Response)=>{
        return response.json();
     })
     .catch((error)=>{
       return error;
     })
}

addEmployee(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
      let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
      return this.http.post(this.serviceUrl+'/AddEmployee',inputData,{headers:headers})
     .map((response:Response)=>{
        return response.json();
     })
     .catch((error)=>{
       return error;
     })
}

importEmployeesExcel(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ImportEmployeesExcel',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

deleteUser(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/DeleteUser',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}
addAdmin(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddAdmin',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

updatePlanDetails(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/UpdatePlanDetails',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}
getEStatement(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetEStatement',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

updatePaymentDate(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/UpdatePaymentDate',inputData,{headers:this.headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

listOfNotificationLog(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/ListOfNotificationLog',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}


addPlan(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddPlan',inputData,{headers:headers})
   .map((response:Response)=>{
      return response.json();
   })
   .catch((error)=>{
     return error;
   })
}

getNotiMsgCount(inputData){
      inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/GetNotiMsgCount',inputData,{headers:headers})
   .map((response:Response)=>{
     const data = response.json();
     if(data['Status']=='Error'){
       this.alertMessage(data['Message'],'error');
       this._router.navigate(['/'])
     }else{
       return data;
     }
     return data;
   })
   .catch((error)=>{
     return error;
   })
}

addUserCarrier(inputData){
    inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
    let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
    return this.http.post(this.serviceUrl+'/AddUserCarrier',inputData,{headers:headers})
   .map((response:Response)=>{
     const data = response.json();
     if(data['Status']=='Error'){
       this.alertMessage(data['Message'],'error');
       this._router.navigate(['/'])
     }else{
       return data;
     }
   })
   .catch((error)=>{
     return error;
   })
}

populateReferrals(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/PopulateReferrals',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

populateBrokers(){
  //inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.get(this.serviceUrl+'/PopulateBrokers',{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

saveCompanyReferrals(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/SaveCompanyReferrals',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

listAllCompanyUsers(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ListAllCompanyUsers',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

decodeText(pvarText){
   if(pvarText != undefined && pvarText != null && pvarText != '' && pvarText != '###'){
     return decodeURIComponent(Array.prototype.map.call(atob(pvarText), function(c) {
       return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
     }).join(''));
   }
 }

 encodeText(str){
   if(str != undefined && str != null && str != ''){
     return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => {
         return String.fromCharCode(("0x" + p1) as any);
     }));
   }
}

getRecentClaimDashboard(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetRecentClaimDashboard',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
getEmpPlanDashboardDetails(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/GetEmpPlanDashboardDetails',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

resentOTP(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ResentOTP',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

validatePasscode(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ValidatePasscode',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

deleteEmployee(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/DeleteEmployee',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
employeeClaimListForCompany(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/EmployeeClaimListForCompany',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
populateCarrier(){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.get(this.serviceUrl+'/PopulateCarrier',{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

addGetInTouch(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/AddGetInTouch',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

sendFinalMailToEmployee(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/SendFinalMailToEmployee',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

importEmployeesExcelOnRegister(inputData){
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ImportEmployeesExcelOnRegister',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

authorizeUser(inputData){
  let headers = new Headers({'Content-Type': 'application/json'})
  return this.http.post(this.serviceUrl+'/AuthorizeUser',
  inputData,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}

ListEmployeesV2(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/ListEmployeesV2',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}
sendMailToUnUpdatedUser(inputData){
  inputData.LoggedInUserId = sessionStorage.getItem("loginUserId");
  let headers = new Headers({'Content-Type': 'application/json','TokenHeader': sessionStorage.getItem('UserToken')})
  return this.http.post(this.serviceUrl+'/SendMailToUnUpdatedUser',inputData
  ,{headers: headers})
  .map(
    (response: Response) => { const data = response.json();
      return data;
  }).catch((error)=>{
      return error;
  })
}





}
