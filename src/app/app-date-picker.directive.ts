import { Directive,ElementRef , AfterViewInit,Input } from '@angular/core';
import {CompanyService} from './company.service';

@Directive({
  selector: '[appDatePicker]'
})
export class AppDatePickerDirective implements AfterViewInit{
  @Input() options
  constructor(private el:ElementRef,private service:CompanyService) {

  }

  ngAfterViewInit(){
  let self=this.el.nativeElement;

  if(this.options!=null){
      console.log(this.options);
    self.flatpickr(this.options);
  }
  this.service.watchStorage().subscribe(()=>{
    this.options=JSON.parse(sessionStorage.getItem('options'));
    console.log(this.options);
    if(this.options!=null){
      self.flatpickr(this.options);
    }
  })


}

}
