import { Component, HostListener } from '@angular/core';
import {CompanyService} from './company.service';
import {Router}  from "@angular/router";
import { Idle } from 'idlejs/dist';
declare function checkOnline():boolean;
declare function CheckAPI():any;
export let isOnline:boolean;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  screenHeight:any;
  screenWidth:any;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  constructor(private service: CompanyService, private router : Router){

      this.getScreenSize();

    this.service.getRandomCode()
    .subscribe((res:Response)=>{
      if(res['Status'] =="Success"){
        sessionStorage.setItem("RandomCode",res['RandomCode']);
      }else{
        this.service.alertMessage("Some thing went worng","error","0")
      }
    });
    setInterval(() => {
      isOnline= navigator.onLine;
      if(!isOnline){
        this.router.navigate(['/offline']);
      }
    }, 3000);

    // with predefined events on `document`
    const idle = new Idle()
    .whenNotInteractive()
    .within(30)
    .do(() => //console.log('time out')
        this.sessionTimeOut()
    )
    .start();
 }

 sessionTimeOut(){
     //this.service.alertMessage("Session has been timeout","success");
     sessionStorage.clear();
     sessionStorage.removeItem('loginUserId');
     sessionStorage.removeItem('UserToken');
     this.router.navigate(['/']);
 }

 @HostListener('window:resize', ['$event'])

 getScreenSize(event?) {
       this.screenHeight = window.innerHeight;
       this.screenWidth = window.innerWidth;
   }

}
