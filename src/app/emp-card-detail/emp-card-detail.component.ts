import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import {CompanyService} from '../company.service';
import {DatePipe} from "@angular/common";
import { Router }              from '@angular/router';
import { ForgetForm }            from '../register/data/formData.model';
import {environment} from '../../environments/environment.prod';
import {IMyDpOptions} from 'mydatepicker';
import {MemberService} from '../member.service';
@Component({
  selector: 'app-emp-card-detail',
  templateUrl: './emp-card-detail.component.html',
  styleUrls: ['./emp-card-detail.component.css']
})
export class EmpCardDetailComponent implements OnInit {

  loading:boolean;
  modal:boolean;
  firstName:any = '';
  lastName: any ='';
  email: any = '';
  mobileNumber: any ='';
  city: any ='';
  postalCode:any = '';
  address:any = '';
  dob:any='';
  sin: any = '';
  confirmSin: any = '';
  state: any = '';
  gender: any = '';
  infoModal: boolean = false;
  companyId: any;
  dbCon: any;
  hostUrl:any='';
  userTypeId: any = '';
  loginUserName:any ='';
  provienceData:any = [];
  d = new Date();
  public myDatePickerOptions: IMyDpOptions = {
     dateFormat: 'mm/dd/yyyy',
     editableDateField: false,
     disableSince: {year: this.d.getFullYear(), month: this.d.getMonth()+1, day: this.d.getDate()}
  };
  constructor( private service: CompanyService,
    private router: Router,
    private el: ElementRef,
    private memberService: MemberService
  ) {
      this.hostUrl = environment.hostUrl
   }

  ngOnInit() {
    if(sessionStorage.getItem("companyId") == null){
      this.router.navigate(['/login']);
    }
    this.infoModal = true;
    this.loginUserName = sessionStorage.getItem('userName');

    if(sessionStorage.getItem("cardCreated") != undefined && sessionStorage.getItem("cardCreated") == 'true' ){
       this.router.navigate(['/bank-detail']);
    }else{
      this.service.populateProvince()
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          this.provienceData = res['ProvinceList'];
          this.getEmpDetail()
        }else{
          console.log(res['Message']);
        }
      },(error)=>{
        //this.service.alertMessage(error,'error','')
      })
    }

    this.getScreenSize()
  }



  screenHeight: any;
  minusHeight: any = 0;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        if(window.innerHeight < 1200){
          this.minusHeight = 0;
        }else{
          this.minusHeight = 150;
        }
        this.screenHeight = window.innerHeight;
    }

  getEmpDetail(){
    const _input={
      "Id": sessionStorage.getItem('loginUserId'),
      "DbCon": sessionStorage.getItem("DbCon")
    };
    this.memberService.getEmpDetailsById(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == "Success"){
        this.firstName = res['GetEmpBasicDetails']['FirstName'];
        this.lastName = res['GetEmpBasicDetails']['LastName'];
        this.email = res['GetEmpBasicDetails']['EmailAddress'];
        this.mobileNumber = res['GetEmpBasicDetails']['MobileNo'];
        sessionStorage.setItem('mobileNo',  res['GetEmpBasicDetails']['MobileNo']);
        this.city = res['GetEmpBasicDetails']['City'];
        this.postalCode = res['GetEmpBasicDetails']['PostalCode'];
        this.address = res['GetEmpBasicDetails']['Address'];
        this.state = res['GetEmpBasicDetails']['Province'];
        this.gender = res['GetEmpBasicDetails']['Gender'];
        if(res['GetEmpBasicDetails']['DOB']){
            this.dob = this.transformDate(res['GetEmpBasicDetails']['DOB']);
        }
      }else{
        this.service.alertMessage(res['Message'],'error');
        return false;
      }

     })
  }

  submitDetail(form){
    if(form.controls.sin.value.length < 4 ){
      this.service.alertMessage("Please enter a 4-digit access code","error");
      return false;
    }
    if(form.controls.sin.value != form.controls.confirmSin.value){
      this.service.alertMessage("Confirmation access code did not match. Please try again.","error");
      return false;
    }
    let name = form.controls.lastName.value+" "+form.controls.lastName.value;
    if(name.length > 20){
      this.service.alertMessage("The max combined length of First Name + Last Name must be less than 21 characters","error");
      return false;
    }

    let dob = '';
    if(form.controls.dob.value.formatted){
       dob= form.controls.dob.value.formatted;
    }else{
      if(typeof  form.controls.dob.value === 'object'){
        let mm = parseInt(form.controls.dob.value.date.month) < 10 ? '0'+form.controls.dob.value.date.month:form.controls.dob.value.date.month;
        let dd = parseInt(form.controls.dob.value.date.day) < 10 ? '0'+form.controls.dob.value.date.day: form.controls.dob.value.date.day;
        dob= mm+"/"+ dd +"/"+form.controls.dob.value.date.year;
      }else{
        dob= form.controls.dob.value;
      }
    }
    if(form.valid){
      console.log(form);
      const _input ={
      	  "Address":form.controls.address.value,
        	"Address2":"",
        	"BusinessPhone":"",
        	"Carrier":"",
        	"City":form.controls.city.value,
        	"CompanyUserId":  sessionStorage.getItem("loginUserId"),
        	"DOB":dob,
        	"DbCon":sessionStorage.getItem("DbCon"),
        	"EmailAddress":form.controls.email.value,
        	"EveningPhone":"",
    	    "FirstName": form.controls.firstName.value,
        	"Gender":this.gender,
        	"HomePhone":"",
        	"IsNotify":false,
        	"LastName":form.controls.lastName.value,
        	"MobileNo":form.controls.mobileNumber.value,
        	"MorningPhone":"",
          "PostalCode":form.controls.postalCode.value,
        	"Province":this.state,
        	"Sin":form.controls.sin.value
      }
      this.loading = true;
      this.memberService.updateEmpBasicDetails(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] == 'Success'){
          sessionStorage.setItem("userName", form.controls.firstName.value);
           sessionStorage.setItem('mobileNo', form.controls.mobileNumber.value);
           this.createBarkelyCard();
        }else{
            this.loading = false;
            this.service.alertMessage(res['Message'],'error','')
        }
      },(error)=>{
          this.loading = false;
          this.service.alertMessage(error,'error','')
      })
    }else{
      this.service.alertMessage("Plase complete all required fields","error","");
      return false;
    }
  }

  createBarkelyCard(){
    let _input = {
    	"CompanyId":sessionStorage.getItem('companyId'),
    	"DbCon":sessionStorage.getItem('DbCon'),
    	"UserId":sessionStorage.getItem("loginUserId")
    }
    this.memberService.createCard(_input)
    .subscribe((res:Response)=>{
      if(res['Status'] == 'Success'){
          this.loading = false;
          sessionStorage.setItem("cardCreated", 'true');
          //this.isCardCreated = true;
          if(this.userTypeId != 1){
             this.service.alertMessage("Detail has been submitted successfully.","success");
             //this.router.navigate(['/member-portal/dashboard']);
             this.router.navigate(['/bank-detail']);
          }else{
             this.service.alertMessage(res['Message'],"error");
          }
      }else{
          this.loading = false;
          this.service.alertMessage(res['Message'],'error','')
      }
    },(error)=>{
        this.loading = false;
        this.service.alertMessage(error,'error','')
    })
  }

  transformDate(value){
    var s=value;
    value=s.replace("AM"," AM")
    var datePipe = new DatePipe("en-US");
    //value = datePipe.transform(new Date(value).toDateString(), 'MMM d, y');
    value = datePipe.transform(new Date(value).toDateString(), 'MM/dd/yyyy');
    let date = new Date(value);
     const cdate= {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
      };
    let dt =  {date: { year: date.getFullYear(), month:  date.getMonth() + 1, day: date.getDate() }}
    return dt;
  }

  _keyPress(event: any) {
     const pattern = /[0-9\+\-\ ]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
     }
 }
 logOut(){
   sessionStorage.clear();
   this.service.alertMessage("You are logout successfully","success");
   this.router.navigate(['/join-us']);
 }

}
