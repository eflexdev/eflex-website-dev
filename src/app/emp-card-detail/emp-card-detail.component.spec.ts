import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpCardDetailComponent } from './emp-card-detail.component';

describe('EmpCardDetailComponent', () => {
  let component: EmpCardDetailComponent;
  let fixture: ComponentFixture<EmpCardDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpCardDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpCardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
