import { Component, OnInit } from '@angular/core';
import {CompanyService}  from "../company.service";
import { FormBuilder, FormGroup, FormArray,FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  privacy: boolean = false;
  term_conditions: boolean = false;
  contactForm: FormGroup;
  isLoading: boolean;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private service : CompanyService,   private fb:FormBuilder) { }

  ngOnInit() {
    this.loadScript();
    this.contactForm = this.fb.group({
        "name":['', Validators.required],
        "message": ['',Validators.required],
        "email":['', [Validators.required, Validators.pattern(this.emailPattern)]]
      });
  }

  openPrivacy(){
    this.privacy = true;
  }

  openTermCondition(){
    this.term_conditions = true;
  }

  saveGetInTuch(){

    let formData = this.contactForm.value;
    if(formData.email != '' && formData.name != '' && formData.message !=''){
      let _input = {
        "Email": formData.email,
  	    "Messages":formData.message,
  	    "Name":formData.name
      }
      this.isLoading = true;
      this.service.addGetInTouch(_input)
      .subscribe((res:Response)=>{
        if(res['Status'] =="Success"){
          this.isLoading = false;
          this.service.alertMessage("Thank you for connecting with us","success");
          formData.email = '';
          formData.name = '';
          formData.message = '';
          this.contactForm.reset({
              "name": '',
              "email": '',
              "message": ''
          });
          this.contactForm.reset();
        }else{
          this.isLoading = false;
          this.service.alertMessage(res['Message'],"error")
        }
      });
    }else{
        this.isLoading = false;
        this.service.alertMessage("Enter required information","error")
    }
  }

  loadScript() {
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
        if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
            isFound = true;
        }
    }

    if (!isFound) {
        var dynamicScripts = ["https://seal.godaddy.com/getSeal?sealID=MzToSEvY4Wl2rpOpLo1vCss3fX6s7r0Ot4e1AvzgnHPeP8oIvKOB1LxwCCNo"];

        for (var i = 0; i < dynamicScripts .length; i++) {
            let node = document.createElement('script');
            node.src = dynamicScripts [i];
            node.type = 'text/javascript';
            node.async = true;
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);
        }

    }
  }

}
